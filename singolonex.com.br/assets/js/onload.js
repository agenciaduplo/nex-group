$(function(){
	$('.nav').localScroll({duration: 1000, queue: true});
	$('.bg_home_1').parallax("center", 0.25);
	//$('.bg_home_2').parallax("center", -0.4);
	//$('.bg_infra').parallax("center", -0.8);
	$('.bg_plantas').parallax("center", -0.8);
	//$('.bg_localiza').parallax("center", -0.1);
	//$('.bg_contato').parallax("center", -0.08);
	$('.bg_tour').parallax("center", -0.2);
	$(".various5").fancybox({
		'width'				: '100%',
		'height'			: '100%',
		'autoScale'     	: true,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});	
	$(".modal_plantas").fancybox({
		'width'				: '100%',
		'height'			: '100%',
		'autoScale'     	: true,
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6
	});
	$(".modal").fancybox({
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6
	});
	$(".modal2").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'title'			: this.title,
			'width'			: 720,
			'height'		: 430,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
				'wmode'			: 'transparent',
				'allowfullscreen'	: 'true'
			}
		});
		return false;
	});
	$('#myCarousel').carousel("pause");
	$('#form_telefone').mask('(99) 9999-9999');
	$("#form_main").validationEngine('attach', {promptPosition : "topRight"});
	$("header nav .drop").hover(function(){
		$(this).find("a:first").addClass("hover");
		//$(this).find(".legenda").stop(true,true).fadeIn(200);
	},function(){
		$(this).find("a:first").removeClass("hover");
		//$(this).find(".legenda").stop(true,true).fadeOut(200);
	});
	$('#link2dorms').click(function(){
		$('.openTab').removeClass('openTab');
		$('#link2dorms').addClass('openTab');
		$('#show3dorms,#showGarden,#showImplant').stop(true,true).fadeOut('fast');
		$('#show2dorms').stop(true,true).delay(500).fadeIn('slow','linear');
	});
	$('#link3dorms').click(function(){
		$('.openTab').removeClass('openTab');
		$('#link3dorms').addClass('openTab');
		$('#show2dorms,#showGarden,#showImplant').stop(true,true).fadeOut('fast');
		$('#show3dorms').stop(true,true).delay(500).fadeIn('slow','linear');
	});
	$('#linkGarden').click(function(){
		$('.openTab').removeClass('openTab');
		$('#linkGarden').addClass('openTab');
		$('#show3dorms,#show2dorms,#showImplant').stop(true,true).fadeOut('fast');
		$('#showGarden').stop(true,true).delay(500).fadeIn('slow','linear');
	});
	$('#linkImplant').click(function(){
		$('.openTab').removeClass('openTab');
		$('#linkImplant').addClass('openTab');
		$('#show3dorms,#show2dorms,#showGarden').stop(true,true).fadeOut('fast');
		$('#showImplant').stop(true,true).delay(500).fadeIn('slow','linear');
	});
});