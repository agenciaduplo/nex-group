<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {

	public function sendInteresseCaptcha($sid = 0){
		require_once('./recaptcha/recaptchalib.php');
		$privatekey = "6Le4ovASAAAAAM9h3MX5k4mg53zHU1M0cFkFd71i";
		$resp = recaptcha_check_answer ($privatekey,
		                            $_SERVER["REMOTE_ADDR"],
		                            $_POST["recaptcha_challenge_field"],
		                            $_POST["recaptcha_response_field"]);

		if(!$resp->is_valid){
			$base = site_url();

			$return = $this->input->get('return');
			$return = !empty($return) ? $return : $base . "#contato";
			$_SESSION['erro'] = true;

			$_SESSION['formNome'] 				= $this->input->post('form_nome');
			$_SESSION['formEmail'] 				= $this->input->post('form_email');
			$_SESSION['formTelefone'] 		= $this->input->post('form_telefone');
			$_SESSION['formComentarios'] 	= $this->input->post('form_comentario');


			redirect($return);

			// What happens when the CAPTCHA was entered incorrectly
			//die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
		     //"(reCAPTCHA said: " . $resp->error . ")");
		}else{
			$this->load->library('user_agent');

			if($this->agent->is_robot()){
				exit;
			}

			$id_empreendimento = 82;

			$this->load->model('contato_model','model');

			$url 					= $_SESSION['url'];
			$url 					= explode("__",$url);

			$utm_source 	= $url[0];
			$utm_medium   = $url[1];
			$utm_campaign = $url[3];

			$email_nex 		= "vendas@nexvendas.com.br";
			$email_out 		= "lucia.keiko@vendasbbsul.com.br";
			$email_go			= $email_nex;
			$id_empresa 	= $this->model->getIdEmpresa($id_empreendimento);
			$id_empresa 	= $id_empresa[0]->id_empresa;
			$id_cidade 		= $this->model->getIdCidade($id_empreendimento);
			$id_cidade 		= $id_cidade[0]->id_cidade;

			if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
				$email_for =  $this->model->verificaLastEmailSendFor(1);
				foreach($email_for as $emf){
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$email_go = $email_out;
						break;
					}else{
						$email_go = $email_nex;
						break;
					}
				}
			}else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)){ // Porto Alegre | 1 quarto dos interesses para fora.
				$email_for =  $this->model->verificaLastEmailSendFor(3);
				$i 				 = 0;
				$flag 		 = false;

				foreach($email_for as $emf){
					++$i;
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$flag = true;
					}else if($emf->email_enviado_for == $email_out && $i = 1){
						$email_go = $email_nex;
						break;
					}else{
						$flag = false;
					}

					if($flag){
						$email_go = $email_out;
					}else{
						$email_go = $email_nex;
					}
				}
			}else{
				$email_go = $email_nex;
			}

			if($sid == session_id()){
				$return = $this->input->get('return');
				$return = !empty($return) ? $return : site_url() . "#contato";

				$data = array(
					'id_empreendimento' 	=> $id_empreendimento,
					'id_estado' 					=> 21,
					'ip' 									=> $this->input->ip_address(),
					'user_agent' 					=> $this->input->user_agent(),
					'url' 								=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
					'origem' 							=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
					'data_envio' 					=> date("Y-m-d H:i:s"),
					'nome' 								=> $this->input->post('form_nome'),
					'email' 							=> strtolower($this->input->post('form_email')),
					'telefone' 						=> $this->input->post('form_telefone'),
					'comentarios' 				=> nl2br(strip_tags($this->input->post('form_comentario'))),
					'hotsite' 						=> 'S',
					'email_enviado_for' => $email_go
				);

				if(!empty($data['email']) && !empty($data['nome'])){
					$interesse = $this->model->setInteresse($data);
					$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

					$dados = array(
						'Enviado em ' => $data['data_envio'] . ' via HotSite',
						'Enviado por ' => $data['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $data['ip'],
						'E-mail' => $data['email'],
						'Telefone' => $data['telefone'],
						('Comentários') => $data['comentarios']
					);

					$this->load->library('email',array(
						'protocol' => 'sendmail',
						'mailtype' => 'html',
						'charset' => 'utf-8',
						'wordwrap' => TRUE
					));

					/* envia email via roleta */
					$id_empreendimento = 82;
					$grupos = $this->model->getGrupos($id_empreendimento);

					$total = 0;
					$total = count($grupos);

					foreach ($grupos as $row):
						if($row->rand == 1):
							$atual = $row->ordem;

							if($atual == $total):

								$this->model->updateRand($id_empreendimento, '001');

								$emails = $this->model->getGruposEmails($id_empreendimento, '001');
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

							else:

								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);

								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

							endif;
						endif;
					endforeach;
					
          if($email_go == $email_out){
            $list = $email_go;
          }else{
            $list[] = $email_go;
          }
					/* envia email via roleta */

					if($p['nome'] == "Teste123"){
						$this->email->to('testes@divex.com.br');
					} else {
						$this->email->to($list);
					}
					$this->email->bcc('testes@divex.com.br');
					$this->email->from("noreply@singolonex.com.br", "SÍNGOLO - Nex Group");
					$this->email->subject('Contato enviado via HotSite');

					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));


					$this->email->send();

					$_SESSION['sucesso'] = true;
					$_SESSION['tracker'] = array(
						'email' => $data['email'],
						'nome' => $data['nome']
					);
				}
				redirect($return);
			} else { show_404(); }

		}
	}
	
	
	public function sendInteresse($sid = 0){
		$this->load->library('user_agent');

		if($this->agent->is_robot()){
			exit;
		}

		$this->load->model('contato_model','model');


		$id_empreendimento = 82;

		$url 					= $_SESSION['url'];
		$url 					= explode("__",$url);

		$utm_source 	= $url[0];
		$utm_medium   = $url[1];
		$utm_campaign = $url[3];

		// $email_nex 		= "vendas@nexvendas.com.br";
		// $email_out 		= "lucia.keiko@vendasbbsul.com.br";
		$email_nex 		= "anita.chiele@divex.com.br";
		$email_out 		= "anita.chiele@divex.com.br";
		$email_go			= $email_nex;
		$id_empresa 	= $this->model->getIdEmpresa($id_empreendimento);
		$id_empresa 	= $id_empresa[0]->id_empresa;
		$id_cidade 		= $this->model->getIdCidade($id_empreendimento);
		$id_cidade 		= $id_cidade[0]->id_cidade;


		if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
			$email_for =  $this->model->verificaLastEmailSendFor(1);
			foreach($email_for as $emf){
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$email_go = $email_out;
					break;
				}else{
					$email_go = $email_nex;
					break;
				}
			}
		}else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)){ // Porto Alegre | 1 quarto dos interesses para fora.
			$email_for =  $this->model->verificaLastEmailSendFor(3);
			$i 				 = 0;
			$flag 		 = false;

			foreach($email_for as $emf){
				++$i;
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$flag = true;
				}else if($emf->email_enviado_for == $email_out && $i = 1){
					$email_go = $email_nex;
					break;
				}else{
					$flag = false;
				}

				if($flag){
					$email_go = $email_out;
				}else{
					$email_go = $email_nex;
				}
			}
		}else{
			$email_go = $email_nex;
		}

		if($sid == session_id()){
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' => $id_empreendimento,
				'id_estado' 				=> 21,
				'ip' 								=> $this->input->ip_address(),
				'user_agent' 				=> $this->input->user_agent(),
				'url' 							=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' 						=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' 				=> date("Y-m-d H:i:s"),
				'nome' 							=> $this->input->post('form_nome'),
				'email' 						=> strtolower($this->input->post('form_email')),
				'telefone' 					=> $this->input->post('form_telefone'),
				'comentarios' 			=> nl2br(strip_tags($this->input->post('form_comentario'))),
				'hotsite' 					=> 'S',
				'email_enviado_for' => $email_go
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				$dados = array(
					'Enviado em ' => $data['data_envio'] . ' via HotSite',
					'Enviado por ' => $data['nome'],
					'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					'IP' => $data['ip'],
					'E-mail' => $data['email'],
					'Telefone' => $data['telefone'],
					('Comentários') => $data['comentarios']
				);
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));

				/* envia email via roleta */
				$id_empreendimento = 82;
				$grupos = $this->model->getGrupos($id_empreendimento);

				$total = 0;
				$total = count($grupos);
				
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;

						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');

							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);

							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						endif;
					endif;
				endforeach;
				$list[] = $email_go;
				/* envia email via roleta */

				if($p['nome'] == "Teste123"){
					$this->email->to('testes@divex.com.br');
				} else {
					$this->email->to($list);
				}
				$this->email->bcc('testes@divex.com.br');
				$this->email->from("noreply@singolonex.com.br", "SÍNGOLO - Nex Group");
				$this->email->subject('Contato enviado via HotSite');

				$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));

				$this->email->send();

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
			}
			redirect($return);
		} else { show_404(); }
	}

	public function setAmigo53543534(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 82,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome_remetente' => $this->input->post('form_nome'),
			'email_remetente' => strtolower($this->input->post('form_email')),
			'nome_destinatario' => $this->input->post('form_nome_amigo'),
			'email_destinatario' => strtolower($this->input->post('form_email_amigo')),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario')))
		);

		if(!empty($data['email_remetente']) && !empty($data['email_destinatario'])){
			$this->load->model('contato_model','model');
			$interesse = $this->model->setIndique($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));


			
			if(strtolower($data['nome']) === 'teste123'){
				$this->email->to('atendimento@divex.com.br');
			} else {
				$this->email->to($data['email_destinatario']);
			}
			$this->email->bcc('atendimento@divex.com.br');
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->subject('Indique :: NEX GROUP');

			$data['empreendimento'] = $empreendimento;

			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			
			$this->email->send();

			$_SESSION['sucesso'] = true;
		}
		redirect($return);
	}
}