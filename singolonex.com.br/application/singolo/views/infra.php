<section id="infra">
		<div class="container box">
			<div class="container_curve bg_infra">
				<div class="container_960">
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/01.jpg" title="FACHADA">
                    	<img class="img1" src="<?=base_url(); ?>assets/img/infra/small_01.png" width="271" height="283" />
                    </a>
					<div class="text">
						<h2>Tranquilidade.<br />
                            conveniência.<br />
                            gastronomia.<br />
                            diversão.<br />
                            liberdade.<br />
                            <span class="bolds">viver tudo isso<br />
                            é muito zona sul.</span>
                        </h2>
					</div>
					<h1>INFRAESTRUTURA</h1>


					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/02.jpg" title="Entrada de veículos"><div class="images"><div class="img a"></div><h3>entrada de veículos</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/03.jpg" title="piscina adulto e infantil"><div class="images"><div class="img b"></div><h3>piscina adulto e infantil</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/04.jpg" title="salão de festas"><div class="images"><div class="img c"></div><h3>salão de festas</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/05.jpg" title="Playground Coberto"><div class="images"><div class="img d"></div><h3> Playground Coberto</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/06.jpg" title="Estar fogo"><div class="images"><div class="img e"></div><h3>Estar fogo</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/07.jpg" title="Espaço Fitness"><div class="images marginleft"><div class="img f"></div><h3>Espaço Fitness</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/08.jpg" title="Playground e Quadra Poliesportiva"><div class="images"><div class="img g"></div><h3>Playground e Quadra Poliesportiva</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/09.jpg" title="Espaço gourmet"><div class="images"><div class="img h"></div><h3>Espaço gourmet</h3></div></a>
					<a class="modal"  href="<?=base_url(); ?>assets/img/infra/zoom/10.jpg" title="Brinquedoteca"><div class="images"><div class="img i"></div><h3>Brinquedoteca</h3></div></a>
					
					<section id="decorado">
						<h1>DECORADO</h1>

						<div id="carouselContainer">
						   <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
							  <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
							<!-- Carousel
							================================================== -->
						   <div id="myCarousel" class="carousel slide">
							  <div class="carousel-inner">
							<div class="item active">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide0.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">LIVING</p>
									</div>
								  </div>
								</div>

							<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide1.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">SALA DE JANTAR</p>
									</div>
								  </div>
								</div>

						
								<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide2.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">SALA DE ESTAR</p>
									</div>
								  </div>
								</div>
						
										<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide3.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">QUARTO MENINO</p>
									</div>
								  </div>
								</div>
						
										<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide4.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">QUARTO MENINA</p>
									</div>
								  </div>
								</div>
						
										<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide5.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">QUARTO CASAL</p>
									</div>
								  </div>
								</div>
						
										<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide6.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">HOME OFFICE</p>
									</div>
								  </div>
								</div>
										<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide7.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">BANHEIRO</p>
									</div>
								  </div>
								</div>
						
												<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide8.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">COZINHA</p>
									</div>
								  </div>
								</div>
						
												<div class="item">
								  <img src="<?=base_url(); ?>assets/img/apartamentos/slide9.jpg" alt="">
								  <div class="container">
									<div class="carousel-caption">
									  <p class="lead">CHURRASQUEIRA</p>
									</div>
								  </div>
								</div>
						
							  </div>
							</div><!-- /.carousel -->
							</div><!-- /.container -->


					</section>
					
					
				</div>
				
			</div>
		</div>	
	</section>