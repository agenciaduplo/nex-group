	<section id="plantas">
		<div class="container box">
			<div class="container_curve bg_plantas">
				<div class="container_960">
					<h1>PLANTAS</h1>

					<article id="boxTexture">
				   <article class="wrap">
					<section class="tipos-lista">
					  <nav style="display:inline-block;">
						<ul>
						  <li class="openTab" id="link2dorms">2 DORMS</li>
						  <li id="link3dorms">3 DORMS</li>
						  <li id="linkGarden">GARDEN</li>
						  <li id="linkImplant">IMPLANTAÇÃO</li>
						</ul>
					  </nav>
					</section>
					<section class="tipos-content">
					  <section class="display" id="show2dorms">
						<figure class="displayPicture">
                            <a class="modal_plantas"  href="<?=base_url(); ?>assets/img/plantas/zoom/2dorms.jpg" title="2 Dormitórios | Torre 1, Final Tipo 01 | Área privativa 69,64m²">
                                  <img src="<?=base_url(); ?>assets/img/plantas/2dorms.png" alt="2 Dormitórios | Torre 1, Final Tipo 01 | Área privativa 69,64m²"/>
                            </a>  
						</figure>
						<section class="displayText">
						  <p>Torre 1, Final Tipo 01<br />Área privativa 69,64m²</p>
						</section>
					  </section>
					  <section class="display" id="show3dorms">
						<figure class="displayPicture">
							<a class="modal_plantas"  href="<?=base_url(); ?>assets/img/plantas/zoom/3dorms.jpg" title="3 Dormitórios | Torre 1, Final Tipo 06 |  Área 91,57m²">
                                <img class="right" src="<?=base_url(); ?>assets/img/plantas/3dorms.png" alt="3 Dormitórios | Torre 1, Final Tipo 06 |  Área 91,57m²"/>
                            </a>
						</figure>
						<section class="displayText">
						  <p>Torre 1, Final Tipo 06<br /> Área 91,57m²</p>
						</section>	  </section>
					  <section class="display" id="showGarden">
						<figure class="displayPicture">
                            <a class="modal_plantas"  href="<?=base_url(); ?>assets/img/plantas/zoom/garden.jpg" title="3 dormitórios | Torre 2, Final Tipo 03 | Área 115,65m²">
								<img class="right" src="<?=base_url(); ?>assets/img/plantas/garden.png" alt="3 dormitórios | Torre 2, Final Tipo 03 | Área 115,65m²"/>
                            </a>
						</figure>
						<section class="displayText">
						  <p>3 dormitórios<br />Torre 2, Final Tipo 03<br />Área 115,65m²</p>
						</section>	  </section>
					  <section class="display" id="showImplant">
						<figure class="displayPicture">
                            <a class="modal_plantas"  href="<?=base_url(); ?>assets/img/plantas/zoom/implant.jpg" title="Implantação">
                                <img src="<?=base_url(); ?>assets/img/plantas/implant.png" alt="Implantação"/>
                            </a>
						</figure>
						<section class="displayText">
							<p class="p_impla">01 - Portaria e entrada de veículos<br>
								02 - Entrada Torre 1<br>
								03 - Estar Fogo<br>
								04 - Piscina adulto<br>
								05 - Piscina infantil<br>
								06 - Playground<br>
								07 - Quadra poliesportiva<br>
								08 - Entrada Torre 2<br>
								09 - Espaço Fitness<br>
								10 - Brinquedoteca<br>
								11 - Playground coberto<br>
								12 - Pergolado <br>
								13 - Salão de Festas<br>
								14 - Espaço Gourmet<br>
							  </p>
						</section>	</section>
					</section>
						 <!-- </section>-->
				   </article> 
				  </article><!-- embalagens -->


				</div>
			
			</div>
		</div>	
	</section>