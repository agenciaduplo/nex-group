	<section id="localiza">
		<div class="container box">
			<div class="container_curve bg_localiza">
				<div class="container_960">
					<h1>localizaçao</h1>
					<h2 class="margin10">Localização privilegiada</h2>
					<p class="margin10 special">4min. do <span>Zaffari da Otto</span> • 3min. do <span>Paseo Zona Sul</span> • Próximo ao <span>pórtico</span> e à <span>praça da Tristeza</span>.</p>
                    
					<img src="<?=base_url(); ?>assets/img/aerea.jpg" width="939" height="480"> 
					<img class="margin110" src="<?=base_url(); ?>assets/img/mapa.jpg" width="719" height="380"> 
					<h2 class="center">Visite plantão de vendas e apartamento decorado:</h2>
					<p class="center bottom">Av. Wenceslau Escobar, 2832</p>
					<div class="line"></div>
					<h2 class="center top">Empreendimento:</h2>
					<p class="center bottom">R. Pereira Neto, 10</p>
				</div>
			</div>
		</div>	
	</section>