	function enviarContato ()
	{
		$("#formContato").submit();
	}
	
	jQuery(document).ready(function(){
		jQuery("#formContato").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status == true)
				{
					var nome			= $("#nome").val();
					var telefone		= $("#telefone").val();
					var email			= $("#email").val();
					var comentarios		= $("#comentarios").val();
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&telefone='+ telefone
								  +'&email='+ email
								  +'&comentarios='+ comentarios;
								  
					base_url  	= "http://www.singolonex.com.br/home/enviarInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								//$("#enviarInteresse").html(msg);
								limpaCampos('#formContato');
								$(".msg-sucesso").fadeIn();
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			},
			promptPosition : "topLeft"
			  
		})
	});


	function limpaCampos (form)
	{
	    $(form).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'select':
	            case 'email':
	            	$(this).val('');
	            case 'tel':
	            	$(this).val('');
	            case 'text':
	            	$(this).val('');
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                $('#ipt-contact-phone').checked = true;
	                $('#ipt-cliente-nao').checked = true;
	        }
	    });
	}