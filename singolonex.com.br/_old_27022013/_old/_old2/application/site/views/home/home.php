<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Singolo</title>

	<link rel="shortcut icon" href="<?=base_url()?>assets/img/fav-ico.ico" type="image/x-icon" />  
	
		<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?=base_url()?>assets/css/supersized.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?=base_url()?>assets/theme/supersized.shutter.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?=base_url()?>assets/validation/validationEngine.jquery.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.easing.min.js"></script>
		
		<script type="text/javascript" src="<?=base_url()?>assets/js/supersized.3.2.7.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/theme/supersized.shutter.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/validation/jquery.validationEngine-pt.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/validation/jquery.validationEngine.js"></script>
		
		<script type="text/javascript">
			
			$(function(){
				
				$.supersized({
				
					// Functionality
					slide_interval          :   5000,		// Length between transitions
					transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	1400,		// Speed of transition
															   
					// Components							
					slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
					slides 					:  	[			// Slideshow Images
														{image : '<?=base_url()?>assets/img/bg.jpg'}
												]	
				});

				$("#formContato").validationEngine('attach',{
					onValidationComplete: function(form, status){
						if(status == true){
							$("#formContato").submit();
							return false;
						} else{
							return false;
						}
					},
					promptPosition : "topLeft",
					scroll: false
				})
		    });
		    
		</script>
		<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-56']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
    
<!-- Fancybox -->
		<script type="text/javascript" src="<?=base_url()?>assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript">
            $(document).ready(function() {
				$("a#modal").fancybox({
					'padding'			: 0,
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'autoScale'			: true,
					'overlayColor'		: '#FFF',
					'overlayOpacity'	: 0.8,
					'autoDimensions'	: true,
					'scrolling'			:'no'
				});
            });
        </script>  
     
</head>
<body>
	<?php
	if($sucesso){
		echo '<div id="sucesso"></div>';
	}
	?>
	<a id="modal" href="<?=base_url()?>assets/img/pin-mapa.jpg"><div class="pin"></div></a>
	<div id="left-top"></div>
	<div id="left-bg">
		<div class="form">
            <div class="tit"></div>
            <form action="<?=current_url(); ?>" method="post" id="formContato">
                <ul>
                    <li><label>*Nome:</label><input class="campo nome validate[required]" name="nome" id="nome" type="text" value="" /></li>
                    <li><label>*E-mail:</label><input class="campo email validate[required,custom[email]]" id="email" name="email" type="text" value="" /></li>
                    <li><label>Telefone:</label><input class="campo fone" name="telefone" type="text" value="" /></li>
                    <li><label>Comentário:</label><textarea name="comentarios" cols="" rows=""></textarea></li>
                    <li><input class="btn" type="submit" value=""/></li>
                    <p>*Campos obrigatórios.</p>
                </ul>
            </form>
        </div>
    
    </div>
	<div id="left-bottom"></div>

</body>

</html>