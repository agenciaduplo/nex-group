<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		$sucesso = false;
		if($this->input->post('email')){
			$this->load->helper('email');
			
			$id_empreendimento		= 82;
			$id_estado				= 21;
			$ip 					= $this->input->ip_address();
			$user_agent				= $this->input->user_agent();
			$url					= $_SESSION['url'];
			$origem					= $_SESSION['origem'];
			$data_envio				= date("Y-m-d H:i:s");
			$nome 					= $this->input->post('nome');
			$email 					= $this->input->post('email');
			$telefone 				= $this->input->post('telefone');
			$comentarios 			= $this->input->post('comentarios');
			$hotsite				= "S";
			if(valid_email($email)){
				$data = array (
					'id_empreendimento'		=> $id_empreendimento,
					'id_estado'				=> $id_estado,
					'ip'					=> $ip,
					'user_agent'			=> $user_agent,
					'url'					=> $url,
					'origem'				=> $origem,
					'data_envio'			=> $data_envio,
					'nome'					=> $nome,
					'email'					=> $email,
					'telefone'				=> $telefone,
					'comentarios'			=> $comentarios,
					'hotsite'				=> $hotsite
				);
				$interesse 			= $this->model->setInteresse($data);
				if($interesse){
					$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
					
					//inicia o envio do e-mail
					//====================================================================
					
					$this->load->library('email');
					
					$config['protocol'] = 'sendmail';
					$config['mailtype'] = 'html';
					$config['charset'] 	= 'utf-8';
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);
					
					ob_start();
					
					?>
						<html>
							<head>
								<title>Nex Group</title>
							</head>
							<body>
								<table>
									<tr align="center">
							 			<td align="center" colspan="2">
							 				<a target="_blank" href="http://www.nexgroup.com.br">
							 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
							 				</a>
							 			</td>
									 </tr>
									<tr>
										<td colspan='2'>Interesse enviado em <?php echo $data_envio; ?> via HotSite</td>
									</tr>
									<tr>
										<td colspan='2'>&nbsp;</td>
									</tr>
									
									<tr>
					    				<td colspan="2">Interesse enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
						  			</tr>
									  <tr>
									    <td width="30%">&nbsp;</td>
									    <td width="70%">&nbsp;</td>
									  </tr>
									   <tr>
									    <td width="30%">Empreendimento:</td>
									    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
									  </tr>
									  <tr>
									    <td width="30%">IP:</td>
									    <td><strong><?=@$ip?></strong></td>
									  </tr>				  
									  <tr>
									    <td width="30%">E-mail:</td>
									    <td><strong><?=@$email?></strong></td>
									  </tr>
									  <tr>
									    <td width="30%">Telefone:</td>
									    <td><strong><?=@$telefone?></strong></td>
									  </tr>
									  <tr>
									    <td width="30%" valign="top">Comentários:</td>
									    <td valign="top"><strong><?=@$comentarios?></strong></td>
									  </tr>						  					  					  
									  <tr>
									    <td>&nbsp;</td>
									    <td>&nbsp;</td>
									  </tr>
									  <tr>
								 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
									  </tr>
								</table>
							</body>
						</html>
					<?php
					
					$conteudo = ob_get_contents();
					ob_end_clean();
					//Fim da Mensagem
					
					//====================================================================
					//termina o envio do e-mail
					
					$id_tipo_form 	= 1;
					$emails 		= $this->model->getEnviaEmail($id_empreendimento, $id_tipo_form);
					
					foreach ($emails as $email)
					{
						if($email->tipo === 'para')
						{
							$list[] = $email->email;
						}
						else if($email->tipo === 'cc')
						{
							$list_cc[] = $email->email;
						}
						else
						{
							$list_bcc[] = $email->email;
						}
					}
					
					$list_bcc[] = 'atendimento@divex.com.br';	
					$this->email->from("noreply@vergeis.com.br", "Nex Group");
					 
					if($nome=='teste123')
					{
						$this->email->to('bruno.freiberger@divex.com.br');
					}
					else
					{
					 	if(@$list)
					 	{
							$this->email->to($list);
						}
						if(@$list_cc)
						{
							$this->email->cc($list_cc);
						}
						if(@$list_bcc)
						{
							$this->email->bcc($list_bcc);
						}	
					}
					
					$this->email->bcc('atendimento@divex.com.br');
					$this->email->subject('Interesse enviado via HotSite');
					$this->email->message("$conteudo");
					
					$this->email->send();
					$sucesso = true;
				} else {
					die("Erro SQL");
				}
			}
		}
		$this->load->view('home/home',array(
			'sucesso' => $sucesso
		));
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */