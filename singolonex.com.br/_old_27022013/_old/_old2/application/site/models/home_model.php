<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {	

    function __construct()
    {
        parent::__construct();
    }
	
	function setInteresse($data)
	{
		$this->db->flush_cache();	
		$this->db->insert('interesses', $data);
		return $this->db->insert_id();
	}
	
	function getEnviaEmail($id_empreendimento, $id_tipo_form)
	{
		$this->db->select('*')->from('emails');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_tipo_form',1);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	function getEmpreendimento($id_empreendimento)
	{
		$this->db->select('*')->from('empreendimentos');
		$this->db->join('cidades', 'cidades.id_cidade = empreendimentos.id_cidade');
		$this->db->where('id_empreendimento',$id_empreendimento);
	
		$query = $this->db->get();
		return $query->row();
	}
}

/* End of file home_model.php */
/* Location: ./system/application/model/home_model.php */