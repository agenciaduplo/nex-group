<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Singolo</title>

	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-56']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>
<body>
        <div id="bg">
        	<iframe width="100%" height="100%" frameborder="0" scrolling="yes" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Dr.+Pereira+Neto,+10+-+Tristeza&amp;aq=&amp;sll=-15.707663,-53.173828&amp;sspn=60.132353,81.914063&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Dr.+Pereira+Neto,+10+-+Tristeza,+Porto+Alegre+-+Rio+Grande+do+Sul,+91920-530&amp;t=p&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Dr.+Pereira+Neto,+10+-+Tristeza&amp;aq=&amp;sll=-15.707663,-53.173828&amp;sspn=60.132353,81.914063&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Dr.+Pereira+Neto,+10+-+Tristeza,+Porto+Alegre+-+Rio+Grande+do+Sul,+91920-530&amp;t=p&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Exibir mapa ampliado</a></small>
        </div>        
        <div id="container"></div>
        	
        
		<div id="footer">
        	<p>Rua Dr. Pereira Neto, 10 - Tristeza</p>
       		<a class="f-mapa" href="<?=site_url()?>">Voltar para Página Inicial</a>
        </div>
        <a target="_blank" href="http://divex.com.br/">
			<div id="divex"></div>
        </a>        
</body>
</html>