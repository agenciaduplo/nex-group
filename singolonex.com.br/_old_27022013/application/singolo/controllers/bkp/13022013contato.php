<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
	
	public function sendInteresse($sid = 0){
		if($sid == session_id()){
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' => 82,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date("Y-m-d H:i:s"),
				'nome' => $this->input->post('form_nome'),
				'email' => strtolower($this->input->post('form_email')),
				'telefone' => $this->input->post('form_telefone'),
				'comentarios' => nl2br(strip_tags($this->input->post('form_comentario'))),
				'hotsite' => 'S'
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				/* antigo
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));
				$mails = array('to' => array(),'cc' => array(),'bcc' => array());
				if(strtolower($data['nome']) == 'teste123'){
					$this->email->to(array('henrique.rieger@divex.com.br','atendimento@divex.com.br'));
				} else {
					$mails = array();
					foreach($this->model->getEnviaEmail(82,1) as $e){
						switch($email->tipo){
							case 'para': array_push($mails['to'],$e->email); break;
							case 'cc': array_push($mails['cc'],$e->email); break;
							default: array_push($mails['bcc'],$e->email); break;
						}
					}
					array_push($mails['bcc'],'atendimento@divex.com.br');
					$this->email->to($mails['to']);
					$this->email->cc($mails['cc']);
					$this->email->bcc($mails['bcc']);

				}
				*/
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));
			
				$next = false; 
				foreach($this->model->getEmailGrupos() as $grupo){
					if($grupo->rand == '1'){
						$emails = array();
						foreach($this->model->getEmailGruposEmails($grupo->id) as $email){
							array_push($emails,$email->email);
						}
						$this->model->grupo_interesse($interesse,$grupo->id);
						$next = true;
						if($grupo->id == 4){
							$this->model->setNextEmailGroup(1);
							break;
						}
					} elseif($next){
						if($grupo->id == 5){
							$id = 1;
						} else {
							$id = $grupo->id;
						}
						$this->model->setNextEmailGroup($id);
						$next = false;
					}
				}
				if($p['nome'] == "Teste123"){
					$this->email->to('atendimento@divex.com.br');
				} else {
					$this->email->to($emails);
				}
				$this->email->bcc('atendimento@divex.com.br');
				
				$this->email->from("noreply@singolonex.com.br", "Nex Group");
				$this->email->subject('Interesse enviado via HotSite');

				$data['empreendimento'] = $empreendimento;

				$this->email->message($this->load->view('tpl/email-interesse',$data,true));
				
				$this->email->send();

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
			}
			redirect($return);
		} else { show_404(); }
	}

	public function setAmigo53543534(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 82,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome_remetente' => $this->input->post('form_nome'),
			'email_remetente' => strtolower($this->input->post('form_email')),
			'nome_destinatario' => $this->input->post('form_nome_amigo'),
			'email_destinatario' => strtolower($this->input->post('form_email_amigo')),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario')))
		);

		if(!empty($data['email_remetente']) && !empty($data['email_destinatario'])){
			$this->load->model('contato_model','model');
			$interesse = $this->model->setIndique($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));


			
			if(strtolower($data['nome']) === 'teste123'){
				$this->email->to('atendimento@divex.com.br');
			} else {
				$this->email->to($data['email_destinatario']);
			}
			$this->email->bcc('atendimento@divex.com.br');
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->subject('Indique :: NEX GROUP');

			$data['empreendimento'] = $empreendimento;

			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			
			$this->email->send();

			$_SESSION['sucesso'] = true;
		}
		redirect($return);
	}
}