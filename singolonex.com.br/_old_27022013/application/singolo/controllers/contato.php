<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
	
	public function sendInteresse($sid = 0){
		if($sid == session_id()){
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' => 82,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date("Y-m-d H:i:s"),
				'nome' => $this->input->post('form_nome'),
				'email' => strtolower($this->input->post('form_email')),
				'telefone' => $this->input->post('form_telefone'),
				'comentarios' => nl2br(strip_tags($this->input->post('form_comentario'))),
				'hotsite' => 'S'
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				$dados = array(
					'Enviado em ' => $data['data_envio'] . ' via HotSite',
					'Enviado por ' => $data['nome'],
					'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					'IP' => $data['ip'],
					'E-mail' => $data['email'],
					'Telefone' => $data['telefone'],
					('Comentários') => $data['comentarios']
				);
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));

				/* envia email via roleta */
				$id_empreendimento = 82;
				$grupos = $this->model->getGrupos($id_empreendimento);

				$total = 0;
				$total = count($grupos);
				
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;

						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');

							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);

							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
				/* envia email via roleta */

				if($p['nome'] == "Teste123"){
					$this->email->to('testes@divex.com.br');
				} else {
					$this->email->to($list);
				}
				$this->email->bcc('testes@divex.com.br');
				$this->email->from("noreply@singolonex.com.br", "SÍNGOLO - Nex Group");
				$this->email->subject('Contato enviado via HotSite');

				$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
				

				$this->email->send();

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
			}
			redirect($return);
		} else { show_404(); }
	}

	public function setAmigo53543534(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 82,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome_remetente' => $this->input->post('form_nome'),
			'email_remetente' => strtolower($this->input->post('form_email')),
			'nome_destinatario' => $this->input->post('form_nome_amigo'),
			'email_destinatario' => strtolower($this->input->post('form_email_amigo')),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario')))
		);

		if(!empty($data['email_remetente']) && !empty($data['email_destinatario'])){
			$this->load->model('contato_model','model');
			$interesse = $this->model->setIndique($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));


			
			if(strtolower($data['nome']) === 'teste123'){
				$this->email->to('atendimento@divex.com.br');
			} else {
				$this->email->to($data['email_destinatario']);
			}
			$this->email->bcc('atendimento@divex.com.br');
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->subject('Indique :: NEX GROUP');

			$data['empreendimento'] = $empreendimento;

			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			
			$this->email->send();

			$_SESSION['sucesso'] = true;
		}
		redirect($return);
	}
}