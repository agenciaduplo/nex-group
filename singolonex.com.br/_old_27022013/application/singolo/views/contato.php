	<section id="contato">
		<div class="container box">
			<div class="container_curve bg_contato">
				<div class="container_960">
					<h1>Contato</h1>
					<div class="formulario">
						<p>* Campos Obrigatórios</p>
						<form action="<?=site_url(); ?>contato/sendInteresse/<?=session_id(); ?>" method="post" id="form_main">
							<ul>
								<li><label>*Nome:</label><input class="campo nome validate[required]" name="form_nome" id="form_nome" type="text" value=""></li>
								<li><label>*E-mail:</label><input class="campo email validate[required,custom[email]]" name="form_email" id="form_email" type="text" value=""></li>
								<li><label>Telefone:</label><input class="campo telefone" name="form_telefone" id="form_telefone" type="text" value=""></li>
								<li class="textareali"><label>Mensagem:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
								<input class="btn-enviar" name="enviar" type="submit" value="ENVIAR" id="btn-submit">
							</ul>
						</form>
						<h2>ou ligue agora:<br><span>51 3311.2561</span></h2>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(function(){
				<?php
				if(isset($_SESSION['sucesso']) && $_SESSION['sucesso']){
					unset($_SESSION['sucesso']);
					echo 'sucessoForm();';
				}
				?>
			});
		</script>
	</section>