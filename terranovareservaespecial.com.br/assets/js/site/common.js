

	// Funções diversas que são utilizadas em todo o site


	$(function() {

		// Função que atribui o target="_blank" aos links que tenham o atributo rel="external"
		// O XHTML 1.0 Strict não suporta mais o atributo target, dessa forma forçamos o link a
		// abrir em uma nova janela/aba sem invalidar o código
		$("a[rel=external]").attr('target', '_blank');

		// Setar o primeiro campo de formulário com o foco
		//$("input[type=text]:first").focus();

		// Exibe o formulário suspenso da funcionalidade "Tenho interesse"
		$(".BotaoInteresseTop").click(function(e) {
			e.preventDefault();
			if ($(".BotaoIndique").hasClass("BotaoIndiqueSel")) {
				$(".BotaoIndique").removeClass("BotaoIndiqueSel");
				$("#ContainerFormIndique").hide();
			}
			if ($(".BotaoInteresseTop").hasClass("BotaoInteresseTopSel")) {
				$(".BotaoInteresseTop").removeClass("BotaoInteresseTopSel");
				$("#ContainerFormInteresse").hide();
			}
			else {
				$("#ContainerFormInteresse").show();
				$(".BotaoInteresseTop").addClass("BotaoInteresseTopSel");
			}
		});
		$("#ContainerFormInteresse").mouseup(function() {return false});
		$(".BotaoInteresseTop").mouseup(function() {return false});
		$(document).mouseup(function(e) {
			if ($(e.target).parent(".BotaoInteresseTop").length == 0) {
				$(".BotaoInteresseTop").removeClass("BotaoInteresseTopSel");
				$("#ContainerFormInteresse").hide();
			}
		});

		// Exibe o formulário suspenso da funcionalidade "Indique para um amigo"
		$(".BotaoIndique").click(function(e) {
			e.preventDefault();
			if ($(".BotaoInteresseTop").hasClass("BotaoInteresseTopSel")) {
				$(".BotaoInteresseTop").removeClass("BotaoInteresseTopSel");
				$("#ContainerFormInteresse").hide();
			}
			if ($(".BotaoIndique").hasClass("BotaoIndiqueSel")) {
				$(".BotaoIndique").removeClass("BotaoIndiqueSel");
				$("#ContainerFormIndique").hide();
			}
			else {
				$("#ContainerFormIndique").show();
				$(".BotaoIndique").addClass("BotaoIndiqueSel");
			}
		});
		$("#ContainerFormIndique").mouseup(function() {return false});
		$(".BotaoIndique").mouseup(function() {return false});
		$(document).mouseup(function(e) {
			if ($(e.target).parent(".BotaoIndique").length == 0) {
				$(".BotaoIndique").removeClass("BotaoIndiqueSel");
				$("#ContainerFormIndique").hide();
			}
		});
		$("#Cep").mask("99999-999");		$("#TelefoneContato").mask("(99) 9999-9999");		$("#Telefone").mask("(99) 9999-9999");



	});