<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
	<title>Terra Nova Reserva Especial</title>
	
	<meta name="description" content="Viver bem é a sua próxima conquista. Casas 2 e 3 dormitórios em condomínio fechado." />
	<meta name="keywords" content="Terra Nova Reserva Especial, casas, dormitórios, 2, 3, dormitorios, condomínio, condominio, fechado, capa, capamax, viver bem, viver, conquista, minha casa minha vida, casa, vida, minha casa, minha vida" />
	<meta name="author" content="Divex - www.divex.com.br" />
	<meta name="copyright" content="&copy; 2011 Capa Max" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta name="google-site-verification" content="Nh4-IMt6zjn47kiu62xXRX2zNCc9XjZVpKEmL7mGR5U" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/navigation.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/pages.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />
	
	<?php if(@$page == "casas"): ?>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/galleriffic.css" type="text/css" media="screen" />	
	<?php endif; ?>
	
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-46']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>

<body class="<?=@$bodyClass?>">

	<div id="Header" class="Header">
		<div class="Container">
			<div class="Left">
				<a href="javascript: AbrirCorretorOnline();" class="BotaoCorretorOnline Imgr" title="Corretor online">Corretor online</a>
				<span class="Telefone Imgr">Fone: 51 3435 4673</span>
			</div>
			<h1 class="Logo Imgr">Terra Nova Reserva Especial</h1>
			<div class="Right">
				<a href="#" class="BotaoInteresseTop Imgr" title="Tenho interesse">Tenho interesse</a>
				<a href="#" class="BotaoIndique Imgr" title="Indique para um amigo">Indique para um amigo</a>
			</div>
			<div id="ContainerFormInteresse" class="FloatForm">
				<form id="FormInteresse" class="Form" method="post" onsubmit="return validaInteresse();">
					<?php 
								
						@$utm_source 	= @$_GET['utm_source'];
						@$utm_medium 	= @$_GET['utm_medium'];
						@$utm_content 	= @$_GET['utm_content'];
						@$utm_campaign 	= @$_GET['utm_campaign'];
						
						@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
						
						
						@session_cache_expire(1440);
						@session_start();
						
						
						if(!@$_SESSION['origem'])
						{
							@$_SESSION['origem']	= @$_SERVER['HTTP_REFERER'];
						}
						if(!@$_SESSION['url'])
						{
							@$_SESSION['url']		= @$url;
						}
						
						//echo "<pre>";
						//print_r($_SESSION);
						//echo "</pre>";
						
					?>
					<input type="hidden" name="origem" id="txtOrigem" value="<?=@$_SESSION['origem']?>" />
					<input type="hidden" name="url"	id="txtUrl" value="<?=@$_SESSION['url']?>" />
					<ul class="First">
						<li>
							<label for="Nome">nome</label>
							<input type="text" id="Nome" class="CampoPadrao validate[required]" name="Nome" />
						</li>
						<li>
							<label for="FaixaEtaria" class="LabelFaixaEtaria">faixa etária</label>
							<select id="FaixaEtaria" class="CampoPadrao" name="FaixaEtaria">
								<option value="Até 30 anos">At&eacute; 30 anos</option>
								<option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                <option value="Mais de 50 anos">Mais de 50 anos</option>
							</select>
						</li>
						<li>
							<label for="EstadoCivil" class="LabelEstadoCivil">estado civil</label>
							<select id="EstadoCivil" class="CampoPadrao" name="EstadoCivil">
								<option value="Casado">Casado</option>
                                <option value="Solteiro">Solteiro</option>
							</select>
						</li>
						<li>
							<label for="Bairro" class="LabelBairro">cidade</label>
							<input type="text" id="Cidade" class="CampoPadrao" name="Cidade" />
						</li>
						<li>
							<label for="Profissao">profissão</label>
							<input type="text" id="Profissao" class="CampoPadrao" name="Profissao" />
						</li>
					</ul>
					<ul class="Left Second">
						<li class="PL24">
							<label for="Telefone">telefone</label>
							<input type="text" id="Telefone" class="CampoPadrao validate[required]" name="Telefone" />
						</li>
						<li class="PL24">
							<label for="Email">e-mail</label>
							<input type="text" id="Email" class="CampoPadrao validate[required,custom[email]]" name="Email" />
						</li>
						<li>
							<div class="CheckGroup">
								<span class="LabelOptions">formas de contato</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkTelefone" name="ChkTelefone" class="Check" value="Telefone" />
									<label for="ChkTelefone" class="Choice">telefone</label>
								</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkEmail" name="ChkEmail" class="Check" value="E-mail" />
									<label for="ChkEmail" class="Choice">e-mail</label>
								</span>
							</div> <!-- .CheckGroup -->
						</li>
						<li class="PL24 Clear">
							<label for="Comentario">comentário</label>
							<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
						</li>
					</ul>
					<div class="SubmitBar">
						<span id="MsgSucesso" class="MensagemSucesso"></span>
						<span style="display:none;" id="MensagemErro" class="MensagemErro"><strong>Nome</strong>, <strong>e-mail</strong> e <strong>telefone</strong> são obrigatórios.</span>
						<input type="submit" id="Enviar" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
				<div class="BottomContainer"></div>
			</div> <!-- #ContainerFormInteresse -->
			<div id="ContainerFormIndique" class="FloatForm">
				<form id="FormIndique" class="Form" method="post" onsubmit="return validaIndique();">
					<ul class="First">
						<li>
							<label for="NomeRemetente">nome</label>
							<input type="text" id="NomeRemetente" class="CampoPadrao" name="NomeRemetente" />
						</li>
						<li>
							<label for="EmailRemetente">e-mail</label>
							<input type="text" id="EmailRemetente" class="CampoPadrao" name="EmailRemetente" />
						</li>
					</ul>
					<ul class="Left Second">
						<li>
							<label for="NomeAmigo">nome do amigo</label>
							<input type="text" id="NomeAmigo" class="CampoPadrao" name="NomeAmigo" />
						</li>
						<li>
							<label for="EmailAmigo">e-mail do amigo</label>
							<input type="text" id="EmailAmigo" class="CampoPadrao" name="EmailAmigo" />
						</li>
					</ul>
					<div class="ContainerComentario">
						<label for="Comentario2">comentário</label>
						<textarea id="Comentario2" class="ComentarioIndique CampoPadrao" name="Comentario2" cols="40" rows="8"></textarea>
					</div>
					<div class="SubmitBar">
						<span id="MsgSucessoIndique" class="MensagemSucesso"></span>
						<span style="display:none;" id="MensagemErroIndique" class="MensagemErroIndique">Todos os campos são obrigatórios.</span>
						<input type="submit" id="EnviarIndique" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
				<div class="BottomContainer"></div>
			</div> <!-- #ContainerFormIndique -->
		</div> <!-- .Container -->
	</div> <!-- .Header -->
	
	<div class="Main">
		<div class="Container">
			<h2 class="Slogan Imgr">Viver bem é a sua próxima conquista. Casas de 2 e 3 dormitótios em condomínio fechado.</h2>
			<p class="CondicoesEspeciais Imgr">Consulte condições especiais de lançamento.</p>
			<ul class="Navigation">
				<li class="ItemHome"><a href="<?=base_url()?>" title="Home">Home</a></li>
				<li class="ItemInfraestrutura"><a href="<?=base_url()?>infraestrutura" title="Infraestrutura">Infraestrutura</a></li>
				<li class="ItemCasas"><a href="<?=base_url()?>conheca-as-casas" title="Conheça as casas">Conheça as casas</a></li>
				<li class="ItemLocalizacao"><a href="<?=base_url()?>localizacao" title="Localização">Localização</a></li>
				<li class="ItemObras"><a href="http://www.nexgroup.com.br/imoveis/20/terra-nova-reserva-especial-alvorada-rio-grande-do-sul" rel="external" title="Acompanhe as obras">Acompanhe as obras</a></li>
				<li class="ItemContato"><a href="<?=base_url()?>contato" title="Contato">Contato</a></li>
			</ul> <!-- .Navigation -->
		</div> <!-- .Container -->