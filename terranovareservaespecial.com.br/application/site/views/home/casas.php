<?=$this->load->view('includes/header');?>

	<div class="Content">
			<div class="Container">
				<div class="Plantas">
					<h2 class="TituloPlantas Imgr">4 opções de plantas</h2>
					<div id="ThumbsNavPlantas" class="Left">
						<ul class="Thumbs SmallThumbs">
							<li><a href="<?=base_url()?>assets/img/site/casas-planta-a.jpg" class="ThumbPlanta1 thumb" title="">A</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-planta-b1.jpg" class="ThumbPlanta2 thumb" title="">B1</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-planta-b2.jpg" class="ThumbPlanta3 thumb" title="">B2</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-planta-c.jpg" class="ThumbPlanta4 thumb" title="">C</a></li>
						</ul> <!-- .Thumbs -->
					</div> <!-- #ThumbsNavPlantas -->
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="SlideshowPlantas" class="ImagePreview"></div>
					</div> <!-- .slideshow-container -->
				</div> <!-- .Plantas -->
				<div class="Fachadas">
					<h2 class="TituloFachadas Imgr">11 opções de fachadas</h2>
					<div id="ThumbsNavFachadas" class="Left">
						<ul class="Thumbs">
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-01.jpg" class="ThumbFachada1 thumb" title="">01</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-02.jpg" class="ThumbFachada2 thumb" title="">02</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-03.jpg" class="ThumbFachada3 thumb" title="">03</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-04.jpg" class="ThumbFachada4 thumb" title="">04</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-05.jpg" class="ThumbFachada5 thumb" title="">05</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-06.jpg" class="ThumbFachada6 thumb" title="">06</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-07.jpg" class="ThumbFachada7 thumb" title="">07</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-08.jpg" class="ThumbFachada8 thumb" title="">08</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-09.jpg" class="ThumbFachada9 thumb" title="">09</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-10.jpg" class="ThumbFachada10 thumb" title="">10</a></li>
							<li><a href="<?=base_url()?>assets/img/site/casas-fachada-11.jpg" class="ThumbFachada11 thumb" title="">11</a></li>
						</ul> <!-- .Thumbs -->
					</div> <!-- #ThumbsNavFachadas -->
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="SlideshowFachadas" class="ImagePreview"></div>
					</div> <!-- .slideshow-container -->
				</div> <!-- .Fachadas -->
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Rial, HabitCasa e Auxiliadora Predial</p>
					<p class="Realizacao Imgr">Realização: Terra Nova Rodobens, Rodobens egócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
			</div> <!-- .Container -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->

<?=$this->load->view('includes/footer');?>