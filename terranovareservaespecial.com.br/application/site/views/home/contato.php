<?=$this->load->view('includes/header');?>

	<div class="Content">
			<div class="Container">
				<div class="Sidebar">
					<h2 class="TituloPlantao">Plantão de vendas</h2>
					<span class="Tel">51 3435 4673</span>
					<span class="Infs">* Nosso horário de atendimento, inclusive através deste site, vai das 8:00 às 12:00 e das 13:00 às 18:20, de segunda à quinta.<br />Sexta das 8:00 às 12:00 e das 13:00 às 18:00.</span>
				</div> <!-- .Sidebar -->
				<form id="FormContato" class="Form" method="post" onsubmit="return validaContato();">
					<h2 class="TituloContato">Formulário de contato</h2>
					<ul class="First">
						<li>
							<label for="NomeContato">nome</label>
							<input type="text" id="NomeContato" class="CampoPadrao" name="NomeContato" />
						</li>
						<li>
							<label for="EmailContato">e-mail</label>
							<input type="text" id="EmailContato" class="CampoPadrao" name="EmailContato" />
						</li>
						<li>
							<label for="Endereco">endereço</label>
							<input type="text" id="Endereco" class="CampoPadrao" name="Endereco" />
						</li>
						<li>
							<label for="Uf" class="LabelUf">UF</label>
							<select id="Uf" class="CampoPadrao" name="Endereco">
								<option value="AC">Acre</option>
							    <option value="AL">Alagoas</option>
							    <option value="AM">Amazonas</option>
							    <option value="AP">Amapá</option>
							    <option value="BA">Bahia</option>
							    <option value="CE">Ceará</option>
							    <option value="DF">Distrito Federal</option>
							    <option value="ES">Espirito Santo</option>
							    <option value="GO">Goiás</option>
							    <option value="MA">Maranhão</option>
							    <option value="MG">Minas Gerais</option>
							    <option value="MS">Mato Grosso do Sul</option>
							    <option value="MT">Mato Grosso</option>
							    <option value="PA">Pará</option>
							    <option value="PB">Paraíba</option>
							    <option value="PE">Pernambuco</option>
							    <option value="PI">Piauí</option>
							    <option value="PR">Paraná</option>
							    <option value="RJ">Rio de Janeiro</option>
							    <option value="RN">Rio Grande do Norte</option>
							    <option value="RO">Rondônia</option>
							    <option value="RR">Roraima</option>
							    <option selected="selected" value="RS">Rio Grande do Sul</option>
							    <option value="SC">Santa Catarina</option>
							    <option value="SE">Sergipe</option>
							    <option value="SP">São Paulo</option>
							    <option value="TO">Tocantins</option>
							</select>
						</li>
						<li>
							<label for="Cidade" class="LabelCidade">cidade</label>
							<input type="text" id="CidadeContato" class="CampoPadrao" name="cidade" />
						</li>
					</ul>
					<ul class="Left">
						<li>
							<label for="Cep">CEP</label>
							<input type="text" id="Cep" class="CampoPadrao" name="Cep" />
						</li>
						<li>
							<label for="TelefoneContato">telefone</label>
							<input type="text" id="TelefoneContato" class="CampoPadrao" name="TelefoneContato" />
						</li>
						<li>
							<label for="Comentario3">comentário</label>
							<textarea id="Comentario3" class="CampoPadrao" name="Comentario3" cols="40" rows="8"></textarea>
						</li>
					</ul>
					<div class="SubmitBar">
						<span id="MensagemSucessoContato" class="MensagemSucesso">&nbsp;</span>
						<span style="display:none;" id="MensagemErroContato" class="MensagemErroContato"><strong>Nome</strong>, <strong>e-mail</strong> e <strong>telefone</strong> são obrigatórios.</span>
						<input type="submit" id="Enviar" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Rial, HabitCasa e Auxiliadora Predial</p>
					<p class="Realizacao Imgr">Realização: Terra Nova Rodobens, Rodobens egócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
			</div> <!-- .Container -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->

<?=$this->load->view('includes/footer');?>