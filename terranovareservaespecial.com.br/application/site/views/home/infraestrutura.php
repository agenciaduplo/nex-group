<?=$this->load->view('includes/header');?>
	
	<div class="Content">
			<div class="Container">
				<div id="ThumbsNav">
					<ul class="Thumbs">
						<li><a href="<?=base_url()?>assets/img/site/infra-casas.jpg" class="ThumbCasas thumb" title="Casas de 2 e 3 dorms. em condomínio fechado">Casas de 2 e 3 dorms. em condomínio fechado</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-plantas.jpg" class="ThumbPlantas thumb" title="4 opções de plantas">4 opções de plantas</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-fachadas.jpg" class="ThumbFachadas thumb" title="11 modelos de fachadas">11 modelos de fachadas</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-portaria-central.jpg" class="ThumbPortaria thumb" title="Portaria central">Portaria central</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-salao-de-festas.jpg" class="ThumbSalao thumb" title="Salão de festas">Salão de festas</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-piscina.jpg" class="ThumbPiscinas thumb" title="Piscinas adulto e infantil">Piscinas adulto e infantil</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-minicampo.jpg" class="ThumbFutebol thumb" title="Minicampo de futebol gramado">Minicampo de futebol gramado</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-quadra-volei.jpg" class="ThumbVolei thumb" title="Quadra de vôlei de areia">Quadra de vôlei de areia</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-quadra-poliesportiva.jpg" class="ThumbQuadra thumb" title="Quadra poliesportiva">Quadra poliesportiva</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-quiosque.jpg" class="ThumbQuiosques thumb" title="Quiosque com churrasqueiras">Quiosque com churrasqueiras</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-playground.jpg" class="ThumbPlayground thumb" title="Playground">Playground</a></li>
						<li><a href="<?=base_url()?>assets/img/site/infra-estacionamento.jpg" class="ThumbVagas thumb" title="Vagas de estacionamento para visitantes">Vagas de estacionamento para visitantes</a></li>
					</ul> <!-- .Thumbs -->
				</div> <!-- #ThumbsNav -->
				<div class="slideshow-container">
					<div id="loading" class="loader"></div>
					<div id="slideshow" class="ImagePreview PT4"></div>
				</div> <!-- .slideshow-container -->
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Rial, HabitCasa e Auxiliadora Predial</p>
					<p class="Realizacao Imgr">Realização: Terra Nova Rodobens, Rodobens egócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
			</div> <!-- .Container -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->

<?=$this->load->view('includes/footer');?>