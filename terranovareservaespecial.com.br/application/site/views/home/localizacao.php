<?=$this->load->view('includes/header');?>

	<div class="Content">
			<div class="Container">
				<div class="Sidebar">
					<p class="InfsLocalizacao">Localização<span>Estd. Caminho do Meio, 5.425 continuação av. Protásio Alves<span>Alvorada.RS / Brasil</span></span></p>
					<p class="PlantaoVendas">Visite plantão de vendas e casas decoradas</p>
					<a href="http://maps.google.com/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=Caminho+do+Meio,+5.425+Alvorada+Rio+Grande+do+Sul&aq=&sll=37.0625,-95.677068&sspn=31.977057,79.013672&ie=UTF8&hq=&hnear=Estr.+Caminho+do+Meio,+5425+-+Viam%C3%A3o+-+Rio+Grande+do+Sul,+94510-420,+Brasil&ll=-30.042833,-51.077757&spn=0.017052,0.038581&z=15&iwloc=A" class="GoogleMaps Imgr" rel="external" title="Google Maps">Google Maps</a>
				</div> <!-- .Sidebar -->
				<div class="Map">
					<img src="<?=base_url()?>assets/img/site/mapa.png" alt="" width="617" height="436" />
				</div> <!-- .Map -->
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Rial, HabitCasa e Auxiliadora Predial</p>
					<p class="Realizacao Imgr">Realização: Terra Nova Rodobens, Rodobens egócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
			</div> <!-- .Container -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->

<?=$this->load->view('includes/footer');?>