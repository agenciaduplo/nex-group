<?=$this->load->view('includes/header2');?>

		<div class="Content">
			<div class="Container">
				<ul class="Featured">
					<li>
						<h3 class="TituloPortariaCentral Imgr PngBelated">Portaria central</h3>
						<a href="<?=base_url()?>infraestrutura/portaria-central" title="Portaria central"><img src="<?=base_url()?>assets/img/site/foto-portaria-central.jpg" alt="Portaria central" width="152" height="138" /></a>
					</li>
					<li>
						<h3 class="TituloPiscinas Imgr">Piscinas adulto e infantil</h3>
						<a href="<?=base_url()?>infraestrutura/piscinas-adulto-e-infantil" title="Piscinas adulto e infantil"><img src="<?=base_url()?>assets/img/site/foto-piscinas.jpg" alt="Piscinas adulto e infantil" width="152" height="138" /></a>
					</li>
					<li>
						<h3 class="TituloSalaoFestas Imgr">Salão de festas</h3>
						<a href="<?=base_url()?>infraestrutura/salao-de-festas" title="Salão de festas"><img src="<?=base_url()?>assets/img/site/foto-salao-festas.jpg" alt="Salão de festas" width="152" height="138" /></a>
					</li>
					<li>
						<h3 class="TituloQuiosques Imgr">Quiosque com churrasqueiras</h3>
						<a href="<?=base_url()?>infraestrutura/quiosque-com-churrasqueiras" title="Quiosque com churrasqueiras"><img src="<?=base_url()?>assets/img/site/foto-quiosque-churrasqueira.jpg" alt="Quiosque com churrasqueiras" width="152" height="138" /></a>
					</li>
					<li class="Last">
						<h3 class="TituloFutebol Imgr">Minicampo de futebol gramado</h3>
						<a href="<?=base_url()?>infraestrutura/minicampo-de-futebol-gramado" title="Minicampo de futebol gramado"><img src="<?=base_url()?>assets/img/site/foto-minicampo-futebol.jpg" alt="Minicampo de futebol gramado" width="152" height="138" /></a>
					</li>
				</ul> <!-- .Featured -->
				<div class="Auxiliary">
					<a href="<?=base_url()?>conheca-as-casas" class="BulletLink First" title="Conheça sua futura casa">conheça sua futura casa</a>
					<a href="<?=base_url()?>infraestrutura" class="BulletLink" title="Veja mais opções de infraestrutura e lazer">veja mais opções de infraestrutura e lazer</a>
				</div> <!-- .Auxiliary -->
				<div class="SocialCounters">
					
					<div class="addthis_toolbox addthis_default_style">
						<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
						<a class="addthis_button_tweet"></a>
						<!-- <a class="addthis_counter addthis_pill_style"></a> -->
					</div>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4db5b7877cdc5367"></script>
					
					<!-- AddThis Button BEGIN -->
					
					<!-- <img src="<?=base_url()?>assets/img/site/ma-social-counters.gif" alt="" width="183" height="20" /> -->
				</div> <!-- .SocialCounters -->
				<div class="Conditions">
					<ul class="Imgr">
						<li>Até R$ 17 mil de benefício do governo federal*</li>
						<li>Use seu FGTS**</li>
						<li>Seguro desemprego***</li>
						<li>Baixo valor de condomínio</li>
						<li>Parcelas decrescentes</li>
						<li>Juros a partir de 4,5% a.a. (0,37% a.m.)</li>
					</ul>
				</div> <!-- .Conditions -->
				<div class="Features">
					<a href="#Header" class="BotaoInteresse FeaturesButtons back" title="Tenho interesse">Tenho interesse<span>clique e preencha nosso formulário de interesse</span></a>
					<a href="<?=base_url()?>localizacao" class="BotaoLocalizacao FeaturesButtons" title="Localização">Localização<span>Estd. Caminho do Meio, 5.425 continuação av. Protásio Alves. Visite plantão de vendas e casas decoradas</span></a>
					<a href="#" style="cursor:default;" class="BotaoPasseioVirtual FeaturesButtons" title="Passeio virtual">Passeio virtual<span>Escolha as opções de casas abaixo e faça um tour:</span></a>
					<ul>
						<li><a href="http://www.shots360.com.br/Capa/Terra_Nova/Casa_A/A_SALA.html" class="BulletLink" rel="external" title="Faça um passeio virtual pela Casa A">Casa A</a></li>
						<li><a href="http://www.shots360.com.br/Capa/Terra_Nova/Casa_B1/B_SALA.html" class="BulletLink" rel="external" title="Faça um passeio virtual pela Casa B1">Casa B1</a></li>
						<li><a href="http://www.shots360.com.br/Capa/Tour_Terra_Nova/B2_SALA.html" class="BulletLink" rel="external" title="Faça um passeio virtual pela Casa B2">Casa B2</a></li>
						<li class="Last"><a href="http://www.shots360.com.br/Capa/Terra_Nova/Casa_C/C_SALA.html" class="BulletLink" rel="external" title="Faça um passeio virtual pela Casa C">Casa C</a></li>
					</ul>
				</div> <!-- .Features -->
				<div class="LegalText">
					<p>* Consulte no plantão de vendas unidades com possibilidade de enquadramento no Programa Minha Casa, Minha Vida.</p>
					<p>** Conforme condições de liberação da Caixa Econômica Federal.</p>
					<p>*** O Fundo Garantidor do Programa Minha Casa, Minha Vida, do Governo Federal, garante 12, 24 ou 36 parcelas do financiamento em caso de desemprego, de acordo com a renda familiar. Consulte demais condições de pagamento no plantão de vendas. Incorporação Registrada no Registro de Imóveis de Alvorada-RS, sob o número 59.933. Imagens meramente ilustrativas.</p>
				</div> <!-- .LegalText -->
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Rial, HabitCasa e Auxiliadora Predial</p>
					<p class="Realizacao Imgr">Realização: Terra Nova Rodobens, Rodobens egócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
			</div> <!-- .Container -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->

<?=$this->load->view('includes/footer');?>