<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "home",
			'bodyClass'				=> "Home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function index2 ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "home",
			'bodyClass'				=> "Home"
		);
		
		$this->load->view('home/home2', $data);
	}
	
	function infraestrutura ($uri = "")
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "infraestrutura",
			'bodyClass'				=> "Infraestrutura",
			'uri'					=> $uri
		);
		
		$this->load->view('home/infraestrutura', $data);
	}
	
	function localizacao ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "localizacao",
			'bodyClass'				=> "Localizacao"
		);
		
		$this->load->view('home/localizacao', $data);
	}
	
	function casas ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "casas",
			'bodyClass'				=> "Casas"
		);
		
		$this->load->view('home/casas', $data);
	}
	
	function contato ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		$data = array (
			'page'					=> "contato",
			'bodyClass'				=> "Contato"
		);
		
		$this->load->view('home/contato', $data);
	}
	
	function enviarContato ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		if($_POST)
		{
		
			$nome 			= $this->input->post('nome');
			$email 			= $this->input->post('email');
			$endereco 		= $this->input->post('endereco');
			$cidade 		= $this->input->post('cidade');
			$uf 			= $this->input->post('uf');
			$cep 			= $this->input->post('cep');
			$telefone 		= $this->input->post('telefone');
			$comentario 	= $this->input->post('comentario');
			
			$url			= @$this->input->post('url');
			$origem			= @$this->input->post('origem');
			
			//$url			= @$_SESSION['url'];
			//$origem			= @$_SESSION['origem'];
			
			$id_empreendimento	= 4;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			$data = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'data_envio'			=> $data,
				
				'nome'					=> $nome,
				'email'					=> $email,
				'endereco'				=> $endereco,
				'cidade'				=> $cidade,
				'uf'					=> $uf,
				'cep'					=> $cep,
				'telefone'				=> $telefone,
				'comentarios'			=> $comentario,
				'origem'				=> $origem,
				'url'					=> $url
			);
			
			$this->model->setContato($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>CAPA</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_grupo_capa.gif" alt="GRUPO CAPA" /></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>TERRA NOVA RESERVA ESPECIAL</b> :: Contato via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2">Contato enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>				  
				  <tr>
				    <td width="30%">E-mail:</td>
				    <td><strong><?=$email?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Endereço:</td>
				    <td><strong><?=$endereco?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Cidade:</td>
				    <td><strong><?=$cidade?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">UF:</td>
				    <td><strong><?=$uf?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">CEP:</td>
				    <td><strong><?=$cep?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Telefone:</td>
				    <td><strong><?=$telefone?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentario?></strong></td>
				  </tr>				  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	GRUPO CAPA
				    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email", "$nome");
			
			$list = array(
							//'estela@capa.com.br',
							'saldanha@capa.com.br',
							'marca.savi@bol.com.br'
						);	
			
			$this->email->to($list);
			$this->email->bcc('bruno.freiberger@divex.com.br'); 
			$this->email->subject('TERRA NOVA RESERVA ESPECIAL - Contato enviado pelo site');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->load->view('home/ajax.contato.php');
		}
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		$this->load->helper('cookie');
		
		if($_POST)
		{
		
			$nome 				= $this->input->post('nome');
			$faixa_etaria 		= $this->input->post('faixa_etaria');
			$estado_civil 		= $this->input->post('estado_civil');
			$bairro_cidade 		= $this->input->post('bairro_cidade');
			$profissao 			= $this->input->post('profissao');
			$telefone 			= $this->input->post('telefone');
			$email 				= $this->input->post('email');
			$comentarios 		= $this->input->post('comentarios');
			$contato 			= $this->input->post('contato');
			
			$url			= @$this->input->post('url');
			$origem			= @$this->input->post('origem');
			
			//$url			= @$_SESSION['url'];
			//$origem			= @$_SESSION['origem'];
			
			$id_empreendimento	= 4;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			$data = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'data_envio'			=> $data,
				
				'nome'					=> $nome,
				'email'					=> $email,
				'faixa_etaria'			=> $faixa_etaria,
				'estado_civil'			=> $estado_civil,
				'bairro_cidade'			=> $bairro_cidade,
				'profissao'				=> $profissao,
				'telefone'				=> $telefone,
				'forma_contato'			=> $contato,
				'comentarios'			=> $comentarios,
				'origem'				=> $origem,
				'url'					=> $url
			);
			
			$this->model->setInteresse($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>CAPA</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_grupo_capa.gif" alt="GRUPO CAPA" /></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>TERRA NOVA RESERVA ESPECIAL</b> :: Contato via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2">Contato enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>				  
				  <tr>
				    <td width="30%">E-mail:</td>
				    <td><strong><?=$email?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Faixa Etária:</td>
				    <td><strong><?=$faixa_etaria?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Estado Civil:</td>
				    <td><strong><?=$estado_civil?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Bairro e Cidade:</td>
				    <td><strong><?=$bairro_cidade?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Profissão:</td>
				    <td><strong><?=$profissao?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Telefone:</td>
				    <td><strong><?=$telefone?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Forma de Contato:</td>
				    <td><strong><?=$contato?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentarios?></strong></td>
				  </tr>					  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	GRUPO CAPA
				    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email", "$nome");
			
			$list = array(
							//'estela@capa.com.br',
							//'saldanha@capa.com.br',
							//'marca.savi@bol.com.br'
							'ricardo.debem@divex.com.br'
						);	
			
			$this->email->to($list);
			$this->email->bcc('bruno.freiberger@divex.com.br'); 
			$this->email->subject('TERRA NOVA RESERVA ESPECIAL - Interesse enviado pelo site');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->load->view('home/ajax.interesse.php');
		}
	}
	
	function enviarIndique ()
	{
		$this->load->model('home_model', 'model');
		//$this->load->helper('cookie');
		
		if($_POST)
		{
		
			$nome_remetente 	= $this->input->post('nome_remetente');
			$email_remetente 	= $this->input->post('email_remetente');
			$nome_amigo 		= $this->input->post('nome_amigo');
			$email_amigo 		= $this->input->post('email_amigo');
			$comentarios 		= $this->input->post('comentarios');
			
			$url			= @$this->input->post('url');
			$origem			= @$this->input->post('origem');
			
			//$url			= @$_SESSION['url'];
			//$origem			= @$_SESSION['origem'];
			
			$id_empreendimento	= 4;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			$data = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'data_envio'			=> $data,
				
				'nome_remetente'		=> $nome_remetente,
				'email_remetente'		=> $email_remetente,
				'nome_destinatario'		=> $nome_amigo,
				'email_destinatario'	=> $email_amigo,
				'comentarios'			=> $comentarios,
				'origem'				=> $origem,
				'url'					=> $url
			);
			
			$this->model->setIndique($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>CAPA</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_grupo_capa.gif" alt="GRUPO CAPA" /></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>TERRA NOVA RESERVA ESPECIAL</b> :: Indicação via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2">Indicação enviada por <strong><?=$nome_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>			  
				  <tr>
				    <td colspan="2">Olá <?=$nome_amigo?>, acesse o site do TERRA NOVA RESERVA ESPECIAL<br/><strong><a href="http://www.terranovareservaespecial.com.br">Clique aqui</a></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">Nome do remetente:</td>
				    <td><strong><?=$nome_remetente?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentarios?></strong></td>
				  </tr>					  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	GRUPO CAPA
				    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email_remetente", "$nome_remetente");
			
			$list = array(
							"$email_amigo"
						);	
			
			$this->email->to($list);
			$this->email->bcc('bruno.freiberger@divex.com.br'); 
			$this->email->subject('TERRA NOVA RESERVA ESPECIAL - Indicação');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->load->view('home/ajax.indique.php');
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */