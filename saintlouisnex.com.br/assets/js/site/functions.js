function enviarIndique()
{
	$("#formIndique").submit();
}	

function enviarEmailContato ()
{
	$("#formContato").submit();
}

jQuery(document).ready(function(){
	
	jQuery("#formIndique").validationEngine(
		'attach', {
			promptPosition : "topLeft",
			onValidationComplete: function(form, status)
			{
				if(status == true)
				{
					$("#btnEnviarAmigo").attr("href","#");
					
					var nome			= $("#indiqueNome").val();
					var email			= $("#indiqueEmail").val();
					var nome_amigo		= $("#indiqueNomeAmigo").val();
					var email_amigo		= $("#indiqueEmailAmigo").val();
					var comentarios		= $("#comentarioAmigo").val();
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&email='+ email
								  +'&nome_amigo='+ nome_amigo
								  +'&email_amigo='+ email_amigo
								  +'&comentarios='+ comentarios;
								  
					base_url  	= "http://www.saintlouisnex.com.br/home/enviarIndique";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
							/* <!-- Google Code for Contato Conversion Page --> */
							window.google_conversion_id = 983735308;
							window.google_conversion_language = "pt"
							window.google_conversion_format = "2"
							window.google_conversion_color = "ffffff"
							window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
							window.google_conversion_value = 0;

							document.write = function(node){ jQuery("body").append(node); }
							jQuery.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });

								limpaCampos('#formIndique');
								$("#mensagem-sucesso-indique-show").fadeIn();
								$("#btnEnviarAmigo").attr("href","javascript: enviarIndique();");
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			} 
		}
	);
	
	jQuery("#formContato").validationEngine(
		'attach', {
			promptPosition : "topLeft",
			onValidationComplete: function(form, status)
			{
				if(status == true)
				{
					$("#btnEnviarInteresse").attr("href","#");
					
					var nome			= $("#nome").val();
					var faixa_etaria	= $("#faixa_etaria").val();
					var estado_civil	= $("#estado_civil").val();
					var bairro_cidade	= $("#bairro_cidade").val();
					var profissao		= $("#profissao").val();
					var telefone		= $("#telefone").val();
					var email			= $("#email").val();
					var comentarios		= $("#comentario").val();
					
					var url				= $("#txtUrl").val();
					var origem			= $("#txtOrigem").val();
					
					if ($('#forma_contato_email').is(':checked'))
					{
						var contato = "E-mail";
					}
					else if ($('#forma_contato_tel').is(':checked'))
					{
						var contato = "Telefone";
					}
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&faixa_etaria='+ faixa_etaria
								  +'&estado_civil='+ estado_civil
								  +'&bairro_cidade='+ bairro_cidade
								  +'&profissao='+ profissao
								  +'&telefone='+ telefone
								  +'&email='+ email
								  +'&contato='+ contato
								  +'&url='+ url
								  +'&origem='+ origem
								  +'&comentarios='+ comentarios;
								  
					base_url  	= "http://www.saintlouisnex.com.br/home/enviarInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								/* <!-- Google Code for Contato Conversion Page --> */
								window.google_conversion_id = 983735308;
								window.google_conversion_language = "pt"
								window.google_conversion_format = "2"
								window.google_conversion_color = "ffffff"
								window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
								window.google_conversion_value = 0;

								document.write = function(node){ jQuery("body").append(node); }
								jQuery.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });


								limpaCampos('#formContato');
								$("#mensagem-sucesso-show").fadeIn();
								$("#btnEnviarInteresse").attr("href","javascript: enviarEmailContato();");
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			} 
		}
	);
});

function limpaCampos (form)
{
    $(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'select':
            case 'email':
            	$(this).val('');
            case 'tel':
            	$(this).val('');
            case 'text':
            	$(this).val('');
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                $('#ipt-contact-phone').checked = true;
                $('#ipt-cliente-nao').checked = true;
        }
    });
}
