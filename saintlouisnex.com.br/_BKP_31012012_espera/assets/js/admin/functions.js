$(document).ready(function() {	
	
	$("#FormFone").mask("(99) 9999-9999");
	$("#FormCelCliente").mask("(99) 9999-9999");
	$("#FormTelefonee").mask("(99) 9999-9999");
	$("#FormTelefoneCasa").mask("(99) 9999-9999");
	$("#FormTelefoneResp").mask("(99) 9999-9999");
	$("#FormCelResp").mask("(99) 9999-9999");
	$("#FormFax").mask("(99) 9999-9999");
	$("#FormCpf").mask("999.999.999-99");
	$("#FormCEP").mask("99999-999");
	$("#FormHoraAgenda").mask("99:99");
	$("#FormRespTelefone").mask("(99) 9999-9999");
	$("#FormRespCelular").mask("(99) 9999-9999");
	$("#FormCelularUser").mask("(99) 9999-9999");
	$("#FormTelefoneUser").mask("(99) 9999-9999");
	$("#FormHora").mask("99:99");
	$("#FormData").mask("99/99/9999");
	$("#FormHorario").mask("99:99");
	
    $('.FlashConceitual').media( { width: 995, height: 183, caption: false, wmode: 'transparent'} );
    $('.FlashMenu').media( { width: 200, height: 420 } );
    $('.FlashTitulo').media( { width: 300, height: 36 } );
    
    
    $("#FormLogon").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormUsuario,Usuário",
        "required,FormSenha,Senha"
        ]
    });    

    $("#FormUsuariosCad").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormNome,Nome",
        "required,FormEmail,E-mail",
        "valid_email,FormEmail,E-mail Inválido",
        "required,FormLogin,Login"
        ]
    });
    
    $("#FormTags").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormPagina,Página"
        ]
    });
    
	$("#FormBlocos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormBloco,Bloco"
        ]
    });
         
    $("#FormApartamentos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormUnidade,Unidade",
        "required,FormBloco,Bloco"
        ]
    });
    
    $("#FormRealizacoes").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Titulo"
        ]
    });
    
    $("#FormOutros").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormNome,Nome",
        "required,FormEndereco,Endereço"
        ]
    });
      
    $("#FormPremios").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Titulo"
        ]
    });
    
    $("#FormCertificacoes").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Titulo"
        ]
    });      
       
    $("#FormNoticias").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Titulo",
        "required,FormData,Data"
        ]
    });
    
    $("#FormSecoes").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormNome,Nome"
        ]
    });  
    
    $("#FormConteudos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Título",
        "required,FormSecao,Seção"
        ]
    });
    
    $("#FormLancamentos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Título"
        ]
    });
    
    $("#FormVendas").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Título"
        ]
    });      
    
    $("#FormDataAgenda").datepicker({
    	yearRange: '-100:+3',
        showOn: "button",
        buttonImage: "http://www.squadraengenharia.com.br/site/assets/img/ico-calendario.gif",
        buttonImageOnly: true
    });

    $("#FormData").datepicker({
    	yearRange: '-100:+3',
        showOn: "button",
        buttonImage: "http://www.squadraengenharia.com.br/site/assets/img/ico-calendario.gif",
        buttonImageOnly: true
    });       

});

function mostraEsconde(div)
{
	d = document.getElementById(div).style;
	
	document.getElementById('mostra_conf').style.display = 'none';
	document.getElementById('mostra_fotos').style.display = 'none';
	document.getElementById('mostra_guia').style.display = 'none';
	document.getElementById('mostra_servicos').style.display = 'none';
	document.getElementById('mostra_formularios').style.display = 'none';
	//document.getElementById('MostraFotos').style.display = 'none';
	
	if (d.display == 'none') {
    	d.display = 'block';
	} else {
    	d.display = 'none';
	}
}

function mostraFotos(div)
{
	d = document.getElementById(div).style;

	if (d.display == 'none') {
    	d.display = 'block';
	} else {
    	d.display = 'none';
	}	
}


function myOnComplete() { return true; }
function abas(alvo) {
    $('.abas').hide();
    $("#abas ul li").removeClass('atual');
    $('#Alvo'+alvo).show();
    $('#Aba'+alvo).addClass("atual")
}