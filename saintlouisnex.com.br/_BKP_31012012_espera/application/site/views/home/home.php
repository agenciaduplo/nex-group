<?=$this->load->view('includes/header');?>

        <div class="Wrapper">
        	<div class="Container">
            	<div class="Main">
                	<div class="Footer">
        				<div class="Plantao">
                        	<address class="TitAddress Left PNG">Rua S&atilde;o Luíz, 1163 - Porto Alegre/RS</address>
							<a class="LnkMapa PNG Right" href="http://maps.google.com/maps?q=Rua+S%C3%A3o+Luiz,+1163+-+Porto+Alegre%2FRS&hl=pt-BR&ie=UTF8&ll=-30.052315,-51.201203&spn=0.007448,0.016512&sll=-30.05251,-51.202426&sspn=0.01445,0.033023&vpsrc=0&hnear=R.+S%C3%A3o+Lu%C3%ADs,+1163+-+Partenon,+Porto+Alegre+-+Rio+Grande+do+Sul,+90620-170,+Brasil&t=m&z=17" target="_blank">Veja a localiza&ccedil;&atilde;o no mapa</a>
                        </div><!--fecha Plantao-->
						<div class="Logotipos">
							<a class="Left LnkDivex" href="http://www.divex.com.br/imobi" title="Divex Imob"><img class="PNG" src="<?=base_url()?>assets/img/site/logo-divex-imobi-footer.gif" alt="Divex Imob" /></a>
							<a class="Right" href="http://www.nexgroup.com.br" title="NexGroup"><img class="PNG" src="<?=base_url()?>assets/img/site/logo-nexgroup.png" alt="NexGroup" /></a>
							<a class="Right" href="http://www.eglengenharia.com.br/novosite/index.php/home/principal" title="EGL Engenharia"><img class="PNG" src="<?=base_url()?>assets/img/site/logo-egl.png" alt="EGL Engenharia" /></a>
                        </div>
        			</div><!--fecha Footer-->
                </div><!--fecha Main-->
                <div class="Sidebar PNG">
                	<form id="FormInteresse" name="FormInteresse" method="POST" action="" onsubmit="" >
	                	<?php 
									
							@$utm_source 	= $_GET['utm_source'];
							@$utm_medium 	= $_GET['utm_medium'];
							@$utm_content 	= $_GET['utm_content'];
							@$utm_campaign 	= $_GET['utm_campaign'];
							
							@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
							
						?>
						<input type="hidden" name="origem" id="txtOrigem" value="<?=@$_SERVER['HTTP_REFERER']?>" />
						<input type="hidden" name="url"	id="txtUrl" value="<?=@$url?>" />
                	
                        <ul class="Formulario">

                            <li class="titlegroup">
                            	<h3>Tenho interesse</h3>
                            	<p>Preencha os campos e aguarde que logo entraremos em contato.</p>
								<p class="Exception">Campos marcados com asterisco (*) s&atilde;o obrigat&oacute;rios.</p>
								
                            </li>

                            <li>
                                <label for="txtInteresseNome">Nome*</label>
                                <input type="text" id="txtInteresseNome" class="Campo validate[required]" name="txtInteresseNome" />
                            </li>

							<li>
                                <label for="selectInteresseFaixa">Faixa et&aacute;ria*</label>
                                <select id="selectInteresseFaixa" class="Select validate[required]" name="selectInteresseFaixa">
                                	<option value="Até 30 anos">At&eacute; 30 anos</option>
                                    <option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                    <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                    <option value="Mais de 50 anos">Mais de 50 anos</option>
                                </select>
                            </li>

							<li>
                                <label for="selectInteresseCivil">Estado civil</label>
                                <select id="selectInteresseCivil" class="Select validate[required]" name="selectInteresseCivil">
                                	<option value="Casado">Casado</option>
                                    <option value="Solteiro">Solteiro</option>
                                </select>
                            </li>

                            <li>
                                <label for="txtInteresseBairroCidade">Bairro e cidade</label>
                                <input type="text" id="txtInteresseBairroCidade" class="Campo validate[required]" name="txtInteresseBairroCidade" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseProfissao">Profiss&atilde;o*</label>
                                <input type="text" id="txtInteresseProfissao" class="Campo validate[required]" name="txtInteresseProfissao" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseTelefone">Telefone*</label>
                                <input type="text" id="txtInteresseTelefone" class="Campo validate[required]" name="txtInteresseTelefone" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseEmail">E-mail*</label>
                                <input type="text" id="txtInteresseEmail" class="Campo validate[required,custom[email]]" name="txtInteresseEmail" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label>Forma de contato</label>
	                            <input type="radio" class="RadioButton" checked="checked" value="E-mail" name="radioInteresseContato" id="radioInteresseEmail"><label class="LabelRadio" for="radioInteresseEmail">E-mail</label>
    	                        <input type="radio" class="RadioButton" value="Telefone" name="radioInteresseContato" id="radioInteresseTelefone"><label class="LabelRadio" for="radioInteresseTelefone">Telefone</label>
                            </li>


							<li>
                                <label for="areaInteresseComentarios">Coment&aacute;rios*</label>
                                <textarea class="TextArea validate[required]" id="areaInteresseComentarios" name="areaInteresseComentarios"></textarea>
                            </li>

                            <li><input type="image" src="<?=base_url()?>assets/img/site/btn-enviar.png" class="btnEnviar" id="btrnInteresseEnviar" name="btrnInteresseEnviar" /></li>

                        </ul>
                        <div id="enviarInteresse"></div>
                    </form>

                </div><!--fecha Sidebar-->
				
            </div><!--fecha Container-->
        </div><!--fecha Wrapper-->
	
<?=$this->load->view('includes/footer');?>