<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$nome 				= $this->input->post('nome');
		$faixa_etaria 		= $this->input->post('faixa_etaria');
		$estado_civil 		= $this->input->post('estado_civil');
		$bairro_cidade 		= $this->input->post('bairro_cidade');
		$profissao 			= $this->input->post('profissao');
		$telefone 			= $this->input->post('telefone');
		$email 				= $this->input->post('email');
		$comentarios 		= $this->input->post('comentarios');
		$contato 			= $this->input->post('contato');
		
		$url 				= $this->input->post('url');
		$origem 			= $this->input->post('origem');
		
		$id_empreendimento	= 1;

		$ip 			= $this->input->ip_address();
		$user_agent		= $this->input->user_agent();
		$data			= date("Y-m-d H:i:s");
		
		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'data_envio'			=> $data,
			
			'nome'					=> $nome,
			'email'					=> $email,
			'faixa_etaria'			=> $faixa_etaria,
			'estado_civil'			=> $estado_civil,
			'bairro_cidade'			=> $bairro_cidade,
			'profissao'				=> $profissao,
			'telefone'				=> $telefone,
			'forma_contato'			=> $contato,
			'comentarios'			=> $comentarios,
			
			'url'					=> $url,
			'origem'				=> $origem
		);
		
		$this->model->setInteresse($data);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Vialog</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_grupo_capa.gif" alt="GRUPO CAPA" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>MORADAS CLUBE - SANTA MARIA</b> :: Contato via Site</td>
			  </tr>
			  <tr>
			    <td colspan="2">Contato enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>				  
			  <tr>
			    <td width="30%">E-mail:</td>
			    <td><strong><?=$email?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Faixa Etária:</td>
			    <td><strong><?=$faixa_etaria?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Estado Civil:</td>
			    <td><strong><?=$estado_civil?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Bairro e Cidade:</td>
			    <td><strong><?=$bairro_cidade?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Profissão:</td>
			    <td><strong><?=$profissao?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Telefone:</td>
			    <td><strong><?=$telefone?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Forma de Contato:</td>
			    <td><strong><?=$contato?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	GRUPO CAPA
			    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		$this->email->from("$email", "$nome");
		
		$list = array(
						'saldanha@capa.com.br',
						'aluizsantacruz@gmail.com',
						'testes@divex.com.br'
					);	
		
		$this->email->to($list);
		$this->email->subject('MORADAS CLUBE - SANTA MARIA - Interesse enviado pelo site');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email			
		
		$this->load->view('home/ajax.interesse.php');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */