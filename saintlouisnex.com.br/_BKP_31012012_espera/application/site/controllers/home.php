<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$nome 				= $this->input->post('nome');
		$faixa_etaria 		= $this->input->post('faixa_etaria');
		$estado_civil 		= $this->input->post('estado_civil');
		$bairro_cidade 		= $this->input->post('bairro_cidade');
		$profissao 			= $this->input->post('profissao');
		$telefone 			= $this->input->post('telefone');
		$email 				= $this->input->post('email');
		$comentarios 		= $this->input->post('comentarios');
		$contato 			= $this->input->post('contato');
		
		$url 				= $this->input->post('url');
		$origem 			= $this->input->post('origem');
		
		$id_empreendimento	= 41;

		$ip 			= $this->input->ip_address();
		$user_agent		= $this->input->user_agent();
		$data			= date("Y-m-d H:i:s");
		
		
		//CADASTRA INTERESSE NO BANCO DO NEXGROUP
			$dataNex = array (
				'id_empreendimento'		=> 41,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'nome'					=> $nome,
				'email'					=> $email,
				'faixa_etaria'			=> $faixa_etaria,
				'estado_civil'			=> $estado_civil,
				'bairro_cidade'			=> $bairro_cidade,
				'profissao'				=> $profissao,
				'telefone'				=> $telefone,
				'forma_contato'			=> $contato,
				'comentarios'			=> $comentarios,
				'url'					=> $url,
				'origem'				=> $origem,			
				'senha'					=> "`w~;ULm3CL{[AfV$"
				
			);
			//set POST variables
			$url = 'http://www.nexgroup.com.br/integracao/cadastraInteresse';
			$data_string = "";
			//url-ify the data for the POST
			foreach($dataNex as $key=>$value){ 
				$data_string .= $key.'='.$value.'&'; 
			}
			rtrim($data_string,'&');
			
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,count($dataNex));
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data_string);
			
			//execute post
			$result = curl_exec($ch);
			
			//close connection
			curl_close($ch);	
	
		//FIM CADASTRA INTERESSE NO BANCO DO NEXGROUP
		//$this->model->setInteresse($data);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Saint Louis</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center">
			  		<a href="http://www.eglengenharia.com.br" target="_blank">
			  			<img width="90px" border="0" src="http://www.eglengenharia.com.br/novosite/assets/img/logo.gif" alt="EGL ENGENHARIA" />
			  		</a>
			  	</td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>Saint Louis</b> :: Interesse via Site</td>
			  </tr>
			  <tr>
			    <td colspan="2">Interesse enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>				  
			  <tr>
			    <td width="30%">E-mail:</td>
			    <td><strong><?=$email?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Faixa Etária:</td>
			    <td><strong><?=$faixa_etaria?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Estado Civil:</td>
			    <td><strong><?=$estado_civil?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Bairro e Cidade:</td>
			    <td><strong><?=$bairro_cidade?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Profissão:</td>
			    <td><strong><?=$profissao?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Telefone:</td>
			    <td><strong><?=$telefone?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Forma de Contato:</td>
			    <td><strong><?=$contato?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	EGL ENGENHARIA
			    	<a href="http://www.eglengenharia.com.br">www.eglengenharia.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		
		//Fim da Mensagem
		
		$this->email->from("$email", "$nome");
	
			$list = array(
					'atendimento@divex.com.br'
			);
		
		$this->email->to($list);
		$this->email->subject('Saint Louis - Interesse enviado pelo site');
		$this->email->message("$conteudo");
		
		//$this->email->send();
		
		//===================================================================
		//Termina o envio do email			
		
		$this->load->view('home/ajax.interesse.php');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */