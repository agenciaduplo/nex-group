<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
		<title>Saint Louis - 2 e 3 dormitórios com suíte</title>
	
		<link href="<?=base_url()?>assets/css/site/promocao.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/jquery.fancybox-1.3.4.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/validationEngine.jquery.css" media="screen" />

	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/default-promo.css" type="text/css" media="screen" />
	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/style-promo.css" type="text/css" media="screen" />
 	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/nivo-slider-promo.css" type="text/css" media="screen" />


		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox-1.3.4.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine-pt.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.nivo.slider.pack.js"></script>
		<!-- <script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.limit-1.2.source.js"></script> -->
		
		<script type="text/javascript">
			
			<?php if(@$enviou == "S"): ?>
			
			function enviouDados ()
			{
				alert("Frase enviada com sucesso!");
			}
			
			enviouDados();
			
			<?php endif; ?>
		
			$(document).ready(function() {

    			//$('#myTextarea').limit('140','#charsLeft');
				
				$("a#modal").fancybox({
					'padding'			: 0,
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'autoScale'			: false,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6,
					'autoDimensions'	: false,
					'width'         	: 580,
					'height'        	: 445,
					'scrolling'			:'yes'
				});
				$("a#modal-galeria").fancybox({
					'padding'			: 0,
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'autoScale'			: true,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6,
					'autoDimensions'	: true,
					'scrolling'			:'no'
				});
				jQuery("#st-promo-box-pergunta-form-in").validationEngine(					
					'attach', {
						promptPosition : "topLeft"
				});

			});
		</script>		
		
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-1622695-53']);
			_gaq.push(['_trackPageview']);
			
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();	
		</script>
	</head>
	<body>
		<div id="st-promo-container">
		
			<div id="st-promo-box">
		    	<div id="st-promo-social">
		        </div>
		    	<div id="st-promo-destaque"></div>
		        <div id="st-promo-logo"></div>
		        <div id="st-promo-foto"></div>
                
                <a href="<?=base_url()?>promocao">
                    <div id="st-promo-vencedor-voltar">
                        <p class="st-promo-vencedor-p1">Voltar para home</p>
                        <p class="st-promo-vencedor-p2">Clique aqui para preencher o formulário.</p>
                    </div>
                </a>        

                <div id="st-promo-box2">
                    <div id="st-promo-box-pergunta-galeria">
                        <div class="box-galeria">

                            <div class="box-galeria-win win-8">
                                <div class="win-data">VENCEDOR DA SEMANA - 19.05 a 25.05</div>
                                <div class="win-frase"><p>"Doce lugar, Saint Louis que é eterno no meu coração. E aos poetas traz inspiração. Para morar nesse lugar eu pedalaria até o Japão."</p><p class="win-frase-name">Márcia Aquino</p></div>
                            </div>

                            <div class="box-galeria-win win-7">
                                <div class="win-data">VENCEDOR DA SEMANA - 12.05 a 18.05</div>
                                <div class="win-frase"><p>"Se eu pudesse parar o tempo, faria isso da janela do Saint Louis. De lá, até o porto do sol de Porto Alegre fica mais bonito."</p><p class="win-frase-name">Diego Garcia Goulart</p></div>
                            </div>

                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/jose.jpg">
                            <div class="box-galeria-win win-6">
                                <div class="win-data">VENCEDOR DA SEMANA - 05.05 a 11.05</div>
                                <div class="win-frase"><p>"Quando as palavras não são tão dignas quanto o silêncio, é melhor calar, esperar uma bicicleta ganhar e então pedalar para o Saint Louis que com certeza é meu lugar."</p><p class="win-frase-name">Jose Enio Rosa Onofrio</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
                            
                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/rea.jpg">
                            <div class="box-galeria-win win-5">
                                <div class="win-data">VENCEDOR DA SEMANA - 28.04 a 04.05</div>
                                <div class="win-frase"><p>"De bike eu pedalaria sem cansar, só para o charme do Saint Louis poder desfrutar."</p><p class="win-frase-name">Rea Ribeiro</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
                            
                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/pedro.jpg">
                            <div class="box-galeria-win win-4">
                                <div class="win-data">VENCEDOR DA SEMANA - 21.04 a 27.04</div>
                                <div class="win-frase"><p>"Não basta apenas gostar de pedalar, encontrei no Saint Louis o meu jeito de morar por isso de tudo faria para garantir aqui o meu lugar."</p><p class="win-frase-name">Pedro Horn Sehbe</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/taisa.jpg">
                            <div class="box-galeria-win win-3">
                                <div class="win-data">VENCEDOR DA SEMANA - 14.04 a 20.04</div>
                                <div class="win-frase"><p>"Eu pedalaria pelas ruas de Porto Alegre a divulgar, que o Saint Louis é o melhor lugar para se morar."</p><p class="win-frase-name">Taisa Rosa</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/fabiana.jpg">
                            <div class="box-galeria-win win-2">
                                <div class="win-data">VENCEDOR DA SEMANA - 06.04 a 13.04</div>
                                <div class="win-frase"><p>"Sairia feliz, cantando por aí a pedalar, que moraria no Saint Louis para ganhar segurança, saúde e bem-estar."</p><p class="win-frase-name">Fabiana da Costa</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
                            <a id="modal-galeria" href="<?=base_url()?>assets/img/site/promocao-bicicletas/bianca.jpg">
                            <div class="box-galeria-win win-1">
                                <div class="win-data">VENCEDOR DA SEMANA - 22.03 a 06.04</div>
                                <div class="win-frase"><p>"Eu dançaria tango no teto. Eu limparia os trilhos do metrô. Eu iria com a minha Caloi nova do Rio a Salvador."</p><p class="win-frase-name">Bianca Araújo Grassi</p></div>
                                <div class="galeria-ampliar"></div>
                            </div>
                            </a>
        
                        </div>
                        <div id="st-promo-box-pergunta-logos-galeria"><p style="font-size:10px">INCORPORAÇÃO E CONSTRUÇÃO:</p></div>
                        <div id="st-promo-box-pergunta-form-galeria">
                            <p style="font-size:9px; text-align:center; margin-top:42px; margin-bottom:30px;">Incorporação registrada sob o nº137.307 do Registro de Imóveis da 2ª Zona de Porto Alegre I Projeto Arquitetônico Núcleo Arquitetura CREA 94395-DE | Projeto Paisagístico Takeda CREA 79320 Todas as imagens são meramente ilustrativas. Áreas comuns equipadas e entregues conforme memorial descritivo.</p>
                        </div>
                    </div>                            
                </div>
                   
            </div>
			
		    
		    <div id="st-promo-footer-divex">
		        <a href="http://www.divex.com.br" target="_blank" title="Desenvolvido por Divex"><div id="st-promo-divex"></div></a>
		    </div>
		    
		</div>
	
	</body>
</html>