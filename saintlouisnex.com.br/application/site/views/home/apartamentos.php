<?=$this->load->view('includes/header');?>

<div id="container-interna">

	<div id="topo-interna">
		<div id="logo">
       		<h1>Saint Louis - 2 e 3 dormitórios com suíte</h1>
       		<a href="<?=site_url()?>" title="Voltar para Página Inicial"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-logo.png" alt="Saint Louis - 2 e 3 dormitórios com suíte" width="260" height="80" /></a>
        </div>
        
        <a href="javascript:window.open('http://web.atendimentoaovivo.com.br/chat.asp?idc=5447&amp;pre_empresa=1192&amp;pre_depto=1276&amp;vivocustom=&amp;vivocustom2=DHZ&amp;vivocustom3=Site+DHZ&amp;vivocustom4=Rodape&amp;idtemplate=0','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="corretor">Corretor Online</a>
        <a id="modal_interesse" href="#modal-interesse" class="interesse">Tenho Interesse</a>
        <a id="modal_amigo" href="#modal-amigo" class="amigo">Indique para um amigo</a>
	
    </div>
    
    <?=$this->load->view('includes/menu.internas.php');?>
    
     <div id="conteudo-apart">
	    <div id="menu-apart">
            <a id="abas-dorm3" class="dorm3-on">3 DORMITÓRIOS <span>COM SUÍTE</span></a>
            <a id="abas-dorm2" class="dorm2">2 DORMITÓRIOS <span>COM SUÍTE</span></a>
            <a id="abas-dorm1" class="impla">IMPLANTAÇÃO</a>
        </div>
   	    <div id="container-dinamico">
			<!-- 3 DORMITORIOS -->   
        	<div id="box-dinamico-1">
                <span><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-rosa-dos-ventos.png" width="60" height="60" alt="Orientação" /></span>
                
              <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-3dormitorios-01.png">
                <img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-3dormitorios-01.png" width="304" height="312" />
              	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
              </a>
                
                <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-3dormitorios-02.png">
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-3dormitorios-02.png" width="330" height="288" alt="" />
                	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>
                
                <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-3dormitorios-03.png">
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-3dormitorios-03.png" width="361" height="306" alt="" />
					<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>
        	</div>
			<!-- 2 DORMITORIOS -->   
        	<div id="box-dinamico-2">
                <span><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-rosa-dos-ventos.png" width="60" height="60" alt="Orientação" /></span>
                
              	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-2dormitorios-02.png">
	            	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-2dormitorios-02.png" width="345" height="264" alt="" />
	            	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
	            </a>
  
                
              <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-2dormitorios-01.png">
                <img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-2dormitorios-01.png" width="367" height="264" />
              	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
              </a>
        	</div>
	  		<!-- IMPLANTAÇÃO -->  
   	  		<div id="box-dinamico-3">
                <span><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-rosa-dos-ventos.png" width="60" height="60" alt="Orientação" /></span>
                
                <div id="implantacao-img"><p><img id="img_03" src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-implatacao-01.png" width="845" height="450" /></p></div> 
                
				<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-02.png">
                	<h4>GARAGEM 2º ANDAR</h4>
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-implatacao-02.png" width="367" height="264" />
            		<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>
                
                <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-03.png">
                	<h4>GARAGEM ANDAR TÉRREO</h4>
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-implatacao-03.png" width="345" height="264" alt="" />
                	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>

                <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-04.png">
                	<h4>GARAGEM 4º ANDAR</h4>
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-implatacao-04.png" width="345" height="264" alt="" />
                	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>

                <a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-05.png">
                	<h4>GARAGEM 3º ANDAR</h4>
                	<img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-implatacao-05.png" width="345" height="264" alt="" />
                	<p>Clique para ampliar <img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-lupa-peq.png" width="12" height="12" alt="" /></p>
                </a>

        	</div>
        </div>
			
  	</div>
    
     <div id="line-1"></div>
     <div id="line-2"></div>
	
<?=$this->load->view('includes/footer');?>