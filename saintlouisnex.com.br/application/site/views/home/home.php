<?=$this->load->view('includes/header');?>
    
<div id="container">

	<div id="topo">
		<div id="logo">
       		<h1>Saint Louis - 2 e 3 dormitórios com suíte</h1>
       		<a title="Saint Louis - 2 e 3 dormitórios com suíte"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-logo.png" alt="Saint Louis - 2 e 3 dormitórios com suíte" width="260" height="80" /></a>
        </div>
        
        <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=8','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="corretor">Corretor Online</a>
        <a id="modal_interesse" href="#modal-interesse" class="interesse" onClick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Tenho Interesse - Saint Louis'])">Tenho Interesse</a>
        <a id="modal_amigo" href="#modal-amigo" class="amigo">Indique para um amigo</a>
	
  		<div id="destaque">
  			<?php if($this->agent->is_mobile()): ?>
  				
  				<img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-destaque.png" />
  			
  			<?php else: ?>
  		
				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="940" height="560" id="FlashID" title="destaque">
				<param name="movie" value="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-destaque-05.swf" />
				<param name="quality" value="high" />
				
				<param name="wmode" value="transparent" />
				<param name="swfversion" value="6.0.65.0" />
				<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
				<param name="expressinstall" value="Scripts/expressInstall.swf" />
				<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-destaque-05.swf" width="940" height="560">
				<!--<![endif]-->
				<param name="quality" value="high" />
				<param name="wmode" value="transparent" />
				<param name="swfversion" value="6.0.65.0" />
				<param name="expressinstall" value="Scripts/expressInstall.swf" />
				<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
				<div>
				<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
				<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
				</div>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
				</object>
				
			<?php endif; ?>
  		</div>
    </div>
    
    <?=$this->load->view('includes/menu.home.php');?>
    
     <div id="conteudo">
     	<div id="conteudo-infra">
        	
          <!--  <div id="conteudo-promo"><a href="<?=site_url()?>promocao" title="Clique e Confira como Participar"><img src="<?=base_url()?>assets/img/site/placa-promo-st.png" alt="line" width="541" height="151" /></a></div> -->
            
        	<div id="infra">
                <h2>INFRAESTRUTURA</h2>
                <p><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-line-h2.png" alt="line" width="270" height="15" /></p>
            </div>
            <div id="home-img1" class="home-img">
               <a href="<?=site_url()?>infraestrutura" title="Porte-Cochère"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-home-cochere.jpg" alt="line" width="230" height="126" />
              <p>Porte-Cochère</p></a>
			</div>
            
            <div id="home-img2" class="home-img">
               <a href="<?=site_url()?>infraestrutura" title="Fitness"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-home-fitness.jpg" alt="line" width="230" height="126" />
              <p>Fitness</p></a>
			</div>
            
            <div id="home-img3" class="home-img">
               <a href="<?=site_url()?>infraestrutura" title="Quadra poliesportiva com bosque"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-home-quadra.jpg" alt="line" width="230" height="126" />
              <p>Quadra poliesportiva com bosque</p></a>
			</div>
            
            <div id="home-img4" class="home-img">
               <a href="<?=site_url()?>infraestrutura" title="Salão de festas"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-home-salao.jpg" alt="line" width="230" height="126" />
              <p>Salão de festas</p></a>
			</div>
            
        </div>
        
       	<div id="conteudo-local">
        	<div id="local">
                <h2>LOCALIZAÇÃO</h2>
                <p><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-line-h2.png" alt="line" width="270" height="15" /></p>
            </div>
            <div id="home-mapa">
               <p><a href="<?=site_url()?>localizacao" title="Localização"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-home-mapa.png" alt="line" width="301" height="303" /></a></p>
			</div>
            
            <div id="home-rua">
                <a href="<?=site_url()?>localizacao" title="Localização"><p>Rua São Luís, 1163 - Porto Alegre/RS</ br></p> 
                <p>Fone: (51) 3286.2162 </p></a>
            </div>
            <div id="home-corretor">
                <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=8','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" title="Corretor Online"><p>CORRETOR ONLINE</ br></p> 
                <p><span>consulte</span></p></a>
	        </div>
        </div>
     </div>
     
     <div id="line-1"></div>
     <div id="line-2"></div>
	
<?=$this->load->view('includes/footer');?>