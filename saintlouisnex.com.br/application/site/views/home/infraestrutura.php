<?=$this->load->view('includes/header');?>

<div id="container-interna">

	<div id="topo-interna">
		<div id="logo">
       		<h1>Saint Louis - 2 e 3 dormitórios com suíte</h1>
       		<a href="<?=site_url()?>" title="Voltar para Página Inicial"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-logo.png" alt="Saint Louis - 2 e 3 dormitórios com suíte" width="260" height="80" /></a>
        </div>
        
        <a href="javascript:window.open('http://web.atendimentoaovivo.com.br/chat.asp?idc=5447&amp;pre_empresa=1192&amp;pre_depto=1276&amp;vivocustom=&amp;vivocustom2=DHZ&amp;vivocustom3=Site+DHZ&amp;vivocustom4=Rodape&amp;idtemplate=0','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="corretor">Corretor Online</a>
        <a id="modal_interesse" href="#modal-interesse" class="interesse">Tenho Interesse</a>
        <a id="modal_amigo" href="#modal-amigo" class="amigo">Indique para um amigo</a>
	
    </div>
    
    <?=$this->load->view('includes/menu.internas.php');?>
    
    <div id="interna-infra">
    	<div id="fotos-destaque">
            <div id="wrapper">
                <div class="slider-wrapper theme-default">
                    <div class="ribbon"></div>
                    <div id="slider" class="nivoSlider">
                        <img src="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-infra-01.jpg" alt="" data-transition="boxRainGrowReverse" title="Porte-Cochère" />
                        <img src="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-infra-02.jpg" alt="" data-transition="boxRainGrowReverse" title="Piscina" />
                        <img src="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-infra-03.jpg" alt="" data-transition="boxRainGrowReverse" title="Fitness" />
                        <img src="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-infra-04.jpg" alt="" data-transition="boxRainGrowReverse" title="Quadra poliesportiva com Bosque" />
                        <img src="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-infra-05.jpg" alt="" data-transition="boxRainGrowReverse" title="Salão de Festas" />
                    </div>
                </div>
            </div>
        </div>
        
    	<div id="detalhes-destaque-left">
        	<ul>
            	<li>•&nbsp&nbspPorte-Cochère</li>
            	<li>•&nbsp&nbspPiscina</li>
            	<li>•&nbsp&nbspFitness</li>
            	<li class="lineheight">•&nbsp&nbspQuadra poliesportiva<br />
       	        &nbsp&nbsp&nbspcom Bosque</li>
            	<li>•&nbsp&nbspSalão de Festas</li>
            </ul>
    	</div>
    	<div id="detalhes-destaque-right">
        	<ul>
            	<li>•&nbsp&nbspPergolado</li>
            	<li>•&nbsp&nbspPlayground</li>
            </ul>
    	</div>
        <div id="impla-destaque">
       	  <h2>IMPLANTAÇÃO</h2>
        	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-01.jpg"><img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-infraestrutura-impla-01.jpg" width="110" height="110" alt="" /></a>
        	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-02.png"><img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-infraestrutura-impla-02.jpg" width="110" height="110" alt="" /></a>
        	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-03.png"><img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-infraestrutura-impla-03.jpg" width="110" height="110" alt="" /></a>
        	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-04.png"><img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-infraestrutura-impla-04.jpg" width="110" height="110" alt="" /></a>
        	<a id="example2" href="<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-05.png"><img src="<?=base_url()?>assets/img/site/fotos/thumbs/nexgroup-hotsite-saintlouis-infraestrutura-impla-02.jpg" width="110" height="110" alt="" /></a>
        </div>
    </div>

     <div id="line-1"></div>
     <div id="line-2"></div>
	
<?=$this->load->view('includes/footer');?>