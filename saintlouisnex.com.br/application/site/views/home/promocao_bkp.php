<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
		<title>Saint Louis - 2 e 3 dormitórios com suíte</title>
	
		<link href="<?=base_url()?>assets/css/site/promocao.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/jquery.fancybox-1.3.4.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/validationEngine.jquery.css" media="screen" />

	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/default-promo.css" type="text/css" media="screen" />
	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/style-promo.css" type="text/css" media="screen" />
 	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/nivo-slider-promo.css" type="text/css" media="screen" />


		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox-1.3.4.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine-pt.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.nivo.slider.pack.js"></script>
		<!-- <script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.limit-1.2.source.js"></script> -->
		
		<script type="text/javascript">
			
			<?php if(@$enviou == "S"): ?>
			
			function enviouDados ()
			{
				alert("Frase enviada com sucesso!");
			}
			
			enviouDados();
			
			<?php endif; ?>
		
			$(document).ready(function() {

    			//$('#myTextarea').limit('140','#charsLeft');
				
				$("a#modal").fancybox({
					'padding'			: 0,
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'autoScale'			: false,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6,
					'autoDimensions'	: false,
					'width'         	: 580,
					'height'        	: 445,
					'scrolling'			:'yes'
				});
				
				jQuery("#st-promo-box-pergunta-form-in").validationEngine(					
					'attach', {
						promptPosition : "topLeft"
				});

			});
		</script>
		
	
		<script type="text/javascript">
			$(window).load(function() {
				$('#slider').nivoSlider({
					effect: '', // Specify sets like: 'fold,fade,sliceDown'
					slices: 15, // For slice animations
					boxCols: 8, // For box animations
					boxRows: 4, // For box animations
					animSpeed: 200, // Slide transition speed
					pauseTime: 6000, // How long each slide will show
					startSlide: 0, // Set starting Slide (0 index)
					directionNav: true, // Next & Prev navigation
					directionNavHide: false, // Only show on hover
					controlNav: false, // 1,2,3... navigation
					controlNavThumbs: false, // Use thumbnails for Control Nav
					controlNavThumbsFromRel: false, // Use image rel for thumbs
					controlNavThumbsSearch: '.jpg', // Replace this with...
					controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
					keyboardNav: true, // Use left & right arrows
					pauseOnHover: true, // Stop animation while hovering
					manualAdvance: false, // Force manual transitions
					captionOpacity: 0.8, // Universal caption opacity
					prevText: 'Prev', // Prev directionNav text
					nextText: 'Next', // Next directionNav text
					randomStart: false, // Start on a random slide
					beforeChange: function(){}, // Triggers before a slide transition
					afterChange: function(){}, // Triggers after a slide transition
					slideshowEnd: function(){}, // Triggers after all slides have been shown
					lastSlide: function(){}, // Triggers when last slide is shown
					afterLoad: function(){} // Triggers when slider has loaded
				});
			});
		</script> 
		
		
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-1622695-53']);
			_gaq.push(['_trackPageview']);
			
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();	
		</script>
	</head>
	<body>
		<div id="st-promo-container">
		
			<div id="st-promo-box">
		    	<div id="st-promo-social">
		            <!-- AddThis Button BEGIN -->
		            <div class="addthis_toolbox addthis_default_style ">
		            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		            <a class="addthis_button_tweet"></a>
		            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
		            <!--<a class="addthis_counter addthis_pill_style"></a>-->
		            </div>
		            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f6a24a51a684879"></script>
		            <!-- AddThis Button END -->
		        </div>
		    	<div id="st-promo-destaque"></div>
		        <div id="st-promo-logo"></div>
		        <div id="st-promo-foto"></div>
                
                <div id="st-promo-vencedor" class="slider-wrapper theme-default">
                    <div id="slider" class="nivoSlider">
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-6.png" data-transition="slideInLeft" alt="" />
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-5.png" data-transition="slideInLeft" alt="" />
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-4.png" data-transition="slideInLeft" alt="" />
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-3.png" data-transition="slideInLeft" alt="" />
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-2.png" data-transition="slideInLeft" alt="" />
                        <img src="<?=base_url()?>assets/img/site/promocao-bicicletas/vencedor-1.png" data-transition="slideInLeft" alt="" />
                    </div>
                </div>
                
				<div id="st-promo-box2">
					<div id="st-promo-box-pergunta">
		            	<div id="st-promo-box-pergunta-logos"><p style="font-size:10px">INCORPORAÇÃO E CONSTRUÇÃO:</p></div>
		            	<div id="st-promo-box-pergunta-form">
		                    
		                    <form method="post" id="st-promo-box-pergunta-form-in" name="st-promo-box-pergunta-form-in" action="<?=site_url()?>home/enviar_pergunta">
		                        
		                            <fieldset>
										<textarea maxlength="140" class="validate[required] Text" id="st-promo-box-pergunta-form-message" name="frase"></textarea><br>
										
		                                <div class="st-promo-box-pergunta-form-1">
		                                    <label for="">Nome*</label>
		                                    <input class="validate[required] st-promo-box-pergunta-form-text" id="nome" name="nome" type="text" value="">
		                                </div>
		                                
		                                <div class="st-promo-box-pergunta-form-1" style="float:right">
		                                    <label for="">E-mail*</label>
		                                    <input class="validate[required, custom[email]] st-promo-box-pergunta-form-text" id="email" name="email" type="text" value="">
		                                </div>
		                                
		                                <div class="st-promo-box-pergunta-form-1">
		                                    <div class="st-promo-box-pergunta-form-2">
		                                        <label for="">Cidade*</label>
		                                        <input class="validate[required] st-promo-box-pergunta-form-text-2" id="cidade" name="cidade" type="text" value="">
		                                    </div>
		                                    <div class="st-promo-box-pergunta-form-2" style="float:right">
		                                        <label for="">Bairro*</label>
		                                        <input class="validate[required] st-promo-box-pergunta-form-text-2" id="bairro" name="bairro" type="text" value="">
		                                    </div>
		                                </div>   
		                                
		                                <div class="st-promo-box-pergunta-form-1" style="float:right">
		                                    <label for="">Como ficou sabendo da promoção?*</label>
		                                    <select class="validate[required] st-promo-box-pergunta-form-text" id="como_soube" name="como_soube">
		                                    	<option value="">Selecione</option>
		                                    	<option value="Caixa de Pizza">Caixa de Pizza</option>
		                                        <option value="Jornal">Jornal</option>
		                                        <option value="Rádio">Rádio</option>
		                                        <option value="Panfletagem">Panfletagem</option>
		                                        <option value="Revista">Revista</option> 
		                                        <option value="Site">Site</option> 
		                                        <option value="E-mail Marketing">E-mail Marketing</option> 
		                                        <option value="Outro">Outro</option> 
		                                    </select>                                    
		                                </div>
		                                
		                                <div class="st-promo-box-pergunta-form-3">
		                                    <input class="validate[required] st-promo-box-pergunta-form-check" name="regulamento" id="regulamento" type="checkbox" value="S" />
		                                    <p style="float:left">Li e aceito todos os <a id="modal" href="<?=site_url()?>home/promocao_termos">termos do regulamento.</a></p>
		                                </div>
		                                
		                                <button class="st-promo-box-pergunta-form-btn" type="submit"></button>
		                         
		                         </fieldset>
		                         
		                    </form>
		                    
		                    <p style="font-size:9px; text-align:center; margin-top:42px; margin-bottom:30px;">Incorporação registrada sob o nº137.307 do Registro de Imóveis da 2ª Zona de Porto Alegre I Projeto Arquitetônico Núcleo Arquitetura CREA 94395-DE | Projeto Paisagístico Takeda CREA 79320 Todas as imagens são meramente ilustrativas. Áreas comuns equipadas e entregues conforme memorial descritivo.</p>
		                </div>
		        	</div>                            
		        </div>
		           
		    </div>
			
		    
		    <div id="st-promo-footer-divex">
		        <a href="http://www.divex.com.br" target="_blank" title="Desenvolvido por Divex"><div id="st-promo-divex"></div></a>
		    </div>
		    
		</div>
	
	</body>
</html>