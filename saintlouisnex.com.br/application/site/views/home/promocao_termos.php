<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Saint Louis - 2 e 3 dormitórios com suíte</title>
<link href="<?=base_url()?>assets/css/site/promocao.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="regulamento-paragrafo" style="width:529px; height:670px; overflow:auto;">
<strong>Regulamento:</strong>
<br/><br/>

Preencha o formulário corretamente com seus dados (nome, email, bairro/cidade, mídia

de captação), crie uma frase respondendo a pergunta: - O que você faria para ter o seu

lugar? A frase mais criativa da semana você ganhará uma bicicleta Caloi modelo 100.

A escolha da melhor frase da semana será sempre aos sábados e a divulgação dos

ganhadores ocorrerá às segundas-feiras no plantão de vendas do Saint Louis na rua São

Luis nº 1163, bairro Santana/ Porto Alegre e também pelo www.saintlouisnex.com.br A

primeira frase será escolhida no dia 7 (sete) de abril e a divulgação do vencedor será no

dia 9 (nove) de abril e assim sucessivamente pelas próximas semanas, dentro do período

da Promoção. O período da Promoção é de 22 (vinte e dois) de março a 25 (vinte e

cinco) de maio de 2012. As melhores frases serão escolhidas por uma comissão

julgadora formada por membros da Compacta Comunicação – agência do Nex Group -

que avaliará a criatividade e o correto uso da língua portuguesa. Será escolhida 1 (uma)

frase por semana, num total de oito frases no período da Promoção. Esta é uma

promoção estritamente de cunho cultural. Não poderão participar deste concurso

funcionários do Nex Group e sua agência de publicidade, bem como funcionários de

empresas envolvidas. As frases escolhidas, assim como imagem/ou voz dos ganhadores

poderão ser usadas pelo Nex Group em toda e qualquer mídia gráfica ou eletrônica. Os

casos omissos neste regulamento serão decididos pela organização da promoção, sem

possibilidade de recurso. O vencedor poderá escolher o modelo Caloi 100 Feminino ou

Masculino. Após a divulgação do nome do vencedor o mesmo terá até 15 dias para

retirar sua bicicleta na loja indicada pela organização, mediante apresentação de

documentação.
</div>
</body>
</html>