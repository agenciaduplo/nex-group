<div id="rodape">
  	   	<div id="rodape-conteudo">
        	<div id="assinatura">
            	<h3>Incorporação e Construção:</h3>
            	<a href="http://www.nexgroup.com.br/" title="NEX GROUP"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-assinatura.png" alt="NexGroup" width="240" height="60" /></a>  
            </div>
            
            <div id="redes-sociais">
            	<!-- <div style="margin-bottom:4px;" class="fb-like" data-href="http://www.saintlouisnex.com.br/site" data-send="false" data-width="450" data-show-faces="false" data-colorscheme="dark"></div> -->
                <a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<g:plusone size="medium"></g:plusone>
				<div class="fb-like" data-href="http://www.saintlouisnex.com.br" data-send="true" data-layout="button_count" data-width="240" data-show-faces="false" data-font="arial"></div>

            </div>
        	
            <div id="incorp">
            <p>Incorporação registrada sob o nº137.307 do Registro de Imóveis da 2ª Zona de Porto Alegre I Projeto Arquitetônico Núcleo Arquitetura CREA 94395-DE | Projeto Paisagístico Takeda CREA 79320</p>
            <p>Todas as imagens são meramente ilustrativas. Áreas comuns equipadas e entregues conforme memorial descritivo.</p></div>
        
        	<div id="divex"><a href="http://www.divex.com.br/" title="Divex Imobi"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-divex.png" alt="Divex Imobi" width="80" height="20" /></a></div>

        </div>
     </div>
</div>

<?=$this->load->view('includes/modal.interesse.php');?>
<?=$this->load->view('includes/modal.amigo.php');?>

<?php if(!$this->agent->is_mobile()): ?>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
<?php endif; ?>

<!-- Código do Google para tag de remarketing -->
		<!--------------------------------------------------
		As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 978900397;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983735308/?value=0&guid=ON&script=0"/>
			</div>
		</noscript>

</body>

</html>