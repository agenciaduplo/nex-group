<div id="rodape">
  	   	<div id="rodape-conteudo">
        	<div id="assinatura">
            	<h3>Incorporação e Construção:</h3>
            	<a href="http://www.nexgroup.com.br/" title="NEX GROUP"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-assinatura.png" alt="NexGroup" width="240" height="60" /></a>  
            </div>
            
            <div id="redes-sociais">
            	<div style="margin-bottom:4px;" class="fb-like" data-href="http://www.saintlouisnex.com.br/site" data-send="false" data-width="450" data-show-faces="false" data-colorscheme="dark"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<g:plusone size="medium"></g:plusone>
            </div>
        	
            <div id="incorp">
            <p>Incorporação registrada sob o nº137.307 do Registro de Imóveis da 2ª Zona de Porto Alegre I Projeto Arquitetônico Núcleo Arquitetura CREA 94395-DE | Projeto Paisagístico Takeda CREA 79320</p>
            <p>Todas as imagens são meramente ilustrativas. Áreas comuns equipadas e entregues conforme memorial descritivo.</p></div>
        
        	<div id="divex"><a href="http://www.divex.com.br/" title="Divex Imobi"><img src="<?=base_url()?>assets/img/site/nexgroup-hotsite-saintlouis-divex.png" alt="Divex Imobi" width="80" height="20" /></a></div>

        </div>
     </div>
</div>

<!-- modal interesse -->
<div style="display:none">  
    <div id="modal-interesse">
        <div id="conteudo-contato">
            <h2>Tenho Interesse</h2>
            <div id="modal-line1"></div>
            <div id="modal-line2"></div>	
            <form action="" name="formContato" id="formContato" method="post">
                <p class="interesse-nome">NOME *</p>
                <input id="nome" name="nome" type="text" class="validate[required] form1" />
                
                <p class="interesse-fone">TELEFONE</p>
                <input id="telefone" name="telefone" type="text" class="form1" />
                
                <p class="interesse-faixa">FAIXA ETÁRIA</p>
                <select id="faixa_etaria" name="faixa_etaria"  class="form1">
                	<option value="Até 30 anos">At&eacute; 30 anos</option>
	                <option value="De 31 a 40 anos">De 31 a 40 anos</option>
	                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
	                <option value="Mais de 50 anos">Mais de 50 anos</option>
                </select>
                
                <p class="interesse-email">EMAIL *</p>
                <input id="email" name="email" type="text" class="validate[required, custom[email]] form1" />
                
                <p class="interesse-civil">ESTADO CIVIL</p>
                <select id="estado_civil" name="estado_civil" class="form1">
                	<option value="Casado">Casado</option>
               	 	<option value="Solteiro">Solteiro</option>
                </select>
    
                <p class="interesse-bairro">BAIRRO/CIDADE *</p>
                <input id="bairro_cidade" name="bairro_cidade" type="text" class="form1" />
    
                <p class="interesse-formas">FORMAS DE CONTATO</p>
                <input id="forma_contato_tel" name="forma_contato" type="radio" class="radio" checked /><p class="interesse-formas">TELEFONE</p>
                <input id="forma_contato_email" name="forma_contato" type="radio" class="radio" /><p class="interesse-formas">EMAIL</p>
    
    			<span class="profissa">
                <p class="interesse-profissa">PROFISSÃO</p>
                <input id="profissao" name="profissao" type="text" class="form1" />
                </span>
    
                <p class="interesse-coment">COMENTÁRIO</p>
                <textarea id="comentario" name="comentario" class="form2" ></textarea>
                
                <p class="interesse-obrigado">* Campo Obrigatório</p>
                <p class="mensagem-sucesso">Interesse enviado com sucesso!</p>         
            </form>
            <div id="btn-enviar"> 
                <a id="btnEnviarInteresse" href="javascript: enviarEmailContato();" class="form3">enviar</a>
            </div>
        </div>
    </div>
</div>
<!-- modal interesse -->


<!-- modal amigo -->
<div style="display:none">  
    <div id="modal-amigo">
        <div id="conteudo-contato">
            <h2>INDIQUE PARA UM AMIGO</h2>
            <div id="modal-line1"></div>
            <div id="modal-line2"></div>	
            <form action="" name="formIndique" id="formIndique" method="post">
                <p class="interesse-nome-amigo">NOME *</p>
                <input id="indiqueNome" name="nome" type="text" class="validate[required] form1" />

                <span class="email-nome">
                	<p class="email-nome-in">EMAIL *</p>
                	<input id="indiqueEmail" name="profissao" type="text" class="validate[required, custom[email]] form1" />
                </span>
                <span class="nome-amigo">
                	<p class="nome-amigo-in">NOME DO AMIGO *</p>
                	<input id="indiqueNomeAmigo" name="nomeamigo" type="text" class="validate[required] form1" />
                </span>
                <span class="email-amigo">
                	<p class="email-amigo-in">EMAIL DO AMIGO *</p>
                	<input id="indiqueEmailAmigo" name="emailamigo" type="text" class="validate[required, custom[email]] form1" />
                </span>
                
                <p class="interesse-coment-amigo">COMENTÁRIO</p>
                <textarea name="comentario" class="form4" ></textarea>
                
                <p class="amigo-obrigado">* Campo Obrigatório</p>        
            </form>
            <div id="btn-enviar-amigo"> 
                <a href="javascript: enviarIndique();" class="form3">enviar</a>
            </div>
        </div>
    </div>
</div>

<!-- modal amigo -->

<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>

</html>