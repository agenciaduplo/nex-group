<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Saint Louis - 2 e 3 dormitórios com suíte em Porto Alegre</title>
	<meta name="description" content="Apartamentos 2 e 3 dormitórios com suíte em Porto Alegre / RS." />
    <meta name="keywords" content="apartamentos 3 dorms, apartamentos 3 dormitorios a venda, apartamentos 3 dormitorios a venda em porto alegre" />
    <meta name="author" content="Divex Imobi - http://www.imobi.divex.com.br" />
    <meta name="robots" content="index, follow" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


	<link href="<?=base_url()?>assets/css/site/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/jquery.fancybox-1.3.4.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site/validationEngine.jquery.css" media="screen" />
	
		
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox-1.3.4.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<?php if(!$this->agent->is_mobile()): ?>
	<script src="<?=base_url()?>assets/js/site/swfobject_modified.js" type="text/javascript"></script>
	<?php endif; ?>
		
	<?php if(@$page == 'infraestrutura'): ?>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.nivo.slider.pack.js"></script>
	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/default.css" type="text/css" media="screen" />
	    <link rel="stylesheet" href="<?=base_url()?>assets/css/site/nivo-slider.css" type="text/css" media="screen" />
	<?php endif; ?>
	
	<?php if(@$page == 'apartamentos'): ?>
    <script src="<?=base_url()?>assets/js/site/jquery.imageLens.js" type="text/javascript"></script>
    <?php endif; ?>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("a#modal_interesse").fancybox({
				'onClosed'			: function () {
					jQuery('#formContato').validationEngine('hide');
				},
				'padding'			: 0,
				'transitionIn'		: 'fade',
				'transitionOut'		: 'fade',
				'autoScale'			: false,
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.6,
				'autoDimensions'	: false,
				'width'         	: 860,
				'height'        	: 445,
				'scrolling'			:'no'
			});
			$("a#modal_amigo").fancybox({
				'onClosed'			: function () {
					jQuery('#formIndique').validationEngine('hide');
				},
				'padding'			: 0,
				'transitionIn'		: 'fade',
				'transitionOut'		: 'fade',
				'autoScale'			: false,
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.6,
				'autoDimensions'	: false,
				'width'         	: 860,
				'height'        	: 395,
				'scrolling'			:'no'
			});
			
			<?php if(@$page == 'infraestrutura'): ?>
				$(window).load(function() {
					$('#slider').nivoSlider({
						captionOpacity: 0.5
					});
				});
				
				$("a#example2").fancybox({
					'padding'			: 20,
					'transitionIn'		: 'elastic',
					'transitionOut'		: 'elastic',
					'autoScale'			: false,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6
				});
			<?php endif; ?>
			
			<?php if(@$page == 'apartamentos'): ?>
			
				$("a#example2").fancybox({
					'padding'			: 20,
					'transitionIn'		: 'elastic',
					'transitionOut'		: 'elastic',
					'autoScale'			: false,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6
				});
			
				$("#abas-dorm3").click(function () {
					$("#box-dinamico-2").fadeOut();
					$("#box-dinamico-3").fadeOut();
					$("#box-dinamico-1").fadeIn();
					
					$("#abas-dorm1").removeClass("impla-on");
					$("#abas-dorm1").addClass("impla");
					$("#abas-dorm2").removeClass("dorm2-on");
					$("#abas-dorm2").addClass("dorm2");
					
					$("#abas-dorm3").removeClass("dorm3");
					$("#abas-dorm3").addClass("dorm3-on");
					
					$("#conteudo-apart").height("1100");
					
				});
				
				$("#abas-dorm2").click(function () {
					$("#box-dinamico-1").fadeOut();
					$("#box-dinamico-3").fadeOut();
					$("#box-dinamico-2").fadeIn();
					
					$("#abas-dorm1").removeClass("impla-on");
					$("#abas-dorm1").addClass("impla");
					$("#abas-dorm3").removeClass("dorm3-on");
					$("#abas-dorm3").addClass("dorm3");
					
					
					$("#abas-dorm2").removeClass("dorm2");
					$("#abas-dorm2").addClass("dorm2-on");
					
					$("#conteudo-apart").height("700");
				});
				
				$("#abas-dorm1").click(function () {
					$("#box-dinamico-1").fadeOut();
					$("#box-dinamico-2").fadeOut();
					$("#box-dinamico-3").fadeIn(function () {
						$("#img_03").imageLens({ imageSrc: "<?=base_url()?>assets/img/site/fotos/nexgroup-hotsite-saintlouis-implatacao-01.jpg", lensSize: 350, borderSize: 5, borderColor: "#944536" });
					});
					
					$("#abas-dorm2").removeClass("dorm2-on");
					$("#abas-dorm2").addClass("dorm2");
					$("#abas-dorm3").removeClass("dorm3-on");
					$("#abas-dorm3").addClass("dorm3");
					
					$("#abas-dorm1").removeClass("impla");
					$("#abas-dorm1").addClass("impla-on");
					
					
					$("#conteudo-apart").height("1550");
				});
			
			<?php endif; ?>
			
		});
	</script> 
       
	<script type="text/javascript">
		  window.___gcfg = {lang: 'pt-BR'};
		
		  (function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/plusone.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
	</script>
	
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-42815744-1']);
	_gaq.push(['_trackPageview']);
	(function()
	{ var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
	)();
	</script>

	<!--script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-1622695-53']);
		_gaq.push(['_trackPageview']);
		
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();	
	</script-->
    
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=266241050086130";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>