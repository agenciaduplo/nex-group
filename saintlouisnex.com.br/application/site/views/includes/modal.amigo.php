<!-- modal amigo -->
<div style="display:none">  
    <div id="modal-amigo">
        <div id="conteudo-contato">
            <h2>INDIQUE PARA UM AMIGO</h2>
            <div id="modal-line1"></div>
            <div id="modal-line2"></div>	
            <form action="" name="formIndique" id="formIndique" method="post">
            <ul class="interesse-left">       
				<li> 
                    <label for="Nome">NOME*</label>
                    <input id="indiqueNome" name="indiqueNome" type="text" class="validate[required] form1" />
                </li>
				<li> 
                    <label for="EMAIL">EMAIL*</label>
                    <input id="indiqueEmail" name="indiqueEmail" type="text" class="validate[required, custom[email]] form1" />
                </li>
				<li> 
                    <label for="NOME DO AMIGO">NOME DO AMIGO*</label>
                    <input id="indiqueNomeAmigo" name="indiqueNomeAmigo" type="text" class="validate[required] form1" />
                </li>
				<li> 
                    <label for="EMAIL DO AMIGO">EMAIL DO AMIGO*</label>
                    <input id="indiqueEmailAmigo" name="indiqueEmailAmigo" type="text" class="validate[required, custom[email]] form1" />
                </li>
                <p class="interesse-obrigado">* Campos obrigatórios</p>
            </ul>
            <ul class="interesse-right">       
				<li> 
                    <label for="COMENTÁRIO">COMENTÁRIO</label>
                    <textarea name="comentarioAmigo" id="comentarioAmigo" type="text" class="form4"></textarea>
                </li>
            </ul>
            </form>
            
            <div id="mensagem-sucesso-indique"> 
            	<p id="mensagem-sucesso-indique-show">Indicação enviada com sucesso!</p>
            </div>
            <div id="btn-enviar"> 
                <a id="btnEnviarAmigo" href="javascript: enviarIndique();">enviar</a>
            </div>
        </div>
    </div>
</div>

<!-- modal amigo -->