<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function promocao_galeria()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/promocao_galeria', $data);
	}
	
	function apartamentos()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "apartamentos"
		);
		
		$this->load->view('home/apartamentos', $data);
	}
	
	function infraestrutura()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "infraestrutura"
		);
		
		$this->load->view('home/infraestrutura', $data);
	}
	
	function localizacao()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/localizacao', $data);
	}
	
	function promocao()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/promocao', $data);
	}
	
	function promocao_termos ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/promocao_termos', $data);	
	}
	
	function enviar_pergunta ()
	{
		$this->load->model('home_model', 'model');
		
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		
		$frase 				= $this->input->post('frase');
		$nome 				= $this->input->post('nome');
		$email 				= $this->input->post('email');
		$cidade 			= $this->input->post('cidade');
		$bairro 			= $this->input->post('bairro');
		$como_soube 		= $this->input->post('como_soube');
		$regulamento 		= $this->input->post('regulamento');
		
		$data = array (
			'data_envio'			=> $data_envio,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			
			'frase'					=> $frase,
			'nome'					=> $nome,
			'email'					=> $email,
			'cidade'				=> $cidade,
			'bairro'				=> $bairro,
			'como_soube'			=> $como_soube,
			'regulamento'			=> $regulamento

		);
		
		$promocao	= $this->model->setPromocao($data);
		
		
		$dataSite = array (
			'enviou'					=> "S"
		);
		
		$this->load->view('home/promocao', $dataSite);
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$id_empreendimento		= 61;
		$id_estado				= 21;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$telefone 				= $this->input->post('telefone');
		$faixa_etaria 			= $this->input->post('faixa_etaria');
		$estado_civil 			= $this->input->post('estado_civil');
		$profissao 				= $this->input->post('profissao');
		$forma_contato 			= $this->input->post('contato');
		$comentarios 			= $this->input->post('comentarios');
		$bairro_cidade 			= $this->input->post('bairro_cidade');
		$hotsite				= "S";
		
		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'				=> $id_estado,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome'					=> $nome,
			'email'					=> $email,
			'telefone'				=> $telefone,
			'faixa_etaria'			=> $faixa_etaria,
			'estado_civil'			=> $estado_civil,
			'profissao'				=> $profissao,
			'forma_contato'			=> $forma_contato,
			'comentarios'			=> $comentarios,
			'bairro_cidade'			=> $bairro_cidade,
			'hotsite'				=> $hotsite
		);
		
		$interesse 			= $this->model->setInteresse($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//inicia o envio do e-mail
		//====================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		ob_start();
		
		?>
			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
				 			<td align="center" colspan="2">
				 				<a target="_blank" href="http://www.nexgroup.com.br">
				 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
				 				</a>
				 			</td>
						 </tr>
						<tr>
							<td colspan='2'>Interesse enviado em <?php echo $data_envio; ?> via HotSite</td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
		    				<td colspan="2">Interesse enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  			</tr>
						  <tr>
						    <td width="30%">&nbsp;</td>
						    <td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
						    <td width="30%">Empreendimento:</td>
						    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
						    <td width="30%">IP:</td>
						    <td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
						    <td width="30%">E-mail:</td>
						    <td><strong><?=@$email?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Faixa Etária:</td>
						    <td><strong><?=@$faixa_etaria?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Estado Civil:</td>
						    <td><strong><?=@$estado_civil?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Bairro e Cidade:</td>
						    <td><strong><?=@$bairro_cidade?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Profissão:</td>
						    <td><strong><?=@$profissao?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Telefone:</td>
						    <td><strong><?=@$telefone?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Forma de Contato:</td>
						    <td><strong><?=@$contato?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Comentários:</td>
						    <td valign="top"><strong><?=@$comentarios?></strong></td>
						  </tr>						  					  					  
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>
						  <tr>
					 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						  </tr>
					</table>
				</body>
			</html>
		<?php
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		//====================================================================
		//termina o envio do e-mail
		
		$id_tipo_form 	= 1;
		$emails 		= $this->model->getEnviaEmail($id_empreendimento, $id_tipo_form);
		
		foreach ($emails as $email)
		{
			if($email->tipo === 'para')
			{
				$list[] = $email->email;
			}
			else if($email->tipo === 'cc')
			{
				$list_cc[] = $email->email;
			}
			else
			{
				$list_bcc[] = $email->email;
			}
		}
		
		$list_bcc[] = 'atendimento@divex.com.br';	
		$this->email->from("noreply@saintlouisnex.com.br", "Nex Group");
		 
		if($nome=='teste123')
		{
			$this->email->to('bruno.freiberger@divex.com.br');
		}
		else
		{
		 	if(@$list)
		 	{
				$this->email->to($list);
			}
			if(@$list_cc)
			{
				$this->email->cc($list_cc);
			}
			if(@$list_bcc)
			{
				$this->email->bcc($list_bcc);
			}	
		}
		
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->subject('Interesse enviado via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
	}
	
	function enviarIndique ()
	{
		$this->load->model('home_model', 'model');
	
		$id_empreendimento 		= 61;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome_remetente 		= $this->input->post('nome');
		$email_remetente 		= $this->input->post('email');
		$nome_destinatario 		= $this->input->post('nome_amigo');
		$email_destinatario		= $this->input->post('email_amigo');
		$comentarios 			= $this->input->post('comentarios');

		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome_remetente'		=> $nome_remetente,
			'email_remetente'		=> $email_remetente,
			'nome_destinatario'		=> $nome_destinatario,
			'email_destinatario'	=> $email_destinatario,
			'comentarios'			=> $comentarios
		);
		
		$indique 			= $this->model->setIndique($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>SAINT LOUIS</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>SAINT LOUIS</b> :: Indicação via HotSite</td>
			  </tr>
			  <tr>
			    <td colspan="2">Indicação enviada por <strong><?=$nome_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>			  
			  <tr>
			    <td colspan="2">Olá <?=@$nome_destinatario?>, acesse o HotSite do Saint Louis<br/><strong><a href="http://www.saintlouisnex.com.br">Clique aqui</a></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">Nome / E-mail do remetente:</td>
			    <td><strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.capa.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		$this->email->from("noreply@saintlouisnex.com.br", "$nome_remetente");
		
		if($nome_remetente=="teste123")
		{
			$list = array('bruno.freiberger@divex.com.br');
		} else 
		{
			$list = array(
				"$email_amigo"
			);	
		}
		$this->email->to($list);
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->subject('SAINT LOUIS - Indicação via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email	

	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */