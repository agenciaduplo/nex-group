<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index(){
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function promocao_galeria(){
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/promocao_galeria', $data);
	}
	
	function apartamentos(){
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "apartamentos"
		);
		
		$this->load->view('home/apartamentos', $data);
	}
	
	function infraestrutura(){
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "infraestrutura"
		);
		
		$this->load->view('home/infraestrutura', $data);
	}
	
	function localizacao()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/localizacao', $data);
	}
	
	function promocao()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/promocao', $data);
	}
	
	function promocao_termos ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/promocao_termos', $data);	
	}
	
	function enviar_pergunta (){
		$this->load->model('home_model', 'model');
		
		$ip 					= $this->input->ip_address();
		$user_agent		= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem				= $_SESSION['origem'];
		$data_envio		= date("Y-m-d H:i:s");
		
		$frase 					= $this->input->post('frase');
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$cidade 				= $this->input->post('cidade');
		$bairro 				= $this->input->post('bairro');
		$como_soube 		= $this->input->post('como_soube');
		$regulamento 		= $this->input->post('regulamento');
		
		$data = array (
			'data_envio'			=> $data_envio,
			'ip'							=> $ip,
			'user_agent'			=> $user_agent,
			'url'							=> $url,
			'origem'					=> $origem,
			
			'frase'						=> $frase,
			'nome'						=> $nome,
			'email'						=> $email,
			'cidade'					=> $cidade,
			'bairro'					=> $bairro,
			'como_soube'			=> $como_soube,
			'regulamento'			=> $regulamento

		);
		
		$promocao	= $this->model->setPromocao($data);
		
		
		$dataSite = array (
			'enviou'					=> "S"
		);
		
		$this->load->view('home/promocao', $dataSite);
	}
	
	function enviarInteresse(){
		$this->load->model('home_model', 'model');

		$id_empreendimento = 61;

		$url = $_SESSION['url'];
		$url = explode("__",$url);

		$utm_source = $url[0];
		$utm_medium = $url[1];
		$utm_campaign = $url[3];

		$email_nex 	= "vendas@nexvendas.com.br";
		$email_out 	= "vendas@nexvendas.com.br";
		$email_go	= $email_nex;
		$id_empresa = $this->model->getIdEmpresa($id_empreendimento);
		$id_empresa = $id_empresa[0]->id_empresa;
		$id_cidade 	= $this->model->getIdCidade($id_empreendimento);
		$id_cidade 	= $id_cidade[0]->id_cidade;


		if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
			$email_for =  $this->model->verificaLastEmailSendFor(1);
			foreach($email_for as $emf){
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$email_go = $email_out;
					break;
				}else{
					$email_go = $email_nex;
					break;
				}
			}
		}else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)){ // Porto Alegre | 1 quarto dos interesses para fora.
			$email_for =  $this->model->verificaLastEmailSendFor(3);
			$i 				 = 0;
			$flag 		 = false;

			foreach($email_for as $emf){
				++$i;
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$flag = true;
				}else if($emf->email_enviado_for == $email_out && $i = 1){
					$email_go = $email_nex;
					break;
				}else{
					$flag = false;
				}

				if($flag){
					$email_go = $email_out;
				}else{
					$email_go = $email_nex;
				}
			}
		}else{
			$email_go = $email_nex;
		}

		$id_empreendimento		= $id_empreendimento;
		$id_estado						= 21;
		$ip 									= $this->input->ip_address();
		$user_agent						= $this->input->user_agent();
		$url									= $_SESSION['url'];
		$origem								= $_SESSION['origem'];
		$data_envio						= date("Y-m-d H:i:s");
		$nome 								= $this->input->post('nome');
		$email 								= $this->input->post('email');
		$telefone 						= $this->input->post('telefone');
		$faixa_etaria 				= $this->input->post('faixa_etaria');
		$estado_civil 				= $this->input->post('estado_civil');
		$profissao 						= $this->input->post('profissao');
		$forma_contato 				= $this->input->post('contato');
		$comentarios 					= $this->input->post('comentarios');
		$bairro_cidade 				= $this->input->post('bairro_cidade');
		$hotsite							= "S";


		$p = array(
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'						=> $id_estado,
			'ip'									=> $ip,
			'user_agent'					=> $user_agent,
			'url'									=> $url,
			'origem'							=> $origem,
			'data_envio'					=> $data_envio,
			'nome'								=> $nome,
			'email'								=> $email,
			'telefone'						=> $telefone,
			'faixa_etaria'				=> $faixa_etaria,
			'estado_civil'				=> $estado_civil,
			'profissao'						=> $profissao,
			'forma_contato'				=> $forma_contato,
			'comentarios'					=> $comentarios,
			'bairro_cidade'				=> $bairro_cidade,
			'hotsite'							=> $hotsite,
			'email_enviado_for' 	=> $email_go
		);

		$interesse 			= $this->model->setInteresse($p);
		$empreendimento	= $this->model->getEmpreendimento($id_empreendimento);

		$dados = array(
			'Enviado em ' => $p['data_envio'] . ' via HotSite',
			'Enviado por ' => $p['nome'],
			'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
			'IP' => $p['ip'],
			'E-mail' => $p['email'],
			'Telefone' => $p['telefone'],
			('Comentários') => $p['comentarios']
		);

		//inicia o envio do e-mail
		//====================================================================

		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		/* envia email via roleta */
		$id_empreendimento = 61;
		$grupos = $this->model->getGrupos($id_empreendimento);

		$total = 0;
		$total = count($grupos);

		foreach ($grupos as $row):

			if($row->rand == 1):
				$atual = $row->ordem;

				if($atual == $total):

					$this->model->updateRand($id_empreendimento, '001');

					$emails = $this->model->getGruposEmails($id_empreendimento, '001');
					//echo "<pre>";print_r($emails);echo "</pre>";

					$emails_txt = "";
					foreach ($emails as $email)
					{
						$grupo = $email->grupo;
						$list[] = $email->email;

						$emails_txt = $emails_txt.$email->email.", ";
					}
					$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

				else:

					$atualizar = "00".$atual+1;
					$this->model->updateRand($id_empreendimento, $atualizar);

					$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
					//echo "<pre>";print_r($emails);echo "</pre>";

					$emails_txt = "";
					foreach ($emails as $email)
					{
						$grupo = $email->grupo;
						$list[] = $email->email;

						$emails_txt = $emails_txt.$email->email.", ";
					}
					$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

				endif;
			endif;
		endforeach;

		if($email_go == $email_out){
      $list = $email_go;
    }else{
      $list[] = $email_go;
    }
		/* envia email via roleta */
	
		$this->email->from("noreply@saintlouisnex.com.br", "SAINT LOUIS - Nex Group");
		$this->email->to($list);
		$this->email->subject('Contato enviado via HotSite');
		$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));

		$this->email->send();
	}

	function enviarIndique ()
	{
		$this->load->model('home_model', 'model');
	
		$id_empreendimento 		= 61;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome_remetente 		= $this->input->post('nome');
		$email_remetente 		= $this->input->post('email');
		$nome_destinatario 		= $this->input->post('nome_amigo');
		$email_destinatario		= $this->input->post('email_amigo');
		$comentarios 			= $this->input->post('comentarios');

		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome_remetente'		=> $nome_remetente,
			'email_remetente'		=> $email_remetente,
			'nome_destinatario'		=> $nome_destinatario,
			'email_destinatario'	=> $email_destinatario,
			'comentarios'			=> $comentarios
		);
		
		$indique 			= $this->model->setIndique($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>SAINT LOUIS</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>SAINT LOUIS</b> :: Indicação via HotSite</td>
			  </tr>
			  <tr>
			    <td>Indicação enviada por:</td>
			  	<td> <strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td>Para:</td>
			  	<td> <strong><?=@$nome_destinatario?> / <?=@$email_destinatario?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>			  
			  <tr>
			    <td colspan="2">Olá <?=@$nome_destinatario?>, acesse o HotSite do Saint Louis<br/><strong><a href="http://www.saintlouisnex.com.br">Clique aqui</a></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">Nome / E-mail do remetente:</td>
			    <td><strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.capa.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		//roleta
		$grupos = $this->model->getGrupos($id_empreendimento);
		$total = 0;
		$total = count($grupos);
		
		if($total == 1) // caso só tenho um grupo cadastrado
		{
			$emails = $this->model->getGruposEmails($id_empreendimento, '001');
			
			$emails_txt = "";
			foreach ($emails as $email)
			{
				$grupo = $email->grupo;
				$list[] = $email->email;

				$emails_txt = $emails_txt.$email->email.", ";
			}
			$this->model->grupo_indique($indique, $grupo, $emails_txt);

		}
		else if($total > 1) // caso tenha mais de um grupo cadastrado
		{
			foreach ($grupos as $row):

				if($row->rand == 1):
					$atual = $row->ordem;
					if($atual == $total):

						$this->model->updateRand($id_empreendimento, '001');
						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						
						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);
					
					else:

						$atualizar = "00".$atual+1;
						$this->model->updateRand($id_empreendimento, $atualizar);
						$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);

					endif;	
				endif;
			endforeach;
		}
		/* envia email via roleta */
		
		
		$this->email->from("noreply@saintlouisnex.com.br", "$nome_remetente");
		$this->email->to($email_destinatario);	
		$this->email->subject("Saint Louis - Indicação enviada para você");
		$this->email->message("$conteudo");
		$this->email->send();

		//envia para corretores
		$this->email->from("noreply@saintlouisnex.com.br", "$nome_remetente");
		$this->email->to($list);	
		$this->email->subject("Saint Louis - Indique enviado via site");
		$this->email->message("$conteudo");
		$this->email->send();
		//envia para corretores
		
		//===================================================================
		//Termina o envio do email	

	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */