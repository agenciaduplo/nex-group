<!DOCTYPE html>

<html lang="pt-br">



<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  

<!--[if IE 8]>     <html class="ie8"> <![endif]-->  

<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->



<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='Life Park Condomínio Clube' />
    <meta property='og:image' content='http://www.lifeparknex.com.br/assets/img/logo.png'/>
    <meta property='og:description' content='Junto ao futuro ParkShoppingCanoas - Life Park Condomínio Clube - Nex Group'/>
    <meta property='og:url' content='http://www.lifeparknex.com.br'/>


	<title>Life Park Condomínio Clube - Junto ao futuro ParkShoppingCanoas</title>

	<!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /> -->



    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">



    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome.css">

    <!--[if IE 7]>

        <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome-ie7.css">

    <![endif]-->

    

	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>

    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>

    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>

    <script src="<?=base_url()?>assets/js/init.js"></script>

    

    <!-- SUPERSIZED -->

    <link rel="stylesheet" href="<?=base_url()?>assets/css/supersized.core.css" type="text/css" media="screen" />

    <script type="text/javascript" src="<?=base_url()?>assets/js/supersized.core.3.2.1.min.js"></script>





    

    <script type="text/javascript">

        

        jQuery(function($){

            

            $.supersized({

                slides  :  	[ {image : '<?=base_url()?>assets/img/bg.jpg', title : 'Image Credit: Maria Kazvan'} ]

            });

        });

        

    </script>



	<script type="text/javascript" src="<?=base_url()?>assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

	<script type="text/javascript" src="<?=base_url()?>assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

	<script type="text/javascript">

		$(document).ready(function() {	

			

			$("a[rel=example_group]").fancybox({

				'showNavArrows'			: true,

				'padding'			: 0,

				'transitionIn'		: 'fade',

				'transitionOut'		: 'fade',

				'overlayColor'		: '#000',

				'overlayOpacity'	: 0.1,

				'width'             : 'auto',

				'height'            : 'auto',

				'autoScale'         : true,

				'easingIn'          : 'swing',

				'easingOut'         : 'swing',

				//'titlePosition' 	: 'outside',

				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {

					return '<span id="fancybox-title-over"> ' + '<span class="ablu"> ' + (title.length ? ' &nbsp; ' +  title : '') + '</span> ' + '<span class="ablu2"> ' + (currentIndex + 1) + ' de ' + currentArray.length + '</span> ' + '</span>';

				}

				});

				

			$(".various1").fancybox({

				'titlePosition'		: 'inside',

				'padding'			: 0,

				'overlayColor'		: '#000',

				'overlayOpacity'	: 0.6,

				'transitionIn'		: 'none',

				'transitionOut'		: 'none'

			});

		});

	</script>



    <script>

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



      ga('create', 'UA-1622695-73', 'lifeparknex.com.br');

      ga('send', 'pageview');



    </script> 



</head>    



<body> 

<!-- HEADER -->

	<header>

    	<div class="flutua">

            <div class="container_12">

                <h3>LANÇAMENTO</h4>

                <h4>Junto ao futuro ParkShoppingCanoas</h3>

            </div>

        </div>

    	<div class="top">

   	    	<img src="<?=base_url()?>assets/img/barra.png" > 

        </div>

        <div class="midle">

            <div class="container_12">

                <h1>Life Park Condomínio Clube</h1>

				<form id="FormInteresse">

                	<h2>Cadastre-se para maiores informações.</h2>

                   	<label>Nome:</label>

                   	<label>Email:</label>

                   	<label>Telefone:</label>

                    <input class="field radius1 validate[required]" name="nome" id="nome" placeholder="" type="text">

                    <input class="field validate[required, custom[email]]" name="email" id="email" placeholder="" type="text">

                    <input class="field validate[required]" name="telefone" id="telefone" placeholder="" type="text">

                    

                    <a class="radius2 smoth" href="javascript:Enviar();">ENVIAR</a>

                   

                    

                        <div id="MsgSucesso" style="display: none;">

                            <div id="sucesso" class="sucesso">

                                <h4>SUA MENSAGEM FOI</h4>

                                <h3><b>ENVIADA COM SUCESSO</b></h3>

                                <p class="mod-p">Obrigado! Em breve estraremos em contato!</p>

                            </div>

                        </div>                    

                </form>



            </div>

        </div>

        <div class="bottom">

            <ul class="container_12">

             	<li class="curtir smoth">
                	<!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style" style="margin-top: 27px; margin-left: 16px;">
                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                        </div>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-513de0b25d7bcf95"></script>
                    <!-- AddThis Button END -->
                </li>   

             	<li class="venha"><i class="btn icon-map-marker"></i><p>Venha conhecer em primeira mão o empreendimento<br>que vai transformar Canoas.</p></li>   

             	<a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=19&referencia=<?=@$_SESSION['url']?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online" class="corretor">
                	fale com um<br><span>corretor<br>online</span></a>   
             	<a href="http://www.nexgroup.com.br/" title="Acesse o site da Nex Group" class="nex">Nex Group</a>   

            </ul>

        </div>

    </header>



     <a id="simulaClick" href="#sucesso">&nbsp;</a>

    <!-- FOOTER -->

	<footer>

     	   

    </footer>
    
    	<!-- Código do Google para tag de remarketing -->
	<!--------------------------------------------------
	As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 978900397;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/978900397/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>


</body>
</html>