<div>
	<style type="text/css">
		.alert{padding:8px 35px 8px 14px;margin-bottom:18px;text-shadow:0 1px 0 rgba(255, 255, 255, 0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;color:#c09853;}
		.alert-error{background-color:#f2dede;border-color:#eed3d7;color:#b94a48;}
		.alert-block{padding-top:14px;padding-bottom:14px;}
		.alert-block>p,.alert-block>ul{margin-bottom:0;}
		.alert-block p+p{margin-top:5px;}
	</style>
	<div class="alert alert-error">
		<h5>PHP</h5>
		<p>
			Gravidade: <?php echo $severity; ?><br />
			Mensagem:  <?php echo $message; ?><br />
			Arquivo: <?php echo $filepath; ?><br />
			Linha: <?php echo $line; ?>
		</p>
	</div>
</div>