function Enviar ()
{
	$("#FormInteresse").submit();
}

$(document).ready(function() {

	jQuery("#FormInteresse").validationEngine(
		'attach', {
			InvalidFields: [],
		    onFieldSuccess: false,
		    onFieldFailure: false,
		    onFormSuccess: false,
		    onFormFailure: false,
		    addSuccessCssClassToField: 'inputbox-success',
		    addFailureCssClassToField: 'inputbox-problem',
		    scroll: false,
			onValidationComplete: function(form, status)
			{
				if(status == true)
				{
					//$("#submitContato").attr("disabled","disabled");
					
					var nome			= $("#nome").val();
					var email			= $("#email").val();
					var telefone		= $("#telefone").val();
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&email='+ email
								  +'&telefone='+ telefone
								  ;
								  
					var pathname = window.location.pathname;			  
								  
					base_url  	= pathname+"index.php/contato/setInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								limpaCampos('#FormInteresse');
								
								alert('Cadastro realizado com sucesso!');

								//$("#simulaClick").click();
								//$('.btn-fechar').delay(1500).fadeIn(500);
								//$('#MsgSucesso').delay(200).fadeIn(500);
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			} 
		}
	);
	
});

function limpaCampos (form)
{
    $(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'select':
            case 'email':
            	$(this).val('');
            case 'tel':
            	$(this).val('');
            case 'text':
            	$(this).val('');
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                $('#ipt-contact-phone').checked = true;
                $('#ipt-cliente-nao').checked = true;
        }
    });
}