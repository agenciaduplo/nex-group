
$(document).ready(function() {


    var navigations = $('#ancora');
    pos = navigations.offset();
    var navigations2 = $('#ancora2');
    pos2 = navigations2.offset();

    $(window).scroll(function() {
        if ($(this).scrollTop() > pos.top + navigations.height()) {
            $('#menu_plantas').removeClass('absoluto');
            $('#menu_plantas').addClass('fixo fadeInDown animated_a');
        } else if ($(this).scrollTop() <= pos.top) {
            $('#menu_plantas').addClass('absoluto');
            $('#menu_plantas').removeClass('fixo fadeInDown animated_a');
        }
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > pos2.top + navigations2.height()) {
            $('#menu_plantas').removeClass('fixo fadeInDown animated_a');
            $('#menu_plantas').addClass('absoluto');
        }
    });



    //MostraPlantas1 click add class active remove class active show plantas 1

    $("#MostraPlantas1").click(function() {

        $("#MostraPlantas2").removeClass('active');
        $("#MostraPlantas3").removeClass('active');
        $("#MostraPlantas4").removeClass('active');
        $("#MostraPlantas1").addClass('active');

        $("#Plantas2").hide();
        $("#Plantas3").hide();
        $("#Plantas4").hide();
        $("#Plantas1").show();

    });

    $("#MostraPlantas2").click(function() {

        $("#MostraPlantas1").removeClass('active');
        $("#MostraPlantas3").removeClass('active');
        $("#MostraPlantas4").removeClass('active');
        $("#MostraPlantas2").addClass('active');

        $("#Plantas1").hide();
        $("#Plantas3").hide();
        $("#Plantas4").hide();
        $("#Plantas2").show();

    });

    $("#MostraPlantas3").click(function() {

        $("#MostraPlantas1").removeClass('active');
        $("#MostraPlantas2").removeClass('active');
        $("#MostraPlantas4").removeClass('active');
        $("#MostraPlantas3").addClass('active');

        $("#Plantas1").hide();
        $("#Plantas2").hide();
        $("#Plantas4").hide();
        $("#Plantas3").show();

    });

    $("#MostraPlantas4").click(function() {

        $("#MostraPlantas1").removeClass('active');
        $("#MostraPlantas2").removeClass('active');
        $("#MostraPlantas3").removeClass('active');
        $("#MostraPlantas4").addClass('active');

        $("#Plantas1").hide();
        $("#Plantas2").hide();
        $("#Plantas3").hide();
        $("#Plantas4").show();

    });

    $('#menu').localScroll({duration: 1500, queue: true});

    $('#home').parallax({speed: 0.15});
    $('#entorno').parallax({speed: 0.15});
    $('#externa').parallax({speed: 0.15});
    $('#interna').parallax({speed: 0.15});
    $('#showroom').parallax({speed: 0.15});

    $(".first").click(function() {



        var displayMenu = $("#menu").css('display');

        if (displayMenu == "none")
        {
            $(".first").addClass('active');

            $("#menu").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $(".first").removeClass('active');

            $("#menu").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".informacao").click(function() {

        var displayMenu = $("#informacoes").css('display');

        if (displayMenu == "none")
        {
            $("#informacoes").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#informacoes").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".close-informacoes").click(function() {

        var displayMenu = $("#informacoes").css('display');

        if (displayMenu == "none")
        {
            $("#informacoes").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#informacoes").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".amigo").click(function() {

        var displayMenu = $("#indique_amigo").css('display');

        if (displayMenu == "none")
        {
            $("#indique_amigo").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#indique_amigo").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".close-amigo").click(function() {

        var displayMenu = $("#indique_amigo").css('display');

        if (displayMenu == "none")
        {
            $("#indique_amigo").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#indique_amigo").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $("#abrir_video").click(function() {

        var displayMenu = $("#assista_video").css('display');

        if (displayMenu == "none")
        {
            $("#assista_video").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#assista_video").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".close-video").click(function() {

        var displayMenu = $("#assista_video").css('display');

        if (displayMenu == "none")
        {
            $("#assista_video").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#assista_video").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $("#abrir_entorno").click(function() {

        var displayMenu = $("#open_entorno").css('display');

        if (displayMenu == "none")
        {
            $("#open_entorno").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#open_entorno").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".close-entorno").click(function() {

        var displayMenu = $("#open_entorno").css('display');

        if (displayMenu == "none")
        {
            $("#open_entorno").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#open_entorno").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $("#abrir_mapa").click(function() {

        var displayMenu = $("#open_mapa").css('display');

        if (displayMenu == "none")
        {
            $("#open_mapa").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#open_mapa").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".close-mapa").click(function() {

        var displayMenu = $("#open_mapa").css('display');

        if (displayMenu == "none")
        {
            $("#open_mapa").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#open_mapa").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $("#enviarIndique").click(function() {
        $("#FormIndique").submit();
    });

    $("#enviarInteresse").click(function() {
        $("#FormInteresse").submit();
    });

    $("#FormInteresse").validate({
        rules: {
            nome: "required",
            email: {
                required: true,
                email: true
            },
            fone: "required"

        },
        messages: {
            nome: "Preencha seu nome",
            email: "Preencha um e-mail válido",
            fone: "Preencha o telefone"
        },
        submitHandler: function(form) {
            setInteresse();
        }
    });

    $("#FormIndique").validate({
        rules: {
            nomea: "required",
            emaila: {
                required: true,
                email: true
            },
            nomei: "required",
            emaili: {
                required: true,
                email: true
            },
            msga: "required"

        },
        messages: {
            nomea: "Preencha seu nome",
            emaila: "Preencha um e-mail válido",
            nomei: "Preencha seu nome",
            emaili: "Preencha um e-mail válido",
            msga: "Preencha a mensagem"
        },
        submitHandler: function(form) {
            setIndique();
        }
    });

});


function setInteresse()
{
    //$("#submitContato").attr("disabled","disabled");

    var nome = $("#nome").val();
    var email = $("#email").val();
    var fone = $("#fone").val();
    var msga = $("#msg").val();

    var msg = '';
    vet_dados = 'nome=' + nome
            + '&email=' + email
            + '&fone=' + fone
            + '&msg=' + msga
            ;

    var pathname = window.location.pathname;

    base_url = pathname + "index.php/contato/setInteresse";

    $.ajax({
        type: "POST",
        url: base_url,
        data: vet_dados,
        success: function(msg) {
            limpaCampos('#FormInteresse');

//				alert('Cadastro realizado com sucesso!');

            $(".close-informacoes").click();
            $("#info_sucesso").fadeIn();
            //$('.btn-fechar').delay(1500).fadeIn(500);
            //$('#MsgSucesso').delay(200).fadeIn(500);
        }
    });
    return false;
}

function setIndique()
{
    //$("#submitContato").attr("disabled","disabled");

    var nomea = $("#nomea").val();
    var emaila = $("#emaila").val();
    var nomei = $("#nomei").val();
    var emaili = $("#emaili").val();
    var msga = $("#msga").val();

    var msg = '';
    vet_dados = 'form_nome=' + nomea
            + '&form_email=' + emaila
            + '&form_amigo_nome=' + nomei
            + '&form_amigo_email=' + emaili
            + '&form_mensagem=' + msga
            ;

    var pathname = window.location.pathname;

    base_url = pathname + "index.php/contato/setIndique";

    $.ajax({
        type: "POST",
        url: base_url,
        data: vet_dados,
        success: function(msg) {
            limpaCampos('#FormIndique');

//				alert('Indicação realizado com sucesso!');

            $(".close-amigo").click();
            $("#amigo_sucesso").fadeIn();
            //$('.btn-fechar').delay(1500).fadeIn(500);
            //$('#MsgSucesso').delay(200).fadeIn(500);
        }
    });
    return false;
}

function limpaCampos(form)
{
    $(form).find(':input').each(function() {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'select':
            case 'email':
                $(this).val('');
            case 'tel':
                $(this).val('');
            case 'text':
                $(this).val('');
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                $('#ipt-contact-phone').checked = true;
                $('#ipt-cliente-nao').checked = true;
        }
    });
}



$(".close-info_sucesso").click(function() {

    var displayMenu = $("#info_sucesso").css('display');

    if (displayMenu == "none")
    {
        $("#info_sucesso").animate({
            height: "show", opacity: "toggle"
        }, {duration: "fast"});
    }
    else
    {
        $("#info_sucesso").animate({
            height: "hide", opacity: "toggle"
        }, {duration: "fast"});
    }
});

$(".close-amigo_sucesso").click(function() {

    var displayMenu = $("#amigo_sucesso").css('display');

    if (displayMenu == "none")
    {
        $("#amigo_sucesso").animate({
            height: "show", opacity: "toggle"
        }, {duration: "fast"});
    }
    else
    {
        $("#amigo_sucesso").animate({
            height: "hide", opacity: "toggle"
        }, {duration: "fast"});
    }
});




