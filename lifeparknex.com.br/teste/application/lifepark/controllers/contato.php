<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
	
	public function index(){
		$this->load->view('contato');
	}

	public function setInteresse()
	{
		if($this->input->post('email') == 'shee-y@live.com.pt')
		{
			exit;
		}
		
		if($this->agent->is_robot())
		{
			exit;
		}
		else
		{
	
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() ;
	
			$data = array(
				'id_empreendimento' 	=> 92,
				'id_estado' 			=> 21,
				'ip' 					=> $this->input->ip_address(),
				'user_agent' 			=> $this->input->user_agent(),
				'url' 					=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' 				=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' 			=> date("Y-m-d H:i:s"),
				'nome' 					=> $this->input->post('nome'),
				'email' 				=> strtolower($this->input->post('email')),
				'telefone' 				=> $this->input->post('fone'),
				'comentarios' 			=> nl2br(strip_tags($this->input->post('msg'))),
				'hotsite' 				=> 'S'
			);
	
			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);
	
				$data['empreendimento'] = $empreendimento;
				$html = $this->load->view('tpl/email-interesse',$data,true);
	
				$this->load->library('email',array(
					'mailtype' 	=> 'html',
					'wordwrap' 	=> false,
					'protocol'	=> 'sendmail'
				));
	
				/* envia email via roleta */
				$id_empreendimento = 92;
				$grupos = $this->model->getGrupos($id_empreendimento);
	
				$total = 0;
				$total = count($grupos);
				$total = $total;
				
				foreach ($grupos as $row):
	
					if($row->rand == 1):
						$atual = $row->ordem;
	
						if($atual == $total):
	
							$this->model->updateRand($id_empreendimento, '001');
	
							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
	
							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;
	
								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
	
						else:
	
							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);
	
							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
	
							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;
	
								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
	
						endif;	
					endif;
				endforeach;
				/* envia email via roleta */
	
				if($data['nome'] == "Teste123"){
					$this->email->to('bruno@divex.com.br');
				} else {
					$this->email->to($list);
				}
	
				$this->email->bcc(array('testes@divex.com.br', 'bruno@divex.com.br'));
				$this->email->from("noreply@lifeparknex.com.br", "Life Park - Nex Group");
				$this->email->subject('Interesse enviado via HotSite');
				$this->email->message($this->load->view('tpl/email-interesse',array('dados' => $data),true));
	
				$this->email->send();
	
				$_SESSION['sucesso'] = "Interesse enviado com sucesso!";
				$_SESSION['tracker'] = array(
					'email' 	=> $data['email'],
					'nome' 		=> $data['nome']
				);
			}
		}
	}
	
	public function setIndique()
	{
	
		$this->load->model('contato_model','model');
		
		if($this->input->post())
		{
			$p = array(
				'id_empreendimento' => 92,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome_remetente' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email_remetente' => strtolower($this->input->post('form_email')),
				'nome_destinatario' => (ucwords(strtolower($this->input->post('form_amigo_nome')))),
				'email_destinatario' => strtolower($this->input->post('form_amigo_email')),
				'comentarios' => ($this->input->post('form_mensagem'))
			);
			
			if(!empty($p['nome_remetente']) && !empty($p['nome_destinatario']) && !empty($p['email_destinatario']))
			{
				$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
				$dados = array(
					'Enviado por ' 		=> $p['nome_remetente']." / ".$p['email_remetente'],
					'Para'				=> $p['nome_destinatario']." / ".$p['email_destinatario'],
					'Empreendimento' 	=> $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					('Comentários') 	=> $p['comentarios'],
					'Hotsite'			=> "<a href='http://www.lifeparknex.com.br'>lifeparknex.com.br</a>"
				);

				$this->load->library('email',array(
					'mailtype' 	=> 'html',
					'wordwrap' 	=> false,
					'protocol'	=> 'sendmail'
				));

				$indique = $this->model->setIndique($p);

				/* envia email via roleta */
				$id_empreendimento = 92;
				$grupos = $this->model->getGrupos($id_empreendimento);
				$total = 0;
				$total = count($grupos);
				
				if($total == 1) // caso só tenho um grupo cadastrado
				{
					$emails = $this->model->getGruposEmails($id_empreendimento, '001');
					
					$emails_txt = "";
					foreach ($emails as $email)
					{
						$grupo = $email->grupo;
						$list[] = $email->email;

						$emails_txt = $emails_txt.$email->email.", ";
					}
					$this->model->grupo_indique($indique, $grupo, $emails_txt);

				}
				else if($total > 1) // caso tenha mais de um grupo cadastrado
				{
					foreach ($grupos as $row):

						if($row->rand == 1):
							$atual = $row->ordem;
							if($atual == $total):

								$this->model->updateRand($id_empreendimento, '001');
								$emails = $this->model->getGruposEmails($id_empreendimento, '001');
								
								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_indique($indique, $grupo, $emails_txt);
							
							else:

								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);
								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_indique($indique, $grupo, $emails_txt);

							endif;	
						endif;
					endforeach;
				}
				/* envia email via roleta */

				$this->email->to($p['email_destinatario']);
				$this->email->bcc("testes@divex.com.br");
				$this->email->from("noreply@lifeparknex.com.br", "Nex Group");
				$this->email->subject("Life Park - Indicação enviada para você");
				$this->email->message($this->load->view('tpl/email-indique',array('dados' => $dados),true));
				$this->email->send();

				//envia para corretores
				$this->email->from("noreply@lifeparknex.com.br", "Nex Group");
				$this->email->to("bruno@divex.com.br");
				//$this->email->to($list);	
				$this->email->cc("testes@divex.com.br");
				$this->email->subject("Life Park - Indique enviado via site");
				$this->email->message($this->load->view('tpl/email-indique',array('dados' => $dados),true));
				$this->email->send();
				//envia para corretores

				
			} else {
				//echo '<span class="msg-erro">Preencha os campos requeridos</span>';
			}
		
		} else { show_404(); }
	}
}