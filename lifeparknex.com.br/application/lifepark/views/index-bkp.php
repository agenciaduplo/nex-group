﻿<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='Life Park Condomínio Clube' />
    <meta property='og:image' content='http://www.lifeparknex.com.br/assets/img/logo2.png'/>
    <meta property='og:description' content='Junto ao futuro ParkShoppingCanoas - Life Park Condomínio Clube - Nex Group'/>
    <meta property='og:url' content='http://www.lifeparknex.com.br'/>


	<title>Life Park Condomínio Clube - Junto ao futuro ParkShoppingCanoas</title>
	
	
	<link rel="shortcut icon" href="<?=base_url()?>assets/img/favicon.ico" type="image/x-icon" />
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,900,700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->
    
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	
	<style> a { cursor: pointer; } </style>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-1622695-73', 'auto');
      ga('send', 'pageview');
    </script>
    
    
    
</head>    

<body>
	<!-- SOLICITE INFORRMACOES SUCESSO -->
    <div id="info_sucesso" style="display:none">
        <a class="close-info_sucesso"><i class="icon-remove"></i></a>

        <div class="container_12">
            <h2><i>Sua mensagem foi<span><br>enviada com sucesso!</span></i></h2>
            <p class="sub">Agradecemos seu interesse e em breve entraremos em contato!</span></p>

        </div> 
    </div>      

	<!-- AMIGO SUCESSO -->
    <div id="amigo_sucesso" style="display:none">
        <a class="close-amigo_sucesso"><i class="icon-remove"></i></a>

        <div class="container_12">
            <h2><i>Sua mensagem foi<span><br>enviada com sucesso!</span></i></h2>
            <p class="sub">Agradecemos seu interesse e em breve entraremos em contato!</span></p>

        </div> 
    </div>      


	<!-- SOLICITE INFORRMACOES -->
    <div id="informacoes" style="display:<?=($more_info == '1') ? '' : 'none'?>">
        <a class="close-informacoes"><i class="icon-remove"></i></a>
		<form name="FormInteresse" id="FormInteresse" method="post">

            <div class="container_12">
                <h2><i>SOLICITE MAIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><br>INFORMAÇÕES</span></i></h2>
                <p class="sub">Preencha todos os campos e clique em enviar para solicitar mais informações, <span>obrigado!</span></p>
    			<ul>
                    <div class="tres nomargin">
                        <h3>Nome:</h3>
                        <input class="field" name="nome" id="nome" type="text">
                    </div>
                    <div class="tres">
                        <h3>E-mail:</h3>
                        <input class="field" name="email" id="email" type="text">
                    </div>
                    <div class="tres">
                        <h3>Telefone:</h3>
                        <input class="field" name="fone" id="fone" type="text">
                    </div>
                    <div class="one">
                        <h3>Mensagem:</h3>
                        <textarea class="field" name="msg" id="msg" cols="" rows="" ></textarea>
                    </div>
                </ul>    
            </div> 
            <a id="enviarInteresse" class="button2"><i>clique aqui para <span>enviar sua mensagem</span></i></a>
        </form>
    </div>      



    <!-- INDIQUE PARA UM AMIGO -->
    <div id="indique_amigo" style="display:none">
        <a class="close-amigo"><i class="icon-remove"></i></a>
		<form name="FormIndique" id="FormIndique" method="post">

            <div class="container_12">
                <h2><i>Indique a um&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><br>Amigo</span></i></h2>
                <p class="sub">Preencha todos os campos e clique em enviar para indicar a um amigo, <span>obrigado!</span></p>
    			<ul>
                    <div class="tres nomargin">
                        <h3>Seu Nome:</h3>
                        <input class="field" name="nomea" id="nomea" type="text">
                    </div>
                    <div class="tres">
                        <h3>Seu E-mail:</h3>
                        <input class="field" name="emaila" id="emaila" type="text">
                    </div>
                    <div class="tres">
                        <h3>Nome do Amigo:</h3>
                        <input class="field" name="nomei" id="nomei" type="text">
                    </div>
                    <div class="tres">
                        <h3>E-mail do Amigo:</h3>
                        <input class="field" name="emaili" id="emaili" type="text">
                    </div>
                    <div class="one">
                        <h3>Mensagem:</h3>
                        <textarea class="field" name="msga" id="msga" cols="" rows="" ></textarea>
                    </div>
                </ul>    
            </div> 
            <a id="enviarIndique" class="button2"><i>clique aqui para <span>enviar</span></i></a>
        </form>
    </div>      

    <!-- ASSISTA AO VIDEO -->
    <div id="assista_video" style="display:none">
        <a class="close-video"><i class="icon-remove"></i></a>
            <div class="container_12">
                <h2><i>PERFEITO PARA UMA VIDA DESCOMPLICADA, <span><br>LIFEPARK</span></i></h2>
                <!--<p class="sub">PERFEITO PARA UMAVIDA DESCOMPLICADA, <span>LIFEPARK</span></p>-->
                
            </div> 
            <!-- <iframe width="80%" height="70%" src="http://www.youtube.com/embed/uCdhBXYIKQw?rel=0" frameborder="0" allowfullscreen></iframe> -->
            <iframe width="80%" height="70%" src="https://www.youtube.com/embed/cANRBu7FWiE" frameborder="0" allowfullscreen></iframe>
    </div>      


    <!-- ENTORNO -->
    <div id="open_entorno" style="display:none">
        <a class="close-entorno"><i class="icon-remove"></i></a>
        <div class="container_12">
            <h2><i>TUDO PRA VOCÊ ENCONTRAR <span><br>MAIS TEMPO PRA TUDO.</span></i></h2>
        </div> 
        <img src="<?=base_url()?>assets/img/entorno.jpg">         
        <p class="sub">
        	[1] ParkShopping*<br>    
        	[2] Parque Capão do Corvo<br>
        	[3] Bourbon Canoas<br>
        	[4] BR-116<br>
        	[5] Extensão da Rua Aurora*<br>
        	[6] Estação do aeromóvel*<br>
            [7] Novo túnel ligando o centro de Canoas à BR 448* <br>
            [8] Extensão da Rua Farroupilha* <br>
            [9] Ulbra: 9min <br>
            [10] UniLasalle: 8min <br>
            [11] UniRitter: 11min
      </p>
    </div>

    <!-- MAPA -->
    <div id="open_mapa" style="display:none">
        <a class="close-mapa"><i class="icon-remove"></i></a>
        <div class="container_12">
            <h2><i>Rua <span>Liberdade</span> esquina Rua <span>Irmão Agnelo Chaves</span></i></h2>
            <img src="<?=base_url()?>assets/img/mapa.jpg" >
            <a href="https://goo.gl/maps/bMjXD" target="_blank" style="float: left; width: 100%; text-align: center; font-size: 16px; font-weight: 700; margin: 20px 0 20px 0;">Como chegar?</a>
        </div>             
    </div>
 
    <!-- HEADER -->
	<header>

    	<div id="menu" class="up" style="display:none">
            <div class="container_12">
            	<a href="#home" class="first">
                    <p class="text">Início</p>
                </a>		
            	<a href="#areas" class="first">
                    <p class="text">Áreas de lazer</p>
                </a>		
            	<a href="#apartamentos" class="first">
                    <p class="text">Apartamentos</p>
                </a>		
            	<a href="#showroom" class="first">
                    <p class="text">Showroom</p>
                </a> 
            </div>
        </div>      
    
    	<div class="down">
            <div class="container_12">
            	<a id="abri" class="first "><!-- class="active" -->
                	<i class="awesome icon-reorder"></i><i class="text"><span>Navegue</span><br>Aqui</i>
                </a>		
            	<div class="plantao">
                    <figure></figure>
                    <p class="text">Plantão de Vendas<br><span><a href="tel:51 34633708" style="color: #41606f;">(51) 3463.3708</a></span></p>
                </div>
                
                <div class="right">
                    <a onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=19&referencia=<?=@$_SESSION['url']?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="corretor blue balance bounceInUp animated delay1">
                        <i class="text">fale com um<br><span>corretor<br>online</span></i>
                    </a>		
                    <a class="amigo green balance bounceInUp animated delay12">
                        <i class="text">indique a um<br><span>amigo</span></i>
                    </a>		
                    <a class="informacao orange balance bounceInUp animated delay13">
                        <i class="text">SOLICITE MAIS<br><span>INFORMAÇÕES</span></i>
                    </a>		
                </div>
            </div>
        </div>      
    </header>

    <!-- HOME -->
    <section id="home">
        <div class="red-alert"><p>Lançamento 2ª Fase</p></div> <!-- RED - ALERT -->

        <div class="container_12">
            <div class="left fadeIn animated">
                <a href="<?=site_url()?>" title="Voltar para página inicial">
                	<img src="<?=base_url()?>assets/img/logo.png" > 
                </a>
            </div>
            <div class="right fadeIn animated delay05">
                <!--<p class="lancamento">Lançamento</p>-->
                <div class="curtir">
                	<div id="fb-root"></div>
					<script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like" data-href="http://www.lifeparknex.com.br/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>                
                </div>
                <i class="text clear">
                    PERFEITO PARA UMA<br> 
                    <span>VIDA DESCOMPLICADA</span>
                </i>
                <a id="abrir_video" class="button blue " >
                    <i class="awesome icon-play"></i><i class="text">Assista ao <span>Vídeo</span></i>
                </a>
                <a class="button green" href="http://www.nexgroup.com.br/tourvirtual/life-park/tour/" target="_blank">
                      <i class="awesome icon-undo"></i><i class="text"><span>Tour</span> virtual</i>
                </a>
            </div>
        </div>
        
        <div class="bottom"></div>
    </section>

    <!-- CANOAS -->
    <section id="canoas">
        <div class="container_12">
            <div class="center fadeIn animated delay1">
                <img src="<?=base_url()?>assets/img/balls.jpg" > 
            </div>
            <div class="left">
                <i class="text">
                    CANOAS HOJE,<br> 
                    <span>OLHOS NO AMANHÃ.</span>
                </i>
            </div>
            <div class="right">
                <p class="text">
                    Com investimentos em obras para melhoria da mobilidade urbana e da qualidade de vida, Canoas se transforma numa das melhores cidades pra se viver na região metropolitana de Porto Alegre.<br>
                    <br>

                <span>	
                    ·  3º maior PIB do RS <br>
                    ·  2º polo de ensino do estado <br> 
                    ·  2º município mais populoso do RS
                </span>
                </p>
            </div>
        </div>
    </section>


    <!-- ENTORNO -->
    <section id="entorno">
    	<div class="relative">
            <div class="top"></div>
            <div class="container_12">
                <div class="left">
                    <i class="text">
                        TUDO PRA VOCÊ ENCONTRAR<br> 
                        <span>MAIS TEMPO PRA TUDO.</span>
                    </i>
                    <p class="text2">
                        O entorno do Parque Capão do Corvo é a região que melhor simboliza o futuro de Canoas. Aqui, é possível encontrar desenvolvimento sustentável, modernos conceitos residenciais, mais mobilidade urbana e muita natureza. Aqui vai estar a estação do aeromóvel, bem como o novo túnel que vai ligar o centro de Canoas à BR-448.
                        Todo o entorno foi repensado: vias estão sendo abertas, espaços públicos sendo adotados

                    </p>
                </div>
                <div class="right">
                    <i class="text">
                    	<img src="<?=base_url()?>assets/img/logo-ico.png" > 
                        Ao lado do Parque Capão do Corvo e do futuro shopping Multiplan.
                    </i>
                    <p class="text2">
                        e muitas outras melhorias estão acontecendo para trazer mudanças positivas para você, para o bairro e para a cidade.
                        Não por acaso, esta região foi o local escolhido pela Multiplan para um novo shopping, pela Nex para um novo empreendimento, e por outras grandes empresas nacionais para novos investimentos em qualidade de vida. O futuro só poderia acontecer aqui.
                    </p>
                </div>
                <div class="center">
                    <a id="abrir_entorno" class="button white">
                          <i class="awesome icon-map-marker"></i><i class="text">veja o<span> entorno do empreendimento</span></i>
                    </a>
                </div>
            </div>
            <div class="bottom"></div>
        </div>
    </section>


    <!-- AREAS DE LAZER -->
    <section id="areas">
        <div class="container_12">
            <div class="left">
                <i class="text">
                    conheça as<br> 
                    <span>Áreas de lazer</span>
                </i>
            </div>
            <div class="right">
                <p class="text">
                    LifePark é um lugar surpreendente, que apresenta um conceito inédito de qualidade de vida. Natureza, conveniência e diversão, juntos, na região que mais se desenvolve e se valoriza em Canoas. O resultado dessa combinação é um lugar diferente de tudo que você está acostumado, perfeito para morar, para viver e para investir seu futuro.<br><br>
                </p>
            </div>
            <div class="center">
                <i class="icon-chevron-down "></i>
            </div>
            
        </div>
    </section>

    <!-- AREA EXTERNA -->
    <section id="externa">
    	<div class="relative">
            <div class="top"></div>
            <div class="container_12">
                <div class="left">
                    <i class="text">
                        Áreas de<br> 
                        <span>lazer externas</span>
                    </i>
                </div>
                <div class="right">
                    <a href="<?=base_url()?>assets/img/externas/03.jpg" class="button orange fancybox-thumb" rel="fancybox-thumb" title="Churrasqueira">
                          <i class="awesome icon-picture"></i><i class="text">veja as fotos das <span>áreas de lazer externas</span></i>
                    </a>

                    <!-- FOTOS -->
                    
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/04.jpg" title="Fachada"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/05.jpg" title="Lounge Bar"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/06.jpg" title="Lounge Disco"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/07.jpg" title="Piscina Adulto Infantil"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/08.jpg" title="Piscina Adulto Infantil"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/09.jpg" title="Piscina Adulto Infantil"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/10.jpg" title="Pórtico"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb" href="<?=base_url()?>assets/img/externas/11.jpg" title="Praça"><img src="" alt="" /></a>
                    
                </div>
                <div class="center">
                	<p class="text">
                        • Piscina infantil • Piscina adulto com prainha • Praça de jogos e convívio • Trilha fitness • Churrasqueira gourmet • Quadra poliesportiva • Street ball • Playground • Estar leitura • Fireplace • Lounge Bar • Espaço Zen • Espaço Pet/Agility 
                    </p>
                </div>
            </div>
            <div class="bottom"></div>
        </div>
    </section>


	<div class="space orange"></div>

	<!-- AREA INTERNA -->
    <section id="interna">
    	<div class="relative">
            <div class="top"></div>
            <div class="container_12" >
                <div class="right"> 
                    <a href="<?=base_url()?>assets/img/internas/01.jpg" class="button green fancybox-thumb" rel="fancybox-thumb2" title="Artes Marciais">
                          <i class="awesome icon-picture"></i><i class="text">veja as fotos das <span>áreas de lazer Internas</span></i>
                    </a>
                    <!-- FOTOS -->
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/02.jpg" title="Discoteca"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/03.jpg" title="Enoteca"><img src="" alt="" /></a>
                    
                    
                    <a style="display:none" href="<?=base_url()?>assets/img/externas/01.jpg" class="fancybox-thumb" rel="fancybox-thumb2" title="SKY CLUB - Varanda Fitness"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/externas/02.jpg" title="SKY CLUB - Varanda Espaço gourmet"><img src="" alt="" /></a>
                    
                    
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/04.jpg" title="SKY CLUB - Espaço Gourmet"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/05.jpg" title="SKY CLUB - Fitness"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/06.jpg" title="Home Office"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/07.jpg" title="Jogos Teen"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/08.jpg" title="Pilates"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/09.jpg" title="Salão de Festas Adulto"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/10.jpg" title="Salão de Festas Infantil"><img src="" alt="" /></a>
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb2" href="<?=base_url()?>assets/img/internas/11.jpg" title="Sport Bar"><img src="" alt="" /></a>
                    
                    
                </div>
                <div class="left">
                    <i class="text">
                        Áreas de<br> 
                        <span>lazer Internas</span>
                    </i>
                </div>
                <div class="center">
                	<p class="text">
                		• Salão de festas adulto • Salão de festas infantil • Artes marciais • Sporte bar • Discoteca • Biciletário  • Enoteca • Jogos teen • Brinquedoteca
                		• Home Office • Pilates • Sky Club nas coberturas • Fitness • Espaço Gourmet
                    </p>
                </div>
            </div>
            <div class="bottom"></div>
        </div>
    </section>


	<div class="space2 green"></div>

	<!-- APARTAMENTOS -->
    <section id="apartamentos">
    	<div class="relative">
            <div class="top"></div>
            <div class="container_12" >
                <div id="ancora"></div>
               
               <div class="boca">
                    <div id="menu_plantas" class="menu"> 
                        <!--<a id="MostraPlantas1" >
                            <i class="text"><span>3 dorms</span><br>76<b>m²</b> c/ living estendido</i>
                        </a>-->		
                        <a id="MostraPlantas1" class="active">
                            <i class="text"><span>3 dorms</span><br>76<b>m²</b> privativos</i>
                        </a>		
                        <a id="MostraPlantas2" class="">
                            <i class="text"><span>2 dorms</span><br>60<b>m²</b> privativos</i>
                        </a>		
                        <a id="MostraPlantas3" class="">
                            <i class="text"><span>3 dorms</span><br>76<b>m²</b> c/ living estendido</i>
                        </a>		
                        <a id="MostraPlantas4" class="">
                            <i class="text"><span>2 dorms</span><br>60<b>m²</b> c/ living estendido</i>
                        </a>		
                    </div>
                </div>
                <div id="Plantas1" class="">
                    <h3>
                        <i class="text">
                            <span>3 dorms. com suíte e churrasqueira </span>
                            <br>
                        </i>
                    </h3>
                    <img src="<?=base_url()?>assets/img/plantas/3dorms.jpg" > 
                </div>
                
                <div id="Plantas2" class="" style="display:none;">
                    <h3>
                        <i class="text">
                            <span>2 dorms. com suíte e churrasqueira<br> 
                            </span>
                        </i>
                    </h3>
                    <img src="<?=base_url()?>assets/img/plantas/2dorms.jpg" > 
                </div>
                
                <div id="Plantas3" class="" style="display:none;">
                    <h3>
                        <i class="text">
                            <span>3 dorms. com suíte e churrasqueira<br> 
                            </span>(com living estendido)
                        </i>
                    </h3>
                    <img src="<?=base_url()?>assets/img/plantas/2dorms_estendidos.jpg" > 
                </div>
                
                <div id="Plantas4" class="" style="display:none;">
                    <h3>
                        <i class="text">
                            <span>2 dorms. com suíte e churrasqueira<br> 
                            </span>(com living estendido)
                        </i>
                    </h3>
                    <img src="<?=base_url()?>assets/img/plantas/1dorm_estendido.jpg" > 
                </div>
				<div id="ancora2"></div>

            </div>
        </div>
        
    </section>

    <!-- SHOWROOM -->
    <section id="showroom">
    	<div class="relative">
            <div class="top"></div>
            <div class="container_12">
            
				<div class="uptown">                
                    <div class="left">
                        <i class="text">
                            <span>Showroom de vendas com<br>
                                2 aptos. decorados
                            </span>
                        </i>
                    </div>
                    <div class="right">
                    <a href="<?=base_url()?>assets/img/decorado/01.jpg" class="button blue fancybox-thumb" rel="fancybox-thumb3" title="Living apto 2 dorms">
                            <i class="awesome icon-picture"></i><i class="text">veja as fotos dos <span>Decorados</span></i>
                        </a>
                    <!-- FOTOS -->
                    <a style="display:none" class="fancybox-thumb" rel="fancybox-thumb3" href="<?=base_url()?>assets/img/decorado/02.jpg" title="Living apto 3 doms - opção living estendido"><img src="" alt="" /></a>
                    </div>
                </div>
                    
				<div class="mapa">                
                    <div class="left">
                        <i class="text">
                         	Rua <span>Liberdade</span> esquina com<br>
							Rua <span>Irmão Agnelo Chaves</span>
                        </i>
                    </div>
                    <div class="right">
                        <a id="abrir_mapa" class="button white">	
                            <i class="awesome icon-map-marker"></i><i class="text">veja o <span>Mapa</span></i>
                        </a>
                    </div>
                </div>
                
            </div>
            <div class="bottom"></div>
        </div>
    </section>

    <!-- FOOTER -->
    <section id="footer" >
        <div class="container_12" >
            <div class="center">
                <figure><img class="nex" src="<?=base_url()?>assets/img/nex.png" ></figure>
                <p class="text">
	                Incorporação registrada no R-2 da matrícula 105.206 do Registro de Imóveis de Canoas-RS. Projeto arquitetônico: Ciro Clemik da Costa - CREA 41.124-D. Projeto paisagístico: Takeda Arquitetura e Paisagismo - CREA 79.320. A planta apresentada é ilustrativa e possui sugestão de decoração. Os móveis e utensílios são de dimensões comerciais e não fazem parte do contrato de compra e venda do imóvel. Os acabamentos serão entregues conforme memorial descritivo.
	                Imagens meramente ilustrativas. Incorporação e construção: Nex Group.
                </p>
                <div class="assinatura">
                    <a class="imobi" href="http://imobi.divex.com.br/?utm_source=nex-lifepark&amp;utm_medium=assinatura&amp;utm_campaign=divexass" title="Acesse o site da Divex Imobi">
                        <p>Site por</p>
                        <img class="smoth" src="<?=base_url()?>assets/img/imobi.png"> 
                    </a>
                </div>
            </div>
        </div>
    </section>


	<script type="text/javascript" src="<?=base_url()?>assets/js/background/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/background/jquery.localscroll-1.2.7-min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/background/jquery.scrollTo-1.4.2-min.js"></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/background/animate.js'></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/parallax-plugin.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/init.js"></script>

    
	<script type="text/javascript">        
		$(document).ready(function() {
			$(".fancybox-thumb").fancybox({
				padding	: 0,
				margin	: 30,
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers	: {
					title	: {
						type: 'outside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
			});
		});    
	</script>
</body>
</html>



     <!-- ************ INTERTITIAL ************ 
            
            <div class="modal">
                <a onClick="closePromo();" class="btn-fechar"><p>X</p></a>
                <img src="<?=base_url()?>assets/img/box.png" class="picture">
            </div>
            <div class="black-modal"></div>


            <script>
                $(function(){
                    $('.picture').delay(1500).fadeIn(500);
                });
                function closePromo() {
                    $('.picture').fadeOut('slow');
                    $('.btn-fechar').fadeOut('slow');
                    $('.black-modal').fadeOut('slow');
                }
            </script>
    
            <style>
                .modal { position: fixed; top: 15%; left: 50%; z-index: 1000;  }
                .modal img {position: relative; z-index: 1005; float: left; width: 421px; height: 555px; margin: 0 0 0 -225px  }
                .modal .btn-fechar {position: absolute; z-index: 1006; float: left; width: 50px; height: 50px; background: black; border: 2px solid white; border-radius: 50%; color: white; text-align: center; margin: -20px 0 0 85px; }
                .modal .btn-fechar:hover { background: #333 }
                .modal .btn-fechar p {float: left; width: 100%; text-align: center; padding-top:7px; font-size: 2.3em; font-weight: 700 }
                .black-modal { position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: black; opacity: 0.7; filter: alpha(opacity =70); z-index: 999  }

            </style>
     ************ INTERTITIAL ************ -->
