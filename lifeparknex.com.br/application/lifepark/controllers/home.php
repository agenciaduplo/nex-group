<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index(){
		$more_info = $this->input->get('more_info');
		$data = array('more_info' => $more_info);

		$this->load->view('index', $data);
	}
	public function notfound(){
		show_404();
	}
}