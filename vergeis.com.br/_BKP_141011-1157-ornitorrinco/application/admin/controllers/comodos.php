<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comodos extends Controller {
	
	function Comodos ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('comodos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('comodos_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.vergeis.com.br/configurador/admin.php/comodos/lista/',
			'total_rows'	=> $this->model->numComodos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numComodos(),
    		'comodos'   	=> ($this->input->post('keyword')) ? $this->model->buscaComodos($this->input->post('keyword')) : $this->model->getComodos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('comodos/comodos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('comodos_model', 'model');
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'tipos_plantas'		=> $this->model->getTiposPlantas()
		);

		$this->load->view('comodos/comodos.cadastro.php', $data);
	}
	
	function editar ($id_comodo = 0)
	{
		$this->load->model('comodos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'comodo' 			=> $this->model->getComodo($id_comodo),
				'tipos_plantas'		=> $this->model->getTiposPlantas()
			);
			
			$this->load->view('comodos/comodos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}		
	
	function salvar ()
	{
		$this->load->model('comodos_model', 'model');
		
		try
		{
			$data = array (
				'id_tipo_planta' 	=> $this->input->post('id_tipo_planta'),
				'comodo' 			=> $this->input->post('comodo'),
				'posicao' 			=> $this->input->post('posicao'),
				'data_cadastro' 	=> date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setComodo($data, $this->input->post('id_comodo'));
				
			if ($this->input->post('id_comodo'))
				redirect('comodos/editar/' . $this->input->post('id_comodo'));
			else
				redirect('comodos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_comodo = 0)
	{
		$this->load->model('comodos_model', 'model');

		try
		{
			$this->model->delComodo($id_comodo);
			redirect('comodos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */