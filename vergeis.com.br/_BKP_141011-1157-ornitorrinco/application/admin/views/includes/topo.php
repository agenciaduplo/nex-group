<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<title>Sistema de Gerenciamento de Conteúdo (CMS)</title>
	
	<style type="text/css">
		<!--
			@import url("<?=base_url()?>assets/css/default.css");
			@import url("<?=base_url()?>assets/css/thickbox.css");
		-->
	</style>
	
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.media.js"></script>

	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.maskedinput.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.rsv.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.ui.datepicker.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.pstrength.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.mask.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.thickbox.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/jquery.filestyle.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/tinymce/plugins/tinybrowser/tb_tinymce.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/functions.js"></script>

	<script type="text/javascript">
		$().ready(function(){
			//$("select.nice").niceSelect();

			var nc			= $(window).height() - 284;
			var $conteudo	= $("#conteudo");
			if($conteudo.height() < nc) $conteudo.css({'min-height': nc + 'px'});
			if($.browser.msie && $.browser.version < 7) $conteudo.css({'height': nc + 'px'});
		});
	</script>

</head>

<body>

<div id="topo">
    <div class="centralizacao">
        <div id="logo"><a href="<?=base_url()?>admin.php/usuarios"><h1>Divex</h1></a></div>
        <div id="identificacao">
        	Olá <span><strong style="text-decoration:underline"><?=$this->session->userdata('repont_login_nome');?></strong></span> | <a href="<?=site_url()?>/login/sair" title="sair">sair</a>
        	<br/>Seu IP é <?=$this->input->ip_address();?>
        </div>
        <br class="clr" />
    </div>
</div>
<div id="conceitual"><div id="conceitual-mg"></div></div>
<div id="miolo">
	<div id="menu">
		<div id="menu-mg">
	
			<?=$this->load->view('includes/menu');?>	
	
		</div>
	</div>
	<div id="conteudo">
    	<div id="conteudo-mg">