<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dormitorios_model extends Model {
	
	function Dormitorios_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numDormitorios ()
	{		
		$this->db->select('*')->from('dormitorios')->order_by("quantidade", "asc"); ;
		
		return $this->db->count_all_results();
	}
	
	function getDormitorios ($offset = 0)
	{		
		$this->db->flush_cache();
		$this->db->select('*')->from('dormitorios')->order_by("quantidade", "asc")->limit(20, $offset);
		
		return $this->db->get();
	}
	
	function buscaDormitorios ($keyword)
	{		
		$this->db->select('*')->from('dormitorios')->like('quantidade', $keyword)->order_by("quantidade", "asc");
		
		return $this->db->get();
	}
	
	function getDormitorio ($id_dormitorio)
	{		
		$where = array ('id_dormitorio' => $id_dormitorio);
		
		$this->db->start_cache();
		$this->db->select('*')->from('dormitorios')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setDormitorio ($data, $id_dormitorio = "")
	{		
		if ($id_dormitorio)
		{
			$where = array ('id_dormitorio' => $id_dormitorio);
			$this->db->select('*')->from('dormitorios')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Dormitorio salvo com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_dormitorio', $id_dormitorio);
                $this->db->update('dormitorios');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "dormitorios";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('dormitorios');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "dormitorios";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 			
			
			return $this->db->insert_id();
		}
	}
	
	function delDormitorio ($id_dormitorio)
	{		
		$where = array ('id_dormitorio' => $id_dormitorio);
		$this->db->select('*')->from('dormitorios')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_dormitorio', $id_dormitorio);
            $this->db->delete('dormitorios');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "dormitorios";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 	            
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */