<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comodos_model extends Model {
	
	function Comodos_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numComodos ()
	{		
		$this->db->select('*')->from('comodos');		
		
		return $this->db->count_all_results();
	}
	
	function getComodos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM comodos c, tipos_plantas tp, dormitorios d
				WHERE c.id_tipo_planta = tp.id_tipo_planta
				AND tp.id_dormitorio = d.id_dormitorio
				ORDER BY tp.id_dormitorio, tp.tipo ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getTiposPlantas()
	{
		$this->db->flush_cache();			
		
		$sql = "SELECT *
				FROM tipos_plantas tp, dormitorios d
				WHERE tp.id_dormitorio = d.id_dormitorio
				ORDER BY d.quantidade, tp.tipo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function buscaComodos ($keyword)
	{	
		$sql = "SELECT *
				FROM comodos c, tipos_plantas tp
				WHERE c.id_tipo_planta = tp.id_tipo_planta
				AND c.comodo LIKE '%$keyword%'
				OR tp.tipo LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}	
	
	function getComodo ($id_comodo)
	{		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM comodos c, tipos_plantas tp
				WHERE c.id_tipo_planta = tp.id_tipo_planta
				AND c.id_comodo = $id_comodo
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function setComodo ($data, $id_comodo = "")
	{		
		if ($id_comodo)
		{
			$where = array ('id_comodo' => $id_comodo);
			$this->db->select('*')->from('comodos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->db->set($data);
                $this->db->where('id_comodo', $id_comodo);
                $this->db->update('comodos');
                
                $this->session->set_flashdata('resposta', 'Comodo atualizado com sucesso!');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "comodos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 	                
		     }
		}
		else
		{
			$this->db->set($data)->insert('comodos');
	        
			//Log Acesso
	        	$acao 		= "insert";
	        	$tabela 	= "comodos";
	        	$sql 		= $this->db->last_query();
	        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso 				
			
			return $this->db->insert_id();	        	
	    	
			$this->session->set_flashdata('resposta', 'Comodo inserido com sucesso!');
		}
	}
	
	function delComodo ($id_comodo)
	{			
		
	    $this->db->where('id_comodo', $id_comodo);
	    $this->db->delete('comodos');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "comodos";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}					
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */