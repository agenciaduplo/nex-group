
	<div class="Main">

        <div class="Projeto">        
			
			<?php if($id_tipo_planta == 10): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/3dorm-tipoA/planta.png" alt="" /></div>
			<?php endif; ?>	 	
			
			<?php foreach ($comodos as $row): ?>
				
				<?php if($id_tipo_planta == 10): ?>
			   	
			   	<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowCozinha->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?=@$display?> <?php //if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->	
	
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowLiving->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?=@$display?> <?php //if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
			   	
			   	<!-- Quartos Solteiro -->
				<?php if($row->comodo == "Dorm Solt"): ?>
	
					<?php
						$quartossol = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartossol): ?>
						<div id="DormSolt">
							<div id="quartoSolPai">
							<?php $contQuartoSol = 0; ?>
							<?php foreach ($quartossol as $rowQuartoSol): ?>
								<?php $contQuartoSol++; ?>
								
								<?php $totalQuartosSol = count($quartossol); ?>
								
								<?php if($contQuartoSol == $totalQuartosSol) : ?>
									<input type="hidden" id="formQuartoSolt" name="quartosol" value="<?=$rowQuartoSol->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuartoSol->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="quartosol-<?=$rowQuartoSol->id_variacao_comodo?>" class="DormSolt0<?=$contQuartoSol?> <?=@$display?> <?php //if($contQuartoSol == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoSol->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosSol > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoSolt');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Quartos Solteiro-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos Solteiro -->
			   	
			   	<!-- Anexo -->
				<?php if($row->comodo == "Anexo"): ?>
	
					<?php
						$anexo = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($anexo): ?>
						<div id="Anexo">
							<div id="anexoPai">
							<?php $contAnexo = 0; ?>
							<?php foreach ($anexo as $rowAnexo): ?>
								<?php $contAnexo++; ?>
								
								<?php $totalAnexo = count($anexo); ?>
								
								<?php if($contAnexo == $totalAnexo) : ?>
									<input type="hidden" id="formAnexo" name="anexo" value="<?=$rowAnexo->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowAnexo->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="anexo-<?=$rowAnexo->id_variacao_comodo?>" class="Anexo0<?=$contAnexo?> <?=@$display?> <?php //if($contAnexo == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowAnexo->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalAnexo > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesAnexo');" class="btnEditar" id="Anexo3D" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- Anexo -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Anexo -->
				
				<!-- Quartos -->
				<?php if($row->comodo == "Dorm Casal"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuarto->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?=@$display?> <?php //if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos -->
			   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowBanheiro->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
								?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?=@$display?> <?php //if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
		   	
		   	<?php endforeach; ?>
		
        </div><!--fecha Projeto-->

		<div id="ListaOpcoesComodos">

			<div id="OpcoesCozinhas" class="DisplayNone">
			<?php
				if($cozinhas): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Cozinhas</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($cozinhas as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="cozinhas<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesLiving" class="DisplayNone">
			<?php
				if($livings): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Livings</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($livings as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="livings<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuarto" class="DisplayNone">
			<?php
				if($quartos): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quartos</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartos as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
					
			        	<li id="quartos<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoA" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto A</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartoa as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartoa<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartosA) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoA('<?=$row->id_variacao_comodo?>', 'quartoa<?=$cont?>');" title="Quarto A"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesAnexo" class="DisplayNone">
			<?php
				if($anexo): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Anexos</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($anexo as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="anexo<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalAnexo) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaAnexo('<?=$row->id_variacao_comodo?>', 'anexo<?=$cont?>');" title="Anexo"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoSolt" class="DisplayNone">
			<?php
				if($quartossol): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quartos Solt.</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartossol as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartosol<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartosSol) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoSol('<?=$row->id_variacao_comodo?>', 'quartosol<?=$cont?>');" title="Quarto Solteiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesBanheiro" class="DisplayNone">
			<?php
				if($banheiros): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Banheiros</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($banheiros as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="banheiros<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
	    
	    </div>

    </div><!--fecha Main-->	