	<div class="Main">

        <div class="Projeto">        
			
			<?php if($id_tipo_planta == 9): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/4dorm-tipoA/planta.png" alt="" /></div>
			<?php endif; ?>	 	
			
			<?php foreach ($comodos as $row): ?>
				
				<?php if($id_tipo_planta == 9): ?>
				
				<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
			   	
			   	<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodos($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->	
	
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
				
				<!-- Quartos -->
				<?php if($row->comodo == "Dorm Casal"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos -->
			   	
			   	<!-- Quarto A -->
				<?php if($row->comodo == "Dorm A"): ?>
	
					<?php
						$quartoa = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartoa): ?>
						<div id="DormA">
							<div id="quartoAPai">
							<?php $contQuartoA = 0; ?>
							<?php foreach ($quartoa as $rowQuartoA): ?>
								<?php $contQuartoA++; ?>
								
								<?php $totalQuartosA = count($quartoa); ?>
								
								<?php if($contQuartoA == $totalQuartosA) : ?>
									<input type="hidden" id="formQuartoA" name="quartoa" value="<?=$rowQuartoA->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartoa-<?=$rowQuartoA->id_variacao_comodo?>" class="DormA-0<?=$contQuartoA?> <?php if($contQuartoA == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoA->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosA > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoA');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto A -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto A -->
			   	
			   	<!-- Quarto B -->
				<?php if($row->comodo == "Dorm B"): ?>
	
					<?php
						$quartob = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartob): ?>
						<div id="DormB">
							<div id="quartoBPai">
							<?php $contQuartoB = 0; ?>
							<?php foreach ($quartob as $rowQuartoB): ?>
								<?php $contQuartoB++; ?>
								
								<?php $totalQuartosB = count($quartob); ?>
								
								<?php if($contQuartoB == $totalQuartosB) : ?>
									<input type="hidden" id="formQuartoB" name="quartob" value="<?=$rowQuartoB->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartob-<?=$rowQuartoB->id_variacao_comodo?>" class="DormB-0<?=$contQuartoB?> <?php if($contQuartoB == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoB->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosB > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoB');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto B -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto B -->
			   	
			   	<!-- Quarto C -->
				<?php if($row->comodo == "Dorm C"): ?>
	
					<?php
						$quartoc = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartoc): ?>
						<div id="DormC">
							<div id="quartoCPai">
							<?php $contQuartoC = 0; ?>
							<?php foreach ($quartoc as $rowQuartoC): ?>
								<?php $contQuartoC++; ?>
								
								<?php $totalQuartosC = count($quartoc); ?>
								
								<?php if($contQuartoC == $totalQuartosC) : ?>
									<input type="hidden" id="formQuartoC" name="quartoc" value="<?=$rowQuartoC->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartoc-<?=$rowQuartoC->id_variacao_comodo?>" class="DormC-0<?=$contQuartoC?> <?php if($contQuartoC == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoC->imagem?>" alt=""  />
				                    
				                    <?php if($totalQuartosC > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoC');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto C -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto C -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
		   	
		   	<?php endforeach; ?>
		
        </div><!--fecha Projeto-->

		<div id="ListaOpcoesComodos">

			<div id="OpcoesCozinhas" class="DisplayNone">
			<?php
				if($cozinhas): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Cozinhas</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($cozinhas as $row): $cont++; ?>
			        	<li id="cozinhas<?=$cont?>" <?php if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesLiving" class="DisplayNone">
			<?php
				if($livings): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Livings</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($livings as $row): $cont++; ?>
			        	<li id="livings<?=$cont?>" <?php if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuarto" class="DisplayNone">
			<?php
				if($quartos): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quartos</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartos as $row): $cont++; ?>
			        	<li id="quartos<?=$cont?>" <?php if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoA" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto A</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartoa as $row): $cont++; ?>
			        	<li id="quartoa<?=$cont?>" <?php if($cont == $totalQuartosA) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoA('<?=$row->id_variacao_comodo?>', 'quartoa<?=$cont?>');" title="Quarto A"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoB" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto B</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartob as $row): $cont++; ?>
			        	<li id="quartob<?=$cont?>" <?php if($cont == $totalQuartosB) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoB('<?=$row->id_variacao_comodo?>', 'quartob<?=$cont?>');" title="Quarto A"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoC" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto C</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartoc as $row): $cont++; ?>
			        	<li id="quartoc<?=$cont?>" <?php if($cont == $totalQuartosC) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoC('<?=$row->id_variacao_comodo?>', 'quartoc<?=$cont?>');" title="Quarto C"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesBanheiro" class="DisplayNone">
			<?php
				if($banheiros): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Banheiros</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($banheiros as $row): $cont++; ?>
			        	<li id="banheiros<?=$cont?>" <?php if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
	    
	    </div>

    </div><!--fecha Main-->	