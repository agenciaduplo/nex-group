	<div class="Main">

        <div class="Projeto">
			
			
			<?php if($id_tipo_planta == 8): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/2dorm-tipoA/planta.png" alt="" /></div>
			<input type="hidden" name="hiddenTipo" id="hiddenTipo" value="A" />
			<?php endif; ?>
			<?php if($id_tipo_planta == 13): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/2dorm-tipoB/planta.png" alt="" /></div>
			<input type="hidden" name="hiddenTipo" id="hiddenTipo" value="B" />
			<?php endif; ?>
			<?php if($id_tipo_planta == 11): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/2dorm-tipoC/planta.png" alt="" /></div>
			<input type="hidden" name="hiddenTipo" id="hiddenTipo" value="C" />
			<?php endif; ?>	 
			<?php if($id_tipo_planta == 12): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/2dorm-tipoD/planta.png" alt="" /></div>
			<input type="hidden" name="hiddenTipo" id="hiddenTipo" value="D" />
			<?php endif; ?>	 	 	 	
			
			<?php if($comodos): ?>
			<?php foreach ($comodos as $row): ?>
				
				<!-- 2 DORM TIPO A -->
			   	<!-- ======================================================================================= -->
			   	<!-- ======================================================================================= -->
				<?php if($id_tipo_planta == 8): ?>
	
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
			   	
			   	<!-- Jantar -->
				<?php if($row->comodo == "Jantar"): ?>
	
					<?php
						$jantar = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($jantar): ?>
						<div id="Jantar">
							<div id="jantarPai">
							<?php $contJantar = 0; ?>
							<?php foreach ($jantar as $rowJantar): ?>
								<?php $contJantar++; ?>
								
								<?php $totalJantar = count($jantar); ?>
								
								<?php if($contJantar == $totalJantar) : ?>
									<input type="hidden" id="formJantar" name="jantar" value="<?=$rowJantar->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="jantar-<?=$rowJantar->id_variacao_comodo?>" class="Jantar0<?=$contJantar?> <?php if($contJantar == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowJantar->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalJantar > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesJantar');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Jantar-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Jantar -->
	
				
				<!-- Quartos -->
				<?php if($row->comodo == "Dorm Casal"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos -->
			   	
			   	<!-- Quartos Solteiro -->
				<?php if($row->comodo == "Dorm Solt"): ?>
	
					<?php
						$quartossol = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartossol): ?>
						<div id="DormSolt">
							<div id="quartoSolPai">
							<?php $contQuartoSol = 0; ?>
							<?php foreach ($quartossol as $rowQuartoSol): ?>
								<?php $contQuartoSol++; ?>
								
								<?php $totalQuartosSol = count($quartossol); ?>
								
								<?php if($contQuartoSol == $totalQuartosSol) : ?>
									<input type="hidden" id="formQuartoSol" name="quartosol" value="<?=$rowQuartoSol->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartosol-<?=$rowQuartoSol->id_variacao_comodo?>" class="DormSolt0<?=$contQuartoSol?> <?php if($contQuartoSol == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoSol->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosSol > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoSolt');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Quartos Solteiro-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos Solteiro -->

			   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->			   	
			   	
			   	<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
			   	
			   	<!-- 2 DORM TIPO B -->
			   	<!-- ======================================================================================= -->
			   	<!-- ======================================================================================= -->
			   	<?php if($id_tipo_planta == 13): ?>
				
				<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodos($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />

				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->
			   	
			   	<!-- Jantar -->
				<?php if($row->comodo == "Jantar"): ?>
	
					<?php
						$jantar = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($jantar): ?>
						<div id="Jantar">
							<div id="jantarPai">
							<?php $contJantar = 0; ?>
							<?php foreach ($jantar as $rowJantar): ?>
								<?php $contJantar++; ?>
								
								<?php $totalJantar = count($jantar); ?>
								
								<?php if($contJantar == $totalJantar) : ?>
									<input type="hidden" id="formJantar" name="jantar" value="<?=$rowJantar->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="jantar-<?=$rowJantar->id_variacao_comodo?>" class="Jantar0<?=$contJantar?> <?php if($contJantar == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowJantar->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalJantar > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesJantar');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Jantar-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Jantar -->
				
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								 <?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                   
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
			   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
				
				<!-- Dorm Casal -->
				<?php if($row->comodo == "Dorm Casal2"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal2">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Dorm Casal -->
			   	
			   	<!-- Quartos Solteiro -->
				<?php if($row->comodo == "Dorm Solt"): ?>
	
					<?php
						$quartossol = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartossol): ?>
						<div id="DormSolt">
							<div id="quartoSolPai">
							<?php $contQuartoSol = 0; ?>
							<?php foreach ($quartossol as $rowQuartoSol): ?>
								<?php $contQuartoSol++; ?>
								
								<?php $totalQuartosSol = count($quartossol); ?>
								
								<?php if($contQuartoSol == $totalQuartosSol) : ?>
									<input type="hidden" id="formQuartoSol" name="quartosol" value="<?=$rowQuartoSol->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartosol-<?=$rowQuartoSol->id_variacao_comodo?>" class="DormSolt0<?=$contQuartoSol?> <?php if($contQuartoSol == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoSol->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosSol > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoSolt');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Quartos Solteiro-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos Solteiro -->
			   	
			   	<!-- Dorm Casal 2 -->
				<?php if($row->comodo == "Dorm Casal1"): ?>
					
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal1">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuartoCasal2" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Dorm Casal 2 -->

			   	
				<?php endif; ?> <!-- if($id_tipo_planta.. = 13 ) -->
			   	
			   	<!-- 2 DORM TIPO C -->
			   	<!-- ======================================================================================= -->
			   	<!-- ======================================================================================= -->
			   	<?php if($id_tipo_planta == 11): ?>
				
				<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->
			   	
			   	<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
			   	
			   	<!-- Jantar -->
				<?php if($row->comodo == "Jantar"): ?>
	
					<?php
						$jantar = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($jantar): ?>
						<div id="Jantar">
							<div id="jantarPai">
							<?php $contJantar = 0; ?>
							<?php foreach ($jantar as $rowJantar): ?>
								<?php $contJantar++; ?>
								
								<?php $totalJantar = count($jantar); ?>
								
								<?php if($contJantar == $totalJantar) : ?>
									<input type="hidden" id="formJantar" name="jantar" value="<?=$rowJantar->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="jantar-<?=$rowJantar->id_variacao_comodo?>" class="Jantar0<?=$contJantar?> <?php if($contJantar == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowJantar->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalJantar > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesJantar');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Jantar-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Jantar -->
			   	
			   	<!-- Quartos Solteiro -->
				<?php if($row->comodo == "Dorm Solt"): ?>
	
					<?php
						$quartossol = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartossol): ?>
						<div id="DormSolt">
							<div id="quartoSolPai">
							<?php $contQuartoSol = 0; ?>
							<?php foreach ($quartossol as $rowQuartoSol): ?>
								<?php $contQuartoSol++; ?>
								
								 <?php $totalQuartosSol = count($quartossol); ?>
								
								<?php if($contQuartoSol == $totalQuartosSol) : ?>
									<input type="hidden" id="formQuartoSol" name="quartosol" value="<?=$rowQuartoSol->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartosol-<?=$rowQuartoSol->id_variacao_comodo?>" class="DormSolt0<?=$contQuartoSol?> <?php if($contQuartoSol == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoSol->imagem?>" alt=""  />
				                    
				                   
				                    <?php if($totalQuartosSol > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoSolt');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Quartos Solteiro-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos Solteiro -->
			   				   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
			   	
			   	<!-- Dorm Casal -->
				<?php if($row->comodo == "Dorm Casal"): ?>
					
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								 <?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                   
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Dorm Casal -->

			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. = 11 ) -->
			   	
			   	<!-- 2 DORM TIPO D -->
			   	<!-- ======================================================================================= -->
			   	<!-- ======================================================================================= -->
			   	<?php if($id_tipo_planta == 12): ?>
				
				
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
				
				<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    <?php $totalCozinhas = count($cozinhas); ?>
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->
			   	
			   	<!-- Jantar -->
				<?php if($row->comodo == "Jantar"): ?>
	
					<?php
						$jantar = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
						
					<?php if($jantar): ?>
						<div id="Jantar">
							<div id="jantarPai">
							<?php $contJantar = 0; ?>
							<?php foreach ($jantar as $rowJantar): ?>
								<?php $contJantar++; ?>
								
								<?php $totalJantar = count($jantar); ?>
								
								<?php if($contJantar == $totalJantar) : ?>
									<input type="hidden" id="formJantar" name="jantar" value="<?=$rowJantar->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="jantar-<?=$rowJantar->id_variacao_comodo?>" class="Jantar0<?=$contJantar?> <?php if($contJantar == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowJantar->imagem?>" alt="" />
				                    
				                    <?php if($totalJantar > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesJantar');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Jantar-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Jantar -->
			   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
				
				<!-- Dorm Casal -->
				<?php if($row->comodo == "Dorm Casal"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } else echo "None";  ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Dorm Casal -->
			   	
			   	<!-- Quartos Solteiro -->
				<?php if($row->comodo == "Dorm Solt"): ?>
	
					<?php
						$quartossol = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartossol): ?>
						<div id="DormSolt">
							<div id="quartoSolPai">
							<?php $contQuartoSol = 0; ?>
							<?php foreach ($quartossol as $rowQuartoSol): ?>
								<?php $contQuartoSol++; ?>
								
								<?php $totalQuartosSol = count($quartossol); ?>	
								
								<?php if($contQuartoSol == $totalQuartosSol) : ?>
									<input type="hidden" id="formQuartoSol" name="quartosol" value="<?=$rowQuartoSol->id_variacao_comodo?>" />
								<?php endif; ?>
				                
				                <div id="quartosol-<?=$rowQuartoSol->id_variacao_comodo?>" class="DormSolt0<?=$contQuartoSol?> <?php if($contQuartoSol == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoSol->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosSol > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoSolt');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Quartos Solteiro-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos Solteiro -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. = 12 ) -->
			   	
		   	
		   	<?php endforeach; ?>
		   	
		   	<?php endif; ?>
		
        </div><!--fecha Projeto-->

		<div id="ListaOpcoesComodos">

		<div id="OpcoesCozinhas" class="DisplayNone">
		<?php
			if($cozinhas): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Cozinhas</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($cozinhas as $row): $cont++; ?>
		        	<li id="cozinhas<?=$cont?>" <?php if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesLiving" class="DisplayNone">
		<?php
			if($livings): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Livings</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($livings as $row): $cont++; ?>
		        	<li id="livings<?=$cont?>" <?php if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesQuarto" class="DisplayNone">
		<?php
			if($quartos): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Quartos</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($quartos as $row): $cont++; ?>
		        	<li id="quartos<?=$cont?>" <?php if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesQuartoSolt" class="DisplayNone">
		<?php
			if($quartossol): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Quartos Solt.</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($quartossol as $row): $cont++; ?>
		        	<li id="quartosol<?=$cont?>" <?php if($cont == $totalQuartosSol) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoSol('<?=$row->id_variacao_comodo?>', 'quartosol<?=$cont?>');" title="Quarto Solteiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesBanheiro" class="DisplayNone">
		<?php
			if($banheiros): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Banheiros</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($banheiros as $row): $cont++; ?>
		        	<li id="banheiros<?=$cont?>" <?php if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesJantar" class="DisplayNone">
		<?php
			if($jantar): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Jantar</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($jantar as $row): $cont++; ?>
		        	<li id="jantar<?=$cont?>" <?php if($cont == $totalJantar) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaJantar('<?=$row->id_variacao_comodo?>', 'jantar<?=$cont?>');" title="Jantar"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>	    
	    </div>
	    
	    </div>

    </div><!--fecha Main-->	