<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
    <title>Vergeis</title>
    
	<meta name="description" content="Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas - Vergeis" />
	<meta name="keywords" content="vergeis, apartamento, apto, porto alegre, poa, rs, rio grande do sul, estúdio, 2 dormitórios, 3 dormitórios, 4 dormitórios, coberturas" />
	<meta name="author" content="Divex Tecnologia" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="revisit-after" content="1 days" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
     
    <!-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/master.css" />
	<link rel="stylesheet" type="text/css" media="print" href="<?=base_url()?>assets/css/site/master.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	
	<!--
	<script src="<?=base_url()?>assets/js/site/jquery.alerts.js" type="text/javascript"></script>
	<link href="<?=base_url()?>assets/css/site/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="<?=base_url()?>assets/js/site/jquery.ui.draggable.js" type="text/javascript"></script>
	-->
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<!--[if IE 6]>
		<script src="<?=base_url()?>assets/js/site/DD_belatedPNG_0.0.8a-min.js"></script>
		<script>
			
			DD_belatedPNG.fix('h1, .AcessePng, .NavPrincipal, .NavTermos, .NavEmpreendimento, .NavCapa, .Wrapper, img, .btnEditar');
	
		</script>
	<![endif]--> 
	
	<script type="text/javascript">
		/*
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-52']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
		*/
	</script>

</head>

<?php
	/*
	echo "<pre>";
		print_r($_COOKIE);
	echo "</pre>";
	echo $this->encrypt->decode($this->session->userdata('id_projeto'));
	*/
?>

<body class="<?=@$body?>">
	<div class="Full">

    	<div class="Header">
        	<div class="Container">

            	<h1></h1>

				<ul class="BreadCrumbs" id="Crumbs" style="display:none">
                	<li class="BreadLabel"><span>Planta:</span></li>
                    <li style="display:none" class="BreadDorms"><a id="BreadDorms" href="javascript: tiposDorms();"></a></li>
                    <li style="display:none" class="BreadTipo"><a id="BreadTipo" href="javascript: tiposPlantas();"></a></li>
                </ul>

				<div class="Conta">
				
					<?php if(@$verificaLogin != "0"): ?>
	                    <p>Ol&aacute;, <strong><?=$this->session->userdata('login_nome');?></strong></p>
	                    <ul>
	                        <li><a href="minha-conta.html" title="Minha Conta">Minha Conta</a></li>
	                        <li><a href="<?=site_url()?>/login/sair" title="Sair">Sair</a></li>
	                    </ul>
                   <?php else: ?>
                    	<!--Acesso visitante -->                    
                    
                    	<p>Ol&aacute;, <strong>Visitante</strong></p>
                    	<ul class="AcessePng"><li><a href="<?=site_url()?>" title="Minha Conta">Acesse sua conta</a></li></ul>
                    <?php endif; ?>
                    
                </div><!--fecha Conta-->

            	<ul class="Navegation">
                	<li><a href="<?=site_url()?>" class="NavPrincipal" title="Principal">Principal</a></li>
                    <!-- <li><a href="<?=site_url()?>/home/termos_de_uso" class="NavTermos" title="Termos de uso">Termos de uso</a></li> -->
                    <li><a href="http://www.vergeis.com.br" class="NavEmpreendimento" target="_blank" title="Site do empreendimento">Site do empreendimento</a></li>
                    <li><a href="http://www.capa.com.br" class="NavCapa" target="_blank" title="Site Capa">Site Capa</a></li>
                </ul><!--fecha Navegation-->

            </div><!--fecha Container-->
        </div><!--fecha Header-->