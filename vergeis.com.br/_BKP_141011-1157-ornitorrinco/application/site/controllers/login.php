<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Controller {

	function Login()
	{
		parent::Controller();	
	}
	
	function index ()
	{
		$this->load->view('login/login');
	}
	
	function entrar ()
	{
		$username = $this->input->post('email');
		$password = $this->input->post('senha');
		
		//echo $username;
		//echo $password;
		//exit;
		
		if ($this->auth->try_login($username, $password))
		{
			//$this->load->model('logs_model', 'model');
			$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
			//$this->model->inserirLogAcesso($id_usuario);
			
			redirect('home/cliente');
		}
		else
		{
			redirect('home');
		}
	}
	
	function sair ()
	{
		$this->auth->logout();
		redirect('home');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */