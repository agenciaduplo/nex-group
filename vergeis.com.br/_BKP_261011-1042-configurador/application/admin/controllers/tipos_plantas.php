<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_plantas extends Controller {
	
	function Tipos_plantas ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('tipos_plantas/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('tipos_plantas_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.vergeis.com.br/configurador/admin.php/tipos_plantas/lista/',
			'total_rows'	=> $this->model->numTipos_plantas(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numTipos_plantas(),
    		'tipos_plantas'   	=> ($this->input->post('keyword')) ? $this->model->buscaTipos_plantas($this->input->post('keyword')) : $this->model->getTipos_plantas($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('tipos_plantas/tipos_plantas.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('tipos_plantas_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'dormitorios'	=> $this->model->getDormitorios()
		);

		$this->load->view('tipos_plantas/tipos_plantas.cadastro.php', $data);
	}
	
	function editar ($id_tipo_planta = 0)
	{
		$this->load->model('tipos_plantas_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'tipo_planta' 	=> $this->model->getTipo_planta($id_tipo_planta),
				'dormitorios'	=> $this->model->getDormitorios()
			);
			
			$this->load->view('tipos_plantas/tipos_plantas.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}		
	
	function salvar ()
	{
		$this->load->model('tipos_plantas_model', 'model');
		
		try
		{	
			$data = array (
				'id_dormitorio' 	=> $this->input->post('id_dormitorio'),
				'tipo' 				=> $this->input->post('tipo'),
				'data_cadastro' 	=> date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setTipo_planta($data, $this->input->post('id_tipo_planta'));
				
			if ($this->input->post('id_tipo_planta'))
				redirect('tipos_plantas/editar/' . $this->input->post('id_tipo_planta'));
			else
				redirect('tipos_plantas/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_tipo_planta = 0)
	{
		$this->load->model('tipos_plantas_model', 'model');

		try
		{
			$this->model->delTipo_planta($id_tipo_planta);
			redirect('tipos_plantas/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */