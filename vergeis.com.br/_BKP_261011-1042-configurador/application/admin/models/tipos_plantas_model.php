<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_plantas_model extends Model {
	
	function Tipos_plantas_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numTipos_plantas ()
	{		
		$this->db->select('*')->from('tipos_plantas');		
		
		return $this->db->count_all_results();
	}
	
	function getTipos_plantas ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM tipos_plantas tp, dormitorios d 
				WHERE tp.id_dormitorio = d.id_dormitorio
				ORDER BY d.quantidade, tp.tipo ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getDormitorios ()
	{
		$sql = "SELECT *
				FROM dormitorios
				ORDER BY quantidade ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function buscaTipos_plantas ($keyword)
	{	
		$sql = "SELECT *
				FROM tipos_plantas
				WHERE titulo LIKE '%$keyword%'
				OR conteudo LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}	
	
	function getTipo_planta ($id_tipo_planta)
	{		
		$where = array ('id_tipo_planta' => $id_tipo_planta);
		
		$this->db->start_cache();
		$this->db->select('*')->from('tipos_plantas')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
	
	function setTipo_planta ($data, $id_tipo_planta = "")
	{		
		if ($id_tipo_planta)
		{
			$where = array ('id_tipo_planta' => $id_tipo_planta);
			$this->db->select('*')->from('tipos_plantas')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Tipo_planta salvo com sucesso!');
				
				$this->load->library('image_lib');
				
				//UPLOAD
		        $config['upload_path']		= './assets/uploads/tipos_plantas/';
		        $config['allowed_types']	= 'gif|jpg|png';
		        $config['max_size']			= '15000';
		        $config['max_width']		= '12000';
		        $config['max_height']		= '12000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
					$imagem = $this->model->getImagem($id_tipo_planta);
					if($imagem->imagem):
						unlink('./assets/uploads/tipos_plantas/' . $imagem->imagem);
						unlink('./assets/uploads/tipos_plantas/thumb/' . $imagem->imagem);
					endif;
		        	
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
					
			        // THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './assets/uploads/tipos_plantas/thumb/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 135;
					$config['width']			= 180;
					$config['quality']			= 100;					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					// THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './assets/uploads/tipos_plantas/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 202;
					$config['width']			= 366;
					$config['quality']			= 100;					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();	
																				
							
					$this->db->set($data);
					$this->db->set('imagem', $file_name);
	                $this->db->where('id_tipo_planta', $id_tipo_planta);
	                $this->db->update('tipos_plantas');
	                
	                $this->session->set_flashdata('resposta', 'Tipo_planta salva com sucesso!');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "tipos_plantas";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                
		        }
		        else
		        {
					$this->db->set($data);
	                $this->db->where('id_tipo_planta', $id_tipo_planta);
	                $this->db->update('tipos_plantas');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "tipos_plantas";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                	        	
		        	
					$this->session->set_flashdata('resposta', 'Tipo_planta inserida sem imagem!');
		        }
			}
		}
		else
		{
			$this->load->library('image_lib');
			
			//UPLOAD
	        $config['upload_path']		= './assets/uploads/tipos_plantas/';
	        $config['allowed_types']	= 'gif|jpg|png';
	        $config['max_size']			= '15000';
	        $config['max_width']		= '12000';
	        $config['max_height']		= '12000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('imagem'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        // THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './assets/uploads/tipos_plantas/thumb/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 135;
				$config['width']			= 180;
				$config['quality']			= 100;					
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				
				// THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './assets/uploads/tipos_plantas/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 202;
				$config['width']			= 366;
				$config['quality']			= 100;					
				$this->image_lib->initialize($config);
				$this->image_lib->resize();											
						
				$this->db->set($data)->set('imagem', $file_name)->insert('tipos_plantas');
				
	            //Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "tipos_plantas";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 
		            				
				return $this->db->insert_id();
	        }
	        else
	        {
				$this->db->set($data)->insert('tipos_plantas');
	            
				//Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "tipos_plantas";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 				
				
				return $this->db->insert_id();	        	
	        	
				$this->session->set_flashdata('resposta', 'Tipo de Planta inserida sem imagem!');
	        }
		}
	}
	
	function delTipo_planta ($id_tipo_planta)
	{   				
		$tipo_planta = $this->model->getImagem($id_tipo_planta);
		if($tipo_planta->imagem):
			unlink('./assets/uploads/tipos_plantas/' . $tipo_planta->imagem);
			unlink('./assets/uploads/tipos_plantas/thumb/' . $tipo_planta->imagem);
		endif;			
		
	    $this->db->where('id_tipo_planta', $id_tipo_planta);
	    $this->db->delete('tipos_plantas');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "tipos_plantas";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getImagem ($id_tipo_planta)
	{
		$sql = "SELECT *
				FROM tipos_plantas
				WHERE id_tipo_planta = $id_tipo_planta
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}					
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */