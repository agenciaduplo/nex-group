<?=$this->load->view('includes/topo');?>
<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/tinymce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "simple"
	});
</script>
<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=CONTEUDOS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormConteudos" action="<?=site_url()?>/conteudos/salvar/" method="post">
 					<? if (@$conteudo->id_cms_conteudos): ?>
                        <input type="hidden" name="id_cms_conteudos" value="<?=@$conteudo->id_cms_conteudos?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Título</label>
                        <div><input type="text" name="titulo" class="campo-padrao" id="FormTitulo" value="<?=@$conteudo->titulo?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Seção</label>
	                    <div>
	                    	<select id="FormSecao" name="id_cms_secoes" class="campo-padrao">
	                    		<option value="">Selecione</option>
	                    		<? foreach ($secoes as $row):?>
	                    			<option value="<?=$row->id_cms_secoes?>" <? if(@$row->id_cms_secoes == @$conteudo->id_cms_secoes) { echo "selected='selected'"; } ?>><?=$row->nome?></option>
	                    		<? endforeach; ?>
	                    		
	                    	</select>
	                    </div>                        
                        
                    </div>
                    <br class="clr" />
                    
                   <div class="formulario">
                        <label class="obrigatorio">Conteúdo</label>
                        <div>
                        	<textarea name="conteudo" class="campo-grande" id="FormConteudo" /><?=@$conteudo->conteudo?></textarea>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                    	<label>&nbsp;</label>
                    	<div>&nbsp;</div>
                    </div>
                    <br class="clr" />
                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/conteudos/lista" title="Voltar" class="bt-voltar">voltar para Conteúdos</a>
			<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>