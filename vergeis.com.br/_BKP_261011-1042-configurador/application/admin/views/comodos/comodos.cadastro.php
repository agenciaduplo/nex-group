<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=COMODOS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>                       
                
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                    <form id="FormComodos" action="<?=site_url()?>/comodos/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$comodo->id_comodo): ?>
                        <input type="hidden" name="id_comodo" value="<?=@$comodo->id_comodo	?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Tipo de Planta</label>
                        <div>
                        	<select class="campo-padrao" name="id_tipo_planta" id="FormTipoPlanta">
                        		<option value="">Selecione</option>
                        		<?php foreach ($tipos_plantas as $row): ?>
                        			<option <? if(@$row->id_tipo_planta == @$comodo->id_tipo_planta) echo "selected='selected'"; ?> value="<?=$row->id_tipo_planta?>"><?=$row->quantidade?> - <?=$row->tipo?></option>
                        		<?php endforeach; ?>
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Comodo</label>
                        <div><input type="text" name="comodo" class="campo-padrao" id="FormComodo" value="<?=@$comodo->comodo?>" /></div>
                    </div>
                    <br class="clr" />                                                                          
                    
                    <div class="formulario-colunado">
                        <div class="formulario-colunado">
                        <label class="obrigatorio">Posição</label>
                        <div><input type="text" name="posicao" class="campo-padrao" id="FormPosicao" value="<?=@$comodo->posicao?>" /></div>
                    </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" /> 
                    
                    
                    <br/><br/>
                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-noticia"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/comodos/lista" title="Voltar" class="bt-voltar">voltar para Comodos</a>
			<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>