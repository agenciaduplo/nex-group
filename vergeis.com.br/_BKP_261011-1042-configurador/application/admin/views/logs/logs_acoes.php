<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=LOGS DE ACAO . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($logs):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                    	<th width="15%">TABELA</th>
                    	<th width="15%">AÇÃO</th>
                        <th width="20%">IP</th>
                        <th width="20%">Usuário</th>
                        <th width="20%">Data</th>
                        <th width="10%">Hora</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($logs as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <?php
                        	$data = date('d/m/Y', strtotime($row->data_hora));
                        	$hora = date('H:i:s', strtotime($row->data_hora));
                        ?>
                        <td width="30%"><?=$row->tabela?></td>
                        <td width="30%"><?=$row->acao?></td>                                            
                        <td width="30%"><?=$row->ip?></td>
                        <td width="30%"><?=$row->login?></td>
                        <td width="30%"><?=$data?></td>
						<td width="30%"><?=$hora?></td>
                    </tr>
                    <?endforeach;?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhum Log de Ação encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>