<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=PROJETOS . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <div class="botoes"><a href="<?=site_url()?>/projetos/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/img/bt_incluir.jpg" alt="Inserir" title="Inserir" /></a></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($projetos):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="30%">Nome</th>
                        <th width="30%">E-mail</th>
                        <th width="20%">Telefone</th>
                        <th width="15%">Unidade</th>
                        <th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($projetos as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="30%"><a href="<?=site_url()?>/projetos/editar/<?=$row->id_projeto?>/<?=$row->id_dormitorio?>" title="Editar" class="bt-editar"><?=$row->nome?> <?=$row->sobrenome?></a></td>
                        <td width="30%"><?=$row->email?></td>
                        <td width="30%"><?=$row->telefone?></td>
                        <td width="30%"><?=$row->unidade_comprada?></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/projetos/editar/<?=$row->id_projeto?>/<?=$row->id_dormitorio?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/img/lapis.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/projetos/apagar/<?=$row->id_projeto?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->nome?> <?=$row->sobrenome?>?');"><img src="<?=base_url()?>assets/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach;?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhum Projeto / Usuário encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>