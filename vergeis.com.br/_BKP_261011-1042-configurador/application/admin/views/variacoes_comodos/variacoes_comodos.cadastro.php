<?=$this->load->view('includes/topo');?>
	
	<script type="text/javascript" charset="utf-8">
	  $(function() {
	            
	      $("input.file_1").filestyle({ 
	          image: "<?=base_url()?>assets/img/file.gif",
	          imageheight : 17,
	          imagewidth : 47,
	          width : 150
	      });  
	      
	      $("input.file_2").filestyle({ 
	          image: "<?=base_url()?>assets/img/file.gif",
	          imageheight : 17,
	          imagewidth : 47,
	          width : 150
	      });    
	  });
	</script>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=VARIACOES COMODOS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>                       
                
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                    <form id="FormVariacoesComodos" action="<?=site_url()?>/variacoes_comodos/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$variacao_comodo->id_variacao_comodo): ?>
                        <input type="hidden" name="id_variacao_comodo" value="<?=@$variacao_comodo->id_variacao_comodo	?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Dormitórios</label>
                        <div>
                        	<select class="campo-padrao" name="id_comodo" id="FormComodo">
                        		<option value="">Selecione</option>
                        		<?php foreach ($comodos as $row): ?>
                        			<option <? if(@$row->id_comodo == @$variacao_comodo->id_comodo) echo "selected='selected'"; ?> value="<?=$row->id_comodo?>"><?=$row->quantidade?>-<?=$row->tipo?> <?=$row->comodo?></option>
                        		<?php endforeach; ?>
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Imagem</label>
                        <div><input type="file" name="imagem" class="file_1" id="FormFoto" ></div>
                        <? if(@$variacao_comodo->imagem): ?>
                        	<br/>
                        	<img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$variacao_comodo->imagem?>" width="100px;">
                        <? endif; ?>
                    </div>
                    <br class="clr" /><br/>
                    
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Thumb</label>
                        <div><input type="file" name="thumb" class="file_2" id="FormThumb" ></div>
                        <? if(@$variacao_comodo->thumb): ?>
                        	<br/>
                        	<img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$variacao_comodo->thumb?>" width="80px;">
                        <? endif; ?>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />                                                                           
                    <br/><br/>
                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-noticia"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/variacoes_comodos/lista" title="Voltar" class="bt-voltar">voltar para Tipos de Plantas</a>
			<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>