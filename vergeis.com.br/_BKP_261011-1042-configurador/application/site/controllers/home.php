<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Controller {

	function Home()
	{
		parent::Controller();	
	}
	
	function index ()
	{
		redirect('home/principal');
	}
	
	function principal ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'body'			=> 'Login',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/principal.php', $data);
	}
	
	function principal2 ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'body'			=> 'Login',
			'verificaLogin'	=> $verificaLogin,
			'sim'			=> "s"
		);
		
		$this->load->view('home/principal2.php', $data);
	}
	
	function enviaDados ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		if($_POST)
		{
			$nome			= $this->input->post('nome');
			$email			= $this->input->post('email');
			$url			= $this->input->post('url');
			$origem			= $this->input->post('origem');
			$data_envio		= date("Y-m-d H:i:s");
			
			$verifica 	= $this->model->verificaEmailEnt($email);
			
			$data = array (	
				'origem'		=> $origem,
				'url'			=> $url,
				'nome'			=> $nome,
				'email'			=> $email,
				'data_envio'	=> $data_envio
			);
			
			if(!$verifica)
			{
				$this->model->setEmail($data);
			}
		}
	}
	
	function visitante ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'dormitorios'	=> $this->model->getDormitorios(),
			'body'			=> 'Interna Dormitorios',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/visitante.php', $data);
	}
	
	function termos_de_uso ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'dormitorios'	=> $this->model->getDormitorios(),
			'body'			=> 'Interna TermosDeUso',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/termos-de-uso.php', $data);
	}
	
	function cliente ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		if(@$verificaLogin != "0"):
			
			$data = array (	
				'dormitorios'	=> $this->model->getDormitorios(),
				'body'			=> 'Interna Dormitorios',
				'verificaLogin'	=> $verificaLogin
			);
			$this->load->view('home/visitante.php', $data);
			
		else:
		
			redirect('home/visitante');
			
		endif;
	}
	
	function buscaTiposPlantas ()
	{
		$this->load->model('home_model', 'model');
	
		$id_dormitorio = $this->input->post('id_dormitorio');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'verificaLogin'	=> $verificaLogin,
			'tipos_plantas'	=> $this->model->getTiposPlantas($id_dormitorio)
		);
		
		$this->load->view('home/ajax.tipos.php', $data);
	}
	
	function buscaComodos ()
	{
		$this->load->model('home_model', 'model');
	
		$id_tipo_planta = $this->input->post('id_tipo_planta');
		
		$planta			= $this->model->getPlantasDorms($id_tipo_planta);
		
		$verificaLogin = $this->auth->check();
		
		$data = array (
			'verificaLogin'		=> $verificaLogin,	
			'comodos'			=> $this->model->getComodos($id_tipo_planta),
			'id_tipo_planta'	=> $id_tipo_planta
		);
		
		if(@$planta->id_dormitorio == 1)
		{
			$this->load->view('home/ajax.comodos.1.php', $data);
		}
		elseif(@$planta->id_dormitorio == 2)
		{
			$this->load->view('home/ajax.comodos.2.php', $data);
		}
		elseif(@$planta->id_dormitorio == 3)
		{
			$this->load->view('home/ajax.comodos.3.php', $data);
		}
		elseif(@$planta->id_dormitorio == 4)
		{
			$this->load->view('home/ajax.comodos.4.php', $data);
		}	
	}
	
	function comodos ()
	{
		$this->load->model('home_model', 'model');
	
		$id_tipo_planta = 6;
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'verificaLogin'		=> $verificaLogin,
			'comodos'	=> $this->model->getComodos($id_tipo_planta)
		);
		
		$this->load->view('home/ajax.comodos.php', $data);	
	}
	
	function enviaDadosUm ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		$cozinha			= $this->input->post('cozinha');
		$quarto				= $this->input->post('quarto');
		$living				= $this->input->post('living');
		$banheiro			= $this->input->post('banheiro');
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
			
			$dataProjeto	= date("Y-m-d H:i:s");
		
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		}
		else
		{
			$dataProjeto	= date("Y-m-d H:i:s");
		
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		}
		
		//$this->model->setProjetoVariacao();
		
		$data = array (	
			'cozinha'	=> $this->input->post('cozinha'),
			'quarto'	=> $this->input->post('quarto'),
			'living'	=> $this->input->post('living'),
			'banheiro'	=> $this->input->post('banheiro')
		);
		
		$this->load->view('home/ajax.dados.php', $data);		
	}
	
	function enviaDadosDois ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		$cozinha			= $this->input->post('cozinha');
		$casal				= $this->input->post('casal');
		$living				= $this->input->post('living');
		$banheiro			= $this->input->post('banheiro');
		$casaldois			= $this->input->post('casaldois');
		$jantar				= $this->input->post('jantar');
		$solt				= $this->input->post('solt');
		
		$tipo				= $this->input->post('tipo');
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
		}
		
		$dataProjeto	= date("Y-m-d H:i:s");
			
		if($tipo == "A")
		{
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);			
		}
		elseif($tipo == "B")
		{
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casaldois, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
		}
		elseif($tipo == "C")
		{	
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
		}
		elseif($tipo == "D")
		{
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
		}
		
		//exit;
		
		$this->load->view('home/ajax.dados.php', @$data);		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */