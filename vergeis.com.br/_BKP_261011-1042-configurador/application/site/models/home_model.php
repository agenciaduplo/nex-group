<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends Model {
	
	function Home_model ()
	{
		parent::Model();			
	}
	
	function getConteudo ($secao, $conteudo)
	{		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM cms_conteudos
				WHERE id_cms_conteudos = $conteudo
				AND id_cms_secoes = $secao
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getDormitorios ()
	{		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM dormitorios
				ORDER BY quantidade ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function setEmail ($data)
	{
		$this->db->flush_cache();
		
		$this->db->set($data);
		$this->db->insert('emails_espera');
		
	}
	
	function verificaEmailEnt ($email)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM emails_espera
				WHERE email = '$email'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getTiposPlantas ($id_dormitorio)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM tipos_plantas
				WHERE id_dormitorio = $id_dormitorio
				ORDER BY tipo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function getComodos ($id_tipo_planta)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM comodos
				WHERE id_tipo_planta = $id_tipo_planta
				ORDER BY posicao ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function getPlantasDorms ($id_tipo_planta)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM comodos c, tipos_plantas tp
				WHERE c.id_tipo_planta = $id_tipo_planta
				AND tp.id_tipo_planta = $id_tipo_planta
				AND c.id_tipo_planta = tp.id_tipo_planta
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getVariacoesComodos ($id_comodo)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM variacoes_comodos
				WHERE id_comodo = $id_comodo
				ORDER BY id_variacao_comodo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function getVariacoesComodosDesc ($id_comodo)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM variacoes_comodos
				WHERE id_comodo = $id_comodo
				ORDER BY id_variacao_comodo DESC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function getProjetoVariacoes ($id_projeto)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM projetos_variacoes
				WHERE id_projeto = $id_projeto
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function LimpaProjetosVariacoes ($id_projeto)
	{
		$this->db->flush_cache();	
		
		$sql = "DELETE
				FROM projetos_variacoes
				WHERE id_projeto = $id_projeto
			   ";		
		
		$query = $this->db->query($sql);	
	}
	
	function setProjetoVariacao ($id_projeto, $variacao, $dataProjeto)
	{
		$this->db->set('id_projeto', $id_projeto);
		$this->db->set('id_variacao_comodo', $variacao);
		$this->db->set('data_cadastro', $dataProjeto);
	    $this->db->insert('projetos_variacoes');
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */