<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

	<head>
		
		<title>Vergeis - Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas</title>
		
		<meta name="description" content="Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas - Vergeis" />
		<meta name="keywords" content="vergeis, apartamento, apto, porto alegre, poa, rs, rio grande do sul, estúdio, 2 dormitórios, 3 dormitórios, 4 dormitórios, coberturas" />
		<meta name="author" content="Divex Tecnologia" />
		<meta name="robots" content="index, follow" />
		<meta name="language" content="pt-br" />
		<meta name="revisit-after" content="1 days" />
		<meta name="mssmarttagspreventparsing" content="true" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="google-site-verification" content="mcFVQI0v-vGbNK_1HPmRtoP1C9SqS96AcAz_O_ZZKWM" />
		
		<link href="<?=base_url()?>assets/css/site/style.css" media="screen" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.min.js"></script>	
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
		<script type="text/javascript"> 
            $().ready(function() {
                $("#content").css({
                    'padding-top': ($(window).height() / 2 - 283) + 'px'
                });
            });
        </script> 
		<!-- Analytics -->
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-1622695-51']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>		
		<!-- Analytics -->
		
	</head>
	<body>
		<div id="content">