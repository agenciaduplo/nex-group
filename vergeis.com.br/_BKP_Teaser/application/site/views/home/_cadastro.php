<?=$this->load->view('includes/header');?>

			<div id="cadastro">
				<div class="Box1">
					<div>
						<h2>Tenho interesse</h2>
						<p class="Submetido" style="font-size:11px;">Campos marcados com asterisco(*) s&atilde;o obrigat&oacute;rios.</p>
						<p style="display:none; color:green; font-weight:bold;" id="ConfirmaForm" class="Sucesso">Interesse enviado com sucesso.<br/>Em breve entraremos em contato.</p>
						<ul class="Submetido" id="erro">
							<li id="erro-nome">O Campo Nome &#233; obrigat&#243;rio.</li>
							<li id="erro-email">O Campo Email &#233; obrigat&#243;rio.</li>
							<li id="erro-telefone">O Campo telefone &#233; obrigat&#243;rio.</li>
						</ul>
					</div>
					<a  class="LnkVoltar" href="<?=site_url()?>" title="Voltar para Home" target="_self">Voltar</a>
				</div>
				<div class="Box2">
					<div class="Foo">
						<form class="Submetido" method="post" action="" id="frm-contato" name="Contato">
							<fieldset>
								
								<?php 
								
									@$utm_source 	= $_GET['utm_source'];
									@$utm_medium 	= $_GET['utm_medium'];
									@$utm_content 	= $_GET['utm_content'];
									@$utm_campaign 	= $_GET['utm_campaign'];
									
									@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
									
								?>
								<input type="hidden" name="origem" id="txtOrigem" value="<?=@$_SERVER['HTTP_REFERER']?>" />
								<input type="hidden" name="url"	id="txtUrl" value="<?=@$url?>" />
								
								<legend>Frmulario de Contato</legend>
								<div>
									<label for="ipt-name">Nome*</label>
									<input class="Text validate[required]" id="ipt-name" name="Nome" tabindex="1" type="text" value=""/>
								</div>
								<div>
									<label for="ipt-faixa">Faixa et&aacute;ria</label>
									<select id="ipt-faixa" name="FaixaEtaria">
										<option>At&eacute; 30 anos</option>
										<option>At&eacute; 31 a 40 anos</option>
										<option>At&eacute; 41 a 50 anos</option>
										<option>Mais de 50 anos</option>
									</select>
								</div>
								<div>
									<label for="ipt-estado-civil">Estado civil</label>
									<select id="ipt-estado-civil" name="EstadoCivil" tabindex="1">
										<option>Casado</option>
										<option>Solteiro</option>
									</select>
								</div>
								<div>
									<label for="ipt-bairro-cidade">Bairro e Cidade</label>
									<input class="Text" id="ipt-bairro-cidade" name="BairroCidade" tabindex="1" type="text" value=""/>
								</div>
								<div>
									<label for="ipt-profissao">Profiss&atilde;o</label>
									<input class="Text" id="ipt-profissao" name="Profissao" tabindex="1" type="text" value=""/>
								</div>
								<div>
									<label for="ipt-telefone">Telefone*</label>
									<input class="Text validate[required]" id="ipt-telefone" name="Telefone" tabindex="1" type="text" value=""/>
								</div>
								<div>
									<label for="ipt-email">E-mail*</label>
									<input class="Text validate[required,custom[email]]" id="ipt-email" name="Email" tabindex="1" type="text" value=""/>
								</div>
								<div>
									<label>Forma de Contato</label>
									<div class="Exception">
										<input id="ipt-forma-email" name="Contato" tabindex="1" checked="checked" type="checkbox" value="E-mail"/>
										<label for="ipt-forma-email">E-mail</label>
									</div>
									<div class="Exception">
										<input id="ipt-forma-telefone" name="Contato" tabindex="1" type="checkbox" value="Telefone"/>
										<label  for="ipt-forma-telefone">Telefone</label>
									</div>
								</div>
								<div>
									<label for="ipt-comentario">Coment&aacute;rios</label>
									<textarea id="ipt-comentario" name="comentarios"></textarea>
								</div>
								<button id="Enviar" class="BtnEnviar" name="Enviar" type="submit" value="" >Enviar</button>
							</fieldset>
						</form>
						<div class="Sucesso">
							<strong>Mensagem enviada com sucesso. Em breve entraremos em contato.</strong>
						</div>
					</div>
				</div>
			</div>
			
<?=$this->load->view('includes/footer');?>			