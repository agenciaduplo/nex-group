<?=$this->load->view('includes/header');?>

        		<div class="Sidebar">
                	<address class="adr"><span class="street-address">Av. Presidente Juscelino K. de Oliveira, 3161 esquina com Rafael Pinto Bandeira</span></address>
                    <!--<p>Pr&eacute;dio mais alto de Pelotas com uma  &aacute;rea de lazer com vista 360&ordm; na cobertura.</p>-->
                    <!--<a href="http://www.facebook.com/pages/Pelotas-Terrace/122739217801287" class="FanPageFacebook" target="_blank" title="Acesse a P&aacute;gina do Terrace no Facebook">Acesse a P&aacute;gina do Terrace no Facebook</a>-->
                </div><!--fecha Sidebar-->

            	<div class="Main">
                	<h1>Pelotas Terrace - O ponto alto da sua vida</h1>
                    <h2>3 dormit&oacute;rios (1 su&iacute;te)</h2>
                    <h3>Tenho interesse</h3>

					<form id="FormInteresse" class="Form" method="post" onsubmit="return validaInteresse();">
					<?php 
								
						@$utm_source 	= $_GET['utm_source'];
						@$utm_medium 	= $_GET['utm_medium'];
						@$utm_content 	= $_GET['utm_content'];
						@$utm_campaign 	= $_GET['utm_campaign'];
						
						@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
						
					?>
					<input type="hidden" name="origem" id="txtOrigem" value="<?=@$_SERVER['HTTP_REFERER']?>" />
					<input type="hidden" name="url"	id="txtUrl" value="<?=@$url?>" />
					<ul class="FormularioLeft">
						<li>
							<label for="Nome">nome</label>
							<input type="text" id="Nome" class="CampoPadrao validate[required]" name="Nome" />
						</li>
						<li>
							<label for="FaixaEtaria" class="LabelFaixaEtaria">faixa et&aacute;ria</label>
							<select id="FaixaEtaria" class="CampoPadrao" name="FaixaEtaria">
								<option value="Até 30 anos">At&eacute; 30 anos</option>
								<option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                <option value="Mais de 50 anos">Mais de 50 anos</option>
							</select>
						</li>
						<li>
							<label for="EstadoCivil" class="LabelEstadoCivil">estado civil</label>
							<select id="EstadoCivil" class="CampoPadrao" name="EstadoCivil">
								<option value="Casado">casado</option>
                                <option value="Solteiro">solteiro</option>
							</select>
						</li>
						<li>
							<label for="Cidade" class="LabelBairro">cidade</label>
							<input type="text" id="Cidade" class="CampoPadrao" name="Cidade" />
						</li>
						<li>
							<label for="Profissao">profiss&atilde;o</label>
							<input type="text" id="Profissao" class="CampoPadrao" name="Profissao" />
						</li>
					</ul>
					<ul class="FormularioRight">
						<li class="PL24">
							<label for="Telefone">telefone</label>
							<input type="text" id="Telefone" class="CampoPadrao validate[required]" name="Telefone" />
						</li>
						<li class="PL24">
							<label for="Email">e-mail</label>
							<input type="text" id="Email" class="CampoPadrao validate[required,custom[email]]" name="Email" />
						</li>
						<li class="LiCheck">
							<div class="CheckGroup">
								<span class="LabelOptions">contato por</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkTelefone" name="ChkTelefone" class="Check" value="Telefone" />
									<label for="ChkTelefone" class="Choice">telefone</label>
								</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkEmail" name="ChkEmail" class="Check" value="E-mail" />
									<label for="ChkEmail" class="Choice">e-mail</label>
								</span>
							</div> <!-- .CheckGroup -->
						</li>
						<li class="PL24 Clear">
							<label for="Comentario">coment&aacute;rio</label>
							<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
						</li>
					</ul>
					<div id="ConfirmaForm">
						<div class="SubmitBar">
							<input type="submit" id="Enviar" class="btnEnviar" name="Enviar" value="" title="Enviar" />
							
							<span style="display:none;" id="MensagemErro" class="MensagemErro"><strong>Nome</strong>, <strong>e-mail</strong> e <strong>telefone</strong> são obrigatórios.</span>
						</div>
					</div>
				</form> <!-- .Form -->

                </div><!--fecha Main-->
	
<?=$this->load->view('includes/footer');?>