	jQuery(document).ready(function(){
		jQuery("#frm-contato").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status == true)
				{
					$("#Enviar").attr("disabled","disabled");
				
					var nome			= $("#Nome").val();
					var faixa_etaria	= $("#FaixaEtaria").val();
					var estado_civil	= $("#EstadoCivil").val();
					var bairro_cidade	= $("#Cidade").val();
					var profissao		= $("#Profissao").val();
					var telefone		= $("#Telefone").val();
					var email			= $("#Email").val();
					var comentarios		= $("#Comentario").val();
					
					var origem			= $("#txtOrigem").val();
					var url				= $("#txtUrl").val();
					
					if($('#ChkEmail').is(':checked') && $('#ChkTelefone').is(':checked'))
					{
						var contato = "E-mail e Telefone";
					}
					else if ($('#ChkEmail').is(':checked'))
					{
						var contato = "E-mail";
					}
					else if ($('#ChkTelefone').is(':checked'))
					{
						var contato = "Telefone";
					}
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&faixa_etaria='+ faixa_etaria
								  +'&estado_civil='+ estado_civil
								  +'&bairro_cidade='+ bairro_cidade
								  +'&profissao='+ profissao
								  +'&telefone='+ telefone
								  +'&email='+ email
								  +'&contato='+ contato
								  +'&comentarios='+ comentarios
								  +'&origem='+ origem
								  +'&url='+ url;
								  
					base_url  	= "http://www.vergeis.com.br/index.php/home/enviarInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								limpaCampos();
								$("#Enviar").removeAttr("disabled");
								$("#ConfirmaForm").fadeIn();
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			}  
		})
	});
	
	function limpaCampos ()
	{
	    $("#FormInteresse").find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'select':
	            case 'text':
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}
