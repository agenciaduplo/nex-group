<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	
	<title>Vergéis - apartamentos com diversas opções de plantas</title>
	
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="geo.position" content="INSERIR_LATITUDE;INSERIR_LONGITUDE">
	<meta name="author" content="Divex - www.divex.com.br" />
	<meta name="copyright" content="&copy; 2011 NexGroup" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<!--<meta name="google-site-verification" content="ID_GOOGLE_WEBMASTER_TOOLS" />-->
	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/feature-carousel.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/fancybox.css" type="text/css" media="screen" />
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->
	
	<!-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.featurecarousel.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/common.js"></script>
	<script src="http://connect.facebook.net/pt_BR/all.js#xfbml=1"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

	<!--[if IE 6]>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			DD_belatedPNG.fix('.pngfix, .CampoPadrao, img');
		});
	</script>
	<![endif]-->

	<!--
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'ID_GOOGLE_ANALYTICS']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	-->
</head>

<body class="Home">

	<div class="Main">
		<div class="Header pngfix">
			<div class="Container">
				<h1 class="Logo pngfix">Vergéis - apartamentos com diversas opções de plantas</h1>
				<ul class="Features">
					<li class="First"><a href="#" class="BotaoCorretor pngfix">Corretor online</a></li>
					<!-- 
					<li class="Second">
						<a href="#" class="BotaoPlantao FeatureButton pngfix">Plantão de vendas</a>
						<div id="PainelPlantao" class="SlidingPanel BoxPlantao">
							<span class="BotaoFechar pngfix">Fechar</span>
							<span class="Tel pngfix">51 3378 7800</span>
							<p class="Address">Rua Francisco Petuco, 121<br />Porto Alegre.RS</p>
							<a href="" class="BotaoVerMapa pngfix" rel="external">Ver no mapa</a>
						</div> 
					</li> -->
					<li class="Third">
						<a href="#" class="BotaoInteresse FeatureButton pngfix" title="Tenho interesse">Tenho interesse</a>
						<div id="PainelTenhoInteresse" class="SlidingPanel BoxTenhoInteresse">
							<span class="BotaoFechar pngfix">Fechar</span>
							<form id="FormInteresse" class="Form" method="post" action="">
								<ul class="First">
									<li>
										<label for="Nome">nome</label>
										<input type="text" id="Nome" class="CampoPadrao" name="Nome" />
									</li>
									<li>
										<label for="FaixaEtaria" class="hfixie">faixa etária</label>
										<select id="FaixaEtaria" name="FaixaEtaria">
											<option value=""></option>
										</select>
									</li>
									<li>
										<label for="EstadoCivil" class="hfixie">estado civil</label>
										<select id="EstadoCivil" name="EstadoCivil">
											<option value=""></option>
										</select>
									</li>
									<li>
										<label for="Bairro" class="hfixie">bairro</label>
										<select id="Bairro" name="Bairro">
											<option value=""></option>
										</select>
									</li>
									<li>
										<label for="Profissao">profissão</label>
										<input type="text" id="Profissao" class="CampoPadrao" name="Profissao" />
									</li>
								</ul>
								<ul class="Second">
									<li class="Indent">
										<label for="Telefone">telefone</label>
										<input type="text" id="Telefone" class="CampoPadrao" name="Telefone" />
									</li>
									<li class="Indent">
										<label for="Email">e-mail</label>
										<input type="text" id="Email" class="CampoPadrao" name="Email" />
									</li>
									<li>
										<div class="CheckGroup">
											<span class="LabelOptions">formas de contato</span>
											<span class="CheckOption">
												<input type="checkbox" id="ChkTelefone" name="ChkTelefone" class="Check" value="Telefone" />
												<label for="ChkTelefone" class="Choice">telefone</label>
											</span>
											<span class="CheckOption">
												<input type="checkbox" id="ChkEmail" name="ChkEmail" class="Check" value="E-mail" />
												<label for="ChkEmail" class="Choice">e-mail</label>
											</span>
										</div> <!-- .CheckGroup -->
									</li>
									<li class="Indent">
										<label for="Comentario">comentário</label>
										<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
									</li>
									<li class="Last"><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
								</ul>
							</form> <!-- #FormInteresse -->
						</div> <!-- #PainelTenhoInteresse -->
					</li>
					<li>
						<a href="#" class="BotaoIndique FeatureButton pngfix" title="Indique para um amigo">Indique para um amigo</a>
						<div id="PainelIndique" class="SlidingPanel BoxIndique">
							<span class="BotaoFechar pngfix">Fechar</span>
							<div></div>
							<form id="FormIndique" class="Form" method="post" action="">
								<ul class="First">
									<li>
										<label for="NomeRemetente">nome</label>
										<input type="text" id="NomeRemetente" class="CampoPadrao" name="NomeRemetente" />
									</li>
									<li>
										<label for="EmailRemetente">e-mail</label>
										<input type="text" id="EmailRemetente" class="CampoPadrao" name="EmailRemetente" />
									</li>
								</ul>
								<ul>
									<li>
										<label for="NomeAmigo">nome do amigo</label>
										<input type="text" id="NomeAmigo" class="CampoPadrao" name="NomeAmigo" />
									</li>
									<li>
										<label for="EmailAmigo">e-mail do amigo</label>
										<input type="text" id="EmailAmigo" class="CampoPadrao" name="EmailAmigo" />
									</li>
								</ul>
								<ul>
									<li>
										<label for="Comentario">comentário</label>
										<textarea id="Comentario" class="ComentarioIndique CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
									</li>
									<li class="Last"><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
								</ul>
							</form> <!-- #FormIndique -->
						</div> <!-- #PainelIndique -->
					</li>
				</ul> <!-- .Features -->
				<ul class="Navigation pngfix">
					<li class="ItemHome"><a href="#Home" class="Active pngfix" title="Voltar para home">Home</a></li>
					<li class="ItemApartamentos"><a href="#Apartamentos" class="pngfix" title="Apartamentos">Apartamentos</a></li>
					<li class="ItemInfraestrutura"><a href="#Infraestrutura" class="pngfix" title="Infraestrutura">Infraestrutura</a></li>
					<li class="ItemContato"><a href="#Contato" class="pngfix" title="Contato">Contato</a></li>
					<li class="ItemLocalizacao Last"><a href="#Localizacao" class="pngfix" title="Localização">Localização</a></li>
				</ul> <!-- .Navigation -->
				<p class="LogoCapa pngfix">Capa Engenharia</p>
				<p class="LogoNex pngfix">Uma empresa NexGroup</p>
			</div> <!-- .Container -->
		</div> <!-- .Header -->