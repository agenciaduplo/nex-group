<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	
	<title>Vergéis - Estúdio 2, 3 e 4 dormitórios</title>
	
	<meta name="description" content="Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas - Vergeis" />
	<meta name="keywords" content="vergeis, apartamento, apto, porto alegre, poa, rs, rio grande do sul, estúdio, 2 dormitórios, 3 dormitórios, 4 dormitórios, coberturas" />
	<meta name="author" content="Divex Tecnologia" />
	<meta name="copyright" content="&copy; 2011 NexGroup" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta name="google-site-verification" content="mcFVQI0v-vGbNK_1HPmRtoP1C9SqS96AcAz_O_ZZKWM" />
	
		
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->
	
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validate.js" ></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/common3.js" ></script>
	<!--[if IE 6]>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			DD_belatedPNG.fix('.pngfix, .CampoPadrao, img');
		});
	</script>
	<![endif]-->

	<!-- Analytics -->
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-51']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>		
	<!-- Analytics -->
</head>
<body class="Home Cadastrar">
	<div class="Main">
		<div class="Header pngfix">
			<div class="Container">
				<h1 class="Logo pngfix"><a href="#Home" style="width:125px; height:106px; display:block; text-indent:-9999em;" >Vergéis - apartamentos com diversas opções de plantas</a></h1>
				<a href="http://www.nexgroup.com.br" target="_blank"><p class="LogoNex2 pngfix">Uma empresa NexGroup</p></a>
				<a href="http://www.nexgroup.com.br" target="_blank"><p class="LogoCapa2 pngfix">Capa Engenharia</p></a>
				
			</div> <!-- .Container -->
		</div> <!-- .Header -->