<div id="SoliciteInformacoes">
	<p>Solicite Informações:</p>
	<form id="FormInformacoes" class="Form" method="post">
		<ul>
			<li>
				<input type="text" id="InfoNome" class="CampoPadrao" name="InfoNome" placeholder="Nome" />
			</li>
			<li>
				<input type="text" id="InfoEmail" class="CampoPadrao" name="InfoEmail" placeholder="Email" />
			</li>
			<li>
				<input type="text" id="InfoTelefone" class="CampoPadrao" name="InfoTelefone" placeholder="Fone" />
			</li>
			<li>
				<input type="text" id="InfoCidade" class="CampoPadrao" name="InfoCidade" placeholder="Cidade" />
			</li>
			<li>
				<textarea id="InfoComentario" name="InfoComentario" cols="40" rows="8" placeholder="Mensagem"></textarea>
			</li>
			<li class="Last"><input type="submit" id="BotaoEnviar1" class="BotaoEnviar1 pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
		</ul>
	</form>
</div>