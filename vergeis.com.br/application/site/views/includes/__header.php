<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	
	<title>Vergéis - Estúdio 2, 3 e 4 dormitórios</title>
	
	<meta name="description" content="Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas - Vergeis" />
	<meta name="keywords" content="vergeis, apartamento, apto, porto alegre, poa, rs, rio grande do sul, estúdio, 2 dormitórios, 3 dormitórios, 4 dormitórios, coberturas" />
	<meta name="author" content="Divex Tecnologia" />
	<meta name="copyright" content="&copy; 2011 NexGroup" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta name="google-site-verification" content="mcFVQI0v-vGbNK_1HPmRtoP1C9SqS96AcAz_O_ZZKWM" />
	
	<meta property="og:title" content="Vergeis - Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas" />
	<meta property="og:type" content="product" />
	<meta property="og:url" content="http://www.vergeis.com.br" />
	<meta property="og:image" content="http://www.nexgroup.com.br/site2011/uploads/imoveis/logo/1759e02b2f0b58bd7241a3a0a487f399.jpg" />
	<meta property="og:site_name" content="Vergeis" />
	<meta property="fb:admins" content="1528287035" />

	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/feature-carousel.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/fancybox.css" type="text/css" media="screen" />
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->
	
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.featurecarousel.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validate.js" ></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/common.js"></script>
	<script src="http://connect.facebook.net/pt_BR/all.js#xfbml=1"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

	<!--[if IE 6]>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			DD_belatedPNG.fix('.pngfix, .CampoPadrao, img');
		});
	</script>
	<![endif]-->

	<!-- Analytics -->
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-51']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>		
	<!-- Analytics -->
</head>

<body class="Home">

	<div class="Main">
		<div class="Header pngfix">
			<div class="Container">
				<h1 class="Logo pngfix"><a href="#Home" style="width:125px; height:106px; display:block; text-indent:-9999em;" >Vergéis - apartamentos com diversas opções de plantas</a></h1>
				<ul class="Features">
					<li class="First"><a href="javascript:window.open('http://web.atendimentoaovivo.com.br/chat.asp?idc=5447&amp;pre_empresa=1192&amp;pre_depto=1276&amp;vivocustom=&amp;vivocustom2=DHZ&amp;vivocustom3=Site+DHZ&amp;vivocustom4=Rodape&amp;idtemplate=0','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix">Corretor online</a></li>
					<!-- 
					<li class="Second">
						<a href="#" class="BotaoPlantao FeatureButton pngfix">Plantão de vendas</a>
						<div id="PainelPlantao" class="SlidingPanel BoxPlantao">
							<span class="BotaoFechar pngfix">Fechar</span>
							<span class="Tel pngfix">51 3378 7800</span>
							<p class="Address">Rua Francisco Petuco, 121<br />Porto Alegre.RS</p>
							<a href="" class="BotaoVerMapa pngfix" rel="external">Ver no mapa</a>
						</div> 
					</li> -->
					<li class="Third">
						<a href="#" class="BotaoInteresse FeatureButton pngfix" title="Tenho interesse">Tenho interesse</a>
						<div id="PainelTenhoInteresse" class="SlidingPanel BoxTenhoInteresse">
							<span class="BotaoFechar pngfix">Fechar</span>
							<form id="FormInteresse" class="Form" method="post">
								<ul class="First">
									<li>
										<label for="Nome">nome</label>
										<input type="text" id="Nome" class="CampoPadrao" name="nome" />
									</li>
									<li>
										<label for="FaixaEtaria" class="hfixie">faixa etária</label>
										<select id="FaixaEtaria" name="FaixaEtaria">
											<option value=""></option>
											<option value="Até 30 anos">At&eacute; 30 anos</option>
											<option value="De 31 a 40 anos">De 31 a 40 anos</option>
			                                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
			                                <option value="Mais de 50 anos">Mais de 50 anos</option>
										</select>
									</li>
									<li>
										<label for="EstadoCivil" class="hfixie">estado civil</label>
										<select id="EstadoCivil" name="EstadoCivil">
											<option value=""></option>
											<option value="Casado">casado</option>
			                                <option value="Solteiro">solteiro</option>
										</select>
									</li>
									<li>
										<label for="Bairro" class="hfixie">bairro / cidade</label>
										<input type="text" id="bairroCidade" class="CampoPadrao" name="bairroCidade" />
									</li>
									<li>
										<label for="Profissao">profissão</label>
										<input type="text" id="Profissao" class="CampoPadrao" name="Profissao" />
									</li>
								</ul>
								<ul class="Second">
									<li class="Indent">
										<label for="Telefone">telefone</label>
										<input type="text" id="Telefone" class="CampoPadrao" name="telefone" />
									</li>
									<li class="Indent">
										<label for="Email">e-mail</label>
										<input type="text" id="Email" class="CampoPadrao" name="email" />
									</li>
									<li>
										<div class="CheckGroup">
											<span class="LabelOptions">formas de contato</span>
											<span class="CheckOption">
												<input type="radio" id="ChkTelefone" name="forma_contato" class="Check" value="Telefone" />
												<label for="ChkTelefone" class="Choice">telefone</label>
											</span>
											<span class="CheckOption">
												<input type="radio" id="ChkEmail" checked="checked" name="forma_contato" class="Check" value="E-mail" />
												<label for="ChkEmail" class="Choice">e-mail</label>
											</span>
										</div> <!-- .CheckGroup -->
									</li>
									<li class="Indent">
										<label for="Comentario">comentário</label>
										<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
									</li>
									<li class="Last"><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
								</ul>
							</form> <!-- #FormInteresse -->
							<div class="sucess">
								<h3><strong>Menssagem enviada com sucesso!</strong></h3>
								<p>Em breve entraremos em contato</p>
							</div>
						</div> <!-- #PainelTenhoInteresse -->
					</li>
					<li>
						<a href="#" class="BotaoIndique FeatureButton pngfix" title="Indique para um amigo">Indique para um amigo</a>
						<div id="PainelIndique" class="SlidingPanel BoxIndique">
							<span class="BotaoFechar pngfix">Fechar</span>
							<div></div>
							<form id="FormIndique" class="Form" method="post" action="">
								<ul class="First">
									<li>
										<label for="NomeRemetente">nome</label>
										<input type="text" id="NomeRemetente" class="CampoPadrao" name="NomeRemetente" />
									</li>
									<li>
										<label for="EmailRemetente">e-mail</label>
										<input type="text" id="EmailRemetente" class="CampoPadrao" name="EmailRemetente" />
									</li>
								</ul>
								<ul>
									<li>
										<label for="NomeAmigo">nome do amigo</label>
										<input type="text" id="NomeAmigo" class="CampoPadrao" name="NomeAmigo" />
									</li>
									<li>
										<label for="EmailAmigo">e-mail do amigo</label>
										<input type="text" id="EmailAmigo" class="CampoPadrao" name="EmailAmigo" />
									</li>
								</ul>
								<ul>
									<li>
										<label for="Comentario">comentário</label>
										<textarea id="ComentarioIndique" class="ComentarioIndique CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
									</li>
									<li class="Last"><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
								</ul>
							</form> <!-- #FormIndique -->
							<div class="sucess">
								<h3><strong>Menssagem enviada com sucesso!</strong></h3>
							</div>
						</div> <!-- #PainelIndique -->
					</li>
				</ul> <!-- .Features -->
				<ul class="Navigation pngfix">
					<li class="ItemHome"><a id="home" href="#Home" class="Active pngfix" title="Voltar para home">Home</a></li>
					<li class="ItemApartamentos"><a href="#Apartamentos" class="pngfix" title="Apartamentos">Apartamentos</a></li>
					<li class="ItemInfraestrutura"><a href="#Infraestrutura" class="pngfix" title="Infraestrutura">Infraestrutura</a></li>
					<li class="ItemContato"><a href="#Contato" class="pngfix" title="Contato">Contato</a></li>
					<li class="ItemLocalizacao Last"><a href="#Localizacao" class="pngfix" title="Localização">Localização</a></li>
				</ul> <!-- .Navigation -->
				<p class="LogoCapa pngfix">Capa Engenharia</p>
				<p class="LogoNex pngfix">Uma empresa NexGroup</p>
			</div> <!-- .Container -->
		</div> <!-- .Header -->