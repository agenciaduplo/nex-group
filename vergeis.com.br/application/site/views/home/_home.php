<?=$this->load->view('includes/header');?>
		<div id="header-slidass">
			<!-- jQuery handles to place the header background images -->
			<div id="headerimgs">
				<div id="headerimg1" class="headerimg"></div>
				<div id="headerimg2" class="headerimg"></div>
			</div>
		</div>

		<div id="Home" class="Featured">
			<div class="MainFeatured pngfix">
				<div class="Container pngfix">
					<h2 class="pngfix">Apartamentos com diversas opções de plantas. 2 e 3 dormitórios.</h2>
					<a href="#Apartamentos" class="BotaoMontePlanta pngfix">Monte sua planta Vergéis</a>
					<a href="#Localizacao" class="BotaoLocalizacao pngfix"></a>
					<div class="SocialMedia">
						<div class="Left">
							<!-- Facebook button -->
							<fb:like href="http://www.vergeis.com.br/" send="false" layout="button_count" width="450" show_faces="false" action="like" font="arial"></fb:like>
							<!-- Facebook button -->
						</div> <!-- .Left -->
						<div class="Left">
							<!-- Twitter button -->
							<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="http://www.vergeis.com.br/">Tweet</a>
							<!-- Twitter button -->
						</div> <!-- .Left -->
					</div> <!-- .SocialMedia -->
				</div> <!-- .Container -->
			</div> <!-- .MainFeatured -->
		</div> <!-- .Featured -->

		<div id="Apartamentos" class="Section Container pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<ul id="menu-dorms">
						<li class="Active"><a class="Lnk1Dorms" href="#" title="Estudio">Estudio</a></li>
						<li><a class="Lnk2Dorms" href="#" title="2 dormitorios">2 dormitorios</a></li>
						<li><a class="Lnk3Dorms" href="#" title="3 dormitorios">3 dormitorios</a></li>
						<li><a class="Lnk4Dorms" href="#" title="4 dormitorios">4 dormitorios</a></li>
					</ul>
					<h3 class="TitleApartamentos pngfix">Apartamentos</h3>
					<a href="<?=site_url()?>configurador" target="_blank" ><p class="Ahidden">Clique nas setas para navegar e veja a implantação e opções de plantas</p></a>
					<a href="<?=site_url()?>configurador" target="_blank" class="LinkMontePlanta pngfix">Monte sua planta, configure seu Vergéis aqui</a>
				</div> <!-- .ContainerTitle -->
				<div class="Clearfix"></div>
				<div id="dormitorios">
					<!-- Load Ajax Carousel -->
				</div>
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->
		<div id="Infraestrutura" class="Section Container pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<h3 class="TitleInfraestrutura pngfix">Infraestrutura</h3>
					<p class="Ahidden">Clique nas setas para navegar e conheça toda infraestrutura Vergéis</p>
				</div> <!-- .ContainerTitle -->
				<div id="infra">
					<!-- Load Ajax Carousel -->
				</div>
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->

		<div id="Contato" class="Section Container Contato pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<h3 class="TitleContato pngfix">Contato</h3>
					<p class="Ahidden">Preencha todos os campos do formulário abaixo<!--, ou se preferir ligue para nosso plantão de vendas--></p>
				</div> <!-- .ContainerTitle -->
				<div class="ContainerForm">
					<p class="TextoPlantao pngfix">Em breve plantão de vendas e apartamentos decorados</p>
					<form id="FormContato" class="Form ContactForm pngfix" method="post">
						<ul class="First">
							<li>
								<label for="NomeContato">Nome</label>
								<input type="text" id="NomeContato" class="CampoPadrao" name="NomeContato" />
							</li>
							<li>
								<label for="EmailContato">E-mail</label>
								<input type="text" id="EmailContato" class="CampoPadrao" name="EmailContato" />
							</li>
							<li>
								<label for="Endereco">Endereço</label>
								<input type="text" id="Endereco" class="CampoPadrao" name="Endereco" />
							</li>
							<li>
								<label for="Uf" class="hfixie">Estado</label>
								<select id="Uf" class="" name="Uf">
									<option value=""></option>
									<option value="AC">Acre</option>
								    <option value="AL">Alagoas</option>
								    <option value="AM">Amazonas</option>
								    <option value="AP">Amapá</option>
								    <option value="BA">Bahia</option>
								    <option value="CE">Ceará</option>
								    <option value="DF">Distrito Federal</option>
								    <option value="ES">Espirito Santo</option>
								    <option value="GO">Goiás</option>
								    <option value="MA">Maranhão</option>
								    <option value="MG">Minas Gerais</option>
								    <option value="MS">Mato Grosso do Sul</option>
								    <option value="MT">Mato Grosso</option>
								    <option value="PA">Pará</option>
								    <option value="PB">Paraíba</option>
								    <option value="PE">Pernambuco</option>
								    <option value="PI">Piauí</option>
								    <option value="PR">Paraná</option>
								    <option value="RJ">Rio de Janeiro</option>
								    <option value="RN">Rio Grande do Norte</option>
								    <option value="RO">Rondônia</option>
								    <option value="RR">Roraima</option>
								    <option selected="selected" value="RS">Rio Grande do Sul</option>
								    <option value="SC">Santa Catarina</option>
								    <option value="SE">Sergipe</option>
								    <option value="SP">São Paulo</option>
								    <option value="TO">Tocantins</option>
								</select>
							</li>
							<li>
								<label for="Cidade" class="hfixie">Cidade</label>
								<input type="text" id="Cidade" class="CampoPadrao" name="Cidade" />
							</li>
						</ul>
						<ul class="SecondCol">
							<li>
								<label for="Cep">CEP</label>
								<input type="text" id="Cep" class="CampoPadrao" name="Cep" />
							</li>
							<li>
								<label for="TelefoneContato">Telefone</label>
								<input type="text" id="TelefoneContato" class="CampoPadrao" name="TelefoneContato" />
							</li>
							<li class="Relative" style="height: 211px;">
								<label for="Mensagem3">Mensagem</label>
								<textarea id="Mensagem3" class="MensagemContato pngfix" name="Mensagem3" cols="40" rows="8"></textarea>
								<input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" />
							</li>
						</ul>
					</form> <!-- #FormContato -->
					<div class="sucess">
						<h3><strong>Menssagem enviada com sucesso!</strong></h3>
						<p>Em breve entraremos em contato</p>
					</div>
				</div> <!-- .ContainerForm -->
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->
	
<?=$this->load->view('includes/footer');?>