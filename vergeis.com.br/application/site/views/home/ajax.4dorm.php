<script type="text/javascript">
$(window).ready(function(){
	$("#carousel-4").featureCarousel({
		smallFeatureWidth: .7,
		smallFeatureHeight: .7,
		topPadding: 0,
		autoPlay: 0,
		trackerSummation: false,
		clickedCenter: function() { return false; }
	});
	$("#Apartamentos div.carousel-feature a.zoom").attr('rel', 'galeria-apartamentos');
	$("div.carousel-feature a.zoom").fancybox({
		'overlayColor': '#000',
		'overlayOpacity': 0.5
	});
});
</script>	
<div id="dorms-4" class="carousel-container">
	<div id="carousel-4" class="carousel">
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
			<div class="carousel-caption">
				<p>Implanta&ccedil;&atilde;o</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_12.png" alt="Living 4 dorms." class="carousel-image"  />
			<div class="carousel-caption">
				<p>Living 4 dorms.</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_12.jpg" title="Living 4 dorms." class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_21.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_21.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_22.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_22.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_23.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_23.jpg"  title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_24.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_24.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_36.png" alt="4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_36.jpg" title="4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_37.png" alt="4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_37.jpg" title="4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
	</div> <!-- #carousel -->
	<div id="carousel-left" class="pngfix">Foto anterior</div>
	<div id="carousel-right" class="pngfix">Pr�xima foto</div>
</div> <!-- .carousel-container -->