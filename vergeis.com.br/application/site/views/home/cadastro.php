<?=$this->load->view('includes/header2');?>

		<div id="Home" class="Featured">
			<div class="MainFeatured pngfix">
				<div class="Container2 pngfix">
					<div class="Cadastro">
						<p><strong>Bem vindo!</strong></p>
						<p>Cadastre-se e receba informações exclusivas sobre este <strong>grande lançamento.</strong><br />
						<strong>Após o cadastro</strong> você será <strong>automaticamente direcionado</strong> ao site do Vergéis.</p>
							<form id="FormCadastro" class="CadastroForm pngfix" action="<?=base_url()?>home/enviarCadastro" onsubmit="return validaCadastro()" method="post">
									<ul>
										<li>
											<label for="NomeContato">Nome</label>
											<input type="text" id="Nome" class="CampoPadrao" name="nome" />
										</li>
										<li>
											<label for="EmailContato">E-mail</label>
											<input type="text" id="Email" class="CampoPadrao" name="email" />
										</li>
									</ul>
									<input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" />
								</form> 
					</div>
				</div>
			</div> <!-- .MainFeatured -->
		</div> <!-- .Featured -->
		</div> <!-- .ContainerForm -->
	</div> <!-- .MainSection -->
</div> <!-- .Section -->
<?=$this->load->view('includes/footer2');?>