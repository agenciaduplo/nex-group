<script type="text/javascript">
$(window).ready(function(){
	$("#carousel2").featureCarousel({
			smallFeatureWidth: .7,
			smallFeatureHeight: .7,
			topPadding: 0,
			autoPlay: 0,
			trackerSummation: false,
			clickedCenter: function() { return false; }
		});

		// Lightbox para exibição das fotos
		$("#Infraestrutura div.carousel-feature a.zoom").attr('rel', 'galeria-infraestrutura');
		$("div.carousel-feature a.zoom").fancybox({
			'overlayColor': '#000',
			'overlayOpacity': 0.5
		});
});
</script>
<div class="carousel-container">
					<div id="carousel2" class="carousel">
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_17.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Ficha T&eacute;cnica</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_17.jpg" title="Ficha T&eacute;cnica" class="zoom dark pngfix">Ampliar foto</a>
          					</div> 
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Implanta&ccedil;&atilde;o</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_02.png" alt="Fachada" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Fachada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_02.jpg" title="Fachada"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_x.png" alt="Fachada noturna e piscina com raia" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Fachada noturna e piscina com raia</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_x.jpg" title="Fachada noturna e piscina com raia"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>

						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_10.png" alt="Hall de entrada" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Hall de entrada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_10.jpg"  title="Hall de entrada" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_08.png" alt="Sal&atilde;o Gourmet com enoteca" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o Gourmet com enoteca</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_08.jpg" title="Sal&atilde;o Gourmet com enoteca" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_15.png" alt="Sport Bar" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sport Bar</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_15.jpg"title="Sport Bar"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_16.png" alt="Sal&atilde;o Balada" class="carousel-image" />
							<div class="carousel-caption">
								<p>Sal&atilde;o Balada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_16.jpg" title="Sal&atilde;o Balada" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_13.png" alt="Sal&atilde;o de Festas Infantil" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o de Festas Infantil</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_13.jpg"  title="Sal&atilde;o de Festas Infantil" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_14.png" alt="Sal&atilde;o de Festas Infantil 02" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o de Festas Infantil 02</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_14.jpg" title="Sal&atilde;o de Festas Infantil 02" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_07.png" alt="Brinquedoteca" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Brinquedoteca</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_07.jpg" title="Brinquedoteca" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						
						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_09.png" alt="Fitness" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Fitness</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_09.jpg" title="Fitness"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_01.png" alt="Quiosque com churrasqueira" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Quiosque com churrasqueira</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_01.jpg" title="Quiosque com churrasqueira"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_06.png" alt="Quadra de T&ecirc;nis e Quadra Poliesportiva" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Quadra de T&ecirc;nis e Quadra Poliesportiva</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_06.jpg" title="Quadra de T&ecirc;nis e Quadra Poliesportiva" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_04.png" alt="Piscinas" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Piscinas</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_04.jpg" title="Piscinas" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_05.png" alt="Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_05.jpg" title="Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
					</div> <!-- #carousel2 -->
					<div id="carousel-left" class="pngfix">Foto anterior</div>
					<div id="carousel-right" class="pngfix">Próxima foto</div>
				</div> <!-- .carousel-container -->