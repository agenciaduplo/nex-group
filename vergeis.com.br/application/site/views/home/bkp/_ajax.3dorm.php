<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.featurecarousel.js"></script>
<script type="text/javascript">
$(window).ready(function(){
	$("#carousel-3").featureCarousel({
		smallFeatureWidth: .7,
		smallFeatureHeight: .7,
		topPadding: 0,
		autoPlay: 0,
		trackerSummation: false,
		clickedCenter: function() { return false; }
	});
	$("#Apartamentos div.carousel-feature a.zoom").attr('rel', 'galeria-apartamentos');
	$("a.zoom").fancybox({
		'overlayColor': '#000',
		'overlayOpacity': 0.5
	});
});
</script>
<div id="dorms-3" class="carousel-container">
	<div id="carousel-3" class="carousel">
		<!-- 3 Dorms -->
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
			<div class="carousel-caption">
				<p>Implanta&ccedil;&atilde;o</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_17.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_17.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_18.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_18.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_19.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_19.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_20.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_20.jpg"  title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_34.png" alt="3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_34.jpg" title="3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_35.png" alt="3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_35.jpg" title="3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		</div>
	<div id="carousel-left" class="pngfix">Foto anterior</div>
	<div id="carousel-right" class="pngfix">Pr�xima foto</div>
</div> <!-- .carousel-container -->