<?=$this->load->view('includes/header');?>

		<div id="Home" class="Featured">
			<div class="MainFeatured pngfix">
				<div class="Container pngfix">
					<h2 class="pngfix">Apartamentos com diversas opções de plantas. Estúdio, 2, 3 e 4 dormitórios.</h2>
					<a href="#Apartamentos" class="BotaoMontePlanta pngfix">Monte sua planta Vergéis</a>
					<div class="SocialMedia">
						<div class="Left">
							<!-- Facebook button -->
							<fb:like href="http://www.vergeis.com.br/" send="false" layout="button_count" width="450" show_faces="false" action="like" font="arial"></fb:like>
							<!-- Facebook button -->
						</div> <!-- .Left -->
						<div class="Left">
							<!-- Twitter button -->
							<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="http://www.vergeis.com.br/">Tweet</a>
							<!-- Twitter button -->
						</div> <!-- .Left -->
					</div> <!-- .SocialMedia -->
				</div> <!-- .Container -->
			</div> <!-- .MainFeatured -->
		</div> <!-- .Featured -->

		<div id="Apartamentos" class="Section Container pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<h3 class="TitleApartamentos pngfix">Apartamentos</h3>
					<a href="<?=site_url()?>configurador" target="_blank" ><p class="Ahidden">Clique nas setas para navegar e veja a implantação e opções de plantas</p></a>
					<a href="<?=site_url()?>configurador" target="_blank" class="LinkMontePlanta pngfix">Monte sua planta, configure seu Vergéis aqui</a>
				</div> <!-- .ContainerTitle -->
				<div class="carousel-container">
					<div id="carousel" class="carousel">
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_11.png" alt="Living 2 dorms." class="carousel-image"  />
							<div class="carousel-caption">
								<p>Living 2 dorms.</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_11.jpg" title="Living 2 dorms." class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_12.png" alt="Living 4 dorms." class="carousel-image"  />
							<div class="carousel-caption">
								<p>Living 4 dorms.</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_12.jpg" title="Living 4 dorms." class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Implanta&ccedil;&atilde;o</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_01.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_01.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_02.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA) " class="carousel-image"  />
							<div class="carousel-caption">
								<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_02.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA) "  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_03.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O " class="carousel-image"  />
							<div class="carousel-caption">
								<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O </p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_03.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O "  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_04.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE " class="carousel-image"  />
							<div class="carousel-caption">
								<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE </p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_04.jpg"  title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE "  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_05.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_05.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_06.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_06.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_07.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_07.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_08.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_08.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_09.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_09.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_10.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_10.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_11.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_11.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_12.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_12.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						



						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_13.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_13.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_14.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_14.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_15.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_15.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_16.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_16.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						



						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_17.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_17.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_18.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_18.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE AMPLIADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_19.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_19.jpg" title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_20.png" alt="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_20.jpg"  title="3DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>




						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_21.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_21.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 3D COM SU&Iacute;TE E CLOSET COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_22.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_22.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 &Aacute;REA SOCIAL ESTENDIDA (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_23.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_23.jpg"  title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 2 SU&Iacute;TES COM CLOSET (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_24.png" alt="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_24.jpg" title="4DORMS - PLANTA BAIXA - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>

						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_25.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_25.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_26.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_26.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O " class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_27.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_27.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_28.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_28.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>

						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_29.png"  alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_29.jpg"  title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_30.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) " class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) </p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_30.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) " class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_31.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_31.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_32.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_32.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						



						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_33.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O" class="carousel-image"  />
							<div class="carousel-caption">
								<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_33.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_34.png" alt="3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_34.jpg" title="3DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_35.png" alt="3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_35.jpg" title="3DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 03 E 04* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_36.png" alt="4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_36.jpg" title="4DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/plantas/planta_37.png" alt="4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
							<div class="carousel-caption">
								<p>4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)</p>
								<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_37.jpg" title="4DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRES A2 E A3 &ndash; FINAIS 01* E 02 PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
					</div> <!-- #carousel -->
					<div id="carousel-left" class="pngfix">Foto anterior</div>
					<div id="carousel-right" class="pngfix">Próxima foto</div>
				</div> <!-- .carousel-container -->
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->

		<div id="Infraestrutura" class="Section Container pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<h3 class="TitleInfraestrutura pngfix">Infraestrutura</h3>
					<p class="Ahidden">Clique nas setas para navegar e conheça toda infraestrutura Vergéis</p>
				</div> <!-- .ContainerTitle -->
				<div class="carousel-container">
					<div id="carousel2" class="carousel">
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_17.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Ficha T&eacute;cnica</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_17.jpg" title="Ficha T&eacute;cnica" class="zoom dark pngfix">Ampliar foto</a>
          					</div> 
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Implanta&ccedil;&atilde;o</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_02.png" alt="Fachada" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Fachada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_02.jpg" title="Fachada"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_10.png" alt="Hall de entrada" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Hall de entrada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_10.jpg"  title="Hall de entrada" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_08.png" alt="Sal&atilde;o Gourmet com enoteca" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o Gourmet com enoteca</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_08.jpg" title="Sal&atilde;o Gourmet com enoteca" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_15.png" alt="Sport Bar" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sport Bar</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_15.jpg"title="Sport Bar"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_16.png" alt="Sal&atilde;o Balada" class="carousel-image" />
							<div class="carousel-caption">
								<p>Sal&atilde;o Balada</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_16.jpg" title="Sal&atilde;o Balada" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_13.png" alt="Sal&atilde;o de Festas Infantil" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o de Festas Infantil</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_13.jpg"  title="Sal&atilde;o de Festas Infantil" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_14.png" alt="Sal&atilde;o de Festas Infantil 02" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Sal&atilde;o de Festas Infantil 02</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_14.jpg" title="Sal&atilde;o de Festas Infantil 02" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_07.png" alt="Brinquedoteca" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Brinquedoteca</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_07.jpg" title="Brinquedoteca" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						
						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_09.png" alt="Fitness" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Fitness</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_09.jpg" title="Fitness"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>						
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_01.png" alt="Quiosque com churrasqueira" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Quiosque com churrasqueira</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_01.jpg" title="Quiosque com churrasqueira"  class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_06.png" alt="Quadra de T&ecirc;nis e Quadra Poliesportiva" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Quadra de T&ecirc;nis e Quadra Poliesportiva</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_06.jpg" title="Quadra de T&ecirc;nis e Quadra Poliesportiva" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_04.png" alt="Piscinas" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Piscinas</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_04.jpg" title="Piscinas" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
						<div class="carousel-feature">
							<img src="<?=base_url()?>assets/img/site/perspectivas/img_05.png" alt="Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha" class="carousel-image"  />
							<div class="carousel-caption">
								<p>Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha</p>
								<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_05.jpg" title="Piscina Adulto com Raia de Nata&ccedil;&atilde;o e Prainha" class="zoom dark pngfix">Ampliar foto</a>
          					</div> <!-- .carousel-feature -->
						</div>
					</div> <!-- #carousel2 -->
					<div id="carousel-left" class="pngfix">Foto anterior</div>
					<div id="carousel-right" class="pngfix">Próxima foto</div>
				</div> <!-- .carousel-container -->
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->

		<div id="Contato" class="Section Container Contato pngfix">
			<div class="MainSection pngfix">
				<div class="ContainerTitle">
					<h3 class="TitleContato pngfix">Contato</h3>
					<p class="Ahidden">Preencha todos os campos do formulário abaixo<!--, ou se preferir ligue para nosso plantão de vendas--></p>
				</div> <!-- .ContainerTitle -->
				<div class="ContainerForm">
					<p class="TextoPlantao pngfix">Em breve plantão de vendas e apartamentos decorados</p>
					<form id="FormContato" class="Form ContactForm pngfix" method="post">
						<ul class="First">
							<li>
								<label for="NomeContato">Nome</label>
								<input type="text" id="NomeContato" class="CampoPadrao" name="NomeContato" />
							</li>
							<li>
								<label for="EmailContato">E-mail</label>
								<input type="text" id="EmailContato" class="CampoPadrao" name="EmailContato" />
							</li>
							<li>
								<label for="Endereco">Endereço</label>
								<input type="text" id="Endereco" class="CampoPadrao" name="Endereco" />
							</li>
							<li>
								<label for="Uf" class="hfixie">Estado</label>
								<select id="Uf" class="" name="Uf">
									<option value=""></option>
									<option value="AC">Acre</option>
								    <option value="AL">Alagoas</option>
								    <option value="AM">Amazonas</option>
								    <option value="AP">Amapá</option>
								    <option value="BA">Bahia</option>
								    <option value="CE">Ceará</option>
								    <option value="DF">Distrito Federal</option>
								    <option value="ES">Espirito Santo</option>
								    <option value="GO">Goiás</option>
								    <option value="MA">Maranhão</option>
								    <option value="MG">Minas Gerais</option>
								    <option value="MS">Mato Grosso do Sul</option>
								    <option value="MT">Mato Grosso</option>
								    <option value="PA">Pará</option>
								    <option value="PB">Paraíba</option>
								    <option value="PE">Pernambuco</option>
								    <option value="PI">Piauí</option>
								    <option value="PR">Paraná</option>
								    <option value="RJ">Rio de Janeiro</option>
								    <option value="RN">Rio Grande do Norte</option>
								    <option value="RO">Rondônia</option>
								    <option value="RR">Roraima</option>
								    <option selected="selected" value="RS">Rio Grande do Sul</option>
								    <option value="SC">Santa Catarina</option>
								    <option value="SE">Sergipe</option>
								    <option value="SP">São Paulo</option>
								    <option value="TO">Tocantins</option>
								</select>
							</li>
							<li>
								<label for="Cidade" class="hfixie">Cidade</label>
								<input type="text" id="Cidade" class="CampoPadrao" name="Cidade" />
							</li>
						</ul>
						<ul class="SecondCol">
							<li>
								<label for="Cep">CEP</label>
								<input type="text" id="Cep" class="CampoPadrao" name="Cep" />
							</li>
							<li>
								<label for="TelefoneContato">Telefone</label>
								<input type="text" id="TelefoneContato" class="CampoPadrao" name="TelefoneContato" />
							</li>
							<li class="Relative">
								<label for="Mensagem3">Mensagem</label>
								<textarea id="Mensagem3" class="MensagemContato pngfix" name="Mensagem3" cols="40" rows="8"></textarea>
								<input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" />
							</li>
						</ul>
					</form> <!-- #FormContato -->
					<div class="sucess">
						<h3><strong>Menssagem enviada com sucesso!</strong></h3>
						<p>Em breve entraremos em contato</p>
					</div>
				</div> <!-- .ContainerForm -->
			</div> <!-- .MainSection -->
		</div> <!-- .Section -->
	
<?=$this->load->view('includes/footer');?>