<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.featurecarousel.js"></script>
<script type="text/javascript">
$(window).ready(function(){
	$("#carousel-22").featureCarousel({
		smallFeatureWidth: .7,
		smallFeatureHeight: .7,
		topPadding: 0,
		autoPlay: 0,
		trackerSummation: false,
		clickedCenter: function() { return false; }
	});
	$("#Apartamentos div.carousel-feature a.zoom").attr('rel', 'galeria-apartamentos');
	$("div.carousel-feature a.zoom").fancybox({
		'overlayColor': '#000',
		'overlayOpacity': 0.5
	});
});
</script>	
<div id="dorms-2" class="carousel-container">
	<div id="carousel-22" class="carousel">
		<!-- 2 Dorms -->
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
			<div class="carousel-caption">
				<p>Implanta&ccedil;&atilde;o</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_11.png" alt="Living 2 dorms." class="carousel-image"  />
			<div class="carousel-caption">
				<p>Living 2 dorms.</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_11.jpg" title="Living 2 dorms." class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_05.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_05.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_06.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_06.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_07.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_07.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 01 E TORRE B4 FINAIS 02* E 06 COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_08.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_08.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_09.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_09.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 &Aacute;REA SOCIAL ESTENDIDA E SU&Iacute;TE COM CLOSET"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_10.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_10.jpg" title="2DORMS - PLANTA BAIXA - TORRE B2 FINAL 02 E TORRE B4 FINAL 05 COZINHA FECHADA"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_11.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_11.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_12.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_12.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* PLANTA COM SU&Iacute;TE (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_13.png" alt="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_13.jpg"  title="2DORMS - PLANTA BAIXA - TORRE B2 FINAIS 04 E 05* COZINHA FECHADA (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_14.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_14.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_15.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_15.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 PLANTA SU&Iacute;TE E &Aacute;REA SOCIAL ESTENDIDA"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_16.png" alt="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_16.jpg" title="2DORMS - PLANTA BAIXA - TORRE B4 FINAL 03 COZINHA FECHADA"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_25.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_25.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 01* E 02 &ndash; PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_26.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_26.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 01 &ndash; PLANTA PADR&Atilde;O " class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_27.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_27.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAL 02 &ndash; PLANTA PADR&Atilde;O" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_28.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_28.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_29.png"  alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_29.jpg"  title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B2 FINAIS 04 E 05* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_30.png" alt="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) " class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) </p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_30.jpg" title="2DORMS COBERTURA - PLANTA BAIXA SUPERIOR - TORRE B4 FINAIS 02, 03*, 05 E 06* PLANTA PADR&Atilde;O (*REBATIDA) " class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_31.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_31.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAIS 02* E 06 PLANTA PADR&Atilde;O (*REBATIDA)"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_32.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_32.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 03 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_33.png" alt="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O" class="carousel-image"  />
			<div class="carousel-caption">
				<p>2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_33.jpg" title="2DORMS COBERTURA - PLANTA BAIXA INFERIOR - TORRE B4 FINAL 05 PLANTA PADR&Atilde;O"  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
	</div> <!-- #carousel -->
	<div id="carousel-left" class="pngfix">Foto anterior</div>
	<div id="carousel-right" class="pngfix">Pr�xima foto</div>
</div> <!-- .carousel-container -->