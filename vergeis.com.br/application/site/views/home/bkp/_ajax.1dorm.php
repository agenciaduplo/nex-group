<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.featurecarousel.js"></script>
<script type="text/javascript">
$(window).ready(function(){
	$("#carousel-1").featureCarousel({
		smallFeatureWidth: .7,
		smallFeatureHeight: .7,
		topPadding: 0,
		autoPlay: 0,
		trackerSummation: false,
		clickedCenter: function() { return false; }
	});
	$("#Apartamentos div.carousel-feature a.zoom").attr('rel', 'galeria-apartamentos');
	$("div.carousel-feature a.zoom").fancybox({
		'overlayColor': '#000',
		'overlayOpacity': 0.5
	});
});
</script>
<div id="dorms-1" class="carousel-container">
	<div id="carousel-1" class="carousel">
		<!--1 Domr Estudio-->
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/perspectivas/img_03.png" alt="Implanta&ccedil;&atilde;o" class="carousel-image"  />
			<div class="carousel-caption">
				<p>Implanta&ccedil;&atilde;o</p>
				<a href="<?=base_url()?>assets/img/site/perspectivas/zoom/img_03.jpg" title="Implanta&ccedil;&atilde;o" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_01.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)" class="carousel-image"  />
			<div class="carousel-caption">
				<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_01.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA PADR&Atilde;O (*REBATIDA)" class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_02.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA) " class="carousel-image"  />
			<div class="carousel-caption">
				<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA)</p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_02.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B2 FINAL 03 E 06* E TORRE B4 FINAL 01* PLANTA LIVRE (*REBATIDA) "  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_03.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O " class="carousel-image"  />
			<div class="carousel-caption">
				<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O </p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_03.jpg" title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA PADR&Atilde;O "  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
		<div class="carousel-feature">
			<img src="<?=base_url()?>assets/img/site/plantas/planta_04.png" alt="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE " class="carousel-image"  />
			<div class="carousel-caption">
				<p>EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE </p>
				<a href="<?=base_url()?>assets/img/site/plantas/zoom/planta_04.jpg"  title="EST&Uacute;DIO - (1 dorm.) PLANTA BAIXA - TORRE B4 FINAL 04 PLANTA LIVRE "  class="zoom dark pngfix">Ampliar foto</a>
			</div> <!-- .carousel-feature -->
		</div>
	</div> <!-- #carousel -->
	<div id="carousel-left" class="pngfix">Foto anterior</div>
	<div id="carousel-right" class="pngfix">Pr�xima foto</div>
</div> <!-- .carousel-container -->