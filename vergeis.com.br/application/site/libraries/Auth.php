<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2006, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

class CI_Auth {

	var $CI;
	var $auth_table_name;
	var $auth_username_field;
	var $auth_password_field;
	
	/**
	 * Constructor
	 *
	 * Loads the calendar language file and sets the default time reference
	 *
	 * @access	public
	 */
	function CI_Auth()
	{	
		$this->CI =& get_instance();

		$this->CI->config->load('auth');
		
		$this->auth_table_name 		= $this->CI->config->item('auth_table_name');
		$this->auth_username_field	= $this->CI->config->item('auth_username_field');
		$this->auth_password_field	= $this->CI->config->item('auth_password_field');
		
		log_message('debug', "Authentication Class Initialized");
	}
	
	// --------------------------------------------------------------------

	/**
	 * Try login with params
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	object
	 */
	function try_login($username, $password)
	{
		$where = array (
			$this->auth_username_field => $username
		);
		
		$query = $this->CI->db->get_where($this->auth_table_name, $where, 1);
	        
        if ($query->num_rows == 1)
        {
        	$row = $query->row();
        	
        	if ($password == $this->CI->encrypt->decode($row->senha))
        	{
        		
        		$session = array (
					'nature_id_user'	=> $this->CI->encrypt->encode($row->id_user),
        			'nature_nome'		=> $row->nome,
					'nature_email'		=> $row->email,
        			'nature_ativo'		=> $row->ativo,
        			'nature_senha'		=> $row->senha
				);
				
				$this->CI->session->set_userdata($session);
				
				$output = TRUE;
        	}
        	else
        	{
        		$output = FALSE;
        		$this->CI->session->set_flashdata('resposta', 'A Senha não está correta!');
        	}
        }
        else
        {
        	$output = FALSE;
			$this->CI->session->set_flashdata('resposta', 'O Usuário não está correto!');
        }
        
        return $output;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if logged
	 *
	 * @access	public
	 * @return	void
	 */
	function check()
	{
		$where = array (
			$this->auth_username_field => ($this->CI->session->userdata('nature_email')) ? $this->CI->session->userdata('nature_email') : "",
			$this->auth_password_field => ($this->CI->session->userdata('nature_senha')) ? $this->CI->session->userdata('nature_senha') : ""
		);
		
		$query = $this->CI->db->get_where($this->auth_table_name, $where, 1);
		
		/*
		if ($query->num_rows != 1)
        {
        	redirect("login/");      	
        }
        */
        
        if ($query->num_rows != 1)
        {
        	$erro = "0";
        	return $erro;
        }
        else
        {
        	$erro = "1";
        	return $erro;
        }
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Initialize the user preferences
	 *
	 * Accepts an associative array as input, containing display preferences
	 *
	 * @access	public
	 * @param	array	config preferences
	 * @return	void
	 */	
	function logout()
	{
		$this->CI->session->sess_destroy();
	}
	
	// --------------------------------------------------------------------
}
// END CI_Authentication class

/* End of file Auth.php */
/* Location: ./system/libraries/Auth.php */