<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');

		$data = array (
			'page'					=> "home"
		);

		$this->load->view('home/home', $data);
	}

	function tour()
	{
		$this->load->model('home_model', 'model');

		$data = array (
			'page'					=> "home"
		);

		$this->load->view('home/home_tour', $data);
	}

	function site()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}

	function dorm1()
	{
		$this->load->view('home/ajax.1dorm.php');
	}

	function dorm2()
	{
		$this->load->view('home/ajax.2dorm.php');
	}

	function dorm3()
	{
		$this->load->view('home/ajax.3dorm.php');
	}

	function dorm4()
	{
		$this->load->view('home/ajax.4dorm.php');
	}
	
	function infra()
	{
		$this->load->view('home/ajax.infra.php');
	}

	function enviarIndique()
	{
		$this->load->model('home_model', 'model');

		$nome_remetente 	= $this->input->post('nome_remetente');
		$email_remetente 	= $this->input->post('email_remetente');
		$nome_amigo 		= $this->input->post('nome_amigo');
		$email_amigo 		= $this->input->post('email_amigo');
		$comentarios 		= $this->input->post('comentarios');

		$id_empreendimento	= 36;
		$ip 				= $this->input->ip_address();
		$user_agent			= $this->input->user_agent();
		$data				= date("Y-m-d H:i:s");

			
		//CADASTRA INDIQUE NO BANCO DO NEXGROUP
		$dataNex = array (

			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'nome_remetente'		=> $nome_remetente,
			'email_remetente'		=> $email_remetente,
			'nome_destinatario'		=> $nome_amigo,
			'email_destinatario'	=> $email_amigo,
			'comentarios'			=> $comentarios,
			'origem'				=> $_SESSION['origem'],
			'url'					=> $_SESSION['url']
		);
		$indique 		= $this->model->setIndique($dataNex);

		//Inicia o envio do email
		//===================================================================
		$this->load->library('email');

		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);

		//Inicio da Mensagem
		ob_start();
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>VERGÉIS</title>
			</head>
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>VERGÉIS</b> :: Indicação via Site</td>
			  </tr>
			  <tr>
			    <td>Indicação enviada por:</td>
			  	<td> <strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td>Para:</td>
			  	<td> <strong><?=@$nome_amigo?> / <?=@$email_amigo?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>			  
			  <tr>
			    <td colspan="2">Olá <?=@$nome_amigo?>, acesse o site do Vergéis<br/><strong><a href="http://www.vergeis.com.br">Clique aqui</a></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">Nome do remetente:</td>
			    <td><strong><?=$nome_remetente?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.capa.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>

		<?
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem

		//roleta
		$grupos = $this->model->getGrupos($id_empreendimento);
		$total = 0;
		$total = count($grupos);
		
		if($total == 1) // caso só tenho um grupo cadastrado
		{
			$emails = $this->model->getGruposEmails($id_empreendimento, '001');
			
			$emails_txt = "";
			foreach ($emails as $email)
			{
				$grupo = $email->grupo;
				$list[] = $email->email;

				$emails_txt = $emails_txt.$email->email.", ";
			}
			$this->model->grupo_indique($indique, $grupo, $emails_txt);

		}
		else if($total > 1) // caso tenha mais de um grupo cadastrado
		{
			foreach ($grupos as $row):

				if($row->rand == 1):
					$atual = $row->ordem;
					if($atual == $total):

						$this->model->updateRand($id_empreendimento, '001');
						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						
						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);
					
					else:

						$atualizar = "00".$atual+1;
						$this->model->updateRand($id_empreendimento, $atualizar);
						$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);

					endif;	
				endif;
			endforeach;
		}
		/* envia email via roleta */
		
		
		$this->email->from("noreply@vergeis.com.br", "$nome_remetente");
		if($nome_remetente=='teste123' || $nome_remetente=='TESTE123'){
			$this->email->to('bruno.freiberger@divex.com.br');	
		}else{
			$this->email->to($email_amigo);
		}
		$this->email->bcc("testes@divex.com.br");		
		$this->email->subject("Vergéis - Indicação enviada para você");
		$this->email->message("$conteudo");
		$this->email->send();

		//envia para corretores
		$this->email->from("noreply@vergeis.com.br", "$nome_remetente");
		$this->email->to($list);	
		$this->email->cc("testes@divex.com.br");
		$this->email->subject("Vergéis - Indique enviado via site");
		$this->email->message("$conteudo");
		$this->email->send();
		//envia para corretores

		//===================================================================
		//Termina o envio do email	
		echo "Indicação Enviada!";
	}

	

	function enviarInteresse(){
		$this->load->model('home_model', 'model');

		$id_empreendimento = 36;

		$url 					= $_SESSION['url'];
		$url 					= explode("__",$url);

		$utm_source 	= $url[0];
		$utm_medium   = $url[1];
		$utm_campaign = $url[3];

		$email_nex 		= "vendas@nexvendas.com.br";
		$email_out 		= "lucia.keiko@vendasbbsul.com.br";
		// $email_nex 		= "anita.chiele@divex.com.br";
		// $email_out 		= "anita.chiele@divex.com.br";
		$email_go			= $email_nex;
		$id_empresa 	= $this->model->getIdEmpresa($id_empreendimento);
		$id_empresa 	= $id_empresa[0]->id_empresa;
		$id_cidade 		= $this->model->getIdCidade($id_empreendimento);
		$id_cidade 		= $id_cidade[0]->id_cidade;


		if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
			$email_for =  $this->model->verificaLastEmailSendFor(1);
			foreach($email_for as $emf){
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$email_go = $email_out;
					break;
				}else{
					$email_go = $email_nex;
					break;
				}
			}
		}else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)){ // Porto Alegre | 1 quarto dos interesses para fora.
			$email_for =  $this->model->verificaLastEmailSendFor(3);
			$i 				 = 0;
			$flag 		 = false;

			foreach($email_for as $emf){
				++$i;
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$flag = true;
				}else if($emf->email_enviado_for == $email_out && $i = 1){
					$email_go = $email_nex;
					break;
				}else{
					$flag = false;
				}

				if($flag){
					$email_go = $email_out;
				}else{
					$email_go = $email_nex;
				}
			}
		}else{
			$email_go = $email_nex;
		}

		$id_empreendimento	= $id_empreendimento;
		$nome 							= $this->input->post('nome');
		$email 							= $this->input->post('email');
		$telefone 					= $this->input->post('telefone');
		$contato 						= $this->input->post('contato');
		$comentarios 				= $this->input->post('comentarios');
		$faixa_etaria 			= $this->input->post('faixa_etaria');
		$estado_civil 			= $this->input->post('estado_civil');
		$bairro_cidade 			= $this->input->post('bairro_cidade');
		$profissao 					= $this->input->post('profissao');
		$origem							= @$_SESSION['origem'];
		$url								= @$_SESSION['url'];
		$ip 								= $this->input->ip_address();
		$user_agent					= $this->input->user_agent();
		$data_envio					= date("Y-m-d H:i:s");

		$p = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'						=> 21,
			'nome'								=> $nome,
			'email'								=> $email,
			'telefone'						=> $telefone,
			'forma_contato'				=> $contato,
			'comentarios'					=> $comentarios,
			'faixa_etaria'				=> $faixa_etaria,
			'estado_civil'				=> $estado_civil,
			'bairro_cidade'				=> $bairro_cidade,
			'profissao'						=> $profissao,
			'origem'							=> $origem,
			'url'									=> $url,
			'ip'									=> $ip,
			'user_agent'					=> $user_agent,
			'data_envio'					=> $data_envio,
			'hotsite'							=> 'S',
			'email_enviado_for' => $email_go
		);

		$empreendimento = $this->model->getEmpreendimento($id_empreendimento);
		$interesse 		  = $this->model->setInteresse($p);

		$dados = array(
			'Enviado em ' 		=> $p['data_envio'] . ' via HotSite',
			'Enviado por ' 		=> $p['nome'],
			'Empreendimento' 	=> $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
			'IP' 							=> $p['ip'],
			'E-mail' 					=> $p['email'],
			'Telefone' 				=> $p['telefone'],
			('Comentários')	 	=> $p['comentarios']
		);

		if($interesse){
			$this->load->library('email');

			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);

			/* envia email via roleta */
			$id_empreendimento = 36;
			$grupos = $this->model->getGrupos($id_empreendimento);

			$total = 0;
			$total = count($grupos);
			
			foreach ($grupos as $row):

				if($row->rand == 1):
					$atual = $row->ordem;

					if($atual == $total):

						$this->model->updateRand($id_empreendimento, '001');

						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						//echo "<pre>";print_r($emails);echo "</pre>";

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

					else:

						$atualizar = "00".$atual+1;
						$this->model->updateRand($id_empreendimento, $atualizar);

						$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
						//echo "<pre>";print_r($emails);echo "</pre>";

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

					endif;
				endif;
			endforeach;
			
      if($email_go == $email_out){
        $list = $email_go;
      }else{
        $list[] = $email_go;
      }
			/* envia email via roleta */

			if($p['nome'] == "Teste123"){
				$this->email->to('testes@divex.com.br');
			} else {
				$this->email->to($list);
			}

			$this->email->from("noreply@vergeis.com.br", "VERGÉIS - Nex Group");
			$this->email->subject('Contato enviado via HotSite');
			$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));

			$this->email->send();

			//===================================================================
			//Termina o envio do email
		}
		redirect('home/site','refresh');
	}
}
/* End of file home.php */
/* Location: ./application/controllers/home.php */