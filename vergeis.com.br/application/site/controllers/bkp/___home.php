<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function dorm1(){
		$this->load->view('home/ajax.1dorm.php');
	}
	function dorm2(){
		$this->load->view('home/ajax.2dorm.php');
	}
	function dorm3(){
		$this->load->view('home/ajax.3dorm.php');
	}
	function dorm4(){
		$this->load->view('home/ajax.4dorm.php');
	}
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$nome 				= $this->input->post('nome');
		$faixa_etaria 		= $this->input->post('faixa_etaria');
		$estado_civil 		= $this->input->post('estado_civil');
		$bairro_cidade 		= $this->input->post('bairro_cidade');
		$profissao 			= $this->input->post('profissao');
		$telefone 			= $this->input->post('telefone');
		$email 				= $this->input->post('email');
		$comentarios 		= $this->input->post('comentarios');
		$contato 			= $this->input->post('contato');
		
		$origem				= $_SESSION['origem'];
		$url				= $_SESSION['url'];
		
		$id_empreendimento	= 5;

		$ip 			= $this->input->ip_address();
		$user_agent		= $this->input->user_agent();
		$data			= date("Y-m-d H:i:s");
		
		//CADASTRA INTERESSE NO BANCO DO NEXGROUP
			$dataNex = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'nome'					=> $nome,
				'email'					=> $email,
				'faixa_etaria'			=> $faixa_etaria,
				'estado_civil'			=> $estado_civil,
				'bairro_cidade'			=> $bairro_cidade,
				'profissao'				=> $profissao,
				'telefone'				=> $telefone,
				'forma_contato'			=> $contato,
				'comentarios'			=> $comentarios,
				'url'					=> $url,
				'origem'				=> $origem,			
				'senha'					=> "}3_$%wB`7Atbdk1w"
				
			);
			//set POST variables
			$url= "http://www.nexgroup.com.br/index.php/integracao/cadastraInteresse";
			$this->CadastraNex($dataNex, $url);
			//FIM CADASTRA INTERESSE NO BANCO DO NEXGROUP
		
		
		
		
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'mail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Vergéis</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_capa.jpg" alt="GRUPO CAPA" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>Vergéis</b> :: Interesse via Site</td>
			  </tr>
			  <tr>
			    <td colspan="2">Interesse enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>				  
			  <tr>
			    <td width="30%">E-mail:</td>
			    <td><strong><?=$email?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Faixa Etária:</td>
			    <td><strong><?=$faixa_etaria?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Estado Civil:</td>
			    <td><strong><?=$estado_civil?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Bairro e Cidade:</td>
			    <td><strong><?=$bairro_cidade?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Profissão:</td>
			    <td><strong><?=$profissao?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Telefone:</td>
			    <td><strong><?=$telefone?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Forma de Contato:</td>
			    <td><strong><?=$contato?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	GRUPO CAPA
			    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		$this->email->from("no-reply@vergeis.com.br", "Interesse");
		//if($nome=="teste123"){
			$list = array(
					'douglas.otto@divex.com.br'
			);
	/*	}else{
			$list = array(
				'saldanha@capa.com.br',
				'henrique@fuhrosouto.com.br',
				'rodrigo@fuhrosouto.com.br'
			);	
		}*/
		$this->email->to($list);
		//$this->email->bcc('bruno.freiberger@divex.com.br'); 
		$this->email->subject('Vergéis - Interesse enviado pelo site');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email	
		echo "Interesse enviado com sucesso!";
		///$this->load->view('home/ajax.interesse.php');
	}
	
	function enviarIndique()
	{
			$nome_remetente 	= $this->input->post('nome_remetente');
			$email_remetente 	= $this->input->post('email_remetente');
			$nome_amigo 		= $this->input->post('nome_amigo');
			$email_amigo 		= $this->input->post('email_amigo');
			$comentarios 		= $this->input->post('comentarios');
			
			$id_empreendimento	= 5;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			//CADASTRA INDIQUE NO BANCO DO NEXGROUP
			$dataNex = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'nome_remetente'		=> $nome_remetente,
				'email_remetente'		=> $email_remetente,
				'nome_destinatario'		=> $nome_amigo,
				'email_destinatario'	=> $email_amigo,
				'comentarios'			=> $comentarios,
				'origem'				=> $_SESSION['origem'],
				'url'					=> $_SESSION['url'],
				'senha'					=> "}3_$%wB`7Atbdk1w"
			);
			
			$url= "http://www.nexgroup.com.br/index.php/integracao/cadastraIndique";
			
			$this->cadastraNex($dataNex, $url);
			//FIM CADASTRA INDIQUE NO BANCO DO NEXGROUP
			
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>VERGÉIS</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_capa.jpg" alt="GRUPO CAPA" /></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>VERGÉIS</b> :: Indicação via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2">Indicação enviada por <strong><?=$nome_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>			  
				  <tr>
				    <td colspan="2">Olá <?=$nome_amigo?>, acesse o site do Vergéis<br/><strong><a href="http://www.vergeis.com.br">Clique aqui</a></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">Nome do remetente:</td>
				    <td><strong><?=$nome_remetente?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentarios?></strong></td>
				  </tr>					  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	GRUPO CAPA
				    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email_remetente", "$nome_remetente");
			
			//if($nome_remetente=="teste123"){
				$list = array('douglas.otto@divex.com.br');
			/*}else{
			$list = array(
							"$email_amigo"
						);	
			}*/
			$this->email->to($list);
			$this->email->subject('VERGÉIS - Indicação');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			echo "Indicação Enviada!";
			//$this->load->view('home/ajax.indique.php');
		
	}
	
	function enviarContato ()
	{
		$this->load->model('home_model', 'model');
		
			$nome 			= $this->input->post('nome');
			$email 			= $this->input->post('email');
			$endereco 		= $this->input->post('endereco');
			$cidade 		= $this->input->post('cidade');
			$uf 			= $this->input->post('uf');
			$cep 			= $this->input->post('cep');
			$telefone 		= $this->input->post('telefone');
			$comentarios 	= $this->input->post('comentarios');
			
			$id_empreendimento	= 5;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			
			//CADASTRA CONTATO NO BANCO DO NEXGROUP
			$dataNex = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'nome'					=> $nome,
				'email'					=> $email,
				'endereco'				=> $endereco,
				'cidade'				=> $cidade,
				'uf'					=> $uf,
				'cep'					=> $cep,
				'telefone'				=> $telefone,
				'comentarios'			=> $comentarios,
				'origem'				=> $_SESSION['origem'],
				'url'					=> $_SESSION['url'],
				'senha'					=> "}3_$%wB`7Atbdk1w"
				
			);
			
			$url= "http://www.nexgroup.com.br/index.php/integracao/cadastraContato";
			
			$this->cadastraNex($dataNex, $url);
			//FIM CADASTRA CONTATO NO BANCO DO NEXGROUP
			
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>VERGÉIS</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.capa.com.br" target="_blank"><img width="90px" border="0" src="http://www.capa.com.br/_img/2/logo_capa.jpg" alt="GRUPO CAPA" /></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>VERGÉIS</b> :: Contato via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2">Contato enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>				  
				  <tr>
				    <td width="30%">E-mail:</td>
				    <td><strong><?=$email?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Endereço:</td>
				    <td><strong><?=$endereco?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Cidade:</td>
				    <td><strong><?=$cidade?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">UF:</td>
				    <td><strong><?=$uf?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">CEP:</td>
				    <td><strong><?=$cep?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Telefone:</td>
				    <td><strong><?=$telefone?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentarios?></strong></td>
				  </tr>				  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	GRUPO CAPA
				    	<a href="http://www.capa.com.br">www.capa.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email", "$nome");
			
			
				$list = array(
						'douglas.otto@divex.com.br'
				);
				
					
			$this->email->to($list);
			//$this->email->bcc('atendimento@divex.com.br');
			$this->email->subject('VERGÉIS - Contato enviado pelo site');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			//$this->load->view('home/ajax.contato.php');
	}
	
	function cadastraNex($data,$url){
		//set POST variables
	
		$data_string = "";
		//url-ify the data for the POST
		foreach($data as $key=>$value){ 
			$data_string .= $key.'='.$value.'&'; 
		}
		rtrim($data_string,'&');
		
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($data));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data_string);
		
		//execute post
		$result = curl_exec($ch);
		
		//close connection
		curl_close($ch);
	
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */