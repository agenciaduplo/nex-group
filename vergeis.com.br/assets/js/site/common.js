

	// Aqui estão todas as chamadas para comportamentos que são utilizados em todo site

	$(function() {
		
		$("#apartamentos").click(function(e) {
			$("#dormitorios").load('http://www.vergeis.com.br/home/dorm1');
			$("#SoliciteInformacoes").hide();
		});
		$("#infraestrutura").click(function(e) {
			$("#infra").load('http://www.vergeis.com.br/home/infra');
			$("#SoliciteInformacoes").hide();
		});
		$(".Lnk1Dorms").click(function(e) {
			e.preventDefault();
			$("#carousel-1").featureCarousel({
				'startingItem' : 0
			});
			$('#menu-dorms li').removeClass();
			$(this).parent().addClass('Active');
			$("#dormitorios").load('http://www.vergeis.com.br/home/dorm1');
			$("#SoliciteInformacoes").hide();
		});
		$(".Lnk2Dorms").click(function(e) {
			e.preventDefault();
			$("#carousel-2").featureCarousel({
				'startingItem' : 0
			});
			$('#menu-dorms li').removeClass();
			$(this).parent().addClass('Active');
			$("#dormitorios").load('http://www.vergeis.com.br/home/dorm2');
			$("#SoliciteInformacoes").hide();
		});
		$(".Lnk3Dorms").click(function(e) {
			e.preventDefault();
			$("#carousel-3").featureCarousel({
				'startingItem' : 0
			});
			$('#menu-dorms li').removeClass();
			$(this).parent().addClass('Active');
			$("#dormitorios").load('http://www.vergeis.com.br/home/dorm3');
			$("#SoliciteInformacoes").hide();
		});
		$(".Lnk4Dorms").click(function(e) {
			e.preventDefault();
			$("#carousel-4").featureCarousel({
				'startingItem' : 0
			});
			$('#menu-dorms li').removeClass();
			$(this).parent().addClass('Active');
			$("#dormitorios").load('http://www.vergeis.com.br/home/dorm4');
		});
		
		// Setar o primeiro campo de formulário com o foco
		//$("input[type=text]:first").focus();

		// Uma div vazia deve ser criada depois do .BotaoFechar para evitar que o mesmo desapareça no IE6
		$(".BotaoFechar").after("<div></div>");

		// Exibe e esconde os painéis dos formulários suspensos acionados pelos botões do topo
		$(".FeatureButton").click(function() {
			if ($(".SlidingPanel").is(":visible")) {
				$(".SlidingPanel").not($(this).next()).fadeOut("slow");
			}
			$(this).next().fadeToggle("slow");
			return false;
		});

		// Botão fechar dos formulários suspensos
		$(".BotaoFechar").click(function() {
			$(this).parent().fadeToggle("slow");
		});

		// Responsável pela navegação por âncoras
		$(".Navigation a, h1 a, .BotaoLocalizacao").click(function(event) {
			event.preventDefault();
			var full_url = this.href;
			var parts = full_url.split("#");
			var target = parts[1];
			var target_offset = $("#" + target).offset();
			
			if (target == 'Localizacao') {
				var target_top = target_offset.top - 81;
			}
			else {
				var target_top = target_offset.top - 98;
			}
			$('.Navigation a.Active').removeClass('Active');
			$(this).addClass('Active');
			$('html, body').animate({ scrollTop:target_top }, 500);
		});
		$('h1 a').click(function(event) {
			event.preventDefault();
			$('.Navigation a').removeClass('Active');
			$('a#home').addClass('Active');
			$("#SoliciteInformacoes").show();
		});
		$(".BotaoMontePlanta").click(function(event) {
			event.preventDefault();
			var full_url = this.href;
			var parts = full_url.split("#");
			var target = parts[1];
			var target_offset = $("#" + target).offset();
			if (target == 'Localizacao') {
				var target_top = target_offset.top - 81;
			}
			else {
				var target_top = target_offset.top - 98;
			}
			$('.Navigation a.Active').removeClass('Active');
			$(this).addClass('Active');
			$('html, body').animate({ scrollTop:target_top }, 500);
		});
		// Inicializa o carrossel de Apartamentos e Infraestrutura
		
	});	$(window).load(function() {
		// Força os links com rel="external" a abrirem em uma nova janela/aba
		$("a[rel*=external]").attr('target', '_blank');
		// Evita que o frame criado pelo widget do AddThis crie uma rolagem horizontal
		$("iframe[name*=twttrHubFrame]").css('left', '0');
	});		

	$(document).ready(function() {
		$(document).scroll(function(){
			if ($(document).scrollTop() > 0) {
				$('#SoliciteInformacoes').fadeOut('slow');
			} else {
				$('#SoliciteInformacoes').fadeIn('fast');
			}
		});
		
		$.validator.setDefaults({
			submitHandler: function() { enviaInteresse(); }
		}); 

			$("#FormInteresse").validate({
				errorElement: "",
				rules:{
					nome:{
						required: true
					},
					email:{
						required: true, email:true			
					},
					telefone:{
						required: true
					}
				},
				messages:{
					nome:{
						required: ""
					},
					email:{
						required: "",
						email: ""
					},
					telefone:{
						required: ""
					}
				}
			
			});
			
			$.validator.setDefaults({
				submitHandler: function() { enviaIndique(); }
			}); 

			$("#FormIndique").validate({
				errorElement: "",
				rules:{
					NomeRemetente:{
						required: true
					},
					EmailRemetente:{
						required: true, email:true			
					},
					NomeAmigo:{
						required: true
					},
					EmailAmigo:{
						required: true, email:true			
					}
				},
				messages:{
					NomeRemetente:{
					required: ""
					},
					EmailRemetente:{
						required: "", email:""		
					},
					NomeAmigo:{
						required: ""
					},
					EmailAmigo:{
						required: "", email:""			
					}
				}
			});
			
			$.validator.setDefaults({
				submitHandler: function() { enviaContato(); }
			}); 
			$("#FormContato").validate({
				errorElement: "",
				rules:{
					NomeContato:{
						required: true
					},
					EmailContato:{
						required: true, email:true			
					},
					TelefoneContato:{
						required: true
					}
				},
				messages:{
					NomeContato:{
						required: ""
					},
					EmailContato:{
						required: "",
						email: ""
					},
					TelefoneContato:{
						required: ""
					}
				}
			});
		
			$.validator.setDefaults({
				submitHandler: function() { enviaInformacoes(); }
			});
			$("#FormInformacoes").validate({
				errorElement: "",
				rules:{
					InfoNome:{
						required: true
					},
					InfoEmail:{
						required: true, email: true			
					},
					InfoCidade:{
						required: true
					},
					InfoTelefone:{
						required: true, digits: false, minlength: 8
					}
				},
				messages:{
					InfoNome:{
						required: ""
					},
					InfoEmail:{
						required: "",
						email: ""
					},
					InfoCidade:{
						required: ""
					},
					InfoTelefone:{
						required: ""
					}
				}
			
			});
		
			
		});
	
		function enviaInteresse(){
			
			$("#FormInteresse #Enviar").attr("disabled","disabled");
			
			var nome			= $("#Nome").val();
			var faixa_etaria	= $("#FaixaEtaria").val();
			var estado_civil	= $("#EstadoCivil").val();
			var bairro_cidade	= $("#bairroCidade").val();
			var profissao		= $("#Profissao").val();
			var telefone		= $("#Telefone").val();
			var email			= $("#Email").val();
			var comentarios		= $("#Comentario").val();
			
			if($('#ChkEmail').is(':checked') && $('#ChkTelefone').is(':checked'))
			{
				var contato = "E-mail e Telefone";
			}
			else if ($('#ChkEmail').is(':checked'))
			{
				var contato = "E-mail";
			}
			else if ($('#ChkTelefone').is(':checked'))
			{
				var contato = "Telefone";
			}
			var msg 	= '';
			var vet_dados 	= 'nome='+ nome
						  +'&faixa_etaria='+ faixa_etaria
						  +'&estado_civil='+ estado_civil
						  +'&bairro_cidade='+ bairro_cidade
						  +'&profissao='+ profissao
						  +'&telefone='+ telefone
						  +'&email='+ email
						  +'&contato='+ contato
						  +'&comentarios='+ comentarios;

			var base_url  	= "http://www.vergeis.com.br/vergeis/home/enviarInteresse";

			$.ajax({
				type: "POST",
				url: base_url,
				data: vet_dados,
				success: function(msg) {
					/* <!-- Google Code for Contato Conversion Page --> */
					window.google_conversion_id = 978900397;
					window.google_conversion_language = "pt"
					window.google_conversion_format = "2"
					window.google_conversion_color = "ffffff"
					window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
					window.google_conversion_value = 0;

					document.write = function(node){ $("body").append(node); }

					$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });

					$('#PainelTenhoInteresse .sucess').show();
					limpaCampos("#FormInteresse");
					$("#FormInteresse #Enviar").removeAttr("disabled");
					}
			});
			

			return false;
		}
		
		function enviaIndique(){
			
			$("#FormIndique #Enviar").attr("disabled","disabled");
			
			var nome_remetente		= $("#NomeRemetente").val();
			var email_remetente		= $("#EmailRemetente").val();
			var nome_amigo			= $("#NomeAmigo").val();
			var email_amigo			= $("#EmailAmigo").val();
			var comentarios			= $("#ComentarioIndique").val();
			
			
			var msg 	= '';
			var vet_dados 	= 'nome_remetente='+ nome_remetente
						  +'&email_remetente='+ email_remetente
						  +'&nome_amigo='+ nome_amigo
						  +'&email_amigo='+ email_amigo
						  +'&comentarios='+ comentarios;
						  
			var base_url  	= "http://www.vergeis.com.br/home/enviarIndique";
																	  
			$.ajax({
				type: "POST",
				url: base_url,
				data: vet_dados,
				success: function(msg) {
					/* <!-- Google Code for Contato Conversion Page --> */
	               	window.google_conversion_id = 978900397;
	               	window.google_conversion_language = "pt"
	               	window.google_conversion_format = "2"
	               	window.google_conversion_color = "ffffff"
	               	window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
	               	window.google_conversion_value = 0;

	               	document.write = function(node){ $("body").append(node); }

	               	$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });
	               	
					$('#PainelIndique .sucess').show();
					limpaCampos("#FormIndique");
					$("#FormIndique #Enviar").removeAttr("disabled");
				}
			});
			return false;
		}
		
		function enviaContato(){
			
			$("#FormContato #Enviar").attr("disabled","disabled");
			
			var nome			= $("#NomeContato").val();
			var email			= $("#EmailContato").val();
			var endereco		= $("#Endereco").val();
			var uf				= $("#Uf").val();
			var cidade			= $("#Cidade").val();
			var cep				= $("#Cep").val();
			var telefone		= $("#TelefoneContato").val();
			var comentarios		= $("#Mensagem3").val();
			
				
			var msg 	= '';
			vet_dados 	= 'nome='+ nome
						  +'&email='+ email
						  +'&endereco='+ endereco
						  +'&uf='+ uf
						  +'&cidade='+ cidade
						  +'&cep='+ cep
						  +'&telefone='+ telefone
						  +'&comentarios='+ comentarios;
						  
			base_url  	= "http://www.vergeis.com.br/home/enviarInteresse";
			
			$.ajax({
				type: "POST",
				url: base_url,
				data: vet_dados,
				success: function(msg) {
					$('.ContainerForm .sucess').show();
					limpaCampos("#FormContato");
					$("#FormContato #Enviar").removeAttr("disabled");
				}
			});
			return false;
		}

		function limpaCampos (form)
		{
		    $(form).find(':input').each(function() {

		        switch(this.type) {
		            case 'password':
		            case 'select-multiple':
		            case 'select-one':
		            case 'select':
		            case 'text':
		            case 'textarea':
		                $(this).val('');
		                break;
		            case 'checkbox':
		            case 'radio':
		                this.checked = false;
		        }
		    });
		}
		
		function enviaInformacoes(){
			$("#FormInformacoes #Enviar").attr("disabled", "disabled");

			var nome          = $("#InfoNome").val();
			var email         = $("#InfoEmail").val();
			var telefone      = $("#InfoTelefone").val();
			var bairro_cidade = $("#InfoCidade").val();
			var comentarios   = $("#InfoComentario").val();
			var vet_dados     = 'nome=' + nome + '&telefone=' + telefone + '&email=' + email + '&bairro_cidade=' + bairro_cidade + '&comentarios=' + comentarios;

			var base_url    = "http://www.vergeis.com.br/home/enviarInteresse";
			var msg         = '';

			$.ajax({
				type: "POST",
				url: base_url,
				data: vet_dados,
				success: function(msg) {
					/* <!-- Google Code for Contato Conversion Page --> */
					window.google_conversion_id = 978900397;
					window.google_conversion_language = "pt"
					window.google_conversion_format = "2"
					window.google_conversion_color = "ffffff"
					window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
					window.google_conversion_value = 0;

					document.write = function(node){ $("body").append(node); }

					$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {});

					limpaCampos("#FormInformacoes");
					alert('Requisição enviada com sucesso!');
					$("#FormInformacoes #Enviar").removeAttr("disabled");
				}
			});			
			return false;
		}