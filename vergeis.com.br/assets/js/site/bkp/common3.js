		function validaCadastro(){
			
			var nome			= $("#Nome").val();
			var email			= $("#Email").val();
			var error = 0;
			if(nome == ''){
				 $("#Nome").addClass('error');
				 error  = 1;
			}
			if(email == ''){
				 $("#Email").addClass('error');
				 error  = 1;
			}else{
				if(!checkMail(email)){
					 $("#Email").addClass('error');
					 error  = 1;
				}
			}
			
			if(error == 1)
				return false;
			else
				return true;
				
			
		}
		
		function checkMail(mail){
			var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
			if(typeof(mail) == "string"){
				if(er.test(mail)){ return true; }
			}else if(typeof(mail) == "object"){
				if(er.test(mail.value)){ 
							return true; 
						}
			}else{
				return false;
				}
		}
		