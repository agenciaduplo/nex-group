<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Controller {

	function Home()
	{
		parent::Controller();	
	}
	
	function index ()
	{
		redirect('home/principal');
	}
	
	function cadastro ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'body'			=> 'Login',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/cadastro.php', $data);
	}
	
	function EsqueciSenha ()
	{
		$this->load->model('home_model', 'model');
		$this->load->view('home/esqueci_minha_senha.php');
	}
	
	function atualizaSuaPlanta ()
	{
		$this->load->model('home_model', 'model');
		
		$idProject		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$dadosProject	= $this->model->getDadosProject($idProject);
		
		if($dadosProject->quantidade == 1)
		{
			$totDormitorios = "1 dormitório - $dadosProject->tipo";
		}
		else
		{
			$totDormitorios = "$dadosProject->quantidade dormitórios - $dadosProject->tipo";
		}
		
		$data = array (	
			'totDormitorios'	=> $totDormitorios
		);
		
		$this->load->view('home/ajax.suaplanta.php', $data);
		
	}
	
	function CadastrarDados ()
	{
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$nome 			= $this->input->post('nome');
		$sobrenome 		= $this->input->post('sobrenome');
		$email 			= $this->input->post('email');
		$telefone 		= $this->input->post('telefone');
		$celular 		= $this->input->post('celular');
		$senha 			= $this->encrypt->encode($this->input->post('senha'));
		
		$verificaEmail = $this->model->verificaEmail($email);
		
		if($verificaEmail)
		{
			$dataEmail = array (
				'color'			=> "red",	
				'msg'			=> 'O e-mail informado já está cadastrado.'
			);
		}
		else
		{
			$data = array (	
				'id_dormitorio'		=> 1,
				'id_tipo_planta'	=> 6,
				'ativo'				=> 1,
				'data_cadastro'		=> date("Y-m-d H:i:s"),
				'nome'				=> $nome,
				'sobrenome'			=> $sobrenome,
				'email'				=> $email,
				'telefone'			=> $telefone,
				'celular'			=> $celular,
				'senha'				=> $senha
			);	
			
			$this->model->setCadUsuario($data);
			
			$dataEmail = array (	
				'msg'			=> 'Cadastro realizado com sucesso!'
			);
			
		}
		
		$this->load->view('home/mostra_insert.php', $dataEmail);		
		
	}
	
	function principal ($e = "")
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		if($e == "e")
		{
			$erro = "S";
		}
		else
		{
			$erro = "N";
		}
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'body'			=> 'Login',
			'verificaLogin'	=> $verificaLogin,
			'erro'			=> $erro
		);
		
		$this->load->view('home/principal.php', $data);
	}
	
	function EnviaSenha ()
	{
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$email = $this->input->post('email');
		
		$verificaEmail = $this->model->verificaEmail($email);
		
		if($verificaEmail)
		{
			
			$data = array (
				'color'			=> "green",	
				'msg'			=> 'Senha enviada para o e-mail cadastrado.'
			);
			$this->load->view('home/recupera.senha.php', $data);
			
		}
		else
		{	
			$data = array (	
				'color'			=> "red",
				'msg'			=> 'O E-mail informado não está cadastrado.'
			);
			$this->load->view('home/recupera.senha.php', $data);
		}
		
	}
	
	function enviaDados ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		if($_POST)
		{
			$nome			= $this->input->post('nome');
			$email			= $this->input->post('email');
			$url			= $this->input->post('url');
			$origem			= $this->input->post('origem');
			$data_envio		= date("Y-m-d H:i:s");
			
			$verifica 	= $this->model->verificaEmailEnt($email);
			
			$data = array (	
				'origem'		=> $origem,
				'url'			=> $url,
				'nome'			=> $nome,
				'email'			=> $email,
				'data_envio'	=> $data_envio
			);
			
			if(!$verifica)
			{
				$this->model->setEmail($data);
			}
		}
	}
	
	function visitante ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'dormitorios'	=> $this->model->getDormitorios(),
			'body'			=> 'Interna Dormitorios',
			'verificaLogin'	=> @$verificaLogin
		);
		
		$this->load->view('home/visitante.php', $data);
	}
	
	function editar ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'dormitorios'	=> $this->model->getDormitorios(),
			'body'			=> 'Interna Dormitorios',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/editar.php', $data);
	}
	
	function termos_de_uso ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'dormitorios'	=> $this->model->getDormitorios(),
			'body'			=> 'Interna TermosDeUso',
			'verificaLogin'	=> $verificaLogin
		);
		
		$this->load->view('home/termos-de-uso.php', $data);
	}
	
	function cliente ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		if(@$verificaLogin != "0"):
			
			$data = array (	
				'dormitorios'	=> $this->model->getDormitorios(),
				'body'			=> 'Interna Dormitorios',
				'verificaLogin'	=> $verificaLogin
			);
			$this->load->view('home/visitante.php', $data);
			
		else:
		
			redirect('home/visitante');
			
		endif;
	}
	
	function cliente2 ()
	{	
		$this->load->model('home_model', 'model');
		$this->load->library('Utilidades');
		
		$verificaLogin = $this->auth->check();
		
		if(@$verificaLogin != "0"):
			
			$data = array (	
				'dormitorios'	=> $this->model->getDormitorios(),
				'body'			=> 'Interna Dormitorios',
				'verificaLogin'	=> $verificaLogin
			);
			$this->load->view('home/visitante2.php', $data);
			
		else:
		
			redirect('home/visitante');
			
		endif;
	}
	
	function buscaTiposPlantas ()
	{
		$this->load->model('home_model', 'model');
	
		$id_dormitorio = $this->input->post('id_dormitorio');
		
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'verificaLogin'	=> $verificaLogin,
			'tipos_plantas'	=> $this->model->getTiposPlantas($id_dormitorio)
		);
		
		$this->load->view('home/ajax.tipos.php', $data);
	}
	
	function imprimir ()
	{
		$this->load->model('home_model', 'model');
	
		$id_tipo_planta = 6;
		
		$planta			= $this->model->getPlantasDorms($id_tipo_planta);
		
		$verificaLogin = $this->auth->check();
		
		
		$id_project = $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaComodos = $this->model->verificaComodos($id_project);
		
		if($verificaComodos)
		{
			$editar = 1;
		}
		else
		{
			$editar = 0;
		}
		
		$data = array (
			'verificaLogin'		=> $verificaLogin,	
			'comodos'			=> $this->model->getComodos($id_tipo_planta),
			'id_tipo_planta'	=> $id_tipo_planta,
			'editar'			=> $editar,
			'id_projeto'		=> $id_project
		);
		
		if($editar == 1)
		{
			if(@$planta->id_dormitorio == 1)
			{
				$this->load->view('home/ajax.comodos.1.print.php', $data);
			}
			elseif(@$planta->id_dormitorio == 2)
			{
				$this->load->view('home/ajax.comodos.2.editar.php', $data);
			}
			elseif(@$planta->id_dormitorio == 3)
			{
				$this->load->view('home/ajax.comodos.3.editar.php', $data);
			}
			elseif(@$planta->id_dormitorio == 4)
			{
				$this->load->view('home/ajax.comodos.4.editar.php', $data);
			}			
		
		
		}
		else
		{
			if(@$planta->id_dormitorio == 1)
			{
				$this->load->view('home/ajax.comodos.1.php', $data);
			}
			elseif(@$planta->id_dormitorio == 2)
			{
				$this->load->view('home/ajax.comodos.2.php', $data);
			}
			elseif(@$planta->id_dormitorio == 3)
			{
				$this->load->view('home/ajax.comodos.3.php', $data);
			}
			elseif(@$planta->id_dormitorio == 4)
			{
				$this->load->view('home/ajax.comodos.4.php', $data);
			}			
		}
		
	}
	
	function buscaComodos ()
	{
		$this->load->model('home_model', 'model');
	
		$id_tipo_planta = $this->input->post('id_tipo_planta');
		$planta			= $this->model->getPlantasDorms($id_tipo_planta);
		$verificaLogin 	= $this->auth->check();
		
		if($this->session->userdata('id_projeto'))
		{
			$id_project = $this->encrypt->decode($this->session->userdata('id_projeto'));
		}
		else
		{
			$id_project = 6;
		}
		$verificaComodos = $this->model->verificaComodos($id_project);
		
		if($verificaComodos)
		{
			$editar = 1;
		}
		else
		{
			//ajustar depois
			$editar = 1;
		}
		
		$data = array (
			'verificaLogin'		=> $verificaLogin,	
			'comodos'			=> $this->model->getComodos($id_tipo_planta),
			'id_tipo_planta'	=> $id_tipo_planta,
			'editar'			=> $editar,
			'id_projeto'		=> $id_project
		);
		
		if($editar == 1)
		{
			if(@$planta->id_dormitorio == 1)
			{
				$this->load->view('home/ajax.comodos.1.editar.php', $data);
			}
			elseif(@$planta->id_dormitorio == 2)
			{
				$this->load->view('home/ajax.comodos.2.editar.php', $data);
			}
			elseif(@$planta->id_dormitorio == 3)
			{
				$this->load->view('home/ajax.comodos.3.editar.php', $data);
			}
			elseif(@$planta->id_dormitorio == 4)
			{
				$this->load->view('home/ajax.comodos.4.editar.php', $data);
			}			
		
		
		}
		else
		{
			if(@$planta->id_dormitorio == 1)
			{
				$this->load->view('home/ajax.comodos.1.php', $data);
			}
			elseif(@$planta->id_dormitorio == 2)
			{
				$this->load->view('home/ajax.comodos.2.php', $data);
			}
			elseif(@$planta->id_dormitorio == 3)
			{
				$this->load->view('home/ajax.comodos.3.php', $data);
			}
			elseif(@$planta->id_dormitorio == 4)
			{
				$this->load->view('home/ajax.comodos.4.php', $data);
			}			
		}
	}
	
	function comodos ()
	{
		$this->load->model('home_model', 'model');
	
		$id_tipo_planta = 6;
		$verificaLogin = $this->auth->check();
		
		$data = array (	
			'verificaLogin'		=> $verificaLogin,
			'comodos'	=> $this->model->getComodos($id_tipo_planta)
		);
		
		$this->load->view('home/ajax.comodos.php', $data);	
	}
	
	function enviaDadosUm ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		$cozinha			= $this->input->post('cozinha');
		$quarto				= $this->input->post('quarto');
		$living				= $this->input->post('living');
		$banheiro			= $this->input->post('banheiro');
		
		$id_dormitorio		= $this->input->post('dormitorios');
		$id_tipo_planta		= $this->input->post('tiposPlantas');
		
		$updateProjeto		= $this->model->updateProject($id_projeto, $id_dormitorio, $id_tipo_planta);
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
			
			$dataProjeto	= date("Y-m-d H:i:s");
		
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		}
		else
		{
			$dataProjeto	= date("Y-m-d H:i:s");
		
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		}
		
		//$this->model->setProjetoVariacao();
		
		$data = array (	
			'cozinha'	=> $this->input->post('cozinha'),
			'quarto'	=> $this->input->post('quarto'),
			'living'	=> $this->input->post('living'),
			'banheiro'	=> $this->input->post('banheiro')
		);
		
		$this->load->view('home/ajax.dados.php', $data);		
	}
	
	function enviaDadosDois ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		$cozinha			= $this->input->post('cozinha');
		$casal				= $this->input->post('casal');
		$living				= $this->input->post('living');
		$banheiro			= $this->input->post('banheiro');
		$casaldois			= $this->input->post('casaldois');
		$jantar				= $this->input->post('jantar');
		$solt				= $this->input->post('solt');
		
		$tipo				= $this->input->post('tipo');
		
		$id_dormitorio		= $this->input->post('dormitorios');
		$id_tipo_planta		= $this->input->post('tiposPlantas');
		
		$updateProjeto		= $this->model->updateProject($id_projeto, $id_dormitorio, $id_tipo_planta);
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
		}
		
		$dataProjeto	= date("Y-m-d H:i:s");
			
		if($tipo == "A")
		{
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);			
		}
		elseif($tipo == "B")
		{
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			//$this->model->setProjetoVariacao($id_projeto, $casaldois, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
		}
		elseif($tipo == "C")
		{	
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
		}
		elseif($tipo == "D")
		{
			$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $jantar, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $casal, $dataProjeto);
			$this->model->setProjetoVariacao($id_projeto, $solt, $dataProjeto);
		}
		
		//exit;
		
		$this->load->view('home/ajax.dados.php', @$data);		
	}
	
	function enviaDadosTres ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		
		$cozinha 		= $this->input->post('cozinha');
		$living 		= $this->input->post('living');
		$quartosol 		= $this->input->post('quartosol');
		$anexo 			= $this->input->post('anexo');
		$quarto 		= $this->input->post('quarto');
		$banheiro 		= $this->input->post('banheiro');
		
		$id_dormitorio		= $this->input->post('dormitorios');
		$id_tipo_planta		= $this->input->post('tiposPlantas');
		
		$updateProjeto		= $this->model->updateProject($id_projeto, $id_dormitorio, $id_tipo_planta);
		
		//$tipo				= $this->input->post('tipo');
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
		}
		
		$dataProjeto	= date("Y-m-d H:i:s");
			
		$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quartosol, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $anexo, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		
		//exit;
		
		$this->load->view('home/ajax.dados.php', @$data);		
	}
	
	function enviaDadosQuatro ()
	{
		$this->load->model('home_model', 'model');
		
		$id_projeto 		= $this->encrypt->decode($this->session->userdata('id_projeto'));
		$verificaProjeto 	= $this->model->getProjetoVariacoes($id_projeto);
		
		
		$banheiro 		= $this->input->post('banheiro');
		$cozinha 		= $this->input->post('cozinha');
		$living 		= $this->input->post('living');
		$quarto		 	= $this->input->post('quarto');
		$quartoa 		= $this->input->post('quartoa');
		$quartob 		= $this->input->post('quartob');
		$quartoc 		= $this->input->post('quartoc');
		
		$id_dormitorio		= $this->input->post('dormitorios');
		$id_tipo_planta		= $this->input->post('tiposPlantas');
		
		$updateProjeto		= $this->model->updateProject($id_projeto, $id_dormitorio, $id_tipo_planta);
		
		//$tipo				= $this->input->post('tipo');
		
		if($verificaProjeto)
		{
			$this->model->LimpaProjetosVariacoes($id_projeto);
		}
		
		$dataProjeto	= date("Y-m-d H:i:s");
			
		$this->model->setProjetoVariacao($id_projeto, $banheiro, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $cozinha, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $living, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quarto, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quartoa, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quartob, $dataProjeto);
		$this->model->setProjetoVariacao($id_projeto, $quartoc, $dataProjeto);
		
		//exit;
		
		$this->load->view('home/ajax.dados.php', @$data);		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */