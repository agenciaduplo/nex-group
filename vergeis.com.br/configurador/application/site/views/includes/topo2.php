<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
    <title>Vergeis</title>
    
	<meta name="description" content="Amplie seu mundo - Estúdio, 2, 3, 4 dormitórios e coberturas - Vergeis" />
	<meta name="keywords" content="vergeis, apartamento, apto, porto alegre, poa, rs, rio grande do sul, estúdio, 2 dormitórios, 3 dormitórios, 4 dormitórios, coberturas" />
	<meta name="author" content="Divex Tecnologia" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="revisit-after" content="1 days" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->
	
	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/master.css" />
	<link rel="stylesheet" type="text/css" media="print" href="<?=base_url()?>assets/css/site/print.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.printElement.min.js"></script>
	
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	
	<!--
	<script src="<?=base_url()?>assets/js/site/jquery.alerts.js" type="text/javascript"></script>
	<link href="<?=base_url()?>assets/css/site/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="<?=base_url()?>assets/js/site/jquery.ui.draggable.js" type="text/javascript"></script>
	-->
	
	<script>
		$('a.Modal').fancybox({
			'width' : 535,
			'height' : 500,
			'hideOnOverlayClick':false,
			'type' : 'iframe'
        });
        
	   
	</script>
	
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<!--[if IE 6]>
		<script src="<?=base_url()?>assets/js/site/DD_belatedPNG_0.0.8a-min.js"></script>
		<script>
			
			DD_belatedPNG.fix('h1, .AcessePng, .NavPrincipal, .NavTermos, .NavEmpreendimento, .NavCapa, .Wrapper, img, .btnEditar');
	
		</script>
	<![endif]--> 
	
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-52']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>

<?php
	/*
	echo "<pre>";
		print_r($_COOKIE);
	echo "</pre>";
	echo $this->encrypt->decode($this->session->userdata('id_projeto'));
	*/
?>

<body class="<?=@$body?>">
	<div class="Full">

    	<div class="Header">
        	<div class="Container">

            	<h1></h1>

				<ul class="BreadCrumbs" id="Crumbs" style="display:none">
                	<li class="BreadLabel"><span>Planta:</span></li>
                    <li style="display:none" class="BreadDorms"><a id="BreadDorms" href="javascript: tiposDorms();"></a></li>
                    <li style="display:none" class="BreadTipo"><a id="BreadTipo" href="javascript: tiposPlantas();"></a></li>
                </ul>

				<?php
				
					//echo $this->session->userdata('login_dorms');
					//echo "<br/>";
					//echo $this->session->userdata('login_tipos');
					//echo "<br/>";
				
				?>
				
				<?php if(@$verificaLogin != "0" && $this->uri->segment(2) != 'principal'): ?>
				<div class="SuaPlanta">
					<?php
						$idProject		= $this->encrypt->decode($this->session->userdata('id_projeto'));
						$dadosProject	= $this->model->getDadosProject($idProject);
						
						if($dadosProject->quantidade == 1)
						{
							$totDormitorios = "1 dormitório - $dadosProject->tipo";
						}
						else
						{
							$totDormitorios = "$dadosProject->quantidade dormitórios - $dadosProject->tipo";
						}
					?>
					<p>
						<span>Atenção:</span>
						&nbsp; Atualmente a sua planta é <span><?=@$totDormitorios?></span>
						&nbsp;&nbsp; | &nbsp;&nbsp; Se você salvar outra planta a troca será realizada automaticamente.
					</p>
					
				</div>
				<?php endif; ?>

				<div class="Conta">
				
					<?php if(@$verificaLogin != "0"): ?>
	                    <p>Ol&aacute;, <strong><?=$this->session->userdata('login_nome');?></strong></p>
	                    <ul>
	                        <li><a href="<?=site_url()?>/home/cliente" title="Minha Conta">Minha Conta</a></li>
	                        <li><a href="<?=site_url()?>/login/sair" title="Sair">Sair</a></li>
	                    </ul>
                   <?php else: ?>
                    	<!--Acesso visitante -->                    
                    
                    	<p>Ol&aacute;, <strong>Visitante</strong></p>
                    	<ul class="AcessePng"><li><a href="<?=site_url()?>" title="Minha Conta">Acesse sua conta</a></li></ul>
                    <?php endif; ?>
                    
                </div><!--fecha Conta-->

            	<ul class="Navegation">
                	<li><a href="<?=site_url()?>" class="NavPrincipal" title="Principal">Principal</a></li>
                    <!-- <li><a href="<?=site_url()?>/home/termos_de_uso" class="NavTermos" title="Termos de uso">Termos de uso</a></li> -->
                    <li><a href="http://www.vergeis.com.br" class="NavEmpreendimento" target="_blank" title="Site do empreendimento">Site do empreendimento</a></li>
                    <li><a href="http://www.nexgroup.com.br" class="NavCapa" target="_blank" title="Site Capa">Site Capa</a></li>
                </ul><!--fecha Navegation-->

            </div><!--fecha Container-->
        </div><!--fecha Header-->