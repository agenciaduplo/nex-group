	<div class="Main">

        <div class="Projeto">        
			
			<?php if($id_tipo_planta == 9): ?>
			<div class="Planta"><img src="<?=base_url()?>assets/img/site/4dorm-tipoA/planta.png" alt="" /></div>
			<?php endif; ?>
			
			<?php 
				$verificaProjeto 	= $this->model->verificaProjeto($id_projeto); 
				if(count($verificaProjeto) == 0)
				{
					$exiteDados = "N";	
				}
				
				$confProjeto		= $this->model->confProjeto($id_projeto);
				$idTipoPlanta		= $confProjeto->id_tipo_planta;
				
			?> 	 	
			
			<?php foreach ($comodos as $row): ?>
				
				<?php if($id_tipo_planta == 9): ?>
				
				<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; $verificou = ""; ?>
								
								<?php $totalBanheiros = count($banheiros); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowBanheiro->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formBanheiro" name="banheiro" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contBanheiro == $totalBanheiros) :
												$display = "Block";
												?> <input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contBanheiro == $totalBanheiros) :
											$display = "Block";
											?> <input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?=@$display?> <?php //if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
			   	
			   	<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodos($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; $verificou = ""; ?>
								
								<?php $totalCozinhas = count($cozinhas); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowCozinha->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formCozinha" name="cozinha" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contCozinha == $totalCozinhas) :
												$display = "Block";
												?> <input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contCozinha == $totalCozinhas) :
											$display = "Block";
											?> <input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?=@$display?> <?php //if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->	
	
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; $verificou = ""; ?>
								
								<?php $totalLivings = count($livings); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowLiving->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formLiving" name="living" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contLiving == $totalLivings) :
												$display = "Block";
												?> <input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contLiving == $totalLivings) :
											$display = "Block";
											?> <input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?=@$display?> <?php //if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
				
				<!-- Quartos -->
				<?php if($row->comodo == "Dorm Casal"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; $verificou = ""; ?>
								
								<?php $totalQuartos = count($quartos); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuarto->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formQuarto" name="quarto" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contQuarto == $totalQuartos) :
												$display = "Block";
												?> <input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contQuarto == $totalQuartos) :
											$display = "Block";
											?> <input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?=@$display?> <?php //if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos -->
			   	
			   	<!-- Quarto A -->
				<?php if($row->comodo == "Dorm A"): ?>
	
					<?php
						$quartoa = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartoa): ?>
						<div id="DormA">
							<div id="quartoAPai">
							<?php $contQuartoA = 0; ?>
							<?php foreach ($quartoa as $rowQuartoA): ?>
								<?php $contQuartoA++; $verificou = ""; ?>
								
								<?php $totalQuartosA = count($quartoa); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuartoA->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formQuartoA" name="quartoa" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contQuartoA == $totalQuartosA) :
												$display = "Block";
												?> <input type="hidden" id="formQuartoA" name="quartoa" value="<?=$rowQuartoA->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contQuartoA == $totalQuartosA) :
											$display = "Block";
											?> <input type="hidden" id="formQuartoA" name="quartoa" value="<?=$rowQuartoA->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="quartoa-<?=$rowQuartoA->id_variacao_comodo?>" class="DormA-0<?=$contQuartoA?> <?=@$display?> <?php //if($contQuartoA == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoA->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosA > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoA');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto A -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto A -->
			   	
			   	<!-- Quarto B -->
				<?php if($row->comodo == "Dorm B"): ?>
	
					<?php
						$quartob = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartob): ?>
						<div id="DormB">
							<div id="quartoBPai">
							<?php $contQuartoB = 0; ?>
							<?php foreach ($quartob as $rowQuartoB): ?>
								<?php $contQuartoB++; $verificou = ""; ?>
								
								<?php $totalQuartosB = count($quartob); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuartoB->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formQuartoB" name="quartob" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contQuartoB == $totalQuartosB) :
												$display = "Block";
												?> <input type="hidden" id="formQuartoB" name="quartob" value="<?=$rowQuartoB->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contQuartoB == $totalQuartosB) :
											$display = "Block";
											?> <input type="hidden" id="formQuartoB" name="quartob" value="<?=$rowQuartoB->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="quartob-<?=$rowQuartoB->id_variacao_comodo?>" class="DormB-0<?=$contQuartoB?> <?=@$display?> <?php //if($contQuartoB == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoB->imagem?>" alt=""  />
				                    
				                    
				                    <?php if($totalQuartosB > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoB');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto B -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto B -->
			   	
			   	<!-- Quarto C -->
				<?php if($row->comodo == "Dorm C"): ?>
	
					<?php
						$quartoc = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($quartoc): ?>
						<div id="DormC">
							<div id="quartoCPai">
							<?php $contQuartoC = 0; ?>
							<?php foreach ($quartoc as $rowQuartoC): ?>
								<?php $contQuartoC++; $verificou = ""; ?>
								
								<?php $totalQuartosC = count($quartoc); ?>
								
								<?php
									$verificaVarComodo = $this->model->verificaVarComodo($rowQuartoC->id_variacao_comodo, $id_projeto);
									
									//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
									
									if(@$verificaVarComodo) 
									{ 
										$verificou = "S";
										$display = "Block"; 
										?> <input type="hidden" id="formQuartoC" name="quartoc" value="<?=$verificaVarComodo->id_variacao_comodo?>" /> <?php
									
									} 
									else
									{ 
										$display = "DisplayNone"; 
									}
									
									if(@$verificou == "S")
									{
										
									}
									else
									{
										if(@$exiteDados == "N"):
										
											if($contQuartoC == $totalQuartosC) :
												$display = "Block";
												?> <input type="hidden" id="formQuartoC" name="quartoc" value="<?=$rowQuartoC->id_variacao_comodo?>" /> <?php
											else:
												$display = "DisplayNone";
											endif;
												
										endif;
										
									}
									
									if(@$idTipoPlanta != @$id_tipo_planta)
									{
										if($contQuartoC == $totalQuartosC) :
											$display = "Block";
											?> <input type="hidden" id="formQuartoC" name="quartoc" value="<?=$rowQuartoC->id_variacao_comodo?>" /> <?php
										
										else:
											$display = "DisplayNone";
										endif;
									}
								?>
				                
				                <div id="quartoc-<?=$rowQuartoC->id_variacao_comodo?>" class="DormC-0<?=$contQuartoC?> <?=@$display?> <?php //if($contQuartoC == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuartoC->imagem?>" alt=""  />
				                    
				                    <?php if($totalQuartosC > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuartoC');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                </div>
							<?php endforeach; ?>
			            	</div>
			            </div><!-- fecha Quarto C -->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quarto C -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
		   	
		   	<?php endforeach; ?>
		
        </div><!--fecha Projeto-->

		<div id="ListaOpcoesComodos">

			<div id="OpcoesCozinhas" class="DisplayNone">
			<?php
				if($cozinhas): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Cozinhas</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($cozinhas as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="cozinhas<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesLiving" class="DisplayNone">
			<?php
				if($livings): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Livings</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($livings as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="livings<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuarto" class="DisplayNone">
			<?php
				if($quartos): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quartos</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartos as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartos<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoA" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto A</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartoa as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartoa<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartosA) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoA('<?=$row->id_variacao_comodo?>', 'quartoa<?=$cont?>');" title="Quarto A"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoB" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto B</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartob as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartob<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartosB) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoB('<?=$row->id_variacao_comodo?>', 'quartob<?=$cont?>');" title="Quarto A"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesQuartoC" class="DisplayNone">
			<?php
				if($quartoa): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Quarto C</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($quartoc as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>
					
			        	<li id="quartoc<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartosC) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuartoC('<?=$row->id_variacao_comodo?>', 'quartoc<?=$cont?>');" title="Quarto C"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
		    
		    <div id="OpcoesBanheiro" class="DisplayNone">
			<?php
				if($banheiros): ?>
				<?php $cont = 0; ?>
				<ul class="OpcoesComodos">
		
			    	<li class="TituloOpcoes">
			        	<h5>Banheiros</h5>
			        </li><!--fecha TituloOpcoes-->
			
					<?php foreach ($banheiros as $row): $cont++; ?>
					
					<?php
						$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
						if(@$verificaVarComodo) 
						{ 
							$comodoSel = "class='ComodoSelecionado'"; 
						}
						else
						{
							$comodoSel = "";
						}
					?>	
					
			        	<li id="banheiros<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
					<?php endforeach; ?>
			
			    </ul><!--fecha OpcoesComodos-->
			<?php endif; ?>
		    </div>
	    
	    </div>

    </div><!--fecha Main-->	