<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
    <title>Vergeis</title>

	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/master.css" />
	<link rel="stylesheet" type="text/css" media="print" href="<?=base_url()?>assets/css/site/print.css" />
<style>
@media print {
@page {
size: A4 landscape;
}
}
</style>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.printElement.min.js"></script>


<body class="Dorm1TipoA" style="zoom: 100%;">
	<div class="Full">
		<div class="Wrapper">
			<div class="Container">
				<div class="Main">
					<div id="divComodos">

						<div class="Main">

					        <div class="Projeto">        
			
								<?php if($id_tipo_planta == 6): ?>
			
									<div class="Planta"><img src="<?=base_url()?>assets/img/site/1dorm-tipoA/planta.png" alt="" /></div>
			
								<?php elseif ($id_tipo_planta == 7): ?>
			
									<div class="Planta"><img src="<?=base_url()?>assets/img/site/1dorm-tipoB/planta.png" alt="" /></div>						
			
								<?php endif; ?>
				 	
			
								<?php foreach ($comodos as $row): ?>
				
									<?php if($id_tipo_planta == 6 || $id_tipo_planta == 7): ?>
	
									<!-- Livings -->
									<?php if($row->comodo == "Living"): ?>
	
										<?php
											$livings = $this->model->getVariacoesComodos($row->id_comodo);
										?>
					
										<?php if($livings): ?>
											<div id="Livings">
												<div id="livingPai">
												<?php $contLiving = 0; ?>
												<?php foreach ($livings as $rowLiving): ?>
													<?php $contLiving++; ?>
								
													<?php $totalLivings = count($livings); ?>
				                    
									                <?php if($contLiving == $totalLivings) : ?>
														<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
													<?php endif; ?>
								
														<?php
															$verificaVarComodo = $this->model->verificaVarComodo($rowLiving->id_variacao_comodo, $id_projeto);
										
															//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
										
															if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
														?>
				                
									                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?=@$display?> <?php //if($contLiving == 1) { echo "Block"; } ?>">
				                
									                <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                
									                </div>
				                
				            
								
												<?php endforeach; ?>
								            	</div>
								            </div><!--fecha AreasSociais-->	
								        <?php endif; ?>
								   	<?php endif; ?>
								   	<!-- Livings -->
			   	
			   	
								   	<!-- Cozinhas -->
									<?php if($row->comodo == "Cozinha"): ?>
	
										<?php
											$cozinhas = $this->model->getVariacoesComodos($row->id_comodo);
										?>
						
										<?php if($cozinhas): ?>
											<div id="Cozinhas">
												<div id="cozinhaPai">
												<?php $contCozinha = 0; ?>
												<?php foreach ($cozinhas as $rowCozinha): ?>
													<?php $contCozinha++; ?>
				                
									                	<?php $totalCozinhas = count($cozinhas); ?>
				                	
									                	<?php if($contCozinha == $totalCozinhas) : ?>
															<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
														<?php endif; ?>
									
														<?php
															$verificaVarComodo = $this->model->verificaVarComodo($rowCozinha->id_variacao_comodo, $id_projeto);
										
															//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
										
															if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
														?>
				                	
									                	<div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?=@$display?> <?php //if($contCozinha == 1) { echo "Block"; } ?>">
									                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				         
									                </div>
				                
												<?php endforeach; ?>
								            	</div>
								            </div><!--fecha Cozinhas-->	
								        <?php endif; ?>
								   	<?php endif; ?>
								   	<!-- Cozinhas -->
	
				
									<!-- Quartos -->
									<?php if($row->comodo == "Quarto"): ?>
	
										<?php
											$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
										?>
					
										<?php if($quartos): ?>
											<div id="DormCasal">
												<div id="quartoPai">
												<?php $contQuarto = 0; ?>
												<?php foreach ($quartos as $rowQuarto): ?>
													<?php $contQuarto++; ?>
				                
									                <?php $totalQuartos = count($quartos); ?>
				                
									                <?php if($contQuarto == $totalQuartos) : ?>
														<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
													<?php endif; ?>
								
														<?php
															$verificaVarComodo = $this->model->verificaVarComodo($rowQuarto->id_variacao_comodo, $id_projeto);
										
															//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
										
															if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
														?>
				                
									                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?=@$display?> <?php //if($contQuarto == 1) { echo "Block"; } ?>">
									                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    
									                </div>
								
												<?php endforeach; ?>
								            	</div>
								            </div><!--fecha Dormitorios-->	
								        <?php endif; ?>
								   	<?php endif; ?>
								   	<!-- Quartos -->
			   	
								   	<!-- Banheiros -->
									<?php if($row->comodo == "Banheiro"): ?>
	
										<?php
											$banheiros = $this->model->getVariacoesComodosDesc($row->id_comodo);
										?>
					
										<?php if($banheiros): ?>
											<div id="Banhos">
												<div id="banheiroPai">
												<?php $contBanheiro = 0; ?>
												<?php foreach ($banheiros as $rowBanheiro): ?>
													<?php $contBanheiro++; ?>
				                
									                	<?php $totalBanheiros = count($banheiros); ?>
				                	
									                	<?php if($contBanheiro == $totalBanheiros) : ?>
															<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
														<?php endif; ?>
									
														<?php
															$verificaVarComodo = $this->model->verificaVarComodo($rowBanheiro->id_variacao_comodo, $id_projeto);
										
															//echo "Aqui: ".$verificaVarComodo->id_variacao_comodo;
										
															if(@$verificaVarComodo) { $display = "Block"; } else { $display = "DisplayNone"; }
														?>
				                
									                	<div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?=@$display?> <?php //if($contBanheiro == 1) { echo "Block"; } ?>">
									                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                	
									                </div>
				                
												<?php endforeach; ?>
								            	</div>
								            </div><!--fecha Banheiros-->	
								        <?php endif; ?>
								   	<?php endif; ?>
								   	<!-- Banheiros -->
			   	
								   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
		   	
							   	<?php endforeach; ?>
		
					        </div><!--fecha Projeto-->

							<div id="ListaOpcoesComodos">

							<div id="OpcoesCozinhas" class="DisplayNone">
							<?php
								if($cozinhas): ?>
								<?php $cont = 0; ?>
								<ul class="OpcoesComodos">
	
							    	<li class="TituloOpcoes">
							        	<h5>Cozinhas</h5>
							        </li><!--fecha TituloOpcoes-->
		
									<?php foreach ($cozinhas as $row): $cont++; ?>
					
										<?php
											$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
											if(@$verificaVarComodo) 
											{ 
												$comodoSel = "class='ComodoSelecionado'"; 
											}
											else
											{
												$comodoSel = "";
											}
										?>
				
							        	<li id="cozinhas<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
									<?php endforeach; ?>
		
							    </ul><!--fecha OpcoesComodos-->
							<?php endif; ?>
						    </div>
	    
						    <div id="OpcoesLiving" class="DisplayNone">
							<?php
								if($livings): ?>
								<?php $cont = 0; ?>
								<ul class="OpcoesComodos">
	
							    	<li class="TituloOpcoes">
							        	<h5>Livings</h5>
							        </li><!--fecha TituloOpcoes-->
		
									<?php foreach ($livings as $row): $cont++; ?>
				
										<?php
											$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
											if(@$verificaVarComodo) 
											{ 
												$comodoSel = "class='ComodoSelecionado'"; 
											}
											else
											{
												$comodoSel = "";
											}
										?>
				
							        	<li id="livings<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
									<?php endforeach; ?>
		
							    </ul><!--fecha OpcoesComodos-->
							<?php endif; ?>
						    </div>
	    
						    <div id="OpcoesQuarto" class="DisplayNone">
							<?php
								if($quartos): ?>
								<?php $cont = 0; ?>
								<ul class="OpcoesComodos">
	
							    	<li class="TituloOpcoes">
							        	<h5>Quartos</h5>
							        </li><!--fecha TituloOpcoes-->
		
									<?php foreach ($quartos as $row): $cont++; ?>
				
										<?php
											$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
											if(@$verificaVarComodo) 
											{ 
												$comodoSel = "class='ComodoSelecionado'"; 
											}
											else
											{
												$comodoSel = "";
											}
										?>
				
							        	<li id="quartos<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
									<?php endforeach; ?>
		
							    </ul><!--fecha OpcoesComodos-->
							<?php endif; ?>
						    </div>
	    
						    <div id="OpcoesBanheiro" class="DisplayNone">
							<?php
								if($banheiros): ?>
								<?php $cont = 0; ?>
								<ul class="OpcoesComodos">
	
							    	<li class="TituloOpcoes">
							        	<h5>Banheiros</h5>
							        </li><!--fecha TituloOpcoes-->
		
									<?php foreach ($banheiros as $row): $cont++; ?>
				
										<?php
											$verificaVarComodo = $this->model->verificaVarComodo($row->id_variacao_comodo, $id_projeto);									
											if(@$verificaVarComodo) 
											{ 
												$comodoSel = "class='ComodoSelecionado'"; 
											}
											else
											{
												$comodoSel = "";
											}
										?>
				
							        	<li id="banheiros<?=$cont?>" <?=@$comodoSel?> <?php //if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
									<?php endforeach; ?>
		
							    </ul><!--fecha OpcoesComodos-->
							<?php endif; ?>
						    </div>
	    
						    </div>

					    </div><!--fecha Main-->	
					</div>
				</div>
			</div>
		</div><!--fecha Full-->

		<script>
			window.print()
		</script>
	
	</body>
</html>
