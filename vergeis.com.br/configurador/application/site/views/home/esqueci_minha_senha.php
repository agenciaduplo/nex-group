<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
 	<title>Vergeis - Configurador - Cadastro</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/formularios.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script>
		$(document).ready(function() {
			$("#FormSenha").validationEngine('attach', {
				promptPosition : "topLeft",
				onValidationComplete: function(form, status){
					if(status == true)
					{
						$("#btrnInteresseEnviar").attr("disabled","disabled");
						
						var email		= $("#txtLoginEmail").val();
							
						var msg 	= '';
						vet_dados 	= 	'email='+ email;
									  
						base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/EnviaSenha";
						
						$.ajax({
							type: "POST",
							url: base_url,
							data: vet_dados,
							success: function(msg) {
									$("#validaLoginn").html(msg);
									limpaCampos();
									$("#btrnInteresseEnviar").removeAttr("disabled");
									}
						});
					}
					else
					{
						return false;
					}
				}  
			});
			
			
		});
		function limpaCampos ()
		{
		    $("#FormSenha").find(':input').each(function() {
		        switch(this.type) {
		            case 'password':
		            case 'select-multiple':
		            case 'select-one':
		            case 'select':
		            case 'text':
		            case 'textarea':
		                $(this).val('');
		                break;
		            case 'checkbox':
		            case 'radio':
		                this.checked = false;
		        }
		    });
		}
	</script>
</head>
<body class="FrmEsqueciMinhaSenha">
	<div class="Wrapper">
		<ul style="display: block; margin-left:50px;">
			<li><img src="<?=base_url()?>assets/img/site/tit_esqueci-minha-senha.png"></li>
		</ul>
		<form id="FormSenha" name="FormEsqueciSenha" method="POST" style="width:413px; margin-left:50px;" />
			<ul class="Formulario">
				<li style="height:50px"><input type="text" id="txtLoginEmail" class="CampoEmail validate[required,custom[email]" name="email" value="E-mail" onfocus="if(this.value == 'E-mail') this.value=''" onblur="if(this.value == '') this.value = 'E-mail'" /></li>
				<li style="margin-top: 22px; font-family: Arial; font-size: 12px; color: red; font-weight: bold;" id="validaLoginn" class="Alert"></li>
				<li class="LiBotao Debug"><input type="submit" style="cursor:pointer;" src="/site/images/btn-enviar.png" class="BtEnviar" value="Enviar dados" id="btrnInteresseEnviar" name="btnEnviar" /></li>
			</ul><!-- Fecha Formulario-->
		</form>
	</div><!--fecha Wrapper-->
</body>
</html>