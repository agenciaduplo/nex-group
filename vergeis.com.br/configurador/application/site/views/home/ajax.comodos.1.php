	<div class="Main">

        <div class="Projeto">        
			
			<?php if($id_tipo_planta == 6): ?>
			
				<div class="Planta"><img src="<?=base_url()?>assets/img/site/1dorm-tipoA/planta.png" alt="" /></div>
			
			<?php elseif ($id_tipo_planta == 7): ?>
			
				<div class="Planta"><img src="<?=base_url()?>assets/img/site/1dorm-tipoB/planta.png" alt="" /></div>						
			
			<?php endif; ?>
				 	
			
			<?php foreach ($comodos as $row): ?>
				
				<?php if($id_tipo_planta == 6 || $id_tipo_planta == 7): ?>
	
				<!-- Livings -->
				<?php if($row->comodo == "Living"): ?>
	
					<?php
						$livings = $this->model->getVariacoesComodos($row->id_comodo);
					?>
					
					<?php if($livings): ?>
						<div id="Livings">
							<div id="livingPai">
							<?php $contLiving = 0; ?>
							<?php foreach ($livings as $rowLiving): ?>
								<?php $contLiving++; ?>
				                
				                <div id="living-<?=$rowLiving->id_variacao_comodo?>" class="Living0<?=$contLiving?> <?php if($contLiving == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowLiving->imagem?>" alt=""  />
				                    
				                    <?php $totalLivings = count($livings); ?>
				                    <?php if($totalLivings > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesLiving');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                   	<?php else: ?> 
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                
				                </div>
				                
				                <?php if($contLiving == $totalLivings) : ?>
									<input type="hidden" id="formLiving" name="living" value="<?=$rowLiving->id_variacao_comodo?>" />
								<?php endif; ?>
								
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha AreasSociais-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Livings -->
			   	
			   	
			   	<!-- Cozinhas -->
				<?php if($row->comodo == "Cozinha"): ?>
	
					<?php
						$cozinhas = $this->model->getVariacoesComodos($row->id_comodo);
					?>
						
					<?php if($cozinhas): ?>
						<div id="Cozinhas">
							<div id="cozinhaPai">
							<?php $contCozinha = 0; ?>
							<?php foreach ($cozinhas as $rowCozinha): ?>
								<?php $contCozinha++; ?>
				                
				                <div id="cozinha-<?=$rowCozinha->id_variacao_comodo?>" class="Cozinha0<?=$contCozinha?> <?php if($contCozinha == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowCozinha->imagem?>" alt="" />
				                    
				                    <?php $totalCozinhas = count($cozinhas); ?>
				                    <?php if($totalCozinhas > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesCozinhas');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                    <?php endif; ?>
				                </div>
				                
				                <?php if($contCozinha == $totalCozinhas) : ?>
									<input type="hidden" id="formCozinha" name="cozinha" value="<?=$rowCozinha->id_variacao_comodo?>" />
								<?php endif; ?>
				                
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Cozinhas-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Cozinhas -->
	
				
				<!-- Quartos -->
				<?php if($row->comodo == "Quarto"): ?>
	
					<?php
						$quartos = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($quartos): ?>
						<div id="DormCasal">
							<div id="quartoPai">
							<?php $contQuarto = 0; ?>
							<?php foreach ($quartos as $rowQuarto): ?>
								<?php $contQuarto++; ?>
				                
				                <div id="quarto-<?=$rowQuarto->id_variacao_comodo?>" class="DormCasal0<?=$contQuarto?> <?php if($contQuarto == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowQuarto->imagem?>" alt=""  />
				                    
				                    <?php $totalQuartos = count($quartos); ?>
				                    <?php if($totalQuartos > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesQuarto');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>--->
				                	<?php endif; ?>
				                </div>
				                
								<?php if($contQuarto == $totalQuartos) : ?>
									<input type="hidden" id="formQuarto" name="quarto" value="<?=$rowQuarto->id_variacao_comodo?>" />
								<?php endif; ?>
								
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Dormitorios-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Quartos -->
			   	
			   	<!-- Banheiros -->
				<?php if($row->comodo == "Banheiro"): ?>
	
					<?php
						$banheiros = $this->model->getVariacoesComodosDesc($row->id_comodo);
					?>
					
					<?php if($banheiros): ?>
						<div id="Banhos">
							<div id="banheiroPai">
							<?php $contBanheiro = 0; ?>
							<?php foreach ($banheiros as $rowBanheiro): ?>
								<?php $contBanheiro++; ?>
				                
				                <div id="banheiro-<?=$rowBanheiro->id_variacao_comodo?>" class="Banho0<?=$contBanheiro?> <?php if($contBanheiro == 1) { echo "Block"; } ?>">
				                    <img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$rowBanheiro->imagem?>" alt=""  />
				                    
				                    <?php $totalBanheiros = count($banheiros); ?>
				                    <?php if($totalBanheiros > 1): ?>
				                    	<a href="javascript: mostraOpcoes('OpcoesBanheiro');" class="btnEditar" title="Editar este c&ocirc;modo">Editar este c&ocirc;modo</a>
				                    <?php else: ?>
				                    	<span title="N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo">N&atilde;o h&aacute; op&ccedil;&otilde;es para este c&ocirc;modo</span>
				                	<?php endif; ?>
				                	
				                </div>
				                
				                <?php if($contBanheiro == $totalBanheiros) : ?>
									<input type="hidden" id="formBanheiro" name="banheiro" value="<?=$rowBanheiro->id_variacao_comodo?>" />
								<?php endif; ?>
				                
							<?php endforeach; ?>
			            	</div>
			            </div><!--fecha Banheiros-->	
			        <?php endif; ?>
			   	<?php endif; ?>
			   	<!-- Banheiros -->
			   	
			   	<?php endif; ?> <!-- if($id_tipo_planta.. ) -->
		   	
		   	<?php endforeach; ?>
		
        </div><!--fecha Projeto-->

		<div id="ListaOpcoesComodos">

		<div id="OpcoesCozinhas" class="DisplayNone">
		<?php
			if($cozinhas): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Cozinhas</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($cozinhas as $row): $cont++; ?>
		        	<li id="cozinhas<?=$cont?>" <?php if($cont == $totalCozinhas) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaCozinha('<?=$row->id_variacao_comodo?>', 'cozinhas<?=$cont?>');" title="Cozinha"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesLiving" class="DisplayNone">
		<?php
			if($livings): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Livings</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($livings as $row): $cont++; ?>
		        	<li id="livings<?=$cont?>" <?php if($cont == $totalLivings) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaLiving('<?=$row->id_variacao_comodo?>', 'livings<?=$cont?>');" title="Living"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesQuarto" class="DisplayNone">
		<?php
			if($quartos): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Quartos</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($quartos as $row): $cont++; ?>
		        	<li id="quartos<?=$cont?>" <?php if($cont == $totalQuartos) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaQuarto('<?=$row->id_variacao_comodo?>', 'quartos<?=$cont?>');" title="Quarto"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    <div id="OpcoesBanheiro" class="DisplayNone">
		<?php
			if($banheiros): ?>
			<?php $cont = 0; ?>
			<ul class="OpcoesComodos">
	
		    	<li class="TituloOpcoes">
		        	<h5>Banheiros</h5>
		        </li><!--fecha TituloOpcoes-->
		
				<?php foreach ($banheiros as $row): $cont++; ?>
		        	<li id="banheiros<?=$cont?>" <?php if($cont == $totalBanheiros) echo "class='ComodoSelecionado'"; ?>><a href="javascript: trocaBanheiro('<?=$row->id_variacao_comodo?>', 'banheiros<?=$cont?>');" title="Banheiro"><img src="<?=base_url()?>assets/uploads/variacoes_comodos/<?=$row->thumb?>" alt="" /></a></li>
				<?php endforeach; ?>
		
		    </ul><!--fecha OpcoesComodos-->
		<?php endif; ?>
	    </div>
	    
	    </div>

    </div><!--fecha Main-->	