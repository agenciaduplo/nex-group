<?=$this->load->view('includes/topo');?>

	<script>
			
			function validaLogin ()
			{
				var email 	= $("#txtLoginEmail").val();
				var senha 	= $("#txtLoginSenha").val();
				
				var erroE 	= 0;
				var erroP	= 0;
				
				if(email == "" || email == "E-mail")
				{
					var erroE = 1;
				}
				if(senha == "" || senha == "Password")
				{
					var erroP = 1;
				}
				
				if(erroE == 1 || erroP == 1)
				{
					$("#validaLoginn").removeClass("NoAlert");
					$("#validaLoginn").addClass("Alert");
					$("#validaLoginn").html("Preencha todos os campos corretamente.");
					
					return false;
				}
				else
				{
					$("#validaLoginn").removeClass("Alert");
					$("#validaLoginn").addClass("NoAlert");
					
					return true;
				}
				
			}
			
	</script>
<div class="Container">
				<img src="<?=base_url()?>/assets/img/bg_passos.png"  style="margin-bottom:15px;"/>
		</div>
        <div class="Wrapper">
			
        	<div class="Container">

				<div class="Main">

                    <div class="ColunLeft">

                    	<h3 class="AcessePng">Acesso para propriet&aacute;rios</h3>
						
						
                        <form id="FormLogin222" name="FormLogin222" method="post" action="<?=site_url()?>/login/entrar/" onsubmit="return validaLogin();">
                            <ul class="Formulario">
                               	
								<li style="margin-top: 40px; color: red; font-weight: bold;" id="validaLoginn" class="NoAlert"></li>
									
                                <li><input type="text" id="txtLoginEmail" class="CampoEmail" name="email" value="E-mail" onfocus="if(this.value == 'E-mail') this.value=''" onblur="if(this.value == '') this.value = 'E-mail'" /></li>
                                <li><input type="password" id="txtLoginSenha" class="CampoSenha" name="senha" value="Password" onfocus="if(this.value == 'Password') this.value=''" onblur="if(this.value == '') this.value = 'Password'" /></li>
                                <li><input type="submit" title="" value="" name="btnLogin" id="btnLogin" class="btnLogin" /> <a class="Modal2" href="<?=base_url()?>index.php/home/EsqueciSenha" ><strong>esqueci minha senha</strong></a></li>
								
    
                            </ul>
                        </form>
                        
                        <!--fecha FormLogin-->
						
                    </div><!--fecha ColunLeft-->

                    <div class="ColunRight">

                    	<h3 class="AcessePng">Acesso para propriet&aacute;rios</h3>
						
						<a class="Modal LnkCadastro" href="<?=base_url()?>index.php/home/cadastro">Cadastre-se</a>
                        
                        <!--fecha FormLogin-->
						


                    </div><!--fecha ColunRight-->

                </div><!--fecha Main-->

            </div><!--fecha Container-->
        </div><!--fecha Wrapper-->

		<script>
					
			<?php if (@$erro == "S"): ?>
				$("#validaLoginn").removeClass("NoAlert");
				$("#validaLoginn").addClass("Alert");
				$("#validaLoginn").html("Login ou senha incorretos.");
			<?php endif; ?>
			
		</script>

		
<?=$this->load->view('includes/rodape');?>