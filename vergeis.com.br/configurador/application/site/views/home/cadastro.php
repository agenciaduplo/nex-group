<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
    
	<title>Vergeis - Configurador - Cadastro</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
    
	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/formularios.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	
	<script>
		$(document).ready(function() {
			$("#FormInteresse").validationEngine('attach', {
				promptPosition : "topLeft",
				onValidationComplete: function(form, status){
					if(status == true)
					{
						$("#btrnInteresseEnviar").attr("disabled","disabled");
						
						var nome		= $("#txtInteresseNome").val();
						var sobrenome	= $("#txtInteresseSobrenome").val();
						var email		= $("#txtInteresseEmail").val();
						var telefone	= $("#txtInteresseTelefone").val();
						var celular		= $("#txtInteresseCelular").val();
						var senha		= $("#txtInteresseSenha").val();
							
						var msg 	= '';
						vet_dados 	= 	'nome='+ nome
									  	+'&sobrenome='+ sobrenome
										+'&email='+ email
										+'&telefone='+ telefone
										+'&celular='+ celular
									  	+'&senha='+ senha;
									  
						base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/CadastrarDados";
						
						$.ajax({
							type: "POST",
							url: base_url,
							data: vet_dados,
							success: function(msg) {
									$("#Confirmacao").html(msg);
									limpaCampos();
									$("#btrnInteresseEnviar").removeAttr("disabled");
									}
						});
					}
					else
					{
						return false;
					}
				}  
			});
			
			
		});
		
		function limpaCampos ()
		{
		    $("#FormInteresse").find(':input').each(function() {
		        switch(this.type) {
		            case 'password':
		            case 'select-multiple':
		            case 'select-one':
		            case 'select':
		            case 'text':
		            case 'textarea':
		                $(this).val('');
		                break;
		            case 'checkbox':
		            case 'radio':
		                this.checked = false;
		        }
		    });
		}
	</script>
</head>
<body class="Form">
 	   <div class="Wrapper">
			<h3>
				<ul style="display: inline;">
				<li style="width:124px; float: left;"><img src="<?=base_url()?>assets/img/site/logo-vergeis.png"></li>
				<li style="margin-top: 30px; width:300px; float: right;">Por favor preencha todos os campos abaixo para realizar o seu cadastro</li>
				</ul>
			</h3>
			<form id="FormInteresse" name="FormInteresse" method="POST" style="margin-top:0px;" />
				<ul class="Formulario">
					<li>
						<label for="txtInteresseNome">Nome*</label>
						<input type="text" id="txtInteresseNome" class="Campo validate[required]" name="txtInteresseNome" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseSobrenome">Sobrenome*</label>
						<input type="text" id="txtInteresseSobrenome" class="Campo validate[required]" name="txtInteresseSobrenome" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseEmail">E-mail*</label>
						<input type="text" id="txtInteresseEmail" class="Campo validate[required,custom[email]]" name="txtInteresseEmail" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseTelefone">Telefone*</label>
						<input type="text" id="txtInteresseTelefone" class="Campo" name="txtInteresseTelefone" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseCelular">Celular*</label>
						<input type="text" id="txtInteresseCelular" class="Campo" name="txtInteresseCelular" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseSenha">Senha*</label>
						<input type="password" id="txtInteresseSenha" class="Campo validate[equals[txtInteresseSenhaConfirma]]" name="txtInteresseSenha" onblur="" OnFocus="" />
					</li>
					<li>
						<label for="txtInteresseSenhaConfirma">Confirme a senha*</label>
						<input type="password" id="txtInteresseSenhaConfirma" class="Campo validate[required]" name="txtInteresseSenhaConfirma" onblur="" OnFocus="" />
					</li>
					<li class="LiBotao Debug">
						<span style="display:block;" id="Confirmacao" class="Confirma"></span>
						<input type="submit" style="cursor:pointer;" src="/site/images/btn-enviar.png" class="BtEnviar" value="Enviar dados" id="btrnInteresseEnviar" name="btrnInteresseEnviar" />
					</li>
				</ul><!-- Fecha Formulario-->
			</form>
	</div><!--fecha Wrapper-->
</body>
</html>