<?=$this->load->view('includes/topo');?>
		
		<div class="Wrapper">
        	<div class="Container">
				
				<form method="POST" action="" />        	

					<div class="Sidebar">
					
						<input type="hidden" name="campoDorms" id="campoDorms" value="" />
		    			<input type="hidden" name="campoOpcoes" id="campoOpcoes" value="" />
	
	                    <ul class="Timeline">
	                    	<li id="PassoDormitorios" class="NumeroDormitoriosSelecionado AcessePng">N&uacute;mero de Dormit&oacute;rios  <div id="voltaDorms" class="DisplayNone"><a class="AcessePng" href="javascript: tiposDorms();" title="Alterar n&uacute;mero de dormit&oacute;rios">Alterar n&uacute;mero de dormit&oacute;rios</a></div></li>
	                        <li id="PassoPlantas" class="OpcoesPlantasDesativado AcessePng">Op&ccedil;&otilde;es de Plantas <div id="voltaPlantas" class="DisplayNone"><a class="AcessePng" href="javascript: tiposPlantas();" title="Alterar tipo de planta">Alterar tipo de planta</a></div></li>
	                        <li id="PassoComodos" class="PersonalizarComodosDesativado AcessePng">Personalizar C&ocirc;modos</li>
                          
                            <?php if($this->uri->segment(2) != "visitante"): ?>
							<a href="http://vimeo.com/37678056" class="PasseioVirtual" title="Veja o Passeio Virtual"><li id="passeio" class="">Veja o Passeio Virtual</li></a>
                            <?php endif; ?>
                            
	                        <!-- <li class="EnviarParaCapaDesativado AcessePng">Enviar para Capa</li>-->
	                    </ul><!--fecha Timeline-->
		
						<?php if(@$verificaLogin != "0"): ?>
	                    	<!-- <input src="<?=base_url()?>assets/img/site/bg-btn-enviar.png" type="image" class="btnEnviar" /> -->
						<?php endif; ?>
						
						<ul class="Opcoes">
	                    	<?php if(@$verificaLogin != "0"): $dorms = $this->session->userdata('login_dorms'); ?><li style="display:none;" id="BotaoSalvar"><a href="javascript: EnviaDadosGeral(<?=@$dorms?>);" class="btnSalvar" title="Salvar planta">Salvar planta</a></li><?php endif; ?>
	                        <li><a href="javascript: window.print();" class="btnImprimir" title="Imprimir planta">Imprimir planta</a></li>
							<!-- <li><a href="javascript: window.print();" class="btnImprimir" title="Imprimir planta">Imprimir planta</a></li> -->
	                    </ul><!--fecha opcoes-->
	
					</div><!--fecha Sidebar-->
	
	            	<div class="Main">
		            	
						<?php if(@$dormitorios): $contDorm = 0; ?>
						
							<div id="divDormitorios">
								<ul class="NumeroDormitorios">								
									<?php foreach ($dormitorios as $row): ?>
										<?php
											if(@$verificaLogin != "0")
											{
												$dorms = $this->session->userdata('login_dorms');
											}
										
											$contDorm++;
											if($contDorm == 1)
											{
												$classDorm = "Opcao1Dormitorio";
											}
											elseif ($contDorm == 2)
											{
												$classDorm = "Opcao2Dormitorios";
											}
											elseif ($contDorm == 3)
											{
												$classDorm = "Opcao3Dormitorios";
											}
											elseif ($contDorm == 4)
											{
												$classDorm = "Opcao4Dormitorios";
											}
										?>
									<?php if(@$verificaLogin != "0"): ?>
										
										<?php //if($row->id_dormitorio == $dorms) : ?>
										<li><a class="<?=@$classDorm?>" href="javascript: buscaTipoPlantas(<?=$row->id_dormitorio?>);"><?=$row->quantidade?></a></li>
										<?php //endif; ?>
									<?php else: ?>
									
										<li><a class="<?=@$classDorm?>" href="javascript: buscaTipoPlantas(<?=$row->id_dormitorio?>);"><?=$row->quantidade?></a></li>
									
									<?php endif; ?>
									
									<?php endforeach; ?>
								</ul>
							</div>
							
							<!-- Hiddens -->
								<input type="hidden" name="tipos_plantas" id="FormHiddenTiposPlantas" value="" />
								<input type="hidden" name="dormitorios" id="FormHiddenDormitorios" value="" />
							<!-- End Hiddens -->
							
							<div id="divTiposPlantas">
							
							</div>
							
							<div id="divComodos">
							
							</div>
							
							<div id="dadosEnviados">
							
							</div>
						
						<?php endif; ?>	
	
	                </div><!--fecha Main Passos-->
				</form>
        	</div><!--fecha Container-->
		</div><!--fecha Wrapper-->
		
		<script>
		jQuery(document).ready(function() {
			$(".PasseioVirtual").click(function() {
				$.fancybox({
					'padding'		: 0,
					'autoScale'		: false,
					'transitionIn'	: 'none',
					'transitionOut'	: 'none',
					'title'			: this.title,
					'width'			: 800,
					'height'		: 450,
					'href'			: this.href.replace(new RegExp("([0-9])","i"),'moogaloop.swf?clip_id=$1'),
					'type'			: 'swf'
				});
		
				return false;
			});
		});
		</script>
		
<?=$this->load->view('includes/rodape');?>