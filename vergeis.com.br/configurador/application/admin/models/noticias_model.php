<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends Model {
	
	function Noticias_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numNoticias ()
	{		
		$this->db->select('*')->from('noticias');		
		
		return $this->db->count_all_results();
	}
	
	function getNoticias ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM noticias
				ORDER BY data DESC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function buscaNoticias ($keyword)
	{	
		$sql = "SELECT *
				FROM noticias
				WHERE titulo LIKE '%$keyword%'
				OR conteudo LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}	
	
	function getNoticia ($id_noticia)
	{		
		$where = array ('id_noticia' => $id_noticia);
		
		$this->db->start_cache();
		$this->db->select('*')->from('noticias')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
	
	function setNoticia ($data, $id_noticia = "")
	{		
		if ($id_noticia)
		{
			$where = array ('id_noticia' => $id_noticia);
			$this->db->select('*')->from('noticias')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Noticia salvo com sucesso!');
				
				$this->load->library('image_lib');
				
				//UPLOAD
		        $config['upload_path']		= './assets/uploads/noticias/';
		        $config['allowed_types']	= 'gif|jpg|png';
		        $config['max_size']			= '15000';
		        $config['max_width']		= '12000';
		        $config['max_height']		= '12000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('foto'))
		        {
					$imagem = $this->model->getImagem($id_noticia);
					if($imagem->foto_chamada):
						unlink('./assets/uploads/noticias/' . $imagem->foto);
						unlink('./assets/uploads/noticias/thumb/' . $imagem->foto);
					endif;
		        	
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
					
			        // THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './assets/uploads/noticias/thumb/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 135;
					$config['width']			= 180;
					$config['quality']			= 100;					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
			        // THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './assets/uploads/noticias/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 500;
					$config['width']			= 600;
					$config['quality']			= 100;					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();																
							
					$this->db->set($data);
					$this->db->set('foto', $file_name);
	                $this->db->where('id_noticia', $id_noticia);
	                $this->db->update('noticias');
	                
	                $this->session->set_flashdata('resposta', 'Noticia salva com sucesso!');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "noticias";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                
		        }
		        else
		        {
					$this->db->set($data);
	                $this->db->where('id_noticia', $id_noticia);
	                $this->db->update('noticias');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "noticias";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                	        	
		        	
					$this->session->set_flashdata('resposta', 'Noticia inserida sem imagem!');
		        }
			}
		}
		else
		{
			$this->load->library('image_lib');
			
			//UPLOAD
	        $config['upload_path']		= './assets/uploads/noticias/';
	        $config['allowed_types']	= 'gif|jpg|png';
	        $config['max_size']			= '15000';
	        $config['max_width']		= '12000';
	        $config['max_height']		= '12000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('foto'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        // THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './assets/uploads/noticias/thumb/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 135;
				$config['width']			= 180;
				$config['quality']			= 100;					
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				
				// THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './assets/uploads/noticias/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 500;
				$config['width']			= 600;
				$config['quality']			= 100;					
				$this->image_lib->initialize($config);
				$this->image_lib->resize();											
						
				$this->db->set($data)->set('foto', $file_name)->insert('noticias');
				
	            //Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "noticias";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 
		            				
				return $this->db->insert_id();
	        }
	        else
	        {
				$this->db->set($data)->insert('noticias');
	            
				//Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "noticias";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 				
				
				return $this->db->insert_id();	        	
	        	
				$this->session->set_flashdata('resposta', 'Noticia inserida sem imagem!');
	        }
		}
	}
	
	function delNoticia ($id_noticia)
	{   				
		$noticia = $this->model->getImagem($id_noticia);
		if($noticia->foto_chamada):
			unlink('./assets/uploads/noticias/' . $noticia->foto);
			unlink('./assets/uploads/noticias/thumb/' . $noticia->foto);
		endif;			
		
	    $this->db->where('id_noticia', $id_noticia);
	    $this->db->delete('noticias');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "noticias";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getImagem ($id_noticia)
	{
		$sql = "SELECT *
				FROM noticias
				WHERE id_noticia = $id_noticia
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}					
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */