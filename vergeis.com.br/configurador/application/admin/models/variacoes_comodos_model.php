<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Variacoes_comodos_model extends Model {
	
	function Variacoes_comodos_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numVariacoes_comodos ()
	{		
		$this->db->select('*')->from('variacoes_comodos');		
		
		return $this->db->count_all_results();
	}
	
	function getVariacoes_comodos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *, vc.imagem as img
				FROM 
					variacoes_comodos vc, 
					comodos c,
					tipos_plantas tp,
					dormitorios d
				WHERE 
					vc.id_comodo = c.id_comodo
				AND
					c.id_tipo_planta = tp.id_tipo_planta
				AND 
					tp.id_dormitorio = d.id_dormitorio	
					
					
				ORDER BY d.quantidade, tp.tipo, c.comodo, vc.id_variacao_comodo ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getComodos ()
	{
		$sql = "SELECT *
				FROM comodos c, tipos_plantas tp, dormitorios d
				WHERE c.id_tipo_planta = tp.id_tipo_planta
				AND tp.id_dormitorio = d.id_dormitorio
				ORDER BY d.quantidade, tp.tipo, c.comodo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function buscaVariacoes_comodos ($keyword)
	{	
		$sql = "SELECT *, vc.imagem as img
				FROM 
					variacoes_comodos vc, 
					comodos c,
					tipos_plantas tp,
					dormitorios d
				WHERE 
					vc.id_comodo = c.id_comodo
				AND
					c.id_tipo_planta = tp.id_tipo_planta
				AND 
					tp.id_dormitorio = d.id_dormitorio	
				AND
					c.comodo LIKE '%$keyword%'
					
					
				ORDER BY d.quantidade, tp.tipo, c.comodo, vc.id_variacao_comodo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}	
	
	function getVariacao_comodo ($id_variacao_comodo)
	{		
		$where = array ('id_variacao_comodo' => $id_variacao_comodo);
		
		$this->db->start_cache();
		$this->db->select('*')->from('variacoes_comodos')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
	
	function setVariacao_comodo ($data, $id_variacao_comodo = "")
	{		
		if ($id_variacao_comodo)
		{
			$where = array ('id_variacao_comodo' => $id_variacao_comodo);
			$this->db->select('*')->from('variacoes_comodos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Variação de comodo salva com sucesso!');
				
				$this->load->library('image_lib');
				
				//UPLOAD
		        $config['upload_path']		= './assets/uploads/variacoes_comodos/';
		        $config['allowed_types']	= 'gif|jpg|png';
		        $config['max_size']			= '15000';
		        $config['max_width']		= '12000';
		        $config['max_height']		= '12000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
					$imagem = $this->model->getImagem($id_variacao_comodo);
					if($imagem->imagem):
						unlink('./assets/uploads/variacoes_comodos/' . $imagem->imagem);
						//unlink('./assets/uploads/variacoes_comodos/thumb/' . $imagem->imagem);
					endif;
		        	
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
																				
							
					$this->db->set($data);
					$this->db->set('imagem', $file_name);
	                $this->db->where('id_variacao_comodo', $id_variacao_comodo);
	                $this->db->update('variacoes_comodos');
	                
	                $this->session->set_flashdata('resposta', 'Variação de comodo salva com sucesso!');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "variacoes_comodos";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                
		        }
		        else
		        {
					$this->db->set($data);
	                $this->db->where('id_variacao_comodo', $id_variacao_comodo);
	                $this->db->update('variacoes_comodos');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "variacoes_comodos";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                	        	
		        	
					$this->session->set_flashdata('resposta', 'Variação de comodo inserida sem imagem!');
		        }
		        
		        //Copy THUMB
		        
		        //UPLOAD
		        $config2['upload_path']		= './assets/uploads/variacoes_comodos/';
		        $config2['allowed_types']	= 'gif|jpg|png';
		        $config2['max_size']		= '15000';
		        $config2['max_width']		= '12000';
		        $config2['max_height']		= '12000';
		        $config2['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config2);
		        
		        if ($this->upload->do_upload('thumb'))
		        {
					$imagem = $this->model->getImagem($id_variacao_comodo);
					if($imagem->thumb):
						unlink('./assets/uploads/variacoes_comodos/' . $imagem->thumb);
					endif;
		        	
		        	$file_data2 = $this->upload->data();
					
			        $file_name2 = $file_data2['file_name'];
			        $file_size2 = $file_data2['file_size'];
																				
							
					$this->db->set($data);
					$this->db->set('thumb', $file_name2);
	                $this->db->where('id_variacao_comodo', $id_variacao_comodo);
	                $this->db->update('variacoes_comodos');
	                
	                $this->session->set_flashdata('resposta', 'Variação de comodo (THUMB) salva com sucesso!');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "variacoes_comodos";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                
		        }
			}
		}
		else
		{
			$this->load->library('image_lib');
			
			//UPLOAD
	        $config['upload_path']		= './assets/uploads/variacoes_comodos/';
	        $config['allowed_types']	= 'gif|jpg|png';
	        $config['max_size']			= '15000';
	        $config['max_width']		= '12000';
	        $config['max_height']		= '12000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('imagem'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];									
						
				$this->db->set($data)->set('imagem', $file_name)->insert('variacoes_comodos');
				
	            //Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "variacoes_comodos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 
		            				
				return $this->db->insert_id();
	        }
	        else
	        {
				$this->db->set($data)->insert('variacoes_comodos');
	            
				//Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "variacoes_comodos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 				
				
				return $this->db->insert_id();	        	
	        	
				$this->session->set_flashdata('resposta', 'Variação de comodo inserida sem imagem!');
	        }
	        
	        //Copy THUMB
		        
	        //UPLOAD
	        $config2['upload_path']		= './assets/uploads/variacoes_comodos/';
	        $config2['allowed_types']	= 'gif|jpg|png';
	        $config2['max_size']		= '15000';
	        $config2['max_width']		= '12000';
	        $config2['max_height']		= '12000';
	        $config2['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config2);
	        
	        if ($this->upload->do_upload('thumb'))
	        {
	        	$file_data2 = $this->upload->data();
				
		        $file_name2 = $file_data2['file_name'];
		        $file_size2 = $file_data2['file_size'];									
						
				$this->db->set($data)->set('thumb', $file_name2)->insert('variacoes_comodos');
				
	            //Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "variacoes_comodos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 
		            				
				return $this->db->insert_id();                
	        }
		}
	}
	
	function delVariacao_comodo ($id_variacao_comodo)
	{   				
		$variacao_comodo = $this->model->getImagem($id_variacao_comodo);
		if($variacao_comodo->imagem):
			unlink('./assets/uploads/variacoes_comodos/' . $variacao_comodo->imagem);
		endif;	
		
		if($variacao_comodo->thumb):
			unlink('./assets/uploads/variacoes_comodos/' . $variacao_comodo->thumb);
		endif;		
		
	    $this->db->where('id_variacao_comodo', $id_variacao_comodo);
	    $this->db->delete('variacoes_comodos');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "variacoes_comodos";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getImagem ($id_variacao_comodo)
	{
		$sql = "SELECT *
				FROM variacoes_comodos
				WHERE id_variacao_comodo = $id_variacao_comodo
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}					
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */