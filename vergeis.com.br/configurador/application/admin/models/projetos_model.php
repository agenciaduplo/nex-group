<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos_model extends Model {
	
	function Projetos_model ()
	{
		parent::Model();
	}
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numProjetos ()
	{		
		$this->db->select('*')->from('projetos')->order_by("nome", "asc"); ;
		
		return $this->db->count_all_results();
	}
	
	function getProjetos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM projetos p, tipos_plantas tp, dormitorios d 
				WHERE p.id_tipo_planta = tp.id_tipo_planta
				AND p.id_dormitorio = d.id_dormitorio
				AND tp.id_dormitorio = d.id_dormitorio
				ORDER BY p.nome, p.sobrenome ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getDormitorios ()
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM dormitorios
				ORDER BY quantidade ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
		
	}
	
	function getTiposPlantas ($id_dormitorio)
	{
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM dormitorios d, tipos_plantas tp
				WHERE tp.id_dormitorio = d.id_dormitorio
				AND d.id_dormitorio = $id_dormitorio
				ORDER BY tp.tipo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function buscaProjetos ($keyword)
	{		
		$this->db->flush_cache();			
		
		$sql = "SELECT *
				FROM projetos p, tipos_plantas tp, dormitorios d 
				WHERE p.id_tipo_planta = tp.id_tipo_planta
				AND p.id_dormitorio = d.id_dormitorio
				AND tp.id_dormitorio = d.id_dormitorio
				AND p.nome LIKE '%$keyword%' OR p.sobrenome LIKE '%$keyword%'
				ORDER BY p.nome, p.sobrenome ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getProjeto ($id_projeto)
	{		
		$where = array ('id_projeto' => $id_projeto);
		
		$this->db->start_cache();
		$this->db->select('*')->from('projetos')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setProjeto ($data, $id_projeto = "")
	{		
		if ($id_projeto)
		{
			$where = array ('id_projeto' => $id_projeto);
			$this->db->select('*')->from('projetos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Projeto salvo com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_projeto', $id_projeto);
                $this->db->update('projetos');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "projetos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('projetos');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "projetos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 			
			
			return $this->db->insert_id();
		}
	}
	
	function delProjeto ($id_projeto)
	{		
		$where = array ('id_projeto' => $id_projeto);
		$this->db->select('*')->from('projetos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_projeto', $id_projeto);
            $this->db->delete('projetos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "projetos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 	            
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */