<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=PROJETOS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormProjetos" action="<?=site_url()?>/projetos/salvar/" method="post">
 					<? if (@$projeto->id_projeto): ?>
                        <input type="hidden" name="id_projeto" value="<?=@$projeto->id_projeto?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Nome</label>
                        <div><input type="text" name="nome" class="campo-padrao" id="FormNome" value="<?=@$projeto->nome?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Sobrenome</label>
                        <div><input type="text" name="sobrenome" class="campo-padrao" id="FormSobrenome" value="<?=@$projeto->sobrenome?>" /></div>
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Telefone</label>
                        <div><input type="text" name="telefone" class="campo-padrao" id="FormTelefonee" value="<?=@$projeto->telefone?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Celular</label>
                        <div><input type="text" name="celular" class="campo-padrao" id="FormCelCliente" value="<?=@$projeto->celular?>" /></div>
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">E-mail (LOGIN)</label>
                        <div><input type="text" name="email" class="campo-padrao" id="FormEmail" value="<?=@$projeto->email?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Senha</label>
                        <div><input type="password" name="senha" class="campo-padrao" id="FormSenha" value="" /></div>
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Dormitórios</label>
                        <div>
                        	<select name="id_dormitorio" class="campo-padrao" id="FormQuantidade" onchange="return buscaTiposPlantas(this.value);">
                        		<option value="">Selecione</option>
                        		<?php foreach (@$dormitorios as $row): ?>
                        			<option <?php if(@$projeto->id_dormitorio == $row->id_dormitorio) echo "selected='selected'"; ?> value="<?=$row->id_dormitorio?>"><?=$row->quantidade?></option>
                        		<?php endforeach; ?>	
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Tipo de Planta</label>
                         <div id="AjaxTposPlantas">
                         	<?php if(@$projeto->id_tipo_planta): ?>
	                        	<select name="id_tipo_planta" class="campo-padrao" id="FormTipoPlanta">
									<option value="">Selecione</option>
									<?php foreach (@$tipos_plantas as $row): ?>
										<option <?php if($row->id_tipo_planta == $projeto->id_tipo_planta) echo "selected='selected'"; ?> value="<?=$row->id_tipo_planta?>"><?=$row->quantidade?> - <?=$row->tipo?></option>
									<?php endforeach; ?>	
	                        	</select>
	                        <?php else: ?>
	                        	<select disabled="disabled" name="id_tipo_planta" class="campo-padrao" id="FormTipoPlanta">
	                        		<option value="">Selecione dormitórios</option>
	                        	</select>	                        
	                        <?php endif; ?>
                        </div>
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Unidade Comprada</label>
                        <div><input type="text" name="unidade_comprada" class="campo-padrao" id="FormUnidadeComprada" value="<?=@$projeto->unidade_comprada?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Ativo</label>
                        <div>
                        	<select name="ativo" class="campo-padrao" id="FormAtivo">
                        		<option <?php if(@$projeto->ativo == '1') echo "selected='selected'"; ?> value="1">Sim</option>
                        		<option <?php if(@$projeto->ativo == '0') echo "selected='selected'"; ?> value="0">Não</option>
                        	</select>
                        </div>
                    </div>
                    <br/><br/>
                    <br class="clr" /><br/>
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/projetos/lista" title="Voltar" class="bt-voltar">voltar para Projetos</a>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>