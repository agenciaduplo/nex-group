<?=$this->load->view('includes/topo');?>


            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=COMODOS . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <div class="botoes"><a href="<?=site_url()?>/comodos/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/img/bt_incluir.jpg" alt="Inserir" title="Inserir" /></a></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($comodos):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="30%">Tipo de Planta</th>
                        <th width="30%">Imagem</th>
                        <th width="30%">Comodo</th>
                        <th width="5%">Posição</th>
                        <th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($comodos as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="30%"><a href="<?=site_url()?>/comodos/editar/<?=$row->id_comodo?>" title="Editar" class="bt-editar"><?=$row->quantidade?> - <?=$row->tipo?></a></td>
                        <td width="30%"><?php if(@$row->imagem): ?><img width="80px" src="<?=base_url()?>assets/uploads/tipos_plantas/thumb/<?=$row->imagem?>" /><?php endif; ?></td>
                        <td width="30%"><?=$row->comodo?></td>
                        <td width="30%"><?=$row->posicao?></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/comodos/editar/<?=$row->id_comodo?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/img/lapis.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/comodos/apagar/<?=$row->id_comodo?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->comodo?>?');"><img src="<?=base_url()?>assets/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhum Comodo encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>