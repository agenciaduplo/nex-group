<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=DORMITORIOS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormDormitorios" action="<?=site_url()?>/dormitorios/salvar/" method="post">
 					<? if (@$dormitorio->id_dormitorio): ?>
                        <input type="hidden" name="id_dormitorio" value="<?=@$dormitorio->id_dormitorio?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Quantidade</label>
                        <div><input type="text" name="quantidade" class="campo-padrao" id="FormQuantidade" value="<?=@$dormitorio->quantidade?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label>&nbsp;</label>
	                    <div>&nbsp;</div>                        
                        
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/dormitorios/lista" title="Voltar" class="bt-voltar">voltar para Dormitórios</a>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>