<?=$this->load->view('includes/topo');?>
	
	<script type="text/javascript" charset="utf-8">
	  $(function() {
	            
	      $("input.file_1").filestyle({ 
	          image: "<?=base_url()?>assets/img/file.gif",
	          imageheight : 17,
	          imagewidth : 47,
	          width : 150
	      });      
	  });
	</script>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=TIPOS DE PLANTAS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>                       
                
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                    <form id="FormTipoPlanta" action="<?=site_url()?>/tipos_plantas/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$tipo_planta->id_tipo_planta): ?>
                        <input type="hidden" name="id_tipo_planta" value="<?=@$tipo_planta->id_tipo_planta	?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Dormitórios</label>
                        <div>
                        	<select class="campo-padrao" name="id_dormitorio" id="FormDormitorios">
                        		<option value="">Selecione</option>
                        		<?php foreach ($dormitorios as $row): ?>
                        			<option <? if(@$row->id_dormitorio == @$tipo_planta->id_dormitorio) echo "selected='selected'"; ?> value="<?=$row->id_dormitorio?>"><?=$row->quantidade?></option>
                        		<?php endforeach; ?>
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Tipo</label>
                        <div><input type="text" name="tipo" class="campo-padrao" id="FormTipo" value="<?=@$tipo_planta->tipo?>" /></div>
                    </div>
                    <br class="clr" />                
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Foto</label>
                        <div><input type="file" name="imagem" class="file_1" id="FormFoto" ></div>
                        <? if(@$tipo_planta->imagem): ?>
                        	<br/>
                        	<img src="<?=base_url()?>assets/uploads/tipos_plantas/<?=$tipo_planta->imagem?>" width="100px;">
                        <? endif; ?>   
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
	                    <div>&nbsp;</div> 
                    </div>
                    <br class="clr" />                                                            
                    <br/><br/>
                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-noticia"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/tipos_plantas/lista" title="Voltar" class="bt-voltar">voltar para Tipos de Plantas</a>
			<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>