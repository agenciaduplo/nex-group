<?=$this->load->view('includes/topo');?>


            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=NOTICIAS . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <div class="botoes"><a href="<?=site_url()?>/noticias/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/img/bt_incluir.jpg" alt="Inserir" title="Inserir" /></a></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($noticias):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="55%">Título</th>
                        <th width="20%">Data</th>
                        <th width="20%">Foto</th>
                        <th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($noticias as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="55%"><a href="<?=site_url()?>/noticias/editar/<?=$row->id_noticia?>" title="Editar" class="bt-editar"><?=$row->titulo?></a></td>
                        <td width="20%"><?=date('d/m/Y', strtotime(@$row->data))?></td>
                        <td width="20%"><img width="80px" src="<?=base_url()?>assets/uploads/noticias/thumb/<?=$row->foto?>" /></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/noticias/editar/<?=$row->id_noticia?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/img/lapis.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/noticias/apagar/<?=$row->id_noticia?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->titulo?>?');"><img src="<?=base_url()?>assets/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma Notícia encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>