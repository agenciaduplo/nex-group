<?=$this->load->view('includes/topo');?>
<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/js/tinymce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "simple"
	});
</script>

<script type="text/javascript" charset="utf-8">
  $(function() {
            
      $("input.file_1").filestyle({ 
          image: "<?=base_url()?>assets/img/file.gif",
          imageheight : 17,
          imagewidth : 47,
          width : 150
      });      
  });
</script>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/media/titulo.swf?titulo=NOTICIAS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>                       
                
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                    <form id="FormNoticias" action="<?=site_url()?>/noticias/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$noticia->id_noticia): ?>
                        <input type="hidden" name="id_noticia" value="<?=@$noticia->id_noticia?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Título</label>
                        <div><input type="text" name="titulo" class="campo-padrao" id="FormTitulo" value="<?=@$noticia->titulo?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Foto</label>
                        <div><input type="file" name="foto" class="file_1" id="FormFoto" ></div>
                        <? if(@$noticia->foto): ?>
                        	<img src="<?=base_url()?>assets/uploads/noticias/<?=$noticia->foto?>" width="100px;">
                        <? endif; ?>   
                    </div>
                    <br class="clr" />
                    
                    <?
                    	if(@$noticia->data):
	                    	@$data = date('d/m/Y', strtotime(@$noticia->data));
	                    endif;
					?>                    
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Data</label>
	                    <div>
		                    <input style="width:80px;" type="text" name="data" class="campo-padrao"  id="FormData" value="<? if(@$data) { echo @$data; } ?>" />
		                    <style type="text/css">.embed + img { position: relative; left: -21px; top: -1px; }</style>
		                    <br class="clr" />	                    
	                    </div> 
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
	                    <div>&nbsp;</div> 
                    </div>
                    <br class="clr" />                                                            
                    
                   <div class="formulario">
                        <label class="obrigatorio">Conteúdo</label>
                        <div>
                        	<textarea name="conteudo" class="campo-grande" id="FormDescricao" /><?=@$noticia->conteudo?></textarea>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                    	<label>&nbsp;</label>
                    	<div>&nbsp;</div>
                    </div>
                    <br class="clr" />
                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-noticia"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/noticias/lista" title="Voltar" class="bt-voltar">voltar para Notícias</a>
			<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>