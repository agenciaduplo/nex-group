<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dormitorios extends Controller {
	
	function Dormitorios ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('dormitorios/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('dormitorios_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/secoes/lista/',
			'total_rows'	=> $this->model->numDormitorios(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numDormitorios(),
    		'dormitorios'  	=> ($this->input->post('keyword')) ? $this->model->buscaDormitorios($this->input->post('keyword')) : $this->model->getDormitorios($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('dormitorios/dormitorios.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('dormitorios_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('dormitorios/dormitorios.cadastro.php', $data);
	}
	
	function editar ($id_dormitorio = 0)
	{
		$this->load->model('dormitorios_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'dormitorio' 	=> $this->model->getDormitorio($id_dormitorio)
			);
			
			$this->load->view('dormitorios/dormitorios.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('dormitorios_model', 'model');
		
		try
		{			
			$data = array (
				'quantidade' 		=> $this->input->post('quantidade'),
				'data_cadastro'		=> date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setDormitorio($data, $this->input->post('id_dormitorio'));
				
			if ($this->input->post('id_dormitorio'))
				redirect('dormitorios/editar/' . $this->input->post('id_dormitorio'));
			else
				redirect('dormitorios/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_dormitorio = 0)
	{
		$this->load->model('dormitorios_model', 'model');

		try
		{
			$this->model->delDormitorio($id_dormitorio);
			redirect('dormitorios/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */