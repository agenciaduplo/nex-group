<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Variacoes_comodos extends Controller {
	
	function Variacoes_comodos ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('variacoes_comodos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('variacoes_comodos_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/variacoes_comodos/lista/',
			'total_rows'	=> $this->model->numVariacoes_comodos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numVariacoes_comodos(),
    		'variacoes_comodos'   	=> ($this->input->post('keyword')) ? $this->model->buscaVariacoes_comodos($this->input->post('keyword')) : $this->model->getVariacoes_comodos($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('variacoes_comodos/variacoes_comodos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('variacoes_comodos_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'comodos'		=> $this->model->getComodos()
		);

		$this->load->view('variacoes_comodos/variacoes_comodos.cadastro.php', $data);
	}
	
	function editar ($id_variacao_comodo = 0)
	{
		$this->load->model('variacoes_comodos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'variacao_comodo' 	=> $this->model->getVariacao_comodo($id_variacao_comodo),
				'comodos'			=> $this->model->getComodos()
			);
			
			$this->load->view('variacoes_comodos/variacoes_comodos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}		
	
	function salvar ()
	{
		$this->load->model('variacoes_comodos_model', 'model');
		
		try
		{	
			$data = array (
				'id_comodo' 		=> $this->input->post('id_comodo'),
				'data_cadastro' 	=> date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setVariacao_comodo($data, $this->input->post('id_variacao_comodo'));
				
			if ($this->input->post('id_variacao_comodo'))
				redirect('variacoes_comodos/editar/' . $this->input->post('id_variacao_comodo'));
			else
				redirect('variacoes_comodos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_variacao_comodo = 0)
	{
		$this->load->model('variacoes_comodos_model', 'model');

		try
		{
			$this->model->delVariacao_comodo($id_variacao_comodo);
			redirect('variacoes_comodos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */