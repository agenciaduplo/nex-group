<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secoes extends Controller {
	
	function Secoes ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('secoes/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('secoes_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/secoes/lista/',
			'total_rows'	=> $this->model->numSecoes(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numSecoes(),
    		'secoes'   	  	=> ($this->input->post('keyword')) ? $this->model->buscaSecoes($this->input->post('keyword')) : $this->model->getSecoes($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('secoes/secoes.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('secoes_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('secoes/secoes.cadastro.php', $data);
	}
	
	function editar ($id_cms_secoes = 0)
	{
		$this->load->model('secoes_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'secao' 		=> $this->model->getSecao($id_cms_secoes)
			);
			
			$this->load->view('secoes/secoes.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('secoes_model', 'model');
		
		try
		{			
			$data = array (
				'nome' 			=> $this->input->post('nome'),
				'data_criacao' 	=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setSecao($data, $this->input->post('id_cms_secoes'));
				
			if ($this->input->post('id_cms_secoes'))
				redirect('secoes/editar/' . $this->input->post('id_cms_secoes'));
			else
				redirect('secoes/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_cms_secoes = 0)
	{
		$this->load->model('secoes_model', 'model');

		try
		{
			$this->model->delSecao($id_cms_secoes);
			redirect('secoes/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */