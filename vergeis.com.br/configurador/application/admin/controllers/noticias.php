<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends Controller {
	
	function Noticias ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('noticias/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('noticias_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/noticias/lista/',
			'total_rows'	=> $this->model->numNoticias(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numNoticias(),
    		'noticias'   	=> ($this->input->post('keyword')) ? $this->model->buscaNoticias($this->input->post('keyword')) : $this->model->getNoticias($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('noticias/noticias.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('noticias_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('noticias/noticias.cadastro.php', $data);
	}
	
	function editar ($id_noticia = 0)
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'noticia' 		=> $this->model->getNoticia($id_noticia)
			);
			
			$this->load->view('noticias/noticias.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}		
	
	function salvar ()
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{
			@$nova_data = explode("/",$this->input->post('data'));
			@$dataEntrega = @$nova_data[2] . "-" . @$nova_data[1] . "-" . @$nova_data[0];			
			
			$data = array (
				'data' 			=> $dataEntrega,
				'titulo' 		=> $this->input->post('titulo'),
				'conteudo' 		=> $this->input->post('conteudo'),
				'data_cadastro' => date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setNoticia($data, $this->input->post('id_noticia'));
				
			if ($this->input->post('id_noticia'))
				redirect('noticias/editar/' . $this->input->post('id_noticia'));
			else
				redirect('noticias/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_noticia = 0)
	{
		$this->load->model('noticias_model', 'model');

		try
		{
			$this->model->delNoticia($id_noticia);
			redirect('noticias/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */