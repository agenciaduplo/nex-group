<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends Controller {
	
	function Logs ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('logs/acessos/');
	}
	
	function acessos ($offset = "")
	{
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/logs/acessos/',
			'total_rows'	=> $this->model->numAcessos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numAcessos(),
    		'logs'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcessos($this->input->post('keyword')) : $this->model->getAcessos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('logs/logs.php', $data);
	}
	
	function acoes ($offset = "")
	{
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://vialog.divexcrm.com.br/admin.php/logs/acoes/',
			'total_rows'	=> $this->model->numAcoes(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numAcoes(),
    		'logs'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcoes($this->input->post('keyword')) : $this->model->getAcoes($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('logs/logs_acoes.php', $data);
	}	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */