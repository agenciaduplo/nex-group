<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos extends Controller {
	
	function Projetos ()
	{
		parent::Controller();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('projetos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('projetos_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://localhost/configurador/admin.php/secoes/lista/',
			'total_rows'	=> $this->model->numProjetos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numProjetos(),
    		'projetos'  	=> ($this->input->post('keyword')) ? $this->model->buscaProjetos($this->input->post('keyword')) : $this->model->getProjetos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('projetos/projetos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('projetos_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'dormitorios'	=> $this->model->getDormitorios()
		);

		$this->load->view('projetos/projetos.cadastro.php', $data);
	}
	
	function editar ($id_projeto, $id_dormitorio)
	{
		$this->load->model('projetos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'projeto' 		=> $this->model->getProjeto($id_projeto),
				'dormitorios'	=> $this->model->getDormitorios(),
				'tipos_plantas'	=> $this->model->getTiposPlantas($id_dormitorio)
			);
			
			$this->load->view('projetos/projetos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('projetos_model', 'model');
		
		try
		{
			if($this->input->post('senha'))
			{
				$data = array (
					'id_dormitorio' 	=> $this->input->post('id_dormitorio'),
					'id_tipo_planta' 	=> $this->input->post('id_tipo_planta'),
					'cpf' 				=> $this->input->post('cpf'),
					'nome' 				=> $this->input->post('nome'),
					'sobrenome' 		=> $this->input->post('sobrenome'),
					'telefone' 			=> $this->input->post('telefone'),
					'celular' 			=> $this->input->post('celular'),
					'unidade_comprada' 	=> $this->input->post('unidade_comprada'),
					'email' 			=> $this->input->post('email'),
					'senha' 			=> $this->encrypt->encode($this->input->post('senha')),
					'ativo' 			=> $this->input->post('ativo'),
					'data_cadastro'		=> date("Y-m-d H:i:s")
				);				
			}
			else
			{
				$data = array (
					'id_dormitorio' 	=> $this->input->post('id_dormitorio'),
					'id_tipo_planta' 	=> $this->input->post('id_tipo_planta'),
					'cpf' 				=> $this->input->post('cpf'),
					'nome' 				=> $this->input->post('nome'),
					'sobrenome' 		=> $this->input->post('sobrenome'),
					'telefone' 			=> $this->input->post('telefone'),
					'celular' 			=> $this->input->post('celular'),
					'unidade_comprada' 	=> $this->input->post('unidade_comprada'),
					'email' 			=> $this->input->post('email'),
					'ativo' 			=> $this->input->post('ativo'),
					'data_cadastro'		=> date("Y-m-d H:i:s")
				);					
			}
			
			$insert_id = $this->model->setProjeto($data, $this->input->post('id_projeto'));
				
			if ($this->input->post('id_projeto'))
				redirect('projetos/editar/' . $this->input->post('id_projeto') .'/'. $this->input->post('id_dormitorio'));
			else
				redirect('projetos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function buscaTiposPlantas()
	{
		$this->load->model('projetos_model', 'model');
		
		$id_dormitorio = $this->input->post('id_dormitorio');
		
		$data = array (
			'tipos_plantas'  	=> $this->model->getTiposPlantas($id_dormitorio)
		);

		$this->load->view('projetos/projetos.tipos.php', $data);
	}
	
	function apagar ($id_projeto = 0)
	{
		$this->load->model('projetos_model', 'model');

		try
		{
			$this->model->delProjeto($id_projeto);
			redirect('projetos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */