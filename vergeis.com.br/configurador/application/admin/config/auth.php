<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Authentication Variables
|--------------------------------------------------------------------------
|
| 'auth_table_name'		= the name of users table
| 'auth_username_field'	= the name of username field
| 'auth_password_field'	= the name of password field
|
*/
$config['auth_table_name']		= 'usuarios'; 
$config['auth_username_field']	= 'login';
$config['auth_password_field']	= 'senha';


/* End of file auth.php */
/* Location: ./system/application/config/auth.php */