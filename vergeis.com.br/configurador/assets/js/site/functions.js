	
	$(document).ready(function() 
	{
		$('a.Modal').fancybox({
				'width' : 535,
				'height' : 500,
				'hideOnOverlayClick':false,
				'type' : 'iframe'
	    });
	
		$('a.Modal2').fancybox({
				'width' : 490,
				'height' : 355,
				'hideOnOverlayClick':false,
				'type' : 'iframe'
	    });	
	});
	
	function printElement()
	{
		$(document).ready(function() {
    		$('body').printElement(	
				{
					overrideElementCSS:[
						'master.css',
						{ href:'http://www.vergeis.com.br/configurador/assets/css/site/master.css',media:'print'}]
				});
		});
		
	}
	
	function buscaTipoPlantas (id_dormitorio)
	{
		var msg 	= '';
		
		vet_dados 	= 'id_dormitorio='+ id_dormitorio;
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/buscaTiposPlantas";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$("#divDormitorios").fadeOut(300, function() {
						
						$("#divTiposPlantas").fadeIn(function() {
							$("#divTiposPlantas").html(msg);
						});
						
						$("body").removeClass("Dormitorios");
						$("body").addClass("Plantas");
						
						$("#Crumbs").fadeIn(function () {
							$(".BreadDorms").fadeIn();
							if(id_dormitorio == 1)
							{
								$("#BreadDorms").html(id_dormitorio+" Dorm");
							}
							else
							{
								$("#BreadDorms").html(id_dormitorio+" Dorms");
							}
						});
						
						$("#PassoDormitorios").removeClass("NumeroDormitoriosSelecionado");
						$("#PassoDormitorios").addClass("NumeroDormitoriosDesativado");
						
						$("#PassoPlantas").removeClass("OpcoesPlantasDesativado");
						$("#PassoPlantas").addClass("OpcoesPlantasSelecionado");
						
						$("#voltaDorms").fadeIn();
						
						$("#campoDorms").val(id_dormitorio);
						$("#FormHiddenDormitorios").val(id_dormitorio);
						
						//alert(id_dormitorio);
					
					});
			}
		});
	}
	
	function tiposDorms ()
	{
		$("#divComodos").fadeOut(300, function() {
		
			$("#BotaoSalvar").fadeOut();
		
			$("#divTiposPlantas").fadeOut(function() {
				$(".BreadTipo").fadeOut();
				$("#Crumbs").fadeOut();
				$("body").removeClass("Plantas");
				$("body").addClass("Dormitorios");
				$("#divDormitorios").fadeIn();
			});
			
			$("body").removeClass("Dorm1TipoA");
			$("body").removeClass("Dorm1TipoB");
			$("body").removeClass("Dorm2TipoA");
			$("body").removeClass("Dorm2TipoB");
			$("body").removeClass("Dorm2TipoC");
			$("body").removeClass("Dorm2TipoD");
			$("body").removeClass("Dorm4TipoA");
			$("body").removeClass("Dorm3TipoA");
			
			$("#voltaPlantas").fadeOut();
			$("#voltaDorms").fadeOut();
			$("#PassoComodos").removeClass("PersonalizarComodosSelecionado");
			$("#PassoComodos").addClass("PersonalizarComodosDesativado");
			
			$("#PassoPlantas").removeClass("OpcoesPlantasSelecionado");
			$("#PassoPlantas").addClass("OpcoesPlantasDesativado");
			
			$("#PassoDormitorios").removeClass("NumeroDormitoriosDesativado");
			$("#PassoDormitorios").addClass("NumeroDormitoriosSelecionado");
			
			
		
		});

	}
	
	function tiposPlantas ()
	{
		$("#divComodos").fadeOut(300, function() {
		
			$("#BotaoSalvar").fadeOut();
		
			$("body").removeClass("Dorm1TipoA");
			$("body").removeClass("Dorm1TipoB");
			$("body").removeClass("Dorm2TipoA");
			$("body").removeClass("Dorm2TipoB");
			$("body").removeClass("Dorm2TipoC");
			$("body").removeClass("Dorm2TipoD");
			$("body").removeClass("Dorm4TipoA");
			$("body").removeClass("Dorm3TipoA");
			
			$(".BreadTipo").fadeOut();
			$("#divTiposPlantas").fadeIn();
			id_dormitorio = $("#campoDorms").val();
			buscaTipoPlantas(id_dormitorio);
			
			$("#voltaPlantas").fadeOut();
			$("#PassoComodos").removeClass("PersonalizarComodosSelecionado");
			$("#PassoComodos").addClass("PersonalizarComodosDesativado");
		
		});
	}
	
	function buscaComodos (id_tipo_planta)
	{
		$("#BotaoSalvar").fadeIn();
	
		var msg 	= '';
		
		vet_dados 	= 'id_tipo_planta='+ id_tipo_planta;
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/buscaComodos";
		
		var dormitorios = $("#FormHiddenDormitorios").val();
		//alert(dormitorios);
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$("#divTiposPlantas").fadeOut(300, function() {
						$("#divComodos").fadeIn(function () {
							$("body").removeClass("Plantas");
							
							if(id_tipo_planta == 6)
							{
								$("body").addClass("Dorm1TipoA");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo A");
								});
							
							}
							if(id_tipo_planta == 7)
							{
								$("body").addClass("Dorm1TipoB");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo B");
								});
							}
							if(id_tipo_planta == 8)
							{
								$("body").addClass("Dorm2TipoA");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo A");
								});
							}
							if(id_tipo_planta == 9)
							{
								$("body").addClass("Dorm4TipoA");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo A");
								});
							}
							if(id_tipo_planta == 10)
							{
								$("body").addClass("Dorm3TipoA");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo A");
								});
							}
							if(id_tipo_planta == 13)
							{
								$("body").addClass("Dorm2TipoB");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo B");
								});
							}
							if(id_tipo_planta == 11)
							{
								$("body").addClass("Dorm2TipoC");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo C");
								});
							}
							if(id_tipo_planta == 12)
							{
								$("body").addClass("Dorm2TipoD");
								
								$(".BreadTipo").fadeIn(function (){
									$("#BreadTipo").html("Tipo D");
								});
							}
							
							
							
							$("#divComodos").html(msg);
						});
						
						$("#PassoPlantas").removeClass("OpcoesPlantasSelecionado");
						$("#PassoPlantas").addClass("OpcoesPlantasDesativado");
						
						$("#PassoComodos").removeClass("PersonalizarComodosDesativado");
						$("#PassoComodos").addClass("PersonalizarComodosSelecionado");
						
						$("#voltaPlantas").fadeIn();
						
						$("#campoOpcoes").val(id_tipo_planta);
						$("#FormHiddenTiposPlantas").val(id_tipo_planta);
					});
				}
		});
		
		teste1 = $("#campoOpcoes").val();
		teste2 = $("#campoDorms").val();
		
		//alert(teste1+"-"+teste2);	
	}
	
	/*
	function EnviaDadosGeral ()
	{
		var dorms = $("#FormHiddenDormitorios").val();
		
		
		
		if(dorms == 1)
		{
			EnviaDadosUm();
		}
		else if(dorms == 2)
		{
			EnviaDadosDois();
		}
		else if(dorms == 3)
		{
			EnviaDadosTres();
		}
		else if(dorms == 4)
		{
			EnviaDadosQuatro();
		}
	}
	*/
	
	//function EnviaDadosGeral (dorms)
	function EnviaDadosGeral ()
	{
		var dorms = $("#FormHiddenDormitorios").val();
		
		//alert(dorms);
		
		if(dorms == 1)
		{
			EnviaDadosUm();
		}
		else if(dorms == 2)
		{
			EnviaDadosDois();
		}
		else if(dorms == 3)
		{
			EnviaDadosTres();
		}
		else if(dorms == 4)
		{
			EnviaDadosQuatro();
		}
	}
	
	function atualizaSuaPlanta ()
	{
		vet_dados 	= 	'id_dormitorio='+ "S"
						+ '&id_tipo_planta='+ "S";
		
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/atualizaSuaPlanta";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$(".SuaPlanta").html(msg);
					}
		});	
		
	}
	
	function EnviaDadosUm ()
	{
		var msg 	= '';
		
		var cozinha 		= $("#formCozinha").val();
		var quarto 			= $("#formQuarto").val();
		var living 			= $("#formLiving").val();
		var banheiro 		= $("#formBanheiro").val();
		var dormitorios 	= $("#FormHiddenDormitorios").val();
		var tiposPlantas 	= $("#FormHiddenTiposPlantas").val();
		
		//alert(cozinha);
		
		vet_dados 	= 	'cozinha='+ cozinha
					  	+ '&quarto='+ quarto
					  	+ '&living='+ living
					  	+ '&banheiro='+ banheiro
						+ '&dormitorios='+ dormitorios
						+ '&tiposPlantas='+ tiposPlantas;
		
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/enviaDadosUm";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$("#dadosEnviados").html(msg);
					atualizaSuaPlanta();
					}
		});	
		
		//return false;		
	}
	
	function EnviaDadosDois ()
	{
		var msg 	= '';
		
		var tipo		= $("#hiddenTipo").val();
		
		//alert('aqui1');
		
		var cozinha 		= $("#formCozinha").val();
		var casal 			= $("#formQuarto").val();
		var living 			= $("#formLiving").val();
		var banheiro 		= $("#formBanheiro").val();
		//var casaldois		= $("#formQuartoCasal2").val();
		var jantar			= $("#formJantar").val();
		var solt			= $("#formQuartoSol").val();
		var dormitorios 	= $("#FormHiddenDormitorios").val();
		var tiposPlantas 	= $("#FormHiddenTiposPlantas").val();
		
		/*
		alert ("cozinha- "+cozinha);
		alert ("casal- "+casal);
		alert ("living- "+living);
		alert ("banheiro- "+banheiro);
		alert ("casaldois- "+casaldois);
		alert ("jantar- "+jantar);
		alert ("solt- "+solt);
		*/
		
		vet_dados 	= 	'cozinha='+ cozinha
					  	+ '&casal='+ casal
					  	+ '&living='+ living
					  	+ '&banheiro='+ banheiro
					  	//+ '&casaldois='+ casaldois
					  	+ '&jantar='+ jantar
					  	+ '&solt='+ solt
					  	+ '&tipo='+ tipo
						+ '&dormitorios='+ dormitorios
						+ '&tiposPlantas='+ tiposPlantas;
		
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/enviaDadosDois";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
						//alert('aqui2');
					$("#dadosEnviados").html(msg);
					atualizaSuaPlanta();
					}
		});	
		
		return false;		
	}
	
	function EnviaDadosTres ()
	{
		var msg 	= '';
		
		var tipo		= $("#hiddenTipo").val();
		
		var cozinha 		= $("#formCozinha").val();
		var living 			= $("#formLiving").val();
		var quartosol 		= $("#formQuartoSol").val();
		var anexo 			= $("#formAnexo").val();
		var quarto 			= $("#formQuarto").val();
		var banheiro 		= $("#formBanheiro").val();
		var dormitorios 	= $("#FormHiddenDormitorios").val();
		var tiposPlantas 	= $("#FormHiddenTiposPlantas").val();
		
		//alert(quartosol);
		
		vet_dados 	= 	'cozinha='+ cozinha
					  	+ '&living='+ living
					  	+ '&quartosol='+ quartosol
					  	+ '&anexo='+ anexo
					  	+ '&quarto='+ quarto
					  	+ '&banheiro='+ banheiro
					  	+ '&tipo='+ tipo
						+ '&dormitorios='+ dormitorios
						+ '&tiposPlantas='+ tiposPlantas;
		
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/enviaDadosTres";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$("#dadosEnviados").html(msg);
					atualizaSuaPlanta();
					}
		});	
		
		return false;		
	}
	
	function EnviaDadosQuatro ()
	{
		var msg 	= '';
		
		var tipo			= $("#hiddenTipo").val();
		
		var banheiro 		= $("#formBanheiro").val();
		var cozinha 		= $("#formCozinha").val();
		var living 			= $("#formLiving").val();
		var quarto 			= $("#formQuarto").val();
		var quartoa 		= $("#formQuartoA").val();
		var quartob 		= $("#formQuartoB").val();
		var quartoc 		= $("#formQuartoC").val();
		var dormitorios 	= $("#FormHiddenDormitorios").val();
		var tiposPlantas 	= $("#FormHiddenTiposPlantas").val();

		
		
		vet_dados 	= 	'banheiro='+ banheiro
					  	+ '&cozinha='+ cozinha
					  	+ '&living='+ living
					  	+ '&quarto='+ quarto
					  	+ '&quartoa='+ quartoa
					  	+ '&quartob='+ quartob
					  	+ '&quartoc='+ quartoc
					  	+ '&tipo='+ tipo
						+ '&dormitorios='+ dormitorios
						+ '&tiposPlantas='+ tiposPlantas;
		
		base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/enviaDadosQuatro";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					$("#dadosEnviados").html(msg);
					atualizaSuaPlanta();
					}
		});	
		
		return false;		
	}
	
	function mostraOpcoes (idValue)
	{
		$("#ListaOpcoesComodos").find("div").fadeOut(function() {
			
		});
		$("#"+idValue).fadeIn();
	}
	
	function trocaLiving (id_variacao_comodo, id_li)
	{
		// Exeptions - 3D e 4D
		// ========================================
			var segue 	= 1;
			var msg		= "";
		
			// 3-A
			if(id_variacao_comodo == 140)
			{
				var anexoSel = $("#formAnexo").val();
				if (anexoSel == 128 || anexoSel == 129)
				{
					msg += "O LIVING selecionado não pode ser combinado com o ANEXO.";
					//alert('O ANEXO selecionado não pode ser combinado com esse QUARTO.');
					segue = 0;
				}
			}
			
			// 4-A
			/*
			if(id_variacao_comodo == 161)
			{
				var quartoCSel = $("#formQuartoC").val();
				//alert(quartoCSel);
				if (quartoCSel != 155)
				{
					msg += "\nO LIVING selecionado não pode ser combinado com o QUARTO C.";
					//alert('O ANEXO selecionado não pode ser combinado com esse QUARTO.');
					segue = 0;
				}
			}
			*/
			
		// ========================================	
		// Exeptions
		
		
		if(segue == 1)
		{
		
			$("#livingPai").find("div").hide('drop', function() {
				
			});
			$("#living-"+id_variacao_comodo).fadeIn();
			
			//alert("living- "+id_variacao_comodo);
			$("#formLiving").val(id_variacao_comodo);
			
			// Exeptions
			// ========================================
				
				// 2-D
				if(id_variacao_comodo == 115 || id_variacao_comodo == 114)
				{
					var jantarTroca = $("#formJantar").val();
					
					//alert(jantarTroca);
					
					if(jantarTroca == 125)
					{
						trocaJantar('112', 'jantar3');
					}
				}
				
				// 3-A
				/*
				if(id_variacao_comodo == 140)
				{
					var anexoSel = $("#formAnexo").val();
					
					//alert(quartoSel);
					
					if (anexoSel == 129)
					{
						trocaLiving('142', 'livings2');
					}
					
					if (anexoSel == 127)
					{
						trocaAnexo('129', 'anexo1');
					}
				}
				*/
				
				// 3-A
				if(id_variacao_comodo == 140)
				{
					var cozinhaTroca = $("#formCozinha").val();
					
					//alert(jantarTroca);
					
					if(cozinhaTroca == 133)
					{
						trocaCozinha('132', 'cozinhas3');
					}
				}
				
				// 4-A
				if(id_variacao_comodo == 161)
				{
					var quartoC = $("#formQuartoC").val();
					
					//alert(quartoSel);
					
					if (quartoC != 155)
					{
						trocaQuartoC('155', 'quartoc1');
					}
				}
				
				// 4-A
				if(id_variacao_comodo == 162 || id_variacao_comodo == 163 || id_variacao_comodo == 164)
				{
					var quartoC = $("#formQuartoC").val();
					
					//alert(quartoSel);
					
					if (quartoC == 155)
					{
						trocaQuartoC('158', 'quartoc4');
					}
				}
							
			// ========================================
			// Exeptions
			
			$("#OpcoesLiving").find("li").removeClass("ComodoSelecionado");
			$("#"+id_li).addClass("ComodoSelecionado");
			
		}
		else
		{
			alert(msg);
		}
	}
	
	function trocaCozinha (id_variacao_comodo, id_li)
	{
		$("#cozinhaPai").find("div").hide('drop', function() {
			
		});
		$("#cozinha-"+id_variacao_comodo).fadeIn();
		
		// Exeptions - 3D
		// ========================================
			
			// 2-A
			if(id_variacao_comodo == 33)
			{
				var jantarTroca = $("#formJantar").val();
				
				//alert(jantarTroca);
				
				if(jantarTroca == 120)
				{
					trocaJantar('40', 'jantar2');
				}
			}
			
			// 2-B
			if(id_variacao_comodo == 78 || id_variacao_comodo == 80)
			{
				var jantarTroca = $("#formJantar").val();
				
				//alert(jantarTroca);
				
				if(jantarTroca == 86)
				{
					trocaJantar('85', 'jantar3');
				}
			}
			if(id_variacao_comodo == 80)
			{
				var jantarTroca = $("#formJantar").val();
				
				//alert(jantarTroca);
				
				if(jantarTroca == 121)
				{
					trocaJantar('85', 'jantar3');
				}
			}
			
			// 3-A
			if(id_variacao_comodo == 133)
			{
				var livingTroca = $("#formLiving").val();
				
				//alert(jantarTroca);
				
				if(livingTroca == 140)
				{
					trocaLiving('142', 'livings2');
				}
			}
		
		// ========================================
		// Exeptions
		
		//alert("cozinha- "+id_variacao_comodo);
		$("#formCozinha").val(id_variacao_comodo);
		
		$("#OpcoesCozinhas").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");

	}
	
	function trocaQuarto (id_variacao_comodo, id_li)
	{
		// Exeptions - 3D
		// ========================================
			var segue 	= 1;
			var msg		= "";
		
			// 3-A
			
			// Apenas um ajuste em uma ordem de comodos
			if(id_variacao_comodo == 136 || id_variacao_comodo == 137)
			{
				var anexoSel = $("#formAnexo").val();
				if (anexoSel == 127)
				{
					$("#anexo-128").fadeOut();
					$("#anexo-129").fadeOut();
					
					//msg += "O ANEXO selecionado não pode ser combinado com esse QUARTO.";
					//alert('O ANEXO selecionado não pode ser combinado com esse QUARTO.');
					//segue = 1;
				}
			}
			
			// Apenas um ajuste em uma ordem de comodos
			if(id_variacao_comodo == 135)
			{
				var anexoSel = $("#formAnexo").val();
				if (anexoSel == 128 || anexoSel == 129)
				{	
					msg += "O QUARTO selecionado não pode ser combinado com o ANEXO.";
					//alert('O ANEXO selecionado não pode ser combinado com esse QUARTO.');
					segue = 0;
				}
			}
			
			
		// ========================================	
		// Exeptions
	
		if(segue == 1)
		{
	
			$("#quartoPai").find("div").hide('drop', function() {
				
			});
			
			if(id_variacao_comodo == 69 || id_variacao_comodo == 70)
			{
				$("#Anexo3D").css("z-index", "1000");
			}
			else
			{
				$("#Anexo3D").css("z-index", "auto");
			}
			
			// Exeptions
			// ========================================
				
				// 2-A
				if(id_variacao_comodo == 35)
				{
					var jantarSel = $("#formJantar").val();
					
					if(jantarSel == 40)
					{
						trocaJantar('39', 'jantar3');
					}
				}
				
				// 2-B
				if(id_variacao_comodo == 81)
				{
					var jantarSel = $("#formJantar").val();
					
					if(jantarSel == 121)
					{
						trocaJantar('85', 'jantar3');
					}
				}
				
				// 2-D
				if(id_variacao_comodo == 108)
				{
					var jantarSel = $("#formJantar").val();
					
					if(jantarSel == 111 || jantarSel == 125)
					{
						trocaJantar('112', 'jantar3');
					}
				}
				
				// 3-A
				/*
				if(id_variacao_comodo == 135)
				{
					var anexoSel = $("#formAnexo").val();
					
					if(anexoSel == 128 || anexoSel == 129)
					{
						trocaAnexo('127', 'anexo3');
					}
				}
				*/
			
			// ========================================
			// Exeptions
			
			$("#quarto-"+id_variacao_comodo).fadeIn();
			
			//alert("quarto- "+id_variacao_comodo);
			$("#formQuarto").val(id_variacao_comodo);
			
			$("#OpcoesQuarto").find("li").removeClass("ComodoSelecionado");
			$("#"+id_li).addClass("ComodoSelecionado");
		}
		else
		{
			alert(msg);
		}
	}
	
	function trocaQuartoSol (id_variacao_comodo, id_li)
	{
		$("#quartoSolPai").find("div").hide('drop', function() {
			
		});
		$("#quartosol-"+id_variacao_comodo).fadeIn();
		
		// Exeptions
		// ========================================
			
			// 2-C
			if(id_variacao_comodo == 122)
			{
				var banheiroSel = $("#formBanheiro").val();
				
				//alert(banheiroSel);
				
				if(banheiroSel == 89)
				{
					trocaBanheiro('90', 'banheiros1');
				}
			}
			
		// ========================================	
		// Exeptions
		
		//alert("quartoSol- "+id_variacao_comodo);
		$("#formQuartoSol").val(id_variacao_comodo);
		
		$("#OpcoesQuartoSolt").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");
	}
	
	function trocaQuartoA (id_variacao_comodo, id_li)
	{
		$("#quartoAPai").find("div").hide('drop', function() {
			
		});
		$("#quartoa-"+id_variacao_comodo).fadeIn();
		
		$("#formQuartoA").val(id_variacao_comodo);
		
		$("#OpcoesQuartoA").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");
	}
	
	function trocaQuartoB (id_variacao_comodo, id_li)
	{
		$("#quartoBPai").find("div").hide('drop', function() {
			
		});
		$("#quartob-"+id_variacao_comodo).fadeIn();
		
		//alert("quartoB- "+id_variacao_comodo);
		$("#formQuartoB").val(id_variacao_comodo);
		
		$("#OpcoesQuartoB").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");
	}
	
	function trocaQuartoC (id_variacao_comodo, id_li)
	{
		// Exeptions - 4D
		// ========================================
			var segue 	= 1;
			var msg		= "";
			
			
		// ========================================	
		// Exeptions
	
		if(segue == 1)
		{
			$("#quartoCPai").find("div").hide('drop', function() {
				
			});
			$("#quartoc-"+id_variacao_comodo).fadeIn();
			
			//alert("quartoC- "+id_variacao_comodo);
			$("#formQuartoC").val(id_variacao_comodo);
			
			// 4-A
			if(id_variacao_comodo == 156 || id_variacao_comodo == 157 || id_variacao_comodo == 158)
			{
				var livingSel = $("#formLiving").val();
				
				//alert(banheiroSel);
				
				if(livingSel == 161)
				{
					trocaLiving('162', 'livings2');
				}
			}
			else if(id_variacao_comodo == 155)
			{
				var livingSel = $("#formLiving").val();
				
				//alert(banheiroSel);
				
				if(livingSel == 162 || livingSel == 163 || livingSel == 164)
				{
					trocaLiving('161', 'livings1');
				}
			}
			
			$("#OpcoesQuartoC").find("li").removeClass("ComodoSelecionado");
			$("#"+id_li).addClass("ComodoSelecionado");
		}
		else
		{
			alert(msg);
		}
	}
	
	function trocaBanheiro (id_variacao_comodo, id_li)
	{
		$("#banheiroPai").find("div").hide('drop', function() {
				
		});
		$("#banheiro-"+id_variacao_comodo).fadeIn();
		
		// Exeptions
		// ========================================
			
			// 2-C
			if(id_variacao_comodo == 89)
			{
				var quartoSolSel = $("#formQuartoSol").val();
				
				//alert(quartoSolSel);
				
				if(quartoSolSel == 122)
				{
					trocaQuartoSol('96', 'quartosol2');
				}
			}
			
		// ========================================	
		// Exeptions	
		
		//alert("banheiro- "+id_variacao_comodo);
		$("#formBanheiro").val(id_variacao_comodo);
		
		$("#OpcoesBanheiro").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");
	}
	
	function trocaJantar (id_variacao_comodo, id_li)
	{
		$("#jantarPai").find("div").hide('drop', function() { });
		$("#jantar-"+id_variacao_comodo).fadeIn();
		
		// Exeptions
		// ========================================
		
			// 2-A
			if(id_variacao_comodo == 40)
			{
				trocaQuarto('36', 'quartos1');
			}
			if(id_variacao_comodo == 120)
			{
				trocaCozinha('34', 'cozinhas1');
			}
			
			// 2-B
			if(id_variacao_comodo == 121)
			{
				trocaQuarto('82', 'quartos1');
			}
			if(id_variacao_comodo == 86)
			{
				var cozinhaSel = $("#formCozinha").val();
				
				//alert(cozinhaSel);
				if(cozinhaSel == 80 || cozinhaSel == 78)
				{
					trocaCozinha('79', 'cozinhas2');
				}
			}
			
			// 2-D
			if(id_variacao_comodo == 111 || id_variacao_comodo == 125)
			{
				var quartoSel = $("#formQuarto").val();
				if(quartoSel == 108)
				{
					trocaQuarto('106', 'quarto2');
				}
			}
			if(id_variacao_comodo == 125)
			{
				var livingSel = $("#formLiving").val();
				if(livingSel == 115 || livingSel == 114)
				{
					trocaJantar('112', 'jantar3');
				}
			}
			
		// ========================================	
		// Exeptions
		
		//alert("jantar- "+id_variacao_comodo);
		$("#formJantar").val(id_variacao_comodo);
		
		$("#OpcoesJantar").find("li").removeClass("ComodoSelecionado");
		$("#"+id_li).addClass("ComodoSelecionado");
	}
	
	function trocaAnexo (id_variacao_comodo, id_li)
	{
		// Exeptions
		// ========================================
			var segue 	= 1;
			var msg		= "";
		
			// 3-A
			if(id_variacao_comodo == 128 || id_variacao_comodo == 129)
			{
				var quartoSel = $("#formQuarto").val();
				if (quartoSel == 135)
				{
					msg += "O ANEXO selecionado não pode ser combinado com o QUARTO.";
					//alert('O ANEXO selecionado não pode ser combinado com esse QUARTO.');
					segue = 0;
				}
				
				var livingSel = $("#formLiving").val();
				if (livingSel == 140)
				{
					msg += "\nO ANEXO selecionado não pode ser combinado com o LIVING.";
					//alert('O ANEXO selecionado não pode ser combinado com esse LIVING.');
					segue = 0;
				}
			}
			
			
		// ========================================	
		// Exeptions
			
		if(segue == 1)
		{	
			
			$("#anexoPai").find("div").hide('drop', function() {
					
			});
			$("#anexo-"+id_variacao_comodo).fadeIn();
			
			// Exeptions
			// ========================================
			
				// 3-A
				/*
				if(id_variacao_comodo == 128 || id_variacao_comodo == 129)
				{
					var quartoSel = $("#formQuarto").val();
					if (quartoSel == 135)
					{
						trocaQuarto('137', 'quartos1');
					}
					
					var livingSel = $("#formLiving").val();
					if (livingSel == 140)
					{
						trocaLiving('141', 'livings3');
					}
				}
				
				if(id_variacao_comodo == 129)
				{
					var livingSel = $("#formLiving").val();
					
					//alert(quartoSel);
					
					if (livingSel == 140)
					{
						trocaLiving('142', 'livings2');
					}
				}
				
				if(id_variacao_comodo == 127)
				{
					var livingSel = $("#formLiving").val();
					
					//alert(quartoSel);
					
					if (livingSel == 141 || livingSel == 142 || livingSel == 143)
					{
						trocaLiving('140', 'livings4');
					}
				}
				*/
				
			// ========================================	
			// Exeptions	
			
			//alert("anexo- "+id_variacao_comodo);
			$("#formAnexo").val(id_variacao_comodo);
			
			$("#OpcoesAnexo").find("li").removeClass("ComodoSelecionado");
			$("#"+id_li).addClass("ComodoSelecionado");
		
		}
		else
		{
			alert(msg);
		}
	}
	
	function ValidaLogin ()
	{
		var login = $("#txtLoginEmail").val();
		var senha = $("#txtLoginSenha").val();
		
		if(login == '' || senha == '' || login == 'E-mail' || senha == 'Password')
		{
			$("#validaLogin").fadeIn();
			return false;
		}
		return true;
	}
	
	function checkNome(field, rules, i, options)
	{
      if (field.val() == "Nome") {
         //var alertText = "Digite o seu Nome";
         return options.allrules.validatePerNome.alertText;
      }
    }
    
    function checkEmail(field, rules, i, options)
	{
      if (field.val() == "E-mail") {
         //var alertText = "Digite o seu E-mail";
         return options.allrules.validatePerEmail.alertText;
      }
    }

	
	
	jQuery(document).ready(function(){
		jQuery("#FormLogin").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status == true)
				{
					
					$("#btnLogin").attr("disabled","disabled");
				
					var nome		= $("#txtLoginNome").val();
					var email		= $("#txtLoginEmail").val();
					
					var origem		= $("#txtOrigem").val();
					var url			= $("#txtUrl").val();
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&email='+ email
								  +'&origem='+ origem
								  +'&url='+ url;
								  
					base_url  	= "http://www.vergeis.com.br/configurador/index.php/home/enviaDados";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								
								//limpaCampos();
								$("#btnLogin").removeAttr("disabled");
								redirect();
								
								}
					});
					
				}
				else
				{
					return false;
				}
			}  
		})
	});
	
	function redirect()
	{
		var url = "http://www.vergeis.com.br/configurador/index.php/home/visitante";
		$(location).attr('href',url);	
	}
	
	function limpaCampos ()
	{
	    $("#FormLogin").find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'select':
	            case 'text':
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}	