	jQuery(document).ready(function(){
		jQuery("#FormInteresse").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status == true)
				{
					$("#btrnInteresseEnviar").attr("disabled","disabled");
					
					var nome			= $("#txtInteresseNome").val();
					var faixa_etaria	= $("#selectInteresseFaixa").val();
					var estado_civil	= $("#selectInteresseCivil").val();
					var bairro_cidade	= $("#txtInteresseBairroCidade").val();
					var profissao		= $("#txtInteresseProfissao").val();
					var telefone		= $("#txtInteresseTelefone").val();
					var email			= $("#txtInteresseEmail").val();
					var comentarios		= $("#areaInteresseComentarios").val();
					
					var url				= $("#txtUrl").val();
					var origem			= $("#txtOrigem").val();
					
					if ($('#radioInteresseEmail').is(':checked'))
					{
						var contato = "E-mail";
					}
					else if ($('#radioInteresseTelefone').is(':checked'))
					{
						var contato = "Telefone";
					}
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&faixa_etaria='+ faixa_etaria
								  +'&estado_civil='+ estado_civil
								  +'&bairro_cidade='+ bairro_cidade
								  +'&profissao='+ profissao
								  +'&telefone='+ telefone
								  +'&email='+ email
								  +'&contato='+ contato
								  +'&url='+ url
								  +'&origem='+ origem
								  +'&comentarios='+ comentarios;
								  
					base_url  	= "http://www.moradascaminhodomeio.com.br/home/enviarInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								$("#enviarInteresse").html(msg);
								$("#btrnInteresseEnviar").removeAttr("disabled");
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			}  
		})
	});
