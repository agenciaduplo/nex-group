<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] 				= "home";
$route['404_override'] 						= '';

$route['o-projeto'] 						= "home/projeto";
$route['especies'] 							= "especies/index";
$route['contato'] 							= "home/contato";
$route['album-virtual'] 					= "home/album_virtual";
$route['login'] 							= "home/login";
$route['esqueceu-sua-senha'] 				= "login/recuperar_senha";
$route['enviar-senha'] 						= "login/enviar_senha";
$route['cadastro'] 							= "home/cadastro";
$route['busca-aptos'] 						= "home/buscaAptos";
$route['envia-cadastro']					= "home/salvarCadastro";
$route['envia-falecom']						= "home/salvarFalecom";
$route['logar']								= "login/entrar";

$route['especies/aguai'] 					= "especies/visualizar/aguai";
$route['especies/aroeira-vermelha'] 		= "especies/visualizar/aroeira_vermelha";

$route['especies/bugreiro'] 				= "especies/visualizar/bugreiro";
$route['especies/camboata-vermelho'] 		= "especies/visualizar/camboata_vermelho";
$route['especies/canela-amarela'] 			= "especies/visualizar/canela_amarela";
$route['especies/capororoca'] 				= "especies/visualizar/capororoca";
$route['especies/cha-de-bugre'] 			= "especies/visualizar/cha_de_bugre";
$route['especies/chal-chal'] 				= "especies/visualizar/chal_chal";
$route['especies/cocao'] 					= "especies/visualizar/cocao";
$route['especies/embauba'] 					= "especies/visualizar/embauba";
$route['especies/figueira-do-mato'] 		= "especies/visualizar/figueira_do_mato";
$route['especies/guapuruvu'] 				= "especies/visualizar/guapuruvu";
$route['especies/leiteiro'] 				= "especies/visualizar/leiteiro";
$route['especies/louro-mole'] 				= "especies/visualizar/louro_mole";
$route['especies/mamica-de-cadela'] 		= "especies/visualizar/mamica_de_cadela";
$route['especies/marica'] 					= "especies/visualizar/marica";
$route['especies/paineira'] 				= "especies/visualizar/paineira";
$route['especies/pitangueira'] 				= "especies/visualizar/pitangueira";

$route['salvar-foto']						= "album/salvarFoto";
$route['gerenciar-album']					= "album/index";
$route['gerenciar-album/(:num)']			= "album/index/$1";
$route['gerenciar-album/(:any)']			= "album/index/$1";
$route['apagar-foto/(:num)']				= "album/apagarFoto/$1";


$route['empreendimentos/(:num)/(:any)'] 	= "empreendimentos/visualizar/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */