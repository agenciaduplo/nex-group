<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
    <title>Moradas Caminho do Meio - Casas de 2 dormitórios em condomínio fechado em Porto Alegre</title>
    <meta name="description" content="Moradas Caminho do Meio - Casas de 2 dormitórios à venda em condomínio fechado localizado na continuação da Av. Protásio Alves, Estrada Caminho do Meio, 5425 - Porto Alegre. Realização Moradas Rodobens, Rodobens Negócios Imobiliários e Capa Max." />
    <meta name="keywords" content="condominio horizontal, condominio fechado, condominio de casas, casas 2 dorms, casas 2 dormitorios a venda, casas 2 dormitorios a venda em porto alegre, casas a venda na protasio alves, protasio alves" />
    <meta name="author" content="Divex Imobi - http://www.imobi.divex.com.br" />
    <meta name="robots" content="index, follow" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/master.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<!-- Abre scripts DD_BelatedPNG -->
	<!--[if lte IE 6]>
        <script src="<?=base_url()?>assets/js/site/DD_belatedPNG_008a.js" type="text/javascript"></script>
        <script type="text/javascript">
        	DD_belatedPNG.fix('.PNG');
		</script>
	<![endif]-->
	<!-- Fecha scripts DD_BelatedPNG -->

	<!-- Abre scripts Shadowbox -->
    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/site/shadowbox.js"></script>
	<script type="text/javascript">
		Shadowbox.init({
			handleOversize: "drag",
			modal: true
		});
	</script>
	<!-- Fecha scripts Shadowbox -->

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-39']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>

<body class="PNG">
	<div class="Full PNG">

    	<div class="Header">
        	<div class="Container">
            	<h1 class="PNG">Moradas Caminho do Meio</h1>
                <p class="PNG">Nosso maior projeto &eacute; ver voc&ecirc; totalmente realizado.</p>
            </div><!--fecha Container-->
        </div><!--fecha Header-->