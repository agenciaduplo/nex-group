<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getConteudo ($secao, $conteudo)
	{		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM cms_conteudos
				WHERE id_cms_conteudos = $conteudo
				AND id_cms_secoes = $secao
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function setInteresse($data)
	{
		$this->db->flush_cache();	
		$this->db->insert('interesses', $data); 
	}
}

/* End of file home_model.php */
/* Location: ./system/application/model/home_model.php */