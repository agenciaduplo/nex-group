<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getConteudo ($secao, $conteudo)
	{		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM cms_conteudos
				WHERE id_cms_conteudos = $conteudo
				AND id_cms_secoes = $secao
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function setInteresse($data)
	{
		$this->db->flush_cache();	
		$this->db->insert('interesses', $data); 
	}
	
	function setIndique($data)
	{
		$this->db->flush_cache();	
		$this->db->insert('indique', $data); 
	}
	
	function setContato($data)
	{
		$this->db->flush_cache();	
		$this->db->insert('contatos', $data); 
	}
	
	function getEnviaEmail($id_empreendimento, $id_tipo_form)
	{
		$this->db->select('*')->from('emails');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_tipo_form',1);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	function getEmpreendimento($id_empreendimento)
	{
		$this->db->select('*')->from('empreendimentos');
		$this->db->join('cidades', 'cidades.id_cidade = empreendimentos.id_cidade');
		$this->db->where('id_empreendimento',$id_empreendimento);
	
		$query = $this->db->get();
		return $query->row();
	}
}

/* End of file home_model.php */
/* Location: ./system/application/model/home_model.php */