<?php 
@session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home",
			'bodyClass'				=> "Home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function infraestrutura ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "infraestrutura",
			'bodyClass'				=> "Infraestrutura"
		);
		
		$this->load->view('home/infraestrutura', $data);
	}
	
	function localizacao ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "localizacao",
			'bodyClass'				=> "Localizacao"
		);
		
		$this->load->view('home/localizacao', $data);
	}
	
	function casas ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "casas",
			'bodyClass'				=> "Casas"
		);
		
		$this->load->view('home/casas', $data);
	}
	
	function contato ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "contato",
			'bodyClass'				=> "Contato"
		);
		
		$this->load->view('home/contato', $data);
	}
	
	function enviarContato ()
	{
		$this->load->model('home_model', 'model');
		
		$id_empreendimento		= 36;
		$id_estado				= 21;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$telefone 				= $this->input->post('telefone');
		$forma_contato 			= $this->input->post('contato');
		$cidade 				= $this->input->post('cidade');
		$cep 					= $this->input->post('cep');
		$endereco 				= $this->input->post('endereco');
		$comentarios 			= $this->input->post('comentarios');
		$hotsite				= "S";
		
		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'				=> $id_estado,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome'					=> $nome,
			'email'					=> $email,
			'telefone'				=> $telefone,
			'forma_contato'			=> $forma_contato,
			'cidade'				=> $cidade,
			'cep'					=> $cep,
			'endereco'				=> $endereco,
			'comentarios'			=> $comentarios,
			'hotsite'				=> $hotsite
		);
		
		$contato 			= $this->model->setContato($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//inicia o envio do e-mail
		//====================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		ob_start();
		
		?>
			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
				 			<td align="center" colspan="2">
				 				<a target="_blank" href="http://www.nexgroup.com.br">
				 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
				 				</a>
				 			</td>
						 </tr>
						<tr>
							<td colspan='2'>Contato enviado em <?php echo $data_envio; ?> via HotSite</td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
		    				<td colspan="2">Contato enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  			</tr>
						  <tr>
						    <td width="30%">&nbsp;</td>
						    <td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
						    <td width="30%">Empreendimento:</td>
						    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
						    <td width="30%">IP:</td>
						    <td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
						    <td width="30%">E-mail:</td>
						    <td><strong><?=@$email?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Telefone:</td>
						    <td><strong><?=@$telefone?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Cidade:</td>
						    <td><strong><?=@$cidade?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">CEP:</td>
						    <td><strong><?=@$cep?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Endereço:</td>
						    <td><strong><?=@$endereco?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Forma de Contato:</td>
						    <td><strong><?=@$contato?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Comentários:</td>
						    <td valign="top"><strong><?=@$comentarios?></strong></td>
						  </tr>						  					  					  
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>
						  <tr>
					 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						  </tr>
					</table>
				</body>
			</html>
		<?php
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		//====================================================================
		//termina o envio do e-mail
		
		$id_tipo_form 	= 1;
		$emails 		= $this->model->getEnviaEmail($id_empreendimento, $id_tipo_form);
		
		foreach ($emails as $email)
		{
			if($email->tipo === 'para')
			{
				$list[] = $email->email;
			}
			else if($email->tipo === 'cc')
			{
				$list_cc[] = $email->email;
			}
			else
			{
				$list_bcc[] = $email->email;
			}
		}
		
		$list_bcc[] = 'atendimento@divex.com.br';	
		$this->email->from("noreply@vergeis.com.br", "Nex Group");
		 
		if($nome=='teste123')
		{
			$this->email->to('bruno.freiberger@divex.com.br');
		}
		else
		{
		 	if(@$list)
		 	{
				$this->email->to($list);
			}
			if(@$list_cc)
			{
				$this->email->cc($list_cc);
			}
			if(@$list_bcc)
			{
				$this->email->bcc($list_bcc);
			}	
		}
		
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->subject('Contato enviado via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//echo "Contato enviado com sucesso!";
	}
	
	function enviarContatoOLD ()
	{
		$this->load->model('home_model', 'model');
		
		if($_POST)
		{
		
			$nome 			= $this->input->post('nome');
			$email 			= $this->input->post('email');
			$endereco 		= $this->input->post('endereco');
			$cidade 		= $this->input->post('cidade');
			$uf 			= $this->input->post('uf');
			$cep 			= $this->input->post('cep');
			$telefone 		= $this->input->post('telefone');
			$comentario 	= $this->input->post('comentario');
			
			$id_empreendimento	= 25;
	
			$ip 			= $this->input->ip_address();
			$user_agent		= $this->input->user_agent();
			$data			= date("Y-m-d H:i:s");
			
			//CADASTRA CONTATO NO BANCO DO NEXGROUP
			$dataNex = array (
				'id_empreendimento'		=> $id_empreendimento,
				'ip'					=> $ip,
				'user_agent'			=> $user_agent,
				'nome'					=> $nome,
				'email'					=> $email,
				'endereco'				=> $endereco,
				'cidade'				=> $cidade,
				'uf'					=> $uf,
				'cep'					=> $cep,
				'telefone'				=> $telefone,
				'comentarios'			=> $comentario,
				'origem'				=> $_SESSION['origem'],
				'url'					=> $_SESSION['url'],
				'senha'					=> "}3_$%wB`7Atbdk1w"
				
			);
			
			$url= "http://www.nexgroup.com.br/index.php/integracao/cadastraContato";
			$this->CadastraNex($dataNex, $url);
			//FIM CADASTRA CONTATO NO BANCO DO NEXGROUP
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] 	= 'sendmail';
			$config['charset'] 		= 'utf-8';
			$config['wordwrap'] 	= TRUE;
			
			$this->email->initialize($config);
			
			//Inicio da Mensagem
			
			ob_start();
			
			?>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>NEX GROUP</title>
				</head>
				
				<body>
				<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
				  <tr align="center">
				  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" alt="NEX GROUP" src="http://localhost/nexgroup/assets/admin/img/logo_nex-02.jpg"></a></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2"><b>MORADAS CAMINHO DO MEIO</b> :: Contato via HotSite</td>
				  </tr>
				  <tr>
				    <td colspan="2">Contato enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>				  
				  <tr>
				    <td width="30%">E-mail:</td>
				    <td><strong><?=$email?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Endereço:</td>
				    <td><strong><?=$endereco?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Cidade:</td>
				    <td><strong><?=$cidade?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">UF:</td>
				    <td><strong><?=$uf?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">CEP:</td>
				    <td><strong><?=$cep?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Telefone:</td>
				    <td><strong><?=$telefone?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%" valign="top">Comentários:</td>
				    <td valign="top"><strong><?=$comentario?></strong></td>
				  </tr>				  					  					  
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr align="center">
				    <td colspan="2" align="center">
				    	NEX GROUP
				    	<a href="http://www.nexgroup.com.br">www.nexgroup.com.br</a>	
				    </td>
				  </tr>		  
				</table>
				</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("$email", "$nome");
			
			if($nome=="teste123"){
				$list = array('bruno.freiberger@divex.com.br');
			}else{
				$list = array(
					'saldanha@capa.com.br',
					'atendimento@capa.com.br',
					'marca.savi@bol.com.br',
					'capires@rodobens.com.br'
				);	
			}			
			$this->email->to($list);
			$this->email->bcc('atendimento@divex.com.br');
			$this->email->subject('MORADAS CAMINHO DO MEIO - Contato enviado pelo HotSite');
			$this->email->message("$conteudo");
			
			//$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->load->view('home/ajax.contato.php');
		}
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$id_empreendimento		= 18;
		$id_estado				= 21;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$telefone 				= $this->input->post('telefone');
		$faixa_etaria 			= $this->input->post('faixa_etaria');
		$estado_civil 			= $this->input->post('estado_civil');
		$profissao 				= $this->input->post('profissao');
		$forma_contato 			= $this->input->post('contato');
		$comentarios 			= $this->input->post('comentarios');
		$bairro_cidade 			= $this->input->post('bairro_cidade');
		$hotsite				= "S";
		
		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'				=> $id_estado,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome'					=> $nome,
			'email'					=> $email,
			'telefone'				=> $telefone,
			'faixa_etaria'			=> $faixa_etaria,
			'estado_civil'			=> $estado_civil,
			'profissao'				=> $profissao,
			'forma_contato'			=> $forma_contato,
			'comentarios'			=> $comentarios,
			'bairro_cidade'			=> $bairro_cidade,
			'hotsite'				=> $hotsite
		);
		
		$interesse 			= $this->model->setInteresse($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//inicia o envio do e-mail
		//====================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		ob_start();
		
		?>
			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
				 			<td align="center" colspan="2">
				 				<a target="_blank" href="http://www.nexgroup.com.br">
				 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
				 				</a>
				 			</td>
						 </tr>
						<tr>
							<td colspan='2'>Interesse enviado em <?php echo $data_envio; ?> via HotSite</td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
		    				<td colspan="2">Interesse enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  			</tr>
						  <tr>
						    <td width="30%">&nbsp;</td>
						    <td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
						    <td width="30%">Empreendimento:</td>
						    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
						    <td width="30%">IP:</td>
						    <td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
						    <td width="30%">E-mail:</td>
						    <td><strong><?=@$email?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Faixa Etária:</td>
						    <td><strong><?=@$faixa_etaria?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Estado Civil:</td>
						    <td><strong><?=@$estado_civil?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Bairro e Cidade:</td>
						    <td><strong><?=@$bairro_cidade?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Profissão:</td>
						    <td><strong><?=@$profissao?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Telefone:</td>
						    <td><strong><?=@$telefone?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Forma de Contato:</td>
						    <td><strong><?=@$contato?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Comentários:</td>
						    <td valign="top"><strong><?=@$comentarios?></strong></td>
						  </tr>						  					  					  
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>
						  <tr>
					 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						  </tr>
					</table>
				</body>
			</html>
		<?php
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		//====================================================================
		//termina o envio do e-mail
		
		$id_tipo_form 	= 1;
		$emails 		= $this->model->getEnviaEmail($id_empreendimento, $id_tipo_form);
		
		foreach ($emails as $email)
		{
			if($email->tipo === 'para')
			{
				$list[] = $email->email;
			}
			else if($email->tipo === 'cc')
			{
				$list_cc[] = $email->email;
			}
			else
			{
				$list_bcc[] = $email->email;
			}
		}
		
		$list_bcc[] = 'atendimento@divex.com.br';	
		$this->email->from("noreply@moradascaminhodomeio.com.br", "Nex Group");
		 
		if($nome=='teste123')
		{
			$this->email->to('bruno.freiberger@divex.com.br');
		}
		else
		{
		 	if(@$list)
		 	{
				$this->email->to($list);
			}
			if(@$list_cc)
			{
				$this->email->cc($list_cc);
			}
			if(@$list_bcc)
			{
				$this->email->bcc($list_bcc);
			}	
		}
		
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->subject('Interesse enviado via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		$this->load->view('home/ajax.interesse.php');
	}
	
	function enviarIndique ()
	{
		$this->load->model('home_model', 'model');
	
		$id_empreendimento 		= 18;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome_remetente 		= $this->input->post('nome_remetente');
		$email_remetente 		= $this->input->post('email_remetente');
		$nome_destinatario 		= $this->input->post('nome_amigo');
		$email_destinatario		= $this->input->post('email_amigo');
		$comentarios 			= $this->input->post('comentarios');

		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome_remetente'		=> $nome_remetente,
			'email_remetente'		=> $email_remetente,
			'nome_destinatario'		=> $nome_destinatario,
			'email_destinatario'	=> $email_destinatario,
			'comentarios'			=> $comentarios
		);
		
		$indique 			= $this->model->setIndique($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>MORADAS CAMINHO DO MEIO</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>MORADAS CAMINHO DO MEIO</b> :: Indicação via HotSite</td>
			  </tr>
			  <tr>
			    <td colspan="2">Indicação enviada por <strong><?=$nome_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>			  
			  <tr>
			    <td colspan="2">Olá <?=@$nome_destinatario?>, acesse o HotSite do MORADAS CAMINHO DO MEIO<br/><strong><a href="http://www.moradascaminhodomeio.com.br">Clique aqui</a></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">Nome / E-mail do remetente:</td>
			    <td><strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.capa.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		$this->email->from("noreply@moradascaminhodomeio.com.br", "$nome_remetente");
		
		if($nome_remetente=="teste123")
		{
			$list = array('bruno.freiberger@divex.com.br');
		} else 
		{
			$list = array(
				"$email_destinatario"
			);	
		}
		$this->email->to($list);
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->subject('MORADAS CAMINHO DO MEIO - Indicação via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email	
		
		$this->load->view('home/ajax.indique.php');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */