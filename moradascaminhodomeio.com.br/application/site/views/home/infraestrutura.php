<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<ul class="FeaturedItems ui-tabs-nav">
					<li id="nav-item-1" class="ui-tabs-nav-item ui-tabs-selected"><h3><a href="#item-1" title="O Empreendimento">O Empreendimento</a></h3></li>
					<li id="nav-item-2" class="ui-tabs-nav-item"><h3><a href="#item-2" title="Portaria Central">Portaria Central</a></h3></li>
					<li id="nav-item-3" class="ui-tabs-nav-item"><h3><a href="#item-3" title="Salão de Festas">Salão de Festas</a></h3></li>
					<li id="nav-item-4" class="ui-tabs-nav-item"><h3><a href="#item-4" title="Quiosque com churrasqueiras">Quiosque com churrasqueiras</a></h3></li>
					<li id="nav-item-5" class="ui-tabs-nav-item"><h3><a href="#item-5" title="Minicampo de Futebol Gramado">Minicampo de Futebol Gramado</a></h3></li>
					<li id="nav-item-6" class="ui-tabs-nav-item"><h3><a href="#item-6" title="Playground">Playground</a></h3></li>
				</ul> <!-- .FeaturedItems -->
				<div class="ImagePreview">
					<div id="item-1" class="ui-tabs-panel"><img src="<?=base_url()?>assets/img/site/infra-empreendimento.jpg" alt="Empreendimento" /></div>
					<div id="item-2" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-condominio-fechado-portaria-central.jpg" alt="Portaria" /></div>
					<div id="item-3" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-salao-de-festas.jpg" alt="Salão de Festa" /></div>
					<div id="item-4" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-quiosque-com-churrasqueira.jpg" alt="Quiosque com churrasqueiras"  /></div>
					<div id="item-5" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-minicampo-futebol-gramado.jpg" alt="Minicampo de Futebol Gramado" /></div>
					<div id="item-6" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-playground.jpg" alt="Playground"  /></div>
				</div> <!-- .ImagePreview -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>