<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<div class="Sidebar">
					<span class="Tel Imgr">(55) 3223 5660 / (55) 2103 8210</span>
					<p class="Last">BREVE PLANTÃO DE VENDAS:<br /> continuação da A. Protásio Alves<br/><b>Estrada Caminho do Meio, 5425</b><br /> Alvorada - RS</p>
				</div> <!-- .Sidebar -->
				<form id="FormContato" class="Form" method="post" onsubmit="return validaContato();">
					<h3 class="Imgr">Formulário de contato</h3>
					<ul class="First">
						<li>
							<label for="NomeContato">nome</label>
							<input type="text" id="NomeContato" class="CampoPadrao" name="NomeContato" />
						</li>
						<li>
							<label for="EmailContato">e-mail</label>
							<input type="text" id="EmailContato" class="CampoPadrao" name="EmailContato" />
						</li>
						<li>
							<label for="Endereco">endereço</label>
							<input type="text" id="Endereco" class="CampoPadrao" name="Endereco" />
						</li>
						<li>
							<label for="Uf" class="LabelUf">UF</label>
							<select id="Uf" class="CampoPadrao" name="Uf">
								<option value="AC">Acre</option>
							    <option value="AL">Alagoas</option>
							    <option value="AM">Amazonas</option>
							    <option value="AP">Amapá</option>
							    <option value="BA">Bahia</option>
							    <option value="CE">Ceará</option>
							    <option value="DF">Distrito Federal</option>
							    <option value="ES">Espirito Santo</option>
							    <option value="GO">Goiás</option>
							    <option value="MA">Maranhão</option>
							    <option value="MG">Minas Gerais</option>
							    <option value="MS">Mato Grosso do Sul</option>
							    <option value="MT">Mato Grosso</option>
							    <option value="PA">Pará</option>
							    <option value="PB">Paraíba</option>
							    <option value="PE">Pernambuco</option>
							    <option value="PI">Piauí</option>
							    <option value="PR">Paraná</option>
							    <option value="RJ">Rio de Janeiro</option>
							    <option value="RN">Rio Grande do Norte</option>
							    <option value="RO">Rondônia</option>
							    <option value="RR">Roraima</option>
							    <option selected="selected" value="RS">Rio Grande do Sul</option>
							    <option value="SC">Santa Catarina</option>
							    <option value="SE">Sergipe</option>
							    <option value="SP">São Paulo</option>
							    <option value="TO">Tocantins</option>
							</select>
						</li>
						<li>
							<label for="Cidade" class="LabelCidade">cidade</label>
							<input type="text" id="CidadeContato" class="CampoPadrao" name="Cidade" />
						</li>
					</ul>
					<ul class="Left">
						<li>
							<label for="Cep">CEP</label>
							<input type="text" id="Cep" class="CampoPadrao" name="Cep" />
						</li>
						<li>
							<label for="TelefoneContato">telefone</label>
							<input type="text" id="TelefoneContato" class="CampoPadrao" name="TelefoneContato" />
						</li>
						<li>
							<label for="Comentario3">comentário</label>
							<textarea id="Comentario3" class="ComentarioContato CampoPadrao" name="Comentario3" cols="40" rows="8"></textarea>
						</li>
					</ul>
					<div class="Submitbar-contato">
						<span style="display:none;" id="MensagemErroContato" class="MensagemErro">Preencha todos os campos corretamente.</span>
						<span style="display:none;" class="MensagemSucesso" id="MensagemSucessoContato"></span>
						<input type="submit" id="Enviar" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>