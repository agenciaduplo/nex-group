<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<div class="Sidebar">
					<p class="Location">Continuação da Av. Protásio Alves <b>Estrada Caminho do Meio, 5425</b> Alvorada - RS</p>
					<p class="Last">Breve Plantão de Vendas no Local:</p>
					<p class="Last">(51) 3435 4673</p>
					<a href="http://maps.google.com/maps?q=Estrada+Caminho+do+Meio,+5425+Alvorada+-+RS&hl=pt-BR&sll=-29.706785,-53.785415&sspn=0.015395,0.033023&vpsrc=0&z=17" class="GoogleMaps Imgr" rel="external" title="Google Maps">Google Maps</a>
				</div> <!-- .Sidebar -->
				<div class="ImagePreview">
					<img src="<?=base_url()?>assets/img/site/mapa.jpg" alt="R. Pedro Santini, próximo à Sede Campestre do Clube Dores Santa Maria" class="Map" width="601" height="311" />
				</div> <!-- .ImagePreview -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>