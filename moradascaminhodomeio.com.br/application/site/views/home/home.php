<?=$this->load->view('includes/header');?>

		<div class="Content">
				<div class="Box">
					<div id="featured">
						<img src="<?=base_url()?>assets/img/site/home-panoramica.jpg" alt="Empreendimento" width="940" />
						<img src="<?=base_url()?>assets/img/site/home-portaria.jpg" alt="Portaria" width="940"/>
						<img src="<?=base_url()?>assets/img/site/home-salao-de-festas.jpg" alt="Salão de Festas" width="940" />
						
						<!--<img src="<?=base_url()?>assets/img/site/home-quiosque-com-churrasqueira.jpg" alt="Quiosque com churrasqueiras" width="940" />
						<img src="<?=base_url()?>assets/img/site/home-minicampo-futebol-gramado.jpg" alt="Minicampo de Futebol Gramado" width="940" />
						<img src="<?=base_url()?>assets/img/site/home-playground.jpg" alt="Playground" width="940" />-->
					</div>
				</div>
			<!--<div class="Featured">
				<ul class="FeaturedItems ui-tabs-nav">
					<li id="nav-item-1" class="ui-tabs-nav-item ui-tabs-selected"><h3><a href="#item-1" title="Portaria">Portaria</a></h3></li>
					<li id="nav-item-2" class="ui-tabs-nav-item"><h3><a href="#item-2" title="Salão de Festas">Salão de Festas</a></h3></li>
					<li id="nav-item-3" class="ui-tabs-nav-item"><h3><a href="#item-3" title="Piscina Adulto e Infantil">Piscina Adulto e Infantil</a></h3></li>
					<li id="nav-item-4" class="ui-tabs-nav-item"><h3><a href="#item-4" title="Quiosque com churrasqueiras">Quiosque com churrasqueiras</a></h3></li>
					<li id="nav-item-5" class="ui-tabs-nav-item"><h3><a href="#item-5" title="Minicampo de Futebol Gramado">Minicampo de Futebol Gramado</a></h3></li>
					<li id="nav-item-6" class="ui-tabs-nav-item"><h3><a href="#item-6" title="Playground">Playground</a></h3></li>
				</ul>
				<div class="ImagePreview">
					<div id="item-1" class="ui-tabs-panel"><img src="<?=base_url()?>assets/img/site/home-portaria.jpg" alt="Portaria" width="640" height="334" /></div>
					<div id="item-2" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/home-salao-de-festas.jpg" alt="Salão de Festas" width="640" height="334" /></div>
					<div id="item-3" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/home-piscina-adulto-infantil.jpg" alt="Piscina adulto e infantil" width="640" height="334" /></div>
					<div id="item-4" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/home-quiosque-com-churrasqueira.jpg" alt="Quiosque com churrasqueiras" width="640" height="334" /></div>
					<div id="item-5" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/home-minicampo-futebol-gramado.jpg" alt="Minicampo de Futebol Gramado" width="640" height="334" /></div>
					<div id="item-6" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/home-playground.jpg" alt="Playground" width="640" height="334" /></div>
				</div>
			</div>--> <!-- .Featured -->
			<div class="Container">
				<ul class="Listing">
					<li>Condomínio fechado com portaria central</li>
					<li>Salão de festas</li>
					<li>Quiosque com churrasqueiras</li>
					<li>Minicampo de futebol gramado</li>
					<li>Playground</li>
					<li>Vagas de estacionamento</li>
					<li>Sistema de comunicação entre casas e portaria</li>
				</ul> <!-- .Listing -->
				<ul class="Listing">
					<li>Saldo até 100% financiado</li>
					<li>Ganhe benefício de até R$ 17 mil**</li>
					<li>Juros a partir de 4,5% a.a. (0,37% a.m.)</li>
					<li>Utilize o Fundo de Garantia (FGTS)***</li>
					<li>Seguro Desemprego****</li>
					<li>Baixo valor condominial</li>
				</ul> <!-- .Listing -->
				<div class="Right"  style="width:306px">
					<div class="Location">
						<p>PLANTÃO DE VENDAS:</p>
						<p>continuação da A. Protásio Alves</p>
						<p><b>Estrada Caminho do Meio, 5425</b></p>
						<p>Alvorada - RS</p>
					</div> <!-- .Location -->
					<span class="Tel Imgr">(55) 3223 5660 / (55) 2103 8210</span>
				</div>
			</div> <!-- .Container -->
		</div> <!-- .Content -->
		<div class="Container">
			<div class="LegalText">
				<p>*Benefício de acordo com renda familiar conforme enquadramento do cliente no programa Minha Casa, Minha Vida do Governo Federal. **FGTS conforme condições de liberação da Caixa Econômica Federal. ***O Fundo garantidor do programa Minha Casa, Minha Vida do Governo Federal garante 12, 24 ou 36 parcelas do financiamento em caso de desemprego, de acordo com a renda familiar. Incorporação Protocolada no Registro de Imóveis de Alvorada - RS sob nº 020331-E. Imagens meramente ilustrativas. Projeto Arquitetônico e Paisagístico: Carlos Alberto de Moraes Schettert - CREA 27.239.  </p>
			</div> <!-- .LegalText -->
		</div>
<?=$this->load->view('includes/footer');?>