<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="index, follow">
	<meta name="description" content="Like - Open Concept - um novo conceito em um novo endereço, para quem está sempre em todos os lugares.">
	<meta name="keywords" content="Like, nex like, like open concept, lançamento NEX, Nex Group, apartamento Porto Alegre, Apartamento 1 e 2 dormitórios, Studios Like, novo empreendimento porto alegre, espaço gourmet, salão de festas, pub, área de lazer, piscina.
">
	<meta name="author" content="Agência Duplo">
	<title>LIKE - Open Concept - Nex Group</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="fancybox/jquery.fancybox.css">
	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
	
	<meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='NEX Like'/>
	<meta property='og:image' content='http://www.nexlike.com.br/img/fb-like-share.jpg'/>
	<meta property='og:description' content='Vem aí o novo endereço de quem está sempre em todos os lugares.'/>
    <meta property='og:url' content='http://www.nexlike.com.br/'/>

	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- GA -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-69113303-7', 'auto');
		ga('send', 'pageview');
	</script>
	
	<!-- Tracker -->
	<script type="text/javascript">
		window.smartlook||(function(d) {
		var o=smartlook=function(){ o.api.push(arguments)},s=d.getElementsByTagName('script')[0];
		var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
		c.charset='utf-8';c.src='//rec.getsmartlook.com/bundle.js';s.parentNode.insertBefore(c,s);
		})(document);
		smartlook('init', '98a993314688dc5ef9cd9a04ae95a37a3f8c0aea');
	</script>
	<?php 
		if(isset($_GET['id']) && $_GET['id'] != 'index.php'){
			
			$midia = $_GET['id'];
			
		}  if(isset($_SERVER['HTTP_REFERER'])) {
			
			$midia = $_SERVER['HTTP_REFERER'];
			
		}	else {
			
			$midia = 'direto';
			
		}
	?>
</head>

<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<pre style="display:none;">
		<?php
			print_r($_SERVER['HTTP_REFERER']);
		?>
	</pre>
	<section class="container-fluid like">
		<header class="row topo animated fadeInDown">
			<div class="col-md-4 col-xs-12">
				<img class="logo-like img-responsive center-block" src="img/logo-like.png" alt="Like - Open Concept">
			</div>
			<div class="col-md-4 hidden-xs"></div>
			<div class="col-md-4 col-xs-12">
				<a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=61&midia=<?php echo $midia; ?>" data-fancybox-type="iframe" class="chat center-block" onclick="ga('send', 'event', 'Chat', 'Clique', 'Topo');">clique aqui e obtenha mais informações</a>
			</div>
		</header>
		<section class="conteudo hidden-xs">
			<div class="row chamada animated fadeIn">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<img src="img/chamada.png" alt="Like" class="img-responsive center-block visible-md visible-lg">
					<img src="img/chamada-sm.png" alt="Like" class="img-responsive center-block visible-sm">
				</div>
				<div class="col-md-1"></div>
			</div>
		</section>
		<section class="container-fluid conteudo-xs visible-xs">
			<div class="row">
				<div class="col-md-12"><img src="img/_chamada-xs.png" class="img-responsive" alt="Like"></div>
			</div>
			<div class="row">
				<div class="col-md-12"><img src="img/_predio-xs.png" alt="Like" class="img-responsive center-block"></div>
			</div>
			<div class="row">
				<div class="col-md-12"><img src="img/_features-xs.png" class="img-responsive" alt="Like"></div>
			</div>

		</section>
	</section>
	<footer class="container-fluid">
	
		<div class="container form-contato">
			<form id="formContact" novalidate="novalidate">
				<div class="row">
					<div class="col-md-12">
						<h1 class="osextra themecolor text-center">FICOU INTERESSADO?</h1>
						<h2 class="osnormal themecolor text-center">SOLICITE UM CONTATO DE NOSSA EQUIPE:</h2>
					</div>
				</div>
				<div class="spacer"></div>
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="row">
							<div class="col-md-12">
								<input name="nome" id="nome" type="text" class="field required themecolor" placeholder="Nome (obrigatório)">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input name="email" id="email" type="text" class="field required themecolor" placeholder="E-mail (obrigatório)">
							</div>
							<div class="col-md-6">
								<input name="telefone" type="text" class="field themecolor telefone" placeholder="Telefone" autocomplete="off">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">		
								<textarea name="msg" class="field themecolor" placeholder="Mensagem" rows="4"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="error-messages"></div>
								<div class="success-messages"></div>
							</div>
							<div class="col-md-4">		
								<input type="hidden" id="is_mobile" name="is_mobile" value="yes" class="hide show-mobile">
								<input type="hidden" name="ref" value="<?php echo $midia; ?>">
								<input type="submit" id="btn-enviar" class="btn whitecolor osextra" value="ENVIAR">
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</form>
		
			<hr>
		</div>

		<div class="container dados-contato">
			<div class="row hidden-xs">
				<div class="col-md-6">
					<p>O empreendimento Like só será comercializado após a aprovação de seu projeto e posterior registro de sua incorporação no Cartório de Registro de Imóveis. Imagens meramente ilustrativas.</p>
				</div>
				<div class="col-md-3">
					<a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=61" data-fancybox-type="iframe" class="chat" onclick="ga('send', 'event', 'Chat', 'Clique', 'Rodape Desktop');">
						<img class="pull-right" src="img/corretor-online.png" alt="Corretor Online">
					</a>
				</div>
				<div class="col-md-3">
					<img src="img/telefone-nexvendas.png" alt="Nex Vendas - (51) 3092.3124">
				</div>
			</div>

			<div class="row visible-xs">
				<div class="col-xs-12 text-center">
					<p>O empreendimento Like só será comercializado após a aprovação de seu projeto e posterior registro de sua incorporação no Cartório de Registro de Imóveis. Imagens meramente ilustrativas.</p>
				</div>
				<div class="col-xs-12">
					<a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=61" data-fancybox-type="iframe" class="chat" onclick="ga('send', 'event', 'Chat', 'Clique', 'Rodape Mobile');">
						<img class="center-block" src="img/corretor-online.png" alt="Corretor Online">
					</a>
				</div>
				<div class="col-xs-12">
					<a href="tel:5130923124" onclick="ga('send', 'event', 'Telefone', 'Clique', 'Rodape Mobile');">
						<img class="center-block" src="img/telefone-nexvendas.png" alt="Nex Vendas - (51) 3092.3124">
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="fb-share-button" data-href="http://nexlike.com.br/" data-layout="button" data-mobile-iframe="true" data-size="large">
						<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fnexlike.com.br%2F&amp;src=sdkpreparse">Compartilhar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid legaltext">
			<div class="container">
				<div class="row hidden-xs">
					<a href="http://www.nexgroup.com.br/" target="_blank" onclick="ga('send', 'event', 'Nex Group', 'Clique', 'Rodape Desktop');">
						<div class="col-md-2"><img src="img/logo-nex.png" alt=""></div>
					</a>
					<div class="col-md-8 text-center">2016® - Todos os direitos reservados www.nexgroup.com.br</div>
					<a href="http://www.agenciaduplo.com.br" target="_blank" onclick="ga('send', 'event', 'Duplo', 'Clique', 'Rodape Desktop');">
						<div class="col-md-2 text-right"><img src="img/logo-duplo.png" alt=""></div>
					</a>
				</div>
				<div class="row visible-xs">
					<div class="row">
						<div class="col-xs-12 text-center">2016® - www.nexgroup.com.br</div>
					</div>
					<div class="row">
						<a href="http://www.nexgroup.com.br/" target="_blank" onclick="ga('send', 'event', 'Nex Group', 'Clique', 'Rodape Mobile');">
							<div class="col-xs-5 col-xs-offset-1"><img src="img/logo-nex.png" alt=""></div>
						</a>
						<a href="http://www.agenciaduplo.com.br/" target="_blank" onclick="ga('send', 'event', 'Duplo', 'Clique', 'Rodape Mobile');">
							<div class="col-xs-5 text-right"><img src="img/logo-duplo.png" alt=""></div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php if(isset($_GET['video'])) { ?>
	<div class="modal fade in" id="pontos-de-venda" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display:block;background:rgba(0,0,0,0.7)">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				<iframe style="width:100%;height:400px;" src="https://www.youtube.com/embed/mVd6PI9LGQo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- jquery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script type="text/javascript" src="http://www.nexlike.com.br/js/jquery.form.js"></script>
	<script type="text/javascript" src="http://www.nexlike.com.br/js/jquery.mask.min.js"></script>
	<script type="text/javascript" src="http://www.nexlike.com.br/js/jquery.validate.min.js"></script>
	<!-- bootstrap -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="fancybox/jquery.fancybox.pack.js"></script>
	<script src="js/script.js"></script>
</body>

</html>