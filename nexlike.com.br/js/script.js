$(function(){
	
	$('.telefone').mask('(00) 000000000');
	
	$("a.chat").fancybox({
		maxWidth	: 650,
		maxHeight	: 600,
		minHeight   : 500,
		fitToView	: false,
		width		: '90%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		padding 	: 0
	});
	
	$('#formContact').validate({
		errorClass: 'error',
		rules: {
			nome: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			nome: {
				required: "Por favor preencha seu nome.",
				minlength: "Seu nome deve ter mais caracteres."
			},
			email: {
				required: "Por favor preencha seu e-mail."
			}
		},
		errorPlacement: function(error, element) {
			error.insertBefore( element );
			$('.error-messages').html(error);
		},
		submitHandler: function(form, event) {

			//Desabilita o form enquanto estiver enviando
			$('#formContact').addClass('locked');
			$('#formContact #btn-enviar').attr('value', 'Enviando...');
			$('#formContact #btn-enviar').attr('disabled', 'disabled');

			//Envia o form via ajax
			$(form).ajaxSubmit({
				type:"POST",
				data: $('#formContact').serialize(),
				url: "/envia/envia.php",
				success: function(data) {
					console.log(data);
					
					if(data == 'success'){
						$('.success-messages').html('Sua mensagem foi enviada com successo. Em breve retornaremos seu contato.');
					} else {
						$('.error-messages').html('Não foi possível enviar sua mensagem. Tente novamente.');
					}
					
					//Habilita o form novamente e limpa
					$('#formContact').removeClass('locked');
					$('#formContact #btn-enviar').attr('value', 'ENVIAR');
					$('#formContact #btn-enviar').removeAttr('disabled');
					$('#formContact')[0].reset();
					
				},
				error: function() {
				  
				}
			});
		}
	});
});

