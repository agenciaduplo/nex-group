<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
	
	public function index(){
		$this->load->helper('source');
		source();
		$this->load->view('Contato');
	}
	public function notfound(){
		show_404();
	}
}