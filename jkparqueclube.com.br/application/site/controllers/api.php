<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function index(){
		show_404();
	}
	public function enviar(){
		$this->load->helper('source');
		$this->load->model('contato_model','model');
		$p = array(
			'id_empreendimento' => 87,
			'id_estado' => 21,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date('Y-m-d H:i:s'),
			'nome' => ucwords(strtolower($this->input->post('form_nome'))),
			'email' => strtolower($this->input->post('form_email')),
			'comentarios' => $this->input->post('form_telefone'),
			'telefone' => $this->input->post('form_comentario'),
			'hotsite' => 'S'
		);
		if(preg_match(EMAIL_FORMAT,$p['email'])){
			if(!empty($p['nome']) && !empty($p['comentarios']) && !empty($p['telefone'])){
			
				$interesse = $this->model->insert($p);
				
				$empreendimento		= $this->model->getEmpreendimento($p['id_empreendimento']);
				
				//inicia o envio do e-mail
				//====================================================================
				
				$this->load->library('email');
				
				$config['protocol'] = 'sendmail';
				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				ob_start();
				
				?>
					<html>
						<head>
							<title>Nex Group</title>
						</head>
						<body>
							<table>
								<tr align="center">
						 			<td align="center" colspan="2">
						 				<a target="_blank" href="http://www.nexgroup.com.br">
						 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
						 				</a>
						 			</td>
								 </tr>
								<tr>
									<td colspan='2'>Interesse enviado em <?php echo $p['data_envio']; ?> via HotSite</td>
								</tr>
								<tr>
									<td colspan='2'>&nbsp;</td>
								</tr>
								
								<tr>
				    				<td colspan="2">Interesse enviado por <strong><?=@$p['nome']?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
					  			</tr>
								  <tr>
								    <td width="30%">&nbsp;</td>
								    <td width="70%">&nbsp;</td>
								  </tr>
								   <tr>
								    <td width="30%">Empreendimento:</td>
								    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
								  </tr>
								  <tr>
								    <td width="30%">IP:</td>
								    <td><strong><?=@$p['ip']?></strong></td>
								  </tr>				  
								  <tr>
								    <td width="30%">E-mail:</td>
								    <td><strong><?=@$p['email']?></strong></td>
								  </tr>
								  <tr>
								    <td width="30%">Telefone:</td>
								    <td><strong><?=@$p['telefone']?></strong></td>
								  </tr>
								  <tr>
								    <td width="30%" valign="top">Comentários:</td>
								    <td valign="top"><strong><?=@$p['comentarios']?></strong></td>
								  </tr>						  					  					  
								  <tr>
								    <td>&nbsp;</td>
								    <td>&nbsp;</td>
								  </tr>
								  <tr>
							 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
								  </tr>
							</table>
						</body>
					</html>
				<?php
				
				$conteudo = ob_get_contents();
				ob_end_clean();
				//Fim da Mensagem
				
				//====================================================================
				//termina o envio do e-mail
				
				$next = false; 
				foreach($this->model->getEmailGrupos() as $grupo){
					if($grupo->rand == '1'){
						$emails = array();
						foreach($this->model->getEmailGruposEmails($grupo->id) as $email){
							array_push($emails,$email->email);
						}
						$this->model->grupo_interesse($interesse,$grupo->id);
						$next = true;
					} elseif($next){
						if($grupo->id == 8){
							$id = 6;
						} else {
							$id = $grupo->id;
						}
						$this->model->setNextEmailGroup($id);
						$next = false;
					}
				}

				/* Random Mail */
				if(strtolower($p['nome']) == "teste123"){
					$this->email->to('atendimento@divex.com.br');
				} else {
					$this->email->to($emails);
				}
				$this->email->bcc('atendimento@divex.com.br');
				$this->email->from("noreply@myurbanlife.com.br", "Nex Group");
				$this->email->subject('Interesse enviado via HotSite');
				$this->email->message("$conteudo");
				
				$this->email->send();		

				echo 'true';
			
			
			} else {
				echo 'Preencha todos os campos';
			}
		} else {
			echo 'E-mail inválido';
		}
	}
}