<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index(){
		$this->load->helper('source');
		source();
		$this->load->view('home');
	}
	public function notfound(){
		show_404();
	}
	
	public function sendInteresse($sid = 0){
		if($sid == session_id()){
			//$return = $this->input->get('return');
			//$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' 	=> 91,
				'id_estado' 			=> 21,
				'ip' 					=> $this->input->ip_address(),
				'user_agent' 			=> $this->input->user_agent(),
				'url' 					=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' 				=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' 			=> date("Y-m-d H:i:s"),
				'nome' 					=> $this->input->get('nome'),
				'email' 				=> strtolower($this->input->get('email')),
				'telefone' 				=> $this->input->get('telefone'),
				'comentarios' 			=> nl2br(strip_tags($this->input->get('mensagem'))),
				'hotsite' 				=> 'S'
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				$dados = array(
					'Enviado em ' => $data['data_envio'] . ' via HotSite',
					'Enviado por ' => $data['nome'],
					'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					'IP' => $data['ip'],
					'E-mail' => $data['email'],
					'Telefone' => $data['telefone'],
					('Coment�rios') => $data['comentarios']
				);
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));

				/* envia email via roleta */
				$id_empreendimento = 91;
				$grupos = $this->model->getGrupos($id_empreendimento);

				$total = 0;
				$total = count($grupos);
				
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;

						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');

							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);

							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
				/* envia email via roleta */

				$this->email->to($list);

				$this->email->bcc('bruno@divex.com.br');
				$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
				$this->email->subject('Contato enviado via HotSite');

				$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));

				$this->email->send();

				//ENVIA EMAIL DE RETORNO
				/*
				$this->email->clear();
				$this->email->to($this->input->get('email'));
				$this->email->bcc('marketing@reweb.com.br');
				$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
				$this->email->subject('Cadastro - JK Parque Clube - Nex Group');
				$this->email->message('<html>
										<head>
											<title>Nex Group</title>
										</head>
										<body>
											<table cellpadding="0" cellspacing="0" border="0">
												<tr>
													<td valign="top" style="font-size: 0;">
														<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img1.jpg">
													</td>
												</tr>
												<tr>
													<td valign="top" style="font-size: 0;">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img2.jpg">
																</td>
																<td valign="top">
																	<a href="http://youtu.be/erM-xCqno8Q"><img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img3.jpg"></a>
																</td>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img4.jpg">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top" style="font-size: 0;">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img5.jpg">
																</td>
																<td valign="top">
																	<a href="http://www.nexgroup.com.br/"><img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img6.jpg"></a>
																</td>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img7.jpg">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img8.jpg">
													</td>
												</tr>
											</table>
										</body>
										</html>');
				$this->email->send();
				*/

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
				echo 'ok';
			} else {
				echo 'erro';
			}
		} else { echo 'erro'; }
	}
	
	public function sendContato($sid = 0){
		if($sid == session_id()){
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' 	=> 91,
				'id_estado' 			=> 21,
				'ip' 					=> $this->input->ip_address(),
				'user_agent' 			=> $this->input->user_agent(),
				'url' 					=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' 				=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' 			=> date("Y-m-d H:i:s"),
				'nome' 					=> $this->input->post('nome'),
				'email' 				=> strtolower($this->input->post('email')),
				'telefone' 				=> $this->input->post('telefone'),
				'endereco' 				=> $this->input->post('endereco'),
				'cep' 					=> $this->input->post('cep'),
				'estado' 				=> $this->input->post('estado'),
				'cidade' 				=> $this->input->post('cidade'),
				'comentarios' 			=> nl2br(strip_tags($this->input->post('mensagem'))),
				'hotsite' 				=> 'S'
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setContato($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				$dados = array(
					'Enviado em ' => $data['data_envio'] . ' via HotSite',
					'Enviado por ' => $data['nome'],
					'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					'IP' => $data['ip'],
					'E-mail' => $data['email'],
					'Telefone' => $data['telefone'],
					'Endere�o' => $data['endereco'],
					'CEP' => $data['cep'],
					'Cidade' => $data['cidade'],
					'Estado' => $data['estado'],
					('Coment�rios') => $data['comentarios']
				);
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));

				/* envia email via roleta */
				$id_empreendimento = 91;
				$grupos = $this->model->getGrupos($id_empreendimento);

				$total = 0;
				$total = count($grupos);
				
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;

						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');

							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);

							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
				/* envia email via roleta */

				if($p['nome'] == "Teste123"){
					$this->email->to('bruno@divex.com.br');
				} else {
					$this->email->to($list);
				}
				$this->email->bcc('bruno@divex.com.br');
				$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
				$this->email->subject('Contato enviado via HotSite');

				$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
				

				$this->email->send();

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
			}
			redirect($return);
		} else { show_404(); }
	}

	public function sendFriend(){

		$data = array(
			'nome' => $this->input->post('nome'),
			'email' => strtolower($this->input->post('email'))
		);

		if(!empty($data['email']) && !empty($data['nome'])){

			$dados = array(
				'Ol� ' . $data['nome'] => ', conhe�a o <a href="'.$_SERVER['HTTP_REFERER'].'">JK Parque Blube - Nex Group</a>'
			);
			
			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));

			$this->email->to($data['email']);
			$this->email->from("noreply@jkparqueclube.com.br", "Nex Group");
			$this->email->subject('Conhe�a o JK Parque Clube');

			$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));

			$retorno = $this->email->send();
		}
		if($retorno) echo 'ok';
		else echo 'erro';
	}

}