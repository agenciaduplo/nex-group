<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Apartamentos de 2 e 3 dormitórios com suíte - JK PARQUE CLUBE</title>
	<meta name="description" content="Apartamentos de 2 e 3 dormitórios com suíte em Pelotas. JK PARQUE CLUBE - Incorporação e Construção NEX GROUP." />
	<meta name="keywords" content="jk, jkparqueclube, parqueclube, apto, apartamentos, pelotas, 1 dormitório, 2 dormitórios, aptos com suíte, suíte, nex group, nexgroup, nex, group, imóveis, aptos, apartamentos, casas, conjuntos, salas, construtora, incorporadora, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='JK PARQUE CLUBE' />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='Apartamentos de 2 e 3 dormitórios com suíte em Pelotas.'/>
	<meta property='og:url' content='http://www.jkparqueclube.com.br'/>
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/favicon.png">
<!-- CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/colorbox.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/skin.css">
<!-- JS -->
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.jcarousel.min.js"></script>
	<script src="<?=base_url()?>assets/js/function.js"></script>
	<script src="<?=base_url()?>assets/js/colorbox.js"></script>
 	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    
    <!--  SUCESSO -->
	<script>
		$(document).ready(function(){
			$(".colorbox").colorbox({rel:'group1'});
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});
        function closePromo() {
            $('.sucbox').fadeOut('slow');
        }
    </script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-42832022-1', 'jkparqueclube.com.br');
	  ga('send', 'pageview');
	
	</script>
     
</head>
<body id="infraestrutura">
<?php if($this->uri->segment(1) == 'cadastro-enviado-com-sucesso'): ?>
    
    <!-- Google Code for Cadastro Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 983769443;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "GScPCM3chwgQ48KM1QM";
	var google_conversion_value = 1;
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/983769443/?value=1&amp;label=GScPCM3chwgQ48KM1QM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	<?php endif; ?>
	
	<header class="container">
		<div class="lancamento"></div>
		<div class="atendimento">
		  <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=30','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Atendimento on line"><img src="assets/img/btn_atendimento.png" /></a>
		</div>
		<div class="clearfix"></div>
		<ul class="nav left">
			<li><a href="home">Home</a></li>
			<li><a href="empreendimento">Empreendimento</a></li>
			<li class="none"><a href="infraestrutura">Infraestrutura</a></li>
		</ul>
		<ul class="nav right">
			<li class="none"><a href="decorado">Decorados</a></li>
			<li><a href="plantas">Plantas</a></li>
			<li><a href="localizacao">Localização</a></li>
			<!--<li><a href="contato">Contato</a></li>-->
		</ul>
		<div class="clearfix"></div>
	</header>
	<div class="clearfix"></div>
	<div class="container">
		<div class="left ctn-empreendimento">
			<div id="area">
				<h2 class="ttl">Infraestrutura</h2>
				<p class="description-ttl">Toda a estrutura que você precisa para viver com muito mais lazer no seu dia a dia.</p>
				<ul id="infracarousel" class="jcarousel-skin-tango-footer">
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra1.jpg&x=700&y=300" alt="Pórtico" />
						<h3>Pórtico</h3>
						<a href="assets/img/infraestrutura/infra1.jpg" class="colorbox" title="Pórtico"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra2.jpg&x=700&y=300" alt="Praça Central" />
						<h3>Praça Central</h3>
						<a href="assets/img/infraestrutura/infra2.jpg" class="colorbox" title="Praça Central"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra3.jpg&x=700&y=300" alt="Piscinas adulto e infantil aquecidas" />
						<h3>Piscinas adulto e infantil aquecidas</h3>
						<a href="assets/img/infraestrutura/infra3.jpg" title="Piscinas adulto e infantil aquecidas" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra7.jpg&x=700&y=300" alt="Quiosque com Parrilla" />
						<h3>Quiosque com Parrilla</h3>
						<a href="assets/img/infraestrutura/infra7.jpg" title="Quiosque com Parrilla" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra5.jpg&x=700&y=300" alt="Horta" />
						<h3>Horta</h3>
						<a href="assets/img/infraestrutura/infra5.jpg" title="Horta" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra8.jpg&x=700&y=300" alt="Redário" />
						<h3>Redário</h3>
						<a href="assets/img/infraestrutura/infra8.jpg" title="Redário" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra6.jpg&x=700&y=300" alt="Quiosque com churrasqueira e forno de pizza" />
						<h3>Quiosque com churrasqueira e forno de pizza</h3>
						<a href="assets/img/infraestrutura/infra6.jpg" title="QQuiosque com churrasqueira e forno de pizza" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra9.jpg&x=700&y=300" alt="Quadra poliesportiva, fitness ao ar livre e pista de roller" />
						<h3>Quadra poliesportiva, fitness ao ar livre e pista de roller</h3>
						<a href="assets/img/infraestrutura/infra9.jpg" title="Quadra poliesportiva, fitness ao ar livre e pista de roller" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra10.jpg&x=700&y=300" alt="Playground Kids, baby e motocacross" />
						<h3>Playground Kids, baby e motocacross</h3>
						<a href="assets/img/infraestrutura/infra10.jpg" title="Playground Kids, baby e motocacross" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra13.jpg&x=700&y=300" alt="Fitness" />
						<h3>Fitness</h3>
						<a href="assets/img/infraestrutura/infra13.jpg" title="Fitness" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra11.jpg&x=700&y=300" alt="Salão Festas" />
						<h3>Salão Festas</h3>
						<a href="assets/img/infraestrutura/infra11.jpg" title="Salão Festas" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra12.jpg&x=700&y=300" alt="Espaço Goumet" />
						<h3>Espaço Goumet</h3>
						<a href="assets/img/infraestrutura/infra12.jpg" title="Espaço Goumet" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra14.jpg&x=700&y=300" alt="Brinquedoteca" />
						<h3>Brinquedoteca</h3>
						<a href="assets/img/infraestrutura/infra14.jpg" title="Brinquedoteca" class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra15.jpg&x=700&y=300" alt="Foto do living do apto. de 2 dorms." />
						<h3>Foto do living do apto. de 2 dorms.</h3>
						<a href="assets/img/infraestrutura/infra15.jpg" title="Foto do living do apto. de 2 dorms." class="colorbox"></a>
					</li>
					<li>
						<img src="tb2.php?img=assets/img/infraestrutura/infra16.jpg&x=700&y=300" alt="Foto do living do apto. de 2 dorms." />
						<h3>Foto do living do apto. de 2 dorms.</h3>
						<a href="assets/img/infraestrutura/infra16.jpg" title="Foto do living do apto. de 2 dorms." class="colorbox"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="ctn-box">
			<div id="head_form">
				<h2 id="ttl-plantao">Plantão de Vendas</h2>
				<h3 id="ttl-phone"><span>(53)</span> 3025.4657</h3>
			</div>
			<div class="ctn-get-lead right">
				<p class="title">SOLICITE<span>MAIORES INFORMAÇÕES</span></p>
				<form id="FormCotacao">
					<input type="hidden" name="midia" id="midia" value="<?=$midia;?>">
					<input name="nome" id="nome" data-validation-placeholder="Nome*" class="textfield watermark" type="text" value="Nome*">
					<input name="email" id="email" data-validation-placeholder="E-mail*" class="mail textfield watermark" type="text" value="E-mail*">
					<input name="telefone" id="telefone" data-validation-placeholder="Fone*" class="tel textfield watermark" type="text" value="Fone*">
					<textarea id="mensagem" name="mensagem" cols="" rows="" data-validation-placeholder="Mensagem*" class="watermark">Mensagem*</textarea>  
					<input class="button" name="ENVIAR" value="ENVIAR" type="button" onclick="sendCotacao()">
					<span id="legend">*Campos obrigatórios</span>
				</form>
			</div>
		</div>
		<div class="list-two-cols left">
			<h4 class="ttl">A MELHOR INFRAESTRUTURA DE LAZER DA CIDADE</h4>
			<ul>
				<li>Bicicletário</li>
				<li>Brinquedoteca</li>
				<li>Casa de Ferramentas</li>
				<li>Espaço Gourmet</li>
				<li>Estar do Bosque</li>
				<li>Fitness</li>
				<li>Fitness ao Ar Livre</li>
				<li>Horta</li>
			</ul>
			<ul>
				<li>Mesa de Jogos</li>
				<li>Pomar</li>
				<li>Pet Care</li>
				<li>Piscina Adulto Aquecida</li>
				<li>Piscina Infantil Aquecida</li>
				<li>Pista de Roller</li>
				<li>Playgound Kids, Baby e Motocross</li>
				<li>Portaria</li>
			</ul>
			<ul>
				<li>Portaria para Prédio Garagem</li>
				<li>Quadra Poliesportiva</li>
				<li>Quiosques com Churrasqueiras e Forno de Pizza</li>
				<li>Quiosque com Parrilla</li>
				<li>Redário</li>
				<li>Roda de Chimarrão e Estar Fogo</li>
				<li>Salão de Festas</li>
			</ul>
		</div>
		<div class="clearfix"></div>
		<ul class="quick-links">
			<li class="center"><a href="decorado"><img src="assets/img/conheca-o-decorado.png" /></a></li>
			<li><a href="http://www.youtube.com/embed/Qoitfe1ntyw" class="youtube"><img src="assets/img/confira-o-video.png" /></a></li>
		</ul>
	</div>
	<footer>
		<div class="container">
			<ul>
				<li id="indique-ft">
					<h3>Indique para um amigo</h3>
					<form action="" method="post">
						<input type="text" name="nome" id="nomeA" class="watermark" value="Nome *"/>
						<input type="text" name="email" id="emailA" id="email_indique" class="watermark" value="E-mail *"/>
						<input type="button" value="OK" id="enviar" onclick="sendFriend()" />
					</form>
				</li>
				<li id="plantas-de-vendas-ft">
					<a href="localizacao" class="lnk-plantao">plantao</a>
					<h3>Plantão de Vendas</h3>
					<em><span>(53)</span> 3025.4657</em>
					<p>Rua Félix da Cunha, 670 - Centro</p>
					<p>Pelotas</p>
				</li>
				<li id="rights-ft">
					<div>
						<span>Comercialização:</span>
						<span class="copy">Copyright ©2013</span>
					</div>
					<div class="dif">
						<span>Incorporação e construção:</span>
					</div>
					<img src="assets/img/logos_rodape.png" class="imgLogos" />
				</li>
			</ul>
			<div class="clearfix"></div>
			<p class="txt-center">Incorporação e construção: Capa Engenharia, uma empresa Nex Group. Projeto arquitetônico: Arquiteto Franklin Moreira - CAU A7762-3. Projeto paisagístico: Arquiteta Evelise T. Vontobel, CAU A76017-0. <br />O empreendimento só será comercializado após o registro do Memorial de Incorporação no Cartório de Registro de Imóveis, sob os termos da lei nº 4.591/64. Matrícula nº 85.827 - 1º Zona do Registro de Imóveis de Pelotas. Imagens meramente ilustrativas. As plantas apresentadas
são ilustrativas e possui sugestão de decoração. Os acabamentos serão entregues de acordo com o memorial
descritivo.</p>
			<div class="clearfix"></div>
		</div>
	</footer>
</body>
</html>