<?php 
$url = $this->uri->segment(1); source();
?><!DOCTYPE html>
<html lang="pt-BR"> 

<!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
<!--[if IE 8]>	 <html class="ie8" lang="pt-BR"> <![endif]-->  
<!--[if IE 9]>	 <html class="ie9" lang="pt-BR"> <![endif]-->  

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="A Zona Sul é única. E vai ficar ainda mais singular. Síngolo - apartamentos 2 e 3 dormitórios com suíte.">
<meta name="keywords" content="singolo, 2 e 3 dormitórios com suíte, zona sul, 2 dormitorios, 3 dormitorios, suíte, nexgroup">
<title>Apartamentos 2 e 3 dormitórios com suíte - Síngolo - Nex Group</title>

<link rel="shortcut icon" type="image/png" href="<?=base_url(); ?>favicon.png">

<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Lato:300,700,900'>
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/animate-custom.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/carousel.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/validation.css">


<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.validation.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.localscroll.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/bootstrap-transition.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/bootstrap-carousel.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/display.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/init.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/onload.js"></script>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-42832022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script>
	function fechaPopup ()
	{
		$(".fechar").fadeOut();
		$(".float").fadeOut();
	}
</script>
</head>
<body class="<?=isset($class) ? $class : 'home'; ?>">


	<header>
		<div id="headerbg"></div>
		<div class="container_960">
			<nav> 
				<ul class="nav">
					<li class="drop"><a class="home" href="#home">Home</a><a class="legenda" href="#home">Home</a></li>
					<li><a href="http://www.youtube.com/watch?v=zFinYzA-RY8" class="modal2">Video</a></li>
					<li><a class="" href="#infra">infraestrutura</a></li>
					<li><a class="" href="#decorado">Decorado</a></li>
					<li><a class="various5" href="http://www.shots360.com.br/360/nexgroup/singolo.html">360º</a></li>
					<li><a class="" href="#plantas">plantas</a></li>
					<li><a class="" href="#localiza">localização</a></li>
					<li><a class="" href="#contato">Contato</a></li>
					<li class="line"></li>
					<?php $ses_url = @$_SESSION['url']; ?>
					<li class="drop"><a class="corretor" onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&referencia=<?=@$ses_url?>','pop','width=450, height=450, top=100, left=100, scrollbars=no'); _gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);" href="javascript:void(0);">Corretor Online</a><a class="legenda leg_corretor" onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14','pop','width=450, height=450, top=100, left=100, scrollbars=no'); _gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);" href="javascript:void(0);">Corretor Online</a></li>
					
					<li class="drop"><a class="plantao" href="javascript:void(0);" onclick="javascript:plantaoForm();">Plantão de Vendas</a><a class="legenda leg_plantao" href="javascript:void(0);" onclick="javascript:plantaoForm();">Plantão de Vendas</a></li>
					<li class="drop"><a style="display:none" class="indique" href="javascript:void(0);" onclick="javascript:indiqueForm();">Indique a um Amigo</a><a class="legenda leg_indique" href="javascript:void(0);" onclick="javascript:indiqueForm();">Indique a um Amigo</a></li>
					<li class="drop"><a class="interesse" href="#contato">Tenho Interesse</a><a class="legenda leg_interesse" href="#contato">Tenho Interesse</a></li>
				</ul>				
			</nav>
		</div>	
	</header>
    
    <!-- CORRETOR ONLINE FLUTUANTE -->
	<a href="javascript: fechaPopup();" class="fechar fadeInRight animated hinge"><p>X</p></a>
   	<div class="float fadeInRight animated hinge2" onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&referencia=<?=@$ses_url?>','pop','width=450, height=450, top=100, left=100, scrollbars=no'); _gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);" href="javascript:void(0);">
    	<p class="pa">Quer mais infomarções?</p>
    	<p class="pb">Clique aqui e converse com o</p>
    	<p class="pc">corretor online.</p>
    </div>
    <!-- CORRETOR ONLINE FLUTUANTE -->
