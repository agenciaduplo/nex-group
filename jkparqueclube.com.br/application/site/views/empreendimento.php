<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Apartamentos de 2 e 3 dormitórios com suíte - JK PARQUE CLUBE</title>
	<meta name="description" content="Apartamentos de 2 e 3 dormitórios com suíte em Pelotas. JK PARQUE CLUBE - Incorporação e Construção NEX GROUP." />
	<meta name="keywords" content="jk, jkparqueclube, parqueclube, apto, apartamentos, pelotas, 1 dormitório, 2 dormitórios, aptos com suíte, suíte, nex group, nexgroup, nex, group, imóveis, aptos, apartamentos, casas, conjuntos, salas, construtora, incorporadora, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='JK PARQUE CLUBE' />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='Apartamentos de 2 e 3 dormitórios com suíte em Pelotas.'/>
	<meta property='og:url' content='http://www.jkparqueclube.com.br'/>
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/favicon.png">
<!-- CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/colorbox.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">
<!-- JS -->
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>
	<script src="<?=base_url()?>assets/js/function.js"></script>
	<script src="<?=base_url()?>assets/js/colorbox.js"></script>
 	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    
    <!--  SUCESSO -->
	<script>
		$(document).ready(function(){
			$(".colorbox").colorbox({rel:'group1'});
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});

        function closePromo() {
            $('.sucbox').fadeOut('slow');
        }
    </script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-42832022-1', 'jkparqueclube.com.br');
	  ga('send', 'pageview');
	
	</script>
     
</head>
<body id="empreendimento">
<?php if($this->uri->segment(1) == 'cadastro-enviado-com-sucesso'): ?>
    
    <!-- Google Code for Cadastro Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 983769443;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "GScPCM3chwgQ48KM1QM";
	var google_conversion_value = 1;
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/983769443/?value=1&amp;label=GScPCM3chwgQ48KM1QM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	<?php endif; ?>
	
	<header class="container">
		<div class="lancamento"></div>
		<div class="atendimento">
		  <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=30','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Atendimento on line"><img src="assets/img/btn_atendimento.png" /></a>
		</div>
		<div class="clearfix"></div>
		<ul class="nav left">
			<li><a href="home">Home</a></li>
			<li><a href="empreendimento">Empreendimento</a></li>
			<li class="none"><a href="infraestrutura">Infraestrutura</a></li>
		</ul>
		<ul class="nav right">
			<li class="none"><a href="decorado">Decorados</a></li>
			<li><a href="plantas">Plantas</a></li>
			<li><a href="localizacao">Localização</a></li>
			<!--<li><a href="contato">Contato</a></li>-->
		</ul>
		<div class="clearfix"></div>
	</header>
	<div class="clearfix"></div>
	<div class="container">
		<div class="left ctn-empreendimento">
				<h2 class="ttl">Empreendimento</h2>
				<p class="description-ttl">JK Parque Clube é um empreendimento de proporções grandiosas. Um projeto inédito em Pelotas que conta com a maior infraestrutura de lazer da cidade, trazendo a diversão de um clube para o seu quintal e a tranquilidade de um parque para o seu jardim. Viver no JK é colecionar bons momentos de lazer todos os dias, sem sair de casa.
É contar com a segurança que a sua família precisa com a qualidade de vida que vocês merecem.</p>
				<!--
				<div class="imgs">
					<img src="assets/img/implantacao.png" alt="Implantação" id="imgIMP" />
					<a href="assets/img/implantacao.png" class="colorbox">lupa</a>
				</div>
				<h3>IMPLANTAÇÃO</h3>
			-->
			<img src="assets/img/implantacao/imp.png" alt="Implantação" id="imgIMP" style="margin-bottom: 20px;" />
			<script type="text/javascript">
				$(document).ready(function(){
					$('.imp').mouseover(function(){
						var rel = $(this).attr('rel');
						$('#imgIMP').attr('src','assets/img/implantacao/imp'+rel+'.png');
					});
					$('.imp').mouseout(function(){
						$('#imgIMP').attr('src','assets/img/implantacao/imp.png');
					});
				});
			</script>
			<div id="ctn-legenda">
				<h4>Legenda</h4>
				<ul>
					<li class="imp" rel="1"><span>1</span><div class="leg">Portaria</div></li>
					<li class="imp" rel="2"><span>2</span><div class="leg">Salão de Festas</div></li>
					<li class="imp" rel="3"><span>3</span><div class="leg">Fitness</div></li>
					<li class="imp" rel="4"><span>4</span><div class="leg">Pet Care</div></li>
					<li class="imp" rel="5"><span>5</span><div class="leg">Estar do Bosque</div></li>
					<li class="imp" rel="6"><span>6</span><div class="leg">Brinquedoteca</li>
					<li class="imp" rel="7"><span>7</span><div class="leg">Piscina Infantil Aquecida</div></li>
					<li class="imp" rel="8"><span>8</span><div class="leg">Piscina Adulto Aquecida</li>
					<li class="imp" rel="9"><span>9</span><div class="leg">Roda de Chimarrão e Estar Fogo</div></li>
					<li class="imp" rel="10"><span>10</span><div class="leg">Redário</li>
					<li class="imp" rel="11"><span>11</span><div class="leg">Playground Kids, Baby e Motocross</div></li>
					<li class="imp" rel="12"><span>12</span><div class="leg">Pomar</div></li>
				</ul>
				<ul>
					<li class="imp" rel="13"><span>13</span><div class="leg">Fitness ao Ar Livre</div></li>
					<li class="imp" rel="14"><span>14</span><div class="leg">Quiosque com Churrasqueira e Forno de Pizza</div></li>
					<li class="imp" rel="15"><span>15</span><div class="leg">Quiosque com Churrasqueira</div></li>
					<li class="imp" rel="16"><span>16</span><div class="leg">Quiosques com Parrilla</div></li>
					<li class="imp" rel="17"><span>17</span><div class="leg">Horta</div></li>
					<li class="imp" rel="18"><span>18</span><div class="leg">Casa de Ferramentas</div></li>
					<li class="imp" rel="19"><span>19</span><div class="leg">Pista de Roller</div></li>
					<li class="imp" rel="20"><span>20</span><div class="leg">Quadra Poliesportiva</div></li>
					<li class="imp" rel="21"><span>21</span><div class="leg">Mesa de Jogos</div></li>
					<li class="imp" rel="22"><span>22</span><div class="leg">Guarita para Prédio Garagem</div></li>
					<li class="imp" rel="23"><span>23</span><div class="leg">Espaço Gourmet</div></li>
					<li class="imp" rel="24"><span>24</span><div class="leg">Bicicletário</div></li>
				</ul>
				<br class="clear" />
				<div class="prg-emp">A | Torre Jacarandá</div>
				<div class="prg-emp">B | Torre Ipê</div>
				<div class="prg-emp">C | Prédio Garagem Coberta</div>
			</div>
			<!--
			<div id="ctn-legenda">
				<h4>Legenda</h4>
				<ul>
					<li class="imp" rel="1"><span>1</span>Portaria</li>
					<li class="imp" rel="2"><span>2</span>Salão de Festas</li>
					<li class="imp" rel="3"><span>3</span>Fitness</li>
					<li class="imp" rel="4"><span>4</span>Pet Care</li>
					<li class="imp" rel="5"><span>5</span>Estar do Bosque</li>
					<li class="imp" rel="6"><span>6</span>Brinquedoteca</li>
				</ul>
				<ul>
					<li class="imp" rel="7"><span>7</span>Piscina Infantil Aquecida</li>
					<li class="imp" rel="8"><span>8</span>Piscina Adulto Aquecida</li>
					<li class="imp" rel="9"><span>9</span>Roda de Chimarrão e Estar Fogo</li>
					<li class="imp" rel="10"><span>10</span>Redário</li>
					<li class="imp" rel="11"><span>11</span>Playground Kids, Baby e Motocross</li>
					<li class="imp" rel="12"><span>12</span>Pomar</li>
				</ul>
				<ul>
					<li class="imp" rel="13"><span>13</span>Fitness ao Ar Livre</li>
					<li class="imp" rel="14"><span>14</span>Quiosque com Churrasqueira e Forno de Pizza</li>
					<li class="imp" rel="15"><span>15</span>Quiosque com Churrasqueira</li>
					<li class="imp" rel="16"><span>16</span>Quiosques com Parrilla</li>
					<li class="imp" rel="17"><span>17</span>Horta</li>
					<li class="imp" rel="18"><span>18</span>Casa de Ferramentas</li>
				</ul>
				<ul>
					<li class="imp" rel="19"><span>19</span>Posta de Holler</li>
					<li class="imp" rel="20"><span>20</span>Quadra Poliesportiva</li>
					<li class="imp" rel="21"><span>21</span>Mesa de Jogos</li>
					<li class="imp" rel="22"><span>22</span>Guarita para Prédio Garagem</li>
					<li class="imp" rel="23"><span>23</span>Espaço Gourmet</li>
					<li class="imp" rel="24"><span>24</span>Bicicletário</li>
				</ul>
			</div>
		-->
		</div>
		<div class="ctn-box">
			<div id="head_form">
				<h2 id="ttl-plantao">Plantão de Vendas</h2>
				<h3 id="ttl-phone"><span>(53)</span> 3025.4657</h3>
			</div>
			<div class="ctn-get-lead right">
				<p class="title">SOLICITE<span>MAIORES INFORMAÇÕES</span></p>
				<form id="FormCotacao">
					<input type="hidden" name="midia" id="midia" value="<?=$midia;?>">
					<input name="nome" id="nome" data-validation-placeholder="Nome*" class="textfield watermark" type="text" value="Nome*">
					<input name="email" id="email" data-validation-placeholder="E-mail*" class="mail textfield watermark" type="text" value="E-mail*">
					<input name="telefone" id="telefone" data-validation-placeholder="Fone*" class="tel textfield watermark" type="text" value="Fone*">
					<textarea id="mensagem" name="mensagem" cols="" rows="" data-validation-placeholder="Mensagem*" class="watermark">Mensagem*</textarea>  
					<input class="button" name="ENVIAR" value="ENVIAR" type="button" onclick="sendCotacao()">
					<span id="legend">*Campos obrigatórios</span>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
		<ul class="quick-links">
			<li><a href="decorado"><img src="assets/img/conheca-o-decorado.png" /></a></li>
			<li class="center"><a href="infraestrutura"><img src="assets/img/conheca-a-infraestrutura.png" /></a></li>
			<li><a href="http://www.youtube.com/embed/Qoitfe1ntyw" class="youtube"><img src="assets/img/confira-o-video.png" /></a></li>
		</ul>
	</div>
	<footer style="margin-top: 20px; padding-top: 15px;">
		<div class="container">
			<ul>
				<li id="indique-ft">
					<h3>Indique para um amigo</h3>
					<form action="" method="post">
						<input type="text" name="nome" id="nomeA" class="watermark" value="Nome *"/>
						<input type="text" name="email" id="emailA" id="email_indique" class="watermark" value="E-mail *"/>
						<input type="button" value="OK" id="enviar" onclick="sendFriend()" />
					</form>
				</li>
				<li id="plantas-de-vendas-ft">
					<a href="localizacao" class="lnk-plantao">plantao</a>
					<h3>Plantão de Vendas</h3>
					<em><span>(53)</span> 3025.4657</em>
					<p>Rua Félix da Cunha, 670 - Centro</p>
					<p>Pelotas</p>
				</li>
				<li id="rights-ft">
					<div>
						<span>Comercialização:</span>
						<span class="copy">Copyright ©2013</span>
					</div>
					<div class="dif">
						<span>Incorporação e construção:</span>
					</div>
					<img src="assets/img/logos_rodape.png" class="imgLogos" />
				</li>
			</ul>
			<div class="clearfix"></div>
			<p class="txt-center">Incorporação e construção: Capa Engenharia, uma empresa Nex Group. Projeto arquitetônico: Arquiteto Franklin Moreira - CAU A7762-3. Projeto paisagístico: Arquiteta Evelise T. Vontobel, CAU A76017-0. <br />O empreendimento só será comercializado após o registro do Memorial de Incorporação no Cartório de Registro de Imóveis, sob os termos da lei nº 4.591/64. Matrícula nº 85.827 - 1º Zona do Registro de Imóveis de Pelotas. Imagens meramente ilustrativas. As plantas apresentadas
são ilustrativas e possui sugestão de decoração. Os acabamentos serão entregues de acordo com o memorial
descritivo.</p>
			<div class="clearfix"></div>
		</div>
	</footer>
</body>
</html>