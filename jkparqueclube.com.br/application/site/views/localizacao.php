<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Apartamentos de 2 e 3 dormitórios com suíte - JK PARQUE CLUBE</title>
	<meta name="description" content="Apartamentos de 2 e 3 dormitórios com suíte em Pelotas. JK PARQUE CLUBE - Incorporação e Construção NEX GROUP." />
	<meta name="keywords" content="jk, jkparqueclube, parqueclube, apto, apartamentos, pelotas, 1 dormitório, 2 dormitórios, aptos com suíte, suíte, nex group, nexgroup, nex, group, imóveis, aptos, apartamentos, casas, conjuntos, salas, construtora, incorporadora, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='JK PARQUE CLUBE' />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='Apartamentos de 2 e 3 dormitórios com suíte em Pelotas.'/>
	<meta property='og:url' content='http://www.jkparqueclube.com.br'/>
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/favicon.png">
<!-- CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/colorbox.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">
<!-- JS -->
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>
	<script src="<?=base_url()?>assets/js/function.js"></script>
	<script src="<?=base_url()?>assets/js/colorbox.js"></script>
 	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <!--  SUCESSO -->
	<script>
		$(document).ready(function(){
			$(".colorbox").colorbox({rel:'group1'});
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});
        function closePromo() {
            $('.sucbox').fadeOut('slow');
        }
    </script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-42832022-1', 'jkparqueclube.com.br');
	  ga('send', 'pageview');
	</script>
	<script>
		function initialize() {
		  //var myLatlng = new google.maps.LatLng(-30.019388339283722,-51.11537218093872);
		  //var myLatlng2 = new google.maps.LatLng(-30.01812494172129,-51.20225429534912);
		  var myLatlng = new google.maps.LatLng(-31.74072290104411, -52.328782081604004);
		  var myLatlng2 = new google.maps.LatLng(-31.768200924570646, -52.3394250869751);
		  var mapOptions = {
		    zoom: 12,
		    center: myLatlng,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		  }
		  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		  var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map,
		      title: 'Empreendimento'
		  });
		  var marker = new google.maps.Marker({
		      position: myLatlng2,
		      map: map,
		      title: 'Plantão de Vendas'
		  });
		}
	</script>

	<style>
      #map-canvas {
      	float: left;
      	width: 650px;
        height: 450px;
        margin: 20px;
        padding: 0px
      }
    </style>
     
</head>
<body id="localizacao">
<?php if($this->uri->segment(1) == 'cadastro-enviado-com-sucesso'): ?>
    
    <!-- Google Code for Cadastro Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 983769443;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "GScPCM3chwgQ48KM1QM";
	var google_conversion_value = 1;
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/983769443/?value=1&amp;label=GScPCM3chwgQ48KM1QM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	<?php endif; ?>
	
	<header class="container">
		<div class="lancamento"></div>
		<div class="atendimento">
		  <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=30','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Atendimento on line"><img src="assets/img/btn_atendimento.png" /></a>
		</div>
		<div class="clearfix"></div>
		<ul class="nav left">
			<li><a href="home">Home</a></li>
			<li><a href="empreendimento">Empreendimento</a></li>
			<li class="none"><a href="infraestrutura">Infraestrutura</a></li>
		</ul>
		<ul class="nav right">
			<li class="none"><a href="decorado">Decorados</a></li>
			<li><a href="plantas">Plantas</a></li>
			<li><a href="localizacao">Localização</a></li>
			<!--<li><a href="contato">Contato</a></li>-->
		</ul>
		<div class="clearfix"></div>
	</header>
	<div class="clearfix"></div>
	<div class="container">
		<h2 class="ttl left">Localização</h2>
		<p class="description-ttl left prg-loca">O JK Parque Clube está localizado na Av. Juscelino Kubitschek de Oliveira, no melhor ponto da Zona Norte: próximo à avenida Dom Joaquim e à importantes vias de acesso para todos os pontos da cidade. E o melhor: está a poucos minutos do Shopping Pelotas, uma novidade que vai valorizar ainda mais a região e trazer muito mais facilidades e conveniência para todo o entorno.</p>
		<div class="ctn-home">
			<ul class="lst-loc">
				<li><a href="javascript:;" id="loc1">FOTO AÉREA</a></li>
				<li><a href="javascript:;" id="loc2">GOOGLE MAPS</a></li>
				<li class="last"><a href="javascript:;" id="loc3">MAPA</a></li>
			</ul>
			<div class="ctloca" id="dvloc1">
				<img src="assets/img/mapa_localizacao.png" class="left"/>
				<a href="assets/img/mapa_localizacao2.png" class="colorbox" id="lupa4">lupa</a>
			</div>
			<div class="ctloca" id="dvloc2">
				<div id="map-canvas"></div>
			</div>
			<div class="ctloca" id="dvloc3">
				<img src="assets/img/mapa-jk.jpg" class="left"/>
				<a href="assets/img/ampliado.jpg" class="colorbox" id="lupa3" style="display: none;">lupa</a>
			</div>
		</div>
		<script type="text/javascript">
			 jQuery(document).ready(function(){
			 	google.maps.event.addDomListener(window, 'load', initialize);
				

			 	$('#loc1').click(function(){
			 		$('#dvloc3').css('display','none');
			 		$('#dvloc2').css('display','none');
			 		$('#lupa3').css('display','none');
			 		$('#dvloc1').css('display','block');
			 		$('#lupa4').css('display','block');
			 	});
			 	$('#loc2').click(function(){
			 		$('#dvloc1').css('display','none');
			 		$('#dvloc3').css('display','none');
			 		$('#lupa3').css('display','none');
			 		$('#dvloc2').css('display','block');
			 		$('#lupa4').css('display','none');
			 	});
			 	$('#loc3').click(function(){
			 		$('#dvloc2').css('display','none');
			 		$('#dvloc1').css('display','none');
			 		$('#dvloc3').css('display','block');
			 		$('#lupa3').css('display','block');
			 		$('#lupa4').css('display','none');
			 	});
			 });
		</script>
		<div class="ctn-box">
			<div id="head_form">
				<h2 id="ttl-plantao">Plantão de Vendas</h2>
				<h3 id="ttl-phone"><span>(53)</span> 3025.4657</h3>
			</div>
			<div class="ctn-get-lead right">
				<p class="title">SOLICITE<span>MAIORES INFORMAÇÕES</span></p>
				<form id="FormCotacao">
					<input type="hidden" name="midia" id="midia" value="<?=$midia;?>">
					<input name="nome" id="nome" data-validation-placeholder="Nome*" class="textfield watermark" type="text" value="Nome*">
					<input name="email" id="email" data-validation-placeholder="E-mail*" class="mail textfield watermark" type="text" value="E-mail*">
					<input name="telefone" id="telefone" data-validation-placeholder="Fone*" class="tel textfield watermark" type="text" value="Fone*">
					<textarea id="mensagem" name="mensagem" cols="" rows="" data-validation-placeholder="Mensagem*" class="watermark">Mensagem*</textarea>  
					<input class="button" name="ENVIAR" value="ENVIAR" type="button" onclick="sendCotacao()">
					<span id="legend">*Campos obrigatórios</span>
				</form>
			</div>
		</div>
		<div class="right endereco">
			<h3 class="title">ENDEREÇO DO EMPREENDIMENTO</h3>
			<p>Av. Presidente Juscelino <br/>Kubitschek, n° 4.808</p>
			<p>Pelotas - RS - Brasil</p><br/>
			<h3 class="title">CENTRAL DE VENDAS</h3>
			<p>Rua Félix da Cunha, 670</p>
			<p>Pelotas</p>
		</div>
		<div class="clearfix"></div>

		<ul class="quick-links">
			<li><a href="decorado"><img src="assets/img/conheca-o-decorado.png" /></a></li>
			<li class="center"><a href="infraestrutura"><img src="assets/img/conheca-a-infraestrutura.png" /></a></li>
			<li><a href="http://www.youtube.com/embed/Qoitfe1ntyw" class="youtube"><img src="assets/img/confira-o-video.png" /></a></li>
		</ul>
	</div>
	<footer>
		<div class="container">
			<ul>
				<li id="indique-ft">
					<h3>Indique para um amigo</h3>
					<form action="" method="post">
						<input type="text" name="nome" id="nomeA" class="watermark" value="Nome *"/>
						<input type="text" name="email" id="emailA" id="email_indique" class="watermark" value="E-mail *"/>
						<input type="button" value="OK" id="enviar" onclick="sendFriend()" />
					</form>
				</li>
				<li id="plantas-de-vendas-ft">
					<a href="localizacao" class="lnk-plantao">plantao</a>
					<h3>Plantão de Vendas</h3>
					<em><span>(53)</span> 3025.4657</em>
					<p>Rua Félix da Cunha, 670 - Centro</p>
					<p>Pelotas</p>
				</li>
				<li id="rights-ft">
					<div>
						<span>Comercialização:</span>
						<span class="copy">Copyright ©2013</span>
					</div>
					<div class="dif">
						<span>Incorporação e construção:</span>
					</div>
					<img src="assets/img/logos_rodape.png" class="imgLogos" />
				</li>
			</ul>
			<div class="clearfix"></div>
			<p class="txt-center">Incorporação e construção: Capa Engenharia, uma empresa Nex Group. Projeto arquitetônico: Arquiteto Franklin Moreira - CAU A7762-3. Projeto paisagístico: Arquiteta Evelise T. Vontobel, CAU A76017-0. <br />O empreendimento só será comercializado após o registro do Memorial de Incorporação no Cartório de Registro de Imóveis, sob os termos da lei nº 4.591/64. Matrícula nº 85.827 - 1º Zona do Registro de Imóveis de Pelotas. Imagens meramente ilustrativas. As plantas apresentadas
são ilustrativas e possui sugestão de decoração. Os acabamentos serão entregues de acordo com o memorial
descritivo.</p>
			<div class="clearfix"></div>
		</div>
	</footer>
</body>
</html>