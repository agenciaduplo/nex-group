<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "home";
$route['404_override'] = 'home/notfound';
$route['sucesso'] = 'home/index';
$route['cadastro-enviado-com-sucesso'] = 'home/index';