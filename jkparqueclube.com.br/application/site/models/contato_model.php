<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	function setInteresse($data){
		$this->db->flush_cache();	
		$this->db->insert('interesses', $data); 
		return $this->db->insert_id();
	}
	function setContato($data){
		$this->db->flush_cache();	
		$this->db->insert('contatos_jk', $data); 
		return $this->db->insert_id();
	}
	function setIndique($data){
		$this->db->flush_cache();	
		$this->db->insert('indique', $data); 
		return $this->db->insert_id();
	}
	function getEnviaEmail($id_empreendimento, $id_tipo_form){
		$this->db->select('*')->from('emails');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_tipo_form',1);
		
		$query = $this->db->get();
		return $query->result();
	}
	function getEmpreendimento($id_empreendimento){
		$this->db->select('*')->from('empreendimentos');
		$this->db->join('cidades', 'cidades.id_cidade = empreendimentos.id_cidade');
		$this->db->where('id_empreendimento',$id_empreendimento);
	
		$query = $this->db->get();
		return $query->row();
	}
	
	function getEmailGrupos(){
		$sql = "SELECT grupos.id, grupos.rand, grupos.ordem FROM grupos WHERE grupos.id_empreendimento = 82 ORDER BY grupos.ordem ASC";
		return $this->db->query($sql)->result();
	}
	function getEmailGruposCount(){
		$sql = "SELECT grupos.id FROM grupos WHERE grupos.id_empreendimento = 82";
		return $this->db->query($sql)->num_rows();
	}
	function getEmailGruposEmails($id){
		$sql = "SELECT grupos_emails.nome, grupos_emails.email FROM grupos_emails WHERE grupos_emails.grupo = '{$id}' ORDER BY grupos_emails.nome ASC";
		return $this->db->query($sql)->result();
	}
	function grupo_interesse($interesse, $grupo, $emails_txt){
		$this->db->insert('grupos_interesses',array(
			'id_interesse' => $interesse,
			'id_grupo' => $grupo,
			'emails' => $emails_txt
		));
	}
	function setNextEmailGroup($id){
		$sql = "UPDATE `grupos` SET `rand`='0' WHERE (`id_empreendimento`='82')";
		$this->db->query($sql);
		$set = "UPDATE `grupos` SET `rand`='1' WHERE (`id`='{$id}') LIMIT 1";
		$this->db->query($set);
	}


	/* envia email via roleta */
	function getGrupos ($id_empreendimento) 
	{
		$sql = "SELECT *
				FROM grupos
				WHERE id_empreendimento = $id_empreendimento
		";
		return $this->db->query($sql)->result();
	}

	function updateRand ($id_empreendimento, $ordem)
	{
		$sql = "UPDATE `grupos` SET `rand`='0' WHERE (`id_empreendimento`='$id_empreendimento')";
		$this->db->query($sql);
		$set = "UPDATE `grupos` SET `rand`='1' WHERE `ordem`='$ordem' AND `id_empreendimento`='$id_empreendimento' LIMIT 1";
		$this->db->query($set);
	}

	function getGruposEmails ($id_empreendimento, $ordem)
	{
		$sql = "SELECT e.email, e.grupo
				FROM grupos_emails e, grupos g
				WHERE g.id_empreendimento = $id_empreendimento
				AND g.ordem = '$ordem'
				AND g.id = e.grupo
		";
		return $this->db->query($sql)->result();
	}

	/* envia email via roleta */
}