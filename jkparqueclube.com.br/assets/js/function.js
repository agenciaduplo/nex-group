 jQuery(document).ready(function(){

	
	jQuery("#FormContato").validationEngine(
		'attach', {
		    promptPosition : "topLeft",
		    onValidationComplete: function(form, status)
		    {
		        if(status == true)
		        {
		            var nome            = $("#nome").val();
		            var email           = $("#email").val();
		            var telefone        = $("#telefone").val();
		            var cep             = $("#cep").val();
		            var endereco        = $("#endereco").val();
		            var estado          = $("#estado").val();
		            var cidade          = $("#cidade").val();
					var mensagem        = $("#mensagem").val();
		            
		                
		            var msg     = '';
		            vet_dados   = 'nome='+ nome
		                          +'&email='+ email
		                          +'&telefone='+ telefone
		                          +'&cep='+ cep                                        
		                          +'&endereco='+ endereco                                        
		                          +'&estado='+ estado                                        
		                          +'&cidade='+ cidade                                        
		                          +'&mensagem='+ mensagem                                        
		                          ;
		                          
		            base_url    = "http://www.jkparqueclube.com.br/index.php/home/sendContato";
		            
		            $.ajax({
		                type: "POST",
		                url: base_url,
		                data: vet_dados,
		                success: function(msg) {
		                        limpaCampos('#FormContato');
		                        $(".sucbox").fadeIn();
		                        }
		            });
		            return false;
		        }
		        else
		        {
		            return false;
		        }
		    } 
		}
	);
	
	/*
	jQuery("#FormCotacao").validationEngine(
		'attach', {
		    promptPosition : "topRight",
		    onValidationComplete: function(form, status)
		    {
		        if(status == true)
		        {
		            var nome            = $("#nome").val();
		            var email           = $("#email").val();
		            var telefone        = $("#telefone").val();
		            var mensagem        = $("#mensagem").val();
		                
		            var msg     = '';
		            vet_dados   = 'nome='+ nome
		                          +'&email='+ email
		                          +'&telefone='+ telefone
		                          +'&mensagem='+ mensagem                                        
		                          ;
		                          
		            base_url    = "http://www.jkparqueclube.com.br/index.php/home/sendInteresse";
		            
		            $.ajax({
		                type: "POST",
		                url: base_url,
		                data: vet_dados,
		                success: function(msg) {
		                        limpaCampos('#FormCotacao');
		                        $(".sucbox").fadeIn();
		                        }
		            });
		            return false;
		        }
		        else
		        {
		            return false;
		        }
		    } 
		}
	);
 */
	
	var bodyID = jQuery("body").attr("id");

	jQuery('input:text').setMask();
	watermark.init();
	
	if(bodyID == 'infraestrutura'){
		$('#infracarousel').jcarousel({
			scroll: 1,
			auto: 3,
			wrap: 'circular',
			initCallback: mycarousel_initCallback
		});
	}
	
	if(bodyID == 'plantas'){
		$('#plantascarousel').jcarousel({
			scroll: 1,
			auto: 3,
			wrap: 'circular',
			initCallback: mycarousel_initCallback
		});
	}
	
	if($('#infracarousel2').size()){
		$('#infracarousel2').jcarousel({
			scroll: 1,
			auto: 3,
			wrap: 'circular',
			initCallback: mycarousel_initCallback
		});
	}
	
	if($('#infracarousel3').size()){
		$('#infracarousel3').jcarousel({
			scroll: 1,
			auto: 3,
			wrap: 'circular',
			initCallback: mycarousel_initCallback
		});
	}
});

 var valEmail = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;

function sendFriend() {
    msg = "";

	if($("#nomeA").val() == "" || $("#nomeA").val() == "Nome *"){
        msg += "* Nome não preenchido. \n";
    }

    if($("#emailA").val() == "" || $("#emailA").val() == "E-mail *"){
        msg += "* Email não preenchido. \n";
    } else if(!valEmail.test($("#emailA").val())){
        msg += "* E-mail inválido. \n";
    }
	
	if(msg != ""){
        alert("Os seguintes campos contém erros:\n\n"+msg);
    } else {
		$.ajax({
            type: 	"POST",
            url: 	'home/sendFriend',
            data: 	"email="+$("#emailA").val()+"&nome="+$("#nomeA").val(),
            success: function(msg){
                if(msg == 'ok') {
					alert('Cadastro realizado com sucesso.');
					$('#nomeA').val('Nome *');
					$('#emailA').val('E-mail *');
				}
            }
        });
		
    }
}

function sendCotacao() {
    msg = "";

	if($("#nome").val() == "" || $("#nome").val() == "Nome*"){
        msg += "* Nome não preenchido. \n";
    }

    if($("#email").val() == "" || $("#email").val() == "E-mail*"){
        msg += "* Email não preenchido. \n";
    } else if(!valEmail.test($("#email").val())){
        msg += "* E-mail inválido. \n";
    }

    if($("#telefone").val() == "" || $("#telefone").val() == "Fone*"){
        msg += "* Telefone não preenchido. \n";
    }

    if($("#mensagem").val() == "" || $("#mensagem").val() == "Mensagem*"){
        msg += "* Mensagem não preenchido. \n";
    }

    var nome            = $("#nome").val();
    var email           = $("#email").val();
    var telefone        = $("#telefone").val();
    var mensagem        = $("#mensagem").val();

    vet_dados   = 'nome='+ nome
                  +'&email='+ email
                  +'&telefone='+ telefone
                  +'&mensagem='+ mensagem                                        
                  ;
	
	if(msg != ""){
        alert("Os seguintes campos contém erros:\n\n"+msg);
    } else {
		$.ajax({
            type: 	"GET",
            url: 	'home/sendInteresse',
            data: vet_dados,
            success: function(msg){
                if(msg == 'ok') {
					alert('Cadastro realizado com sucesso.');
					window.location.replace("http://www.jkparqueclube.com.br/cadastro-enviado-com-sucesso");
					$('#nome').val('Nome*');
					$('#email').val('E-mail*');
					$('#telefone').val('Telefone*');
					$('#mensagem').val('Mensagem*');
				}
				if(msg == 'erro') {
					alert('Erro ao cadastro!');
					$('#nome').val('Nome*');
					$('#email').val('E-mail*');
					$('#telefone').val('Telefone*');
					$('#mensagem').val('Mensagem*');
				}
            }
        });
    }
}

function mycarousel_initCallback(carousel){
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
}

function limpaCampos (form)
{
    $(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'select':
            case 'email':
                $(this).val('');
            case 'tel':
                $(this).val('');
            case 'text':
                $(this).val('');
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                $('#ipt-contact-phone').checked = true;
                $('#ipt-cliente-nao').checked = true;
        }
    });
}

/**
 * --------------------------------------------------------------------
 * jQuery-Plugin "watermark"
 * Version: 1.1, 11.09.2007
 * by bits
 *
 * Copyright (c) 2009
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 */
watermark={
	init:function(){
			$('input.watermark[type="text"], input.watermark[type="password"], textarea.watermark').focus(
				function(){
					watermark.focus($(this))
			}).blur(
				function(){
					watermark.blur($(this))
			}).each(function(){
				if($(this).attr("type") == 'password') {
					$(this).addClass('watermarkPass');
				} else
					$(this).attr("title",$(this).val()
			)}
	)}
	,focus:function(a){
		val=$(a).val();
		if($(a).val()==$(a).attr("title")) {
			$(a).val("");
			$(a).attr("alt",val);
			if($(a).attr("type") == 'password')
				$(a).removeClass('watermarkPass');
		} 
	}
	,blur:function(a){
		val=$(a).attr("alt");
		if($(a).val()=="") {
			$(a).val(val);
			$(a).attr("alt","");
			if($(a).attr("type") == 'password')
				$(a).addClass('watermarkPass');
		}
	}
};

/**
 * Função máscara javascript para inputs
 *
 * http://www.meiocodigo.com/projects/meiomask/
 *
 */
(function(B){var A=(window.orientation!=undefined);B.extend({mask:{rules:{"z":/[a-z]/,"Z":/[A-Z]/,"a":/[a-zA-Z]/,"*":/[0-9a-zA-Z]/,"@":/[0-9a-zA-ZÃƒÆ’Ã‚Â§ÃƒÆ’Ã¢â‚¬Â¡ÃƒÆ’Ã‚Â¡ÃƒÆ’ ÃƒÆ’Ã‚Â£ÃƒÆ’Ã‚Â©ÃƒÆ’Ã‚Â¨ÃƒÆ’Ã‚Â­ÃƒÆ’Ã‚Â¬ÃƒÆ’Ã‚Â³ÃƒÆ’Ã‚Â²ÃƒÆ’Ã‚ÂµÃƒÆ’Ã‚ÂºÃƒÆ’Ã‚Â¹ÃƒÆ’Ã‚Â¼]/},fixedChars:"[(),.:/ -]",keyRepresentation:{8:"backspace",9:"tab",13:"enter",27:"esc",37:"left",38:"up",39:"right",40:"down",46:"delete"},ignoreKeys:[8,9,13,16,17,18,27,33,34,35,36,37,38,39,40,45,46,91,116],iphoneIgnoreKeys:[10,127],signals:["+","-"],options:{attr:"mask",mask:null,type:"fixed",defaultValue:"",signal:false,onInvalid:function(){},onValid:function(){},onOverflow:function(){}},masks:{"phone":{mask:"9999-9999"},"phone-br":{mask:"99 9999-9999"},"phone-us":{mask:"(999) 9999-9999"},"cpf":{mask:"999.999.999-99"},"cnpj":{mask:"99.999.999/9999-99"},"date":{mask:"39/19/9999"},"hour":{mask:"24:60"},"date-us":{mask:"19/39/9999"},"date-cc":{mask:"39/9999"},"cep":{mask:"99999-999"},"hour":{mask:"29:69"},"cc":{mask:"9999 9999 9999 9999"},"integer":{mask:"999.999.999.999",type:"reverse"},"decimal":{mask:"99,999.999.999.999",type:"reverse",defaultValue:"000"},"decimal-us":{mask:"99.999,999,999,999",type:"reverse",defaultValue:"000"},"signed-decimal":{mask:"99,999.999.999.999",type:"reverse",defaultValue:"+000"},"signed-decimal-us":{mask:"99,999.999.999.999",type:"reverse",defaultValue:"+000"}},init:function(){if(!this.hasInit){var C;this.ignore=false;this.fixedCharsReg=new RegExp(this.fixedChars);this.fixedCharsRegG=new RegExp(this.fixedChars,"g");for(C=0;C<=9;C++){this.rules[C]=new RegExp("[0-"+C+"]")}this.hasInit=true}},set:function(G,D){var C=this,E=B(G),F="maxLength";this.init();return E.each(function(){var N=B(this),O=B.extend({},C.options),M=N.attr(O.attr),H="",J=C.__getPasteEvent();H=(typeof D=="string")?D:(M!="")?M:null;if(H){O.mask=H}if(C.masks[H]){O=B.extend(O,C.masks[H])}if(typeof D=="object"){O=B.extend(O,D)}if(B.metadata){O=B.extend(O,N.metadata())}if(O.mask!=null){if(N.data("mask")){C.unset(N)}var I=O.defaultValue,L=N.attr(F),K=(O.type=="reverse");O=B.extend({},O,{maxlength:L,maskArray:O.mask.split(""),maskNonFixedCharsArray:O.mask.replace(C.fixedCharsRegG,"").split(""),defaultValue:I.split("")});if(K){N.css("text-align","right")}if(N.val()!=""){N.val(C.string(N.val(),O))}else{if(I!=""){N.val(C.string(I,O))}}N.data("mask",O);N.removeAttr(F);N.bind("keydown",{func:C._keyDown,thisObj:C},C._onMask).bind("keyup",{func:C._keyUp,thisObj:C},C._onMask).bind("keypress",{func:C._keyPress,thisObj:C},C._onMask).bind(J,{func:C._paste,thisObj:C},C._delayedOnMask)}})},unset:function(D){var C=B(D),E=this;return C.each(function(){var H=B(this);if(H.data("mask")){var F=H.data("mask").maxlength,G=E.__getPasteEvent();if(F!=-1){H.attr("maxLength",F)}H.unbind("keydown",E._onMask).unbind("keypress",E._onMask).unbind("keyup",E._onMask).unbind(G,E._delayedOnMask).removeData("mask")}})},string:function(F,D){this.init();var E={};if(typeof F!="string"){F=String(F)}switch(typeof D){case"string":if(this.masks[D]){E=B.extend(E,this.masks[D])}else{E.mask=D}break;case"object":E=D;break}var C=(E.type=="reverse");this._insertSignal(C,F,E);return this.__maskArray(F.split(""),E.mask.replace(this.fixedCharsRegG,"").split(""),E.mask.split(""),C,E.defaultValue,E.signal)},_onMask:function(C){var E=C.data.thisObj,D={};D._this=C.target;D.$this=B(D._this);if(D.$this.attr("readonly")){return true}D.value=D.$this.val();D.nKey=E.__getKeyNumber(C);D.range=E.__getRangePosition(D._this);D.valueArray=D.value.split("");D.data=D.$this.data("mask");D.reverse=(D.data.type=="reverse");return C.data.func.call(E,C,D)},_delayedOnMask:function(C){C.type="paste";setTimeout(function(){C.data.thisObj._onMask(C)},1)},_keyDown:function(D,E){var C=A?this.iphoneIgnoreKeys:this.ignoreKeys;this.ignore=(B.inArray(E.nKey,C)>-1);if(this.ignore){E.data.onValid.call(E._this,this.keyRepresentation[E.nKey]?this.keyRepresentation[E.nKey]:"",E.nKey)}return A?this._keyPress(D,E):true},_keyUp:function(C,D){if(D.nKey==9&&(B.browser.safari||B.browser.msie)){return true}return this._paste(C,D)},_paste:function(D,E){this._changeSignal(D.type,E);var C=this.__maskArray(E.valueArray,E.data.maskNonFixedCharsArray,E.data.maskArray,E.reverse,E.data.defaultValue,E.data.signal);E.$this.val(C);if(!E.reverse&&E.data.defaultValue.length&&(E.range.start==E.range.end)){this.__setRange(E._this,E.range.start,E.range.end)}return true},_keyPress:function(J,C){if(this.ignore||J.ctrlKey||J.metaKey||J.altKey){return true}this._changeSignal(J.type,C);var K=String.fromCharCode(C.nKey),M=C.range.start,G=C.value,E=C.data.maskArray;if(C.reverse){var F=G.substr(0,M),I=G.substr(C.range.end,G.length);G=(F+K+I);if(C.data.signal&&(M-C.data.signal.length>0)){M-=C.data.signal.length}}var L=G.replace(this.fixedCharsRegG,"").split(""),D=this.__extraPositionsTill(M,E);C.rsEp=M+D;if(!this.rules[E[C.rsEp]]){C.data.onOverflow.call(C._this,K,C.nKey);return false}else{if(!this.rules[E[C.rsEp]].test(K)){C.data.onInvalid.call(C._this,K,C.nKey);return false}else{C.data.onValid.call(C._this,K,C.nKey)}}var H=this.__maskArray(L,C.data.maskNonFixedCharsArray,E,C.reverse,C.data.defaultValue,C.data.signal,D);C.$this.val(H);return(C.reverse)?this._keyPressReverse(J,C):this._keyPressFixed(J,C)},_keyPressFixed:function(C,D){if(D.range.start==D.range.end){if((D.rsEp==0&&D.value.length==0)||D.rsEp<D.value.length){this.__setRange(D._this,D.rsEp,D.rsEp+1)}}else{this.__setRange(D._this,D.range.start,D.range.end)}return true},_keyPressReverse:function(C,D){if(B.browser.msie&&((D.rangeStart==0&&D.range.end==0)||D.rangeStart!=D.range.end)){this.__setRange(D._this,D.value.length)}return false},_setMaskData:function(F,C,E){var D=F.data("mask");D[C]=E;F.data("mask",D)},_changeSignal:function(D,E){if(E.data.signal!==false){var C=(D=="paste")?E.value.substr(0,1):String.fromCharCode(E.nKey);if(B.inArray(C,this.signals)>-1){if(C=="+"){C=""}this._setMaskData(E.$this,"signal",C);E.data.signal=C}}},_insertSignal:function(C,F,E){if(C&&E.defaultValue){if(typeof E.defaultValue=="string"){E.defaultValue=E.defaultValue.split("")}if(B.inArray(E.defaultValue[0],this.signals)>-1){var D=F.substr(0,1);E.signal=(B.inArray(D,this.signals)>-1)?D:E.defaultValue[0];if(E.signal=="+"){E.signal=""}E.defaultValue.shift()}}},__getPasteEvent:function(){return(B.browser.opera||(B.browser.mozilla&&parseFloat(B.browser.version.substr(0,3))<1.9))?"input":"paste"},__getKeyNumber:function(C){return(C.charCode||C.keyCode||C.which)},__maskArray:function(H,G,E,D,C,I,F){if(D){H.reverse()}H=this.__removeInvalidChars(H,G);if(C){H=this.__applyDefaultValue.call(H,C)}H=this.__applyMask(H,E,F);if(D){H.reverse();if(!I||I=="+"){I=""}return I+H.join("").substring(H.length-E.length)}else{return H.join("").substring(0,E.length)}},__applyDefaultValue:function(E){var C=E.length,D=this.length,F;for(F=D-1;F>=0;F--){if(this[F]==E[0]){this.pop()}else{break}}for(F=0;F<C;F++){if(!this[F]){this[F]=E[F]}}return this},__removeInvalidChars:function(E,D){for(var C=0;C<E.length;C++){if(D[C]&&this.rules[D[C]]&&!this.rules[D[C]].test(E[C])){E.splice(C,1);C--}}return E},__applyMask:function(E,C,F){if(typeof F=="undefined"){F=0}for(var D=0;D<E.length+F;D++){if(C[D]&&this.fixedCharsReg.test(C[D])){E.splice(D,0,C[D])}}return E},__extraPositionsTill:function(E,C){var D=0;while(this.fixedCharsReg.test(C[E])){E++;D++}return D},__setRange:function(E,F,C){if(typeof C=="undefined"){C=F}if(E.setSelectionRange){E.setSelectionRange(F,C)}else{var D=E.createTextRange();D.collapse();D.moveStart("character",F);D.moveEnd("character",C-F);D.select()}},__getRangePosition:function(D){if(!B.browser.msie){return{start:D.selectionStart,end:D.selectionEnd}}var E={start:0,end:0},C=document.selection.createRange();E.start=0-C.duplicate().moveStart("character",-100000);E.end=E.start+C.text.length;return E}}});B.fn.extend({setMask:function(C){B.invalid;return B.mask.set(this,C)},unsetMask:function(){return B.mask.unset(this)}})})(jQuery)
