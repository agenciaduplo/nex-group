<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Apartamentos de 2 e 3 dormitórios com suíte - JK PARQUE CLUBE</title>
	
	<meta name="description" content="Apartamentos de 2 e 3 dormitórios com suíte em Pelotas. JK PARQUE CLUBE - Incorporação e Construção NEX GROUP." />
	<meta name="keywords" content="jk, jkparqueclube, parqueclube, apto, apartamentos, pelotas, 1 dormitório, 2 dormitórios, aptos com suíte, suíte, nex group, nexgroup, nex, group, imóveis, aptos, apartamentos, casas, conjuntos, salas, construtora, incorporadora, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	    		
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='JK PARQUE CLUBE' />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='Apartamentos de 2 e 3 dormitórios com suíte em Pelotas.'/>
	<meta property='og:url' content='http://www.jkparqueclube.com.br'/>
	    

	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,700,900' rel='stylesheet' type='text/css'>
    
<!-- CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">

<!-- JS -->
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>
	<script src="<?=base_url()?>assets/js/function.js"></script>
 	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    
    <!--  SUCESSO -->
	<script>
        function closePromo() {
            $('.sucbox').fadeOut('slow');
        }
    </script>

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-42832022-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
     
</head>    

<body> 

	<div class="sucbox" style="display:none;">
    	<div class="suc title"><p>Sua Mensagem foi <span>enviada com Sucesso!</span></p></div>
        <a onclick="closePromo();" href="#" class="fechar"><i></i><div class="ok">Ok!</div></a>
    </div>
<div class="container">
	<div class="left">
        <!-- AddThis Button BEGIN
        <div class="addthis_toolbox addthis_default_style " style="float:left; margin-top: 10px;">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-513de0b25d7bcf95"></script>
        AddThis Button END -->	
        <!--div class="line"></div-->
        <!--h3>Breve Lançamento</h3>	
        <!--div class="abkuu"></div-->
        <!--h3>Zona Norte, Próximo ao Novo Shopping Pelotas</h3-->
    </div>
	<!--div class="midle">
    	<h1>JK PARQUE CLUBE JACARANDÁ - Aptos de 2 e 3 Dorms. com suíte.
A maior infraestrutura de lazer da cidade.</h1>
		<h2>Agora você pode se divertir em um clube todo dia.
e nem precisa colocar o pé pra fora de casa.</h2>
    </div-->

  <div class="fix">
    <div class="atendimento">
      <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=30&referencia=<?=urlencode($midia);?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Atendimento on line"><img src="assets/img/btn_atendimento.fw.png" /></a>
    </div>
  	<div class="ctn-box">
  	  <div id="head_form">
  		<h2 id="ttl-plantao">Plantão de Vendas</h2>
      <h3 id="ttl-rua">Rua Félix da Cunha, 670 - Centro</h3>
  		<h3 id="ttl-phone"><span>(53)</span> <? if(strlen($telefone)>3) { echo $telefone; } else { ?> 3025.4657 <? } ?></h3>
	 </div>
  	<div class="box">
		  <p class="title">Cadastre-se e receba <span>maiores informações</span></p>
          <form id="FormInteresse">
            <input type="hidden" name="midia" id="midia" value="<?=$midia;?>">
          	<label>
                  <p>Nome*</p>
                  <input name="nome" id="nome" class="validate[required] nome textfield" type="text">    
          	</label>
          	<label>
                  <p class="iemail">E-mail*</p>
                  <input name="email" id="email" class="validate[required,custom[email]] mail textfield" type="text">    
          	</label>
          	<label>
                  <p class="ietel">Fone*</p>
                  <input name="telefone" id="telefone" class="validate[required] tel textfield" type="text">    
          	</label>
          	<label>
                  <p class="iemen">Mensagem*</p>
                  <textarea id="mensagem" name="mensagem" class="validate[required]" cols="" rows=""></textarea>  
              </label>
              <input class="button" id="buttonSend" name="ENVIAR" value="ENVIAR" type="submit">
              <span id="legend">*Campos obrigatórios</span>
          </form>
     </div>
  </div>
  </div>
</div>
<!-- FOOTER -->
	<footer>
		<div class="container">
			<div class="foo_left"></div>
			<div class="foo_right">
				<p>
					Incorporação e Construção: Capa Engenharia, uma empresa Nex Group.<br>
					Projeto Arquitetônico: Arquiteto Frankling Moreira - CAU A7762-3. Projeto Paisagístico: Arquiteta Evelise Tellini Vontobel - CAU A76017-0. Matrícula nº 85.827 - 1º Zona do Registro de Imóveis de Pelotas.
				</p>
			</div>
		</div>
    </footer>
    
    <?php if($this->uri->segment(1) == 'cadastro-enviado-com-sucesso'): ?>
    
    <!-- Google Code for Cadastro Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 983769443;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "GScPCM3chwgQ48KM1QM";
	var google_conversion_value = 1;
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/983769443/?value=1&amp;label=GScPCM3chwgQ48KM1QM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

    
    <script>
    	jQuery(document).ready(function(){
    		$(".sucbox").fadeIn();
    	});
    </script>
    <?php endif; ?>
    <!-- <div id="conversaoAdwords"></div> -->
    
<!-- FOOTER END -->

	<!-- Código do Google para tag de remarketing -->
	<!--------------------------------------------------
	As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 978900397;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/978900397/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

</body>
</html>