<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index(){
		$this->load->helper('source');
		source();
		$this->load->view('home');
	}
	
	public function conversao ()
	{
		$this->load->view('conversao_adwords.php');
	} 
		
	public function sendInteresse()
	{
		$this->load->library('user_agent');
		
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;
		
		if($this->input->post('nome') == "MonitorLead" || $this->input->post('email') == "monitor@leads.com.br")
		{
			
		}
		else
		{
			if($this->agent->is_robot())
			{
				
			}
			else
			{
		
				$data = array(
					'id_empreendimento' 	=> 91,
					'id_estado' 			=> 21,
					'ip' 					=> $this->input->ip_address(),
					'user_agent' 			=> $this->input->user_agent(),
					'url' 					=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
					'origem' 				=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
					'data_envio' 			=> date("Y-m-d H:i:s"),
					'nome' 					=> $this->input->post('nome'),
					'email' 				=> strtolower($this->input->post('email')),
					'telefone' 				=> $this->input->post('telefone'),
					'comentarios' 			=> nl2br(strip_tags($this->input->post('mensagem'))),
					'hotsite' 				=> 'S'
				);
		
				if(!empty($data['email']) && !empty($data['nome'])){
					$this->load->model('contato_model','model');
					$interesse = $this->model->setInteresse($data);
					$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);
		
					$data['empreendimento'] = $empreendimento;
					$html = $this->load->view('tpl/email',$data,true);
		
					$this->load->library('email',array(
						'mailtype' 	=> 'html',
						'wordwrap' 	=> false,
						'protocol'	=> 'sendmail'
					));
		
					/* envia email via roleta */
					$id_empreendimento = 91;
					$grupos = $this->model->getGrupos($id_empreendimento);
		
					$total = 0;
					$total = count($grupos);
					$total = $total;
					
					foreach ($grupos as $row):
		
						if($row->rand == 1):
							$atual = $row->ordem;
		
							if($atual == $total):
		
								$this->model->updateRand($id_empreendimento, '001');
		
								$emails = $this->model->getGruposEmails($id_empreendimento, '001');
		
								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;
		
									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
		
							else:
		
								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);
		
								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
		
								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;
		
									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
		
							endif;	
						endif;
					endforeach;
					/* envia email via roleta */
		
					if($data['nome'] == "Teste123"){
						$this->email->to('testes@divex.com.br');
					} else {
						$this->email->to($list);
					}
		
					$this->email->bcc(array('testes@divex.com.br', 'bruno@divex.com.br'));
					$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
					$this->email->subject('Interesse enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $data),true));
		
					$this->email->send();
		
					$_SESSION['sucesso'] = "Interesse enviado com sucesso!";
					$_SESSION['tracker'] = array(
						'email' 	=> $data['email'],
						'nome' 		=> $data['nome']
					);
				}
				redirect($return);
			}
		}
	}

}