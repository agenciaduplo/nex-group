<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index(){
		$this->load->helper('source');
		source();
		$this->load->view('home');
	}
	
	public function conversao ()
	{
		$this->load->view('conversao_adwords.php');
	}
  
	public function notfound(){
		$this->load->helper('source');
		source();
    $data['telefone'] = "";
    $data['midia'] = "";
    switch(strtolower($_SERVER['REQUEST_URI'])) {
      case '/oferta':
        $data['telefone'] = '4062-9150';
        $data['midia'] = "Jornal - Diario Polular";
      break;
      case '/promocao':
        $data['telefone'] = '4062-9210';
        $data['midia'] = "Site Pop Diario Popular";
      break;
      case '/oportunidade':
        $data['telefone'] = '4062-9310';
        $data['midia'] = "Google Ads";
      break;
      case '/lancamento':
        $data['telefone'] = '4062-9190';
        $data['midia'] = " Facebook Ads";
      break;      
      case '/pelotas':
        $data['telefone'] = '4062-9309';
        $data['midia'] = "Volante";
      break;     
      case '/desconto':
        $data['telefone'] = '4062-9260';
        $data['midia'] = "Conteiner";  
      break;       
      case '/zonanorte':
        $data['telefone'] = '4062-9212';
        $data['midia'] = "Email Mkt";
      break;
    }
		$this->load->view('home',$data);
	} 
	
	public function sendInteresse($sid = 0){
	
	
    //------------------------------
    // REGISTRA ACESSO
    require_once("AdManagerAPI.class.php");    
    $admanager = new AdManagerAPI ();
    $admanager->registraLead('FORM_INTERESSE',$this->input->post('nome'),$this->input->post('email'),$this->input->post('telefone'),'','','Brasil',nl2br(strip_tags($this->input->post('mensagem'))));
    //------------------------------
    
	
		//if($sid == session_id()){
			$return = $this->input->get('return');
			$return = !empty($return) ? $return : site_url() . "#contato";

			$data = array(
				'id_empreendimento' 	=> 91,
				'id_estado' 			=> 21,
				'ip' 					=> $this->input->ip_address(),
				'user_agent' 			=> $this->input->user_agent(),
				'url' 					=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' 				=> $this->input->post('midia'),
				'data_envio' 			=> date("Y-m-d H:i:s"),
				'nome' 					=> $this->input->post('nome'),
				'email' 				=> strtolower($this->input->post('email')),
				'telefone' 				=> $this->input->post('telefone'),
				'comentarios' 			=> nl2br(strip_tags($this->input->post('mensagem'))),
				'hotsite' 				=> 'S'
			);

			if(!empty($data['email']) && !empty($data['nome'])){
				$this->load->model('contato_model','model');
				$interesse = $this->model->setInteresse($data);
				$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

				$dados = array(
					'Enviado em ' => $data['data_envio'] . ' via HotSite',
					'Enviado por ' => $data['nome'],
					'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
					'IP' => $data['ip'],
					'E-mail' => $data['email'],
					'Telefone' => $data['telefone'],
          'Midia' => $this->input->post('midia'),
					('Comentários') => $data['comentarios']
				);
				
				$this->load->library('email',array(
					'protocol' => 'sendmail',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				));

				/* envia email via roleta */
				$id_empreendimento = 91;
				$grupos = $this->model->getGrupos($id_empreendimento);

				$total = 0;
				$total = count($grupos);
				
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;

						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');

							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);

							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
							//echo "<pre>";print_r($emails);echo "</pre>";

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
				/* envia email via roleta */

				$this->email->to($list);
				$this->email->bcc('marketing@reweb.com.br');
				$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
				$this->email->subject('Contato enviado via HotSite');

				$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
				

				$this->email->send();

				//ENVIA EMAIL DE RETORNO
				$this->email->clear();
				$this->email->to($this->input->post('email'));
				$this->email->bcc('marketing@reweb.com.br');
				$this->email->from("noreply@jkparqueclube.com.br", "JK Parque Clube - Nex Group");
				$this->email->subject('Cadastro - JK Parque Clube - Nex Group');
				$this->email->message('<html>
										<head>
											<title>Nex Group</title>
										</head>
										<body>
											<table cellpadding="0" cellspacing="0" border="0">
												<tr>
													<td valign="top" style="font-size: 0;">
														<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img1.jpg">
													</td>
												</tr>
												<tr>
													<td valign="top" style="font-size: 0;">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img2.jpg">
																</td>
																<td valign="top">
																	<a href="http://www.youtube.com/watch?v=Qoitfe1ntyw"><img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img3.jpg"></a>
																</td>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img4.jpg">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top" style="font-size: 0;">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img5.jpg">
																</td>
																<td valign="top">
																	<a href="http://www.nexgroup.com.br/"><img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img6.jpg"></a>
																</td>
																<td valign="top">
																	<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img7.jpg">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<img border="0" src="http://www.nexgroup.com.br/assets/admin/img/email/img8.jpg">
													</td>
												</tr>
											</table>
										</body>
										</html>');
				$this->email->send();

				$_SESSION['sucesso'] = true;
				$_SESSION['tracker'] = array(
					'email' => $data['email'],
					'nome' => $data['nome']
				);
			}
			redirect($return);
		//} else { show_404(); }
	}

}