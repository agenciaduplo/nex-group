<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>My Urban Life - Assista ao Vídeo</title>

	<link href="<?=base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?=base_url(); ?>/favico.png" type="image/x-png" />
    
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='My Urban Life.' />
	<meta property='og:image' content='http://www.myurbanlife.com.br/assets/img/logo.png'/>
	<meta property='og:description' content='Minhas escolhas, minhas conquistas. Antecipe-se ao pré-lançamento.'/>
	<meta property='og:url' content='http://www.myurbanlife.com.br/'/>

    
    
	<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/mask.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/defualt.js"></script>

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-63']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	  
	  
	</script>

</head>
<body> 
    <div class="video-body">
        <div class="box">
            <div class="video-tit"></div>
            <a href="<?=site_url(); ?>"><div class="video-fechar"></div></a>
            
            <div class="youtube">
                <iframe width="700" height="394" src="http://www.youtube.com/embed/4puqVHYyh4A?autoplay=1" frameborder="0" allowfullscreen></iframe>
            </div>
            
        </div>
    </div>
    
    <a class="DivexRodape" href="http://www.divex.com.br/imobi" target="_blank" title="Desenvolvido por Divex"><div id="divex2"></div></a>
	    
</body>
</html>