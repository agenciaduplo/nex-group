<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Apartamentos de 2 e 3 dormitórios com suíte - JK PARQUE CLUBE</title>
	
	<meta name="description" content="Apartamentos de 2 e 3 dormitórios com suíte em Pelotas. JK PARQUE CLUBE - Incorporação e Construção NEX GROUP." />
	<meta name="keywords" content="jk, jkparqueclube, parqueclube, apto, apartamentos, pelotas, 1 dormitório, 2 dormitórios, aptos com suíte, suíte, nex group, nexgroup, nex, group, imóveis, aptos, apartamentos, casas, conjuntos, salas, construtora, incorporadora, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	    		
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='JK PARQUE CLUBE' />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='Apartamentos de 2 e 3 dormitórios com suíte em Pelotas.'/>
	<meta property='og:url' content='http://www.jkparqueclube.com.br'/>
	    

	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,700,900' rel='stylesheet' type='text/css'>
    
<!-- CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/template.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validationEngine.jquery.css">

<!-- JS -->
	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine-pt_BR.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.validationEngine.js"></script>
	<script src="<?=base_url()?>assets/js/function.js"></script>
 	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
    
    <!--  SUCESSO -->
	<script>
        function closePromo() {
            $('.sucbox').fadeOut('slow');
        }
    </script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-41179199-1', 'jkparqueclube.com.br');
	  ga('send', 'pageview');
	
	</script>
     
</head>    

<body> 

	<div class="sucbox" style="display:none;">
    	<div class="suc title"><p>Sua Mensagem foi <span>enviada com Sucesso!</span></p></div>
        <a onclick="closePromo();" href="#" class="fechar"><i></i><div class="ok">Ok!</div></a>
    </div>

	<div class="left">
        <!-- AddThis Button BEGIN 	-->	
        <div class="addthis_toolbox addthis_default_style " style="float:left; margin-top: 10px;">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-513de0b25d7bcf95"></script>
        <!-- AddThis Button END -->	
        <div class="line"></div>
        <h3>Breve Lançamento | Pelotas</h3>	
        <div class="abkuu"></div>
        <h3 style="color:#FFF; font-weight:300; font-size: 14px;">Zona Norte, Próximo ao Novo Shopping</h3>
    </div>
	<div class="midle">
    	<h1>JK PARQUE CLUBE JACARANDÁ - Aptos de 2 e 3 Dorms. com suíte.
A maior infraestrutura de lazer da cidade.</h1>
		<h2>Agora você pode se divertir em um clube todo dia.
e nem precisa colocar o pé pra fora de casa.</h2>
    </div>

	<div class="box">
    	<p class="title">Cadastre-se para receber <span>maiores informações.</span></p>
        <form id="FormInteresse">
        	<label>
                <p>Nome:</p>
                <input name="nome" id="nome" class="validate[required] nome textfield" type="text">    
        	</label>
        	<label>
                <p class="iemail">E-mail:</p>
                <input name="email" id="email" class="validate[required,custom[email]] mail textfield" type="text">    
        	</label>
        	<label>
                <p class="ietel">Telefone:</p>
                <input name="telefone" id="telefone" class="validate[required] tel textfield" type="text">    
        	</label>
        	<label>
                <p class="iemen">Mensagem:</p>
                <textarea id="mensagem" name="mensagem" cols="" rows=""></textarea>  
            </label>
            <p class="obri">* Campos Obrigatórios</p>
            <input class="button" name="ENVIAR" value="ENVIAR" type="submit">
            
        </form>
    </div>

<!-- FOOTER -->
	<footer>
       <a href="http://www.divex.com.br/" title="Desenvolvido por Divex Imobi" id="divex"></a>
       <div class="foo_left"></div>
       <div class="foo_right">
       		<p>
       			Incorporação e Construção: Capa Engenharia, uma empresa Nex Group.<br>
       			Projeto Arquitetônico: Arquiteto Frankling Moreira - CAU A7762-3. Projeto Paisagístico: Arquiteta Evelise Tellini Vontobel - CAU A76017-0.
	       	</p>
       </div>
    </footer>
<!-- FOOTER END -->

	<!-- Código do Google para tag de remarketing -->
		<!--------------------------------------------------
		As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 983735308;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983735308/?value=0&guid=ON&script=0"/>
			</div>
		</noscript>

</body>
</html>