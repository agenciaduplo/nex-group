<?php

$lang['upload_userfile_not_set'] = "Não foi possível encontrar uma variável post chamado userfile.";
$lang['upload_file_exceeds_limit'] = "O arquivo enviado excede o tamanho máximo permitido no seu arquivo de configuração do PHP.";
$lang['upload_file_exceeds_form_limit'] = "O arquivo enviado excede o tamanho máximo permitido pelo formulário de envio.";
$lang['upload_file_partial'] = "O arquivo foi apenas parcialmente carregado.";
$lang['upload_no_temp_directory'] = "A pasta temporária não foi encontrda.";
$lang['upload_unable_to_write_file'] = "O arquivo não pôde ser gravada em disco.";
$lang['upload_stopped_by_extension'] = "O arquivo não pôde ser escrever upload do arquivo foi interrompido por extensão.";
$lang['upload_no_file_selected'] = "Você não selecionou um arquivo para upload.";
$lang['upload_invalid_filetype'] = "O tipo de arquivo que você está tentando enviar não é permitido.";
$lang['upload_invalid_filesize'] = "O arquivo que você está tentando fazer o upload é maior do que o tamanho permitido.";
$lang['upload_invalid_dimensions'] = "A imagem que você está tentando fazer o upload exceede a altura máxima ou largura.";
$lang['upload_destination_error'] = "Um problema foi encontrado ao tentar mover o arquivo enviado para o destino final.";
$lang['upload_no_filepath'] = "O caminho do upload não parece ser válido.";
$lang['upload_no_file_types'] = "Você não especificou nenhum tipo de arquivo permitido.";
$lang['upload_bad_filename'] = "O nome do arquivo que você enviou já existe no servidor.";
$lang['upload_not_writable'] = "A pasta de destino de upload não parece ter permissão de escrita.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */