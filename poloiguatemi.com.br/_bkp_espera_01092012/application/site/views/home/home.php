<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Polo Iguatemi</title>

	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validation/validationEngine.jquery.css" type="text/css" media="screen" />
	
	<meta name="title" content="Polo Iguatemi" />
    <meta name="description" content="2 e 3 suítes a 200 Metros do Shopping Iguatemi" />
    <meta name="keywords" content="polo iguatemi, 2 e 3 suítes, 2 suítes, 3 suítes, iguatemi, próximo ao iguatemi, 200 metros do iguatemi" />
    <meta name="robots" content="all" />
	
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='Polo Iguatemi' />
	<meta property='og:description' content='2 e 3 suítes a 200 Metros do Shopping Iguatemi'/>
	<meta property='og:url' content='http://www.poloiguatemi.com.br'/>
	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<link rel="shortcut icon" href="<?=base_url()?>assets/img/fav-ico.ico" type="image/x-icon" />
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,300italic,400italic' rel='stylesheet' type='text/css'>

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-59']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>    
<body style="background:url(<?=base_url()?>assets/img/bg.jpg) top no-repeat;"> 
<img src="http://www.poloiguatemi.com.br/assets/img/polo.png" width="153" height="93" style="display:none" />
<div id="container">
  <div id="info1">BREVE LANÇAMENTO, JUNTO AO SHOPPING IGUATEMI</div>	
    <div id="info2">2 e 3 suítes</div>	
    <!-- <div id="info2">2 e 3 suítes • <span>88, 123 e 134 m²</span></div>	-->
    <div id="cadastro">
    	<p>CADASTRE-SE E DESCUBRA EM PRIMEIRA
           MÃO O QUE FAZ O POLO IGUATEMI SER 
           ÚNICO NO ESPAÇO E IDEAL NA LOCALIZAÇÃO.
		</p>
		<form action="<?=base_url()?>enviar-interesse" name="formContato" id="formContato" method="post">
        	<ul>
                <li><label>*Nome:</label><input id="frmNome" class="formus validate[required]" name="nome" type="text" value="" /></li>
                <li><label>*E-mail:</label><input id="frmEmail" class="formus validate[required,custom[email]]" name="email" type="text" value="" /></li>
                <li><label>Telefone:</label><input id="frmTelefone" class="formus" name="telefone" type="text" value="" /></li>
				<li><label>Comentário:</label><textarea id="frmComentario" name="comentario" cols="" rows=""></textarea></li>
				<span>*Campos obrigatórios.</span>
                <li><input class="btn" name="enviar" type="submit" value="enviar >"/></li>
			</ul>
		</form>
    </div>	
    <div id="menu">
    	<p class="menu-in menu-in-1">
        	<span>Metragem diferenciada.<br /></span>
                  Apartamentos
                  realmente amplos,
                  todos com suítes.
        </p>
    	<p class="menu-in menu-in-2">
        	<span> Localização ideal.<br /></span>
                   A 200 metros do Shopping
                   Iguatemi e junto ao
                   Parque Germânia.
        </p>
    	<p class="menu-in menu-in-3">
        	<span>Quadra de tênis e espaço fitness.<br /></span>
                  Uma completa estrura
                  de lazer para você.
        </p>
    	<p class="menu-in menu-in-4">
        	<span>Lançamento com o selo Nex.<br /></span>
                  Credibilidade que garante
                  a sua tranquilidade.     
	   </p>
    
    </div>	
    <div id="footer">
    	<p>Imagens meramente ilustrativas.</p>
        <div id="st-promo-social">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <!-- <a class="addthis_button_tweet"></a> -->
            </div>
            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f6a24a51a684879"></script>
            <!-- AddThis Button END -->
        </div>
    	<a href="https://www.facebook.com/NexGroup" class="facebook" title="Acesse o facebook da Nex Group"><span>Acesse o facebook Nex Group</span></a>
    	<a href="http://www.nexgroup.com.br/" class="nex" title="Acesse o site da Nexgroup"></a>
    	<a href="http://www.eglengenharia.com.br/" class="egl" title="Acesse o site da EGL Engenharia"></a>
    </div>	
</div>

</body>
</html>