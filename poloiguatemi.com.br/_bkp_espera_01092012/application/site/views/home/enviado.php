<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Polo Iguatemi</title>

	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?=base_url()?>assets/img/fav-ico.ico" type="image/x-icon" />
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,300italic,400italic' rel='stylesheet' type='text/css'>

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-59']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>    
<body style="background:url(<?=base_url()?>assets/img/bg2.jpg) top;"> 
  
<div id="container">
    <div id="info2-2">2 e 3 suítes • <span>88, 123 e 134 m²</span></div>	
    <div id="cadastro2">
    	<p class="p1">ÚNICO NO ESPAÇO. IDEAL NA LOCALIZAÇÃO.</p>
    	<p class="p2">SUA MENSAGEM FOI ENVIADA COM SUCESSO!</p>
    	<p class="p3">Em breve entraremos em contato com você.</p>
		<a class="btn" href="<?=base_url()?>"><p>< voltar</p></a>

    </div>	
    <div id="footer">
    	<p>Imagens meramente ilustrativas.</p>
        <div id="st-promo-social">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <!-- <a class="addthis_button_tweet"></a> -->
            </div>
            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f6a24a51a684879"></script>
            <!-- AddThis Button END -->
        </div>

        
    	<a href="https://www.facebook.com/NexGroup" class="facebook" title="Acesse o facebook da Nexgroup"><span>Acesse o facebook da Nexgroup</span></a>
    	<a href="http://www.nexgroup.com.br/" class="nex" title="Acesse o site da Nexgroup"></a>
    	<a href="http://www.eglengenharia.com.br/" class="egl" title="Acesse o site da EGL Engenharia"></a>
    </div>	
</div>

</body>
</html>