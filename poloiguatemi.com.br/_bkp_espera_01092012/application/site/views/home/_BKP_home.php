<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Singolo</title>

	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>
	
	<script type="text/javascript">
	/*
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-56']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	*/
	</script>

</head>
<body>
	<div id="container"></div>
    <a href="index.html">
		<div id="logo"></div>
    </a>
	<div id="topa">	
		<div id="text1"><p><span>Antecipe-se</span> <br />
		 Em breve mais um empreendimento com a qualidade Nex Group.</p></div>
	</div>
	<div id="text">
	</div>

	<div id="interesse">
		<div id="form">
		<div id="text4"><p>Cadastre-se	</p></div>
        	<p>Preencha os campos e aguarde que logo entraremos em contato.</p>
            <form action="" name="formContato" id="formContato" method="post">
                <ul>       
                    <li> 
                        <label for="nome">NOME*</label>
                        <input name="nome" id="nome" type="text" class="form1 validate[required]" />
                    </li>
                    <li> 
                        <label for="email">EMAIL *</label>
                        <input name="email" id="email" type="text" class="form1 validate[required,custom[email]]" />
                    </li>
                    <li> 
                        <label for="telefone">TELEFONE</label>
                        <input name="telefone" id="telefone" type="text" class="form1" />
                    </li>
                    <li> 
                        <label for="comentarios">COMENTÁRIO</label>
                        <textarea id="comentarios" name="comentarios" type="text" class="form3"></textarea>
                    </li>
                </ul>
                
                <p class="ie7">Campos marcados com asterisco (*) são obrigatórios.</p>
				<input id="btn-enviar" name="Enviar" value="ENVIAR" type="submit" />
			</form>
			<div style="display:none;" class="msg-sucesso"><p>Sua mensagem foi enviada com sucesso.</p></div>

           
            <!-- <div id="btn-enviar"> 
                <a href="javascript:void(0)" onclick="enviarContato();" >Enviar</a>
            </div> -->
            
		</div>
	</div>
	<div id="footer">
    	<p>Rua Dr. Pereira Neto, 10 - Tristeza - Porto Alegre</p>
   		<a class="f-home" href="<?=site_url()?>veja-o-mapa">Ver Mapa</a>
    </div>
    <a target="_blank" href="http://divex.com.br/">
		<div id="divex"></div>
    </a>
        
</body>
</html>