<!DOCTYPE html>
<html lang="pt-BR">
<!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8" lang="pt-BR"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9" lang="pt-BR"> <![endif]-->  

<title><?=isset($title) ? $title : 'Apartamentos de 2 ou 3 suítes, opções de garden e cobertura - Polo'; ?></title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="<?=isset($description) ? $description : 'Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.'; ?>">
<meta name="keywords" content="<?=isset($keywords) ? $keywords : 'apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico'; ?>">

<meta property='og:locale' content='pt_BR' />
<meta property='og:title' content='Polo' />
<meta property='og:image' content='http://www.poloiguatemi.com.br/assets/img/logo-branco.png'/>
<meta property='og:description' content='2 ou 3 suítes e opções de garden e cobertura a 200 Metros do Shopping Iguatemi'/>
<meta property='og:url' content='http://www.polonex.com.br'/>

<link rel="shortcut icon" type="image/png" href="<?=base_url(); ?>favicon.png">
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Lato:300,700,900'>
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/960_12_col.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/animate-custom.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/nivo-slider.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/img/default/default.css">

<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />


<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/ga.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.easing.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.history.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.nivo.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.lemmon.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript" src="<?=base_url(); ?>assets/js/metas.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/onload.js"></script>


</head>
<body>
