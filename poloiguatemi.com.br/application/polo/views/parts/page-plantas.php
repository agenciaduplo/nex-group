	<section id="plantas">
	<div class="bg-balls-details">
		<div class="bg2">
			<div class="container_12">
				<header id="plantasnav">
					<?php $this->load->view('tpl/menu',array('active' => 'plantas')); ?>
				</header>
				<h2>PLANTAS</h2>
				<div class="box-green">
					<!--<h3>Consulte opções de Garden e Cobertura</h3>-->
					<div class="swift">
						<ul>
							<li onclick="javascript:plantas.dorms2(this);" class="active"><a href="javascript:void(0);">2 suítes</a></li>
							<li onclick="javascript:plantas.dorms3(this);"><a href="javascript:void(0);">3 suítes</a></li>
							<li onclick="javascript:plantas.impla4(this);"><a style="margin-left: -10px;" href="javascript:void(0);">Implantação</a></li>
						</ul>
					</div>
					<div id="2suites" class="slider-wrapper theme-default">
						<div class="slider nivoSlider">
							<img src="<?=base_url(); ?>assets/img/plantas/02.jpg" width="500" >
							<img src="<?=base_url(); ?>assets/img/plantas/03.jpg" width="500" >
						</div>
					</div>	

                    
					<div id="3suites" style="display:none" class="slider-wrapper theme-default">
						<div class="slider nivoSlider">
							<img src="<?=base_url(); ?>assets/img/plantas/01.jpg" width="500" >
							<img src="<?=base_url(); ?>assets/img/plantas/04.jpg" width="500" >
						</div>
					</div>						 

					<div id="impla" style="display:none" class="slider-wrapper theme-default">
						<div class="slider nivoSlider">
							<img src="<?=base_url(); ?>assets/img/plantas/impla.jpg" width="500" >
						</div>
					</div>						 

				</div>
				<h4>2 e 3 suítes <span>89 a 136 m²</span><br><br> CONSULTE OPÇÕES DE <span>GARDEN</span> E <span>COBERTURA</span></h4>	<br />
				<ul class="nav"><a href="#localizacaonav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
			</div>  
		</div>  
	</div>
</section>