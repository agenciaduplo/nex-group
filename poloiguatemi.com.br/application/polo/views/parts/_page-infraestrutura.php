<section id="infraestrutura">
	<div class="bg3"></div> 
	<div class="people2"></div>
	<div class="wrap">
		<div id="slider1" class="slider2">
			<ul>
				<li><img src="<?=base_url(); ?>assets/img/infra/09.jpg" width="630" height="346" alt="Tennis lounge" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/01.jpg" width="630" height="346" alt="Quadra de tênis de saibro" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/02.jpg" width="630" height="346" alt="Lazer externo" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/03.jpg" width="630" height="346" alt="Piscina aquecida com raia" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/04.jpg" width="630" height="346" alt="Piscina externa com pool lounge" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/05.jpg" width="630" height="346" alt="Fitness" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/06.jpg" width="630" height="346" alt="Brinquedoteca" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/07.jpg" width="630" height="346" alt="Salão de jogos teen" /></li>
				<li><img src="<?=base_url(); ?>assets/img/infra/08.jpg" width="630" height="346" alt="Salão de jogos adulto" /></li>
				
			</ul>
		</div>
		<div class="controls">
			<a href="#" class="next-slide">Next Slide</a>
		</div>
	</div>
  
	<div class="container_12">
			<header id="infraestruturanav">
				<?php $this->load->view('tpl/menu',array('active' => 'infraestrutura')); ?>
			</header>
			<h2>INFRAESTRUTURA</h2>
			<h3>Único no espaço.  Ideal na localização.  E especial até no lazer.</h3>

			<ul class="nav"><a href="#plantasnav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
	</div>  
</section>