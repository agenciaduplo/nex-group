<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index(){
		source();
		$this->load->view('home');
	}
	function notfound(){
		show_404();
	}
}