function setMetas(page){
	var page = page.split('#').join('').split('nav').join('');
	var metas = {};
	switch(page){
		case 'empreendimento' :
			metas = {
				title: "Conheça o empreendimento - Polo Iguatemi",
				description: "Conheça o empreendimento. Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.",
				keywords: "empreendimento, apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
		case 'infraestrutura' :
			metas = {
				title: "Conheça a infraestrutura - Polo Iguatemi",
				description: "Conheça a infraestrutura do Polo Iguatemi. Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.",
				keywords: "infraestrutura, infra, apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
		case 'plantas' :
			metas = {
				title: "Plantas de 2 ou 3 suítes e opções de garden e cobertura - Polo Iguatemi",
				description: "Plantas de 2 ou 3 suítes e opções de garden e cobertura. Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.",
				keywords: "infraestrutura, infra, apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
		case 'localizacao' :
			metas = {
				title: "Localizado a 200 metros do Shopping Iguatemi - Polo Iguatemi",
				description: "Localizado a 200 metros do Shopping Iguatemi em Porto Alegre. Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura.",
				keywords: "localização, localizado a 200 metros do shopping iguatemi, apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
		case 'contato' :
			metas = {
				title: "Entre em contato - Polo Iguatemi",
				description: "Venha conhecer o Polo Iguatemi, fale conosco. Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.",
				keywords: "contato, fale conosco, apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
		default :
			metas = {
				title: "Apartamentos de 2 ou 3 suítes, opções de garden e cobertura - Polo Iguatemi",
				description: "Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.",
				keywords: "apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico"
			}
		break;
	}
	if(!$.browser.msie){
		$('title').text(metas.title);
		$('meta[name=description]').attr('content',metas.description);
		$('meta[name=keywords]').attr('content',metas.keywords);
	}
	return metas.title;
}