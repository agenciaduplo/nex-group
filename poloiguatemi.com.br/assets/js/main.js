var History = window.History;
var Title;
var Block = false;
var Cache = false;
var Request = URL.site + 'api/';
var Top = 0;
var Height = 0;
var CurrentPath;
var PagesHeight;

/* Defualt Functions */
var api = {
	block: function(){
		Cache = true;
	},
	release: function(){
		Cache = false;
	},
	abort: function(){
		if(typeof(Cache) == 'object'){
			Cache.abort();
		}
		Cache = false;
	},
	ajax: function(u,o){
		if(Cache){
			api.abort();
		}
		if(o == 'undefined'){
			return Cache = $.post(Request+u);
		} else {
			return Cache = $.post(Request+u,o);
		}
	},
	corretor: function(){
		_gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);
		window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=15','pop','width=450, height=450, top=100, left=100, scrollbars=no');
	}
}
/* Forms Functions */
var input = {
	checked: function(o){
		var o = $(o + ':checked').map(function(i,n) {
			return $(n).val();
		}).get();
		if(o.length == 0) { o = ''; }
		return o;
	},
	get: function(o){
		var o = $(o).val();
		if(o == 'undefined'){
			return '';
		} else {
			return o;
		}
	},
	resetAll: function(){
		$('input').val('');
		$('textarea').text('');
	}
}

/* URI Functions */
var uri = {
	setURL: function(path){
		var Title = setMetas(path);
		if($.browser.msie){
			return path;
		} else {
			if(CurrentPath != path){
				History.pushState(null,Title,path.replace('nav',''));
				CurrentPath = path;
				return path;
			}
		}
		return false;
	},
	normalizePath: function(object){
		object = object.split('#').join('');
		if(object.lastIndexOf('/') == object.length - 1){
			return object.substr(0, object.length - 1);
		} else {
			return object;
		}
	}
}

/* Events Functions */
var events = {
	scroll: function(object){
		if(Block == false){
			Block = true;
			var path = uri.setURL(uri.normalizePath(object));
			if(path != false){
				$(document).scrollTo('#'+path,{
					duration: 2000,
					easing: 'easeOutBack',
					queue: true,
					axis: 'y'
				});
				_gaq.push(['_trackPageview']);
			}
			Block = false;
		}
	},
	scrolling: function(object){
		setMetas(object);
		$(document).scrollTo(object,{ duration: 0, queue: true, axis: 'y' });
	}
}
                      
/* Forms Functions */
var interesse = {

	open: function(){
		$('#modalinteresse').stop().fadeIn(350);
		_gaq.push(['_trackEvent', 'Interesse', 'Abriu', 'Formulário de interesse aberto.']);
		scroll.block()
	},
	close: function(){
		$('#modalinteresse').stop().fadeOut(350);
		scroll.release();
	},
	enviar: function(){
		if(Cache == false){
			api.block();
			var form = '#interesse_form';
			var post = {
				form_nome: input.get(form + ' .form_nome'),
				form_email: input.get(form + ' .form_email'),
				form_telefone: input.get(form + ' .form_telefone'),
				form_mensagem: input.get(form + ' .form_mensagem')
			}
			api.ajax('setInteresse',post).done(function(response){
				_gaq.push(['_trackEvent', 'Interesse', 'Enviou', 'Formulário de interesse enviado.']);
				$(form + ' .msg').html(response);
				scroll.release();
				api.release();
			});
		} return false;
	}
}
var amigo = {

	open: function(){
		$('#modalamigo').stop().fadeIn(350);
		_gaq.push(['_trackEvent', 'Indique', 'Abriu', 'Formulário de indique aberto.']);
		scroll.block()
	},
	close: function(){
		$('#modalamigo').stop().fadeOut(350);
		scroll.release();
	},
	enviar: function(){
		if(Cache == false){
			api.block();
			var form = '#amigo_form';
			var post = {
				form_nome: input.get(form + ' .form_nome'),
				form_email: input.get(form + ' .form_email'),
				form_amigo_nome: input.get(form + ' .form_amigo_nome'),
				form_amigo_email: input.get(form + ' .form_amigo_email'),
				form_mensagem: input.get(form + ' .form_mensagem')
			}
			api.ajax('setIndique',post).done(function(response){
				_gaq.push(['_trackEvent', 'Indique', 'Enviou', 'Formulário de indique enviado.']);
				$(form + ' .msg').html(response);
				scroll.release();
				api.release();
			});
		} return false;
	}
}
var contato = {
	enviar: function(){
		if(Cache == false){
			api.block();
			var form = '#contato_form';
			var post = {
				form_nome: input.get(form + ' .form_nome'),
				form_email: input.get(form + ' .form_email'),
				form_telefone: input.get(form + ' .form_telefone'),
				form_mensagem: input.get(form + ' .form_mensagem')
			}
			api.ajax('setInteresse',post).done(function(response){
				_gaq.push(['_trackEvent', 'Contato', 'Enviou', 'Formulário de contato enviado.']);
				$(form + ' .msg').html(response);
				scroll.release();
				api.release();
			});
		} return false;
	}
}

/* Scroll Functions */
var scroll = {
	block: function(){
		$('body').css('overflow-y','hidden');
	},
	release: function(){
		$('body').css('overflow-y','scroll');
	},
	updateURI: function(i){

		if((i > PagesHeight.contato.top) && (i < PagesHeight.contato.bottom)){
				
			if(CurrentPath != 'contato'){ uri.setURL('contatonav'); }
			
		} else if((i > PagesHeight.localizacao.top) && (i < PagesHeight.localizacao.bottom)){

			if(CurrentPath != 'localizacao'){ uri.setURL('localizacaonav'); }

		} else if((i > PagesHeight.plantas.top) && (i < PagesHeight.plantas.bottom)){

			if(CurrentPath != 'plantas'){ uri.setURL('plantasnav'); }

		} else if((i > PagesHeight.infraestrutura.top) && (i < PagesHeight.infraestrutura.bottom)){

			if(CurrentPath != 'infraestrutura'){ uri.setURL('infraestruturanav'); }

		} else if((i > PagesHeight.empreendimento.top) && (i < PagesHeight.empreendimento.bottom)){

			if(CurrentPath != 'empreendimento'){ uri.setURL('empreendimentonav'); }

		} else if((i > PagesHeight.home.top) && (i < PagesHeight.home.bottom)){

			if(CurrentPath != 'home'){ uri.setURL('home'); }

		}
	}
}

/* Plantas Functions */
var plantas = {
	dorms2: function(object){
		$('.swift li').removeClass('active');
		$(object).addClass('active');
		$('#2suites').show();
		$('#3suites').hide();
		$('#impla').hide();

	},
	dorms3: function(object){
		$('.swift li').removeClass('active');
		$(object).addClass('active');
		$('#3suites').show();
		$('#2suites').hide();
		$('#impla').hide();
	},
	impla4: function(object){
		$('.swift li').removeClass('active');
		$(object).addClass('active');
		$('#impla').show();
		$('#2suites').hide();
		$('#3suites').hide();
	}
}