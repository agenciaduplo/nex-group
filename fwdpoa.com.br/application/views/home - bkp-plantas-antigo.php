<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>FWD</title>

	<link rel="shortcut icon" href="<?=asset_url()?>img/favicon.ico" type="image/x-icon" />

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?=asset_url()?>css/styles.css">

	<!-- <link rel="stylesheet" href="<?=asset_url()?>classic/galleria.classic.css"> -->
	<!-- <link rel="stylesheet" href="<?=asset_url()?>css/gallery-sample.css"> -->

	<script src="<?=asset_url()?>js/jquery-1.11.2.min.js"></script>

	<script> BASE_URL = "<?=base_url()?>"; </script>

	<?php if(base_url() == 'http://www.fwdpoa.com.br/') : ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-54143076-1', 'auto');
		ga('require', 'displayfeatures');
		ga('send', 'pageview');
	</script>
	<?php endif; ?>
</head>
<body>
	<!-- ************ HEADER ************ -->

	<!-- HEADER FIXO -->
	<nav class="header-fixo ">
		<div class="row">
			<a href="#setor1" class="logo"><h1>FWD</h1></a>

			<div class="phone">
				<p>Plantão de Vendas</p>
				<p class="bigger">51 <b>3026</b>.7594</p>
			</div>

			<div class="menu_phone abrir" title="Menu"></div>

			<div id="menu">
				<a id="serto1-click">início</a>
				<a id="serto2-click">Áreas de Lazer</a>
				<a id="serto3-click">Apartamentos</a>
				<a id="serto4-click">decorados</a>
				<a id="serto5-click" class="no-border">localização</a>

				<a href="javascript:window.open('http://www.nexgroup.com.br/chat','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="btn no-border">Corretor<br><b>Online</b></a>
				<a href="javascript:void(0);" class="btn no-border" onclick="ModalInfo.open();">solicite mais<br><b>informações</b></a>
			</div>
		</div>
	</nav>

	<!-- ************ INICIO ************ -->
	<section id="setor1" class="img-setor1">
		<div class="row">
			<div class="left">
				<img src="<?=asset_url()?>img/fwd.png" alt="FWD">
				<!--<ul>
					<li>LANÇAMENTO</li>
					<li>DESIGN RESIDENCE</li>
					<li>1 E 2 DORMS.</li>
				</ul>-->
			</div>

			<div class="full">
				
				<h2>SEU LUGAR. SEU TEMPO.</h2>

				<ul>
					<li class="f-img-um"><img src="<?=asset_url()?>img/inicio-img-01.jpg"></li>
					<li class="f-text-um">
						<p>
							A Nex apresenta o FWD.
							Um lugar para quem vive à frente das principais tendências.
							Para quem é movido por desafios e leva a vida com estilo próprio.
							Venha para o FWD e encontre o espaço perfeito para viver o seu tempo.
						</p>
						<!--<a href="#" class="btn">Faça um Tour Virtual pelo FWD</a>-->
					</li>
				</ul>

				<ul class="Exception">
					<li class="f-text-dois">
						<p>
							Ambientes pensados para o seu estilo de vida.
						</p>
						<!--<a href="#" class="btn">Assista ao video do FWD</a>-->
					</li>
					<li class="f-img-dois">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/svtEoAt6Gm8?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>				
					<!--<img src="<?=asset_url()?>img/inicio-img-02.jpg">-->
					</li>

				</ul>

			</div>
		</div>

	</section>
	<!-- ************ INICIO END ************ -->

	<!-- ************ AREAS DE LAZER ************ -->
	<section id="setor2" class="img-setor1">
		<div class="row">

			<h2>ÁREAS DE LAZER</h2>

			<div class="full content">
				<div id="galleria2">
					<a href="<?=asset_url()?>img/areas-lazer/01.jpg"><img src="<?=asset_url()?>img/areas-lazer/01.jpg" data-title="Party Lounge"></a>
					<a href="<?=asset_url()?>img/areas-lazer/02.jpg"><img src="<?=asset_url()?>img/areas-lazer/02.jpg" data-title="Espelho D'agua"></a>
					<a href="<?=asset_url()?>img/areas-lazer/03.jpg"><img src="<?=asset_url()?>img/areas-lazer/03.jpg" data-title="Apartamento Garden"></a>
					<a href="<?=asset_url()?>img/areas-lazer/04.jpg"><img src="<?=asset_url()?>img/areas-lazer/04.jpg" data-title="Coffee Lounge"></a>
					<a href="<?=asset_url()?>img/areas-lazer/05.jpg"><img src="<?=asset_url()?>img/areas-lazer/05.jpg" data-title="Infinity Pool"></a>
					<a href="<?=asset_url()?>img/areas-lazer/06.jpg"><img src="<?=asset_url()?>img/areas-lazer/06.jpg" data-title="Infinity Pool"></a>
					<a href="<?=asset_url()?>img/areas-lazer/07.jpg"><img src="<?=asset_url()?>img/areas-lazer/07.jpg" data-title="Acesso"></a>
					<a href="<?=asset_url()?>img/areas-lazer/08.jpg"><img src="<?=asset_url()?>img/areas-lazer/08.jpg" data-title="Terraço Cobertura"></a>
					<a href="<?=asset_url()?>img/areas-lazer/09.jpg"><img src="<?=asset_url()?>img/areas-lazer/09.jpg" data-title="Fachadas"></a>
					<a href="<?=asset_url()?>img/areas-lazer/10.jpg"><img src="<?=asset_url()?>img/areas-lazer/10.jpg" data-title="Green Fitness"></a>
					<a href="<?=asset_url()?>img/areas-lazer/11.jpg"><img src="<?=asset_url()?>img/areas-lazer/11.jpg" data-title="Working HUB"></a>
					<a href="<?=asset_url()?>img/areas-lazer/12.jpg"><img src="<?=asset_url()?>img/areas-lazer/12.jpg" data-title="Malt PUB"></a>
					<a href="<?=asset_url()?>img/areas-lazer/13.jpg"><img src="<?=asset_url()?>img/areas-lazer/13.jpg" data-title="Hall Concierge"></a>
					<a href="<?=asset_url()?>img/areas-lazer/14.jpg"><img src="<?=asset_url()?>img/areas-lazer/14.jpg" data-title="Smart Laundry"></a>
					<a href="<?=asset_url()?>img/areas-lazer/15.jpg"><img src="<?=asset_url()?>img/areas-lazer/15.jpg" data-title="Party Club"></a>
					<a href="<?=asset_url()?>img/areas-lazer/16.jpg"><img src="<?=asset_url()?>img/areas-lazer/16.jpg" data-title="W Gourmet"></a>
					<a href="<?=asset_url()?>img/areas-lazer/17.jpg"><img src="<?=asset_url()?>img/areas-lazer/17.jpg" data-title="M Gourmet"></a>
					<a href="<?=asset_url()?>img/areas-lazer/18.jpg"><img src="<?=asset_url()?>img/areas-lazer/18.jpg" data-title="Coffee Corner"></a>

				</div>
			</div>

		</div>
	</section>
	<!-- ************ AREAS DE LAZER END ************ -->


	<!-- ************ APARTAMENTOS ************ -->
	<section id="setor3" class="img-setor1">

		<div class="top">

			<div class="row"><h2>APARTAMENTOS</h2></div>

			<!--  <div id="ancora"></div>-->

			<div id="menu-secun" class="fixo">
				<div class="row">
					<a id="implantacao-click" >Implantação</a>
					<a id="1dormitorio-click" >1 Dormitório</a>
					<a id="2dormitorio-click" >2 Dormitórios</a>
					<a id="loft-click" >Loft</a>
					<a id="cobertura-click" class="no-border">cobertura</a>
				</div>
			</div>
		</div>

		<div class="row">

			<!-- === IMPLANTACAO === -->
			<div id="implantacao" class="full">
				<div class="galleria-one implan-height">
					<a href="<?=asset_url()?>img/apartamentos/implantacao.jpg"><img src="<?=asset_url()?>img/apartamentos/implantacao.jpg" data-title="Implantacao"></a>
				</div>

				<div class="box">
					<h3>Implantação</h3>
					<a href="#" class="btn icon-down">Fazer o download da<br>planta de implantação</a>
				</div>
			</div>

			<!-- === 1 DORMITORIO === -->
			<div id="1dormitorio" class="estru1">
				<div id="fotos" class="list_carousel left">
					<div class="gallerias">
						<a href="<?=asset_url()?>img/apartamentos/1-dorms-01.png"><img src="<?=asset_url()?>img/apartamentos/1-dorms-01.png" data-title="1 Dormitório"></a>
						<a href="<?=asset_url()?>img/apartamentos/1-dorms-02.png"><img src="<?=asset_url()?>img/apartamentos/1-dorms-02.png" data-title="1 Dormitório Studio"></a>
					</div>
				</div>

				<div class="box">
					<h3>1 Dormitório / Studio</h3><br>
					<a href="#" class="btn icon-down">Fazer o download das<br>plantas de 1 dormitório</a>
				</div>
			</div>

			<!-- === 2 DORMITORIO === -->
			<div id="2dormitorio" class="estru2">
				<div class="box">
					<h3>2 Dormitório / Opção Living</h3><br>
					<div style="float:right;">
						<a href="#" class="btn icon-down">Fazer o download das<br>plantas de 2 dormitórios</a>
					</div>
				</div>

				<div id="fotos" class="list_carousel left">
					<div class="gallerias">
						<a href="<?=asset_url()?>img/apartamentos/2-dorms-01.png"><img src="<?=asset_url()?>img/apartamentos/2-dorms-01.png" data-title="2 Dormitório"></a>
						<a href="<?=asset_url()?>img/apartamentos/2-dorms-02.png"><img src="<?=asset_url()?>img/apartamentos/2-dorms-02.png" data-title="2 Dormitório Opção Living"></a>
					</div>
				</div>
			</div>


			<!-- === LOFT === -->
			<div id="loft" class="estru1">
				<div id="fotos" class="list_carousel left">
					<div class="gallerias">
						<a href="<?=asset_url()?>img/apartamentos/loft-inferior.png"><img src="<?=asset_url()?>img/apartamentos/loft-inferior.png" data-title="Loft Inferior"></a>
						<a href="<?=asset_url()?>img/apartamentos/loft-superior.png"><img src="<?=asset_url()?>img/apartamentos/loft-superior.png" data-title="Loft Superior"></a>
					</div>
				</div>

				<div class="box">
					<h3>Loft Inferior / Superior</h3><br>
					<a href="#" class="btn icon-down">Fazer o download das<br>plantas de 1 dormitório</a>
				</div>
			</div>

			<!-- === COBERTURA === -->
			<div id="cobertura" class="estru2">
				<div class="box">
					<h3>Cobertura</h3><br>
					<div style="float:right;">
						<a href="#" class="btn icon-down">Fazer o download da<br>planta da Cobertura</a>
					</div>
				</div>

				<div id="fotos" class="list_carousel left">
					<div class="galleria-one">
						<a href="<?=asset_url()?>img/apartamentos/cobertura.png"><img src="<?=asset_url()?>img/apartamentos/cobertura.png" data-title="Cobertura"></a>
					</div>
				</div>
			</div>

		</div> <!-- .row; -->
	</section> <!-- #setor3; -->

	<!-- ************ APARTAMENTOS END ************ -->


	<!-- ************ DECORADOS ************ -->
	<section id="setor4" class="paral">
		<div class="row">
			
			<h2>APARTAMENTOS DECORADOS</h2>

			<div class="full content">
				<div id="galleria">
					<a class="fancybox-thumb" rel="fancybox-thumb" href="<?=asset_url()?>img/decorados/2dorms/01.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/01.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/02.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/02.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/03.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/03.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/04.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/04.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/05.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/05.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/06.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/06.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/07.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/07.jpg" data-title="2 Dormitórios"></a>
					<a href="<?=asset_url()?>img/decorados/2dorms/08.jpg"><img src="<?=asset_url()?>img/decorados/2dorms/08.jpg" data-title="2 Dormitórios"></a>

					<a href="<?=asset_url()?>img/decorados/loft/01.jpg"><img src="<?=asset_url()?>img/decorados/loft/01.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/02.jpg"><img src="<?=asset_url()?>img/decorados/loft/02.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/03.jpg"><img src="<?=asset_url()?>img/decorados/loft/03.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/04.jpg"><img src="<?=asset_url()?>img/decorados/loft/04.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/05.jpg"><img src="<?=asset_url()?>img/decorados/loft/05.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/06.jpg"><img src="<?=asset_url()?>img/decorados/loft/06.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/07.jpg"><img src="<?=asset_url()?>img/decorados/loft/07.jpg" data-title="Loft"></a>
					<a href="<?=asset_url()?>img/decorados/loft/08.jpg"><img src="<?=asset_url()?>img/decorados/loft/08.jpg" data-title="Loft"></a>
				</div>
			</div>

			<div class="full2">
				<h4>
					<b>Visite os decorados no espaço FWD</b><br>Av. Ipiranga, 8223 - TELEFONE: 3026.7594
				</h4>
				<a href="https://www.google.com/maps/dir//Av.+Ipiranga,+8223+-+Praia+de+Belas,+Porto+Alegre+-+RS,+90160-092,+Brasil/@-30.057996,-51.1543532,17z/data=!4m13!1m4!3m3!1s0x95199d6156b07ca3:0xfe3ea6fe797ef236!2sAv.+Ipiranga,+8223+-+Praia+de+Belas,+Porto+Alegre+-+RS,+90160-092,+Brasil!3b1!4m7!1m0!1m5!1m1!1s0x95199d6156b07ca3:0xfe3ea6fe797ef236!2m2!1d-51.1543532!2d-30.057996" class="btn" target="_blank">Como chegar</a>
			</div>

		</div> <!-- .row; -->
	</section> <!-- #setor4; -->
	
	<!-- ************ DECORADOS END ************ -->


	<!-- ************ LOCALIZACAO ************ -->
	<section id="setor5">
		<div class="row">
			<div class="full">
				<h2>EM UM ÚNICO ENDEREÇO, TODOS OS DESTINOS.</h2>

				<ul>
					<li class="f-text-um">
						<p>
							O tempo se tornou o nosso bem mais precioso, e viver pertinho de tudo é fundamental.
							<br><br> 
							No FWD Central Parque você tem fácil acesso a escolas, hospitais, espaços de
							lazer, supermercados, transporte público e shoppings.
						</p>
					</li>
					<li class="f-img-um"><img src="<?=asset_url()?>img/setor5-img-01.jpg"></li>
				</ul>

				<ul class="Exception">
					<li class="f-img-dois"><img src="<?=asset_url()?>img/setor5-img-02.jpg"></li>
					<li class="f-text-dois">
						<p>
							Hospital são lucas • <b>10 min</b><br>
							Pucrs • <b>10 min</b><br>
							Colégio farroupilha • <b>10 min</b><br>
							Moinhos de vento • <b>12 min</b><br>
							Shopping iguatemi • <b>12 min</b><br>
							Aeroporto • <b>20 min</b><br>
							Brs 290 e 116 • <b>25 min</b><br>
						</p>
						<a href="#" class="btn">Como chegar</a>
					</li>
				</ul>

			</div> <!-- .full; -->
		</div> <!-- .row; -->
	</section> <!-- #setor5; -->

	<!-- ************ LOCALIZACAO END ************ -->


	<!-- ************ FOOTER ************ -->
	<footer>
		<div class="row">
			<a href="http://www.nexgroup.com.br/" title="Acesse Nex Group"><img src="<?=asset_url()?>img/fwd-nex.png" class="nex"></a>
			<p>Incorporação registrada no R-2 da matrícula 105.206 do Registro de Imóveis de Canoas-RS. Projeto arquitetônico: Ciro Clemik da Costa - CREA 41.124-D. Projeto paisagístico: Takeda Arquitetura e Paisagismo - CREA 79.320. A planta apresentada é ilustrativa e possui sugestão de decoração. Os móveis e utensílios são de dimensões comerciais e não fazem parte do contrato de compra e venda do imóvel. Os acabamentos serão entregues conforme memorial descritivo. Imagens meramente ilustrativas. Incorporação e construção: Nex Group.</p>
			<a href="http://imobi.divex.com.br/" title="Acesse Divex Imobi"><img src="<?=asset_url()?>img/divex.png" class="divex"></a>
		</div>
	</footer>
	<!-- ************ FOOTER END ************ -->


	<!-- ************ MODAL CONTATO ************  -->

	<section id="area_contato" style="display:none !important">
		<div class="black"></div>
		<div class="row">
			<div class="contato-box">
				<span class="btn close" onclick="ModalInfo.close();">X</span>
				
				<h2>SOLICITE MAIS INFORMAÇÕES</h2>

				<form id="formSoliciteInfo" name="formSoliciteInfo" method="post" onsubmit="return false;">
					<?php
					$url = current_url();
					if($_SERVER['QUERY_STRING']) { $url .= '?' . $_SERVER['QUERY_STRING']; }

					if($this->agent->referrer()) { $url = $this->agent->referrer(); }
					?>
					<input type="hidden" name="fwd_refer" value="<?=$url?>" />

					<ul class="left">
						<label>Nome:</label>
						<input type="text" name="fwd_nome" class="field" />
					</ul>

					<ul class="right">
						<label>Telefone:</label>
						<input type="text" name="fwd_telefone" class="field" />
					</ul>

					<ul class="full">
						<label>E-mail:</label>
						<input type="text" name="fwd_email" class="field" />
					</ul>
					
					<ul class="full">
						<label>Mensagem:</label>
						<textarea id="fwd_msg" class="field"></textarea>
					</ul>
				</form> 

				<h5>Preencha todos os campos e clique em enviar para solicitar mais informações, obrigado!
					<input type="submit" class="btn" value="ENVIAR" onclick="ModalInfo.send();" />
				</h5>	

			</div>
		</div> <!-- .row; -->
	</section> <!-- #area_contato; -->

	<!-- ************ MODAL CONTATO END ************ -->


	<!-- ************ MENSAGEM DE SUCESSO ************ -->
	<section id="mensagem-sucesso" style="display:none !important">
		<div onClick="closePromo();" class="btn-fechar">X</div>
		<div class="mensagem-box"><h2>Sua mensagem foi <b>enviada com sucesso!</b></h2></div>
	</section>
	<!-- ************ MENSAGEM DE SUCESSO END ************ -->

	<script type="text/javascript" src="<?=asset_url()?>classic/galleria-1.4.2.min.js"></script>

	<script type="text/javascript" src="<?=asset_url()?>js/jquery.carouFredSel-6.2.1-packed.js"></script>

	<script type="text/javascript" src="<?=asset_url()?>js/jquery.maskedinput.min.js" ></script>

	<script type="text/javascript" src="<?=asset_url()?>js/init.js"></script>

</body>
</html>








