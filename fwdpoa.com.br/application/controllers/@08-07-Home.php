<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function index()
	{
		$this->load->view('home');
	}

	/**
	* Function soliciteInfoXHR()
	*
	* Envia formulário de interesse (Modal Solicite Mais Informações)
	* Registra novo interesse no banco (tabela interesse)
	*
	* @return (json) ($response)
	*/
	function soliciteInfoXHR()
	{
		$origem = $this->agent->referrer();

		if(( ! $this->agent->is_referral() || $origem != base_url()) && ! $this->input->is_ajax_request())
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}

		if($this->agent->is_robot())
		{
			exit; // robot escape!
		}

		header('Content-type: application/json');

		//  Validação do dados
		//===================================================================

		$this->load->helper('email');

		if($this->input->post('nome') == "")
		{
			echo json_encode(array("erro" => 1));
			exit;
		}

		if($this->input->post('telefone') == "" || $this->input->post('telefone') == "(__) ____-_____")
		{
			echo json_encode(array("erro" => 3));
			exit;
		}

		if(!valid_email($this->input->post('email')))
		{
			echo json_encode(array("erro" => 2));
			exit;
		}

		//	Fim da validação
		//===================================================================
		//	Prepara os dados

		/*
		|--------------------------------------------------------------------------
		| URL de Origem
		|--------------------------------------------------------------------------
		*/

		// if($origem)
		// {
		// 	$this->load->library('utilidades');

		// 	if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
		// 	{
		// 		$origem = $this->input->post('refer'); 
		// 	}
		// }
		// else
		// {
		// 	$origem = $this->input->post('refer');
		// }

			$origem = $this->input->post('refer');

		/*
		|--------------------------------------------------------------------------
		| URL de Campanha
		|--------------------------------------------------------------------------
		*/	

		$ex_url = explode("?", $origem);

		parse_str(@$ex_url[1], $output);

		$url  = 	   @$output['utm_source'];
		$url .= "__" . @$output['utm_medium'];
		$url .= "__" . @$output['utm_content'];
		$url .= "__" . @$output['utm_campaign'];

		if($url == "______" || $url == "")
		{
			$url 	= $this->input->post('refer');
			$origem = $this->agent->referrer();
		}

		//	Fim da preparação
		//===================================================================
		//	Efetua o registro na base

		$data = array();
		$data['data_envio']   		= date('Y-m-d H:i:s');
		$data['id_empreendimento']	= 104;
		$data['nome']				= $this->input->post('nome');
		$data['email']				= $this->input->post('email');
		$data['telefone']			= $this->input->post('telefone');
		$data['forma_contato']		= 'Email';
		$data['comentarios']		= $this->input->post('mensagem');
		$data['origem']				= $origem;
		$data['url']				= $url;
		$data['ip']					= $this->input->ip_address();
		$data['user_agent']			= $this->input->user_agent();

		
		$this->db->set($data)->insert('interesses');

		//  Inicia o envio do email para nex
  		//===================================================================

		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset']  = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['newline'] = "\r\n";

		$this->email->initialize($config);

		//  Fim do cadastro de interesse na database
		//===================================================================
		//
		//  Mensagem do Email

      	ob_start();

  		?>
		<html>
		<head>
			<title>FWD</title>
		</head>
		<body>
			<table>
				<tr>
					<td colspan="2">
						<img width="120px" border="0" alt="FWD" src="http://www.fwdpoa.com.br/assets/img/logo2.png" />
					</td>
				</tr>
				<tr>
					<td colspan='2'>Enviado em <?php echo date("d/m/Y") . " ás " . date("H:i:s"); ?></td>
				</tr>
				<tr>
					<td colspan='2'>&nbsp;</td>
				</tr>
				<tr>
					<td>Nome:</td>
					<td><?=$data['nome']?></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><?=$data['email']?></td>
				</tr>
				<tr>
					<td>Telefone:</td>
					<td><?=$data['telefone']?></td>
				</tr>
				<?php if($url != "______") : ?>
				<tr>
					<td>URL / Campanha:</td>
					<td><?=$data['url']?></td>
				</tr>
				<?php endif; ?>
				<tr>
					<td>Origem:</td>
					<td><?=$data['origem']?></td>
				</tr>
				<?php if($data['comentarios'] != "") : ?>
				<tr>
					<td colspan="2"><br/>Mensagem:<br/><?=nl2br($data['comentarios'])?></td>
				</tr>
				<?php endif; ?>
				<tr>
					<td colspan='2'>&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2'>&nbsp;</td>
				</tr>
			</table>
		</body>
		</html>
		<?

		$conteudo = ob_get_contents();
		ob_end_clean();

		// Fim da Mensagem

		// Recepientes

		$this->email->from("noreply@fwdpoa.com.br", "FWD");

		$this->email->to('andre.a@nexvendas.com.br');
		$this->email->bcc('gabriel.oribes@divex.com.br, ricardo@divex.com.br, andre.wille@divex.com.br');
		
		// $this->email->to('mail.teste@divex.com.br');
		// $this->email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br');

		$this->email->subject('FWD - Novo Lead');
		$this->email->message("$conteudo");

		$this->email->send();


		//  Termina o envio do email  
		//===================================================================


		//  Inicia o envio do email para o lead
		//===================================================================
		//
		//  Mensagem do Email

		ob_start();

		?>
		<html>
		<head>
			<title>FWD</title>
		</head>
		<body >
			<table style="background:#f1f1f1; padding:4em 2em 2em 2em; border-radius:7px; margin:auto; color:#666">
				<tr>
					<td colspan="2">
						<img width="550px" border="0" alt="FWD" src="http://www.fwdpoa.com.br/assets/img/logo2.png" />
					</td>
				</tr>
				<tr>
					<td colspan='2' style="font-family: tahoma; margin: 0 0 0 30px; line-height:1.5; font-size: 1.5em; width: 550px; text-align: center; padding:0.5em 0 2em 0">
						<b style="color:#000">OBRIGADO,</b> <?=$data['nome']?><br/>
						Aguarde. Em breve você terá novidades!
					</td>
				</tr>
			</table>
		</body>
		</html>
		<?

		$conteudo = ob_get_contents();
		ob_end_clean();

		// Fim da Mensagem

		// Recepientes

		$this->email->clear();

		$this->email->from("noreply@fwdpoa.com.br", "FWD");

		$this->email->to($data['email']);

		$this->email->subject('FWD - Cadastro registrado');
		$this->email->message("$conteudo");


		$this->email->send();


		//  Termina o envio do email  
		//===================================================================
		//


		$response = array('erro' => 0); // Success

		echo json_encode($response);
		exit;

	} // soliciteInfoXHR();


	function download()
	{
		$files = explode(',', $this->input->post('files'));

		if(count($files) > 0)
		{
			// $this->load->helper('download');

			foreach($files as $f)
			{
				$file = trim($f);
				if(file_exists('assets/img/apartamentos/' . $file))
				{
					// $data = file_get_contents('assets/img/apartamentos/'. $file);
					
					// force_download('assets/img/apartamentos/'. $file, $data);

					$quoted = sprintf('"%s"', addcslashes(basename('assets/img/apartamentos/' . $file), '"\\'));
					$size   = filesize('assets/img/apartamentos/' . $file);

					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=' . $quoted); 
					header('Content-Transfer-Encoding: binary');
					header('Connection: Keep-Alive');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . $size);
				}
			}		
		}

		exit;
	}


}