$(document).ready(function()
{



    // Telefone mask
    $("input[name=fwd_telefone]", "#formSoliciteInfo").mask("(99) 9999-9999?9");

    //  -------- MENU CLICKs -------- //

    $("#serto1-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#setor1").offset().top
        }, 1000);
    });
    $("#serto2-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#setor2").offset().top
        }, 1000);
    });
    $("#serto3-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#setor3").offset().top
        }, 1000);
    });
    $("#serto4-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#setor4").offset().top
        }, 1000);
    });
    $("#serto5-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#setor5").offset().top
        }, 1000);
    });


    //  -------- APARTAMENTOS CLICKs -------- //

    $("#implantacao-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#implantacao").offset().top
        }, 1000);
    });
    $("#1dormitorio-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#1dormitorio").offset().top
        }, 1000);
    });
    $("#2dormitorio-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#2dormitorio").offset().top
        }, 1000);
    });
    $("#loft-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#loft").offset().top
        }, 1000);
    });
    $("#cobertura-click").click(function() {
        $('html, body').animate({
            scrollTop: $("#cobertura").offset().top
        }, 1000);
    });


//  -------- ROLAGEM NO CLICK -------- //
 // $('.clickroll').localScroll({duration: 700, queue: true});



//  -------- HEADER FIXO -------- //

 //   var navigations = $('#ancora');
//    pos = navigations.offset();

//    $(window).scroll(function() {
 //       if ($(this).scrollTop() > pos.top + navigations.height()) {
//            $('#menu-secun').removeClass('absoluto');
//            $('#menu-secun').addClass('fixo animated fadeInDown');
//        } else if ($(this).scrollTop() <= pos.top) {
//            $('#menu-secun').addClass('absoluto');
//            $('#menu-secun').removeClass('fixo animated fadeInDown');
 //       }
//    });





    //  -------- HEADER MENU MOBILE -------- //

    $(".abrir").click(function() {
      var displayMenu = $("#menu").css('display');
      if(displayMenu == "none"){
        $("#menu").animate({
          height: "show", opacity: "toggle"
        }, { duration: "fast" });
      }else{
        if($(".menu_phone").css("display") == "block"){
          $("#menu").animate({
            height: "hide", opacity: "toggle"
          }, { duration: "fast" });
        }
      }
    });


    //  -------- MODAL -------- //

    $(".click_contato").click(function() {

        var displayMenu = $("#area_contato").css('display');

        if (displayMenu == "none")
        {
            $("#area_contato").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#area_contato").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });


    //  -------- GALLERY CLASSIC -------- //

    // Load the classic theme
    Galleria.loadTheme('assets/classic/galleria.classic.js');

    // Initialize Galleria
    Galleria.run('#galleria');
    Galleria.run('#galleria2');
    Galleria.run('.gallerias');

//    Galleria.run('.galleria-one', {
 //       thumbnails: false,
//        showInfo: false,
//        showImagenav: false
//    });

    Galleria.configure({
      //  lightbox: true,
        autoplay: 4000,
        imageCrop: false,
        fullscreenDoubleTap: true
    })

    Galleria.ready(function() {
        var gallery = this;
        this.addElement('fscr');
        this.appendChild('stage','fscr');
        var fscr = this.$('fscr')
            .click(function() {
                gallery.toggleFullscreen();
            });
        this.addIdleState(this.get('fscr'), { opacity:0 });
    });

});




var ModalInfo = {
        form: $('#formSoliciteInfo'),
        nome: '',
        email: '',
        telefone: '',
        mensagem: '',
        refer: '',
        getFields: function()
        {
            this.nome     = $('input[name=fwd_nome]', this.form);
            this.email    = $('input[name=fwd_email]', this.form);
            this.telefone = $('input[name=fwd_telefone]', this.form);
            this.mensagem = $('#fwd_msg', this.form);
            this.refer    = $('input[name=fwd_refer]', this.form);
        },
        validation: function()
        {
            if(this.nome.val() == "")
            {   
                this.showMassage("Nome inválido! Por favor, informe seu NOME corretamente."); 
                this.nome.focus();
                return false;
            }

            if(this.telefone.val() == "" || this.telefone.val() == "(__) ____-_____")
            {   
                this.showMassage("Telefone inválido! Por favor, informe seu TELEFONE corretamente."); 
                this.telefone.focus();
                return false;
            }

            var rxEmail = /^.+@.+\..{2,}$/;
            if(!rxEmail.test(this.email.val()))
            {
                this.showMassage("Email inválido! Por favor, informe seu EMAIL corretamente."); 
                this.email.focus();
                return false;
            }

            return true;
        },
        showMassage: function(text)
        {
            alert(text);
        },
        btnText: function(text)
        {
            $('.btn', this.form).text(text);
        },
        send: function()
        {
            this.btnText('ENVIANDO...');

            this.getFields();

            if(!this.validation())
            {
                this.btnText('ENVIAR');
                return false;
            }

            $.ajax({
                type: "POST",
                url: BASE_URL+"home/soliciteInfoXHR",
                data: {
                    nome:     ModalInfo.nome.val(),
                    email:    ModalInfo.email.val(),
                    telefone: ModalInfo.telefone.val(),
                    mensagem: ModalInfo.mensagem.val(),
                    refer:    ModalInfo.refer.val()
                },
                dataType: "json",
                success: function(response)
                {
                    ModalInfo.btnText('ENVIAR');

                    if(response.erro == 1)
                    {
                        ModalInfo.showMassage("Por favor, preencha o campo NOME corretamente."); 
                        ModalInfo.nome.focus();
                        return false;
                    }

                    if(response.erro == 2)
                    {
                        ModalInfo.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente.");  
                        ModalInfo.email.focus();
                        return false;
                    }

                    if(response.erro == 3)
                    {
                        ModalInfo.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
                        ModalInfo.telefone.focus();
                        return false;
                    }

                    if(response.erro == 0 || response.erro == 101)
                    {
                        $('#mensagem-sucesso').animate({opacity: "toggle"}, {duration: 400});
                        ModalInfo.close();
                        ModalInfo.form.get(0).reset();
                        return true;
                    }
                }
            });
        },
        open: function()
        {
            $("#area_contato").animate({height: "show", opacity: "toggle"}, {duration: "fast"});
        },
        close: function()
        {
            $("#area_contato").animate({height: "hide", opacity: "toggle"}, {duration: "fast"});
        }
    } // object

    function openPromo()  { $('#mensagem-sucesso').animate({opacity: "toggle"}, {duration: 400}); }
    function closePromo() { $('#mensagem-sucesso').animate({opacity: "toggle"}, {duration: 400}); }