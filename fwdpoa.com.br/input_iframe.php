<?php $base_url   = "http://".$_SERVER['SERVER_NAME']."/"; ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome = 1, requiresActiveX = true"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  
  <title>FWD</title>
  
  <meta name="description" content="FWD POA." />
  <meta name="keywords" content="FWD, poa, porto alegre" />
  
  <!-- <link rel="shortcut icon" href="favicon.ico"> -->
  <script src="<?=$base_url?>assets/js/prototype.js"></script>
  
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' />
  
  <link rel="stylesheet" type="text/css" href="<?=$base_url?>assets/css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="<?=$base_url?>assets/css/platinum-demo.css" />
  <link rel="stylesheet" type="text/css" href="<?=$base_url?>assets/css/platinum-component.css" />
  
  <script src="<?=$base_url?>/assets/js/modernizr.custom.js"></script>
</head>
<body style="background: none;">
  <div class="container">    
    <section style="width: 100%; margin-left: 0;">
      <h2 style=" text-align: left; margin-top: -12px;">Informe-se:</h2>
      <form id="theForm" class="simform" autocomplete="off">
        <input type="hidden" id="qcamp" name="url_campanha" value="<?=@$url_campanha?>" />
        <div class="simform-inner">
          <ol class="questions">
            <li>
              <!-- <span><label for="q1">Digite seu nome:</label></span> -->
              <input id="q1" name="q1" type="text" data-set="name" placeholder="Digite seu nome:" />
            </li>
            <li>
              <!-- <span><label id="label-name" for="q2">digite seu e-mail:</label></span> -->
              <input id="q2" name="q2" type="text" data-validate="email" placeholder="Digite seu e-mail:"/>
            </li>
          </ol>
          
          <button class="submit" type="submit">Send answers</button>
          
          <div class="controls">
            <button class="next"></button>
            <div class="progress"></div>
            <span class="number">
              <span class="number-current"></span>
              <span class="number-total"></span>
            </span>
            <span class="error-message"></span>
          </div>
        </div>

        <span class="final-message"></span>

      </form>     
    </section>
  </div>

  <script src="<?=$base_url?>assets/js/classie.js"></script>
  <script src="<?=$base_url?>assets/js/stepsForm.js"></script>
  <script src="<?=$base_url?>assets/js/placeholders.min.js"></script>
  
  <script>
    var theForm = document.getElementById('theForm');

    new stepsForm(theForm, 
    {
      onSubmit : function(form) {

        var name = theForm.querySelector("#q1");
        var mail = theForm.querySelector("#q2");
        var camp = theForm.querySelector("#qcamp");

        var base_url = "<?=$base_url;?>" + "home/sendLead";

        new Ajax.Request(base_url, {
          method:'post',
          parameters: {nome: name.value, email: mail.value, campanha: camp.value},
          requestHeaders: {Accept: 'application/json'},
          onSuccess: function(transport) {
            var response = transport.responseText || "no response text";
            
            console.log(response);

            classie.addClass(theForm.querySelector('.simform-inner'), 'hide');

            var messageEl = theForm.querySelector('.final-message');

            messageEl.innerHTML = '<b>Obrigado!</b><br>Em breve entraremos em contato.';
            classie.addClass(messageEl, 'show');
          }
        });
        
      }
    });
  </script>
</body>
</html>