//SCRIPT ABRE POPUP
function abrirpopup(pagina,largura,altura) {

var esquerda = (screen.width - largura)/2;
var topo = (screen.height - altura)/2;
   
//abre a nova janela, j� com a sua devida posi��o
window.open(pagina,'','status=yes,height=' + altura + ', width=' + largura + ', top='+topo+', left='+esquerda+''); 
}


//SCRIPT ABRE IMAGEM

var winpopup=null;

function AbreJanelaIMG(imgname, prwidth, prheight, legenda) 
{ 
  var strcode = 'PopImg.asp?img=' + imgname + '&legenda=' + legenda;
  var auxstr;

  auxstr  = 'width=' + prwidth;
  auxstr  = auxstr + ',height=' + prheight;
  auxstr  = auxstr + ',top=10,left=10,scrollbars=1,resizable=yes';

  if (winpopup != null) 
  {
    winpopup.close();
  }
  winpopup = window.open(strcode,'', auxstr);
}



//SCRIPT LIMPA INPUT PREENCHIDO
<!--
function limpainput(obj) {
	obj.value="";
}
//-->

// SCRIPTS MM
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

	function ValidaIndicacao()
	{
		var strErro = 'Verifique o(s) seguinte(s) erro(s):\n\n';
		var erro = false;
		var form = document.frmOcorrSmart;
		
		if(form.txtDestinatario.value == '')
		{
			strErro += '* Preencha o nome do destinat�rio\n';
			erro = true;
		}
		if(form.txtRemetente.value == '')
		{
			strErro += '* Preencha seu nome\n';
			erro = true;
		}
		
		if(form.txtMailDestinatario.value == '')
		{
			strErro += '* Preencha o e-mail do destinat�rio\n';
			erro = true;
		}
		else
		{
			if(isMail('frmOcorrSmart','txtMailDestinatario') == false)
			{
				strErro += '* Preencha corretamente o e-mail do destinat�rio\n';
				erro = true;
			}
		}
		if(form.txtMailRemetente.value == '')
		{
			strErro += '* Preencha seu e-mail\n';
			erro = true ;
		}
		else
		{
			if(isMail('frmOcorrSmart','txtMailRemetente') == false)
			{
				strErro += '* Preencha corretamente o seu e-mail\n';
				erro = true;
			}
		}
		if(erro)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}	
	}
	function ValidaMauiInteresse()
	{
		var strErro = 'Verifique o(s) seguinte(s) erro(s):\n\n';
		var erro = false;
		var form = document.frmMauiInteresse;
		
		if(form.txtNome.value == '')
		{
			strErro += '* Preencha seu nome\n';
			erro = true;
		}
		
		if(form.txtEmail.value == '')
		{
			strErro += '* Preencha seu e-mail \n';
			erro = true;
		}
		else
		{
			if(isMail('frmMauiInteresse','txtEmail') == false)
			{
				strErro += '* Preencha corretamente o e-mail \n';
				erro = true;
			}
		}
		
		if(erro)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}	
	}
	
	function ValidaAssistencia()
	{
		var strErro = 'Verifique o(s) seguinte(s) erro(s):\n\n';
		var erro = false;
		var form = document.frmAssistencia;
		
		if(form.txtUnidade.value == '')
		{
			strErro += '* Informe o n� do seu Apto!\n';
			erro = true;
		}
		if(form.txtProprietario.value == '')
		{
			strErro += '* Informe o propriet�rio\n';
			erro = true;
		}
		
		if(form.txtSolicitante.value == '')
		{
			strErro += '* Informe o solictante\n';
			erro = true;
		}
		
		if(form.txtEmail.value == '')
		{
			strErro += '* Preencha o e-mail\n';
			erro = true;
		}
		else
		{
			if(isMail('frmAssistencia','txtEmail') == false)
			{
				strErro += '* Preencha corretamente o e-mail \n';
				erro = true;
			}
		}
		if(form.txtTelefone.value == '')
		{
			strErro += '* Informe o telefone para contato\n';
			erro = true ;
		}
		if(form.txtSolicitacao.value == '')
		{
			strErro += '* Preencha sua solicita��o\n';
			erro = true ;
		}
		
		if(erro)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}	
	}
	
	function ValidaTerreno()
	{
		var strErro = 'Verifique o(s) seguinte(s) erro(s):\n\n';
		var erro = false;
		var form = document.frmTerreno;
		
		if(form.txtNome.value == '')
		{
			strErro += '* Informe seu nome!\n';
			erro = true;
		}
		
		if(form.txtTelefone.value == '')
		{
			strErro += '* Informe seu telefone ou celular\n';
			erro = true;
		}
		
		if(form.txtEmail.value == '')
		{
			strErro += '* Preencha o e-mail\n';
			erro = true;
		}
		else
		{
			if(isMail('frmTerreno','txtEmail') == false)
			{
				strErro += '* Preencha corretamente o e-mail \n';
				erro = true;
			}
		}
		
		if(erro)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}	
	}
	
	
	
	function ValidaContato()
	{
		var strErro = "Verifique o(s) seguinte(s) erro(s):\n\n";
		var erro = false;
		var form = eval("document.frmContato");
		
		if(form.txtNome.value == "")
		{
			strErro += '* Preencha seu NOME\n';
			erro = true;
		}
		if(form.txtEmail.value == "")
		{	strErro += '* Preencha seu E-MAIL\n';
			erro = true;
		}
		else
		{
			if(isMail('frmContato','txtEmail') == false)
			{
				strErro += '* O E-MAIL est� num formato inv�lido!\n';
				erro = true;
			}
		}
		if(form.txtMsg.value == "")
		{
			strErro += '* Preencha a MENSAGEM\n';
			erro = true;
		}
		if(erro == true)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function ValidaForm()
	{
		var strErro = "Verifique o(s) seguinte(s) erro(s):\n\n";
		var erro = false;
		var form = eval("document.frmNovidades");
		
		if(form.txtNome.value == "")
		{
			strErro += '* Preencha seu NOME\n';
			erro = true;
		}
		if(form.txtEmail.value == "")
		{	strErro += '* Preencha seu E-MAIL\n';
			erro = true;
		}
		else
		{
			if(isMail('frmNovidades','txtEmail') == false)
			{
				strErro += '* O E-MAIL est� num formato inv�lido!\n';
				erro = true;
			}
		}		
		if(erro == true)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function ValidaUpload()
	{
		var strErro = "Verifique o(s) seguinte(s) erro(s):\n\n";
		var erro = false;
		var form = document.frmEnvioFoto;
		
		if(form.file.value == "")
		{
			strErro += "* Selecione uma foto\n";
			erro = true;
		}
		if(form.txtLegenda.value == "")
		{
			strErro += '* Preencha uma legenda para a foto\n';
			erro = true;
		}		
		if(form.selCategoriaFoto.value == "")
		{
			strErro += '* Voc� precisa selecionar uma categoria para a foto\n ou ent�o voc� j� inseriu a quantidade de fotos permitida\n';
			erro = true;
		}
		if(form.txtLocal.value == "")
		{
			strErro += '* Descreve em que local foi feita a foto\n';
			erro = true;
		}
		if(erro == true)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function ValidaInteresse()
	{
		var strErro = "Verifique o(s) seguinte(s) erro(s):\n\n";
		var erro = false;
		var form = eval("document.frmInteresse");
		
		if(form.txtNome.value == "")
		{
			strErro += '*Preencha seu NOME\n';
			erro = true;			
		}
		if(form.selIdade.value == "")
		{
			strErro += '*Selecione sua IDADE\n';
			erro = true;			
		}
		if(form.selEstadoCivil.value == "")
		{
			strErro += '*Selecione seu ESTADO CIVIL\n';
			erro = true;			
		}
		if(form.selBairro.value == "Indique o seu bairro")
		{
			strErro += '*Selecione seu BAIRRO\n';
			erro = true;			
		}
		if(form.txtProfissao.value == "")
		{
			strErro += '*Preencha sua PROFISS�O\n';
			erro = true;			
		}
		if(form.txtTelefone.value == "")
		{
			strErro += '*Preencha seu TELEFONE\n';
			erro = true;			
		}
		if(form.txtEmail.value == "")
		{
			strErro += '*Preencha seu E-MAIL\n';
			erro = true;			
		}
		else
		{
			if(isMail('frmInteresse', 'txtEmail') == false)
			{
				strErro += '*Seu E-MAIL est� num formato inv�lido\n';
				erro = true;
			}
		}
		if(form.chkTelefone.checked == false && form.chkEmail.checked == false)
		{
			strErro += '*Selecione pelo menos UMA forma de contato\n';
			erro = true;
		}		
		if(form.txtComentarios.value == "")
		{
			strErro += '*Preecha seu COMENT�RIO\n';
			erro = true;
		}
		if(erro == true)
		{
			alert(strErro);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function ValidaCorretor()
	{
		var strErro = "Verifique o(s) seguinte(s) erro(s):\n\n";
		var erro = false;
		var form = eval("document.frmCorretor");
		
		if(form.txtIdentificador.value == "")
		{
			strErro += '* Preencha seu IDENTIFICADOR\n';
			erro = true;
		}
		if(form.txtEmail.value == "")
		{	strErro += '* Preencha seu E-MAIL\n';
			erro = true;
		}
		else
		{
			if(isMail('frmCorretor','txtEmail') == false)
			{
				strErro += '* O E-MAIL est� num formato inv�lido!\n';
				erro = true;
			}
		}		
		if(erro == true)
		{
			alert(strErro);
			return false;
		}
		else
		{			
			window.open('http://www.aspserver.divex.com.br/smartweb/bitpress/PainelDeControle/SmartSuport/chat_valor/ProcEntradaUser.asp?cod_empresa=28&txtIdentificador=' + form.txtIdentificador.value +'&txtEmail=' + form.txtEmail.value ,'suporte','width=431,height=430');
		}
	}

	function validaCadastroConcurso()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmCadastra;

		if(form.txtNome.value == "")
		{
			erro = true;
			msg += "\nNome";
		}
		if(form.txtSobreNome.value == "")
		{
			erro = true;
			msg += "\nSobrenome";
		}
		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmCadastra', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(form.txtUsuario.value == "")
		{
			erro = true;
			msg += "\nUsu�rio";
		}
		if(form.txtSenha.value == "")
		{
			erro = true;
			msg += "\nSenha";
		}
		if(form.txtSenha2.value == "")
		{
			erro = true;
			msg += "\nConfirma��o de Senha";
		}
		if(form.txtSenha.value != form.txtSenha2.value)
		{
			erro = true;
			msg += "\nSenhas informadas diferentes";
		}
		if(form.txtDtNasc.value == "")
		{
			erro = true;
			msg += "\nData nascimento";
		} else {
			if(!verificaData(form.txtDtNasc.value))
			{
				erro = true;
				msg += "\nData de nascimento inv�lida";
			}
		}
		if(form.txtFone.value == "")
		{
			erro = true;
			msg += "\nTelefone";
		} else {
			if(!verificaIsNum(form.txtFone.value))
			{
				erro = true;
				msg += "\nTelefone inv�lido";
			}
		}
		if(form.selSexo.value == "")
		{
			erro = true;
			msg += "\nSexo";
		}

		if(form.sozinho.checked == false && form.selNumPessoas.value == "")
		{
			erro = true;
			msg += "\nN�mero de pessoas";
		}

		if(erro)
		{
			alert(msg);
			return false;
		} else {
			return true;
		}
	}
	function validaEdicaoConcurso()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmEdicao;

		if(form.txtNome.value == "")
		{
			erro = true;
			msg += "\nNome";
		}
		if(form.txtSobreNome.value == "")
		{
			erro = true;
			msg += "\nSobrenome";
		}
		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmEdicao', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(form.txtSenha.value == "")
		{
			erro = true;
			msg += "\nSenha";
		}
		if(form.txtSenha2.value == "")
		{
			erro = true;
			msg += "\nConfirma��o de Senha";
		}
		if(form.txtSenha.value != form.txtSenha2.value)
		{
			erro = true;
			msg += "\nSenhas informadas diferentes";
		}
		if(form.txtDtNasc.value == "")
		{
			erro = true;
			msg += "\nData nascimento";
		} else {
			if(!verificaData(form.txtDtNasc.value))
			{
				erro = true;
				msg += "\nData de nascimento inv�lida";
			}
		}
		if(form.txtFone.value == "")
		{
			erro = true;
			msg += "\nTelefone";
		} else {
			if(!verificaIsNum(form.txtFone.value))
			{
				erro = true;
				msg += "\nTelefone inv�lido";
			}
		}
		if(form.selSexo.value == "")
		{
			erro = true;
			msg += "\nSexo";
		}

		if(form.sozinho.checked == false && form.selNumPessoas.value == "")
		{
			erro = true;
			msg += "\nN�mero de pessoas";
		}

		if(erro)
		{
			alert(msg);
			return false;
		} else {
			return true;
		}
	}

	function validaLoginConcurso()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmLoginConcurso;

		if(form.txtUsuario.value == "")
		{
			erro = true;
			msg += "\nUsu�rio";
		}
		if(form.txtSenha.value == "")
		{
			erro = true;
			msg += "\nSenha";
		}

		if(erro)
		{
			alert(msg);
			return false;
		} else {
			return true;
		}
	}

	function validaEsqueceuSenha()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmEsqueceuSenha;

		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmEsqueceuSenha', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(erro)
		{
			alert(msg);
			return false;
		} else {
			return true;
		}
	}
	
	function ValidaVoto()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmVotar;
		
		if(form.codUsuarioConcurso.value == "")
		{
			erro = true;
			msg += "\nC�digo do usu�rio no concurso";
		}
		if(form.codFotoConcurso.value == "")
		{
			erro = true;
			msg += "\nC�digo da foto no concurso";
		}
		if(form.txtNome.value == "")
		{
			erro = true;
			msg += "\nNome completo";
		}
		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmVotar', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(erro) {
			alert(msg);
			return false;
		}
		return true;
	}
	
	function isMail(form, campo)
	{
		var re = new RegExp;
		strMail = eval('document.' + form +  '.' + campo + '.value');

		re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var arr = re.exec(strMail);
		if (arr == null)
		{    
	    	return false;
		}
		else
		{
    		return true;
		}
	}

	//Verifica uma data no padr�o:
	//00/00/0000 ou 0/0/00
	function verificaData(valor)
	{
		reDate = /^((0?[1-9]|[12]\d{1,2})\/(0?[1-9]|1[0-2])|30\/(0?[13-9]|1[0-2])|31\/(0?[13578]|1[02]))\/(19|20)?\d{2}$/;
		if (reDate.test(valor)) {
			return true;
		} else if (valor != null && valor != "") {
			return false;
		}
	}

	//Verifica se o valor � inteiramente num�rico
	function verificaIsNum(valor)
	{
		p = valor;
		p = p.replace("(", "")
		p = p.replace(")", "")
		p = p.replace("-", "")
		p = p.replace("-", "")

		if(isNaN(p) == true) {
			return false;
		} else {
			return true;
		}
	}

//FUN��ES DE PAGINA��O
function decrementa(nameForm, nameField)
{
	var form = eval("document." + nameForm);
	if(form == null)
	{
		alert("Fun��o decrementa n�o encontrou o formul�rio");
	} else {
		var field = eval("document." + nameForm + "." + nameField);
		if(field == null)
		{
			alert("Fun��o decrementa n�o encontrou o campo que cont�m a p�gina atual");
		} else {
			if(verificaIsNum(field.value))
			{
				if(field.value > 1)
				{
					field.value = parseInt(field.value) - 1;
					form.submit();
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
}
	
function incrementa(nameForm, nameField, totalPaginas)
{
	var form = eval("document." + nameForm);
	if(form == null)
	{
		alert("Fun��o incrementa n�o encontrou o formul�rio");
	} else {
		var field = eval("document." + nameForm + "." + nameField);
		if(field == null)
		{
			alert("Fun��o incrementa n�o encontrou o campo que cont�m a p�gina atual");
		} else {
			if( (verificaIsNum(field.value)) && (verificaIsNum(totalPaginas)) )
			{
				if(field.value < totalPaginas)
				{
					field.value = parseInt(field.value) + 1;
					form.submit();
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
}

function validaCadastroConcurso()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmCadastra;

		if(form.txtNome.value == "")
		{
			erro = true;
			msg += "\nNome";
		}
		if(form.txtSobreNome.value == "")
		{
			erro = true;
			msg += "\nSobrenome";
		}
		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmCadastra', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(form.txtUsuario.value=="")
		{
		erro=true;
		msg+="\nUsu�rio ";
		}
		//if (form.txtUsuario.value.< 6)
        //{
		//erro=true;
		//msg+="\nUsu�rio deve conter no minimo 6 caracteres";
		//}
		if(form.txtSenha.value == "")
		{
			erro = true;
			msg += "\nSenha";
		}
		if(form.txtSenha2.value == "")
		{
			erro = true;
			msg += "\nConfirma��o de Senha";
		}
		if(form.txtSenha.value != form.txtSenha2.value)
		{
			erro = true;
			msg += "\nSenhas informadas diferentes";
		}
		if(form.txtDtNasc.value == "")
		{
			erro = true;
			msg += "\nData nascimento";
		} else {
			if(!verificaData(form.txtDtNasc.value))
			{
				erro = true;
				msg += "\nData de nascimento inv�lida";
			}
		}
		if(form.txtFone.value == "")
		{
			erro = true;
			msg += "\nTelefone";
		} else {
			if(!verificaIsNum(form.txtFone.value))
			{
				erro = true;
				msg += "\nTelefone inv�lido";
			}
		}
		//if(form.selSexo.value == "")
	//	{
	//		erro = true;
	//		msg += "\nSexo";
	//	}

		//if(form.sozinho.checked == false && form.selNumPessoas.value == "")
		//{
			//erro = true;
			//msg += "\nN�mero de pessoas";
	//	}

		if(erro)
		{
			alert(msg);
			return false;
		} else {
			return true;
		}
	}


	function validaCadastroConcursoVotos()
	{
		var erro = false;
		var msg = "Preencha o(s) campo(s) abaixo:\n";
		var form = document.frmCadastroVotos;

		if(form.txtNome.value == "")
		{
			erro = true;
			msg += "\nNome";
	     }
	
		if(form.txtEmail.value == "")
		{
			erro = true;
			msg += "\nE-mail";
		} else {
			if(!isMail('frmCadastroVotos', 'txtEmail'))
			{
				erro = true;
				msg += "\nE-mail inv�lido";
			}
		}
		if(form.txtUsuario.value == "")
		{
			erro = true;
			msg += "\nUsu�rio";
		}
		if(form.txtSenha.value == "")
		{
			erro = true;
			msg += "\nSenha";
		}
		if(form.txtSenha2.value == "")
		{
			erro = true;
			msg += "\nConfirma��o de Senha";
		}
		if(form.txtSenha.value != form.txtSenha2.value)
		{
			erro = true;
			msg += "\nSenhas informadas diferentes";
		}
	 if (erro)
	 {
	 alert(msg);
	 return false;
	 }
	 else
	 return true;
	 
	}

//-->