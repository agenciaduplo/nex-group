<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<title>Projeto Engenharia Social</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="capa.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<style type="text/css">
<!--
.style10 {color: #999999}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="742" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="251"><a href="default.php"><img src="images/top_01.gif" alt="Projeto Engenharia Social" width="251" height="72" border="0"></a></td>
          <td>&nbsp;</td>
          <td width="126"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" alt="Capa Engenharia" width="126" height="72" border="0"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><img src="images/bannertopo.jpg" alt="N&atilde;o &eacute; por acaso que a cor da Capa Engenharia &eacute; o verde. Verde esperan&ccedil;a..." width="742" height="116"></td>
  </tr>
  <tr>
    <td> 
      <table width="742" height="19" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="A Capa Engenharia" WIDTH=129 HEIGHT=19 border="0"></a></td>
          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="O Projeto" WIDTH=80 HEIGHT=19 border="0"></a></td>
          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="Apoiadores" WIDTH=91 HEIGHT=19 border="0"></a></td>
          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" alt="Entidade do Ano" width="121" height="19" border="0"></a></td>
          <td width="83"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>
          <td bgcolor="#009933"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="289" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="15">&nbsp;</td>
                <td>&nbsp;</td>
                <td width="10">&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><strong><img src="images/not_1.gif" alt="Como nasceu a id&eacute;ia" width="233" height="18"></strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td> <div align="left"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Com um longo hist&oacute;rico 
                    de a&ccedil;&otilde;es sociais e culturais junto a entidades 
                    como Santa Casa, MARGS, Hospital Esp&iacute;rita e ONG&acute;s 
                    de diversos fins, a Capa resolveu aproveitar a comemorativa 
                    data de seu vig&eacute;simo anivers&aacute;rio em 2004 para lan&ccedil;ar 
                    um projeto de responsabilidade social de maior impacto perante 
                as demandas da sociedade.</font></div></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><img src="images/not_2.gif" alt="Objetivo" width="233" height="18"></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td> <div align="left"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">O
                      objetivo do  Engenharia Social &eacute; ampliar o impacto
                      de suas a&ccedil;&otilde;es 
                    sociais, atrav&eacute;s da formata&ccedil;&atilde;o de um
                     projeto de longo prazo o qual prev&ecirc; a participa&ccedil;&atilde;o
                    de colaboradores, parceiros e clientes da empresa, disseminando
                    a cultura da cidadania corporativa e oferecendo oportunidade
                    de realiza&ccedil;&atilde;o pessoal a todos participantes.</font></div></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><img src="images/not_3.gif" alt="Como funciona" width="233" height="18"></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td> <div align="left"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Tomada
                      esta decis�o, em conjunto com seus parceiros apoiadores � a
                      Capa elege, periodicamente, uma entidade com atividades
                      comprovadamente sociais, que atuem para colaborar na educa&ccedil;&atilde;o
                      infantil e com demandas de novas instala��es
                      ou de melhorias substanciais nas instala��es atuais. Aproveitando
                      seu know-how e sua imagem de idoneidade e admira��o em
                      toda cadeia produtiva, a Capa buscar� parceiros e se responsabilizar� pela
                      constru��o
                      do novo projeto</font></div></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><br /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><img src="images/home_01.gif" alt=" A GENTE AJUDA A CONSTRUIR UM FUTURO MELHOR." width="267" height="147" /></font></td>
                <td>&nbsp;</td>
              </tr>
          </table></td>
          <td width="267" valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="center"><table width="260" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>  <div align="center"><a href="entidade_ano.php"><img src="images/creche2009/creche_2010.gif" alt="Creche Comunit&aacute;ria S&atilde;o Guilherme" border="0" /></a></div></td>
                    </tr>
                  </table>
                  <br />
                  <table width="260" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"><a href="http://www.abrhrs.com.br/top2008/vence2008.php" target="_blank"><img src="images/premio_topcidadania08.gif" alt="Top Cidadania" width="181" height="111" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td> <div align="justify">A <span class="construtor">Capa Engenharia</span> recebeu no dia 1� de outubro, o pr�mio <strong>Top Cidadania ABRH-RS 2008</strong>.

                          <br />
                          <br />
                          O <strong>Top Cidadania</strong> premia institui��es sem fins lucrativos e empresas p�blicas ou privadas que desenvolvem projetos de investimento social privado que beneficiam comunidades externas � empresa. 

                          <br />
                          <br />
                          A <strong>Capa</strong> agradece a todos os clientes, fornecedores e parceiros que participaram desta conquista. </div>                        </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><br />
                      <img src="images/creches.gif" width="262" height="200" border="0" usemap="#Map2" /></td>
                    </tr>
                   
                  </table>                </td>
              </tr>
             
            </table></td>
          <td valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              
        <!--      
              
              <tr> 
                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" alt="Cadastre sua entidade" width="186" height="20"></p></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><img src="images/cadastre2.gif" width="186" height="73" border="0" usemap="#Map"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA" align="center"> </td>
              </tr>
              <tr> 
                <td height="1" bgcolor="#FFFFFF"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">
               </td>
              </tr>
              <tr> 
                <td width="186" valign="top" bgcolor="#C0E7BA" ><div style="padding:5%; font-size: 9;"> <font face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><a href="entidades.php">Aberta a sele��o de entidades para 2010.</a></font> </div></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" alt="Indique um amigo" width="186" height="20" /></td>
              </tr> 
              <tr> 
                <td bgcolor="#C0E7BA"><div style="padding:5%;"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&amp;CodProd=34&amp;CodItem=1&amp;url=http://www.projetoengenhariasocial.com.br" target="_blank"> Ajude essa id&eacute;ia a se propagar! 
                  Indique nosso site a uma pessoa amiga.</a><br>
                <br><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank">
                
                <img src="images/tit_indique.gif" alt="INDIQUE" width="79" height="16" border="0"></a></div></td>
              </tr>
              
              
              -->
              
              
              <tr> 
                <td bgcolor="#C0E7BA"><p><img src="images/novidades.gif" alt="Novidades" width="186" height="20"></p></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA" align="left"><div style="padding:5%;" >                  <a href="http://www.nexgroup.com.br/contato" target="_blank">Cadastre-se para receber novidades sobre o projeto.</a>                    <br>
                    <br>
                    <a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/tit_clique.gif" alt="CLIQUE AQUI" width="79" height="16" border="0" ></a></div></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA" align="center"> </td>
              </tr>
              <tr> 
                <td height="1" bgcolor="#FFFFFF"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td align="center" bgcolor="#C0E7BA"><a href="http://www.construtordaesperanca.com.br" target="_blank"><img src="images/icone_construtor.gif" alt="Construtor da Esperan&ccedil;a" width="71" height="100" border="0" /></a>&nbsp;&nbsp;&nbsp;<a href="http://www.capa.com.br" target="_blank"><img src="images/26anos.gif" width="60" height="46" border="0" /></a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td valign="top" bgcolor="#99CC99">&nbsp;</td>
          <td valign="top" bgcolor="#DEDEBD"><div align="right"><a href="http://www.divex.com.br/" target="_blank"><img src="images/bydivex.gif" alt="Produzido por DIVEX" width="70" height="18" border="0"></a></div></td>
        </tr>
        <tr> 
          <td height="3"></td>
          <td height="3" valign="top" bgcolor="#99CC99"></td>
          <td height="3" valign="top" bgcolor="#DEDEBD"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10">&nbsp;</td>
        <td><font color="#999999" size="1"><strong>Apoiadores do Projeto 2008<br />
        </strong></font></td>
        <td width="10">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td valign="top"><div align="justify" class="style10">Alusistem - Alum�nio para Constru��o Civil Ltda (Beneficiamento de alum�nio).Amanco - Brasil Ltda(Material hidr�ulico).Arcelor Mittal Brasil S/A(Tela + a�o radier) .Arte M�veis(Portas de madeira e guarni��es).Baumgarten Com�rcio & Instala��o em Sistema Drywall(M�o-de-obra forro de gesso) .Belmetal Ind�stria e Com�rcio Ltda(Alum�nio para esquadrias). Blumengarten Plantas e Flores Ltda	(Plantas e Flores) . Brocca Correa Ltda(M�o-de-obra radier) . Cecrisa Revestimentos Cer�micos Ltda	(Piso cer�mico e azulejos) .Cia Nacional do A�o Ind�stria e Com�rcio(Tela + a�o radier) . Cimatex materiais de constru��o Ltda (Cimento) . CCB- Cimpor Cimentos do Brasil Ltda (Concreto) . Creel-Sul Com�rcio de Equipamentos el�tricos Ltda (Material el�trico) . Deca	(Lou�as) . Design Decora��es Ltda (M�o-de-obra piso laminado) . 
Di Luce -Fonte de Luz Ilumina��o Ltda (Lumin�rias) . Docol Metais Sanit�rios Ltda (Metais) . Duratex S.A (Piso laminado) . Elektra Distribuidora (Material el�trico e lumin�rias externas) . Eletric-Sul com�rcio de materiais el�tricos Ltda (Material el�trico) . Empreiteira de obras Teixeira & Silva(M�o-de-obra pisos e azulejos cer�micos) . Esquadrias Pintz Ltda (Port�o de entrada ve�culos e port�o reservat�rio superior) . Est�dio 3 Engenharia de estuturas Ltda (Projeto Radier) . Faber Mobilis Industria de M�veis Ltda (Banco para deficiente) . Fechart Comercio de Fechaduras Ltda	(Fornecimento de fechaduras e dobradi�as) . Fibrafort Piscinas Ltda (Reservat�rios) . Giancarlo C�ndido (M�o-de-obra pintura ) . Golder Constru��es Ltda	(M�o-de-obra alvenaria, reboco e cobertura) . Hydrus Projetos de Instala��es Sanit�rias (Projeto Hidrossanit�rio) . Igua�u Pr�-fabricados de Granito Ltda	(Peitoris de Janelas, tampo granito cozinha e banheiros) . Inap -Ind�stria Nacional de A�o Pronto (Corte e dobra a�o radier) . Instaladora Base LTDA (Subs�dio m�o-de-obra el�trica) . Irm�os Ciocari Ltda (Argamassa de assentamento) . Knauf do Brasil Ltda (forro de gesso) . Ludwig Herrmann Ltda (Limpeza do terreno e aterro) . MDR-Com�rcio Ferragem e Materiais de Constru��o (Vasos sanit�rios, chuveiros e materiais hidr�ulicos) . Metal�rgica Borba Ind�stria e com�rcio Ltda (Caixilhos de piso, port�o para pedestres e port�o de g�s) . Metal�rgica Visconti Ltda (Material el�trico) . Milititsky Consultoria Geot�cnica� Engenheiros Assoc.S/S Ltda	(Estudo viabilidade projeto Radier) . Multicabos distribuidora de materiais el�tricos Ltda (Material el�trico) . Norte Pisos Ltda (Acabamento de piso de concreto, rampas e passeios) . Pauluzzi Produtos Cer�micos Ltda (Blocos cer�micos) . Plenobr�s Distribuidora el�trica Ltda (Material el�trico) .
Rampa Compensados Ltda	(Compensado) . Ricardo Lemos da Costa ME (Enleivamento e plantio de mudas e plantas) . Saint Gobain Brasilit Ltda (Telhas) . Sanidro Instala��es Hidr�ulicas Ltda (Subs�dio m�o-de-obra hidr�ulica) . Shine Service (M�o-de-obra limpeza) . Sia Com�rcio e Representa��o Ltda (Fornecimento de extintores) . Tintas Kresil Ltda (Tintas) . Vidra�aria Lajeadense Ltda	(Vidros) . Divex (desenvolvimento site)</div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="94,48,175,67" href="entidades.php">
</map>

<map name="Map2" id="Map2"><area shape="rect" coords="7,72,231,96" href="entidade_ano.php" />
<area shape="rect" coords="8,102,258,125" href="entidade_ano_2007.php" /><area shape="rect" coords="2,132,259,163" href="entidade_ano2005.php" /><area shape="rect" coords="7,170,203,196" href="entidade_ano2004.php" />
</map></body>
</html>
