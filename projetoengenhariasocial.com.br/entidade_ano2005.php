<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>

<title>Projeto Engenharia Social</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="capa.css" rel="stylesheet" type="text/css">

<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open(theURL,winName,features);

}

//-->

</script>

</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="742" border="0" cellspacing="0" cellpadding="0" style="margin:auto">

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>

          <td>&nbsp;</td>

          <td width="126"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="268" height="72" border="0"></a></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td><img src="images/imagem.gif" width="742" height="116"></td>

  </tr>

  <tr>

    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">

        <tr> 

          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>

          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>

          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>

          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>

          <td width="83"><a href="entidades.php"><IMG SRC="images/menu_05.gif" ALT="" WIDTH=83 HEIGHT=19 border="0"></a></td>

          <td width="121" bgcolor="#009933"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>

          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td width="10">&nbsp;</td>

                <td>&nbsp;</td>

                <td width="10">&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td height="13"><br></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td> <table width="75%" border="0" align="center">

                    <tr> 

                      <td><b>Um natal especial na Vila Gua&iacute;ba </b></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                    <tr> 

                      <td><!--<p><img src="images/creche2.jpg" width="200" height="117" align="right">S&atilde;o 25 crian&ccedil;as com menos de sete anos de idade. Diariamente, ganham cinco refei&ccedil;&otilde;es, brincam e aprendem as primeiras letras num pr&eacute;dio pequeno ao lado da Par&oacute;quia de Nossa Senhora de Assun&ccedil;&atilde;o. S&atilde;o todas carentes da Vila Gua&iacute;ba, a regi&atilde;o mais pobre da Vila Assun&ccedil;&atilde;o, situada num estreito espa&ccedil;o entre a rua e o rio. Desde maio, elas t&ecirc;m um novo brilho de esperan&ccedil;a nos olhos: a creche foi escolhida pelo Projeto de Engenharia Social da Capa e at&eacute; o final do ano deve se mudar para um terreno ali perto, cedido pelo DAER. </p>

                        <p>Esther Terburg tinha vontade e tempo, mas n&atilde;o tinha nenhuma id&eacute;ia. Padre Cl&oacute;vis sugeriu que procurasse a irm&atilde; Vilma, no bairro Teres&oacute;polis. Ela trabalhava com v&aacute;rias comunidades carentes e certamente teria alguma sugest&atilde;o. Tinha: recomendou procurar uma l&iacute;der comunit&aacute;ria. "Sempre tem algu&eacute;m que se destaca, que as outras pessoas confiam numa quadra e algu&eacute;m que &eacute; refer&ecirc;ncia para esses l&iacute;deres" ensinou a irm&atilde; Vilma. </p>

                        <p>Esther lembra que foi um avan&ccedil;o dif&iacute;cil, intermediado pelo p&aacute;roco, at&eacute; conseguirem reunir-se no sal&atilde;o paroquial, em julho de 1992. Vinte moradoras estavam l&aacute; e ficaram silenciosas enquanto Esther falava. O ambiente come&ccedil;ou a ficar descontra&iacute;do quando ela citou um volunt&aacute;rio como &uacute;nico homem presente. Padre Cl&oacute;vis n&atilde;o perdeu a oportunidade: "Eu tamb&eacute;m sou homem", disse com um sorriso nos l&aacute;bios, o que provocou uma gargalhada na sala. A partir da&iacute;, a conversa foi bem mais f&aacute;cil. Esther confirma: "Enfim, foi uma 'rata' que descontraiu o ambiente e serviu para melhorar a conversa". At&eacute; que uma m&atilde;e sugeriu fazer uma creche no sal&atilde;o paroquial. Precisaria de algumas reformas, mas era o &uacute;nico espa&ccedil;o dispon&iacute;vel que havia no bairro. E mesmo, era pouco utilizado. Todos concordaram. </p>

                        <p><img src="images/creche1.jpg" width="200" height="150" align="left">Eles precisavam de uma creche: "Era praxe, e ainda acontece isso, os pais sa&iacute;rem para trabalhar e deixarem os filhos trancados em casa com um peda&ccedil;o de p&atilde;o e uma mamadeira. N&atilde;o fazem por maldade, mas para que as crian&ccedil;as n&atilde;o saiam para a rua", descobriu Esther. </p>

                        <p>Bem, ent&atilde;o era s&oacute; instalar a creche. Mas Esther &eacute; t&eacute;cnica em qu&iacute;mica industrial, trabalhou pouco mais de um ano e depois de casada, com Paulo, n&atilde;o trabalhou mais. A experi&ecirc;ncia de criar dois filhos - o casal veio para Porto Alegre h&aacute; 15 anos, depois da aposentadoria de Paulo - n&atilde;o era suficiente. Quem sabia mais era Sandra Leal, diretora do Instituto Nossa Senhora das Gra&ccedil;as, que na &eacute;poca era uma creche. Esther foi at&eacute; ela. "Primeiro de tudo, confie em Nossa Senhora", disse In&ecirc;s e, em seguida, recomendou procurar volunt&aacute;rios e mostrar o valor do que estava sendo feito. </p>

                        <p>Os volunt&aacute;rios apareceram e ajudaram a organizar ch&aacute;s e almo&ccedil;os de arrecada&ccedil;&atilde;o de dinheiro. Uma fam&iacute;lia mudou-se para S&atilde;o Paulo e doou a sala de jantar em estilo colonial, com um buffet e uma mesa enorme, para oito pessoas. O buffet servia de arm&aacute;rio, a mesa, para refei&ccedil;&otilde;es. Tinham que p&ocirc;r revistas nas cadeiras para que as crian&ccedil;as ficassem na altura da mesa. </p>

                        <p>E assim come&ccedil;ou a funcionar a creche, no dia 15 de novembro de 1992. Eram 10 crian&ccedil;as, entre 2 e 6 anos, com seis volunt&aacute;rios cuidando delas. <br>

                        </p>

                        <p><b>Nada de gra&ccedil;a <br>

                          <br>

                        </b>Todo mundo pagando mensalidade. &Eacute; assim desde o primeiro dia. A creche tem contribui&ccedil;&atilde;o de muita gente, mas n&atilde;o d&aacute; nada gr&aacute;tis. "Cobrar um pre&ccedil;o valoriza a auto-estima de quem paga", acredita Esther e at&eacute; agora a experi&ecirc;ncia tem comprovado a teoria. </p>

                        <p><img src="images/creche3.jpg" width="200" height="150" align="right">Assim, os pais das crian&ccedil;as pagam, conforme as possibilidades de cada um, de R$ 10 e R$ 50 por m&ecirc;s. </p>

                        <p>E os filhos t&ecirc;m cinco refei&ccedil;&otilde;es di&aacute;rias: leite com chocolate acompanhado de p&atilde;o com manteiga de manh&atilde;, uma fruta &agrave;s 9h, almo&ccedil;o, lanche &agrave;s 15h e sopa com carne e legumes &agrave;s 17h. Ultimamente, inverteram a ordem entre lanche e sopa. O lanche &eacute; um doce e muitas vezes as crian&ccedil;as comiam pouca sopa por causa do doce ingerido duas horas antes. A invers&atilde;o deu resultado: est&atilde;o tomando mais sopa. </p>

                        <p>Entretanto, n&atilde;o foi somente com o pagamento das mensalidades que a creche se sustentou todo este tempo. Foi com contribui&ccedil;&otilde;es mensais de paroquianos (R$ 5,00) e doa&ccedil;&otilde;es de diversos tipos. Mesmo roupas velhas doadas para a creche n&atilde;o s&atilde;o diretamente repassadas a crian&ccedil;as e adultos. "Muita gente acha que dar um sapato furado para os pobres est&aacute; &oacute;timo, porque o pobre n&atilde;o tem sapato. Mas como fica a auto-estima dessa pessoa que recebeu o sapato furado?" pergunta Esther. Ou doa uma camisa vermelha a quem n&atilde;o gosta do vermelho: "S&atilde;o seres humanos, com paladar, gostos e prefer&ecirc;ncias. Imagina como fica essa pessoa vendo-se obrigada a usar aquela camisa porque &eacute; a &uacute;nica que tem. Como fica a sua auto-estima?". </p>

                        <p>A id&eacute;ia da associa&ccedil;&atilde;o de fazer feiras e vender as doa&ccedil;&otilde;es foi uma solu&ccedil;&atilde;o providencial: "Um par de sapatos pode sair por 10, 50 centavos, no m&aacute;ximo, um real. Ent&atilde;o, a pessoa vem aqui e compra a roupa que gosta e sente-se valorizada, feliz", relata. A renda da feira tamb&eacute;m reverte para a creche. </p>

                        <p>Em 2000, foi feito um conv&ecirc;nio com a Prefeitura, o que possibilitou a contrata&ccedil;&atilde;o de professoras "profissionais". Mas todas essas iniciativas ainda s&atilde;o insuficientes. O principal problema &eacute; o local, pequeno para atender a quantidade de crian&ccedil;as necessitadas. Houve uma &eacute;poca, em que havia 31 delas freq&uuml;entando a creche. </p>

                        <p><img src="images/creche4.jpg" width="200" height="150" align="left">Esther foi atr&aacute;s de ajuda, at&eacute; que Miguel Velasquez, promotor de Justi&ccedil;a, engajou-se na causa. Descobriu um terreno do DAER, 200 metros adiante da atual localiza&ccedil;&atilde;o, que poderia sediar um novo pr&eacute;dio. Em julho de 2004, conversou com a primeira-dama do Estado e conseguiu autoriza&ccedil;&atilde;o para ocupar o terreno, pois parte do DAER vai sair dali. </p>

                        At&eacute; que um dia, soube do Programa de Responsabilidade Social da Capa, soube da Casa Amarela e decidiu: "Preciso obter este aux&iacute;lio". Visitou a inaugura&ccedil;&atilde;o da Casa Amarela e conversou com os diretores da empresa: "Eu vi a preocupa&ccedil;&atilde;o em fazer algo bonito, com bom acabamento, de qualidade. Do mesmo jeito que a gente acha que tem que ser". Neste ano, quando eles visitaram a creche, Esther afiou o discurso: "Acho que nunca falei tanto na minha vida". Valeu a pena, a 





 Creche Comunit&aacute;ria Capela Navegantes (ABENSA) &eacute; a escolhida pelo Projeto de Engenharia Social da Capa. <br>-->

                        <p><img src="images/abensa_foto1.jpg" width="200" height="190" hspace="5" align="left">Para as crian&ccedil;as e pais da vila Gua&iacute;ba o dia de Natal foi 20 de dezembro. N&atilde;o   tinha um Papai Noel de barbas brancas e roupa vermelha, mas o presente daquela   tarde tinha o mesmo significado de bondade, solidariedade e ajuda. Era a nova   sede da Creche Comunit&aacute;ria Capela Navegantes da Associa&ccedil;&atilde;o Beneficente Nossa   Senhora da Assun&ccedil;&atilde;o &ndash; ABENSA, entidade apoiada pelo Programa de Engenharia   Social da Capa em 2005. </p>

                        <p> A hist&oacute;ria   dessa creche come&ccedil;ou h&aacute; 12 anos. Esther Papa de Terburg, moradora da Vila   Assun&ccedil;&atilde;o, bairro de classe m&eacute;dia na zona sul de Porto Alegre &agrave; beira do Gua&iacute;ba,   ficou comovida com a pobreza de uma vila vizinha ao seu bairro, a Gua&iacute;ba. Falou   com o padre Cl&oacute;vis de Quadros, com as m&atilde;es daquela vila e foi conseguindo apoios   para instalar uma creche no sal&atilde;o paroquial. Assim, em 15 de novembro de 1992,   aconteceu a inaugura&ccedil;&atilde;o de uma pr&eacute;-escola para menores de sete anos. Ao mesmo   tempo, foi criada a associa&ccedil;&atilde;o, atualmente presidida por Esther. </p>

                        <p><img src="images/abensa_foto2.jpg" width="200" height="190" hspace="5" align="left">Em 2000, a creche credenciou-se no Conselho Municipal da Crian&ccedil;a e   Adolescente (CMDCA), conquistou conv&ecirc;nio com a Secretaria Municipal de Educa&ccedil;&atilde;o   (SMED) e passou a receber repasses mensais da Funda&ccedil;&atilde;o de Assist&ecirc;ncia Social e   Comunit&aacute;ria (Fasc). </p>

                        <p>Tudo isso ajudou, mas n&atilde;o era suficiente. O pr&eacute;dio n&atilde;o tinha estrutura ideal   para abrigar todas as crian&ccedil;as necessitadas. Em 2004, Esther e suas colegas de   dire&ccedil;&atilde;o souberam do Programa de Engenharia Social da Capa atrav&eacute;s de um folder   de divulga&ccedil;&atilde;o institucional e da constru&ccedil;&atilde;o de uma nova sede, a Casa Amarela,   para a Funda&ccedil;&atilde;o Recriar, que abriga meninos de rua. &ldquo;Precisamos entrar nisso&rdquo;,   ela disse para as colegas. Conseguiu, depois de enfrentar, com ajuda do Promotor   P&uacute;blico da Inf&acirc;ncia e Juventude, Miguel Vel&aacute;squez, da Prefeitura Municipal de   Porto Alegre e da Capa, uma floresta burocr&aacute;tica enorme.<br />

                            <br>

                            <br>

                            <img src="images/abensa_foto3.jpg" width="200" height="190" hspace="3" align="right">Era preciso encontrar um local melhor, com mais espa&ccedil;o para salas e &aacute;reas   de lazer e que n&atilde;o fosse muito distante. Logo descobriram que o DAER, a 200   metros da creche, se mudaria. Iniciaram-se as conversa&ccedil;&otilde;es com o Governo do   Estado, com o Gabinete da Primeira-dama, com as secretarias municipais para que   tudo ficasse de acordo com as determina&ccedil;&otilde;es legais. Em 5 de outubro de 2005, as   crian&ccedil;as mal podiam acreditar: tudo estava aprovado. Houve at&eacute; um ato oficial no   terreno da nova creche, com a presen&ccedil;a de autoridades e da dire&ccedil;&atilde;o da Capa   Engenharia. </p>

                        <p>A obra estava come&ccedil;ando. E andou veloz e eficiente. No dia 20 de dezembro,   dois meses e meio depois, estava entregue. Dentro do prazo previsto, &ldquo;um   compromisso que sempre cumprimos&rdquo;, como gosta de repetir Carlos Alberto   Schettert, s&oacute;cio-diretor da Capa. &Eacute; um grande presente de Natal para comunidade,   pois vai abrigar mais crian&ccedil;as. <br />

                            <br />

                            <img src="images/abensa_foto4.jpg" width="200" height="190" hspace="5" align="left">A solenidade de entrega teve muita   alegria, com bal&otilde;es para a crian&ccedil;ada, presen&ccedil;a do prefeito Jos&eacute; Foga&ccedil;a e da   primeira-dama do munic&iacute;pio Isabela Foga&ccedil;a, b&ecirc;n&ccedil;&atilde;o do padre Cl&oacute;vis de Quadros e o   agradecimento especial de Schettert aos mais de 60 parceiros da Capa, entre   todos fornecedores e prestadores de servi&ccedil;o, que ajudaram a construir uma nova   creche para crian&ccedil;as carentes. <br />

  <br />

                          A creche oferece cinco refei&ccedil;&otilde;es di&aacute;rias,   sem frituras, sob orienta&ccedil;&atilde;o de nutricionista, tem professoras especializadas e,   agora, &oacute;timas condi&ccedil;&otilde;es de salubridade e higiene. <br />

  <br />

                        

                          A Capa aproveitou parte da constru&ccedil;&atilde;o j&aacute; existente no terreno, principalmente   funda&ccedil;&otilde;es e algumas paredes. Ao todo, s&atilde;o 277 metros quadrados de &aacute;rea   constru&iacute;da. Tr&ecirc;s salas no andar superior para v&aacute;rias atividades e repouso. Uma   delas para crian&ccedil;as de 2 a 4 anos. Ainda uma oficina e um laborat&oacute;rio. </p>

                        <p><img src="images/abensa_foto5.jpg" width="200" height="190" align="right">No andar intermedi&aacute;rio, est&atilde;o a recep&ccedil;&atilde;o, a secretaria e o ber&ccedil;&aacute;rio. No andar   inferior, ficam cozinha, lavanderia, despensa e dep&oacute;sito. Al&eacute;m disso, h&aacute; dois   p&aacute;tios. Um de 180 metros quadrados e outro, &agrave; beira do Gua&iacute;ba, de 22 metros   quadrados. </p>

                        <p>Janeiro foi o m&ecirc;s de f&eacute;rias para crian&ccedil;as e professores, mas a diretoria da   Associa&ccedil;&atilde;o precisou abrir m&atilde;o de alguns de seus dias de descanso. Mas n&atilde;o foi um   sacrif&iacute;cio. Era o trabalho de fazer a mudan&ccedil;a e organizar o funcionamento da   nova creche. Afinal, a capacidade dela &eacute; de at&eacute; 50 crian&ccedil;as. E todas v&atilde;o estar   aproveitando o novo espa&ccedil;o quando acabarem as f&eacute;rias, dia 6 de   fevereiro.<br>

                          <br>

                          <img src="images/abensa_foto6.jpg" width="200" height="190"></p>                        </td>

                    </tr>

                    <tr>

                      <td>&nbsp;</td>

                    </tr>

                  </table></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>&nbsp;</td>

              </tr>

            </table></td>

          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/cadastre2.gif" width="186" height="73" border="0" usemap="#Map"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>


              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div align="center"><img src="images/26anos.gif" width="120" height="46"></div></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

            </table>

          <div style="padding-left:8px; padding-top:8px;"><strong>Links:</strong><br>

            <br>

            Associa��o Beneficente Nossa Senhora da Assun��o <br><a href="http://www.abensa.com.br/" target="_blank">www.abensa.com.br</a><br>

            <br>

            Prefeitura de Porto Alegre:<a href="http://www2.portoalegre.rs.gov.br/cs/default.php?reg=47263&p_secao=3&di=2005-10-06" target="_blank"> <br>

            Not&iacute;cia relacionada</a></div> 

            <map name="Map">

              <area shape="rect" coords="94,48,175,67" href="entidades.php">

            </map>

            <map name="Map2">

              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">

            </map></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td height="105" bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td><font color="#999999" size="1"><strong>Apoiadores do Projeto 2006</strong></font></td>

          <td width="10">&nbsp;</td>

        </tr>

        <tr> 

          <td>&nbsp;</td>

          <td><div align="justify"><font color="#999999" size="1">. Arq. Franklin Moreira (Projeto arquitet�nico)

. Projetak (Projeto estrutural)

. El�trons (Projeto el�trico)

. Hydrus (Projeto hidrossanit�rio)

. Sanidro (Mat. e inst. hidrossanit�ria)

. Casa dos Extintores (Preven��o inc�ndio)

. Arq. Renate Ara�jo Vianna

. Brocca & Correa (M�o-de-obra)

. Torres & Cristovam (M�o-de-obra)

. Celter Constru��es (M�o-de-obra)

. Nilo Marfetan (M�o-de-obra)

. Isotec (M�o-de-obra instala��es el�tricas) 

. ABS (Instala��o entrada de energia) 

. Pinturas Candido (M�o-de-obra pintura) 

. Esquadrias Pintz (Janelas basculantes) 

. Pathenon Estrut. Met�licas (Portas de ferro)

. Metal�rgica Borba (Corrim�os e funilaria) 

. Metal�rgica Visconti (Caixas el�tricas)

. AL Todeschini (Arremates de serralheria) 

. Tintas Kresil (Tintas)

. Belgo (A�o e arame recozido)

. CNA (Corte e dobra de a�o e mat. p/ tapume)

. Demolidora Santa Rita (Pedras de alicerce)

. Ludwig Herrmann (Escava��o e aterro)

. Vidra�aria Lajeadense (Vidros janelas)

. Vidrobox (Box banheiros)

. Brasilit (Reservat�rios de �gua)

. Marsul (Esquadrias de alum�nio)

. Cordeiro (Fios e cabos)

. Fida (Fornecimento de argamassa)

. Pauluzzi (Blocos cer�micos)

. Imperterm (Impermeabiliza��o)

. Cimatex (Cimento)

. Eletric-Sul (Disjuntores)

. Fabrimar (Metais sanit�rios)

. Protefix (Lonas e EPIs)   . Sia (Extintores)

. Diprotec (Impermeabilizantes e aditivos)

. SPR Metais (Acess�rios para banheiros)

. WD Madeiras (Toda a madeira da obra)

. Nichele entulhos (Container entulhos)

. Cecrisa (Revestimento cer�mico)

. Stilo Elevato (Registros e lou�as)

. Colormat (Tintas e pinc�is)

. RM loca��es (Equip. para constru��o)

. Dimacon (Loca��o betoneira)

. Real Containers (Container vesti�rio)

. Rampa Compensados

. Di Luce (Lumin�rias)

. Arq. Fernando Bertuol 

. Expresso Nova Santa Rita e Reichelt (Fretes)

. Comercial El�trica Pampa (Material el�trico)

. Vedacit (Impermeabilizantes)

. Igua�u M�rmores e Granitos (Peitoris de m�rmore)

. Carlos Groli Moreira (M�o-de-obra gesso acartonado)

. Knauf do Brasil Ltda.

. Shine Service - Limpezas

. Salgado Com�rcio de Fechaduras

. Martini Materiais de Constru��o Ltda. (Coloca��o piso flutuante) 

. TTP Thermoflex - Molduras de poliuretano

. Raul (Mestre Brocca)

 <br>

              </font></div></td>

          <td>&nbsp;</td>

        </tr>

      </table></td>

  </tr>

</table>

</body>

</html>

