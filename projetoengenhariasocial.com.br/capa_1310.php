<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Projeto Engenharia Social</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="capa.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<style type="text/css">
<!--
.style1 {color: #999999}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="742" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>
          <td>&nbsp;</td>
          <td width="126"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="126" height="72" border="0"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><img src="images/bannertopo.jpg" width="742" height="116"></td>
  </tr>
  <tr>
    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>
          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>
          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>
          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>
          <td width="83"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>
          <td width="121" bgcolor="#009933"></td>
          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="10">&nbsp;</td>
          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td>&nbsp;</td>
                <td width="10">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>A Capa Engenharia</strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td height="13"><br></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td> 
                  <table width="75%" border="0" align="center">
                    <tr>
                      <td bordercolor="#333333"><p align="justify">Qualidade de vida come&ccedil;a pela 
                          qualidade em constru&ccedil;&atilde;o. E essa sempre 
                          foi a caracter&iacute;stica da Capa em seus empreendimentos. 
                          Tanto que &eacute; refer&ecirc;ncia internacional, com 
                          reconhecimento atrav&eacute;s das certifica&ccedil;&otilde;es 
                          ISO-9001/2000 e PBQP-H &#8211; N&iacute;vel A e de premia&ccedil;&otilde;es 
                          como Construtora do Ano Sinduscon-RS e Top de Marketing 
                          ADVB-RS, entre outras.</p>
                        <p align="justify">As modernas t&eacute;cnicas de gest&atilde;o 
                          e as inova&ccedil;&otilde;es implementadas nos processos 
                          de planejamento e produ&ccedil;&atilde;o elevam os n&iacute;veis 
                          de excel&ecirc;ncia da empresa e garantem obras sempre 
                          entregues no prazo e com alto valor agregado.<br>
                          <br>
                          Em 2010 a Capa Engenharia completou vinte e seis
                          anos. Per&iacute;odo de dedica&ccedil;&atilde;o e trabalho 
                          para construir uma das mais respeitadas empresas do 
                          setor imobili&aacute;rio do Rio Grande do Sul. Vinte e seis
                          anos marcados por um lema: o ser humano em primeiro 
                          lugar. &Eacute; a partir dele que surgem as nossas cren&ccedil;as: 
                          a eterna busca por inova&ccedil;&atilde;o, o investimento 
                          em nossos profissionais e a incessante procura por melhoras 
                          cont&iacute;nuas em nosso padr&atilde;o de qualidade.</p>
                        <p align="justify">Al&eacute;m disso, foi dessa valoriza&ccedil;&atilde;o 
                          do ser humano que nasceu a preocupa&ccedil;&atilde;o 
                          da Capa pela responsabilidade social e cultural.</p>
                        <p align="justify">Por isso, em 2004, 
                          a Capa lan&ccedil;ou a maior dessas a&ccedil;&otilde;es:
                           o Projeto Engenharia Social - uma iniciativa que 
                          une colaboradores e clientes para apoiar entidades
                          sem fins lucrativos.                          </p>
                        <p align="justify">&Eacute; dessa forma, premiando o
                          ser  humano, que a Capa Investe nos seus pr&oacute;prios crescimentos.
                          Afinal, 
                          mais do que pr&eacute;dios, a Capa constr&oacute;i
                          sonhos.  Mais do que tijolos e concreto, &eacute; o
                          sonho das  pessoas a nossa grande mat&eacute;ria&#8211;prima.
                           O sonho de quem quer viver em um lugar especial. Um
                          
                          lugar como o mundo que queremos ajudar a construir.</p>
                        <p></p>
                        <p align="justify">site - <a href="http://www.capa.com.br" target="_blank">http://www.capa.com.br</a></p></td>
                    </tr>
                  </table>
                  
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              
              <!-- 
              
              <tr> 
                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><div style="padding:5%; font-size: 9;"> <font face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><a href="entidades.php">Aberta a sele��o de entidades para 2009.</a></font> </div></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA" align="center"> </td>
              </tr>
              <tr> 
                <td height="1" bgcolor="#FFFFFF"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>
              </tr>
              
               -->
              
              <tr> 
                <td bgcolor="#C0E7BA"><p><img src="images/novidades.gif" width="186" height="20"></p></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><img src="images/novidades3.gif" width="186" height="66" border="0" usemap="#Map2"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA" align="center"> </td>
              </tr>
              <tr> 
                <td height="1" bgcolor="#FFFFFF"></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA"><div align="center"><a href="http://www.capa.com.br" target="_blank"><img src="images/26anos.gif" width="60" height="46" border="0"></a></div></td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
              <tr> 
                <td bgcolor="#C0E7BA">&nbsp;</td>
              </tr>
            </table>
<map name="Map">
              <area shape="rect" coords="9,2,175,67" href="entidades.php">
            </map>
            <map name="Map2">
              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">
            </map></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10">&nbsp;</td>
        <td><font color="#999999" size="1"><strong>Apoiadores do Projeto 2008<br>
        </strong></font></td>
        <td width="10">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td valign="top"><div align="justify" class="style1">Alusistem - Alum�nio para Constru��o Civil Ltda (Beneficiamento de alum�nio).Amanco - Brasil Ltda(Material hidr�ulico).Arcelor Mittal Brasil S/A(Tela + a�o radier) .Arte M�veis(Portas de madeira e guarni��es).Baumgarten Com�rcio & Instala��o em Sistema Drywall(M�o-de-obra forro de gesso) .Belmetal Ind�stria e Com�rcio Ltda(Alum�nio para esquadrias). Blumengarten Plantas e Flores Ltda	(Plantas e Flores) . Brocca Correa Ltda(M�o-de-obra radier) . Cecrisa Revestimentos Cer�micos Ltda	(Piso cer�mico e azulejos) .Cia Nacional do A�o Ind�stria e Com�rcio(Tela + a�o radier) . Cimatex materiais de constru��o Ltda (Cimento) . CCB- Cimpor Cimentos do Brasil Ltda (Concreto) . Creel-Sul Com�rcio de Equipamentos el�tricos Ltda (Material el�trico) . Deca	(Lou�as) . Design Decora��es Ltda (M�o-de-obra piso laminado) . 
Di Luce -Fonte de Luz Ilumina��o Ltda (Lumin�rias) . Docol Metais Sanit�rios Ltda (Metais) . Duratex S.A (Piso laminado) . Elektra Distribuidora (Material el�trico e lumin�rias externas) . Eletric-Sul com�rcio de materiais el�tricos Ltda (Material el�trico) . Empreiteira de obras Teixeira & Silva(M�o-de-obra pisos e azulejos cer�micos) . Esquadrias Pintz Ltda (Port�o de entrada ve�culos e port�o reservat�rio superior) . Est�dio 3 Engenharia de estuturas Ltda (Projeto Radier) . Faber Mobilis Industria de M�veis Ltda (Banco para deficiente) . Fechart Comercio de Fechaduras Ltda	(Fornecimento de fechaduras e dobradi�as) . Fibrafort Piscinas Ltda (Reservat�rios) . Giancarlo C�ndido (M�o-de-obra pintura ) . Golder Constru��es Ltda	(M�o-de-obra alvenaria, reboco e cobertura) . Hydrus Projetos de Instala��es Sanit�rias (Projeto Hidrossanit�rio) . Igua�u Pr�-fabricados de Granito Ltda	(Peitoris de Janelas, tampo granito cozinha e banheiros) . Inap -Ind�stria Nacional de A�o Pronto (Corte e dobra a�o radier) . Instaladora Base LTDA (Subs�dio m�o-de-obra el�trica) . Irm�os Ciocari Ltda (Argamassa de assentamento) . Knauf do Brasil Ltda (forro de gesso) . Ludwig Herrmann Ltda (Limpeza do terreno e aterro) . MDR-Com�rcio Ferragem e Materiais de Constru��o (Vasos sanit�rios, chuveiros e materiais hidr�ulicos) . Metal�rgica Borba Ind�stria e com�rcio Ltda (Caixilhos de piso, port�o para pedestres e port�o de g�s) . Metal�rgica Visconti Ltda (Material el�trico) . Milititsky Consultoria Geot�cnica� Engenheiros Assoc.S/S Ltda	(Estudo viabilidade projeto Radier) . Multicabos distribuidora de materiais el�tricos Ltda (Material el�trico) . Norte Pisos Ltda (Acabamento de piso de concreto, rampas e passeios) . Pauluzzi Produtos Cer�micos Ltda (Blocos cer�micos) . Plenobr�s Distribuidora el�trica Ltda (Material el�trico) .
Rampa Compensados Ltda	(Compensado) . Ricardo Lemos da Costa ME (Enleivamento e plantio de mudas e plantas) . Saint Gobain Brasilit Ltda (Telhas) . Sanidro Instala��es Hidr�ulicas Ltda (Subs�dio m�o-de-obra hidr�ulica) . Shine Service (M�o-de-obra limpeza) . Sia Com�rcio e Representa��o Ltda (Fornecimento de extintores) . Tintas Kresil Ltda (Tintas) . Vidra�aria Lajeadense Ltda	(Vidros) . Divex (desenvolvimento site)</div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
