<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>

<title>Projeto Engenharia Social</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="capa.css" rel="stylesheet" type="text/css">

<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open(theURL,winName,features);

}



function MM_displayStatusMsg(msgStr) { //v1.0

  status=msgStr;

  document.MM_returnValue = true;

}

//-->

</script>

</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="742" border="0" cellspacing="0" cellpadding="0" style="margin:auto">

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>

          <td>&nbsp;</td>

          <td width="126"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="268" height="72" border="0"></a></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td><img src="images/imagem.gif" width="742" height="116"></td>

  </tr>

  <tr>

    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">

        <tr> 

          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>

          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>

          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>

          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>

          <td width="83"><a href="entidades.php"><IMG SRC="images/menu_05.gif" ALT="" WIDTH=83 HEIGHT=19 border="0"></a></td>

          <td width="121" bgcolor="#009933"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>

          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td width="10">&nbsp;</td>

                <td>&nbsp;</td>

                <td width="10">&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td height="13"><br></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td> <table width="80%" border="0" align="center">

                    <tr> 

                      <td><b>Escolhida a nova entidade para 2007 </b></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                    <tr> 

                      <td>

                        <p align="left"> <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto1.jpg','vilapinto1','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto1_th.jpg" width="180" height="135" hspace="5" border="0" align="right"></a>Depois de uma criteriosa sele&ccedil;&atilde;o, foi escolhida a nova entidade para ser beneficiada com a constru&ccedil;&atilde;o pelo Grupo Capa

                        em 2007.<br>

                        <br>

                        A entidade escolhida &eacute; a futura CRECHE COMUNIT&Aacute;RIA NA VILA PINTO, na rua Joaquim Porto Vila Nova. Localizada numa comunidade muito carente, a creche proporcionar&aacute; o atendimento de 120 crian&ccedil;as  de 0 a 6 anos.<br>

                        <br>

                        <br> 

                        <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto2.jpg','vilapinto2','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto2_th.jpg" width="180" height="135" hspace="5" border="0" align="right"></a>O projeto, que ter&aacute; o apoio da Prefeitura de Porto Alegre, firmou-se no dia 15 de dezembro, numa cerim&ocirc;nia na Prefeitura de Porto Alegre. Estavam presentes o sr.  Prefeito Jos&eacute; Foga&ccedil;a, a primeira-dama Isabela Foga&ccedil;a, alguns deputados e assessores, o diretor da Capa Engenharia, Carlos Alberto de M. Schettert, a assessoria de comunica&ccedil;&atilde;o da Capa, a Coordenadora do Projeto, Raquel Paiani, e tamb&eacute;m a l&iacute;der comunit&aacute;ria da Vila Pinto, Sra. Marli.<br>

                        <br>

                        <strong><br>

                          <br>

                          <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto3.jpg','vilapinto5','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto3_th.jpg" width="180" height="135" hspace="5" border="0" align="left"></a> &Aacute;rea

                          a ser constru&iacute;da</strong>: 287 m&sup2;<br>

                          <strong>Previs&atilde;o de in&iacute;cio da constru&ccedil;&atilde;o</strong>: Mar&ccedil;o/2007.<br>

                          <strong>Previs&atilde;o de conclus&atilde;o</strong>:

                          Agosto/2007<br>

                          <strong>Valor estimado</strong>: R$ 250.000,00<br>

                        <strong>Constru&ccedil;&atilde;o</strong>: Contar&aacute; com a participa&ccedil;&atilde;o dos parceiros da Cadeia Produtiva do Grupo Capa Engenharia.</p>

                      </td>

                    </tr>

                    <tr>

                      <td>&nbsp;</td>

                    </tr>

                    <tr>

                      <td>Veja as fotos do in&iacute;cio das obras:</td>

                    </tr>

                    <tr>

                      <td> <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao1.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto_construcao1_th.jpg" width="100" height="75" border="0"></a> &nbsp; <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao2.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto_construcao2_th.jpg" width="100" height="75" border="0"></a><a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto7.jpg','vilapinto7','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"> </a>&nbsp; <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao3.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto_construcao3_th.jpg" width="100" height="75" border="0"></a><br>

                      <br>

                      <a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao4.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"><img src="images/vilapinto_construcao4_th.jpg" width="100" height="75" border="0"></a><a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao5.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"> &nbsp; <img src="images/vilapinto_construcao5_th.jpg" width="100" height="75" border="0"></a><a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto_construcao6.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue"> &nbsp; <img src="images/vilapinto_construcao6_th.jpg" width="100" height="75" border="0"></a></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                  </table></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>&nbsp;</td>

              </tr>

            </table></td>

          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/cadastre2.gif" width="186" height="73" border="0" usemap="#Map"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>


              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div align="center"><img src="images/26anos.gif" width="120" height="46"></div></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

            </table><div style="padding-left:8px; padding-top:8px;"><strong>Link:</strong><br><br>

            Associa��o Beneficente Nossa Senhora da Assun��o <br><a href="http://www.abensa.com.br/" target="_blank">www.abensa.com.br</a></div> 

            <map name="Map">

              <area shape="rect" coords="94,48,175,67" href="entidades.php">

            </map>

            <map name="Map2">

              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">

            </map></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td height="105" bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td><font color="#999999" size="1"><strong>Apoiadores do Projeto 2006</strong></font></td>

          <td width="10">&nbsp;</td>

        </tr>

        <tr> 

          <td>&nbsp;</td>

          <td><div align="justify"><font color="#999999" size="1">. Arq. Franklin Moreira (Projeto arquitet�nico)

. Projetak (Projeto estrutural)

. El�trons (Projeto el�trico)

. Hydrus (Projeto hidrossanit�rio)

. Sanidro (Mat. e inst. hidrossanit�ria)

. Casa dos Extintores (Preven��o inc�ndio)

. Arq. Renate Ara�jo Vianna

. Brocca & Correa (M�o-de-obra)

. Torres & Cristovam (M�o-de-obra)

. Celter Constru��es (M�o-de-obra)

. Nilo Marfetan (M�o-de-obra)

. Isotec (M�o-de-obra instala��es el�tricas) 

. ABS (Instala��o entrada de energia) 

. Pinturas Candido (M�o-de-obra pintura) 

. Esquadrias Pintz (Janelas basculantes) 

. Pathenon Estrut. Met�licas (Portas de ferro)

. Metal�rgica Borba (Corrim�os e funilaria) 

. Metal�rgica Visconti (Caixas el�tricas)

. AL Todeschini (Arremates de serralheria) 

. Tintas Kresil (Tintas)

. Belgo (A�o e arame recozido)

. CNA (Corte e dobra de a�o e mat. p/ tapume)

. Demolidora Santa Rita (Pedras de alicerce)

. Ludwig Herrmann (Escava��o e aterro)

. Vidra�aria Lajeadense (Vidros janelas)

. Vidrobox (Box banheiros)

. Brasilit (Reservat�rios de �gua)

. Marsul (Esquadrias de alum�nio)

. Cordeiro (Fios e cabos)

. Fida (Fornecimento de argamassa)

. Pauluzzi (Blocos cer�micos)

. Imperterm (Impermeabiliza��o)

. Cimatex (Cimento)

. Eletric-Sul (Disjuntores)

. Fabrimar (Metais sanit�rios)

. Protefix (Lonas e EPIs)   . Sia (Extintores)

. Diprotec (Impermeabilizantes e aditivos)

. SPR Metais (Acess�rios para banheiros)

. WD Madeiras (Toda a madeira da obra)

. Nichele entulhos (Container entulhos)

. Cecrisa (Revestimento cer�mico)

. Stilo Elevato (Registros e lou�as)

. Colormat (Tintas e pinc�is)

. RM loca��es (Equip. para constru��o)

. Dimacon (Loca��o betoneira)

. Real Containers (Container vesti�rio)

. Rampa Compensados

. Di Luce (Lumin�rias)

. Arq. Fernando Bertuol 

. Expresso Nova Santa Rita e Reichelt (Fretes)

. Comercial El�trica Pampa (Material el�trico)

. Vedacit (Impermeabilizantes)

. Igua�u M�rmores e Granitos (Peitoris de m�rmore)

. Carlos Groli Moreira (M�o-de-obra gesso acartonado)

. Knauf do Brasil Ltda.

. Shine Service - Limpezas

. Salgado Com�rcio de Fechaduras

. Martini Materiais de Constru��o Ltda. (Coloca��o piso flutuante) 

. TTP Thermoflex - Molduras de poliuretano

. Raul (Mestre Brocca)

 <br>

              </font></div></td>

          <td>&nbsp;</td>

        </tr>

      </table></td>

  </tr>

</table>

</body>

</html>

