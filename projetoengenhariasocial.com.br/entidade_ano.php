<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>

<title>Projeto Engenharia Social</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="capa.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_js/prototype.js"></script>

<script type="text/javascript" src="_js/scriptaculous.js?load=effects,builder"></script>

<script type="text/javascript" src="_js/lightbox.js"></script>

<link rel="stylesheet" href="_css/lightbox.css" type="text/css" media="screen" />

<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open(theURL,winName,features);

}



function MM_displayStatusMsg(msgStr) { //v1.0

  status=msgStr;

  document.MM_returnValue = true;

}

//-->

</script>

<script language="JavaScript" type="text/JavaScript">

function ocultaLayer(p){

       if (p == 'a'){

               document.getElementById('vila').style.display = '';

       }

       if (p == 'f'){

               document.getElementById('vila').style.display = 'none';

       }

}

</script>

<style type="text/css">

<!--

.style5 {color: #999999}

-->

</style>

</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="742" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto">

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>

          <td>&nbsp;</td>


        <td width="20">&nbsp;</td>

		<td width="154"><div align="center"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="268" height="72" border="0"></a></div></td>

		</tr>

      </table></td>

  </tr>

  <tr>

    <td><img src="images/bannertopo.jpg" width="742" height="116"></td>

  </tr>

  <tr>

    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">

        <tr> 

          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>

          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>

          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>

          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>

          <td width="83"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>

          <td width="121" bgcolor="#009933"></td>

          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td width="10">&nbsp;</td>

                <td>&nbsp;</td>

                <td width="10">&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td height="13"><br></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td> 
                <table width="520" border="0" align="center">
                	<tr id="vila" style="display:;"> 
                    	<td align="right"></td>
                    </tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><div class="instituicao_2008">Entidades contempladas</div></td>
                    </tr>
                    <tr>
						<td valign="top" >                     
                      		<br>
                        	<div align="justify">A Funda��o Instituto Recriar foi a primeira contemplada, no ano de 2004, com a constru��o da Casa Amarela das Arauc�rias, entregue em 2005. No mesmo ano, o projeto construiu a nova sede da Creche Comunit�ria Capela Navegantes da Associa��o Beneficente Nossa Senhora da Assun��o � ABENSA, entregue em 2006. Em fevereiro de 2008, aconteceu a entrega da terceira realiza��o: a Escola de Educa��o Infantil Vov� Belinha, no Centro de Educa��o Ambiental da Vila Pinto. Em mar�o de 2009, foi a entrega da Escola de Educa��o Infantil Vila S�o Guilherme, no Bairro Partenon.
                               <br>
                         	   <br>
                       		   <div class="instituicao_2008"><br><br>Projeto Engenharia Social Fase Um: Casa Amarela � Funda��o Instituto Recriar</div> 
                       		   <br>
                    			 O terreno para a nova Casa Amarela, no Bairro Nonoai, foi cedido pela prefeitura. O projeto proporcionou a constru��o de uma nova casa de alvenaria com 180m�, com quatro quartos, sala de refei��o, cozinha, sala de estar e estudos, tr�s banheiros, lavanderia e demais depend�ncias, al�m de uma ampla �rea de lazer, como casa da Figueira, playground, horta, minhoc�rio, bosque, campo de futebol, biciclet�rio e sala de jogos.
                               <br>
                         	   <br>
                     			Desde a cria��o, j� conviveram na Casa, aproximadamente, 50 garotos entre 7 e 16 anos. Os adolescentes s�o acompanhados por oito educadores, durante 24 horas. Todos frequentam a escola e praticam esportes. A maior parte desses jovens j� atingiu a maioridade. Ou seja, cresceram e foram seguir suas pr�prias vidas, empregados e, muitos deles, estudando. 
							</div>
						</td>
                    </tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><br>
							<br>

							Galeria de fotos casa amarela:<br> <br>
                        </td>
					</tr>
					<tr>
						<td valign="top" class="instituicao_2008">
                            <table width="400" border="0" align="center" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td><a href="images/casa_amarela/01.jpg" rel="lightbox[roadtrip]"><img src="images/casa_amarela/01.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/casa_amarela/02.jpg" rel="lightbox[roadtrip]"><img src="images/casa_amarela/02.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/casa_amarela/03.jpg" rel="lightbox[roadtrip]"><img src="images/casa_amarela/03.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/casa_amarela/04.jpg" rel="lightbox[roadtrip]"><img src="images/casa_amarela/04.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                            </table>
                        </td>
					</tr>

                    <tr>
						<td valign="top" class="instituicao_2008"><div class="instituicao_2008"><br><br><br>Projeto Engenharia Social Fase Dois: Creche Comunit�ria Capela Navegantes da Associa��o Beneficente Nossa Senhora da Assun��o</div></td>
                    </tr>
                    <tr>
						<td valign="top" >                     
                      		<br>
                        	<div align="justify">A nova sede da Creche Comunit�ria Capela Navegantes da Associa��o Beneficente Nossa Senhora da Assun��o � ABENSA foi a obra eleita pelo Programa de Engenharia Social da CAPA, em 2005.
                               <br><br>
                               A CAPA aproveitou parte da constru��o j� existente no terreno - funda��es e algumas paredes. Ao todo, s�o 277m� de �rea constru�da para atender a 50 crian�as. Tr�s salas no andar superior para as atividades e repouso - uma delas espec�fica para crian�as de 2 a 4 anos. E mais uma oficina e um laborat�rio. No andar intermedi�rio, se localiza a recep��o, a secretaria e o ber��rio. No andar inferior, est� a cozinha, a lavanderia, a despensa e o dep�sito. Al�m disso, h� dois p�tios: um de 180m� e outro, � beira do Gua�ba, de 22m�.
                         	   <br>
							</div>
						</td>
                    </tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><br>
							<br>

							Galeria de fotos Capela navegantes<br> <br>
                        </td>
					</tr>
					<tr>
						<td valign="top" class="instituicao_2008">
                            <table width="400" border="0" align="center" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td><a href="images/capela_dos_navegantes/01.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/01.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/02.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/02.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/03.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/03.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/04.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/04.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                                <tr>
                                    <td><a href="images/capela_dos_navegantes/05.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/05.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/06.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/06.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/07.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/07.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/08.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/08.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                                <tr>    
                                    <td><a href="images/capela_dos_navegantes/09.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/09.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/10.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/10.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/capela_dos_navegantes/11.jpg" rel="lightbox[roadtrip2]"><img src="images/capela_dos_navegantes/11.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                            </table>
                        </td>
					</tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><div class="instituicao_2008"><br><br><br>Projeto Engenharia Social Fase Tr�s: Creche Comunit�ria da Vila Pinto</div></td>
                    </tr>
                    <tr>
						<td valign="top" >                     
                      		<br>
                        	<div align="justify">A terceira fase recebeu uma significativa parceria. O aliado estrat�gico da CAPA, em outros empreendimentos, confirmou a sua a participa��o efetiva. A Rodobens Neg�cios Imobili�rios aderiu ao Projeto Engenharia Social. Essa importante presen�a, em 2007, foi complementada com a implanta��o do Projeto Construtor da Esperan�a � que visa fomentar uma contribui��o espont�nea dos clientes da CAPA para a complementa��o de mobili�rio e equipagem das institui��es beneficiadas.
                               <br><br>
                               A entidade contemplada pelo Projeto Engenharia Social foi a Creche Comunit�ria da Vila Pinto. A a��o foi oficializada no dia 15 de dezembro de 2006, na Prefeitura de Porto Alegre, quando a CAPA Engenharia iniciou a viabiliza��o da constru��o da creche, localizada na Rua Joaquim Porto, para atender a 120 crian�as de at� 6 anos. A conclus�o da obra foi no final do segundo semestre de 2007, e, a entrega, para no in�cio de 2008. 
                         	   <br> 
                               
                               <br>
                              A Vila Pinto tem uma popula��o de cerca de 11 mil pessoas, que, somada a de outras duas vilas pr�ximas, totaliza 30 mil habitantes. Infelizmente, cerca de 70% das fam�lias que l� residem vivem em situa��o de risco social. A constru��o da creche � a esperan�a de que as novas gera��es do local consigam reverter esse quadro. A obra recebeu um investimento de aproximadamente R$ 460 mil. Em uma �rea de 286m2, a creche tem beneficiado mais de uma centena de crian�as de at� 6 anos de idade.  
                         	   <br>
							</div>
						</td>
                    </tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><br>
							<br>

							Galeria de fotos Vila Pinto<br> <br>
                        </td>
					</tr>
					<tr>
						<td valign="top" class="instituicao_2008">
                            <table width="400" border="0" align="center" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td><a href="images/vila_pinto/01.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/01.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/vila_pinto/02.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/02.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/vila_pinto/03.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/03.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/vila_pinto/04.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/04.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                                <tr>
                                    <td><a href="images/vila_pinto/05.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/05.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/vila_pinto/06.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/06.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/vila_pinto/07.jpg" rel="lightbox[roadtrip3]"><img src="images/vila_pinto/07.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                            </table>
                        </td>
					</tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><div class="instituicao_2008"><br><br><br>Projeto Engenharia Social Fase Quatro: Escola de Educa��o Infantil Vila S�o Guilherme</div></td>
                    </tr>
                    <tr>
						<td valign="top" >                     
                      		<br>
                        	<div align="justify">A quarta entidade beneficiada pelo projeto foi a Escola de Educa��o Infantil Vila S�o Guilherme, no Bairro Partenon, na Zona Leste. Atrav�s do projeto, a escola, em uma �rea de 285m�, tem capacidade para atender a 120 crian�as de 2 a 6 anos de idade, em situa��o de risco social. A partir de mar�o de 2009, elas receberam um local com toda a infraestrutura necess�ria para se desenvolver, com uma ampla pra�a arborizada equipada de v�rios brinquedos, dois ber��rios, dois maternais, cozinha, refeit�rio e banheiros. A constru��o teve um or�amento em torno de R$ 480 mil.
                               <br><br>
                               Em 2010 foi definida a constru��o de mais tr�s Casas em Porto Alegre: Creche Prisma, da Vila Cruzeiro, Creche Gabriel Obino, em Teres�polis; e Creche Paulino Azurenha, no Partenon. As creches dever�o ser entregues at� julho de 2012.
                         	   <br>                                
							</div>
						</td>
                    </tr>
                    <tr>
						<td valign="top" class="instituicao_2008"><br>
							<br>Galeria de fotos S�o Guilherme<br> <br>
                        </td>
					</tr>
					<tr>
						<td valign="top" class="instituicao_2008">
                            <table width="400" border="0" align="center" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td><a href="images/sao_guilherme/01.jpg" rel="lightbox[roadtrip4]"><img src="images/sao_guilherme/01.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/sao_guilherme/02.jpg" rel="lightbox[roadtrip4]"><img src="images/sao_guilherme/02.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/sao_guilherme/03.jpg" rel="lightbox[roadtrip4]"><img src="images/sao_guilherme/03.jpg" width="103" height="78" border="0"></a></td>
                                    <td><a href="images/sao_guilherme/04.jpg" rel="lightbox[roadtrip4]"><img src="images/sao_guilherme/04.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                                <tr>
                                    <td><a href="images/sao_guilherme/05.jpg" rel="lightbox[roadtrip4]"><img src="images/sao_guilherme/05.jpg" width="103" height="78" border="0"></a></td>
                                </tr>
                            </table>
                        </td>
					</tr>
					<tr>
						<td valign="top" class="instituicao_2008"><br><br></td>
                    </tr>

				</table>
                
                
                
                
                </td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>&nbsp;</td>

              </tr>

            </table></td>

          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

             

             

             <!-- 

             

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div style="padding:5%; font-size: 9;"><font face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><a href="entidades.php">Aberta a sele��o de entidades para 2009. </a></font></div> </td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>

              </tr>

              

               -->

              


              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div align="center"><a href="http://www.capa.com.br" target="_blank"><img src="images/26anos.gif" width="120" height="46" border="0"></a></div></td>

              </tr>

              <tr>

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td background="images/linha.gif" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr>

                <td bgcolor="#C0E7BA"><div align="center">

                  <p class="construtor"><strong>Participe do Projeto Construtor

                    da Esperan&ccedil;a</strong></p>

                  <p><img src="images/logo_construtor.gif" width="90" height="138" border="0"></p>

                </div></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

            </table>


            <map name="Map2">

              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">

            </map></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top" bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td><font color="#999999" size="2"><strong>Apoiadores do Projeto 2008<br>

          </strong></font></td>

          <td width="10">&nbsp;</td>

        </tr>

        <tr> 

          <td>&nbsp;</td>

          <td valign="top"><div align="justify" class="style5">Alusistem - Alum�nio para Constru��o Civil Ltda (Beneficiamento de alum�nio).Amanco - Brasil Ltda(Material hidr�ulico).Arcelor Mittal Brasil S/A(Tela + a�o radier) .Arte M�veis(Portas de madeira e guarni��es).Baumgarten Com�rcio & Instala��o em Sistema Drywall(M�o-de-obra forro de gesso) .Belmetal Ind�stria e Com�rcio Ltda(Alum�nio para esquadrias). Blumengarten Plantas e Flores Ltda	(Plantas e Flores) . Brocca Correa Ltda(M�o-de-obra radier) . Cecrisa Revestimentos Cer�micos Ltda	(Piso cer�mico e azulejos) .Cia Nacional do A�o Ind�stria e Com�rcio(Tela + a�o radier) . Cimatex materiais de constru��o Ltda (Cimento) . CCB- Cimpor Cimentos do Brasil Ltda (Concreto) . Creel-Sul Com�rcio de Equipamentos el�tricos Ltda (Material el�trico) . Deca	(Lou�as) . Design Decora��es Ltda (M�o-de-obra piso laminado) . 

Di Luce -Fonte de Luz Ilumina��o Ltda (Lumin�rias) . Docol Metais Sanit�rios Ltda (Metais) . Duratex S.A (Piso laminado) . Elektra Distribuidora (Material el�trico e lumin�rias externas) . Eletric-Sul com�rcio de materiais el�tricos Ltda (Material el�trico) . Empreiteira de obras Teixeira & Silva(M�o-de-obra pisos e azulejos cer�micos) . Esquadrias Pintz Ltda (Port�o de entrada ve�culos e port�o reservat�rio superior) . Est�dio 3 Engenharia de estuturas Ltda (Projeto Radier) . Faber Mobilis Industria de M�veis Ltda (Banco para deficiente) . Fechart Comercio de Fechaduras Ltda	(Fornecimento de fechaduras e dobradi�as) . Fibrafort Piscinas Ltda (Reservat�rios) . Giancarlo C�ndido (M�o-de-obra pintura ) . Golder Constru��es Ltda	(M�o-de-obra alvenaria, reboco e cobertura) . Hydrus Projetos de Instala��es Sanit�rias (Projeto Hidrossanit�rio) . Igua�u Pr�-fabricados de Granito Ltda	(Peitoris de Janelas, tampo granito cozinha e banheiros) . Inap -Ind�stria Nacional de A�o Pronto (Corte e dobra a�o radier) . Instaladora Base LTDA (Subs�dio m�o-de-obra el�trica) . Irm�os Ciocari Ltda (Argamassa de assentamento) . Knauf do Brasil Ltda (forro de gesso) . Ludwig Herrmann Ltda (Limpeza do terreno e aterro) . MDR-Com�rcio Ferragem e Materiais de Constru��o (Vasos sanit�rios, chuveiros e materiais hidr�ulicos) . Metal�rgica Borba Ind�stria e com�rcio Ltda (Caixilhos de piso, port�o para pedestres e port�o de g�s) . Metal�rgica Visconti Ltda (Material el�trico) . Milititsky Consultoria Geot�cnica� Engenheiros Assoc.S/S Ltda	(Estudo viabilidade projeto Radier) . Multicabos distribuidora de materiais el�tricos Ltda (Material el�trico) . Norte Pisos Ltda (Acabamento de piso de concreto, rampas e passeios) . Pauluzzi Produtos Cer�micos Ltda (Blocos cer�micos) . Plenobr�s Distribuidora el�trica Ltda (Material el�trico) .

Rampa Compensados Ltda	(Compensado) . Ricardo Lemos da Costa ME (Enleivamento e plantio de mudas e plantas) . Saint Gobain Brasilit Ltda (Telhas) . Sanidro Instala��es Hidr�ulicas Ltda (Subs�dio m�o-de-obra hidr�ulica) . Shine Service (M�o-de-obra limpeza) . Sia Com�rcio e Representa��o Ltda (Fornecimento de extintores) . Tintas Kresil Ltda (Tintas) . Vidra�aria Lajeadense Ltda	(Vidros) . Divex (desenvolvimento site)</div></td>

          <td>&nbsp;</td>

        </tr>

    </table></td>

  </tr>

</table>
</body>

</html>

