<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>

<title>Projeto Engenharia Social</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="capa.css" rel="stylesheet" type="text/css">

<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open(theURL,winName,features);

}

//-->

</script>

</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="742" border="0" cellspacing="0" cellpadding="0" style="margin:auto">

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>

          <td>&nbsp;</td>

          <td width="126"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="268" height="72" border="0"></a></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td><img src="images/imagem.gif" width="742" height="116"></td>

  </tr>

  <tr>

    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">

        <tr> 

          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>

          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>

          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>

          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>

          <td width="83"><a href="entidades.php"><IMG SRC="images/menu_05.gif" ALT="" WIDTH=83 HEIGHT=19 border="0"></a></td>

          <td width="121" bgcolor="#009933"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>

          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td width="10">&nbsp;</td>

                <td>&nbsp;</td>

                <td width="10">&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td height="13"><br></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td> <table width="75%" border="0" align="center">

                    <tr> 

                      <td><strong>A nova Casa Amarela</strong></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                    <tr> 

                      <td><p><img src="images/casa_meninos.jpg" width="150" height="184" align="right">No fim da Rua Canto e Melo, naquela casa nova atr&aacute;s dos muros, moram 14 meninos. Alguns t&ecirc;m 7 anos, outros, 17. Alguns s&atilde;o irm&atilde;os, mas todos vivem como uma fam&iacute;lia. Eles chegaram no dia 16 de dezembro, quando teve inaugura&ccedil;&atilde;o, festa e corte de fita. Essa casa faz parte de uma hist&oacute;ria que come&ccedil;ou h&aacute; quatro anos e &eacute; muito bonita. &Eacute; a hist&oacute;ria da Casa Amarela e do Projeto Engenharia Social da Capa. </p>

                        <p>Era uma vez um casal que morava na Cidade Baixa e um dia, chegando em casa, encontrou um grupo de crian&ccedil;as na cal&ccedil;ada. Ele se chama Gilmar Dal'Osto Rossa e ela, Maria Cristina Gon&ccedil;alves da Costa. Foram conversar. Cristina ficou bem impressionada com a organiza&ccedil;&atilde;o dos cinco meninos: "Cada um tinha suas coisas e s&oacute; queriam dormir ali". Pediram para tomar banho. O casal, que j&aacute; criava quatro filhos, decidiu acomodar os meninos na sala, pois a noite era muito fria. </p>

                        <p>O m&ecirc;s era agosto de 2000. No dia seguinte, come&ccedil;ou a procura de um abrigo para os cinco. "N&atilde;o pod&iacute;amos deix&aacute;-los na rua", diz Gilmar. Mas n&atilde;o havia vagas e durante 50 dias eles dormiram na sala da casa. Ainda bem que Gilmar e Cristina j&aacute; tinham alguma experi&ecirc;ncia com meninos de rua e muitos amigos, que ajudaram com comida, roupas e at&eacute; levando alguns para passear nos fins de semana. </p>

                        <p>Esses amigos ajudaram a fundar, em outubro de 2000, a Funda&ccedil;&atilde;o Instituto Recriar, uma organiza&ccedil;&atilde;o n&atilde;o-governamental (ONG) para dar um lar aos meninos, que j&aacute; eram sete. No final de setembro, haviam alugado uma casa no bairro Teres&oacute;polis. Era amarela e com duas arauc&aacute;rias na frente. Ganhou o nome de "Casa Amarela das Arauc&aacute;rias". "Quer&iacute;amos um nome que soasse po&eacute;tico, que as crian&ccedil;as n&atilde;o tivessem preconceitos", recorda Cristina. "Casa porque n&atilde;o &eacute; um abrigo, &eacute; outra filosofia. Queremos proporcionar um lar", diz Gilmar, por isso, acrescenta "o tratamento &eacute; com carinho e rigor". Escolheram um bairro residencial, que n&atilde;o fosse na periferia nem no centro, onde as crian&ccedil;as n&atilde;o sofressem preconceito. </p>

                        <p>Foi um dif&iacute;cil come&ccedil;o. Os moradores desconfiaram dos novos vizinhos, mas as crian&ccedil;as do bairro logo travaram amizade com eles e aos poucos houve entendimento. Eram apenas crian&ccedil;as brincando com outras crian&ccedil;as. Este foi o primeiro resultado do trabalho, explica Cristina: "Sendo tratados como crian&ccedil;as, tendo apoio e refer&ecirc;ncias, eles se comportam como crian&ccedil;as". </p>                        

                        <p align="justify"><strong>Uma hist&oacute;ria <br>

                          <br>

                        </strong>A Casa Amarela n&atilde;o &eacute; albergue ou abrigo e n&atilde;o pretende substituir o lar de ningu&eacute;m. O exemplo &eacute; a hist&oacute;ria de um menino que procurou a casa h&aacute; dois anos. A hist&oacute;ria dele n&atilde;o &eacute; &uacute;nica. Tr&ecirc;s irm&atilde;os, primeiro maltratados e depois abandonados pelo pai, morando em uma &uacute;nica pe&ccedil;a. Quando Cristina visitou a m&atilde;e, encontrou uma mulher depressiva, vinda do interior, sem condi&ccedil;&otilde;es sequer de telefonar para fam&iacute;lia. A iniciativa da Funda&ccedil;&atilde;o Recriar foi estabelecer contato com os agora av&oacute;s, que aceitaram ajudar filha e netos, providenciando a constru&ccedil;&atilde;o de uma casa no mesmo terreno em que t&ecirc;m a sua. "Levamos a m&atilde;e e as crian&ccedil;as at&eacute; l&aacute;", relata Gilmar, e "com dinheiro arrecadado entre os amigos compramos algumas coisas para a casa nova". Para Cristina, &eacute; isso o que a ONG pretende: "Fortalecer os la&ccedil;os familiares, ajudar sempre que poss&iacute;vel. Dos meninos que passaram aqui, nenhum voltou para rua". </p>

                        <p>O objetivo &eacute; "defender e garantir os direitos da Crian&ccedil;a e do Adolescente da melhor maneira". Por&eacute;m, o mais dif&iacute;cil sempre foi a manuten&ccedil;&atilde;o. Hoje, s&atilde;o 350 s&oacute;cios, confirmando a esperan&ccedil;a despertada em Cristina em outubro de 2000: "T&iacute;nhamos certeza que conseguir&iacute;amos aliados para esta causa". </p>

                        <p align="justify"><strong>Projeto Engenharia Social <br>

                          <br>

                        </strong>Mas existe outra hist&oacute;ria bonita para contar. &Eacute; novinha, come&ccedil;ou em 2004, quando a Capa comemorou seus 20 anos e decidiu potencializar suas a&ccedil;&otilde;es de responsabilidade social. "Elegemos, como a melhor forma para comemorar nossos 20 anos, a cria&ccedil;&atilde;o de um projeto que retribu&iacute;sse para a sociedade o acolhimento que nos deu nesses anos", diz Paulo Fam, na �poca diretor da empresa. Assim nasceu a proposta do Projeto Engenharia Social - Para Construir Esperan&ccedil;a. </p>

                        <p>"Toda nossa verba de campanha institucional para comemora&ccedil;&atilde;o dos 20 anos e mais uma parte do or&ccedil;amento anual dos pr&oacute;ximos dois anos est&atilde;o destinadas ao projeto," totalizando 500 mil reais em tr&ecirc;s anos, explica Fam. Mas o projeto da Capa n&atilde;o encerra com a assinatura de um cheque por parte de um dos diretores. Longe disso, assegura: "queremos utilizar o respaldo da Capa perante a cadeia produtiva da constru&ccedil;&atilde;o civil para conscientizar e aglutinar empresas e parceiros em torno deste e de outros projetos sociais". J&aacute; no lan&ccedil;amento oficial, em 8 de junho, na sede do Sindicato da Constru&ccedil;&atilde;o Civil, o Sinduscon, o sal&atilde;o lotado deu uma id&eacute;ia boa do apoio que a iniciativa ia receber. No final do Projeto, foram contabilizados mais de 50 parceiros, que devem continuar no ano que vem. </p>

                        <p>Alguns meses antes, o engenheiro Henrique Ledur havia apresentado a Fam a hist&oacute;ria da Casa Amarela das Arauc&aacute;rias. Impressionou bem e a equipe do Projeto Engenharia Social, que pediu os estatutos da Recriar, avaliou a entidade em conjunto com outras demandas e a elegeu como beneficiada pelo projeto em 2004. O lan&ccedil;amento oficial foi no dia 8 de junho. </p>

                        <p>Como ningu&eacute;m dessas empresas, como se dizia antigamente, &eacute; de capinar sentado, a obra iniciou rapidamente. Para simbolizar a solidariedade do Projeto Engenharia Social, no dia 12 de agosto houve um mutir&atilde;o, no qual funcion&aacute;rios da Capa misturaram cimento, carregaram carrinhos de m&atilde;o e alinharam tijolos junto com pedreiros e futuros moradores. A nova Casa Amarela estava nascendo e j&aacute; tinha nome, em homenagem a uma &aacute;rvore que est&aacute; naquele terreno h&aacute; 100 anos: Casa Amarela da Figueira. <br>

                          <br>

                          <br>

                          <b>A casa nova</b> <br>

                          <br>

                        Quando Carlos Schettert e Paulo Fam perguntaram, &agrave;s dez e meia da manh&atilde; do dia 16 de dezembro, quem queria cortar a fita, Nicolas, 7 anos, h&aacute; um residente da Casa, timidamente, se candidatou. Depois, explicou que "queria ver a casa logo". &Eacute; que eles s&oacute; tinham ido uma vez l&aacute;, antes, no dia do mutir&atilde;o. A Recriar achou melhor n&atilde;o aumentar muito a ansiedade de todos com visitas constantes. "Eu estava ansioso mesmo", confessa Rodrigo Leite Stulpen, 15 anos. "Agora, estou muito feliz". S&atilde;o quatro dormit&oacute;rios, cozinha, banheiros, duas salas multiuso e mais um sal&atilde;o. "A casa nova &eacute; boa porque tem espa&ccedil;o para trabalhar a individualidade e privacidade dos grupos diferentes", diz Gilmar. Todos estudam e no turno inverso desenvolvem alguma atividade. Rodrigo, por exemplo, joga futebol na escolinha do Inter. O irm&atilde;o dele, Roberto, tamb&eacute;m j&aacute; freq&uuml;entou a escolinha, hoje estuda ingl&ecirc;s e faz planos para o futuro: "Quero ser jogador de futebol". O irm&atilde;o duvida: "Tu reclamas muito". Eles discutem um pouco e deixam o assunto de lado. </p>

                        <p>Para Gilmar, a nova Casa Amarela simboliza a for&ccedil;a que t&ecirc;m o amor e a amizade: "A Casa expressa a solidariedade de outras pessoas. &Eacute; uma casa recheada com amor. A Capa foi o canal para que outras empresas participassem". Entre as empresas a agradecer, ele cita o Banrisul e o Banco do Brasil, que abriram d&eacute;bito em conta para a ONG, o Banco de Boston e a Gerdau. </p>

                        <p>No dia da inaugura&ccedil;&atilde;o, foi a vez da Mobiliari, revenda da Delano, aderir. Depois da visita, a representante F&aacute;tima Santos anunciou a doa&ccedil;&atilde;o de uma cozinha. </p>

                        <p>"Este &eacute; mais um la&ccedil;o que estabelecemos com a sociedade", anuncia Fam. "&Eacute; a satisfa&ccedil;&atilde;o de termos feito um pouco para diminuir o sofrimento de tantas car&ecirc;ncias sociais". Naquele momento, entre refrigerantes e salgadinhos da festa de inaugura&ccedil;&atilde;o, encerrava-se a primeira a&ccedil;&atilde;o do Projeto Engenharia Social da Capa. Existem outras entidades interessadas e se cadastrando. "Na reuni&atilde;o de mar&ccedil;o, vamos definir qual ser&aacute; a entidade com a qual vamos trabalhar em 2005". </p>

                        No hall de entrada, da nova Casa Amarela, h&aacute; um agradecimento singelo e eterno. &Eacute; um quadro do chargista Santiago, dedicado "aos amigos da Capa e da Prefeitura".

                        <p><b>A import&acirc;ncia dos parceiros</b><br>

                          <br>

Uma vez, um empres&aacute;rio visitou uma pedreira e perguntou a tr&ecirc;s oper&aacute;rios que encontrou: "O que voc&ecirc; faz?". O primeiro respondeu: "Eu quebro pedras". O segundo disse que "fazia cal&ccedil;adas" e o terceiro: "Eu ergo catedrais". O empres&aacute;rio contratou o terceiro. Cada parceiro do projeto da Casa Amarela fez mais que ajudar uma entidade de atendimento a menores; est&aacute; construindo catedrais de solidariedade. A Capa agradece a todos.</p></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                    <tr> 

                      <td><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#DEDEBD">

                          <tr> 

                            <td height="20" colspan="3"><div align="center"><strong>A nova casa :</strong></div></td>

                          </tr>

                          <tr> 

                            <td colspan="3"><div align="center"><font color="#666666">clique 

                                nas images para ampliar</font></div></td>

                          </tr>

                          <tr> 

                            <td width="180">&nbsp;</td>

                            <td width="15">&nbsp;</td>

                            <td><div align="right"></div></td>

                          </tr>

                          <tr> 

                            <td><div align="center"><a href="javascript:;" onClick="MM_openBrWindow('casa1.htm','','width=350,height=263')"><img src="images/casa1_peq.jpg" width="100" height="75" border="0"></a></div></td>

                            <td>&nbsp;</td>

                            <td><div align="center"><a href="javascript:;" onClick="MM_openBrWindow('casa2.htm','','width=350,height=263')"><img src="images/casa2_peq.jpg" width="100" height="75" border="0"></a></div></td>

                          </tr>

                          <tr> 

                            <td>&nbsp;</td>

                            <td>&nbsp;</td>

                            <td>&nbsp;</td>

                          </tr>

                          <tr> 

                            <td><div align="center"><a href="javascript:;" onClick="MM_openBrWindow('casa3.htm','','width=350,height=263')"><img src="images/casa3_peq.jpg" width="100" height="75" border="0"></a></div></td>

                            <td>&nbsp;</td>

                            <td><div align="center"><a href="javascript:;" onClick="MM_openBrWindow('casa4.htm','','width=350,height=263')"><img src="images/casa4_peq.jpg" width="100" height="75" border="0"></a></div></td>

                          </tr>

                          <tr> 

                            <td>&nbsp;</td>

                            <td>&nbsp;</td>

                            <td>&nbsp;</td>

                          </tr>

                        </table></td>

                    </tr>

                    <tr> 

                      <td>&nbsp;</td>

                    </tr>

                  </table></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>&nbsp;</td>

              </tr>

            </table></td>

          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/cadastre2.gif" width="186" height="73" border="0" usemap="#Map"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>


              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div align="center"><img src="images/26anos.gif" width="120" height="46"></div></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

            </table><div style="padding-left:8px; padding-top:8px;"><strong>Link:</strong><br><br>

            Instituto Recriar<br><a href="http://www.recriar.org.br/" target="_blank">www.recriar.org.br</a></div> 

            <map name="Map">

              <area shape="rect" coords="94,48,175,67" href="entidades.php">

            </map>

            <map name="Map2">

              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">

            </map></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td height="105" bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td><font color="#999999" size="1"><strong>Apoiadores do Projeto 2004</strong></font></td>

          <td width="10">&nbsp;</td>

        </tr>

        <tr> 

          <td>&nbsp;</td>

          <td><div align="justify"><font color="#999999" size="1">Cimpor Brasil 

              &#8211; Cimentos :: Portinari &#8211; Pisos e azulejos :: Renner 

              e Kresil &#8211; Tintas :: Sider&uacute;rgica Belgo Mineiro &#8211; 

              A&ccedil;o :: Siemens &#8211; Materiais El&eacute;tricos :: Fechabem 

              &#8211; Fechaduras :: D&#8217;Coberlini &#8211; M&aacute;rmores 

              e granitos :: Sanidro &#8211; Instala&ccedil;&otilde;es Hidr&aacute;ulicas 

              :: Pauluzzi &#8211; Blocos Cer&acirc;micos Instaladora Base &#8211; 

              Instala&ccedil;&otilde;es El&eacute;tricas :: Pedreira Vila Rica 

              &#8211; Pedras e britas :: Fabrimar &#8211; Metais sanit&aacute;rios 

              :: Conquistar - Areia :: Vidrobox &#8211; Vidros :: WD &#8211; Madeiras 

              :: Imperterm - Impermeabiliza&ccedil;&atilde;o :: Alquadros &#8211; 

              Esquadria de alum&iacute;nio :: Conduspar &#8211; Fios e cabos :: 

              Arte &amp; M&oacute;veis &#8211; Esquadria de madeira :: Torres 

              &amp;Crist&oacute;vam &#8211; M&atilde;o de obra :: Celter &#8211; 

              M&atilde;o de obra :: Transportadora Lou&ccedil;assinos &#8211; 

              Lou&ccedil;as sanit&aacute;rias :: Hickmann &#8211; Esquadrias de 

              Madeira :: Betina Chede - Arquitetura :: Graphitar - Maquetes :: 

              Divex - Internet :: Thpromoth - Propaganda :: L&aacute;bor - Im&oacute;veis 

              :: Inove Solu&ccedil;&otilde;es - Comunica&ccedil;&atilde;o visual 

              :: Nichele - Entulhos <br>

              </font></div></td>

          <td>&nbsp;</td>

        </tr>

      </table></td>

  </tr>

</table>

</body>

</html>

