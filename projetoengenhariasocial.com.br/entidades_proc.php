<%
on error resume next
%>
<!--#INCLUDE FILE="_includes/clsUpload.php"-->
<%
Dim objUpload
Dim strFileName
Dim strPath

' Instantiate Upload Class
Set objUpload = New clsUpload

' Grab the file name
strFileName = objUpload.Fields("arquivo").FileName

' Compile path to save file to
strPath = Server.MapPath("Arquivos") & "\" & strFileName

' Save the binary data to the file system
objUpload("arquivo").SaveAs strPath

' Release upload object from memory
Set objUpload = Nothing
Response.Write "<script>window.opener.document.forms[0].arquivo.value='"&strFileName&"';window.close();</script>"
%>