<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>

<title>Projeto Engenharia Social</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="capa.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_js/prototype.js"></script>

<script type="text/javascript" src="_js/scriptaculous.js?load=effects,builder"></script>

<script type="text/javascript" src="_js/lightbox.js"></script>

<link rel="stylesheet" href="_css/lightbox.css" type="text/css" media="screen" />

<SCRIPT LANGUAGE="JavaScript" src="_includes/js_funcoes.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

<!--

function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open(theURL,winName,features);

}



function MM_displayStatusMsg(msgStr) { //v1.0

  status=msgStr;

  document.MM_returnValue = true;

}

//-->

</script>

<script language="JavaScript" type="text/JavaScript">

function ocultaLayer(p){

       if (p == 'a'){

               document.getElementById('vila').style.display = '';

       }

       if (p == 'f'){

               document.getElementById('vila').style.display = 'none';

       }

}

</script>

<style type="text/css">

<!--

.style5 {color: #999999}

-->

</style>

</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="742" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="251"><a href="default.php"><img src="images/top_01.gif" width="251" height="72" border="0"></a></td>

          <td>&nbsp;</td>

          <td width="126" valign="middle"><div align="center"><a href="http://www.rodobens.com/" target="_blank"><img src="images/rodobens.gif" width="100" height="46" border="0"></a></div></td>

        <td width="20">&nbsp;</td>

		<td width="154"><div align="center"><a href="http://www.capa.com.br" target="_blank"><img src="images/top_02.gif" width="126" height="72" border="0"></a></div></td>

		</tr>

      </table></td>

  </tr>

  <tr>

    <td><img src="images/bannertopo.jpg" width="742" height="116"></td>

  </tr>

  <tr>

    <td><table width="742" height="19" border="0" cellpadding="0" cellspacing="0">

        <tr> 

          <td width="129"><a href="capa.php"><IMG SRC="images/menu_01.gif" ALT="" WIDTH=129 HEIGHT=19 border="0"></a></td>

          <td width="80"><a href="oprojeto.php"><IMG SRC="images/menu_02.gif" ALT="" WIDTH=80 HEIGHT=19 border="0"></a></td>

          <td width="91"><a href="apoiadores.php"><IMG SRC="images/menu_03.gif" ALT="" WIDTH=91 HEIGHT=19 border="0"></a></td>

          <td width="121"><a href="entidade_ano.php"><img src="images/menu_07.gif" width="121" height="19" border="0"></a></td>

          <td width="83"><a href="http://www.nexgroup.com.br/contato" target="_blank"><img src="images/menu_08.gif" width="121" height="19" border="0"></a></td>

          <td width="121" bgcolor="#009933"></td>

          <td bgcolor="#009933"><div align="right"><a href="default.php"><img src="images/home.gif" width="50" height="19" border="0"></a></div></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td valign="top" bgcolor="#99CC99"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td width="10">&nbsp;</td>

                <td>&nbsp;</td>

                <td width="10">&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td height="13"><br></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td> <table width="520" border="0" align="center">

                    <tr id="vila" style="display:;"> 

                      <td align="right">                  </td>

                    </tr>

                     <tr>

                      <td valign="top" class="instituicao_2008">                       <div align="center">GRUPO CAPA E RODOBENS NEG�CIOS IMOBILI�RIOS DOARAM MAIS UMA OBRA PARA A POPULA��O CARENTE DE PORTO ALEGRE </div></td>

                    </tr>

                    <tr>

                      <td valign="top" >                       <div align="justify">A Capa Engenharia e a Rodobens Neg�cios Imobili�rios entregaram oficialmente no dia 11 de mar�o de 2008 mais uma obra � popula��o carente de Porto Alegre. Ao todo desde o in�cio do projeto de 2004, j� s�o 4 creches entregues.

                        

                          <br>

                          <br>

                          Em parceria com a Prefeitura foi entregue a Escola de Educa��o Infantil S�o Guilherme no bairro Partenon, em uma �rea de 285 m2, para 120 crian�as de 0 a 6 anos de idade. A constru��o faz parte do Projeto Engenharia Social das Incorporadoras e teve um or�amento em torno de R$ 450 mil.

                        O Projeto contou com o envolvimento da cadeia produtiva como fornecedores e parceiros do Grupo para realiza��o da obra, al�m da participa��o efetiva da Rodobens Neg�cios Imobili�rios. Al�m disso, atrav�s do Projeto Construtor da Esperan�a arrecadou mais de R$ 4 mil para equipagem da creche. 

                        

                        

                        <br>

                        <br>

                      Estavam presentes na entrega, Carlos Alberto Schettert, diretor-presidente do Grupo Capa, Eduardo Gorayeb, diretor-presidente da Rodobens Neg�cios Imobili�rios, o Prefeito de Porto Alegre, Jos� Foga�a, a primeira-dama, Isabela Foga�a, participantes do GT Creches, colaboradores da Capa Engenharia e v�rias outras entidades. Al�m desses convidados contamos tamb�m com a participa��o de v�rios parceiros, fornecedores e da comunidade local.</div></td>

                    </tr>

                    <tr>

                      <td valign="top" class="instituicao_2008"><img src="images/creche_2008/galeria.gif" width="160" height="152" hspace="5" vspace="5" align="left"><br>

                        <br>

                        <br>

                        <br>

                        Confira a galeria da entrega da obra<br></td>

                    </tr>

                      <tr>

                      <td valign="top" class="instituicao_2008"><table width="400" border="0" align="center" cellpadding="3" cellspacing="0">

                        <tr>

                          <td><a href="images/creche_2008/foto1.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto1_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto2.JPG" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto2_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto3.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto3_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto4.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto4_th.gif" width="103" height="78" border="0"></a></td>

                        </tr>

                        <tr>

                          <td align="center"><a href="images/creche_2008/foto5.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto5_th.gif" width="66" height="98" border="0"></a></td>

                          <td align="center"><a href="images/creche_2008/foto6.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto6_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto7.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto7_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto8.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto8_th.gif" width="103" height="78" border="0"></a></td>

                        </tr>

                        <tr>

                          <td><a href="images/creche_2008/foto9.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto9_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto10.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto10_th.gif" width="103" height="78" border="0" ></a></td>

                          <td><a href="images/creche_2008/foto12.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto12_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto11.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto11_th.gif" width="103" height="78" border="0"></a></td>

                        </tr>

                        <tr>

                          <td>&nbsp;</td>

                          <td>&nbsp;</td>

                          <td>&nbsp;</td>

                          <td>&nbsp;</td>

                        </tr>

                        <tr>

                          <td><a href="images/creche_2008/foto13.jpg" rel="lightbox[roadtrip]"> <img src="images/creche_2008/foto13_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto14.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto14_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto15.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto15_th.gif" width="103" height="78" border="0"></a></td>

                          <td><a href="images/creche_2008/foto16.JPG" rel="lightbox[roadtrip]"><img src="images/creche_2008/foto16_th.gif" width="103" height="78" border="0"></a></td>

                        </tr>

                      </table>

                        <br>

                        <br></td>

                    </tr>

                    <tr>

                      <td> <p align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript://;" onClick="MM_openBrWindow('images/vilapinto9_big.jpg','vilapinto6','resizable=yes,width=550,height=420')" onMouseOver="MM_displayStatusMsg('Clique para ampliar a foto');return document.MM_returnValue">&nbsp;&nbsp;&nbsp;</a><span class="instituicao_2008">Veja como foi o andamento da obra </span><br>

				      </p>                        </td>

                    </tr>

                    <tr> 

                      <td><div align="center">

                        <table width="400" border="0" cellspacing="0" cellpadding="0">

                          <tr>

                            <td><div align="center"><a href="images/creche_2008/obra1_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra1_pq.gif" width="152" height="104" border="0"></a></div></td>

                            <td><div align="center"><a href="images/creche_2008/obra2_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra2_pq.gif" width="152" height="104" border="0"></a></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"></div></td>

                            <td><div align="center"></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"><a href="images/creche_2008/obra5_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra5_pq.gif" width="152" height="104" border="0"></a></div></td>

                            <td><div align="center"><a href="images/creche_2008/obra6_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra6_pq.gif" width="152" height="104" border="0"></a></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"></div></td>

                            <td><div align="center"></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"><a href="images/creche_2008/obra7_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra7_pq.gif" width="152" height="104" border="0"></a></div></td>

                            <td><div align="center"><a href="images/creche_2008/obra8_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra8_pq.gif" width="152" height="104" border="0"></a></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"></div></td>

                            <td><div align="center"></div></td>

                            </tr>

                          <tr>

                            <td><div align="center"><a href="images/creche_2008/obra3_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra3_pq.gif" width="152" height="104" border="0"></a></div></td>

                            <td><div align="center"><a href="images/creche_2008/obra4_gr.jpg" rel="lightbox[roadtrip]"><img src="images/creche_2008/obra4_pq.gif" width="152" height="104" border="0"></a></div></td>

                            </tr>

                        </table>

                      </div></td>

                    </tr>

                </table></td>

                <td>&nbsp;</td>

              </tr>

              <tr> 

                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>&nbsp;</td>

              </tr>

            </table></td>

          <td width="186" valign="top" bgcolor="#DEDEBD"><table width="186" border="0" cellspacing="0" cellpadding="0">

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

             

             

             <!-- 

             

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/cadastre.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div style="padding:5%; font-size: 9;"><font face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><a href="entidades.php">Aberta a sele��o de entidades para 2009. </a></font></div> </td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td width="186" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/home_06.gif" width="186" height="20"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><a href="http://aspserver.divex.com.br/smartfriend/Recomenda.asp?CodEmpresa=2&CodProd=34&CodItem=1&url=http://www.projetoengenhariasocial.com.br" target="_blank"><img src="images/home_07.gif" width="186" height="74" border="0"></a></td>

              </tr>

              

               -->

              

              <tr> 

                <td bgcolor="#C0E7BA"><p><img src="images/novidades.gif" width="186" height="20"></p></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><img src="images/novidades3.gif" width="186" height="66" border="0" usemap="#Map2"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA" align="center"> </td>

              </tr>

              <tr> 

                <td height="1" bgcolor="#FFFFFF"></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA"><div align="center"><a href="http://www.capa.com.br" target="_blank"><img src="images/26anos.gif" width="120" height="46" border="0"></a></div></td>

              </tr>

              <tr>

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr> 

                <td background="images/linha.gif" bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

              <tr>

                <td bgcolor="#C0E7BA"><div align="center">

                  <p class="construtor"><strong>Participe do Projeto Construtor

                    da Esperan&ccedil;a</strong></p>

                  <p><a href="http://www.construtordaesperanca.com.br" target="_blank"><img src="images/logo_construtor.gif" width="90" height="138" border="0"></a></p>

                </div></td>

              </tr>

              <tr> 

                <td bgcolor="#C0E7BA">&nbsp;</td>

              </tr>

            </table>

          <div style="padding-left:8px; padding-top:8px;"><strong>Link:</strong><br><br>

            Associa��o Beneficente Nossa Senhora da Assun��o <br><a href="http://www.abensa.com.br/" target="_blank">www.abensa.com.br</a></div> 

            <map name="Map2">

              <area shape="rect" coords="93,41,176,63" href="http://www.nexgroup.com.br" target="_blank">

            </map></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td valign="top" bgcolor="#003300"><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr> 

          <td width="10">&nbsp;</td>

          <td><font color="#999999" size="2"><strong>Apoiadores do Projeto 2008<br>

          </strong></font></td>

          <td width="10">&nbsp;</td>

        </tr>

        <tr> 

          <td>&nbsp;</td>

          <td valign="top"><div align="justify" class="style5">Alusistem - Alum�nio para Constru��o Civil Ltda (Beneficiamento de alum�nio).Amanco - Brasil Ltda(Material hidr�ulico).Arcelor Mittal Brasil S/A(Tela + a�o radier) .Arte M�veis(Portas de madeira e guarni��es).Baumgarten Com�rcio & Instala��o em Sistema Drywall(M�o-de-obra forro de gesso) .Belmetal Ind�stria e Com�rcio Ltda(Alum�nio para esquadrias). Blumengarten Plantas e Flores Ltda	(Plantas e Flores) . Brocca Correa Ltda(M�o-de-obra radier) . Cecrisa Revestimentos Cer�micos Ltda	(Piso cer�mico e azulejos) .Cia Nacional do A�o Ind�stria e Com�rcio(Tela + a�o radier) . Cimatex materiais de constru��o Ltda (Cimento) . CCB- Cimpor Cimentos do Brasil Ltda (Concreto) . Creel-Sul Com�rcio de Equipamentos el�tricos Ltda (Material el�trico) . Deca	(Lou�as) . Design Decora��es Ltda (M�o-de-obra piso laminado) . 

Di Luce -Fonte de Luz Ilumina��o Ltda (Lumin�rias) . Docol Metais Sanit�rios Ltda (Metais) . Duratex S.A (Piso laminado) . Elektra Distribuidora (Material el�trico e lumin�rias externas) . Eletric-Sul com�rcio de materiais el�tricos Ltda (Material el�trico) . Empreiteira de obras Teixeira & Silva(M�o-de-obra pisos e azulejos cer�micos) . Esquadrias Pintz Ltda (Port�o de entrada ve�culos e port�o reservat�rio superior) . Est�dio 3 Engenharia de estuturas Ltda (Projeto Radier) . Faber Mobilis Industria de M�veis Ltda (Banco para deficiente) . Fechart Comercio de Fechaduras Ltda	(Fornecimento de fechaduras e dobradi�as) . Fibrafort Piscinas Ltda (Reservat�rios) . Giancarlo C�ndido (M�o-de-obra pintura ) . Golder Constru��es Ltda	(M�o-de-obra alvenaria, reboco e cobertura) . Hydrus Projetos de Instala��es Sanit�rias (Projeto Hidrossanit�rio) . Igua�u Pr�-fabricados de Granito Ltda	(Peitoris de Janelas, tampo granito cozinha e banheiros) . Inap -Ind�stria Nacional de A�o Pronto (Corte e dobra a�o radier) . Instaladora Base LTDA (Subs�dio m�o-de-obra el�trica) . Irm�os Ciocari Ltda (Argamassa de assentamento) . Knauf do Brasil Ltda (forro de gesso) . Ludwig Herrmann Ltda (Limpeza do terreno e aterro) . MDR-Com�rcio Ferragem e Materiais de Constru��o (Vasos sanit�rios, chuveiros e materiais hidr�ulicos) . Metal�rgica Borba Ind�stria e com�rcio Ltda (Caixilhos de piso, port�o para pedestres e port�o de g�s) . Metal�rgica Visconti Ltda (Material el�trico) . Milititsky Consultoria Geot�cnica� Engenheiros Assoc.S/S Ltda	(Estudo viabilidade projeto Radier) . Multicabos distribuidora de materiais el�tricos Ltda (Material el�trico) . Norte Pisos Ltda (Acabamento de piso de concreto, rampas e passeios) . Pauluzzi Produtos Cer�micos Ltda (Blocos cer�micos) . Plenobr�s Distribuidora el�trica Ltda (Material el�trico) .

Rampa Compensados Ltda	(Compensado) . Ricardo Lemos da Costa ME (Enleivamento e plantio de mudas e plantas) . Saint Gobain Brasilit Ltda (Telhas) . Sanidro Instala��es Hidr�ulicas Ltda (Subs�dio m�o-de-obra hidr�ulica) . Shine Service (M�o-de-obra limpeza) . Sia Com�rcio e Representa��o Ltda (Fornecimento de extintores) . Tintas Kresil Ltda (Tintas) . Vidra�aria Lajeadense Ltda	(Vidros) . Divex (desenvolvimento site)</div></td>

          <td>&nbsp;</td>

        </tr>

    </table></td>

  </tr>

</table>

</body>

</html>

