<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<ul class="FeaturedItems ui-tabs-nav">
					<li id="nav-item-1" class="ui-tabs-nav-item ui-tabs-selected"><h3><a href="#item-1" title="Alameda">Alameda</a></h3></li>
					<li id="nav-item-1" class="ui-tabs-nav-item"><h3><a href="#item-2" title="Fachada 1A / 1B">Fachada 1A / 1B</a></h3></li>
					<li id="nav-item-2" class="ui-tabs-nav-item"><h3><a href="#item-3" title="Fachada 2A / 2B">Fachada 2A / 2B</a></h3></li>
					<li id="nav-item-3" class="ui-tabs-nav-item"><h3><a href="#item-4" title="Fachada 3A / 3B">Fachada 3A / 3B</a></h3></li>
					<li id="nav-item-4" class="ui-tabs-nav-item"><h3><a href="#item-5" title="Fachada 4A / 4B">Fachada 4A / 4B</a></h3></li>
					<li id="nav-item-5" class="ui-tabs-nav-item"><h3><a href="#item-6" title="Planta baixa 3 dormitórios">Planta baixa 3 dorms.</a></h3></li>
					<li id="nav-item-6" class="ui-tabs-nav-item"><h3><a href="#item-7" title="Planta baixa 2 suítes">Planta baixa 2 suítes</a></h3></li>
				</ul> <!-- .FeaturedItems -->
				<div class="ImagePreview">
					<div id="item-1" class="ui-tabs-panel"><img src="<?=base_url()?>assets/img/site/alameda.jpg" alt="Alameda"  /></div>
					<div id="item-2" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/fachada-1a-1b.jpg" alt="Fachada 1A / 1B"  /></div>
					<div id="item-3" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/fachada-2a-2b.jpg" alt="Fachada 2A / 2B"  /></div>
					<div id="item-4" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/fachada-3a-3b.jpg" alt="Fachada 3A / 3B"  /></div>
					<div id="item-5" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/fachada-4a-4b.jpg" alt="Fachada 4A / 4B"  /></div>
					<div id="item-6" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/planta-baixa-3dorms.jpg" alt="Planta baixa 3 dormitórios"  /></div>
					<div id="item-7" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/planta-baixa-2suites.jpg" alt="Planta baixa 2 suítes"  /></div>
				</div> <!-- .ImagePreview -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>