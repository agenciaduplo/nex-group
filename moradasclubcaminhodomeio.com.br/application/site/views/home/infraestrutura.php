<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<ul class="FeaturedItems ui-tabs-nav">
					<li id="nav-item-1" class="ui-tabs-nav-item ui-tabs-selected"><h3><a href="#item-1" title="Condomínio fechado com portaria central">Empreendimento</a></h3></li>
					<li id="nav-item-2" class="ui-tabs-nav-item"><h3><a href="#item-2" title="Piscina Adulto e Infantil">Portaria Central</a></h3></li>
					<li id="nav-item-3" class="ui-tabs-nav-item"><h3><a href="#item-3" title="Salão de Festas">Salão de Festas</a></h3></li>
					<li id="nav-item-3" class="ui-tabs-nav-item"><h3><a href="#item-4" title="Piscina Adulto e Infantil">Piscina Adulto e Infantil</a></h3></li>
					<li id="nav-item-4" class="ui-tabs-nav-item"><h3><a href="#item-5" title="Quiosque com churrasqueiras">Quiosque com churrasqueiras</a></h3></li>
					<li id="nav-item-5" class="ui-tabs-nav-item"><h3><a href="#item-6" title="Minicampo de Futebol Gramado">Minicampo de Futebol Gramado</a></h3></li>
					<li id="nav-item-6" class="ui-tabs-nav-item"><h3><a href="#item-7" title="Playground">Playground</a></h3></li>
					<!--<li id="nav-item-7" class="ui-tabs-nav-item"><h3><a href="#item-7" title="Vagas de estacionamento">Vagas de estacionamento</a></h3></li>
					<li id="nav-item-8" class="ui-tabs-nav-item"><h3><a href="#item-8" title="Comunicação entre casas e portaria">Comunicação entre casas e portaria</a></h3></li>-->
				</ul> <!-- .FeaturedItems -->
				<div class="ImagePreview">
					<div id="item-1" class="ui-tabs-panel"><img src="<?=base_url()?>assets/img/site/infra-empreendimento.jpg" alt="Condomínio fechado com portaria central"   /></div>
					<div id="item-2" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-portaria-central.jpg" alt="Piscina Adulto e Infantil"   /></div>
					<div id="item-3" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-salao-de-festas.jpg" alt="Salão de Festas"   /></div>
					<div id="item-4" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-piscina-adulto-infantil.jpg" alt="Piscina Adulto e Infantil"   /></div>
					<div id="item-5" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-quiosque-com-churrasqueira.jpg" alt="Quiosque com churrasqueiras"   /></div>
					<div id="item-6" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-minicampo-futebol-gramado.jpg" alt="Minicampo de Futebol Gramado"   /></div>
					<div id="item-7" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-playground.jpg" alt="Playground"   /></div>
					<!--<div id="item-7" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-vagas-estacionamento.jpg" alt="Vagas de estacionamento"   /></div>
					<div id="item-8" class="ui-tabs-panel ui-tabs-hide"><img src="<?=base_url()?>assets/img/site/infra-comunicacao-casas-portaria.jpg" alt="Comunicação entre casas e portaria"   /></div>-->
				</div> <!-- .ImagePreview -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>