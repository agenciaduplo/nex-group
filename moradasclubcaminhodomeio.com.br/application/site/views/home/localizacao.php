<?=$this->load->view('includes/header');?>

		<div class="Content">
			<div class="Featured">
				<div class="Sidebar">
					<p class="Location">Continuação da Av. Protásio Alves Estrada Caminho do Meio, 5425 Alvorada - RS </p>
					<p class="Last">Breve Plantão de Vendas e Casas Decoradas no Local:</p>
					<p class="Last">(51) 3435 4673</p>
					<a href="http://maps.google.com/maps?q=Estrada+Caminho+do+Meio,+5425+Alvorada+-+RS&hl=pt-BR&ll=-30.045953,-51.082199&spn=0.00977,0.033023&sll=-30.045652,-51.09267&sspn=0.00977,0.033023&vpsrc=0&z=16" class="GoogleMaps Imgr" rel="external" title="Google Maps">Google Maps</a>
				</div> <!-- .Sidebar -->
				<div class="ImagePreview">
					<img src="<?=base_url()?>assets/img/site/mapa.jpg" alt="R. Pedro Santini, próximo à Sede Campestre do Clube Dores Santa Maria" class="Map" />
				</div> <!-- .ImagePreview -->
			</div> <!-- .Featured -->
		</div> <!-- .Content -->

<?=$this->load->view('includes/footer');?>