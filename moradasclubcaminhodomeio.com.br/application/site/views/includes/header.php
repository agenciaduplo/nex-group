<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	
    <title>Moradas Club - Casas de 3 dormitórios ou 2 suítes em condomínio fechado em Porto Alegre</title>
    <meta name="description" content="Moradas Club - Casas de 3 dormitórios ou 2 suítes à venda em condomínio fechado localizado na continuação da Av. Protásio Alves, Estrada Caminho do Meio, 5425 - Porto Alegre. Realização Moradas Rodobens, Rodobens Negócios Imobiliários e Capa Max" />
    <meta name="keywords" content="condominio fechado, condominio de casas, casas 3 dorms, casas 3 dormitorios a venda, casas 3 dormitorios a venda em porto alegre, casas a venda na protasio alves, casas 2 suites, casas 2 suites a venda" />
    <meta name="author" content="Divex Imobi - http://www.imobi.divex.com.br" />
    <meta name="robots" content="index, follow" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="google-site-verification" content="njCo-jb6LwSteVntQFCtP8GfPZw31lw_loaPWOiJPiQ" />
	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/navigation.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/pages.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/orbit-1.2.3.css" type="text/css" media="screen" />
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->
	
	<?=$this->load->view('includes/scripts');?>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-37']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	
</head>
<?php
 
	@$utm_source 	= @$_GET['utm_source'];
	@$utm_medium 	= @$_GET['utm_medium'];
	@$utm_content 	= @$_GET['utm_content'];
	@$utm_campaign 	= @$_GET['utm_campaign'];
	
	@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
	
	@session_cache_expire(1440);
	@session_start();
	
	if(!@$_SESSION['origem'])
	{
		@$_SESSION['origem']	= @$_SERVER['HTTP_REFERER'];
	}
	if(!@$_SESSION['url'])
	{
		@$_SESSION['url']		= @$url;
	}

?>
<body class="<?=@$bodyClass?>">

	<div class="Main">
		<div class="Header">
			<div class="Container">
				<div class="Topbar">
					<h1 class="Logo Imgr">Moradas Clube - Santa Maria - 3 dormitórios ou 2 suítes em condomínio fechado</h1>
					<ul class="Features">
						<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=20','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor Imgr" title="Corretor online">Corretor online</a></li>
						<li><a href="#" class="BotaoPlantao Imgr" title="Plantão de vendas">Plantão de vendas</a></li>
						<li><a href="#" class="BotaoInteresse Imgr" title="Tenho interesse">Tenho interesse</a></li>
						<li><a href="#" class="BotaoIndique Imgr" title="Indique para um amigo">Indique para um amigo</a></li>
					</ul> <!-- .Features -->
				</div> <!-- .Topbar -->
				<strong class="Emphasis Imgr">3 dormitórios ou 2 suítes em condomínio fechado</strong>
				<h2 class="Slogan Imgr">Complete sua vida com a felicidade</h2>
				<p class="Price Imgr">Prestação mensal a partir de R$ 581,00*</p>
			</div> <!-- .Container -->
		</div> <!-- .Header -->

		<div class="Container">
			<ul class="Navigation">
				<li class="ItemHome"><a href="<?=base_url()?>" title="Home">Home</a></li>
				<li class="ItemInfraestrutura"><a href="<?=base_url()?>infraestrutura" title="Infraestrutura">Infraestrutura</a></li>
				<li class="ItemCasas"><a href="<?=base_url()?>conheca-as-casas" title="Conheça as casas">Conheça as casas</a></li>
				<li class="ItemLocalizacao"><a href="<?=base_url()?>localizacao" title="Localização">Localização</a></li>
				<li class="ItemObras"><a href="http://www.nexgroup.com.br/imoveis/30/moradas-club-alvorada-rio-grande-do-sul" rel="external" title="Acompanhe as obras">Acompanhe as obras</a></li>
				<li class="ItemContato"><a href="<?=base_url()?>contato" title="Contato">Contato</a></li>
			</ul> <!-- .Navigation -->
		</div> <!-- .Container -->

		<div id="PainelPlantaoVendas" class="SlidePanel">
			<div class="Container">
				<span class="Tel Imgr">(55) 3223 5660 / (55) 2103 8210</span>
				<h5 class="BotaoPlantao Imgr">Plantão de vendas</h5>
				<a href="#" class="BotaoFechar Imgr" title="Fechar">Fechar</a>
			</div> <!-- .Container -->
		</div> <!-- #PainelPlantaoVendas -->

		<div id="PainelTenhoInteresse" class="SlidePanel">
			<div class="Container">
				<form id="FormInteresse" class="Form" method="post" onsubmit="return validaInteresse();">
					<ul class="First">
						<li>
							<label for="Nome">nome</label>
							<input type="text" id="Nome" class="CampoPadrao" name="Nome" />
						</li>
						<li>
							<label for="FaixaEtaria" class="LabelFaixaEtaria">faixa etária</label>
							<select id="FaixaEtaria" class="CampoPadrao" name="FaixaEtaria">
								<option value="Até 30 anos">At&eacute; 30 anos</option>
								<option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                <option value="Mais de 50 anos">Mais de 50 anos</option>
							</select>
						</li>
						<li>
							<label for="EstadoCivil" class="LabelEstadoCivil">estado civil</label>
							<select id="EstadoCivil" class="CampoPadrao" name="EstadoCivil">
								<option value="Casado">Casado</option>
                                <option value="Solteiro">Solteiro</option>
							</select>
						</li>
						<li>
							<label for="Cidade" class="LabelBairro">cidade</label>
							<input type="text" id="Cidade" class="CampoPadrao" name="Cidade" />
						</li>
						<li>
							<label for="Profissao">profissão</label>
							<input type="text" id="Profissao" class="CampoPadrao" name="Profissao" />
						</li>
					</ul>
					<ul class="Left Second">
						<li class="PL28">
							<label for="Telefone">telefone</label>
							<input type="text" id="Telefone" class="CampoPadrao" name="Telefone" />
						</li>
						<li class="PL28">
							<label for="Email">e-mail</label>
							<input type="text" id="Email" class="CampoPadrao" name="Email" />
						</li>
						<li>
							<div class="CheckGroup">
								<span class="LabelOptions">formas de contato</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkTelefone" name="ChkTelefone" class="Check" value="Telefone" />
									<label for="ChkTelefone" class="Choice">telefone</label>
								</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkEmail" name="ChkEmail" class="Check" value="E-mail" />
									<label for="ChkEmail" class="Choice">e-mail</label>
								</span>
							</div> <!-- .CheckGroup -->
						</li>
						<li class="PL28 Clear">
							<label for="Comentario">comentário</label>
							<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="Comentario" cols="40" rows="8"></textarea>
						</li>
					</ul>
					<div class="Submitbar">
						<span style="display:none;" id="MensagemErro" class="MensagemErro"><strong>Nome</strong>, <strong>e-mail</strong> e <strong>telefone</strong> são obrigatórios.</span>
						<span style="display:none;" class="MensagemSucesso" id="MsgSucesso"></span>
						<input type="submit" id="Enviar" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
				<h5 class="BotaoInteresse Imgr">Tenho interesse</h5>
				<a href="#" class="BotaoFechar Imgr" title="Fechar">Fechar</a>
			</div> <!-- .Container -->
		</div> <!-- #PainelTenhoInteresse -->

		<div id="PainelIndique" class="SlidePanel">
			<div class="Container">
				<form id="FormIndique" class="Form" method="post" onsubmit="return validaIndique();">
					<ul class="First">
						<li>
							<label for="NomeRemetente">nome</label>
							<input type="text" id="NomeRemetente" class="CampoPadrao" name="NomeRemetente" />
						</li>
						<li>
							<label for="EmailRemetente">e-mail</label>
							<input type="text" id="EmailRemetente" class="CampoPadrao" name="EmailRemetente" />
						</li>
					</ul>
					<ul class="Left Second">
						<li>
							<label for="NomeAmigo">nome do amigo</label>
							<input type="text" id="NomeAmigo" class="CampoPadrao" name="NomeAmigo" />
						</li>
						<li>
							<label for="EmailAmigo">e-mail do amigo</label>
							<input type="text" id="EmailAmigo" class="CampoPadrao" name="EmailAmigo" />
						</li>
					</ul>
					<div class="ContainerComentario">
						<label for="Comentario2">comentário</label>
						<textarea id="Comentario2" class="ComentarioIndique CampoPadrao" name="Comentario2" cols="40" rows="8"></textarea>
					</div>
					<div class="Submitbar">
						<span style="display:none;" id="MensagemErroIndique" class="MensagemErroIndique">Preencha todos os campos.</span>
						<span style="display:none;" id="MsgSucessoIndique" class="MensagemSucesso"></span>
						<input type="submit" id="EnviarIndique" class="BotaoEnviar Imgr" name="Enviar" value="" title="Enviar" />
					</div>
				</form> <!-- .Form -->
				<h5 class="BotaoIndique Imgr">Indique para um amigo</h5>
				<a href="#" class="BotaoFechar Imgr" title="Fechar">Fechar</a>
			</div> <!-- .Container -->
		</div> <!-- #PainelIndique -->