	<script>
		function validaInteresse()
		{
			var nome			= $("#Nome").val();
			var faixa_etaria	= $("#FaixaEtaria").val();
			var estado_civil	= $("#EstadoCivil").val();
			var bairro_cidade	= $("#Cidade").val();
			var profissao		= $("#Profissao").val();
			var telefone		= $("#Telefone").val();
			var email			= $("#Email").val();
			var comentarios		= $("#Comentario").val();
			
			if($('#ChkEmail').is(':checked') && $('#ChkTelefone').is(':checked'))
			{
				var contato = "E-mail e Telefone";
			}
			else if ($('#ChkEmail').is(':checked'))
			{
				var contato = "E-mail";
			}
			else if ($('#ChkTelefone').is(':checked'))
			{
				var contato = "Telefone";
			}
			
			if(nome == "" || email == "" || telefone == "")
			{
				$("#MsgSucesso").fadeOut(function () {
					$("#MensagemErro").fadeIn();
				});
				return false;
				
			}else if(!checkMail(email)){
				$("#MsgSucesso").fadeOut(function () {
					$("#MensagemErro").fadeIn().html("Email Inválido");
				});
				return false;
			}
			else
			{
				$("#Enviar").attr("disabled","disabled");
				$("#MensagemErro").fadeOut();

				var ajaxview = "interesse";
				
				var msg 	= '';
				vet_dados 	= 'nome='+ nome
							  +'&faixa_etaria='+ faixa_etaria
							  +'&estado_civil='+ estado_civil
							  +'&bairro_cidade='+ bairro_cidade
							  +'&profissao='+ profissao
							  +'&telefone='+ telefone
							  +'&email='+ email
							  +'&contato='+ contato
							  +'&comentarios='+ comentarios
							  +'&ajaxview='+ ajaxview;
							  
				base_url  	= "http://www.moradasclubcaminhodomeio.com.br/home/enviarInteresse";
				
				$.ajax({
					type: "POST",
					url: base_url,
					data: vet_dados,
					success: function(msg) {
							$("#MsgSucesso").fadeIn();
							$("#MsgSucesso").html(msg);
							$("#Enviar").removeAttr("disabled");
							limpaCampos("FormInteresse");
							}
				});
				return false;
			}
		}
		
		function validaIndique()
		{
			var nome_remetente		= $("#NomeRemetente").val();
			var email_remetente		= $("#EmailRemetente").val();
			var nome_amigo			= $("#NomeAmigo").val();
			var email_amigo			= $("#EmailAmigo").val();
			var comentarios			= $("#Comentario2").val();
			
			if(nome_remetente == "" || email_remetente == "" || nome_amigo == "" || email_amigo == "")
			{
				$("#MsgSucessoIndique").fadeOut(function () {
					$("#MensagemErroIndique").fadeIn();
				});
				return false;
			}else if(!checkMail(email_remetente)){
				$("#MsgSucessoIndique").fadeOut(function () {
					$("#MensagemErroIndique").fadeIn().html("Email Remetente Inválido");
					
				});
				return false;
			}else if(!checkMail(email_amigo)){
				$("#MsgSucessoIndique").fadeOut(function () {
					$("#MensagemErroIndique").fadeIn().html("Email Amigo Inválido");
					
				});
				return false;
			}
			else
			{
				$("#EnviarIndique").attr("disabled","disabled");
				$("#MensagemErroIndique").fadeOut();
				
				var msg 	= '';
				vet_dados 	= 'nome_remetente='+ nome_remetente
							  +'&email_remetente='+ email_remetente
							  +'&nome_amigo='+ nome_amigo
							  +'&email_amigo='+ email_amigo
							  +'&comentarios='+ comentarios;
							  
				base_url  	= "http://www.moradasclubcaminhodomeio.com.br/home/enviarIndique";
				
				$.ajax({
					type: "POST",
					url: base_url,
					data: vet_dados,
					success: function(msg) {
							$("#MsgSucessoIndique").fadeIn();
							$("#MsgSucessoIndique").html(msg);
							$("#EnviarIndique").removeAttr("disabled");
							limpaCampos("FormIndique");
							}
				});
				return false;
			}
		}
		
		function limpaCampos (form)
		{
		    $("#"+form).find(':input').each(function() {
		        switch(this.type) {
		            case 'password':
		            case 'select-multiple':
		            case 'select-one':
		            case 'select':
		            case 'text':
		            case 'textarea':
		                $(this).val('');
		                break;
		            case 'checkbox':
		            case 'radio':
		                this.checked = false;
		        }
		    });
		}
		function checkMail(mail){
	        var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	        if(typeof(mail) == "string"){
	        	if(er.test(mail)){ return true; }
	        }else if(typeof(mail) == "object"){
	        	if(er.test(mail.value)){ 
	        		return true; 
	       		}
	        }else{
	      		return false;
	        }
		}
	</script>
	
	<?php if(@$page == "contato"): ?>
	
	<!--[if IE 6]>
	<script type="text/javascript" src="../../../../assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			DD_belatedPNG.fix('a, span, p, h1, h2, h3, ul, div, .BotaoEnviar');
		});
	</script>
	<![endif]-->
	
	<script>
		function validaContato ()
		{
			
		
			var nome		= $("#NomeContato").val();
			var email		= $("#EmailContato").val();
			var endereco	= $("#Endereco").val();
			var cidade		= $("#CidadeContato").val();
			var uf			= $("#Uf").val();
			var cep			= $("#Cep").val();
			var telefone	= $("#TelefoneContato").val();
			var comentario	= $("#Comentario3").val();
			
			if(nome == "" || email == "" || telefone == "")
			{
				$("#MensagemSucessoContato").fadeOut(function () {
					$("#MensagemErroContato").fadeIn();
					
				});
				return false;
			}else if(!checkMail(email)){
				$("#MensagemSucessoContato").fadeOut(function () {
					$("#MensagemErroContato").fadeIn().html("Email Inválido");
					
				});
				return false;
			}
			else
			{
				
				$("#Enviar").attr("disabled","disabled");
				$("#MensagemErroContato").fadeOut(function(){
					$("#MensagemSucessoContato").fadeIn();
					});
				
				var ajaxview = "contato";

				var msg 	= '';
				vet_dados 	= 'nome='+ nome
							  +'&email='+ email
							  +'&endereco='+ endereco
							  +'&cidade='+ cidade
							  +'&uf='+ uf
							  +'&cep='+ cep
							  +'&telefone='+ telefone
							  +'&comentarios='+ comentario
							  +'&ajaxview='+ ajaxview;
							  
				base_url  	= "http://www.moradasclubcaminhodomeio.com.br/home/enviarInteresse";
				
				$.ajax({
					type: "POST",
					url: base_url,
					data: vet_dados,
					success: function(msg) {
							//$("#MensagemSucessoContato").fadeIn();
							$("#MensagemSucessoContato").html(msg);
							$("#Enviar").removeAttr("disabled");
							limpaCampos("FormContato");
							}
				});
				return false;
			}
		}


		
	</script>
	
	<?php endif; ?>