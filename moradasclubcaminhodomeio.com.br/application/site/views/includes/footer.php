		<div class="Footer">
			<div class="Container">
				<div class="Sponsors">
					<p class="Financiamento Imgr">Financiamento: Caixa Minha Casa, Minha Vida</p>
					<p class="Comercializacao Imgr">Comercialização: Jair Behr Assessoria Imobiliária</p>
					<p class="Realizacao Imgr">Realização: Moradas Rodobens, Rodobens Negócios Imobiliários e Capa Max</p>
				</div> <!-- .Sponsors -->
				<h4 class="LogoFooter Imgr">Moradas Clube</h4>
				<div class="Bottombar">
					<div class="SocialMedia">
						<p>Compartilhe também no Facebook e Orkut.</p>
						<ul>
							<li class="First"><a href="http://www.facebook.com/NexGroup" class="BotaoFacebook Imgr" rel="external" title="Facebook">Facebook</a></li>
							<li><a href="http://www.orkut.com.br/Main#Community?cmm=109453320" class="BotaoOrkut Imgr" rel="external" title="Orkut">Orkut</a></li>
						</ul>
					</div> <!-- .SocialMedia -->
					<div class="Signature">
						<a href="http://www.imobi.divex.com.br/" class="Credits Imgr" rel="external" title="Produzido por: Divex Imobi">Produzido por: Divex Imobi</a>
					</div> <!-- .Signature -->
				</div> <!-- .Bottombar -->
			</div> <!-- .Container -->
		</div> <!-- .Footer -->
	</div> <!-- .Main -->

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
	<script type="text/javascript">!window.jQuery && document.write(unescape('%3Cscript src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"%3E%3C/script%3E'))</script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-ui-1.8.12.custom.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.orbit-1.2.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/common.js"></script>

	<!--[if IE 6]>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			//DD_belatedPNG.fix('a, span, p, h1, h2, h3, ul, div, .BotaoEnviar');
		});
	</script>
	<![endif]-->

	<script type="text/javascript">
		$(document).ready(function() {
			$(".Featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
		});
	</script>

</body>
</html>