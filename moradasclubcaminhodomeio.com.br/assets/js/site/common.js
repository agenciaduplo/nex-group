

	// Funções diversas que são utilizadas em todo o site
	$(window).load(function() {
			$('#featured').orbit({
				animation: 'fade',					// fade, horizontal-slide, vertical-slide, horizontal-push
				animationSpeed: 800,				// how fast animtions are
				timer: true, 						// true or false to have the timer
				advanceSpeed: 4000, 				// if timer is enabled, time between transitions 
				pauseOnHover: true, 				// if you hover pauses the slider
				startClockOnMouseOut: true, 		// if clock should start on MouseOut
				startClockOnMouseOutAfter: 1000, 	// how long after MouseOut should the timer start again
				directionalNav: true, 				// manual advancing directional navs
				captions: true, 					// do you want captions?
				captionAnimation: 'fade', 			// fade, slideOpen, none
				captionAnimationSpeed: 800, 		// if so how quickly should they animate in
				bullets: true,						// true or false to activate the bullet navigation
				bulletThumbs: true,				// thumbnails for the bullets
				bulletThumbLocation: ''			// location from this file where thumbs will be
			});
		});

	$(function() {
		// Função banner home 
		
		
		// Função que atribui o target="_blank" aos links que tenham o atributo rel="external"
		// O XHTML 1.0 Strict não suporta mais o atributo target, dessa forma forçamos o link a
		// abrir em uma nova janela/aba sem invalidar o código
		$("a[rel=external]").attr('target', '_blank');

		// Setar o primeiro campo de formulário com o foco
		//$("input[type=text]:first").focus();

		// Uma div vazia deve ser criada depois do .LogoFooter para evitar que o mesmo desapareça no IE6
		$('.LogoFooter').after('<div></div>');

		// Exibe e esconde os painéis do topo das funcionalidades
		// "Plantão de vendas", "Tenho interesse" e "Indique para um amigo"
		$('.SlidePanel').hide()
		$('.BotaoInteresse').click(function() {
			$('#PainelTenhoInteresse').slideToggle();
			return false;
		});
		$(document).mouseup(function(e) { $("#PainelTenhoInteresse").slideUp("slow"); });

		$('.BotaoPlantao').click(function() {
			$('#PainelPlantaoVendas').slideToggle();
			return false;
		});
		$(document).mouseup(function(e) { $("#PainelPlantaoVendas").slideUp("slow"); });

		$('.BotaoIndique').click(function() {
			$('#PainelIndique').slideToggle();
			return false;
		});
		$(document).mouseup(function(e) { $("#PainelIndique").slideUp("slow"); });

		$(".SlidePanel").mouseup(function() {return false});
		$('.BotaoFechar').click(function() {
			$(".SlidePanel").slideUp("slow");
			return false;
		});
		$("#Cep").mask("99999-999");
		$("#TelefoneContato").mask("(99) 9999-9999");
		$("#Telefone").mask("(99) 9999-9999");
		

	});