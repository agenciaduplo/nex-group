<?=$this->load->view('includes/header');?>

		<div class="Wrapper">
        	<div class="Container">

            	<div class="Main">

					<img class="ImgFamilia PNG" src="<?=base_url()?>assets/img/site/familia.png" alt="" height="237" width="405" />
					<img class="ImgVista PNG" src="<?=base_url()?>assets/img/site/vista.png" alt="Simula&ccedil;&atilde;o a partir da perspectiva ilustrada das piscinas do Moradas Clube Santa Maria" height="317" width="453" />
                    <p class="Legenda">Simula&ccedil;&atilde;o a partir da perspectiva ilustrada das piscinas do Moradas Clube Santa Maria</p>

                	<div class="Footer">

        				<div class="Plantao">
                        	<h2>Plant&atilde;o de vendas:</h2>
                            <span class="EnderecoDestaque">Continuação da Av. Protásio Alves</span>
                            <span class="Endereco">Estrada Caminho do Meio, 5425</span>
                            <a href="<?=base_url()?>assets/img/site/mapa-localizacao.jpg" rel="shadowbox" title="Estrada Caminho do Meio, 5425">Clique e veja o mapa de localiza&ccedil;&atilde;o</a>
                        </div><!--fecha Plantao-->

        				<div class="LigueAgora">
                        	<h2>Ligue agora:</h2>
                            <span class="Fone">51 3435.4673</span>
                        </div><!--fecha Fone-->

						<div class="Logotipos">
                        	<img class="PNG" src="<?=base_url()?>assets/img/site/logo-moradas-rodobens.png" alt="Moradas Rodobens" height="17" width="77" />
                        	<img class="LogotipoRodobens PNG" src="<?=base_url()?>assets/img/site/logo-rodobens.png" alt="Rodobens Neg&oacute;cios Imobili&aacute;rios" height="26" width="74" />
                        	<img class="PNG" src="<?=base_url()?>assets/img/site/logo-capamax.png" alt="Capa Max - Mais F&aacute;cil ser feliz" />
                        </div>

        			</div><!--fecha Footer-->

                </div><!--fecha Main-->

                <div class="Sidebar PNG">

                	<form id="FormInteresse" name="FormInteresse" method="POST" action="" onsubmit="" >
                		<?php 
									
							@$utm_source 	= $_GET['utm_source'];
							@$utm_medium 	= $_GET['utm_medium'];
							@$utm_content 	= $_GET['utm_content'];
							@$utm_campaign 	= $_GET['utm_campaign'];
							
							@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
							
						?>
						<input type="hidden" name="origem" id="txtOrigem" value="<?=@$_SERVER['HTTP_REFERER']?>" />
						<input type="hidden" name="url"	id="txtUrl" value="<?=@$url?>" />
                	
                        <ul class="Formulario">

                            <li class="titlegroup">
                            	<h3>Tenho interesse</h3>
                            	<p>Campos marcados com asterisco (*) s&atilde;o obrigat&oacute;rios.</p>
                            </li>

                            <li>
                                <label for="txtInteresseNome">Nome*</label>
                                <input type="text" id="txtInteresseNome" class="Campo validate[required]" name="txtInteresseNome" />
                            </li>

							<li>
                                <label for="selectInteresseFaixa">Faixa et&aacute;ria*</label>
                                <select id="selectInteresseFaixa" class="Select validate[required]" name="selectInteresseFaixa">
                                	<option value="Até 30 anos">At&eacute; 30 anos</option>
                                    <option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                    <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                    <option value="Mais de 50 anos">Mais de 50 anos</option>
                                </select>
                            </li>

							<li>
                                <label for="selectInteresseCivil">Estado civil</label>
                                <select id="selectInteresseCivil" class="Select validate[required]" name="selectInteresseCivil">
                                	<option value="Casado">Casado</option>
                                    <option value="Solteiro">Solteiro</option>
                                </select>
                            </li>

                            <li>
                                <label for="txtInteresseBairroCidade">Bairro e cidade</label>
                                <input type="text" id="txtInteresseBairroCidade" class="Campo validate[required]" name="txtInteresseBairroCidade" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseProfissao">Profiss&atilde;o*</label>
                                <input type="text" id="txtInteresseProfissao" class="Campo validate[required]" name="txtInteresseProfissao" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseTelefone">Telefone*</label>
                                <input type="text" id="txtInteresseTelefone" class="Campo validate[required]" name="txtInteresseTelefone" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label for="txtInteresseEmail">E-mail*</label>
                                <input type="text" id="txtInteresseEmail" class="Campo validate[required,custom[email]]" name="txtInteresseEmail" onblur="" OnFocus="" />
                            </li>

							<li>
                            	<label>Forma de contato</label>
	                            <input type="radio" class="RadioButton" checked="checked" value="E-mail" name="radioInteresseContato" id="radioInteresseEmail"><label class="LabelRadio" for="radioInteresseEmail">E-mail</label>
    	                        <input type="radio" class="RadioButton" value="Telefone" name="radioInteresseContato" id="radioInteresseTelefone"><label class="LabelRadio" for="radioInteresseTelefone">Telefone</label>
                            </li>


							<li>
                                <label for="areaInteresseComentarios">Coment&aacute;rios*</label>
                                <textarea class="TextArea validate[required]" id="areaInteresseComentarios" name="areaInteresseComentarios"></textarea>
                            </li>

                            <li><input type="image" src="<?=base_url()?>assets/img/site/btn-enviar.png" class="btnEnviar" id="btrnInteresseEnviar" name="btrnInteresseEnviar" /></li>

                        </ul>
                        <div id="enviarInteresse"></div>
                    </form>

                </div><!--fecha Sidebar-->
                
                <a href="http://www.imobi.divex.com.br" class="Assinatura" rel="nofollow" target="_blank" title="Produzido por Divex Imobi">Produzido por Divex Imobi</a>

            </div><!--fecha Container-->
        </div><!--fecha Wrapper-->
	
<?=$this->load->view('includes/footer');?>