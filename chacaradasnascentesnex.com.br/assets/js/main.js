var modal = {
	open: function(m){
		$('#' + m).show();
		$("#form_main").validationEngine('hide');
		$('body').css('overflow', 'hidden');
	},
	close: function(m){
		$('#' + m).hide();
		$('#cupcakemodal').hide();
		$('#' + m + '_main').validationEngine('hide');
		$('body').css('overflow', 'visible');
	}
}