<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'page'					=> "home"
		);
		
		$this->load->view('home/home', $data);
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$id_empreendimento		= 86;
		$id_estado				= 21;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$telefone 				= $this->input->post('telefone');
		$comentarios 			= $this->input->post('comentarios');
		$hotsite				= "S";
		
		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'				=> $id_estado,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome'					=> $nome,
			'email'					=> $email,
			'telefone'				=> $telefone,
			'comentarios'			=> $comentarios,
			'hotsite'				=> $hotsite
		);
		
		$interesse 			= $this->model->setInteresse($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//inicia o envio do e-mail
		//====================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		ob_start();
		
		?>
			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
				 			<td align="center" colspan="2">
				 				<a target="_blank" href="http://www.nexgroup.com.br">
				 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
				 				</a>
				 			</td>
						 </tr>
						<tr>
							<td colspan='2'>Interesse enviado em <?php echo $data_envio; ?> via HotSite</td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
		    				<td colspan="2">Interesse enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  			</tr>
						  <tr>
						    <td width="30%">&nbsp;</td>
						    <td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
						    <td width="30%">Empreendimento:</td>
						    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
						    <td width="30%">IP:</td>
						    <td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
						    <td width="30%">E-mail:</td>
						    <td><strong><?=@$email?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Telefone:</td>
						    <td><strong><?=@$telefone?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Comentários:</td>
						    <td valign="top"><strong><?=@$comentarios?></strong></td>
						  </tr>						  					  					  
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>
						  <tr>
					 	  	<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						  </tr>
					</table>
				</body>
			</html>
		<?php
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		
		$next = false; 
		foreach($this->model->getEmailGrupos() as $grupo){
			if($grupo->rand == '1'){
				$emails = array();
				foreach($this->model->getEmailGruposEmails($grupo->id) as $email){
					array_push($emails,$email->email);
				}
				$this->model->grupo_interesse($interesse,$grupo->id);
				$next = true;
			} elseif($next){
				if($grupo->id == 14){
					$id = 10;
				} else {
					$id = $grupo->id;
				}
				$this->model->setNextEmailGroup($id);
				$next = false;
			}
		}

		/* Random Mail */
		$this->email->to($emails);
		$this->email->bcc('atendimento@divex.com.br');
		$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
		$this->email->subject('Interesse enviado via HotSite');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		echo "Interesse enviado com sucesso!";
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */