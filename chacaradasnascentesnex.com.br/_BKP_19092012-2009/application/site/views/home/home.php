<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Chácara das Nascentes</title>
	
	<meta name="title" content="Chácara das Nascentes" />
    <meta itemprop="name" content="Chácara das Nascentes" />
    <meta name="description" content="Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer" />
    <meta name="keywords" content="chácara das nascentes, porto alegre, poa, imóvel, bairro planejado, clube de lazer, monitoramento" />
    <meta name="robots" content="all" />
	
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='Chácara das Nascentes' />
	<meta property='og:image' content='http://www.chacaradasnascentesnex.com.br/assets/img/logo.png'/>
	<meta property='og:description' content='Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer'/>
	<meta property='og:url' content='http://www.chacaradasnascentesnex.com.br'/>

	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.fancybox-1.3.4.css" media="screen" />
	<link href="<?=base_url()?>assets/css/validation/template.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/css/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
    
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>	
	<script type="text/javascript" src="<?=base_url()?>assets/js/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/functions.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#modal").fancybox({
				'transitionIn'		: 'elastic',
				'transitionOut'		: 'elastic',
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.6
			});
		});

		$(document).ready(function() {
					$("#modal2").click(function() {

						$.fancybox({

								'padding'		: 0,

								'autoScale'		: false,

								'title'			: this.title,

								'width'			: 700,

								'height'		: 394,

								'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),

								'type'			: 'swf',

								'swf'			: {

								'wmode'			: 'transparent',

								'allowfullscreen'	: 'true'

								}

							});

					

						return false;

					});
		});


	</script> 
    
    
    <script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-61']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
    
</head>    
<body> 
<div id="mensagem" style="display:none">
	<div class="sucesso"></div>
    <div class="sucessoin"></div>
    <a href="javascript: fechaModal();" id="btFechar" class="fechar"></a>
</div>

<div id="container">
    <div id="box">
        <div id="logo"></div>
        <div id="header-text"></div>
        <a href="<?=base_url()?>assets/img/mapa.png" id="modal" class="btn-mapa"></a>
        <div class="sinta-casa"></div>
        <div class="formulario">
            <form name="EnviarInteresse" id="FormInteresse" action="" method="post">
            	<h2>Cadastre-se aqui para maiores informações.</h2>
                <ul class="left">
                    <li><label>*Nome:</label><input class="validate[required] formus" name="nome" id="nome" type="text" value="" /></li>
                    <li><label>*E-mail:</label><input class="validate[required,custom[email]] formus" name="email" id="email" type="text" value="" /></li>
                    <li><label>Telefone:</label><input class="formus" name="telefone" id="telefone" type="text" value="" /></li>
					<p>*Campos obrigatórios.</p>
                </ul>
                <ul class="right">
                    <li><label>Comentário:</label><textarea name="comentario" id="comentarios" cols="" rows=""></textarea></li>
                    <li><input class="btn" name="enviar" type="submit" value=""/></li>
                </ul>
            </form>
        </div>
    </div>
    <div id="footer">
        <a href="http://www.youtube.com/watch?v=LYJFgA8ftOU" id="modal2" title="Assista ao Vídeo"><div class="video"></div></a>
        <div class="dorms"></div>
        <div class="social">
            <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            </div>
            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f6a24a51a684879"></script>
        </div>
        <div class="logos-nex">
        	<a href="http://www.capa.com.br/" class="capa" title="Acesse o site da Capa Engenharia"></a>
        	<a href="http://www.nexgroup.com.br" class="nex" title="Acesse o site da Nex Group"></a>
        </div>    
    </div>

</div>
</body>
</html>