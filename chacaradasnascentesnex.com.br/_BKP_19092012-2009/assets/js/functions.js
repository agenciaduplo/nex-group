	
	function fechaModal () {
		$("#mensagem").fadeOut();
	}
	
	jQuery(document).ready(function(){
	
		jQuery("#FormInteresse").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status == true)
				{
					var nome			= $("#nome").val();
					var telefone		= $("#telefone").val();
					var email			= $("#email").val();
					var comentarios		= $("#comentarios").val();
						
					var msg 	= '';
					vet_dados 	= 'nome='+ nome
								  +'&telefone='+ telefone
								  +'&email='+ email
								  +'&comentarios='+ comentarios;
								  
					base_url  	= "http://www.chacaradasnascentesnex.com.br/home/enviarInteresse";
					
					$.ajax({
						type: "POST",
						url: base_url,
						data: vet_dados,
						success: function(msg) {
								limpaCampos('#FormInteresse');
								$("#mensagem").fadeIn();
								}
					});
					return false;
				}
				else
				{
					return false;
				}
			},
			promptPosition : "topRight"
			  
		})
	});


	function limpaCampos (form)
	{
	    $(form).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'select':
	            case 'email':
	            	$(this).val('');
	            case 'tel':
	            	$(this).val('');
	            case 'text':
	            	$(this).val('');
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                $('#ipt-contact-phone').checked = true;
	                $('#ipt-cliente-nao').checked = true;
	        }
	    });
	}