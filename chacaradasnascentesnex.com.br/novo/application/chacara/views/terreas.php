<?php
$this->load->view('tpl/header',array(
		'description' => 'Conheça o clube de lazer com infraestrutura completa. Casas de 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'clube de lazer, infraestrutura completa, casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro planejado, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Clube de Lazer - Casas 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre',
		'class' => ''
	));
?>

<!-- CLUBE -->
	
	<section id="clube" class="container_12">
		<h2>Casas Térreas</h2>
		<img src="<?=base_url(); ?>assets/img/_varanda-casa-terrea.png" data-thumb="<?=base_url(); ?>assets/img/_varanda-casa-terrea.png" alt="" />
	</section>
<!-- CLUBE END -->

<!-- FOOTER INTERNO -->
	<section id="footer_interno" class="container_12">
		<div class="left">
			<h3>Faça um Tour Virtual:</h3>
			<p>Conheça por dentro todas as qualidades <br>do empreendimento.</p>
			<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank">ver mais</a>
		</div>
		<div class="right">
			<h3>Visite o Plantão de Vendas e Casa Decorada:</h3>
			<a href="<?=site_url(); ?>localizacao">ver mais</a>
		</div>
		
	</section>
<!-- FOOTER INTERNO END -->
<?php
$this->load->view('tpl/footer');
?>