<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance();
$CI->load->view('tpl/header',array(
	'title' => 'Database Error',
	'description' => '',
	'keywords' => '',
	'class' => null
));
?>
<h1><?php echo $heading; ?></h1>
<?php echo $message; ?>
<?=$CI->load->view('tpl/footer'); ?>