$(function(){
	$(".modal").fancybox({
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6
	});
	$("#modal2").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'title'			: this.title,
			'width'			: 700,
			'height'		: 394,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
				'wmode'			: 'transparent',
				'allowfullscreen'	: 'true'
			}
		});
		return false;
	});
	$('#slider').nivoSlider({
		effect: 'fade',
		slices: 15,
		boxCols: 8,
		boxRows: 4,
		animSpeed: 500,
		pauseTime: 3000,
		startSlide: 0,
		directionNav: true,
		controlNav: false,
		controlNavThumbs: false,
		pauseOnHover: true,
		manualAdvance: false,
		prevText: 'Prev',
		nextText: 'Next',
		randomStart: false
	});
	$('#slider2').nivoSlider({
		effect: 'fade',
		slices: 15,
		boxCols: 8,
		boxRows: 4,
		animSpeed: 500,
		pauseTime: 3000,
		startSlide: 0,
		directionNav: true,
		controlNav: true,  
		controlNavThumbs: true,
		pauseOnHover: true,
		manualAdvance: false,
		prevText: 'Prev',
		nextText: 'Next',
		randomStart: false
	});
});