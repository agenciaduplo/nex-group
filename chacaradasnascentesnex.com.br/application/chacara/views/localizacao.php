<?php
$this->load->view('tpl/header',array(
		'description' => 'Localização. Casas de 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'localização, casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Localização - Casas 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre',
		'class' => ''
	));
?>

<!-- LOCALIZAÇAO -->
	<section id="localizacao" class="container_12">
		<h2>Mapa de Localizaçao</h2>
		<div class="mapa"></div>
	</section>
<!-- LOCALIZAÇAO END -->

<!-- FOOTER INTERNO -->
	<section id="footer_interno" class="container_12">
		<div class="left">
			<h3>Faça um Tour Virtual:</h3>
			<p>Conheça por dentro todas as qualidades <br>do empreendimento.</p>
			<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank">ver mais</a>
		</div>
		<div class="right">
			<h3>Plantão de Vendas:</h3>
		</div>
		
	</section>
<!-- FOOTER INTERNO END -->
<?php
$this->load->view('tpl/footer');
?>