<section id="amigo">
	<div class="black"></div>   
	<a href="javascript:void(0);" onclick="javascript:modal.close('amigo');" class="fechar"></a>
	<div class="box">
		<div class="box_in">
			<h1>INDIQUE PARA UM AMIGO</h1>
			<div class="formulario">
				<form action="<?=site_url(); ?>contato/setAmigo?return=<?=rawurlencode(current_url()); ?>" method="post" id="amigo_main">
					<ul class="left">
						<li><label class="a">Nome:</label><input class="campo nome" name="form_nome" id="form_nome" type="text" value="" /></li>
						<li><label class="b">*E-mail:</label><input class="campo email validate[required,custom[email]]" name="form_email" id="form_email" type="text" value="" /></li>
						<li><label class="c">Nome do Amigo:</label><input class="campo telefone" name="form_nome_amigo" id="form_nome_amigo" type="text" value="" /></li>
						<li><label class="e">*E-mail do amigo:</label><input class="campo amigo validate[required,custom[email]]" name="form_email_amigo" id="form_email_amigo" type="text" value="" /></li>
						<p>* Campos Obrigatórios</p>
					</ul>
					<ul class="right">
						<li><label class="d">Mensagem:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
						<input class="btn-enviar" name="enviar" type="submit" value="" id="btn-submit" />
					</ul>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){
		$("#amigo_main").validationEngine('attach', {promptPosition : "topRight",scroll: false});
	});
	</script>
</section>