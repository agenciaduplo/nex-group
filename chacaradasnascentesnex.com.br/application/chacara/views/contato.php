<?php
$this->load->view('tpl/header',array(
		'description' => 'Contato. Casas de 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'Contato, casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Contato - Casas 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre',
		'class' => ''
	));
?>

<!-- CONTATO -->
	<div id="bg_familia"></div>

	<section id="contato" class="container_12">
		<h2>Contato</h2>
		<div class="formulario">
			<h3>Solicite um contato de nossa equipe:</h3>
			<form action="<?=site_url(); ?>contato/set?return=<?=rawurlencode(current_url()); ?>" method="post" id="form_main">
				<ul>
					<li><label class="a">*Nome:</label><input class="campo nome validate[required]" name="form_nome" id="form_nome" type="text" value="" /></li>
					<li><label class="b">*E-mail:</label><input class="campo email validate[required,custom[email]]" name="form_email" id="form_email" type="text" value="" /></li>
					<li><label class="c">Telefone:</label><input class="campo telefone" name="form_telefone" id="form_telefone" type="text" value="" /></li>
					<li><label class="d">Mensagem:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
					<p>* Campos Obrigatórios</p>
					<input class="btn-enviar" name="enviar" type="submit" value="" id="btn-submit" />
				</ul>
			</form>
		</div>
	</section>
<!-- CONTATO END -->

<!-- FOOTER INTERNO -->
	<section id="footer_interno" class="container_12">
		<div class="left">
			<h3>Faça um Tour Virtual:</h3>
			<p>Conheça por dentro todas as qualidades <br>do empreendimento.</p>
			<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank">ver mais</a>
		</div>
		<div class="right">
			<h3>Plantão de Vendas:</h3>
		</div>
		
	</section>
<!-- FOOTER INTERNO END -->

<script type="text/javascript">
$(function(){
	$('#form_telefone').mask('(99) 9999-9999');
	$("#form_main").validationEngine('attach', {promptPosition : "topRight"});
});
</script>

<?php
$this->load->view('tpl/footer');
?>