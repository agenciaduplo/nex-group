<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "home";
$route['404_override'] = 'home/notfound';
$route['clube-de-lazer'] = 'clube/index';
$route['casas-terreas'] = 'terreas/index';

