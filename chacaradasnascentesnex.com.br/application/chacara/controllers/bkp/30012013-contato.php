<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
	
	public function index(){
		$this->load->view('contato');
	}
	public function set(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 86,
			'id_estado' => 21,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome' => $this->input->post('form_nome'),
			'email' => strtolower($this->input->post('form_email')),
			'telefone' => $this->input->post('form_telefone'),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario'))),
			'hotsite' => 'S'
		);

		if(!empty($data['email']) && !empty($data['nome'])){
			$this->load->model('contato_model','model');
			$interesse = $this->model->setInteresse($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$data['empreendimento'] = $empreendimento;
			$html = $this->load->view('tpl/email-interesse',$data,true);

			$this->load->library('Nexemail');

			/* Random Mail */
			if(strtolower($data['nome']) === 'teste123'){
				$this->nexemail->to('atendimento@divex.com.br')->send("Contato enviado via HotSite",$html)->clean();
			} else {
				$this->nexemail->bcc('atendimento@divex.com.br')->random($data['id_empreendimento'])->save($interesse)->next()->send("Interesse enviado via HotSite",$html)->clean();
			}
			
			$_SESSION['sucesso'] = true;
			$_SESSION['tracker'] = array(
				'email' => $data['email'],
				'nome' => $data['nome']
			);
		}
		redirect($return);
	}

	public function setAmigo(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 86,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome_remetente' => $this->input->post('form_nome'),
			'email_remetente' => strtolower($this->input->post('form_email')),
			'nome_destinatario' => $this->input->post('form_nome_amigo'),
			'email_destinatario' => strtolower($this->input->post('form_email_amigo')),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario')))
		);

		if(!empty($data['email_remetente']) && !empty($data['email_destinatario'])){
			$this->load->model('contato_model','model');
			$interesse = $this->model->setIndique($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));


			
			if(strtolower($data['nome']) === 'teste123'){
				$this->email->to('atendimento@divex.com.br');
			} else {
				$this->email->to($data['email_destinatario']);
			}
			$this->email->bcc('atendimento@divex.com.br');
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->subject('Indique :: NEX GROUP');

			$data['empreendimento'] = $empreendimento;

			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			
			$this->email->send();

			$_SESSION['sucesso'] = true;
		}
		redirect($return);
	}
}