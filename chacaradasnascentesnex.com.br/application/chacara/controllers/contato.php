<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller{

	public function index(){
		$this->load->view('contato');
	}
	public function set(){
		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url();

		$id_empreendimento = 86;

		$this->load->model('contato_model','model');

		$url = $_SESSION['url'];
		$url = explode("__",$url);

		$utm_source = $url[0];
		$utm_medium   = $url[1];
		$utm_campaign = $url[3];

		$email_nex 		= "vendas@nexvendas.com.br";
		$email_out 		= "vendas@nexvendas.com.br";

		$email_go		= $email_nex;
		$id_empresa 	= $this->model->getIdEmpresa($id_empreendimento);
		$id_empresa 	= $id_empresa[0]->id_empresa;
		$id_cidade 		= $this->model->getIdCidade($id_empreendimento);
		$id_cidade 		= $id_cidade[0]->id_cidade;

		if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
			$email_for =  $this->model->verificaLastEmailSendFor(1);
			foreach($email_for as $emf){
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$email_go = $email_out;
					break;
				}else{
					$email_go = $email_nex;
					break;
				}
			}
		}else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)){ // Porto Alegre | 1 quarto dos interesses para fora.
			$email_for =  $this->model->verificaLastEmailSendFor(3);
			$i 				 = 0;
			$flag 		 = false;

			foreach($email_for as $emf){
				++$i;
				if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
					$flag = true;
				}else if($emf->email_enviado_for == $email_out && $i = 1){
					$email_go = $email_nex;
					break;
				}else{
					$flag = false;
				}

				if($flag){
					$email_go = $email_out;
				}else{
					$email_go = $email_nex;
				}
			}
		}else{
			$email_go = $email_nex;
		}

		$data = array(
			'id_empreendimento' => $id_empreendimento,
			'id_estado'				 	=> 21,
			'ip' 								=> $this->input->ip_address(),
			'user_agent' 				=> $this->input->user_agent(),
			'url' 							=> (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' 						=> (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' 				=> date("Y-m-d H:i:s"),
			'nome' 							=> $this->input->post('form_nome'),
			'email' 						=> strtolower($this->input->post('form_email')),
			'telefone' 					=> $this->input->post('form_telefone'),
			'comentarios' 			=> nl2br(strip_tags($this->input->post('form_comentario'))),
			'hotsite' => 			 'S',
			'email_enviado_for' => $email_go
		);

		if(!empty($data['email']) && !empty($data['nome'])){
			$interesse = $this->model->setInteresse($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$data['empreendimento'] = $empreendimento;
			$html = $this->load->view('tpl/email-interesse',$data,true);


			$this->load->library('email',array(
				'mailtype' 	=> 'html',
				'wordwrap' 	=> false,
				'protocol'	=> 'sendmail'
			));

			/* envia email via roleta */
			$id_empreendimento = 86;
			$grupos = $this->model->getGrupos($id_empreendimento);

			$total = 0;
			$total = count($grupos);
			$total = $total;

			//echo "total: $total <br>";

			foreach ($grupos as $row):

				if($row->rand == 1):
					$atual = $row->ordem;

					if($atual == $total):

						$this->model->updateRand($id_empreendimento, '001');

						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						//echo "<pre>";print_r($emails);echo "</pre>";

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

					else:

						$atualizar = "00".$atual+1;
						$this->model->updateRand($id_empreendimento, $atualizar);

						$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
						//echo "<pre>";print_r($emails);echo "</pre>";

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

					endif;
				endif;
			endforeach;

      if($email_go == $email_out){
			  $list = $email_go;
      }else{
        $list[] = $email_go;
      }
			/* envia email via roleta */

			
			$this->email->to($list);
			
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "CHÁCARA DAS NASCENTES - Nex Group");
			$this->email->subject('Interesse enviado via HotSite');
			$this->email->message($this->load->view('tpl/email-interesse',array('dados' => $data),true));

			//$html = $this->load->view('tpl/email-interesse',$data,true);

			$this->email->send();

			//$this->email->print_debugger(); exit;

			$_SESSION['sucesso'] = true;
			$_SESSION['tracker'] = array(
				'email' => $data['email'],
				'nome' => $data['nome']
			);
		}
		redirect($return);
	}

	public function setAmigo()
	{
		$this->load->library('user_agent');
		
		if($this->agent->is_robot())
		{
			die();
		}

		$list_ip_block = array();
		$list_ip_block[] = "200.98.246.163";

		$list_ua_block = array();
		$list_ua_block[] = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)";

		$acesso_code = TRUE;

		if(in_array($this->input->ip_address(), $list_ip_block) && in_array($this->input->user_agent(), $list_ua_block))
		{
			$acesso_code = FALSE;
		}



		$return = $this->input->get('return');
		$return = !empty($return) ? $return : site_url() ;

		$data = array(
			'id_empreendimento' => 86,
			'ip' => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent(),
			'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
			'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
			'data_envio' => date("Y-m-d H:i:s"),
			'nome_remetente' => $this->input->post('form_nome'),
			'email_remetente' => strtolower($this->input->post('form_email')),
			'nome_destinatario' => $this->input->post('form_nome_amigo'),
			'email_destinatario' => strtolower($this->input->post('form_email_amigo')),
			'comentarios' => nl2br(strip_tags($this->input->post('form_comentario')))
		);

		if(!empty($data['email_remetente']) && !empty($data['email_destinatario']) && $acesso_code === TRUE){
			$this->load->model('contato_model','model');
			$indique = $this->model->setIndique($data);
			$empreendimento = $this->model->getEmpreendimento($data['id_empreendimento']);

			$this->load->library('email',array(
				'protocol' => 'sendmail',
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE
			));

			$id_empreendimento = 86;

			//roleta
			$grupos = $this->model->getGrupos($id_empreendimento);
			$total = 0;
			$total = count($grupos);
			
			if($total == 1) // caso só tenho um grupo cadastrado
			{
				$emails = $this->model->getGruposEmails($id_empreendimento, '001');
				
				$emails_txt = "";
				foreach ($emails as $email)
				{
					$grupo = $email->grupo;
					$list[] = $email->email;

					$emails_txt = $emails_txt.$email->email.", ";
				}
				$this->model->grupo_indique($indique, $grupo, $emails_txt);

			}
			else if($total > 1) // caso tenha mais de um grupo cadastrado
			{
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;
						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');
							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							
							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_indique($indique, $grupo, $emails_txt);
						
						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);
							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_indique($indique, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
			}
			/* envia email via roleta */

			
			
			$this->email->to($data['email_destinatario']);
			
			$data['empreendimento'] = $empreendimento;

			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->subject("Chácara das Nascentes - Indicação enviada para você");		
			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			$this->email->send();

			//envia para corretores
			$this->email->from("noreply@chacaradasnascentesnex.com.br", "Nex Group");
			$this->email->to($list);
			$this->email->subject("Chácara das Nascentes - Indique enviado via site");
			$this->email->message($this->load->view('tpl/email-amigo',$data,true));
			$this->email->send();
			//envia para corretores

			$_SESSION['sucesso'] = true;
		}
		redirect($return);
	}
}