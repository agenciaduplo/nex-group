<?php

/*
 * Classe: Nexemail
 * Autor: Divex (divex.com.br)
 * Função: Enviar e-mails NexGroup
 * Exemplo 1: 
 *
 * 	$this->load->library('Nexemail');
 *  $this->nexemail->bcc('atendiment@divex.com.br')->random($id_empreednimento)->save($id_interesse)->next()->send($assunto,$html)->clean();
 *
 * Exemplo 2:
 *
 * 	$this->load->library('Nexemail');
 *  $this->nexemail->empreendimento($id_empreendimento)->send($assunto,$html)->clean();
 * 
 * Nessecita de conexão com banco de dados NexGroup
*/
class Nexemail {
	
	private $from = 'no-reply@'; 
	private $from_name = 'Não Responda - '; 
	private $to = array();
	private $cc = array();
	private $bcc = array();

	private $ci = null;
	private $config = array('protocol' => 'sendmail', 'mailtype' => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
	private $gid = 0;
	private $eid = 0;

	function __construct($config = array()){
		$this->ci =& get_instance();

		$domain = str_replace('www.','',$_SERVER['HTTP_HOST']);

		$this->from = $this->from . $domain;
		$this->from_name = $this->from_name . $domain;
		if(sizeof($config) > 0){
			$this->config = array_merge($this->config,$config);
		}
	}

	/* Destinatários */

	// Define remetente do e-mail

	public function from($email,$name){
		$this->from = $email;
		$this->from_name = $name;
		return $this;
	}

	// Define para que vai o e-mail

	public function to($to = array()){
		if(is_array($to)){
			if(sizeof($to) > 0){
				$this->to = $to;
			}
		} else {
			$this->to = array($to);
		}
		return $this;
	}

	// Define quem ira receber cópia do e-mail

	public function cc($cc = array()){
		if(is_array($cc)){
			if(sizeof($cc) > 0){
				$this->cc = $cc;
			}
		} else {
			$this->cc = array($cc);
		}
		return $this;
	}

	// Define quem ira receber cópia oculta do e-mail

	public function bcc($bcc = array()){
		if(is_array($bcc)){
			if(sizeof($bcc) > 0){
				$this->bcc = $bcc;
			}
		} else {
			$this->bcc = array($bcc);
		}
		return $this;
	}

	// Reseta destinatários

	public function clean(){
		$this->to = array();
		$this->cc = array();
		$this->bcc = array();
		return true;
	}

	/* Grupos Functions */

	// Seta grupo de e-mails de um empreendimentos

	public function random($emp = 0){
		$this->eid = $emp;
		$this->gid = $this->_getGroup($this->eid)->id;
		$to = array();
		
		foreach($this->_getGroupEmails($this->gid) as $e){
			array_push($to,$e->email);
		}
		
		$this->to = $to;

		return $this;
	}

	// Salva relação entre interesse e grupo

	public function save($interesse){
		
		$this->_saveRelation($interesse,$this->eid);
		
		return $this;
	}
	// Define próximo grupo do empreendimento atual

	public function next(){
		$grupos = (array) $this->_getGroups($this->eid);
		for($i = 0; $i < sizeof($grupos); $i++){
			if($grupos[$i]->rand == '1'){
				if(isset($grupos[$i + 1])){
					$newRand = $grupos[$i + 1]->id; break;
				} else {
					$newRand = $grupos[0]->id; break;
				}
			}
		}

		$this->_setGroupRand($newRand);
		
		return $this;
	}

	/* Empreendimentos Functions */

	public function empreendimento($emp = 0){
		$this->eid = $emp;

		$lista = $this->_getEmpreendimentoEmails($this->eid);

		if(sizeof($lista) > 0){

			$this->clean();

			foreach($lista as $e){
				switch($e->tipo){
					case 'para': $this->to = array_merge($this->to,array($e->email)); break;
					case 'cc': $this->cc = array_merge($this->cc,array($e->email)); break;
					case 'bcc': $this->bcc = array_merge($this->bcc,array($e->email)); break;
					default : $this->to = array_merge($this->to,array($e->email)); break;
				}
			}
		}
		return $this;
	}

	/* Mails Functions */

	// Envia e-mail

	public function send($assunto,$html,$anexo = array()){

		$this->ci->load->library('email',$this->config);

		$this->ci->email->to($this->to);
		if(sizeof($this->cc) > 0){
			$this->ci->email->cc($this->cc);
		} if(sizeof($this->bcc) > 0){
			$this->ci->email->bcc($this->bcc);
		}
		$this->ci->email->from($this->from,$this->from_name);
		$this->ci->email->subject($assunto);
		$this->ci->email->message($html);

		if(sizeof($anexo) > 0 and $anexo != null){
			$this->ci->email->attach($anexo);
		}

		$this->ci->email->send();	

		return $this;
	}

	/* Database Functions */

	// Pega grupo atual de um empreendimento

	private function _getGroup($id = 0){
		return $this->ci->db->select('grupos.id')->from('grupos')->where(array(
			'grupos.id_empreendimento' => $id,
			'grupos.rand' => '1'
		))->get()->row();
	}
	
	// Pega lista de grupos de um empreendimento

	private function _getGroups($id = 0){
		return $this->ci->db->select('grupos.id, grupos.rand, grupos.ordem')->from('grupos')->where('grupos.id_empreendimento',$id)->order_by('ordem','asc')->get()->result();
	}
	
	// Pega e-mails do grupo

	private function _getGroupEmails($id = 0){
		return $this->ci->db->select('grupos_emails.email')->from('grupos_emails')->where('grupos_emails.grupo',$id)->get()->result();
	}
	
	// Grava o próximo grupo

	private function _setGroupRand($id = 0){
		$this->ci->db->update('grupos',array('rand' => '0'),"grupos.rand = '1' AND grupos.id_empreendimento = " . $this->eid);
		$this->ci->db->update('grupos',array('rand' => '1'),"grupos.id = '{$id}'");
	}

	// Lista de e-mails por empreendimento

	private function _getEmpreendimentoEmails($id = 0, $tipo = 1){
		return $this->ci->db->select('emails.email, emails.tipo')->from('emails')->where(array(
			'emails.id_empreendimento' => $id,
			'emails.id_tipo_form' => $tipo
		))->get()->result();
	}

	// Salva Relação

	private function _saveRelation($id, $eid){
		$this->ci->db->insert('grupos_interesses',array(
			'id_interesse' => $id,
			'id_grupo' => $eid
		));
	}
}