<section id="interesse">
	<div class="black"></div>
	<a href="javascript:void(0);" onclick="javascript:modal.close('interesse');" class="fechar"></a>
	<div class="box">
		<div class="box_in">
			<h1>Tenho interesse no empreendimento</h1>
			<div class="formulario">
				<form action="<?=site_url(); ?>contato/set?return=<?=rawurlencode(current_url()); ?>" method="post" id="interesse_main">
					<ul class="left">
						<li><label class="a">*Nome:</label><input class="campo nome validate[required]" name="form_nome" id="form_nome" type="text" value="" /></li>
						<li><label class="b">*E-mail:</label><input class="campo email validate[required,custom[email]]" name="form_email" id="form_email" type="text" value="" /></li>
						<li><label class="c">Telefone:</label><input class="campo telefone" name="form_telefone" id="interesse_telefone" type="text" value="" /></li>
						<p>* Campos Obrigatórios</p>
					</ul>
					<ul class="right">
						<li><label class="d">Mensagem:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
						<input class="btn-enviar" name="enviar" type="submit" value="" id="btn-submit" />
					</ul>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){
		$('#interesse_telefone').mask('(99) 9999-9999');
		$("#interesse_main").validationEngine('attach', {promptPosition : "topRight",scroll: false});
	});
	</script>
</section>