<?php
$this->load->view('tpl/header',array(
		'description' => 'Conheça as Casas, 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro planejado, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Casas 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre',
		'class' => ''
	));
?>

<!-- CASAS -->
	<div id="bg_verde_in"></div>
	
	<section id="casas" class="container_12">
		<h2>CASAS</h2>
		<div class="slider-wrapper theme-default">
			<div id="slider" class="nivoSlider">
				<img src="<?=base_url(); ?>assets/img/casas/01.png" data-thumb="<?=base_url(); ?>assets/img/clube/01p.png" alt="" />
				<img src="<?=base_url(); ?>assets/img/casas/02.png" data-thumb="<?=base_url(); ?>assets/img/clube/02p.png" alt="" />
				<img src="<?=base_url(); ?>assets/img/casas/03.png" data-thumb="<?=base_url(); ?>assets/img/clube/03p.png" alt="" />
			</div>
		</div>
		<div id="tabs">
			<ul id="casas_menu">
				<li class=""><a href="#tabs-1" class="a"><p>Casa Padrão</p></a></li>
				<li class=""><a href="#tabs-2" class="b"><p>Casa com varanda</p></a></li>
				<li class=""><a href="#tabs-3" class="c"><p>Casa unificada <br />sem varanda</p></a></li>
				<li class=""><a href="#tabs-4" class="d"><p>Casa unificada <br />com varanda</p></a></li>
			</ul>
			<div id="tabs-1" class="">
                <div class="planta">
                    <a class="modal" href="<?=base_url(); ?>assets/img/casas/planta-01.jpg" >
                        <img src="<?=base_url(); ?>assets/img/casas/planta-01p.png" >
                        <div class="lupa">Clique nas imagens para ampliá-las</div>
                    </a>
                </div>
			</div>
			<div id="tabs-2" class="">
                <div class="planta">
                    <a class="modal" href="<?=base_url(); ?>assets/img/casas/planta-02.jpg" >
                        <img src="<?=base_url(); ?>assets/img/casas/planta-02p.png" >
                        <div class="lupa">Clique nas imagens para ampliá-las</div>
                    </a>
                </div>
			</div>
			<div id="tabs-3" class="">
                <div class="planta">
                    <a class="modal" href="<?=base_url(); ?>assets/img/casas/planta-04.jpg" >
                        <img src="<?=base_url(); ?>assets/img/casas/planta-04p.png" >
                        <div class="lupa">Clique nas imagens para ampliá-las</div>
                    </a>
                </div>
			</div>
			<div id="tabs-4" class="">
                <div class="planta">
                    <a class="modal" href="<?=base_url(); ?>assets/img/casas/planta-03.jpg" >
                        <img src="<?=base_url(); ?>assets/img/casas/planta-03p.png" >
                        <div class="lupa">Clique nas imagens para ampliá-las</div>
                    </a>
                </div>
			</div>
		</div>
	</section>
<!-- CASAS END -->
<script type="text/javascript">
$(function(){
	$("#tabs").tabs({
	});
});
</script>
<!-- FOOTER INTERNO -->
	<section id="footer_interno" class="container_12">
		<div class="left">
			<h3>Faça um Tour Virtual:</h3>
			<p>Conheça por dentro todas as qualidades <br>do empreendimento.</p>
			<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank">ver mais</a>
		</div>
		<div class="right">
			<h3>Visite o Plantão de Vendas e Casa Decorada:</h3>
			<a href="<?=site_url(); ?>localizacao">ver mais</a>
		</div>
		
	</section>
<!-- FOOTER INTERNO END -->
<?php
$this->load->view('tpl/footer');
?>