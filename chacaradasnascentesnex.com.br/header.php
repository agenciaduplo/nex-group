<?php 
$url = $this->uri->segment(1); source();
?><!DOCTYPE html>
<html lang="pt-BR"> 

<!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
<!--[if IE 8]>	 <html class="ie8" lang="pt-BR"> <![endif]-->  
<!--[if IE 9]>	 <html class="ie9" lang="pt-BR"> <![endif]-->  

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="<?=isset($description) ? $description : ''; ?>">
<meta name="keywords" content="<?=isset($keywords) ? $keywords : ''; ?>">
<title><?=isset($title) ? $title . ' - Chacara' : 'Chacara'; ?></title>

<meta property='og:locale' content='pt_BR' />
<meta property='og:title' content='Chácara das Nascentes' />
<meta property='og:image' content='http://www.chacaradasnascentesnex.com.br/assets/img/logo.png'/>
<meta property='og:description' content='Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer'/>
<meta property='og:url' content='http://www.chacaradasnascentesnex.com.br'/>

<meta name="google-site-verification" content="6sd4yVMO36C9QvWU0dq9yMdI5pl8ST8yS5efDHvuR9M" />

<link rel="shortcut icon" type="image/x-ico" href="<?=base_url(); ?>favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/960_12_col.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/nivo-slider.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/validation.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/styles.css">

<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.ui.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.imageLens.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.validation.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/onload.js"></script>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-42831015-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="http://www.henrique.rs/perceptions/api/v1.js"></script>
<script type="text/javascript">perceptions.tracking(7);</script>
</head>
<body class="<?=isset($class) ? $class : 'home'; ?>">

<!-- MODALS -->
<?php
$this->load->view('modals/modal-cupcake');
$this->load->view('modals/modal-amigo');
$this->load->view('modals/modal-interesse');
$this->load->view('modals/modal-plantao');
$this->load->view('modals/modal-sucesso');

?>
<script type="text/javascript">

	<?php
	if(isset($_SESSION['sucesso'])){
		unset($_SESSION['sucesso']);
		echo "$(function(){ javascript:modal.open('sucesso'); });";
		?>
	</script>	
	<!-- Google Code for Contato Conversion Page -->
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/978900397/?value=0&label=5rocCOzXmAYQjLiK1QM&guid=ON&script=0"/>
	</div>
	</noscript>
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 978900397;
	var google_conversion_language = "pt";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "5rocCOzXmAYQjLiK1QM";
	var google_conversion_value = 0;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

		<?php
	}
	?>
</script>
<!-- MODALS END -->
<header class="container_12">
	<a href="<?=site_url(); ?>" onclick="" title="Chacarra das Nascente"><h1>Chacarra das Nascente</h1></a>
	<?php if(strlen($url)>3) { ?>
	<ul id="menu2">
		<a style="color: #006571" href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank"><li class="a">Tour<br>Virtual</li></a>
		<a style="color: #006571" href="http://www.youtube.com/watch?v=LYJFgA8ftOU" id="modal2" title="Assista ao Vídeo"><li class="b">Assista ao<br>vídeo</li></a>
		<a style="color: #006571" href="javascript:void(0);" onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=25','pop','width=450, height=450, top=100, left=100, scrollbars=no'); _gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);"><li class="c">corretor<br>online</li></a>
		<a style="color: #006571" href=""><li class="d"><img src="assets/img/logo_topo_verde.png" /></li></a>
		<a style="color: #006571" href="javascript:void(0);" onclick="javascript:modal.open('plantao');"><li class="e">plantão<br>de vendas</li></a>
		<a style="color: #006571" href="javascript:void(0);" onclick="javascript:modal.open('interesse');"><li class="f">tenho<br>interesse</li></a>
		<a style="color: #006571" href="javascript:void(0);" onclick="javascript:modal.open('amigo');"><li class="g">indique para<br>um amigo</li></a>
	</ul>	
	<? } else { ?>
	<ul id="menu2">
		<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank"><li class="a">Tour<br>Virtual</li></a>
		<a href="http://www.youtube.com/watch?v=LYJFgA8ftOU" id="modal2" title="Assista ao Vídeo"><li class="b">Assista ao<br>vídeo</li></a>
		<a href="javascript:void(0);" onclick="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=25','pop','width=450, height=450, top=100, left=100, scrollbars=no'); _gaq.push(['_trackEvent', 'Corretor Online', 'Abriu', 'Corretor online aberto.']);"><li class="c">corretor<br>online</li></a>
		<a href=""><li class="d"><img src="assets/img/logo_topo.png" /></li></a>
		<a href="javascript:void(0);" onclick="javascript:modal.open('plantao');"><li class="e">plantão<br>de vendas</li></a>
		<a href="javascript:void(0);" onclick="javascript:modal.open('interesse');"><li class="f">tenho<br>interesse</li></a>
		<a href="javascript:void(0);" onclick="javascript:modal.open('amigo');"><li class="g">indique para<br>um amigo</li></a>
	</ul>	
	<?php } ?>
 	<nav>
 		<ul>
 			<a href="<?=site_url(); ?>"><li class="a<?=empty($url) ? ' active' : ''; ?>"></li></a>
 			<!--<a href="<?=site_url(); ?>casas"><li class="b<?=$url == 'casas' ? ' active' : ''; ?>">CASAS</li></a>-->
 			<a href="<?=site_url(); ?>clube-de-lazer"><li class="b<?=$url == 'clube-de-lazer' ? ' active' : '';  if(strlen($url)>3) { echo ' home'; } ?>">CLUBE DE LAZER</li></a>
 			<a href="<?=site_url(); ?>casas-terreas"><li class="c<?=$url == 'casas-terreas' ? ' active' : ''; if(strlen($url)>3) { echo ' home'; } ?> ">CASAS TÉRREAS</li></a>
 			<a href="<?=site_url(); ?>sobrados"><li class="d<?=$url == 'sobrados' ? ' active' : '';  if(strlen($url)>3) { echo ' home'; } ?>">SOBRADOS</li></a>
 			<a href="<?=site_url(); ?>implantacao"><li class="e<?=$url == 'implantacao' ? ' active' : '';  if(strlen($url)>3) { echo ' home'; } ?>">IMPLANTAÇÃO</li></a>
 			<a href="<?=site_url(); ?>localizacao"><li class="f<?=$url == 'localizacao' ? ' active' : '';  if(strlen($url)>3) { echo ' home'; } ?>">LOCALIZAÇAO</li></a>
 			<a href="<?=site_url(); ?>contato"><li class="g<?=$url == 'contato' ? ' active' : '';  if(strlen($url)>3) { echo ' home'; } ?>">CONTATO</li></a>
 		</ul>				
 	</nav>
</header>