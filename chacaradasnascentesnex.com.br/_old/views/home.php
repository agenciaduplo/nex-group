<?php
$this->load->view('tpl/header',array(
		'description' => 'Casas de 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro planejado, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Bairro planejado com acesso monitorado, casas de 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre - Chácara das Nascentes'
	));

?>
<!-- HOME -->
	<div id="bg_home"></div>
	<div id="bg_verde"></div>
	
	<section id="home" class="container_12">
		<div class="casas"></div>
		<!--<a class="cupcake" href="javascript:modal.open('cupcakemodal');" style="display:block"></a>-->
		<div class="flor"><h2>Venha morar em um bairro planejado com sistema <br>de monitoramento e clube de lazer.</h2></div>
		<div class="imgs_home">
			<h3>Clube de Lazer</h3>
			<ul>
				<li>• Piscina adulto e infantil</li>
				<li>• SALÃO DE FESTAS</li>
				<li>• PLAYGROUND</li>
				<li>• cancha de bocha</li>
				<li>• quiosque com churrasqueira</li>
				<li>• quadra poliesportiva</li>
			</ul>
			<a href="<?=site_url(); ?>clube-de-lazer">Conheça a infraestrutura</a>
		</div>
		<div class="video">
			<h3>Assista o vídeo do empreendimento:</h3>
			<div class="video_box">
				<object width="522" height="294"><param name="movie" value="http://www.youtube.com/v/LYJFgA8ftOU?version=3&amp;hl=pt_BR"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/LYJFgA8ftOU?version=3&amp;hl=pt_BR" type="application/x-shockwave-flash" width="522" height="294" allowscriptaccess="always" allowfullscreen="true"></embed></object>
			</div>
		</div>
		<div class="formulario">
			<h3>Solicite um contato de nossa equipe:</h3>
			<form action="<?=site_url(); ?>contato/set?return=<?=rawurlencode(current_url()); ?>" method="post" id="form_main">
				<ul>
					<li><label class="a">*Nome:</label><input class="campo nome validate[required]" name="form_nome" id="form_nome" type="text" value="" /></li>
					<li><label class="b">*E-mail:</label><input class="campo email validate[required,custom[email]]" name="form_email" id="form_email" type="text" value="" /></li>
					<li><label class="c">Telefone:</label><input class="campo telefone" name="form_telefone" id="form_telefone" type="text" value="" /></li>
					<li><label class="d">Mensagem:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
					<p>* Campos Obrigatórios</p>
					<input class="btn-enviar" name="enviar" type="submit" value="" id="btn-submit" />
				</ul>
			</form>
		</div>
		
	</section>
<!-- HOME END -->

<script type="text/javascript">
$(function(){
	$('#form_telefone').mask('(99) 9999-9999');
	$("#form_main").validationEngine('attach', {promptPosition : "topRight"});
});
</script>

<?php
$this->load->view('tpl/footer');
?>