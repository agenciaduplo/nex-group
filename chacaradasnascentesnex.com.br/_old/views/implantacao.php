<?php
$this->load->view('tpl/header',array(
		'description' => 'Implantação. Casas de 3 dormitórios com suíte, pátio e churrasqueira. Venha morar em um bairro planejado com sistema de monitoramento e clube de lazer. Chácara das Nascentes. Estrada João de Oliveira Remião, 3105 - Porto Alegre / RS.',
		'keywords' => 'implantação, casas 3 dormitorios, casas, 3 dormitórios, suíte, churrasqueira, bairro planejado, acesso monitorado, clube de lazer, 3 dorms, casa, porto alegre, nex group',
		'title' => 'Implantação - Casas 3 dormitórios com suíte, pátio e churrasqueira em Porto Alegre',
		'class' => 'implaover'
	));
?>


<!-- IMPLANTACAO -->
	<section id="implantacao" class="container_12">
		<h2>Implantaçao</h2>
		<div id="img"><p><img id="img_03" src="<?=base_url(); ?>assets/img/implantacao/01.png" width="961" height="677" /></p></div> 
		<div class="flor"><h3>Venha morar em um bairro planejado <br>com sistema de monitoramento</h3></div>
		<a href="" class="lupa">Passe o mouse para ampliar</a>
	</section>
<!-- CLUBE END -->

<!-- FOOTER INTERNO -->
	<section id="footer_interno" class="container_12">
		<div class="left">
			<h3>Faça um Tour Virtual:</h3>
			<p>Conheça por dentro todas as qualidades <br>do empreendimento.</p>
			<a href="http://www.shots360.com.br/360/nexgroup/chacaradasnascentes.html" target="_blank">ver mais</a>
		</div>
		<div class="right">
			<h3>Visite o Plantão de Vendas e Casa Decorada:</h3>
			<a href="<?=site_url(); ?>localizacao">ver mais</a>
		</div>
		
	</section>
<!-- FOOTER INTERNO END -->
<script type="text/javascript" language="javascript">
$(function(){
	$("#img_03").imageLens({ imageSrc: "<?=base_url(); ?>assets/img/implantacao/02.png", lensSize: 350, borderSize: 5, borderColor: "#004b55" });
});	
</script>

<?php
$this->load->view('tpl/footer');
?>