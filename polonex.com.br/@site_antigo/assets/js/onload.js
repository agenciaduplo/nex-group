var sliderTimeout;

$(function(){
 	$('a.scroll').click(function(e){
 		events.scroll($(this).attr('href'));
 		e.preventDefault();
 	});
	$("#modal").click(function(){
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'title'			: this.title,
			'width'			: 700,
			'height'		: 525,
			'href'			: 'http://www.youtube.com/embed/UKe8YdbJMtQ?autoplay=1',
			'type'			: 'iframe'
		});
	});
	Height = $(window).height();
	$('body').scroll(function(){
		Top = $(this).scrollTop();
		scroll.updateURI(Top);

	});
	$('body').resize(function(){
		Height = $(window).height();
	});
	$('.bg').parallax("55%", -0.3);
	$('.people').parallax("50%", 0.8);
	$('.people2').parallax("0", 0.5);
	$('.people3').parallax("0", 0.5);
	$('.bg-balls-details').parallax("80%", 1.5);
	$('.bg2').parallax("50%", -0.8);
	$('.bgsss3').parallax("0%", -0.8);
	$('.mapa').parallax("50%", 1.0);
	$( '#slider1' ).lemmonSlider({
		infinite: true,
		slideToLast: true
	});
	$('.slider').nivoSlider({
		effect: 'fade',
		animSpeed: 0,
		manualAdvance: true,
		directionNav: true,
		pauseOnHover: true
	});
	sliderAutoplay();
	PagesHeight = {
		home: {
			top: $("#home").offset().top,
			bottom: $("#home").offset().top + $("#home").outerHeight(true)
		},
		empreendimento: {
			top: $("#empreendimentonav").offset().top,
			bottom: $("#empreendimentonav").offset().top + $("#empreendimentonav").outerHeight(true) 
		},
		infraestrutura: {
			top: $("#infraestruturanav").offset().top,
			bottom: $("#infraestruturanav").offset().top + $("#infraestruturanav").outerHeight(true) 
		},
		plantas: {
			top: $("#plantasnav").offset().top,
			bottom: $("#plantasnav").offset().top + $("#plantasnav").outerHeight(true) 
		},
		localizacao: {
			top: $("#localizacaonav").offset().top,
			bottom: $("#localizacaonav").offset().top + $("#localizacaonav").outerHeight(true) 
		},
		contato: {
			top: $("#contatonav").offset().top,
			bottom: $("#contatonav").offset().top + $("#contatonav").outerHeight(true) 
		}
	}
});

function sliderAutoplay(){
    $( '#slider1' ).trigger( 'nextSlide' );
}