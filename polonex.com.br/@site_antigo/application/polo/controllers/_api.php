<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function setContato()
	{
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email' => strtolower($this->input->post('form_email')),
				'comentarios' => ($this->input->post('form_mensagem')),
				'telefone' => $this->input->post('form_telefone'),
				'hotsite' => 'S'
			);
			if(preg_match(EMAIL_FORMAT,$p['email'])){
				if(!empty($p['nome'])){
					$interesse = $this->model->insert("interesses",$p);
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado em ' => $p['data_envio'] . ' via HotSite',
						'Enviado por ' => $p['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $p['ip'],
						'E-mail' => $p['email'],
						'Telefone' => $p['telefone'],
						('Comentários') => $p['comentarios']
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));

				
					/* envia email via roleta */
					$id_empreendimento = 83;
					$grupos = $this->model->getGrupos($id_empreendimento);

					$total = 0;
					$total = count($grupos);
					
					foreach ($grupos as $row):

						if($row->rand == 1):
							$atual = $row->ordem;

							if($atual == $total):

								$this->model->updateRand($id_empreendimento, '001');

								$emails = $this->model->getGruposEmails($id_empreendimento, '001');
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							else:

								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);

								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							endif;	
						endif;
					endforeach;
					/* envia email via roleta */

					if($p['nome'] == "Teste123"){
						$this->email->to('testes@divex.com.br');
					} else {
						$this->email->to($list);
					}
					$this->email->bcc('testes@divex.com.br');
					$this->email->from("noreply@polonex.com.br", "POLO - Nex Group");
					$this->email->subject('Contato enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/contato.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
	
	public function setInteresse()
	{
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email' => strtolower($this->input->post('form_email')),
				'comentarios' => ($this->input->post('form_mensagem')),
				'telefone' => $this->input->post('form_telefone'),
				'hotsite' => 'S'
			);
			if(preg_match(EMAIL_FORMAT,$p['email'])){
				if(!empty($p['nome'])){
					$interesse = $this->model->insert("interesses",$p);
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado em ' => $p['data_envio'] . ' via HotSite',
						'Enviado por ' => $p['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $p['ip'],
						'E-mail' => $p['email'],
						'Telefone' => $p['telefone'],
						('Comentários') => $p['comentarios']
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));
				
					/* envia email via roleta */
					$id_empreendimento = 83;
					$grupos = $this->model->getGrupos($id_empreendimento);

					$total = 0;
					$total = count($grupos);
					
					foreach ($grupos as $row):

						if($row->rand == 1):
							$atual = $row->ordem;

							if($atual == $total):

								$this->model->updateRand($id_empreendimento, '001');

								$emails = $this->model->getGruposEmails($id_empreendimento, '001');

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							else:

								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);

								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							endif;	
						endif;
					endforeach;
					/* envia email via roleta */

					if($p['nome'] == "Teste123"){
						$this->email->to('testes@divex.com.br');
					} else {
						$this->email->to($list);
					}

					$this->email->bcc('testes@divex.com.br');
					$this->email->from("noreply@polonex.com.br", "POLO - Nex Group");
					$this->email->subject('Interesse enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/interesse.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
	
	public function setSolicita()
	{
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email' => strtolower($this->input->post('form_email')),
				'comentarios' => ($this->input->post('form_mensagem')),
				'telefone' => $this->input->post('form_telefone'),
				'bairro_cidade' => $this->input->post('form_bairro_cidade'),
				'hotsite' => 'S'
			);
			if(preg_match(EMAIL_FORMAT,$p['email'])){
				if(!empty($p['nome'])){
					$interesse = $this->model->insert("interesses",$p);
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado em ' => $p['data_envio'] . ' via HotSite',
						'Enviado por ' => $p['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $p['ip'],
						'E-mail' => $p['email'],
						'Telefone' => $p['telefone'],
						'Cidade' => $p['bairro_cidade'],
						('Comentários') => $p['comentarios']
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));
				
					/* envia email via roleta */
					$id_empreendimento = 83;
					$grupos = $this->model->getGrupos($id_empreendimento);

					$total = 0;
					$total = count($grupos);
					
					foreach ($grupos as $row):

						if($row->rand == 1):
							$atual = $row->ordem;

							if($atual == $total):

								$this->model->updateRand($id_empreendimento, '001');

								$emails = $this->model->getGruposEmails($id_empreendimento, '001');

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							else:

								$atualizar = "00".$atual+1;
								$this->model->updateRand($id_empreendimento, $atualizar);

								$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
								//echo "<pre>";print_r($emails);echo "</pre>";

								$emails_txt = "";
								foreach ($emails as $email)
								{
									$grupo = $email->grupo;
									$list[] = $email->email;

									$emails_txt = $emails_txt.$email->email.", ";
								}
								$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

							endif;	
						endif;
					endforeach;
					/* envia email via roleta */

					if($p['nome'] == "Teste123"){
						$this->email->to('testes@divex.com.br');
					} else {
						$this->email->to($list);
					}

					$this->email->bcc('testes@divex.com.br');
					$this->email->from("noreply@polonex.com.br", "POLO - Nex Group");
					$this->email->subject('Interesse enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/interesse.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}

	public function setIndique()
	{
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome_remetente' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email_remetente' => strtolower($this->input->post('form_email')),
				'nome_destinatario' => (ucwords(strtolower($this->input->post('form_amigo_nome')))),
				'email_destinatario' => strtolower($this->input->post('form_amigo_email')),
				'comentarios' => ($this->input->post('form_mensagem'))
			);
			
			if(preg_match(EMAIL_FORMAT,$p['email_remetente']))
			{
				if(!empty($p['nome_remetente']) && !empty($p['nome_destinatario']) && !empty($p['email_destinatario']))
				{
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado por ' 		=> $p['nome_remetente']." / ".$p['email_remetente'],
						'Para'				=> $p['nome_destinatario']." / ".$p['email_destinatario'],
						'Empreendimento' 	=> $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						('Comentários') 	=> $p['comentarios'],
						'Hotsite'			=> "<a href='http://www.polonex.com.br'>polonex.com.br</a>"
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));

					$indique = $this->model->insert("indique",$p);

					/* envia email via roleta */
					$id_empreendimento = 83;
					$grupos = $this->model->getGrupos($id_empreendimento);
					$total = 0;
					$total = count($grupos);
					
					if($total == 1) // caso só tenho um grupo cadastrado
					{
						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						
						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);

					}
					else if($total > 1) // caso tenha mais de um grupo cadastrado
					{
						foreach ($grupos as $row):

							if($row->rand == 1):
								$atual = $row->ordem;
								if($atual == $total):

									$this->model->updateRand($id_empreendimento, '001');
									$emails = $this->model->getGruposEmails($id_empreendimento, '001');
									
									$emails_txt = "";
									foreach ($emails as $email)
									{
										$grupo = $email->grupo;
										$list[] = $email->email;

										$emails_txt = $emails_txt.$email->email.", ";
									}
									$this->model->grupo_indique($indique, $grupo, $emails_txt);
								
								else:

									$atualizar = "00".$atual+1;
									$this->model->updateRand($id_empreendimento, $atualizar);
									$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

									$emails_txt = "";
									foreach ($emails as $email)
									{
										$grupo = $email->grupo;
										$list[] = $email->email;

										$emails_txt = $emails_txt.$email->email.", ";
									}
									$this->model->grupo_indique($indique, $grupo, $emails_txt);

								endif;	
							endif;
						endforeach;
					}
					/* envia email via roleta */

					$this->email->to($p['email_destinatario']);
					$this->email->bcc("testes@divex.com.br");
					$this->email->from("noreply@polonex.com.br", "Nex Group");
					$this->email->subject("Polo Nex - Indicação enviada para você");
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					$this->email->send();

					//envia para corretores
					$this->email->from("noreply@polonex.com.br", "Nex Group");
					$this->email->to($list);	
					$this->email->cc("testes@divex.com.br");
					$this->email->subject("Polo Nex - Indique enviado via site");
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					$this->email->send();
					//envia para corretores

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/indique.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
}