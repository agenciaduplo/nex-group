<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function setContato(){
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email' => strtolower($this->input->post('form_email')),
				'comentarios' => ($this->input->post('form_mensagem')),
				'telefone' => $this->input->post('form_telefone'),
				'hotsite' => 'S'
			);
			if(preg_match(EMAIL_FORMAT,$p['email'])){
				if(!empty($p['nome'])){
					$interesse = $this->model->insert("interesses",$p);
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado em ' => $p['data_envio'] . ' via HotSite',
						'Enviado por ' => $p['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $p['ip'],
						'E-mail' => $p['email'],
						'Telefone' => $p['telefone'],
						('Comentários') => $p['comentarios']
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));
				
					$next = false; 
					foreach($this->model->getEmailGrupos() as $grupo){
						if($grupo->rand == '1'){
							$emails = array();
							foreach($this->model->getEmailGruposEmails($grupo->id) as $email){
								array_push($emails,$email->email);
							}
							$this->model->grupo_interesse($interesse,$grupo->id);
							$next = true;
							if($grupo->id == 4){
								$this->model->setNextEmailGroup(1);
								break;
							}
						} elseif($next){
							if($grupo->id == 5){
								$id = 1;
							} else {
								$id = $grupo->id;
							}
							$this->model->setNextEmailGroup($id);
							$next = false;
						}
					}
					if($p['nome'] == "Teste123"){
						$this->email->to('atendimento@divex.com.br');
					} else {
						$this->email->to($emails);
					}
					$this->email->bcc('atendimento@divex.com.br');
					$this->email->from("noreply@poloiguatemi.com.br", "Nex Group");
					$this->email->subject('Contato enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/contato.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
		public function setInteresse(){
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'id_estado' => 21,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email' => strtolower($this->input->post('form_email')),
				'comentarios' => ($this->input->post('form_mensagem')),
				'telefone' => $this->input->post('form_telefone'),
				'hotsite' => 'S'
			);
			if(preg_match(EMAIL_FORMAT,$p['email'])){
				if(!empty($p['nome'])){
					$interesse = $this->model->insert("interesses",$p);
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado em ' => $p['data_envio'] . ' via HotSite',
						'Enviado por ' => $p['nome'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						'IP' => $p['ip'],
						'E-mail' => $p['email'],
						'Telefone' => $p['telefone'],
						('Comentários') => $p['comentarios']
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));
				
					$next = false; 
					foreach($this->model->getEmailGrupos() as $grupo){
						if($grupo->rand == '1'){
							$emails = array();
							foreach($this->model->getEmailGruposEmails($grupo->id) as $email){
								array_push($emails,$email->email);
							}
							$this->model->grupo_interesse($interesse,$grupo->id);
							$next = true;
							if($grupo->id == 4){
								$this->model->setNextEmailGroup(1);
								break;
							}
						} elseif($next){
							if($grupo->id == 5){
								$id = 1;
							} else {
								$id = $grupo->id;
							}
							$this->model->setNextEmailGroup($id);
							$next = false;
						}
					}
					if($p['nome'] == "Teste123"){
						$this->email->to('atendimento@divex.com.br');
					} else {
						$this->email->to($emails);
					}
					$this->email->bcc('atendimento@divex.com.br');
					$this->email->from("noreply@poloiguatemi.com.br", "Nex Group");
					$this->email->subject('Interesse enviado via HotSite');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/interesse.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
	public function setIndique(){
		if($this->input->post()){
			$this->load->model('api_model','model');
			$p = array(
				'id_empreendimento' => 83,
				'ip' => $this->input->ip_address(),
				'user_agent' => $this->input->user_agent(),
				'url' => (isset($_SESSION['url']) ? $_SESSION['url'] : '______'),
				'origem' => (isset($_SESSION['origem']) ? $_SESSION['origem'] : ''),
				'data_envio' => date('Y-m-d H:i:s'),
				'nome_remetente' => (ucwords(strtolower($this->input->post('form_nome')))),
				'email_remetente' => strtolower($this->input->post('form_email')),
				'nome_destinatario' => (ucwords(strtolower($this->input->post('form_amigo_nome')))),
				'email_destinatario' => strtolower($this->input->post('form_amigo_email')),
				'comentarios' => ($this->input->post('form_mensagem'))
			);
			if(preg_match(EMAIL_FORMAT,$p['email_remetente'])){
				if(!empty($p['nome_remetente']) && !empty($p['nome_destinatario']) && !empty($p['email_destinatario'])){
					$empreendimento	= $this->model->getEmpreendimento($p['id_empreendimento']);
					$dados = array(
						'Enviado por ' => $p['nome_remetente'],
						'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
						('Comentários') => $p['comentarios'],
						'Hotsite'	=> "<a href='http://www.poloiguatemi.com.br'>www.poloiguatemi.com.br</a>"
					);

					$this->load->library('email',array(
						'mailtype' => 'html',
						'wordwrap' => false
					));
					if($p['nome_remetente'] == "Teste123"){
						$interesse = $this->model->insert("indique",$p);
					}
					$this->email->to($p['email_destinatario']);
					$this->email->from("noreply@poloiguatemi.com.br", "Nex Group");
					$this->email->subject('Indique :: NEX GROUP');
					$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
					
					$this->email->send();

					echo '
						<span class="msg-sucesso">
							Enviado com sucesso
							<iframe src="/adwords/indique.html" width="0px" height="0px" border="0"></iframe> 
							<script type="text/javascript">
								input.resetAll();
							</script>
							
						</span>
					';
				} else {
					echo '<span class="msg-erro">Preencha os campos requeridos</span>';
				}
			} else {
				echo '<span class="msg-erro">E-mail inválido</span>';
			}
		} else { show_404(); }
	}
}