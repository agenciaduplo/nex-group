<?php
$this->load->view('tpl/header');

/* Modals */

$this->load->view('parts/modal-interesse');
$this->load->view('parts/modal-amigo');

/* Pages */

$this->load->view('parts/page-home');
$this->load->view('parts/page-empreendimento');
$this->load->view('parts/page-infraestrutura');
$this->load->view('parts/page-plantas');
$this->load->view('parts/page-localizacao');
$this->load->view('parts/page-contato');

$this->load->view('tpl/footer');
?>