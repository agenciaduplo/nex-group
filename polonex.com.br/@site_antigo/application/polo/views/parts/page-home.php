<section id="home">
	<div class="bg">
		<div class="container_12">
			<header>
				<?php $this->load->view('tpl/menu',array('active' => 'home')); ?>
			</header>
			<h2>Único no espaço. <br>Ideal na localização.</h2>
			<div class="people fadeInRight animated hinge"></div>
			<ul class="nav"><a href="#empreendimentonav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
			<h4>2 e 3 suítes <span><br>89 a 136 m²</span></h4>
			<h5>Opções de <span><br>garden e cobertura</span></h5>
		</div>

		<div class="ctn-solicita">
			<h3>Solicite <strong>sua cotação</strong></h3>
			<form id="solicita_form" method="get" action="">
				<label>Nome*</label>
				<input class="textfield nome form_nome" type="text" name="">
				<label style="margin-top:51px">E-mail*</label>
				<input class="textfield email form_email" type="text" name="">
				<label style="margin-top:92px">Telefone</label>
				<input class="textfield telefone form_telefone" type="text" name="">
				<label style="margin-top:134px">Cidade</label>
				<input class="textfield bairro_cidade form_bairro_cidade" type="text" name="">
				<label style="margin-top:175px">Mensagem</label>
				<textarea class="form_mensagem" rows="" cols="" name=""></textarea>
				<a class="enviar" onclick="javascript:solicita.enviar();" href="javascript:void(0);"></a>
				<div class="msgSolicita"></div>
			</form>
		</div>
		
	</div>  
</section>