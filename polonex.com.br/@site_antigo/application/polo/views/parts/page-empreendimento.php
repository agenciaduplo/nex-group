<section id="empreendimento">
	<div class="bg-balls-details">
		<div class="bg2">
			<div class="container_12">
				<header id="empreendimentonav">
					<?php $this->load->view('tpl/menu',array('active' => 'empreendimento')); ?>
				</header>
				<h2>EMPREENDIMENTO</h2>
				<!--<h3>Um imóvel único no espaço, ideal na localização<br> 
					e surpreendente nos diferenciais.</h3>-->
					
				<div class="box-pins">
					<div class="box-pins-in pins-in-01 bounce animated hinge2"><p>Somente suítes, nada de dormitórios.</p></div>
					<div class="box-pins-in pins-in-02 bounce animated hinge2"><p>Piso aquecido no banheiro das suítes.</p></div>
					<div class="box-pins-in pins-in-03 bounce animated hinge2"><p>Leitura biométrica de acesso.</p></div>
					<div class="box-pins-in pins-in-04 bounce animated hinge2"><p>Quadra de tênis de saibro e lazer diferenciado.</p></div>
					<div class="box-pins-in pins-in-05 bounce animated hinge2"><p>A 200 metros do Shopping Iguatemi.</p></div>
					<div class="box-pins-in pins-in-06 bounce animated hinge2"><p>Assinatura Nex, um selo de qualidade.</p></div>
				</div>
				<div class="box-green">
				  <div class="theme-default">
						<div class="slider nivoSlider">
							<img src="<?=base_url(); ?>assets/img/emp/02.jpg" title="FACHADA">
							<img src="<?=base_url(); ?>assets/img/emp/03.jpg" title="PÁTIO CENTRAL">
							<img src="<?=base_url(); ?>assets/img/emp/01.jpg" title="PÓRTICO">
						</div>
					</div>						 
				</div>
				<ul class="nav"><a href="#infraestruturanav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
			</div>  
		</div>  
	</div>
</section>