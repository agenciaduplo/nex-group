<section id="home">
	<div class="bg">
		<div class="container_12">
			<header>
				<?php $this->load->view('tpl/menu',array('active' => 'home')); ?>
			</header>
			<h2>Único no espaço. <br>Ideal na localização.</h2>
			<div class="people fadeInRight animated hinge"></div>
			<ul class="nav"><a href="#empreendimentonav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
			<h4>2 e 3 suítes <span><br>89 a 136 m²</span></h4>
			<h5>Opções de <span><br>garden e cobertura</span></h5>
			
		</div>  
	</div>  
</section>