<section id="localizacao">

	<div class="pin lc1 bounce animated hinge2"><p>Av. Dr. Nilo Peçanha</p><div class="img"></div></div>
	<div class="pin lc2 bounce animated hinge2"><p>Country Club</p><div class="img"></div></div>
	<div class="pin lc3 bounce animated hinge2"><p>Shopping Iguatemi</p><div class="img"></div></div>
	<div class="pin lc4 bounce animated hinge2"><p>Pq. Germânia</p><div class="img"></div></div>
			
	<div class="localiza-img"></div> 
	<div class="people3"></div>
	<div class="boxtext"><p><span>Polo</span><br>A 200 metros do Shopping Iguatemi e ao lado do Parque Germânia. Mais dois motivos para ser especial.</p></div>
	<div class="bg">
		<div class="container_12">
			<header id="localizacaonav">
				<?php $this->load->view('tpl/menu',array('active' => 'localizacao')); ?>
			</header>
			<h2>LOCALIZAÇÃO</h2>
			<h3>chácara das pedras. Uma região que<br>
				vai ficar ainda mais valorizada com você.</h3>

			<ul class="nav"><a href="#contatonav" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
		</div>  
	</div>  
</section>