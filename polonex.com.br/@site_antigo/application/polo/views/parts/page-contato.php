<section id="contato">
	<div class="bg-balls-details">
		<div class="bg2">
			<div class="container_12">
				<header id="contatonav">
					<?php $this->load->view('tpl/menu',array('active' => 'contato')); ?>
				</header>
				<h2>plantão de vendas<br> e apartamento decorado</h2>
				<address>RUA CANANÉIA, 291 
						<br />CHÁCARA DAS PEDRAS
						<br />PORTO ALEGRE - RS - BRASIL</address>
				<h3>(51) 3331 9247</h3>
				<div class="mapa"></div>
				<form action="" method="get" id="contato_form">
				 	<label>Nome*</label><input class="textfield nome form_nome" name="" type="text">  
				 	<label style="margin-top:51px">E-mail*</label><input class="textfield email form_email" name="" type="text">  
				 	<label style="margin-top:92px">Telefone</label><input class="textfield telefone form_telefone" name="" type="text">  
					<label style="margin-top:135px;">Mensagem</label><textarea name="" cols="" rows="" class="form_mensagem"></textarea>
					<a class="enviar" href="javascript:void(0);" onclick="javascript:contato.enviar();"></a>
					<div class="msg msg-sucesso2"></div>
				</form>
				<ul><a href="#home" class="scroll"><div class="pinnav bounce animated hinge2"></div></a></ul>
				
			</div>  
		</div>  
	</div>
</section>