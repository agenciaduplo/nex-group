<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<head>
    <title>Apartamentos de 2 ou 3 suítes, opções de garden e cobertura - Polo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Único no espaço. Ideal na Localização. 2 ou 3 suítes de 89 a 136 m², opções de garden e cobertura. A 200 metros do Shopping Iguatemi em Porto Alegre.">
    <meta name="keywords" content="apartamentos, aptos, imoveis, imovel, 2 suites, 3 suites, garden, cobertura, porto alegre, chácara das pedras, proximo ao iguatemi, iguatemi, porto alegre, nex group, tenis, brinquedoteca, piscina com raia, piso aquecido, leitor biométrico">
    
    <meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='Polo' />
    <!-- <meta property='og:image' content='http://www.polonex.com.br/assets/img/logo-branco.png'/> -->
    <meta property='og:description' content='2 ou 3 suítes e opções de garden e cobertura a 200 Metros do Shopping Iguatemi'/>
    <meta property='og:url' content='http://www.polonex.com.br'/>

    <link type="image/x-icon" rel="shortcut icon" href="<?=base_url();?>assets/img/favicon.ico"  />

    <link type='text/css' rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'>
    
    <!-- <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/orbit-1.2.3.css"> -->
    <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/animate.css">
    <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/styles.css">

    <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/webfontkit/stylesheet.css" charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome/css/font-awesome.css">

    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/ga.js"></script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-42834805-1']);
        _gaq.push(['_trackPageview']);
        (function()
            { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
        )();
    </script>

</head>    
<body>  

    <div id="ancora"></div>
        
    <nav class="header-fixo smoth fixo clickroll">
        <a href="#paralau" class="logo">
            <h1 class="title">POLO</h1>
        </a>
        <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=15&referencia=______','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="ste lk1 smoth">
            <i class="icon icon-comments"></i><p>Corretor Online</p>
        </a>
        <div class="right">
            <p class="segues">Últimas unidades<br>garanta a sua agora:</p>

            <form id="formInteresse" name="formInteresse" action="<?=base_url();?>home/envia_interesse" method="post" onsubmit="return false;">
                <input name="nome" id="nome" type="text" class="field" placeholder="Seu nome:" maxlength="300" />

                <input name="email" id="email" type="text" class="field" placeholder="Seu e-mail:" maxlength="300" />
                <input name="telefone" id="telefone" type="text" class="field" placeholder="Seu telefone:" maxlength="14" />

                <input type="button" class="btn" value="Ok" onclick="Interesse.submit();" />
            </form>

        </div>
        <section id="assina" class="">
            <a href="http://www.nexgroup.com.br/" target="_blank"><img class="left" src="<?=base_url();?>assets/img/newlife_nex_logo.png"></a>
            <p>Todos os direitos reservados. Desenvolvido por</p>
            <a href="http://imobi.divex.com.br/" target="_blank"><img class="right" src="<?=base_url();?>assets/img/divex.png"></a>
        </section>
    </nav>

    <section id="paralau" class="bloco">
        <div class="clickroll">
            <a href="#home" class="ste">
                <i class="icon icon-chevron-down"></i><p>veja imagens</p>
            </a>
        </div>
        <div id="container" class="container ">
            <ul id="scene" class="scene">
                <li class="layer bloco" data-depth="0.20"><img class="seg1" src="<?=base_url();?>assets/img/parallax/bg.jpg"></li>
                <li class="layer bloco" data-depth="-2.0"><img class="seg45" src="<?=base_url();?>assets/img/parallax/4.png"></li>

                <li class="layer bloco" data-depth="0.50"><img class="seg2" src="<?=base_url();?>assets/img/parallax/3.png"></li>
                <li class="layer bloco" data-depth="1.80"><img class="seg4" src="<?=base_url();?>assets/img/parallax/4.png"></li>

                <li class="layer bloco" data-depth="-0.40"><img class="seg5" src="<?=base_url();?>assets/img/parallax/5.png"></li>
                <li class="layer bloco" data-depth="0.60"><img class="seg6" src="<?=base_url();?>assets/img/parallax/6.png"></li>
                <li class="layer bloco" data-depth="-0.40"><img class="seg3" src="<?=base_url();?>assets/img/parallax/3.png"></li>
                <li class="layer bloco" data-depth="0.70"><img class="seg7" src="<?=base_url();?>assets/img/parallax/polo.png"></li>
            </ul>
        </div>
    </section>

    <section id="home" class="bloco">
        <div class="clickroll">
            <a href="#paralau" class="ste">
                <i class="icon icon-chevron-up"></i><p>voltar</p>
            </a>
        </div>
        <div id="fotos2" class="list_carousel clickroll">
            <ul id="foo5">
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/01.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Fachada</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/01.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Fachada"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/02.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Pátio Central</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/02.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Pátio Central"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/03.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Pórtico</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/03.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Pórtico"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/04.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Fitness</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/02.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Fitness"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/05.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Brinquedoteca</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/05.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Brinquedoteca"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/06.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Lazer Externo</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/06.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Lazer Externo"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/07.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Piscina Externa</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/07.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Piscina Externa"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/08.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Piscina aquecida com raia</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/08.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Piscina aquecida com raia"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/09.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Quadra de tênis de saibro</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/09.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Quadra de tênis de saibro"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/10.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Tennis Lounge</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/10.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Tennis Lounge"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="background:url(<?=base_url();?>assets/img/fotos/13.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Sala de jogos adulto</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/13.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Sala de jogos adulto"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
                <li class="box scales bloco paral" style="<?=base_url();?>assets/background:url(img/fotos/14.jpg)">
                    <div class="box2">
                        <div class="left">
                            <h3><i>Sala de jogos teen</i></h3>
                            <a href="<?=base_url();?>assets/img/fotos/14.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Sala de jogos teen"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
                        </div>
                    </div>
                    <div class="black"></div>
                </li>
            </ul>
            <div class="clearfix"></div>
            <div class="navegation">
                <a id="prev5" class="prev" href="#fotos2"></a>
                <div id="pager5" class="pager"></div>
                <a id="next5" class="next" href="#fotos2"></a>
            </div>
        </div>

        <div class="local bloco"><img class="seg3" src="<?=base_url();?>assets/img/planta.png"></div>
    </section>

    <script type="text/javascript" src="<?=base_url();?>assets/js/parallax.js"></script>
    <script>
      var scene = document.getElementById('scene');
      var parallax = new Parallax(scene);
    </script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/helper-plugins/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/helper-plugins/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/helper-plugins/jquery.transit.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.orbit-1.2.3.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/background/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/background/jquery.localscroll-1.2.7-min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/background/jquery.scrollTo-1.4.2-min.js"></script>
    <script type='text/javascript' src='<?=base_url();?>assets/js/background/animate.js'></script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/parallax-plugin.js"></script>


    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

    <script type="text/javascript" src="<?=base_url();?>assets/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script> 
    <script type="text/javascript" src="<?=base_url();?>assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.backgroundSize.js"></script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>assets/js/init.js"></script>

    <script>
        $(document).ready(function() {
            $("#telefone", "#formInteresse").mask("(99) 9999-9999");

            $(".fancybox-thumb").fancybox({
                padding: 0,
                margin: 30,
                prevEffect: 'none',
                nextEffect: 'none',
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
        });    
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 978900397;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/978900397/?value=0&guid=ON&script=0"/>
        </div>
    </noscript>
</body>
</html>