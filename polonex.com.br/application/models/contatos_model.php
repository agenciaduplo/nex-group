<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contatos_model extends CI_Model{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    	
	function setInteresse($data)
	{
		$this->db->set($data)->insert('interesses');
		return $this->db->insert_id();
	}

	function verificaLastEmailSendFor($limit = 4, $table="interesses"){
		$sql = "SELECT email_enviado_for FROM {$table} ORDER BY data_envio DESC LIMIT {$limit}";
		$ret = $this->db->query($sql)->result();

		return $ret;
	}
	
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */