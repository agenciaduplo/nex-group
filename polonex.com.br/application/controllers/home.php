<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		
		@$utm_source 	= @$_GET['utm_source'];
		@$utm_medium 	= @$_GET['utm_medium'];
		@$utm_content 	= @$_GET['utm_content'];
		@$utm_campaign 	= @$_GET['utm_campaign'];
		
		@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
			
		@session_start();
		
		if(!@$_SESSION['POLO']['REFERER'])
		{
			@$_SESSION['POLO']['REFERER'] = @$_SERVER['HTTP_REFERER'];
		}
		if(!@$_SESSION['POLO']['URL'] )
		{
			@$_SESSION['POLO']['URL'] = @$url;
		}
		if(@$_SESSION['POLO']['URL'] == "______")
		{
			@$_SESSION['POLO']['URL'] = @$url;
		}


		$this->load->view('home');
	}

	public function envia_interesse() 
	{
		if(!$this->agent->is_robot())
		{
			$this->load->model('contatos_model','model');

			$nome = $this->input->post('nome');
			$email = $this->input->post('email');
			$telefone = $this->input->post('telefone');

			if (!preg_match("/^[a-zA-ZãÃáÁàÀêÊéÉèÈíÍìÌôÔõÕóÓòÒúÚùÙûÛçÇºª ]+$/", $nome)) {
				$response = array("erro" => 1, "msg" => "Nome inválido, preencha o campo nome corretamente!");
				echo json_encode($response);
				exit;
			}

			$this->load->helper('email');

			if (!valid_email($email)) {
				$response = array("erro" => 2, "msg" => "Email inválido, preencha o campo email corretamente!");
				echo json_encode($response);
				exit;
			}

			if($telefone != "" || $telefone == "(__) ____-____") {
				if (!preg_match('/^\([0-9]{2}\) [0-9]{4}\-[0-9]{4}$/', $telefone)) {
					$response = array("erro" => 3, "msg" => "Telefone inválido, preencha o campo telefone corretamente!");
					echo json_encode($response);
					exit;
				}
			} else {
				$telefone = "Não informado";
			}


			//Inicia o envio do email
			//===================================================================

			$this->load->library('email');
			
			$config['protocol'] = 'sendmail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n";

			$this->email->initialize($config);

			@session_start();
			

			// 	ROLETA DE ENVIO DO EMAIL
			//===================================================================

			$email_nex 	= "vendas@nexvendas.com.br";
			$email_out 	= "lucia.keiko@vendasbbsul.com.br";

			// $email_nex = "vendas@divex.com.br";
			// $email_out = "japa@divex.com.br";

			$email_go	= $email_nex;
			$id_cidade 	= 4237;

			$url = isset($_SESSION['POLO']['URL']) ? @$_SESSION['POLO']['URL'] : '______';
			$url = explode("__",$url);

			$utm_source   = $url[0];
			$utm_medium   = $url[1];
			$utm_campaign = $url[3];

			if(@$_SESSION['POLO']['URL'] != "") {
				$campanha_url = @$_SESSION['POLO']['URL'];
			}

			if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
				$email_for =  $this->model->verificaLastEmailSendFor(1, "interesses", $campanha_url);
				foreach($email_for as $emf){
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$email_go = $email_out;
						break;
					}else{
						$email_go = $email_nex;
						break;
					}
				}
			}else{ // Porto Alegre | 1 quarto dos interesses para fora.
				$email_for =  $this->model->verificaLastEmailSendFor(3, "interesses");
				$i = 0;
				$flag = false;

				foreach($email_for as $emf){
					++$i;
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$flag = true;
					}else if($emf->email_enviado_for == $email_out && $i = 1){
						$email_go = $email_nex;
						break;
					}else{
						$flag = false;
					}

					if($flag){
						$email_go = $email_out;
					}else{
						$email_go = $email_nex;
					}
				}
			}

			// 	Cadastra interesse na database
			//===================================================================

			$data = array(
				'data_envio' 		=> date("Y-m-d H:i:s"),
				'nome'				=> mb_strtoupper($nome),
				'email'				=> mb_strtoupper($email),
				'telefone'			=> $telefone,
				'id_empreendimento'	=> 83,
				'id_estado'			=> 21,
				'forma_contato'		=> "Email",
				'origem'			=> @$_SESSION['POLO']['REFERER'],
				'url'				=> @$_SESSION['POLO']['URL'],
				'ip'				=> $this->input->ip_address(),
				'user_agent'		=> $this->input->user_agent(),
				'email_enviado_for' => $email_go
				);

			$this->model->setInteresse($data);

			// 	Fim do cadastro de interesse na database
			//===================================================================
			//
			//  Mensagem do Email

			ob_start();

			?>
			<html>
			<head>
				<title>Nex Group</title>
			</head>
			<body>
				<table>
					<tr>
						<td colspan="2">
							<a title="Acesse o portal Nex Group" href="http://www.nexgroup.com.br" target="_blank" >
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" />
							</a>
						</td>
					</tr>
					<tr>
						<td colspan='2'>Enviado em <?php echo date("d/m/Y") . " ás " . date("H:i:s"); ?></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td>Nome:</td>
						<td><?php echo $nome; ?></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><?php echo $email; ?></td>
					</tr>
					<tr>
						<td>Telefone:</td>
						<td><?php echo $telefone; ?></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='2'>
							<a  title="Acesse o portal Nex Group" href='http://www.nexgroup.com.br' target="_blank">http://www.nexgroup.com.br</a>
						</td>
					</tr>
				</table>
			</body>
			</html>
			<?

			$conteudo = ob_get_contents();
			ob_end_clean();

			// Fim da Mensagem

			// Config de recepientes
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");
			$this->email->to("{$email_go}");
			$this->email->subject('POLO - Novo contato de interesse');
			$this->email->message("$conteudo");


			$this->email->send();

			//Termina o envio do email	
			//===================================================================

			$response = array("erro" => 0, "msg" => "Interesse enviado com sucesso! Em breve entraremos em contato.");
			echo json_encode($response);
			exit;

		} else {
			exit;
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */