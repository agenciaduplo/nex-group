$(function(){
	$('#interessado').click(function(){
		if($('#indique-amigo').is(':visible')) {
			$('#indique-amigo').hide();
			$('#indicador').removeClass('clicouFade');
			$("#form_indique").validationEngine('hide');
		}
		$('#contato-interesse').stop().toggle();
		$('#interessado').stop().toggleClass('clicouFade');
		$('.menuTopo').stop().attr('height',30);
		$("#contatoform").validationEngine('hide');
	});
	$('#indicador').click(function(){
		if($('#contato-interesse').is(':visible')) {
			$('#contato-interesse').hide();
			$('#interessado').removeClass('clicouFade');
			$("#form_interesse").validationEngine('hide');
		}
		$('#indique-amigo').stop().toggle();
		$('#indicador').stop().toggleClass('clicouFade');
		$('.menuTopo').stop().attr('height',30);
		$("#contatoform").validationEngine('hide');
	});

	$('#form_telefone_indique').mask('(99) 9999-9999');
	$('#form_telefone_interesse').mask('(99) 9999-9999');
	$("#form_indique").validationEngine('attach', {promptPosition : "topRight"});
	$("#form_interesse").validationEngine('attach', {promptPosition : "topRight"});
});