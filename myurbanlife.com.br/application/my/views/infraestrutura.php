<?php
$this->load->view('tpl/header',array(
		'description' => 'Description:
Infraestrutura completa com mais de 14 opções de lazer, apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.',
		'keywords' => 'infraestrutura completa, mais de 14 opções de lazer, conheça o empreendimento, apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group',
		'title' => 'Infraestrutura - Apartamentos 2 dormitórios ou living estendido'
	));

?>
	<h1>INFRAESTRUTURA</h1>
	
	<div class="sliderWrap"></div><!-- fundo amarelinho transparente com cidade no fundo-->
		<article class="sliderContent">
			<div id="wrapper">
				<div class="slider-wrapper theme-dark">
					<div id="slider" class="nivoSlider">
						<img src="<?=base_url(); ?>assets/img/infraestrutura/fitness.jpg" width="100%" alt="" title="FITNESS"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/lavanderia.jpg" width="100%" alt="" title="LAVANDERIA"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/Playbaby.jpg" width="100%" alt="" title="PLAYBABY"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/Playkids.jpg" width="100%" alt="" title="PLAYKIDS"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/salaodefestas.jpg" width="100%" alt="" title="SALÃO DE FESTAS"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/salaogourmet.jpg" width="100%" alt="" title="SALÃO GOURMET"/>
						<img src="<?=base_url(); ?>assets/img/infraestrutura/wifizone.jpg" width="100%" alt="" title="WIFI ZONE"/>
					</div>
					<div class="controlNav-BKG-Bottom"></div>
					<div id="legendaespecial" class="nivo-html-caption"></div>
				</div>
		
			</div>
		</article> <!-- position absolute com o slider -->
 <section class="listaWrap">
	<em>MAIS DE 14<br />OPÇÕES DE LAZER</em>
			<ul class="lista-infra">
				<li><i class="icon-list"></i>Wifi-zone</li>
				<li><i class="icon-list"></i>For kids</li>
				<li><i class="icon-list"></i>Salão de Festas</li>
				<li><i class="icon-list"></i>Pergolado</li>
			</ul>
			<ul class="lista-infra" style="width:20%;">
				<li><i class="icon-list"></i>2 quiosques com churrasqueiras</li>
				<li><i class="icon-list"></i>Redário</li>
				<li><i class="icon-list"></i>Lavanderia</li>
				<li><i class="icon-list"></i>Espaço Gourmet</li>
			</ul>
			<ul class="lista-infra" style="width:10%">
				<li><i class="icon-list"></i>Bicicletário</li>
				<li><i class="icon-list"></i>Baby Place</li>
				<li><i class="icon-list"></i>Fitness</li>
				<li><i class="icon-list"></i>Espaço Pet</li>
			</ul>
			<ul class="lista-infra">
				<li><i class="icon-list"></i>Espaço Chima</li>
				<li><i class="icon-list"></i>2 Quiosques com churrasqueira e forno de pizza</li>
				<li><i class="icon-list"></i>Horta</li>
			</ul>
		</section>
		
		<article class="content">
			<address><span>ENDEREÇO DO FUTURO <br />EMPREENDIMENTO</span>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p>ESSA É A MELHOR FASE DAS NOSSAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>
   
<script type="text/javascript">
	$(function(){
		$('#slider').nivoSlider();
	});
</script>
<?php
$this->load->view('tpl/footer');
?>