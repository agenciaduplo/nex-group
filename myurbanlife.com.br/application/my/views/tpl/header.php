<?php 
$url = $this->uri->segment(1); source();
?><!DOCTYPE html>
<html lang="pt-BR"> 

<!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
<!--[if IE 8]>	 <html class="ie8" lang="pt-BR"> <![endif]-->  
<!--[if IE 9]>	 <html class="ie9" lang="pt-BR"> <![endif]-->  

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="<?=isset($description) ? $description : 'Apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.'; ?>">
<meta name="keywords" content="<?=isset($keywords) ? $keywords : 'apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group'; ?>">
<title><?=isset($title) ? $title . ' - My Urban Life - Minhas Escolhas, Minhas Conquistas' : 'Apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas'; ?></title>

<meta property="og:locale" content="pt_BR">
<meta property="og:title" content="My Urban Life">
<meta property="og:image" content="<?=base_url(); ?>assets/img/logo.png">
<meta property="og:description" content="">
<meta property="og:url" content="<?=base_url(); ?>">

<link rel="shortcut icon" type="image/png" href="<?=base_url(); ?>favicon.png">
<link href="http://fonts.googleapis.com/css?family=Lato:400,400italic,700italic,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/slider/default.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/slider/dark.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/nivo-slider.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/slider1.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/slider/default.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/validation.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/<?=!empty($url) ? $url : 'home'; ?>.css">


<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.ui.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/modernizr.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.validation.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>assets/js/onload.js"></script>

<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-42814161-1']);
_gaq.push(['_trackPageview']);
(function()
{ var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
)();
</script>

</head>
<body class="<?=isset($class) ? $class : 'home'; ?>">

<script type="text/javascript">

</script>
	<div class="wrap"></div>
	<div class="headerbg"></div>
	<header>
		<figure></figure><!--logo-->

			<nav id="top">
				<ul>
					<li class="menuTopo menuinter" id="interessado"><a href="javascript:void(0);"><i id="interesse"></i><b>Tenho</b><br />Interesse</a></li>
					<li class="menuTopo menucorreto"><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=26','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);"><i id="corretor"></i><b>Fale com o</b><br />Corretor Online</a></li>
					<li class="menuTopo menuamigo" id="indicador"><a href="javascript:void(0);"><i id="indique"></i><b>Indique para</b><br />um amigo</a></li>
					<!--<li class="menuTopo menuvirtual"><a href="javascript:void(0);"><i id="tour"></i><b>Tour</b><br />Virtual</a></li>-->
					<li class="menuTopo menuvideo"><a href="http://www.youtube.com/watch?v=4puqVHYyh4A&feature=plcp" target="_blank"><i id="video"></i><b>Assista</b><br />ao Vídeo</a></li>
				</ul>
			</nav>
			<section id="contato-interesse">
				<div class="borderMaskInteresse"></div>
				<form id="form_interesse" action="<?=site_url(); ?>contato/setInteresse?return=<?=rawurlencode(current_url()); ?>" method="post">
					<h5>TENHO INTERESSE NO EMPREENDIMENTO</h5>
					<fieldset class="interesseDados">
						<input class="inputDados validate[required]" id="form_interesse_nome" type="text" name="form_nome" value="Nome" <?=placeholder("Nome") ?>><br />
						<input class="inputDados validate[required,custom[email]]" id="form_interesse_email" type="text" name="form_email" value="Email" <?=placeholder("Email") ?>><br />
						<input class="inputDados validate[required]" type="text" name="form_telefone" id="form_telefone_interesse" placeholder="Telefone">
					</fieldset> <!-- dados pessoais -->
					<fieldset class="interesseMensagem">
						<textarea name="form_comentario" class="validate[required]" id="form_interesse_mensagem" <?=placeholder("Mensagem") ?>>Mensagem</textarea>
					</fieldset> <!-- mensagem -->
					<input id="submit" type="submit" value="ENVIAR">
				</form>
			</section> <!-- contato / interesse-->
			<section id="indique-amigo"> <!-- indicar amigo -->
				<div class="borderMaskIndicar"></div>
				<form id="form_indique" action="<?=site_url(); ?>contato/setIndique?return=<?=rawurlencode(current_url()); ?>" method="post">
					<h5>INDIQUE PARA UM AMIGO</h5>
					<fieldset class="indiqueDados pessoa"><!-- dados PESSOA -->
						<span>DE</span>
						<input class="inputDados validate[required]" id="form_de_nome" type="text" name="form_nome" value="Nome" <?=placeholder("Nome") ?>><br />
						<input class="inputDados validate[required,custom[email]]" id="form_de_email" type="text" name="form_email" value="Email" <?=placeholder("Email") ?>><br />
					</fieldset> 
					<fieldset class="indiqueDados amigo">
						<span>PARA</span><!-- dados AMIGO -->
						<input style="text-align:left;"class="inputDados validate[required]" id="form_para_nome" type="text" name="form_nome_amigo" value="Nome" <?=placeholder("Nome") ?>><br />
						<input class="inputDados validate[required,custom[email]]" id="form_para_email" type="text" name="form_email_amigo" value="Email" <?=placeholder("Email") ?>><br />
					</fieldset> <!-- dados pessoais -->
					<fieldset class="indiqueMensagem">
						<textarea id="form_indique_mensagem" class="validate[required]" name="form_comentario" <?=placeholder("Mensagem") ?>>Mensagem</textarea>
					</fieldset> <!-- mensagem -->
					<input id="submit" type="submit" value="ENVIAR">
				</form>
			</section> <!-- contato / interesse-->
			<nav id="menu">
				<ul>
					<li class="<?=empty($url) ? 'current' : ''; ?>"><a href="<?=site_url(); ?>">HOME</a></li>
					<li class="<?=$url == 'empreendimentos' ? 'current' : ''; ?>"><a href="<?=site_url(); ?>empreendimentos">EMPREENDIMENTO</a></li>
					<li class="<?=$url == 'infraestrutura' ? 'current' : ''; ?>"><a href="<?=site_url(); ?>infraestrutura">INFRAESTRUTURA</a></li>
					<li class="<?=$url == 'plantas' ? 'current' : ''; ?>"><a href="<?=site_url(); ?>plantas">PLANTAS</a></li>
					<li class="<?=$url == 'localizacao' ? 'current' : ''; ?>"><a href="<?=site_url(); ?>localizacao">LOCALIZAÇÃO</a></li>
					<li class="<?=$url == 'contato' ? 'current' : ''; ?>"><a href="<?=site_url(); ?>contato">CONTATO</a></li>
				</ul>
					<!--<a title="Instagram" href="javascript:void(0);" target="_blank"><i id="instagram"></i></a>-->
					<a href="https://www.facebook.com/NexGroup" title="Facebook" target="_blank"><i id="facebook"></i></a>
				<!-- 					<li><a href="javascript:void(0);"><i id="instagram"></i></a></li>
					<li><a href="javascript:void(0);"><i id="facebook"></i></a></li> -->
			</nav><!-- menu + social -->
		</header>