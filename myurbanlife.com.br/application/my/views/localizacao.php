<?php
$this->load->view('tpl/header',array(
		'description' => 'Localização, apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.',
		'keywords' => 'localização, mais de 14 opções de lazer, conheça o empreendimento, apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group',
		'title' => 'Apartamentos 2 dormitórios ou living estendido',
		'class' => 'localizacao'
	));

?>
	<section class="mapWrap">
		<section class="mapContent">
			<img src="<?=base_url(); ?>assets/img/map.jpg" title="Rua 1º de Março esq. Rua 1º Setembro" alt="Mapa My Urban Life" />
			<p>Todas as suas escolhas sempre por perto.</p>
		</section>
	</section>

	<div  class="content">
		<h1>LOCALIZAÇÃO</h1>
		<h2>SOMOS CAPAZES DE
TRANSFORMAR O MUNDO
À NOSSA VOLTA.</h2>
		<article id="conteudo">
		</article>
		<h4 class="marginleft">BOURBON IPIRANGA<br>6 mins</h4>
		<h4>AV. CARLOS GOMES<br>10 mins</h4>
		<h4>PUCRS<br>5 mins</h4>
	 </div>  

		<article class="content2">
			<address><span>ENDEREÇO DO FUTURO <br />EMPREENDIMENTO</span>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p>ESSA É A MELHOR FASE DAS NOSSAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>
<?php
$this->load->view('tpl/footer');
?>