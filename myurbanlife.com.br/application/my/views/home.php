<?php
$this->load->view('tpl/header');
?>
		<article class="content">
			<div id="boxFrmCotacao">
				<form action="<?=site_url(); ?>contato/setCotacao?return=<?=rawurlencode(current_url()); ?>" method="post" id="cotacaoForm">
					<h2>SOLICITE COTAÇÃO</h2>
					<fieldset class="frmCotacao">
						<input class="inputDados validate[required]" type="text" id="cot_nome" name="nome" 
						<?=placeholder("Nome"); ?> value="Nome"/>
						<input class="inputDados validate[required,custom[email]]" type="text" id="cot_email" name="email" 
						<?=placeholder("Email"); ?> value="Email"/>
						<input class="inputDados validate[required]" type="text" id="cot_telefone" name="telefone" placeholder="Telefone">
						<input class="inputDados validate[required]" type="text" id="cot_cidade" name="cidade" 
						<?=placeholder("Cidade"); ?> value="Cidade"/>
						<textarea id="cot_mensagem" name="mensagem" class="validate[required]" <?=placeholder("Mensagem") ?>>Mensagem</textarea>
					</fieldset>
					<input id="submit" type="submit" value="ENVIAR" onClick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Enviar Mensagem de Contato My Urban Life'])">
				</form>
			</div>
			<strong id="escolhas">Minhas Escolhas,</strong>
			<strong id="conquistas">Minhas Conquistas</strong>
			<em>2 dormitórios<br />ou<br />living estendido</em>
			<a class="textdecorationnone" href="<?=site_url(); ?>empreendimentos">
				<section id="boxEmp">
					<figcaption>EMPREENDIMENTO</figcaption>
					<figure><img src="<?=base_url(); ?>assets/img/empreendimento.png" title="MY URBAN LIFE - Empreendimento" /></figure>
				</section>
			</a>
			<a class="textdecorationnone" href="<?=site_url(); ?>infraestrutura">
				<section id="boxInfra">
					<figcaption>INFRAESTRUTURA</figcaption>
					<figure><img src="<?=base_url(); ?>assets/img/infraestrutura.jpg" title="MY URBAN LIFE - Infraestrutura" /></figure>
				</section>
			</a>
			<a class="textdecorationnone" href="<?=site_url(); ?>plantas">
				<section id="boxPlant">
					<figcaption>PLANTAS</figcaption>
					<figure><img src="<?=base_url(); ?>assets/img/planta.png" title="MY URBAN LIFE - Plantas" /></figure>
				</section>
			</a>
			

			<address>
			<span>ENDEREÇO DO FUTURO <br />EMPREENDIMENTO</span>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p class="texto_home">ESSA É A MELHOR FASE DAS NOSSAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>
<script type="text/javascript">
$(function(){
	$('#cot_telefone').mask('(99) 9999-9999');
	$("#cotacaoForm").validationEngine('attach', {promptPosition : "topRight"});
});
</script>

<?php
$this->load->view('tpl/footer');
?>