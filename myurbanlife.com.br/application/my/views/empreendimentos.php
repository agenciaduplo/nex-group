<?php
$this->load->view('tpl/header',array(
		'description' => 'Conheça o empreendimento, apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.',
		'keywords' => 'empreendimento, conheça o empreendimento, apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group',
		'title' => 'Empreendimento - Apartamentos 2 dormitórios ou living estendido'
	));

?>
	
	<h1>EMPREENDIMENTO</h1>
	
	<div class="sliderWrap"></div><!-- fundo amarelinho transparente com cidade no fundo-->
		<article class="sliderContent">
			<div id="wrapper">
				<div class="slider-wrapper theme-dark">
					<div id="slider" class="nivoSlider">
						<img src="<?=base_url(); ?>assets/img/empreendimento/fachada.png" alt="" title="FACHADA"/>
						<img src="<?=base_url(); ?>assets/img/empreendimento/fachada2.jpg"  alt="" title="FACHADA" />
						<img src="<?=base_url(); ?>assets/img/empreendimento/living1d.jpg" alt="" title="LIVING"/>
						<img src="<?=base_url(); ?>assets/img/empreendimento/living2d.jpg"  alt="" title="LIVING" />
					</div>
					<div class="controlNav-BKG-Bottom"></div>
					<div id="legendaespecial" class="nivo-html-caption">
					</div>
				</div>
		
			</div>
		</article> <!-- position absolute com o slider -->
		<article class="content">
			<address><span>ENDEREÇO DO FUTURO <br />EMPREENDIMENTO</span>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p>ESSA É A MELHOR FASE DAS NOSSAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>
	</article>

<script type="text/javascript">
	$(function(){
		$('#slider').nivoSlider();
	});
</script>

<?php
$this->load->view('tpl/footer');
?>