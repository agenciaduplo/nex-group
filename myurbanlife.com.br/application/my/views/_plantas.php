<?php
$this->load->view('tpl/header',array(
		'description' => 'Conheça as plantas, apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.',
		'keywords' => 'conheça as plantas, plantas, mais de 14 opções de lazer, conheça o empreendimento, apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group',
		'title' => 'Plantas - Apartamentos 2 dormitórios ou living estendido',
		'class' => 'planta'
	));

?>
		<h1>PLANTAS</h1>
		
		<div class="conteudo"></div>
		<div class="conteudodown">
			<p id='tipoPlanta'>2 dormitórios ou<br />living estendido</p>
			<p id='area'>51m²</p>
			<!-- planta superior -->
			<figure id="planta1">
				<img src="<?=base_url(); ?>assets/img/plantas/planta1.png" />
			</figure>
			
			<!-- planta inferior -->
			<figure id="planta2">
				<img src="<?=base_url(); ?>assets/img/plantas/planta2.png" />
			</figure>
		</div>
		<article class="content">
			<address><span>ENDEREÇO DO FUTURO <br />EMPREENDIMENTO</span>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p>ESSA É A MELHOR FASE DAS NOVAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>
<?php
$this->load->view('tpl/footer');
?>