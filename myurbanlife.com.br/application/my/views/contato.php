<?php
$this->load->view('tpl/header',array(
		'keywords' => 'fale conosco, contato, mais de 14 opções de lazer, conheça o empreendimento, apartamento, 2 dormitórios, living estendido, apto, 2 dorms, my urban life, capa engenharia, rodobens, nex group',
		'description' => 'Entre em contato conosco, apartamentos 2 dormitórios ou living estendido - My Urban Life - Minhas Escolhas, Minhas Conquistas. Mais de 14 opções de lazer. Um empreendimento Nex Group / Capa Engenharia / Rodobens.',
		'title' => 'Contato - Apartamentos 2 dormitórios ou living estendido',
		'class' => 'contato'
	));

?>
		<article class="content">
			<h1>CONTATO</h1>
			<h2>plantão de vendas e apartamento decorado</h2>
			<!--<h3>Para a nossa geração, tudo é possível. </h3>-->
			<span>* Campos Obrigatórios</span>
			<form id="contatoform" action="<?=site_url(); ?>contato/setContato?return=<?=rawurlencode(current_url()); ?>" method="post">
				<fieldset class="interesseDados">
					<input class="inputDados validate[required]" type="text" id="form_nome" name="nome" <?=placeholder("Nome") ?> value="Nome"><br />
					<input class="inputDados validate[required,custom[email]]" type="text" id="form_email" name="email" <?=placeholder("Email") ?> value="Email"><br />
					<input class="inputDados validate[required]" type="text" id="form_telefone" name="telefone" placeholder="Telefone">
					<textarea id="form_mensagem" name="comentarios" class="validate[required]" <?=placeholder("Mensagem") ?>>Mensagem</textarea>
				</fieldset> <!-- dados pessoais -->
				<input id="submit" type="submit" value="ENVIAR" onClick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Enviar Mensagem de Contato My Urban Life'])">
			</form>
			
		</article>
		
		<article class="content2">
			<address><p class="visitespan">ENDEREÇO DO FUTURO<br>EMPREENDIMENTO</p>
			<p><i id="fone"></i>Rua 1° de Março esq. Rua 1° de Setembro<br />Porto  Alegre- RS - Brasil</p>
			<p>(51) 3092.3124<p></address>
			<p>ESSA É A MELHOR FASE DAS NOSSAS VIDAS<br />VAMOS COMPARTILHAR AS NOSSAS ESCOLHAS, <br>AS NOSSAS CONQUISTAS</p>
		</article>

<script type="text/javascript">
$(function(){
	$('#form_telefone').mask('(99) 9999-9999');
	$("#contatoform").validationEngine('attach', {promptPosition : "topRight"});
});
</script>
<?php
$this->load->view('tpl/footer');
?>