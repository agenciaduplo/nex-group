<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>My Urban Life - Cadastre-se</title>

	<link href="<?=base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?=base_url(); ?>/favico.png" type="image/x-png" />
	
	<meta property='og:locale' content='pt_BR' />
	<meta property='og:title' content='My Urban Life.' />
	<meta property='og:image' content='http://www.myurbanlife.com.br/assets/img/logo.png'/>
	<meta property='og:description' content='Minhas escolhas, minhas conquistas. Antecipe-se ao pré-lançamento.'/>
	<meta property='og:url' content='http://www.myurbanlife.com.br/'/>
	<meta name="google-site-verification" content="PQsNcmABVJR9c3GV4RuQ98xzvYwJVLGh6FLuGNjNBbY" />

	
	
	<script type="text/javascript">var URL = {base: '<?=base_url(); ?>',site: '<?=site_url(); ?>',current: '<?=current_url(); ?>'};</script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/mask.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>assets/js/defualt.js"></script>

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-63']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	  
	  
	</script>

</head>
<body> 
	<a href="<?=site_url(); ?>video">
		<div class="videobox">
			<div class="video-img"></div>
			<div class="video"></div>
		</div>
	</a>
	<div class="black" id="alert" style="display: none">
		<div class="msg"><p>Mensagem Enviada com Sucesso!</p><a href="javascript:void(0);" title="fechar" id="btn-close"></a></div>
	</div>

	<div id="box">
		<div id="mylogo"><img src="<?=base_url(); ?>assets/img/logo.png" width="100%" height="380" /></div>
		<div id="myform">
			<form action="<?=current_url(); ?>" method="get">
				<ul>
					<li><label class="ie7">Nome:</label><input class="campo nome" name="form_nome" id="form_nome" type="text" value="" /></li>
					<li><label class="ie7">E-mail:</label><input class="campo email" name="form_email" id="form_email" type="text" value="" /></li>
					<li><label class="ie7">Telefone:</label><input class="campo telefone" name="form_telefone" id="form_telefone" type="text" value="" /></li>
					<li><label class="ie7">Comentário:</label><textarea name="form_comentario" id="form_comentario" cols="" rows=""></textarea></li>
					<input class="btn-enviar" name="enviar" type="button" value="" id="btn-submit" />
				</ul>
			</form>
			<img src="<?=base_url(); ?>assets/img/btn-enviar-bg.png" width="100%" height="380" />
		</div>
	</div>
	<div class="top-left"></div>
	<div class="top-right"></div>
	
	<div class="middle-left"></div>
<!--	<div class="middle-right"></div> -->
	
	<div class="bottom-left">
		<p><span>Rua</span> 1º de Março <span>esq. Rua</span> 1º de Setembro</p>
		<a class="mapa" href="https://maps.google.com/maps?f=d&source=s_d&saddr=-30.065603,-51.16406&daddr=&hl=pt-BR&geocode=&sll=-30.065584,-51.16406&sspn=0.002507,0.004812&vpsrc=0&mra=mift&mrsp=0&sz=18&ie=UTF8&t=m&z=18&iwloc=ddw0" title="Ver mapa no Google" target="_blank"></a>
		<div class="social">
			<div class="addthis_toolbox addthis_default_style ">
			<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
   			<a class="addthis_button_tweet"></a>
			</div>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f6a24a51a684879"></script>
		</div>
	</div>
	<div class="bottom-right"></div>
	
	<a class="DivexRodape" href="http://www.divex.com.br/imobi" target="_blank" title="Desenvolvido por Divex"><div id="divex"></div></a>
<script type="text/javascript">
	<?php
	if($this->uri->segment(1) == "sucesso"){
		?>
		$(function(){
			$('#alert').fadeIn(500);
			$('#alert p').text("Contato enviado com sucesso");
		});
</script>
<script type="text/javascript">
/* <![CDATA[ */
	var google_conversion_id = 999424896;
	var google_conversion_language = "en";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "I0w8CNDduAMQgIfI3AM";
	var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		<?php

	}

	?>
</script>
</body>
</html>