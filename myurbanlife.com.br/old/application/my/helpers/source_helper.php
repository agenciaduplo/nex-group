<?php
function source(){
	$CI = get_instance();
	$utm = array(
		'utm_source' => $CI->input->get('utm_source'),
		'utm_medium' => $CI->input->get('utm_medium'),
		'utm_content' => $CI->input->get('utm_content'),
		'utm_campaign' => $CI->input->get('utm_campaign')
	);
	if(!empty($utm['utm_source'])){
		$_SESSION = array(
			'url' => implode('__',$utm),
			'origem' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL
		);
	}
}
?>