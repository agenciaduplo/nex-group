var cache = false;
var request_url = URL.site + 'api/';


var api = {
	block: function(){
		cache = true;
	},
	release: function(){
		cache = false;
	},
	abort: function(){
		if(typeof(cache) == 'object'){
			cache.abort();
			cache = false;
		}
	},
	ajax: function(u,o){
		if(cache){
			api.abort();
		}
		if(o == 'undefined'){
			return cache = $.post(request_url+u);
		} else {
			return cache = $.post(request_url+u,o);
		}
	},
	getchecked: function(o){
		var o = $(o + ':checked').map(function(i,n) {
			return $(n).val();
		}).get();
		if(o.length == 0) { o = ''; }
		return o;
	},
	get: function(o){
		var o = $(o).val();
		if(o == 'undefined' || o == ''){
			return '';
		} else {
			return o;
		}
	}
}


$(function(){
	var mylogo = $('#mylogo'),
		myform =  $('#myform');
/*
	$('#box').hover(function(){
		mylogo.stop(false,true).animate({ width: 0 },200,function(){
			mylogo.hide();
			myform.show().css('width','0px');
			myform.stop(false,true).animate({ width: 400 },200);
		});
	},function(){
		myform.stop(false,true).animate({ width: 0 },200,function(){
			myform.hide();
			mylogo.show().css('width','0px');
			mylogo.show().stop(false,true).animate({ width: 400 },200);
		});
	});
*/
	$('#btn-submit').click(function(){
		var recurse = api.ajax('enviar',{
			form_nome: api.get('#form_nome'),
			form_email: api.get('#form_email'),
			form_telefone: api.get('#form_telefone'),
			form_comentario: api.get('#form_comentario')
		}).done(function(texto){
			if(texto == "true"){
				location=URL.site + 'sucesso';
			} else {
				$('#alert').fadeIn(500);
				$('#alert p').text(texto);
			}
		});
	});
	$('#btn-close').click(function(){
		$('#alert').fadeOut(300);
	});

	$("#form_telefone").mask('99 9999-9999');

});