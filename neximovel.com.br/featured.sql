/*
Navicat MySQL Data Transfer

Source Server         : [NEX] NEX Imóvel (HML)
Source Server Version : 50630
Source Host           : neximovel.mysql.dbaas.com.br:3306
Source Database       : neximovel

Target Server Type    : MYSQL
Target Server Version : 50630
File Encoding         : 65001

Date: 2016-11-23 09:39:30
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `featured`
-- ----------------------------
DROP TABLE IF EXISTS `featured`;
CREATE TABLE `featured` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `type` text,
  `link` text,
  `subtitle` text,
  `button` text,
  `enabled` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of featured
-- ----------------------------
INSERT INTO featured VALUES ('19', 'CASA 5 SUÍTES', null, 'http://neximovel.com.br/litoral/DA100191/casa-5-dormitorios-suites-condominio-enseada-xangri-la', 'CONDOMÍNIO ENSEADA, XANGRI-LÁ', 'CONHEÇA', '1');
INSERT INTO featured VALUES ('20', 'SINGOLO', null, 'http://www.nexgroup.com.br/singolo/?utm_campaign=singolo_duplo&utm_source=site&utm_medium=banner', 'Melhor localização do bairro tristeza', 'CONHEÇA', '0');
