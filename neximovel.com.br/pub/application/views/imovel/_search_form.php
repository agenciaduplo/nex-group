<?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal ajaxsubmit', 'enctype' => 'multipart/form-data')) ?>
<div class="spacer"></div>

	<div class="col-md-12">
		<h4 class="ossemibold upper">Tipo de im&oacute;vel</h4>
		<?php echo Form::select('types', $types, $search['types'], array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
	</div>
	<div class="col-md-12 location">
		<h4 class="ossemibold upper">Localiza&ccedil;&atilde;o</h4>
		<?php echo Form::input('location', $search['location'], array('id' => 'location', 'class' => 'text-input', 'placeholder' => 'Digite bairro ou cidade')); ?>
	</div>

<div class="spacer hide-tablet"></div>
<div class="spacer"></div>

	<div class="col-md-12 col-sm-12 beds icons">
		<?php echo Form::select('beds', $beds, $search['beds'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<div class="spacer hide show-mobile"></div>
	</div>
<div class="spacer"></div>
	<div class="col-md-12 col-sm-12 suite icons">
		<?php echo Form::select('suite', $suite, $search['suite'], array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<div class="spacer hide show-mobile"></div>
	</div>
<div class="spacer"></div>
	<div class="col-md-12 col-sm-12 parking icons">
		<?php echo Form::select('parking', $parking, $search['parking'], array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
	</div>

<div class="spacer"></div>

	<div class="col-md-12 col-sm-12 area icons clearfix">
		<?php //echo Form::select('area', $area, $search['area'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<?php echo Form::input('area', $search['area'], array('id' => 'area_input', 'type' => 'hidden')); ?>
		<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input left', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
			<span class="caret"></span> 
			<span class="sr-only">Toggle Dropdown</span> 
		</button>
		<ul class="dropdown-menu">
			<li>
				<p>
				  <label for="amount-area">Área:</label>
				  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
				</p>
				<div id="slider-area-range"></div>
			</li> 
		</ul>
	</div>
<div class="spacer"></div>
	<div class="col-md-12 col-sm-12 price icons clearfix">
		<?php //echo Form::select('price', $price, $search['price'], array('id' => 'price', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
		<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input left', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
			<span class="caret"></span> 
			<span class="sr-only">Toggle Dropdown</span> 
		</button>
		<ul class="dropdown-menu">
			<li>
				<p>
				  <label for="amount">Faixa de pre&ccedil;o:</label>
				  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
				</p>
				<div id="slider-range"></div>
			</li> 
		</ul>
	</div>
<div class="spacer"></div>
	<div class="col-md-12 col-sm-6 lupa icons">
		<?php //echo Form::select('code', $types, $search['beds'], array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input', 'placeholder' => 'Digite o código do imóvel')); ?>
		<div class="spacer"></div>
	</div>

	<div class="col-md-12 col-sm-6">
		<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
	</div>
<div class="spacer"></div>
<?php echo Form::input('load', 'ajax', array('type' => 'hidden')); ?>
<?php echo Form::close() ?>