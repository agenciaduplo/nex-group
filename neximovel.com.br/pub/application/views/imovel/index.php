<section class="breadcrumb-sec">
<div class="container">
  <ul class="breadcrumb no-padding-mobile">
    <li>
		<a class="ajaxload" href="<?php echo URL::base(TRUE) ?>">In&iacute;cio</a>
    </li>
    <li>
		<a class="ajaxload" href="<?php echo !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'javascript:history.go(-1);'; ?>">Im&oacute;veis</a>
    </li>
	<li>
      Im&oacute;vel c&oacute;d. <?php echo $property->sale.$property->id; ?>
    </li>
  </ul>
</div>
</section>

<section class="gallery lazyloader">
<div class="container">
	<div class="row">
		<div class="col-md-9 no-padding-left">
			<div class="fs25 hide-mobile hide-tablet left"><?php echo $property->title; ?></div>
			<div class="fs35 hide show-mobile show-tablet p15-mobile left"><?php echo $property->title; ?></div>
			<div class="right hide-mobile">
				<span class="osnormal fs16 blackcolor">R$ </span>
				<span class="osbold fs25 redcolor"><?php echo number_format($property->value, 2, ',', '.'); ?></span>
			</div>
		</div>
		<div class="col-md-3 "></div>
	</div>
</div>
</section>
<section class="gallery">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 slidercol no-padding-left">
				<!-- Place somewhere in the <body> of your page -->
				<div id="slider" class="flexslider">
				  <ul class="slides">
					<?php foreach($property->medias->order_by('order')->find_all() as $k => $m){ ?>
						<li><img src="<?php echo $m->path; ?>" /></li>
					<?php } ?>
					<!-- items mirrored twice, total of 12 -->
				  </ul>
				</div>
				<div id="carousel" class="flexslider hide-mobile">
				  <ul class="slides">
					<?php foreach($property->medias->order_by('order')->find_all() as $k => $m){ ?>
						<?php if(!empty($m->thumb_slider)){ ?>
						<li>
							<img src="<?php echo $m->thumb_slider; ?>?ver=0.1" />
						</li>
						<?php }  ?>
					<?php } ?>
					<!-- items mirrored twice, total of 12 -->
				  </ul>
				</div>
			</div>
			<div class="row hide show-mobile">
				<div class="col-md-12 hide show-mobile show-tablet left">
					<span class="osnormal fs40 blackcolor">R$ </span>
					<span class="osbold fs45 redcolor"><?php echo number_format($property->value, 2, ',', '.'); ?></span>
					<div class="spacer"></div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-12 col-xs-12 searchbox hide-mobile hide-tablet no-padding page-imovel">
				<!--<iframe border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=63&amp;midia=http://compromissonex.nexgroup.com.br/saiba-mais/" style="display: block;width:100%;border:0;height:100%;"></iframe>-->
				<?php 
					echo View::factory('imovel/_search_form')
								->set('types', $types)
								->set('search', $search)
								->set('beds', $beds)
								->set('suite', $suite)
								->set('price', null)
								->set('parking', $parking);
				?>
				
			</div>
		</div>
	</div>
</section>

<section class="details">
	<div class="container">	
	<div class="row">
		<div class="col-md-6 col-sm-12 graybg">
			<div class="spacer"></div>
			<div class="row"><div class="col-md-12"><h4 class="ossemibold upper">Caracter&iacute;sticas do im&oacute;vel</h4></div></div>
			
			<div class="col-md-5 im-home"><?php echo $property->type->name; ?></div>
			<div class="col-md-7 im-pin"><?php echo $property->neighborhood; ?> - <?php echo $property->city->name; ?></div>
			<div class="col-md-5 im-area"><?php echo $property->area; ?> m&sup2;</div>
			<?php if($property->suite > 0){ ?>
			<div class="col-md-7 im-suite"><?php echo $property->suite > 1 ? $property->suite.' suites' :  $property->suite.' suite'; ?></div>
			<?php } ?>
			<?php if($property->beds > 0){ ?>
			<div class="col-md-5 im-beds"><?php echo $property->beds > 1 ? $property->beds.' dormit&oacute;rios' :  $property->beds.' dormit&oacute;rio'; ?></div>
			<?php } ?>
			<div class="col-md-7 im-parking"><?php echo $property->parking > 1 ? $property->parking.' vagas' :  $property->parking.' vaga'; ?></div>
			
			<div class="spacer"></div>
			<div class="row">
				<div class="col-md-12"><h4 class="ossemibold upper">Descri&ccedil;&atilde;o do im&oacute;vel</h4></div>
				<div class="col-md-12 fs16"><?php echo $property->description; ?></div>
			</div>
			<div class="spacer"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 mmb10">		
				<?php if(Auth::instance()->logged_in()) { ?>
					<?php $test = Auth::instance()->get_user()->has('properties', $property); ?>
					<?php if($test){ ?>
					<button class="btn im-button twolines remove-from-list osbold fs12 left" pid="<?php echo $property->id; ?>">
						<i class="glyphicon glyphicon-trash"></i> 
						<div class="text right">REMOVER DA <br class="hide-mobile" />MINHA LISTA</div>
						<div class="corner"></div>
					</button>
					<?php } else { ?>
					<button class="btn im-button twolines add-to-list osbold" pid="<?php echo $property->id; ?>">
						<i class="glyphicon glyphicon-star whitecolor"></i> 
						<div class="text right">ADICIONAR &Agrave; <br class="hide-mobile" />MINHA LISTA</div>
						<div class="corner"></div>
					</button>
					<?php } ?>
					<?php } else { ?>
					<button data-toggle="modal" data-id="login" data-target="#myModal" class="btn im-button twolines  osbold" pid="<?php echo $property->id; ?>">
						<i class="glyphicon glyphicon-star whitecolor"></i> 
						<div class="text right">ADICIONAR &Agrave; <br class="" />MINHA LISTA</div>
						<div class="corner"></div>
					</button>
					<?php } ?>
				
					<a href="javascript:window.open('http://www.facebook.com/share.php?u=<?php echo URL::site(Request::current()->uri(), TRUE); ?>&title=<?php echo urlencode($property->title); ?>', '', 'width=600, height=450');" target="_blank">
						<button class="btn im-button osbold">
							<i class="glyphicon glyphicon-share whitecolor"></i> COMPARTILHAR
							<div class="corner"></div>
						</button>
					</a>	
				
					<a href="javascript:window.print();" target="_blank">
						<button class="btn im-button osbold">
							<i class="glyphicon glyphicon-print whitecolor"></i> IMPRIMIR
							<div class="corner"></div>
						</button>
					</a>
				</div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer hide show-mobile"></div>
		<div class="col-md-6 col-sm-12  col-xs-12 no-padding-right no-padding-mobile no-padding-tablet">
			<div class="col-md-12 graybg">
				<div class="spacer"></div>
				<div class="row"><div class="col-md-12"><h4 class="ossemibold upper">Mapa de localiza&ccedil;&atilde;o</h4></div></div>
				<div class="row">
					<div class="col-md-12">
						<div id="map-canvas" data-lat="<?php echo $property->lat; ?>" data-long="<?php echo $property->lng; ?>" ></div>
					</div>
				</div>
				<div class="spacer"></div>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

</section>



<?php if(isset($banner) && !empty($banner)) { ?>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
		<div class="spacer"></div>
		<div class="tcenter">
			<a href="<?php echo $banner->link; ?>" target="_blank">
				<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
			</a>
		</div>
		<div class="spacer"></div>
	</div>
</section>
<?php } ?>
<div class="spacer"></div>
<section class="related">
	<div class="container">
		<div class="row">
			<h3 class="title ossemibold">Outros im&oacute;veis sugeridos</h2>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<?php 
				foreach($related as $k => $p){
					echo View::factory('home/widget_item')->set('k', $k)->set('p', $p);
				} 
			?>
		</div>
		<div class="spacer"></div>
		<div class="spacer hide-mobile"></div>
		<div class="spacer hide-mobile"></div>
	</div>
</section>

<section class="graybg">
<div class="container">
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 searchbox searchboxmobile hide show-tablet show-mobile">
		<?php 
			echo View::factory('imovel/_search_form')
						->set('types', $types)
						->set('search', $search)
						->set('beds', $beds)
						->set('suite', $suite)
						->set('price', null)
						->set('parking', $parking);
		?>
		
	</div>
	</div>
	</div>
</section>
<div class="spacer"></div>
<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>