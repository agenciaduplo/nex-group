<div class="spacer hide-mobile"></div>
<?php echo Form::open('forgotpassword', array('id' => 'forgotpasswordform', 'class' => 'form-horizontal')) ?>
	<div class="row">
		<div class="col-md-12">
			<p class="oslight fs12 tcenter"><?php echo Form::input('email', null, array('id' => 'email', 'class' => 'text-input email', 'placeholder' => 'Email')) ?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6"><?php echo Form::button('signup', 'ENVIAR NOVA SENHA', array('id' => 'forgotPass', 'type' => 'submit', 'class' => 'btn btn-login redbg right whitecolor')); ?></div>
		<div class="col-md-3"></div>
	</div>
<?php echo Form::close() ?>
<div class="spacer hide-mobile"></div>