<div class="col-md-4 col-sm-4 hide show-mobile show-tablet grid-item mb20 <?php echo $p->id; ?> <?php echo $p->highlight ? 'highlight' : ''; ?> <?php echo @$hide ? 'hide' : ''; ?>">
	<a href="<?php echo URL::base(TRUE) ?>imovel/<?php echo $p->sale.$p->id.'/'.Controller_Application::friendly_seo_string($p->title); ?>" class="ajaxload">
		<?php if(!empty($p->medias->order_by('order')->find()->thumb_400)){ ?>
		<div class="box" style="background:url(<?php echo $p->medias->order_by('order')->find()->thumb_400; ?>);">
		<?php } ?>
			<?php if($p->highlight){ ?>
			<div class="im-highlight" ></div>
			<?php } ?>
				
				<?php if(!empty($p->medias->order_by('order')->find()->thumb_400)){ ?>
				<img style="opacity:0;" class="w100" src="<?php echo $p->medias->order_by('order')->find()->thumb_400; ?>">
				<?php } ?>
		
			<div class="owrap">
				<div class="pricetag left whitecolor fs25 osbold"><span class="cipher osnormal">R$ </span><?php echo number_format($p->value, 2, ',', '.'); ?></div>
				<div class="overlay">
					<div class="im-buttons tright hide-mobile hide-tablet">
						<i class="glyphicon glyphicon-plus-sign lightgreencolor"></i> 
						<?php $test = Auth::instance()->logged_in() ? Auth::instance()->get_user()->has('properties', $p) : false; ?>
						<?php if($test){ ?>
						<i class="glyphicon glyphicon-trash lightgreencolor remove-from-list" pid="<?php echo $p->id; ?>"></i>
						<?php } else { ?>
							<?php if(Auth::instance()->logged_in()){ ?>
							<i class="glyphicon glyphicon-star lightgreencolor add-to-list" pid="<?php echo $p->id; ?>"></i>
								<?php } else { ?>
								<a href="#" title="Adicionar à lista" data-toggle="modal" data-id="login" data-target="#myModal" class="noty" data-noty-options='{"text":"Registre-se para adicionar este imóvel a sua lista.","layout":"top","type":"information"}'>
									<i class="glyphicon glyphicon-star lightgreencolor" pid="<?php echo $p->id; ?>"></i>
								</a>
							<?php }  ?>
						<?php }  ?>
					</div>
					<div class="im-line-1 oslight whitecolor">
						<!--<i class="glyphicon glyphicon-home lightgreencolor"></i>-->
						<?php echo $p->type->name; ?> | <?php echo $p->area; ?> m&sup2;
					</div>
					<div class="im-line-2 oslight whitecolor">
						<!--<i class="glyphicon glyphicon-map-marker lightgreencolor"></i>-->
						<?php echo $p->address; ?> | <?php echo $p->neighborhood; ?> | <?php echo $p->city->name; ?>
					</div>
					<?php if($p->beds > 0){ ?>
					<div class="im-line-3 oslight whitecolor">
						<!--<i class="glyphicon glyphicon-map-marker lightgreencolor"></i>-->
						<?php echo $p->beds > 1 ? $p->beds.' dormit&oacute;rios' :  $p->beds.' dormit&oacute;rio'; ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</a>
</div>