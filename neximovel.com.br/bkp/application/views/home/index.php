<section class="conceitual">
	<div class="spacer"></div>
	<div class="container searchform">
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
			<div class="col-md-10 no-padding-mobile">
				<div class="row">
					<div class="col-md-6">
						<h4 class="whitecolor ossemibold">TIPO DE IM&Oacute;VEL</h4>
						<?php echo Form::select('types', $types, null, array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
					</div>
					<div class="col-md-6">
						<h4 class="whitecolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', null, array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false', 'autocomplete' => 'off')); ?>
						<ul class="dropdown-menu location-dropdown">
							<li>
								<p class="search-tip">
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
								<nav class="live-search" style="display:none">
										<?php foreach($live as $v){ ?>
										<p class="no-margin"><?php echo $v; ?></p>
										<?php } ?>
								</nav>
								
							</li> 
						</ul>
					</div>
				
					<div class="spacer"></div>
					<div class="advanced">
						
							<div class="col-md-4 col-sm-6 beds icons mb10"><?php echo Form::select('beds', $beds, null, array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
							<div class="col-md-4 col-sm-6 suite icons mb10"><?php echo Form::select('suite', $suite, null, array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
							<div class="col-md-4 col-sm-6 parking icons mb10"><?php echo Form::select('parking', $parking, null, array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						
						
							
							<div class="col-md-4 col-sm-6 col-xs-12 area icons btn-group mb10">
								<!-- AREA -->
								<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
								
									<?php echo Form::input('area', null, array('id' => 'area_input', 'type' => 'hidden')); ?>
									<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
										<span class="caret"></span> 
										<span class="sr-only">Toggle Dropdown</span> 
									</button>
									<ul class="dropdown-menu">
										<li>
											<p>
											  <label for="amount-area">Área:</label>
											  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
											</p>
											<div id="slider-area-range"></div>
										</li> 
									</ul>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 price icons btn-group mb10">
								<!-- PRICE -->
									<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
									<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
										<span class="caret"></span> 
										<span class="sr-only">Toggle Dropdown</span> 
									</button>
									<ul class="dropdown-menu">
										<li>
											<p>
											  <label for="amount">Faixa de pre&ccedil;o:</label>
											  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
											</p>
											<div id="slider-range"></div>
										</li> 
									</ul>
							</div>
							<div class="col-md-4 col-sm-6 mb10"><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
						
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="row">
					<h4 class="whitecolor ossemibold">&nbsp;</h4>
					<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
				</div>
				<div class="spacer"></div>
				<div class="row"><?php echo Form::button('save', '+ BUSCA AVAN&Ccedil;ADA', array('type' => 'button', 'class' => 'btn btn-advanced')); ?></div>
			</div>
		<?php echo Form::close() ?>
	</div>
</section>
<div class="spacer"></div>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
	<div class="row">
		<div class="spacer"></div>
			<div class="tcenter">
			<?php if($banner->loaded()){?>
				<a href="<?php echo $banner->link; ?>" target="_blank">
					<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
				</a>
			<?php } ?>	
			</div>
		<div class="spacer"></div>
	</div>
	</div>
</section>

<section class="im-items">
	<div class="container">
		<div class="row">
			<h3 class="title ossemibold">Im&oacute;veis em destaque</h2>
		</div>
		<div class="spacer"></div>
		<div class="row widgets">
			<?php 
				foreach($properties as $k => $p){ 
					echo View::factory('home/widget_item')->set('k', $k)->set('p', $p);
				} 
			?>
		</div>
		<div class="spacer"></div>
		<div class="spacer hide-mobile"></div>
		<div class="row">
			<div class="col-md-12 tcenter">
				<img class="loader hide" src="<?php echo URL::base(TRUE) ?>public/img/ajax-loader-7.gif" title="Loader">
				<div class="view-more">VISUALIZAR MAIS IM&Oacute;VEIS</div>
			</div>
		</div>
		<div class="spacer hide-mobile"></div>
	</div>
</section>
