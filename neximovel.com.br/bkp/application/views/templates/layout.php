<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title><?php echo $ogtitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Nex imóvel, imóveis porto alegre, casas, terrenos, sala comercial, casas em condomínio, nex group, Imóveis a venda, apartamento alto padrão, lançamentos nex, pronto para morar, Terrenos, porto alegre, grande porto alegre, região metropolitana.">

	<!-- OPEN GRAPH META -->
	<meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='<?php echo $ogtitle; ?>'/>
	<meta property='og:image' content='<?php echo $ogimage; ?>'/>
	<meta property='og:description' content='<?php echo $ogdescription; ?>'/>
    <meta property='og:url' content='<?php echo URL::site(Request::current()->uri(), TRUE); ?>'/>
	
    <!-- The styles -->
    <link id="bs-css" href="<?php echo URL::base(TRUE) ?>assets/css/bootstrap-united.min.css" rel="stylesheet">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="<?php echo URL::base(TRUE) ?>assets/css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/animate.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>public/css/flexslider.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>public/css/style.css?ver=0.3' rel='stylesheet'>

	<script> var base_url = '<?php echo URL::base(TRUE) ?>'; </script>
    <!-- jQuery -->
    <script src="<?php echo URL::base(TRUE) ?>assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo URL::base(TRUE) ?>assets/http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo URL::base(TRUE) ?>public/img/favicon.ico">

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69113303-6', 'auto');
	  ga('send', 'pageview');

	</script>
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '918457114901692');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=918457114901692&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->
		<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},s=d.getElementsByTagName('script')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.getsmartlook.com/bundle.js';s.parentNode.insertBefore(c,s);
    })(document);
    smartlook('init', 'f60da429fd79cd29fdd641dbebe54130a6345b07');
	</script>
</head>

	<body class="<?php echo $pageClass; ?>">

	<script>
	  // This is called with the results from from FB.getLoginStatus().
	  function statusChangeCallback(response) {
		console.log('statusChangeCallback');
		console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
		  // Logged into your app and Facebook.
		  testAPI();
		} else if (response.status === 'not_authorized') {
		  // The person is logged into Facebook, but not your app.
		  //document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		} else {
		  // The person is not logged into Facebook, so we're not sure if
		  // they are logged into this app or not.
		  //document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		}
	  }

	  // This function is called when someone finishes with the Login
	  // Button.  See the onlogin handler attached to it in the sample
	  // code below.
	  function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	  }

	  window.fbAsyncInit = function() {
	  FB.init({
		appId      : '568263573338119',
		cookie     : true,  // enable cookies to allow the server to access 
							// the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.5' // use graph api version 2.5
	  });

	  // Now that we've initialized the JavaScript SDK, we call 
	  // FB.getLoginStatus().  This function gets the state of the
	  // person visiting this page and can return one of three states to
	  // the callback you provide.  They can be:
	  //
	  // 1. Logged into your app ('connected')
	  // 2. Logged into Facebook, but not your app ('not_authorized')
	  // 3. Not logged into Facebook and can't tell if they are logged into
	  //    your app or not.
	  //
	  // These three cases are handled in the callback function.

	  FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	  });

	  };

	  // Load the SDK asynchronously
	  (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));

	  // Here we run a very simple test of the Graph API after login is
	  // successful.  See statusChangeCallback() for when this call is made.
	  function testAPI() {
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
		  console.log('Successful login for: ' + response.name);
		  //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
		});
	  }
	</script>
	
		<header>
			<div class="container-fluid hide show-tablet show-mobile">
				<div class="row">
					<div class="navbar navbar-default redbg no-border" role="navigation">
						<div class="navbar-inner">
							<button type="button" class="navbar-toggle pull-left animated flip dropdown-toggle" data-toggle="dropdown">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
								<style>
									.menu-mobile { list-style: none;border-radius: 0; text-align:center}

								</style>
								<ul class="menu-mobile nav top-menu dropdown-menu redbg" style="margin-right:10px">
									<?php if(Auth::instance()->logged_in()) { ?>
									<li><a class="whitecolor" href="<?php echo URL::base(TRUE) ?>minha-lista">Minha Lista</a></li>
									<li>
										<a class="whitecolor" href="<?php echo URL::base(TRUE) ?>logout">
										Sair <i class="glyphicon glyphicon-log-out"></i>
										</a>
									</li>
									<?php } else { ?>
									
									<li><a class="whitecolor" href="#" data-toggle="modal" data-id="login" data-target="#myModal">Login</a></li>
									<li><a class="whitecolor" href="#" data-toggle="modal" data-id="signup" data-target="#otherModal">Cadastre-se</a></li>
									<li><a class="whitecolor" href="#" data-toggle="modal" data-id="login" data-target="#myModal">Minha Lista</a></li>
									<li><a class="facebook-login" href="#"><img style="width:154px;" src="<?php echo URL::base(TRUE) ?>public/img/btn-fb-connect.png"></a></li>
									
									<?php } ?>
									
								</ul>
							
						</div>
					</div>
				</div>
			</div>
				<!-- END MENU MOBILE -->
			<div class="container">
				<div class="spacer"></div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<a href="<?php echo URL::base(TRUE) ?>">
							<img src="<?php echo URL::base(TRUE) ?>public/img/logo.png" class="imgmax100">
						</a>
					</div>
					<div class="col-lg-1 col-md-1 hide-tablet col-xs-8">
						<div class="hide show-mobile hide-tablet">
						<ul class="navbar-collapse nav navbar-nav top-menu no-margin no-padding right">
							<li class="col-xs-6 no-margin no-padding-right">
								<a href="tel:+555130922124" target="_blank" class="no-margin no-padding">
									<img class="h32" src="<?php echo URL::base(TRUE) ?>public/img/icon-phone-header.png">
								</a>
							</li>
							<li class="col-xs-6 no-margin no-padding-right">
								<a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=26&amp;midia=direto_2" target="_blank" class="no-margin no-padding">
									<img class="h32" src="<?php echo URL::base(TRUE) ?>public/img/icon-chat-header.png">
								</a>
							</li>
						</ul>
						</div>
					</div>
					<div class="col-lg-7 col-md-6 col-sm-8">
						<div class=" hide-mobile hide-tablet">
						<ul class="collapse navbar-collapse nav navbar-nav top-menu">
							<li><a href="<?php echo URL::base(TRUE) ?>">Home</a></li>
							
							<?php if(Auth::instance()->logged_in()) { ?>
							<li><a href="<?php echo URL::base(TRUE) ?>minha-lista">Minha Lista</a></li>
							<li><a class="btn-logout"href="<?php echo URL::base(TRUE) ?>logout">Sair <i class="glyphicon glyphicon-log-out"></i></a></li>
							<?php } else { ?>
							<li><a href="#" data-toggle="modal" data-id="login" data-target="#myModal" class="hide-tablet">Minha Lista</a></li>
							<li><a href="#" data-toggle="modal" data-id="login" data-target="#myModal">Login</a></li>
							<li><a href="#" data-toggle="modal" data-id="signup" data-target="#otherModal">Cadastre-se</a></li>
							<li><a href="#" class="facebook-login"><img src="<?php echo URL::base(TRUE) ?>public/img/btn-fb-connect.png"></a></li>
							
							<?php } ?>
							
						</ul>
						</div>
					</div>
					
					<!-- DESKTOP -->
					<div class="col-lg-2 col-md-4 col-sm-8 hide-tablet hide-mobile">
						<div class="row">
							<span class="osnormal right hide-tablet">NEX VENDAS</span>
						</div>
						<div class="row phone-number osbold right">
							<div class="col-lg-12 col-md-6 no-padding">
								<span class="ossemibold">(51)</span> 3092.3124
							</div>
						</div>
					</div>
					
					<!-- TABLET -->
					<div class="col-lg-2 col-md-4 col-sm-8 hide show-tablet">
						<div class="row phone-number osbold right">
							<div class="col-md-6 col-sm-6">
								<span class="osbold right blackcolor" style="margin-top: 10px;">NEX VENDAS</span><br class="" /><span class="ossemibold">(51)</span> 3092.3124
							</div>
							<div class="col-md-6 col-sm-6">
								<a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=26&amp;midia=direto_2" target="_blank" class="">
									<img style="margin-top: 10px;" src="<?php echo URL::base(TRUE) ?>public/img/icon-chat-header.png">
								</a>
							</div>	
						</div>
					</div>
				</div>
			</div>
			<div class="spacer"></div>
		</header>
		<div class="layout-content">
			<?php echo $content; ?>
		</div>
		<div class="spacer"></div>
		<footer>
			<section class="newsletter">
				<div class="container">
					<h3 class="title tcenter whitecolor">Receba nossa newsletter</h2>
					<div class="spacer"></div>
					<div class="spacer"></div>
					<div class="row">
						<form id="newsletter">
							<div class="col-md-1"></div>
							<div class="col-md-4"><?php echo Form::input('name', null, array('id' => 'news-name', 'class' => 'text-input', 'placeholder' => 'Nome')); ?></div>
							<div class="col-md-4"><?php echo Form::input('email', null, array('id' => 'news-email', 'class' => 'text-input email', 'placeholder' => 'E-mail')); ?></div>
							<div class="col-md-2">
								<?php echo Form::button('save', 'ENVIAR', array('type' => 'submit', 'class' => 'btn btn-news')); ?>
								<?php echo Form::input('origin', @$origin, array('type' => 'hidden')); ?>
							</div>
							<div class="col-md-1"></div>
						</form>
					</div>
				</div>
				<div class="spacer"></div>
			</section>
			<section class="contact">
				<div class="row">
						<div class="col-lg-1 hide-tablet hide-mobile"></div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 right-border fitems1">
							<a href="tel:+555130922124">
								<img src="<?php echo URL::base(TRUE) ?>public/img/icon-phone.png?ver=1" class="imgmax100">
							</a>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 right-border">
							<a href="mailto:vendas@nexvendas.com.br">
								<img src="<?php echo URL::base(TRUE) ?>public/img/icon-mail.png?ver=1" class="imgmax100">
							</a>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 right-border">
							<a href="#" data-toggle="modal" data-id="wecallyou" data-target="#weCallYou" onClick="ga('send', 'event','Home', 'Ligamos para você');">
								<img  src="<?php echo URL::base(TRUE) ?>public/img/icon-call.png?ver=1" class="imgmax100">
							</a>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 right-border fitems4 toggle-chat" onClick="ga('send', 'event','Chat', 'Rodapé');">
							<img src="<?php echo URL::base(TRUE) ?>public/img/icon-chat.png?ver=1" class="imgmax100 pointer">
						</div>
						<div class="col-lg-2 hide-tablet hide-mobile">
							<a href="http://www.nexgroup.com.br/conexao/" target="_blank">
								<img src="<?php echo URL::base(TRUE) ?>public/img/icon-conexao.png?ver=1" class="imgmax100">
							</a>	
						</div>
						<div class="col-md-1 hide-tablet hide-mobile"></div>
					</div>
			</section>
			<section class="socials">
				<div class="container">
					<div class="spacer"></div>
					<div class="spacer"></div>
					<div class="row">
						<div class="col-md-2 col-sm-9 col-xs-9"><img src="<?php echo URL::base(TRUE) ?>public/img/logo-footer.png?ver=1" class="imgmax100"></div>
						<div class="col-md-8 col-sm-9 col-xs-9 tcenter">
							<br class="hide-tablet hide-mobile" />
							<a href="<?php echo URL::base(TRUE) ?>termos-de-uso" class="whitecolor fs16">
								Termos de uso
							</a>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-3">
							<a class="right" href="https://www.facebook.com/NexGroup" target="_blank" onClick="ga('send', 'event','Facebook', 'Rodapé');">
								<img src="<?php echo URL::base(TRUE) ?>public/img/icon-fb.png?ver=1" class="imgmax100">
							</a>
						</div>
						<!--<div class="col-md-1 col-sm-3 col-xs-3"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-yt.png?ver=1" class="imgmax100"></div>-->
					</div>
					<div class="spacer"></div>
					<div class="spacer"></div>
				</div>
			</section>
			<section class="rights">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-12 col-xs-12"></div>
						<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?php echo URL::base(TRUE) ?>public/img/rights1.png" class="left no-float-mobile"></div>
						<div class="col-md-1 col-sm-6 col-xs-12 tcenter">
							<a href="http://agenciaduplo.com.br?utm_source=neximovel&utm_medium=site&utm_campaign=neximovel_site" target="_blank" onClick="ga('send', 'event','Duplo', 'Rodapé');">
								<img src="<?php echo URL::base(TRUE) ?>public/img/logo-duplo1.png" class="right no-float-mobile">
							</a>
						</div>
					</div>
				</div>
			</section>
		</footer>
<section>
	
	<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6"><a href="#" class="facebook-login"><img src="<?php echo URL::base(TRUE) ?>public/img/btn-fb-connect.jpg" class="w100"></a></div>
						<div class="col-md-3"></div>
					</div>
					<div class="spacer hide-mobile"></div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6"><p class="ossemibold fs12 tcenter">Você pode usar sua conta do Facebook<br class="hide-mobile" /> para acessar com mais rapidez e praticidade.</p></div>
						<div class="col-md-3"></div>
					</div>
					<div class="alert alert-danger" style="display:none;">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Erro</strong>
					</div>
				</div>
				<div class="modal-body graybg">
					<div class="row login-signup">
						<div class="col-md-6 greenbt no-padding-right">
							<form id="login-form" action="<?php echo URL::base(TRUE) ?>login" method="POST" style="padding-right: 15px;border-right: 1px solid #ccc;">
								<h4 class="ossemibold upper tcenter">Já sou cadastrado</h3>
								<p class="oslight fs12 tcenter">Se você já é um usuário,<br class="hide-mobile" /> faça o login abaixo.</p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('login_email', null, array('id' => 'login_email', 'class' => 'text-input email', 'placeholder' => 'Email')); ?></p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('login_password', null, array('id' => 'login_password', 'type' => 'password', 'class' => 'text-input email', 'placeholder' => 'Senha')); ?></p>
								<div class="row">
									<div class="col-md-6"><div class="spacer hide-mobile"></div><a href="#" class="osnormal fs12 greencolor" data-toggle="modal" data-id="forgotpassword" data-target="#forgotModal" data-dismiss="modal">Esqueci minha senha</a></div>
									<div class="col-md-6"><?php echo Form::button('login', 'ENTRAR', array('id' => 'site-login', 'type' => 'button', 'class' => 'btn btn-login greenbg right whitecolor')); ?></div>
								</div>
							</form>
						</div>
						<div class="col-md-6 redbt">
							<h4 class="title ossemibold upper tcenter">Não sou cadastrado</h3>
							<p class="oslight fs12 tcenter">Preencha os campos abaixo<br class="hide-mobile" /> para iniciar o cadastro</p>
							<p class="oslight fs12 tcenter"><?php echo Form::input('signup_name', null, array('id' => 'signup_name', 'class' => 'text-input email', 'placeholder' => 'Nome')); ?></p>
							<p class="oslight fs12 tcenter"><?php echo Form::input('signup_email', null, array('id' => 'signup_email', 'class' => 'text-input email', 'placeholder' => 'Email')); ?></p>
							<div class="col-md-6 no-padding"></div>
							<div class="col-md-6 no-padding">
								<a href="#" data-toggle="modal" data-id="login" data-target="#otherModal" data-dismiss="modal">
									<?php echo Form::button('signup', 'CADASTRAR', array('type' => 'submit', 'class' => 'btn btn-signup redbg right whitecolor')); ?>
								</a>
							</div>
						</div>
						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade in" id="otherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3 class="title osbold upper tcenter">Cadastro</h3>
					<div class="spacer hide-mobile"></div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6"><p class="ossemibold fs12 tcenter">Preencha os campos abaixo para<br class="hide-mobile" /> iniciar o cadastro.</p></div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<div class="modal-body graybg">
					<div class="row signup  redbt">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<div class="spacer hide-mobile"></div>
							<form id="signup-form" action="<?php echo URL::base(TRUE) ?>signup" method="POST">
								<p class="oslight fs12 tcenter"><?php echo Form::input('name', null, array('id' => 'name', 'class' => 'text-input', 'placeholder' => 'Nome', 'required' => 'required')); ?></p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('email', null, array('id' => 'email', 'class' => 'text-input', 'placeholder' => 'Email', 'required' => 'required')); ?></p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('email_confirm', null, array('id' => 'email_confirm', 'class' => 'text-input', 'placeholder' => 'Confirmar email', 'required' => 'required', 'minlength' => 6)); ?></p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('password', null, array('id' => 'password', 'type' => 'password', 'class' => 'text-input', 'placeholder' => 'Senha', 'required' => 'required', 'minlength' => 6)); ?></p>
								<p class="oslight fs12 tcenter">
									<?php echo Form::input('password_confirm', null, array('id' => 'password_confirm', 'type' => 'password', 'class' => 'text-input', 'placeholder' => 'Confirmar senha', 'required' => 'required')); ?>
									<small class="help-block">A senha deve conter no mínimo 8 caracteres</small>
								</p>
								<div class="row">
									<div class="col-md-8">
										<p>
											<input type="checkbox" id="receive_news" name="receive_news" checked="checked" value="1"/>
											<label for="receive_news" class="ossemibold fs12">Desejo receber novidades</label>
										</p>
									</div>
									<div class="col-md-4"><?php echo Form::button('signup', 'CADASTRAR', array('id' => 'site-signup', 'type' => 'submit', 'class' => 'btn btn-login redbg right whitecolor')); ?></div>
								</div>
							</form>
							<div class="spacer hide-mobile"></div>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="modal-footer">
					<p class="tcenter">Ao me cadastrar confirmo que li e concordo  com os <a href="<?php echo URL::base(TRUE) ?>termos-de-uso" target="_blank"><strong>Termos de Uso</strong></a></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade in" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3 class="title osbold upper tcenter">Esqueci minha senha</h3>
					<div class="spacer hide-mobile"></div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6"><p class="ossemibold fs12 tcenter">Informe seu e-mail para receber uma nova senha.</p></div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<div class="modal-body graybg">
					<div class="row signup  redbt">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<?php echo View::factory('forgotpassword/index'); ?>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="modal-footer">
					<p class="tcenter"></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade in" id="weCallYou" tabindex="-1" role="dialog" aria-labelledby="weCallYou" aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3 class="title osbold upper tcenter">Ligamos para voc&ecirc;!</h3>
					<div class="spacer hide-mobile"></div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6"><p class="ossemibold fs12 tcenter">Informe seu telefone.</p></div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<div class="modal-body graybg">
					<div class="row signup  redbt">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<div class="spacer hide-mobile"></div>
							<form id="wecallyouform" action="<?php echo URL::base(TRUE) ?>wecallyou" method="POST">
								<p class="oslight fs12 tcenter"><?php echo Form::input('name', null, array('id' => 'name', 'class' => 'text-input', 'placeholder' => 'Nome')); ?></p>
								<p class="oslight fs12 tcenter"><?php echo Form::input('phone', null, array('id' => 'phone', 'class' => 'text-input phone', 'placeholder' => 'Telefone', 'required' => 'required')); ?></p>
								<div class="row">
									<div class="col-md-8">
										
									</div>
									<div class="col-md-4">
										<?php echo Form::input('origin', @$origin, array('type' => 'hidden')); ?>
										<?php echo Form::button('sendcall', 'Enviar', array('id' => 'sendcall', 'type' => 'submit', 'class' => 'btn btn-login redbg right whitecolor')); ?>
									</div>
								</div>
							</form>
							<div class="spacer hide-mobile"></div>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
				<div class="modal-footer">
					<p class="tcenter"></p>
				</div>
			</div>
		</div>
	</div>
	
</section>


<?php if(Request::current()->controller() != 'Properties'){ ?>
<div class="chatbox hide-tablet hide-mobile">
	<div class="toggle-chat redbg themecolor" onClick="ga('send', 'event','Chat', 'Aba');"><img width="29" class="left" src="http://www.nexgroup.com.br/assets/chacara/img/icon-online.png?ver=0.2"> <span>CHAT</span></div>
	<div class="iframe closed">
	 <iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=63&midia=<?php echo $ref; ?>" style="display: block;"></iframe>
	</div>
</div>
<?php } ?>
	<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!-- library for cookie management -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='<?php echo URL::base(TRUE) ?>assets/bower_components/moment/min/moment.min.js'></script>
	<script src='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='<?php echo URL::base(TRUE) ?>assets/js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.autogrow-textarea.js"></script>

	<!-- history.js for cross-browser state change on ajax -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.history.js"></script>
	<!-- input mask plugin -->
	<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.mask.min.js"></script>

	<script src="<?php echo URL::base(TRUE) ?>public/js/jquery.lazyload.min.js"></script>
	
	<script src="<?php echo URL::base(TRUE) ?>public/js/jquery.validate.min.js"></script>
	
	<script src="<?php echo URL::base(TRUE) ?>public/js/jquery.flexslider-min.js"></script>
	<!-- GOOGLE MAPS -->
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?v=3.exp&#038;#038;ver=3.7.1'></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
	
	<!-- application script for Charisma demo -->
	<script src="<?php echo URL::base(TRUE) ?>public/js/functions.js?ver=0.6"></script>

	<script>
		$('a.modalpics').colorbox({maxHeight: '600px',current : "Imagem {current} de {total}", returnFocus: false});
		<?php foreach(Notices::get() as $n) { ?>
		noty({text: "<?php echo $n['key']; ?>","layout":"top","type":"<?php echo $n['type']; ?>","closeButton":"true"});
		<?php } ?>
	</script>
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 972557761;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
</body>
</html>
