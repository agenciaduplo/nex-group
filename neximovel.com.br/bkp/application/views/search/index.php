<section class="breadcrumb-sec graybg">
<div class="container">
  <ul class="breadcrumb graybg no-padding-left no-margin">
    <li>
      <a href="<?php echo URL::base(TRUE) ?>">In&iacute;cio</a>
    </li>
	<li>
      Busca
    </li>
  </ul>
</div>
</section>
<section class="single-page-header graybg">
	<div class="container searchform">
		<!-------------------------------BUSCA--------------------------------->
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal hide-mobile', 'enctype' => 'multipart/form-data')) ?>
			<div class="col-md-10">
				
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">TIPO DE IM&Oacute;VEL</h4>
						<?php echo Form::select('types', $types, @$search['types'], array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
					</div>
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', @$search['location'], array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
						<ul class="dropdown-menu">
							<li>
								<p>
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
							</li> 
						</ul>
					</div>
				
				<div class="spacer"></div>
				<div class="advanced">
					
						<div class="col-md-4 col-sm-6 beds icons mb10"><?php echo Form::select('beds', $beds, @$search['beds'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 suite icons mb10"><?php echo Form::select('suite', $suite, @$search['suite'], array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 parking icons mb10"><?php echo Form::select('parking', $parking, @$search['parking'], array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
					
					
						
						<div class="col-md-4 col-sm-6 area icons btn-group mb10">
							<!-- AREA -->
							<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
							
								<?php echo Form::input('area', @$search['area'], array('id' => 'area_input', 'type' => 'hidden')); ?>
								<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount-area">Área:</label>
										  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-area-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 price icons btn-group mb10">
							<!-- PRICE -->
								<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
								<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount">Faixa de pre&ccedil;o:</label>
										  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 mb10"><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<h4 class="whitecolor ossemibold">&nbsp;</h4>
					<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
				</div>
			</div>
		<?php echo Form::close() ?>		
		
		
		<!-------------------------------BUSCA--------------------------------->
	
	
		<h3 class="redcolor osbold upper hide show-mobile">Busca</h3>
		<h4 class="ossemibold graycolor mb20 hide show-mobile">Revise e compare seus imóveis preferidos</h4>
		<div class="spacer"></div>
		<div class="controls semilightgreenbg left w100">
			<div class="spacer"></div>
			<div class="col-lg-4 hide-mobile hide-tablet">
				<ul class="inline-list no-margin">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Exibir como</span></li>
					<li id="radio">
						<input type="radio" id="radio1" name="radio" value="list" checked="checked"><label for="radio1" class="whitecolor"><i class="glyphicon glyphicon-list lightgreencolor"></i> Lista</label>
						<input type="radio" id="radio2" name="radio" value="grid"><label for="radio2" class="whitecolor"><i class="glyphicon glyphicon-th-large lightgreencolor"></i> Grade</label>
					</li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-3 col-xs-6 whitecolor tcenter hide-mobile">
				<strong class="osbold fs25"><?php echo $count; ?></strong>
				<span>imóveis encontrados</span>
			</div> 
			<div class="col-lg-4 col-md-4 col-xs-6 whitecolor tcenter hide show-tablet hide-mobile"></div>
			<div class="col-lg-4 col-md-5 col-xs-12">
				<ul class="inline-list no-margin right">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Ordernar por</span></li>
					<li class="ctrlpad">
						<?php echo Form::open('busca/index', array('id' => 'orderform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
							<?php echo Form::select('order', $order, @$post_order, array('id' => 'ordersearch', 'class' => 'sel-input')); ?>
						<?php echo Form::close() ?>	
					</li>
				</ul>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
</section>
<div class="spacer"></div>
<section class="lazyloader">
	<div class="container">
		<div class="row results">
		<?php 
			foreach($properties as $p){
				echo View::factory('search/list_item')->set('p', $p);
				echo View::factory('search/grid_item')->set('p', $p);
			} 
		?>
		</div>
		<div class="spacer">
		<div class="col-md-12 tcenter">
			<?php if ($count > 6){ ?>
				<img class="loader hide" src="<?php echo URL::base(TRUE) ?>public/img/ajax-loader-7.gif" title="Loader">
				<!--<div class="view-more-search hide">VISUALIZAR MAIS IM&Oacute;VEIS</div>-->
			<?php } ?>
			</div>
		</div>
	</div>
</section>
<section class="sugestions">
	<div class="container">
	
	<?php if(@$search['types'] == 5 || @$search['types'] == 6) { ?>
		
		<?php if(isset($samearea) && count($samearea) > 0) { ?>
		<div class="row">
			<h3 class="title ossemibold">Mesma area</h3>
		</div>
		<div class="row">
		<?php 
			foreach($samearea as $p){
				echo View::factory('home/widget_item')->set('p', $p);
			} 
		?>
		</div>
		<?php } ?>
	
	
	<?php } else { ?>
		
		<?php if(count($samelocation) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Mesma localização</h3>
			</div>
			<div class="row">
			<?php 
				foreach($samelocation as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
		
		<?php if(isset($samebeds) && count($samebeds) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Com a mesma infraestrutura</h3>
			</div>
			<div class="row">
			<?php 
				foreach($samebeds as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
		
		<?php if(isset($sameprice) && count($sameprice) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Mesma faixa preço</h3>
			</div>
			<div class="row">
			<?php 
				foreach($sameprice as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
	<?php } ?>
	
	<?php if(isset($others) && count($others) > 0 && ($search['types'] == 5 || $search['types'] == 6 || ( count($samelocation) == 0  && count($samebeds) == 0 && count($sameprice) == 0 ))) { ?>
		<div class="row">
			<h3 class="title ossemibold">Outras sugestões</h3>
		</div>
		<div class="row">
		<?php 
			foreach($others as $p){
				echo View::factory('home/widget_item')->set('p', $p);
			} 
		?>
		</div>
	<?php } ?>
	</div>
</section>

<?php if(isset($banner) && !empty($banner)) { ?>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
		<div class="spacer"></div>
		<div class="tcenter">
			<a href="<?php echo $banner->link; ?>" target="_blank">
				<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
			</a>
		</div>
		<div class="spacer"></div>
	</div>
</section>
<?php } ?>
<section class="single-page-header graybg hide show-mobile">
	<div class="container searchform">
		<!-------------------------------BUSCA--------------------------------->
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
			<div class="col-md-10">
				
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">TIPO DE IM&Oacute;VEL</h4>
						<?php echo Form::select('types', $types, @$search['types'], array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
					</div>
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', @$search['location'], array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false', 'required' => 'required')); ?>
						<ul class="dropdown-menu">
							<li>
								<p>
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
							</li> 
						</ul>
					</div>
				
				<div class="spacer"></div>
				<div class="advanced">
					
						<div class="col-md-4 col-sm-6 beds icons mb10"><?php echo Form::select('beds', $beds, @$search['beds'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 suite icons mb10"><?php echo Form::select('suite', $suite, @$search['suite'], array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 parking icons mb10"><?php echo Form::select('parking', $parking, @$search['parking'], array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
					
					
						
						<div class="col-md-4 col-sm-6 area icons btn-group mb10">
							<!-- AREA -->
							<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
							
								<?php echo Form::input('area', @$search['area'], array('id' => 'area', 'type' => 'hidden')); ?>
								<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount-area">Área:</label>
										  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-area-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 price icons btn-group mb10">
							<!-- PRICE -->
								<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
								<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount">Faixa de pre&ccedil;o:</label>
										  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 mb10"><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<h4 class="whitecolor ossemibold">&nbsp;</h4>
					<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
				</div>
			</div>
		<?php echo Form::close() ?>		
	</div>
</section>
<script>
	search_query = '<?php echo json_encode($search); ?>';
	search_order = '<?php echo json_encode($post_order); ?>';
</script>