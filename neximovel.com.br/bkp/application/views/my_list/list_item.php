<div class="col-md-12 list-item hide-mobile hide-tablet graybg no-padding-right mb20 <?php echo $p->id; ?>">
	<div class="spacer"></div>
	<div class="col-md-5">
		<div class="im-price ossemibold"><span class="cipher osnormal">R$ </span><?php echo number_format($p->value, 2, ',', '.'); ?></div>
		<div class="im-line-1 oslight"><?php echo $p->type->name; ?> | <?php echo $p->area; ?> m&sup2;</div>
		<div class="im-line-2 oslight"><?php echo $p->address; ?> | <?php echo $p->neighborhood; ?> | <?php echo $p->city->name; ?></div>
		<?php if($p->beds > 0){ ?>
		<div class="im-line-3 oslight"><?php echo $p->beds > 1 ? $p->beds.' dormit&oacute;rios' :  $p->beds.' dormit&oacute;rio'; ?></div>
		<?php } ?>
		<div class="spacer"></div>
		<div class="col-md-4 no-padding-left">
			<a href="<?php echo URL::base(TRUE) ?>imovel/<?php echo $p->sale.$p->id.'/'.Controller_Application::friendly_seo_string($p->title); ?>">
				<div class="im-button osbold fs12"><i class="glyphicon glyphicon-plus-sign"></i> INFORMAÇÕES<div class="corner"></div></div>
			</a>
		</div>
		<div class="col-md-4 no-padding-left">
			<div class="im-button remove-from-list osbold fs12 left mylist" pid="<?php echo $p->id; ?>"><i class="glyphicon glyphicon-trash"></i> <div class="text right">REMOVER DA <br />MINHA LISTA</div><div class="corner"></div></div>
		</div>
		<div class="col-md-4 no-padding-left">
			<div class="im-button osbold fs12"><i class="glyphicon glyphicon-share"></i> COMPARTILHAR<div class="corner"></div></div>
		</div>
	</div>
	<div class="col-md-7 overhide">
		<div class="im-gallery">	
			<ul class="slides inline-list">
				<?php foreach($p->medias->order_by('order')->find_all() as $k => $m){ ?>
				<li <?php echo $k > 2 ? 'style="display:none"' : 'style="margin-right:18px;"'; ?>>
					<?php if(!empty($m->thumb_200)){ ?>
						<a href="<?php echo $m->path; ?>" class="modalpics" rel="group<?php echo $p->id; ?>">
							<img class="lazy" width="200" height="200" data-original="<?php echo $m->thumb_200; ?>" />
						</a>
					<?php } ?>
				</li>
				<?php } ?>
				<!-- items mirrored twice, total of 12 -->
			</ul>
		</div>
	</div>
	<div class="spacer"></div>
</div>