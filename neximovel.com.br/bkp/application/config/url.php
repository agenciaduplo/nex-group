<?php
return array(
		'trusted_hosts' => array(
		'localhost',
		'192.168.0.54',
		'neximovel.com.br',
		'neximovel.tempsite.ws',
		'neximovel.hospedagemdesites.ws',
		'www.neximovel.com.br'
    ),
);
?>