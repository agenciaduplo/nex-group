<?php defined('SYSPATH') or die('No direct script access.');

// Static file serving (CSS, JS, images)
Route::set('public', 'public(/<folder>(/<file>))', array('file' => '.+'))
	->defaults(array(
		'directory' => 'public',
		'folder' => NULL,
		'file'       => NULL,
	));
	
	