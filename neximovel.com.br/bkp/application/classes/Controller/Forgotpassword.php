<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Forgotpassword extends Controller_Application {
  
  public function before(){
    parent::before();

    //if already logged in, redirect to dashboard
    if(Auth::instance()->logged_in(array('manager')) || Auth::instance()->logged_in(array('admin'))){
      $this->redirect('manager');
    }
  }
  
  public function action_index(){

	$this->auto_render = FALSE;
	$this->profiler    = NULL;  

    if($this->request->post('email') != ''){
        try {
                $user  = ORM::factory('User')->where('email', '=', $this->request->post('email'))->find();
                $pwd    = substr(md5(microtime()),0,8);
                $email = View::factory('templates/email');
                $email->name= $user->username;
                $email->password = $pwd;
                $email->url = URL::site(NULL, TRUE);

                if($user->name != ''){
                        Mailer::instance()
                        ->to($user->email)
                        ->from('suporte@neximovel.com.br')
                        ->subject('Nex Imóvel - Nova Senha')
                        ->html($email->render())
                        ->send();

					$user->update_user(array('password' => $pwd, 'password_confirm' => $pwd), array('password'));
					
					echo json_encode(array('status' => 'success', 'msg' => 'Nova senha enviada.'));

                }else{

					echo json_encode(array('status' => 'error', 'msg' => 'Email n&atilde;o cadastrado.'));
                }

        } catch(ORM_Validation_Exception $e) {
		  $errors = $e->errors('models');
		  echo json_encode(array('status' => 'error', 'msg' => 'Erro inesperado. Tente novamente.'));
        }
    } else {
		if($this->request->method() == Request::POST) {
			echo json_encode(array('status' => 'error', 'msg' => 'E-mail inv&aacute;lido.'));
		}
	}
  }
} // End Manager forgot password
