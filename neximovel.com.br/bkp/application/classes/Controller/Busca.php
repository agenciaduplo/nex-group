<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Busca extends Controller_Application {
	
	public function before(){
		parent::before();

		/*$session = Session::instance();
		
		if(empty($session->get('search_query'))){
		
			$search_query = array('types' => '1', 
								  'location' =>'',
								  'beds' => 0,
								  'suite' => 0,
								  'parking' => 0,
								  'area' => 0,
								  'price' => 0,
								  'code' => 0);
			
			$session->set('search_query', $search_query);
		}*/ 
		
		
		$this->template->ogtitle = 'NEX Imóvel';
		$this->template->ogdescription = 'NEX Imóvel';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Home';
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
			Cookie::set('ref', $ref);
			
		} else if(!empty(Cookie::get('ref'))) {
			
			$ref = Cookie::get('ref');
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'neximovel') === false) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			Cookie::set('ref', $ref);
			
		} else {
			$ref = 'nex_imovel';
		}
		
		$live = ORM::factory('Property')
					->select('name_index')
					->distinct(TRUE)
					->find_all()
					->as_array('name_index', 'name_index');
							
		View::bind_global('live', $live);
		
		$this->template->ref = $ref;
	}
	
	public function action_index()
	{
		
		$post = $this->request->post();
		
		if(isset($post['ajax_request'])){
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
		}
		
		$view = View::factory('search/index');
		
		$view->banner = ORM::factory('Place')->where('name', '=', 'Resultado de busca')->find()->banners->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->find();
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		$view->beds = array('Dormitórios', '1', '2', '3', '4+');
		$view->suite = array('Suítes', '1', '2', '3', '4+');
		$view->parking = array('Vagas', '1', '2', '3', '4+');
		
		if($this->request->method() == Request::POST && !isset($post['order'])){
			
			if(isset($post['ajax_request'])){
				Cookie::set('search_query', $post['search_query']);
			} else {
				Cookie::set('search_query', json_encode($post));
			}
		}

		if($this->request->method() == Request::POST && isset($post['order'])){
			if(isset($post['ajax_request'])){
				Cookie::set('search_order', $post['order']);
			} else {
				Cookie::set('search_order', $post['order']);
			}
		}
		$search_order = Cookie::get('search_order');
		
		$search_query = json_decode(Cookie::get('search_query'), true);
		
		$code = substr(@$post['code'], 2);
		
		$search = ORM::factory('Property', $code);
		
		
		
		if(!$search->loaded()){
		
			$against = isset($post['location']) ? $post['location'] : '';
		
			$against = addslashes($against);
		
			$select = "MATCH (name_index) AGAINST ('$against' IN BOOLEAN MODE) ";
			
			$post['types'] = isset($post['types']) ? $post['types'] : 1;
			
			$samelocation = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', '=', $post['types'])
								->and_where(DB::expr("MATCH (name_index)"), 'AGAINST', DB::expr("('$against' IN BOOLEAN MODE)"))
								->and_where('sold', '=', 0)
								->order_by('value', 'ASC')
								->limit(3);
		
			if(isset($post['location']) && !empty($post['location'])){
				$search->where(DB::expr("MATCH (name_index)"), 'AGAINST', DB::expr("('$against' IN BOOLEAN MODE)"));
			}
		
			if(isset($post['types']) && !empty($post['types'])){
				//$select .= "+ (CASE WHEN type_id = ".$search_query['types']." THEN 0.05 ELSE 0 END)";
				$search->where('type_id', '=', $post['types']);
			}
			
			if(isset($post['beds']) && !empty($post['beds'])){
				//$select .= "+ (CASE WHEN beds = ".$search_query['beds']." THEN 0.05 ELSE 0 END)";
				
				if($post['beds'] == 4){
					$search->where('beds', '>', $post['beds']);
				} else {
					$search->where('beds', '=', $post['beds']);
				}
				
				$samebeds = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', '=', $post['types'])
								->and_where('beds', '=', $post['beds'])
								->and_where('sold', '=', 0)
								->order_by('value', 'ASC')
								->limit(3);
				
			}
			
			if(isset($post['suite']) && !empty($post['suite'])){
				//$select .= "+ (CASE WHEN suite = ".$search_query['suite']." THEN 0.05 ELSE 0 END)";
				
				if($post['suite'] == 4){
					$search->where('suite', '>', $post['suite']);
				} else {
					$search->where('suite', '=', $post['suite']);
				}
				
			}
			
			if(isset($post['parking']) && !empty($post['parking'])){
				//$select .= "+ (CASE WHEN parking = ".$search_query['parking']." THEN 0.05 ELSE 0 END)";
				
				if($post['parking'] == 4){
					$search->where('parking', '>', $post['parking']);
				} else {
					$search->where('parking', '=', $post['parking']);
				}
			}
			
			if(isset($post['area']) && !empty($post['area'])){
				
				//print_r($search_query['area']); die;
				
				$area = explode(':', $post['area']);
				
				//$select .= "+ (CASE WHEN area BETWEEN ".$area[0]." AND ".$area[1]." THEN 0.05 ELSE 0 END)";
				if(!isset($area[0]) || !isset($area[1])) 
				{
					$area[0] = 10;
					$area[1] = 500;
				}

				$search->where('area', 'BETWEEN', DB::expr($area[0]." AND ".$area[1]));
				
				$area0 = $area[0] - 10;
				$area1 = $area[1] + 10;
				
				$samearea = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', '=', $post['types'])
								->and_where('area', 'BETWEEN', DB::expr($area0." AND ".$area1))
								->and_where('sold', '=', 0)
								->order_by('value', 'ASC')
								->limit(3);
			}
			
			
			$others = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', '=', $post['types'])
								->and_where('sold', '=', 0)
								->order_by('value', 'ASC')
								->limit(3);
			
			
			if(isset($post['price']) && !empty($post['price'])){
				
				$price = explode(':', $post['price']);
				
				//$select .= "+ (CASE WHEN value < ".$price[1]." THEN 0.05 ELSE 0 END)";
				
				//$select .= "+ (CASE WHEN value < ".$price[0]." THEN 0.03 ELSE 0 END)";
				
				$search->where('value', 'BETWEEN', DB::expr($price[0]." AND ".$price[1]));
				
				$sameprice = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', '=', $post['types'])
								->and_where('value', 'BETWEEN', DB::expr($price[0]." AND ".$price[1]))
								->and_where('sold', '=', 0)
								->order_by('value', 'ASC')
								->limit(3);
			}
			
			$select .= " AS relevance";
			$search->select(DB::expr("$select"))->and_where('enabled', '=', 1)->and_where('sold', '=', 0);
			
			$total = clone $search;
			$total = $total->count_all();
			
			if($search_order !== null && $search_order != 'relevance'){
				$search_order = str_replace('"', '', $search_order);
				if($search_order == NULL ){
					$search_order = 'ASC';
				}
				$search->order_by('value', DB::expr($search_order));
			} else {
				$search->order_by('relevance', 'DESC')
						->order_by('value', 'ASC');
			}
		
			$offset = isset($post['offset']) ? $post['offset'] : 0;
			
			$properties = $search
								 //->having('relevance', '>', 0)
								 //->limit(6)
								 //->offset($offset)
							     ->find_all();
			$count = count($properties);								
			//$properties = $search->order_by('relevance', 'DESC')->find_all();
			//print_r($properties);					 
	
		} else {

			$this->redirect('imovel/'.$post['code'].'/'.Controller_Application::friendly_seo_string($search->title));
			
		}
		
		if(isset($post['ajax_request'])){
			
			$result['content'] = '';
			if($count < 6){	
				$result['count'] = 'end';
				
			} else {
				$result['count'] = 'keep';
			}
			
			foreach($properties as $k => $p){ 
				
				if($post['list_type'] == 'list'){
					$hide_list = false;
					$hide_grid = true;
					
				} else if($post['list_type'] == 'grid') {
					$hide_list = true;
					$hide_grid = false;
				}
				
				$result['content'] .=  (string) View::factory('search/list_item')->set('p', $p)->set('hide',$hide_list);
				$result['content'] .=  (string) View::factory('search/grid_item')->set('p', $p)->set('hide',$hide_grid);
			
				//$result['content'] .=  (string) View::factory('home/widget_item')->set('k', $k)->set('p', $p);
			}
			echo json_encode($result);
			
		} else {
			
			$view->post_order = @$search_order;
			$view->order = array('relevance' => 'Relevancia', 'ASC' => 'Menor preço', 'DESC' => 'Maior preço');
			
			//$view->pagi = $pagi;
			$view->properties = $properties;
			$view->count = $total;
			$view->search = $post;
			
			$ids = !empty($properties->as_array(null,'id')) ? implode(',', $properties->as_array(null,'id')) : '(0)';
			
			
			
			$view->samelocation = isset($samelocation) ? $samelocation->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
			$view->samebeds = isset($samebeds) ? $samebeds->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
			$view->samearea = isset($samearea) ? $samearea->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
			$view->others = isset($others) ? $others->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
			$view->sameprice = isset($sameprice) ? $sameprice->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
			
			$this->template->origin = 'Busca';
			$this->template->pageClass = 'page-my-list';
			$this->template->content = $view;
			
		}
	}
}