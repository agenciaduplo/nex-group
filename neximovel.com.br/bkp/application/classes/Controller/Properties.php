<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Properties extends Controller_Application {

	public function before(){
		parent::before();

		if(empty(Cookie::get('search_query'))){
		
			$search_query = array('types' => 0, 
								  'location' =>'',
								  'beds' => 0,
								  'suite' => 0,
								  'parking' => 0,
								  'area' => 0,
								  'price' => 0,
								  'code' => 0);
			
			Cookie::set('search_query', json_encode($search_query));
		}
		
		if(empty(Cookie::get('search_order'))){
			
			Cookie::set('search_order', 'ASC');
		}
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
			Cookie::set('ref', $ref);
			
		} else if(!empty(Cookie::get('ref'))) {
			
			$ref = Cookie::get('ref');
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'neximovel') === false) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			Cookie::set('ref', $ref);
			
		} else {
			$ref = 'nex_imovel';
		}
		
		$this->template->ref = $ref;
		
		$this->template->ogtitle = 'NEX Imóvel';
		$this->template->ogdescription = 'A NEX tem o imóvel ideal para você. Apartamentos, coberturas, casas e diversos tipos de imóveis do jeito que você procura, do tamanho da sua família e dos seus sonhos. Clique e confira!';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Nex Imóvel - Apartamentos, Casas, Terrenos , Sala Comercial';
		
	}
	
	public function action_imovel()
	{
		$view = View::factory('imovel/index');
	
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		
		$id = substr($this->request->param('id'), 2);
		
		$view->property = ORM::factory('Property', $id);
		
		if(!$view->property->enabled || !$view->property->loaded()){
			Notices::add('error', 'Imóvel não encontrado');
			$this->redirect('/');
		}
		
		$view->property->viewed = $view->property->viewed + 1;
		$view->property->save();
		
		$view->related = $view->property->get_related();
		
		$this->template->pageClass = 'page-imovel';
		
		$this->template->ogtitle = $view->property->title.' | NEX Imóvel';
		$this->template->ogdescription = $view->property->description;
		$this->template->ogimage = $view->property->medias->find()->path;
	
		$this->template->origin = $view->property->id;
		
		$view->search = json_decode(Cookie::get('search_query'), true);
		$view->beds = array('Dormitórios', '1', '2', '3', '4+');
		$view->suite = array('Suítes', '1', '2', '3', '4+');
		$view->parking = array('Vagas', '1', '2', '3', '4+');
		$view->ref = $this->template->ref;
		$view->banner = $view->property->banners->where('enabled' ,'=', 1)->order_by(DB::expr('RAND()'))->find();
		
		$this->template->content = $view;
	}

	
} // End Properties
