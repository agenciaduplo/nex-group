<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Application extends Controller_Template {

	public $template = 'templates/layout';
	public $view = null;
	public $title = 'Home';

	public function before(){
		parent::before();
		
		if(isset($_GET['splash'])){
			Cookie::set('splash', $_GET['splash']);
		}
		
		$this->template->title = $this->title;
	}
	
	/**
	 * function, receives string, returns seo friendly version for that strings, 
	 *     sample: 'Hotels in Buenos Aires' => 'hotels-in-buenos-aires'
	 *    - converts all alpha chars to lowercase
	 *    - converts any char that is not digit, letter or - into - symbols into "-"
	 *    - not allow two "-" chars continued, converte them into only one syngle "-"
	 */
	static function friendly_seo_string($vp_string){
		
		$vp_string = trim($vp_string);
		
		$vp_string = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($vp_string));
		
		$vp_string = html_entity_decode($vp_string);
		
		$vp_string = strip_tags($vp_string);
		
		$vp_string = strtolower($vp_string);
		
		$vp_string = preg_replace('~[^ a-z0-9_.]~', ' ', $vp_string);
		
		$vp_string = preg_replace('~ ~', '-', $vp_string);
		
		$vp_string = preg_replace('~-+~', '-', $vp_string);
			
		return $vp_string;
    } # friendly_seo_string()

	public function action_thumbnail()
	{		
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		
		if(!empty($this->request->param('crop')))
		{
			$crop = explode('x',$this->request->param('crop'));
			$width = $crop[0];
			$height = $crop[1];	
			
		} else {
			$width = 200;
			$height = 200;
		}
		
		$media = ORM::factory('Media', $this->request->param('id'));
		
		$filename = $media->docroot;
		
		$etag_sum = md5(base64_encode(file_get_contents($filename))."$width,$height");
		
		// Render as image and cache for 1 hour
		$this->response->headers('Content-Type', 'image/jpeg')
				->headers('Cache-Control', 'max-age='.Date::HOUR.', public, must-revalidate')
				->headers('Expires', gmdate('D, d M Y H:i:s', time() + Date::HOUR).' GMT')
				->headers('Last-Modified', date('r', filemtime($filename)))
				->headers('ETag', $etag_sum);
		
		$image = Image::factory($filename);
		$save_2 = Image::factory($filename);
		$save_4 = Image::factory($filename);
		$save_s = Image::factory($filename);
		
		// The following determines which dimension we should resize 
		// to allow it to fit into the final cropped image.
		if ( ($image->width / $image->height) > ($width / $height) )
		{   
			// Calculate where we should crop off the x-axis
			$resized_w = ($height / $image->height) * $image->width;
			$offset_x = round(($resized_w - $width) / 2); 
			$offset_y = 0;
			$image->resize(NULL, $height);
			
			if(empty($media->thumb_200)){
				$thumb = $save_2->resize(null, 200)
						 ->crop(200, 200, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_200.jpg');
					 
				if($thumb){
					$media->thumb_200 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_200.jpg';
					$media->save();
				}
				
			}
			if(empty($media->thumb_400)){
				$thumb = $save_4->resize(null, 400)
					 ->crop(400, 400, $offset_x, $offset_y)
					 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_400.jpg');
					 
				if($thumb){
					$media->thumb_400 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_400.jpg';
					$media->save();
				}
			}
			if(empty($media->thumb_slider)){
				$thumb = $save_s->resize(null, 128)
					 ->crop(190, 128, $offset_x, $offset_y)
					 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_slider1.jpg');
					 
				if($thumb){
					$media->thumb_slider = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_slider1.jpg';
					$media->save();
				}
			}
		}   
		else
		{   
			// Calculate where we should crop off the y-axis
			$resized_h = ($width / $image->width) * $image->height;
			$offset_x = 0;
			$offset_y = round(($resized_h - $height) / 2); 
			$image->resize($width, NULL);
			
			if(empty($media->thumb_200)){
				$thumb = $save_2->resize(200, null)
					 ->crop(200, 200, $offset_x, $offset_y)
					 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_200.jpg');
					 
				if($thumb){
					$media->thumb_200 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_200.jpg';
					$media->save();
				}
				
			}
			if(empty($media->thumb_400)){
				$thumb = $save_4->resize(400, null)
					 ->crop(400, 400, $offset_x, $offset_y)
					 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_400.jpg');
					 
				if($thumb){
					$media->thumb_400 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_400.jpg';
					$media->save();
				}
			}
			if(empty($media->thumb_slider)){
				$thumb = $save_s->resize(190, null)
					 ->crop(190, 128, $offset_x, $offset_y)
					 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_slider1.jpg');
					 
				if($thumb){
					$media->thumb_slider = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_slider1.jpg';
					$media->save();
				}
			}
		}   

		// Now the image has been resized, we can safely crop it
		$image->crop($width, $height, $offset_x, $offset_y);
		
		
		$this->response->body($image->render('jpg'));
	}
	
	public function action_live_search()
	{		
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$live = ORM::factory('Property')
					->select('name_index')
					->distinct(TRUE)
					->find_all()
					->as_array('name_index', 'name_index');
			
		echo '<pre>';
		print_r(json_encode($live));
		
		
		
	}
}
