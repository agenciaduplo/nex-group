//chosen - improves select
$('[data-rel="chosen"],[rel="chosen"]').chosen({ width:"100%",disable_search_threshold: 10 });

$('.btn-advanced').click(function(e){
	$('.advanced').fadeToggle();
	ga('send', 'event','Abrir busca avançada', 'click');
});

/*$('.lazyloader').animate({
		opacity:1
	}, 5000);*/

/*setTimeout(function() {
	$('.lazyloader').css('opacity', '1');
}, 50);*/
	
	
$('#ordersearch').change(function(){
	$('#orderform').submit();
});

$('.toggle-chat').click(function(){

	$('.toggle-chat').toggleClass('opened');
	
	if($('.toggle-chat').hasClass('opened')){
		$('.chatbox .iframe').animate({ width:'340px' }, 500);
	} else {
	
		$('.chatbox .iframe').animate({ width:'0px' }, 500);
	}
});

$(".phone").mask('(00) 0000-00009');

$('.view-more').click(function(e){
	$(this).hide();
	$('.loader').fadeIn();

	$.ajax({
		type:"POST",
		data: { offset: $('.widgets').children().length },
		dataType: 'json',
		url:base_url+"welcome/load_more",
		success: function(data) {
			console.log(data.count);
			$('.widgets').append(data.content);
			$('.loader').hide();
			if(data.count == 'keep'){
				$('.view-more').fadeIn();
			}

			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 60000;
			now.setTime(expireTime);
			
			var limit = $('.widgets').children().length;
			document.cookie = 'limit=' + limit + ';expires='+now.toGMTString()+';path=/';
			
		},
		error: function(data) {
			console.log('Load more error:');
			console.log(data);
		}
	});
	ga('send', 'event','Home', 'Visualizar mais imóveis');
});

$('.view-more-search').click(function(e){
	$(this).hide();
	$('.loader').fadeIn();
	//console.log($('input[name="radio"]:checked').val());
	$.ajax({
		type:"POST",
		data: { offset: $('.results').children('.grid-item').length, ajax_request: 1, list_type:$('input[name="radio"]:checked').val(), search_query: search_query, order: search_order },
		dataType: 'json',
		url:base_url+"busca",
		success: function(data) {
			//console.log(data.count);
			$('.results').append(data.content);
			$('.loader').hide();
			
			if($('input[name="radio"]:checked').val() == 'grid') {
				$('.grid-item').stop().fadeIn();
			}
			$('.im-gallery').each(function(e){  
				$(this).flexslider({
					animation: "slide",
					controlNav: false,
					animationLoop: false,
					slideshow: false,
					itemWidth: 200,
					itemMargin: 18,
					maxItems: 3,
					start: function(slider) { // Fires when the slider loads the first slide
					  var slide_count = slider.count - 1;

					  $(slider)
						.find('img.lazy:eq(0), img.lazy:eq(1), img.lazy:eq(2)')
						.each(function() {
						  var src = $(this).attr('data-original');
						  $(this).attr('src', src).removeAttr('data-original');
						});
					},
					before: function(slider) { // Fires asynchronously with each slider animation
					  var slides     = slider.slides,
						  index      = slider.animatingTo,
						  $slide     = $(slides[index]),
						  $img       = $slide.find('img[data-original]'),
						  current    = index,
						  nxt_slide  = current + 1,
						  prev_slide = current - 1;

					  $slide
						.parent()
						//.find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')')
						.find('img.lazy:gt(2)')
						.each(function() {
						  var src = $(this).attr('data-original');
						  $(this).attr('src', src).removeAttr('data-original');
						});
					}
				  });
			  });
			if(data.count == 'keep'){
				$('.view-more.search').fadeIn();
			}
		},
		error: function(data) {
			console.log('Load more error:');
			console.log(data);
		}
	});

	ga('send', 'event','Busca', 'Visualizar mais imóveis');
});

$('#signup-form').validate({
	onfocusout: false,
	onkeyup: false,
	errorClass: 'error',
	rules: {
		name: {
			required: true,
			minlength: 2
		},
		email: {
			required: true,
			email: true
		},
		email_confirm: {
			required: true,
			email: true
		},
		password: {
			required: true,
			minlength: 8
		},
		password_confirm: {
			required: true,
			equalTo : "#password"
			
		}
	},
	messages: {
		name: {
			required: "Informe o seu nome",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail.",
			email: "Por favor utilizae um endereço de e-mail válido."
		},
		email_confirm: {
			required: "Confirme seu e-mail.",
			email: "Por favor utilize um endereço de e-mail válido."
		},
		password: {
			required: "Digite uma senha",
			minlength: "A senha deve conter no mínimo 8 caracteres"
		},
		password_confirm: {
			required: "Confirme sua senha",
			equalTo : "Senha não confirmada."
		}
	},
	errorPlacement: function(error, element) {
		var n = noty({text: error,"layout":"bottom","type":"error","closeButton":"true"});
	},
	submitHandler: function(form){
		ga('send', 'event','Cadastro', 'Site');
		form.submit();
	}
});

$('#newsletter').validate({
	errorClass: 'error',
	rules: {
		name: {
			minlength: 2
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		name: {
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail.",
			email: "Por favor utilizae um endereço de e-mail válido."
		}
	},
	errorPlacement: function(error, element) {
		var n = noty({text: error,"layout":"bottom","type":"error","closeButton":"true"});
	},
	submitHandler: function(form) {
		//event.preventDefault();
		//Envia o form via ajax
		$.ajax({
			type:"POST",
			data: $(form).serialize(),
			dataType: 'json',
			url:base_url+"newsletter",
			success: function(data) {
				//console.log(data);
				var n = noty({text: 'Você foi cadastrado com sucesso em nossa Newsletter',"layout":"center","type":"success","closeButton":"true"});
				$('#newsletter')[0].reset();
				ga('send', 'event','Newsletter', 'Cadastro com sucesso');
			},
			error: function(data) {
				//console.log(data);
				var n = noty({text: 'Não foi possível adicionar este e-mail a nossa Newsletter!',"layout":"center","type":"error","closeButton":"true"});
			}
		});
	}
});

$(document).on('click','.add-to-list', function(){
	var pid = $(this).attr('pid');
	var item = $(this);

	$.ajax({
		url: base_url+'add_to_list',
		type: 'POST',
		dataType: 'json',
		data: {
			pid: pid
		},
		success: function(data) {
			//console.log(data);
			if(data.status == 'success'){
				var n = noty({text: 'Este imóvel foi adicionado a sua lista!',"layout":"center","type":"success"});
				
				$.post( base_url+'welcome/get_button/remove_from_list', { pid: pid, grid: $(item).hasClass('glyphicon')},function( data ) {
				  $(item).replaceWith( data );
				});
				ga('send', 'event','Minha lista', 'Adicionar', window.btoa(pid));
				
			} else {
				var n = noty({text: 'Não foi possível adicionar este imóvel a sua lista!',"layout":"center","type":"error"});
			}
		},
		error: function(data) {
			//console.log(data);
			var n = noty({text: 'Não foi possível adicionar este imóvel a sua lista!',"layout":"center","type":"error"});
		}
	});
});

$(document).on('click','.remove-from-list', function(){
	var pid = $(this).attr('pid');
	var item = $(this);
	//console.log(pid);
	
	//console.log($(this).parents('.item-list').attr('class'));
	$.ajax({
		url: base_url+'remove_from_list',
		type: 'POST',
		dataType: 'json',
		data: {
			pid: pid
		},
		success: function(data) {
			//console.log(data);
			if(data.status == 'success'){
				
				if($(item).hasClass('mylist')){
				
					$('.'+ pid).fadeOut(400, function(){
						$(this).remove();
					});
					count = parseInt($('.count').text()) - 1;
					$('.count').text(count);
				}
				
				var n = noty({text: 'Este imóvel foi removido da sua lista!',"layout":"center","type":"success"});
				
				$.post( base_url+'welcome/get_button/add_to_list', { pid: pid, grid: $(item).hasClass('glyphicon')},function( data ) {
					$(item).replaceWith( data );
				});
				
			} else {
				var n = noty({text: 'Não foi possível remover este imóvel da sua lista!',"layout":"center","type":"error"});
			}
		},
		error: function(data) {
			//console.log(data);
			var n = noty({text: 'Não foi possível remover este imóvel da sua lista!',"layout":"center","type":"error"});
		}
	});
});


//LIST CHANGE
$('input[name="radio"]').change(function(){
	//console.log($(this).val());
	var type = $(this).val();
	
	if(type == 'list'){
		$('.grid-item').stop().fadeOut(400, function(){
			$('.list-item').stop().fadeIn();
		});
	} else if(type == 'grid'){
		$('.list-item').stop().fadeOut(400, function(){
			$('.grid-item').stop().fadeIn();
		});
		
	}
	
});

//$("img.lazy").lazyload({ effect : "fadeIn", threshold : 800, failure_limit : 100 });

$( "#radio" ).buttonset();

//SITE LOGIN
$('#signup_name').focusout(function(){
	$('#name').val($(this).val());
});

$('#signup_email').blur(function(){
	$('#email').val($(this).val());
});

//LOGIN
$('#site-login').click(function(e){
	e.preventDefault();
	
	$.ajax({
		url: base_url+'login',
		type: 'POST',
		dataType: 'json',
		data: $('#login-form').serialize(),
		success: function(data) {
			//console.log(data);
			if(data.response == 'success'){
				location.reload();
			} else {
				$('#myModal .alert-danger strong').text(data.msg);
				$('#myModal .alert-danger').fadeIn();
			}
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});

//FORGOT PASSWORD
$('#forgotPass').click(function(e){
	e.preventDefault();
	$('#forgotPass').prop('disabled', true);
	$.ajax({
		url: base_url+'forgotpassword',
		type: 'POST',
		dataType: 'json',
		data: $('#forgotpasswordform').serialize(),
		success: function(data) {
			$('#forgotPass').prop('disabled', false);
			//console.log(data);
			if(data.status == 'success'){
				$('#forgotModal').modal("hide");
				var n = noty({text: data.msg,"layout":"top","type":"success"});

			} else {
				var n = noty({text: data.msg,"layout":"top","type":"error"});
			}
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});

//LIGAMOS PRA VC
$('#wecallyouform').validate({
	onfocusout: false,
	onkeyup: false,
	errorClass: 'error',
	rules: {
		name: {
			required: true,
			minlength: 2
		},
		phone: {
			required: true
			
		}
	},
	messages: {
		name: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		phone: {
			required: "Por favor preencha seu telefone."
		}
	},
	errorPlacement: function(error, element) {
		var n = noty({text: error,"layout":"bottom","type":"error","closeButton":"true"});
	},
	submitHandler: function(form) {
		$.ajax({
		url: base_url+'welcome/call',
		type: 'POST',
		dataType: 'json',
		data: $(form).serialize(),
		success: function(data) {
			//console.log(data);
			if(data.status == 'success'){
				$('#weCallYou').modal("hide");
				var n = noty({text: data.msg,"layout":"top","type":"success"});
				ga('send', 'event','Ligamos para você', 'Envio');
				//console.log(data);

			} else {
				console.log(data);
				var n = noty({text: data.msg,"layout":"top","type":"error"});
			}
			
		},
		error: function(data) {
			console.log(data);
		}
	});
	}
});

//FACEBOOK LOGIN
$(document).on('click', ".facebook-login", function(e) {
	/* Act on the event */
	e.preventDefault();
	FB.login(function(response) {
		//console.log(response);
		FB.getLoginStatus(function(response) {
			//console.log(response);
			if (response.status === 'connected') {
				var signed_request =   FB.getAuthResponse()['signedRequest'];
				//console.log(signed_request);
				FB.api('/me?fields=id,email,name', function(response) {
					console.log(JSON.stringify(response));
					$.ajax({
						url: base_url+'fb_connect',
						type: 'POST',
						dataType: 'text',
						data: {
							name: response.name,
							email: response.email,
							login: response.id
						},
						success: function(data) {
							data = JSON.parse(data);
							if(data.signup == 'signed'){
								ga('send', 'event','Cadastro', 'Facebook');
							}
							location.reload();
							
						},
						error: function(data) {
							console.log('Facebook Connect Error');
							console.log(response.birthday);
							console.log(data);
						}
					});

				});
			}
		});
	}, {
		scope: "email,public_profile"
	})
});

//PRICE RANGE
$( "#slider-range" ).slider({
	range: true,
	step:20000,
	min: 0,
	max: 4000000,
	values: [ 200000, 500000 ],
	slide: function( event, ui ) {
        $( "#amount, #price" ).val( numeral(ui.values[ 0 ]).format('$0,0.00') + " - " + numeral(ui.values[ 1 ]).format('$0,0.00') );		
        $( "#price" ).text( numeral(ui.values[ 0 ]).format('$0,0.00') + " - " + numeral(ui.values[ 1 ]).format('$0,0.00') );
		
		$('#price_input').val( numeral(ui.values[ 0 ]).format('0') + ":" + numeral(ui.values[ 1 ]).format('0') );
    }
});

$( "#amount, #price" ).val(  numeral($( "#slider-range" ).slider( "values", 0 )).format('$0,0.00') + " - " + numeral($( "#slider-range" ).slider( "values", 1 )).format('$0,0.00') );

//AREA RANGE
$( "#slider-area-range" ).slider({
	range: true,
	step:10,
	min: 10,
	max: 500,
	values: [ 100, 400 ],
	slide: function( event, ui ) {
        $( "#area" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );		
        $( "#amount-area" ).val( ui.values[ 0 ] + ' m²' + " - " + ui.values[ 1 ] + ' m²' );		
        $( "#area_btn" ).text( ui.values[ 0 ] + ' m²' + " - " + ui.values[ 1 ] + ' m²' );
		//console.log($( "#area" ).text());
		$('#area_input').val( ui.values[ 0 ] +  ":" + ui.values[ 1 ]  );
    }
});
	
$( "#amount-area" ).val(  numeral($( "#slider-area-range" ).slider( "values", 0 )) + ' m²' + " - " + numeral($( "#slider-area-range" ).slider( "values", 1 )) + ' m²');
$( "#area" ).val(  numeral($( "#slider-area-range" ).slider( "values", 0 )) + " - " + numeral($( "#slider-area-range" ).slider( "values", 1 )) );
	

$(window).load(function() {
	
	/*$("#location").keyup(function(){
 
		$('.live-search').show();
		$('.search-tip').hide();
		
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val();
 
        // Loop through the comment list
        $(".live-search p").each(function(){
 
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
            }
        });
 
    });*/
	
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 152,
    itemMargin: 12,
    asNavFor: '#slider'
  });
 
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
  
  $('.slidercol').hover(function(){
		$('#carousel').stop().animate({
			opacity:1
		});
  }, function(){
		$('#carousel').stop().animate({
			opacity:0
		});
  });
  
	$('.im-gallery').each(function(e){  
		$(this).flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 200,
			itemMargin: 18,
			maxItems: 3,
			start: function(slider) { // Fires when the slider loads the first slide
			  var slide_count = slider.count - 1;

			  $(slider)
				.find('img.lazy:eq(0), img.lazy:eq(1), img.lazy:eq(2)')
				.each(function() {
				  var src = $(this).attr('data-original');
				  $(this).attr('src', src).removeAttr('data-original');
				});
			},
			before: function(slider) { // Fires asynchronously with each slider animation
			  var slides     = slider.slides,
				  index      = slider.animatingTo,
				  $slide     = $(slides[index]),
				  $img       = $slide.find('img[data-original]'),
				  current    = index,
				  nxt_slide  = current + 1,
				  prev_slide = current - 1;

			  $slide
				.parent()
				//.find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')')
				.find('img.lazy:gt(2)')
				.each(function() {
				  var src = $(this).attr('data-original');
				  $(this).attr('src', src).removeAttr('data-original');
				});
			}
		  });
	  });
});


//GOOGLE MAPS
$(document).ready(function(){
	

	google.maps.event.addDomListener(window, 'load', initialize());
	var map;
	var infowindow;
	
	function initialize() {
		if (document.getElementById('map-canvas') == null) return;
        
		var directionsDisplay = new google.maps.DirectionsRenderer;
		var directionsService = new google.maps.DirectionsService;
		
        $gmap = $('#map-canvas');
		
		var mapOptions = {
			zoom: 15,
			scrollwheel: false,
			//center: new google.maps.LatLng( $gmap.data( 'lat' ), $gmap.data( 'long' ) ),
			center: new google.maps.LatLng( '-30.0227431', '-51.1625598' ),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			panControl: false,
			zoomControl: true,
			scaleControl: false,

			streetViewControl: false,
			overviewMapControl: false,
			//disableDefaultUI: true
			
		}
		
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
		 directionsDisplay.setMap(map);
		
		var myLatLng = new google.maps.LatLng( $gmap.data( 'lat' ), $gmap.data( 'long' ) );
		var userLatLng;// = new google.maps.LatLng( '-30.0227431', '-51.1625598' );
		var userLat;
		var userLng;
		
		//PIN
		desc = '<strong></strong><br />';
		createMarker(desc, myLatLng);
		map.setZoom(15);
		map.setCenter(myLatLng);
		
		//ROUTE
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				
				var userLatLng = new google.maps.LatLng( position.coords.latitude, position.coords.longitude );
	
				calculateAndDisplayRoute(directionsService, directionsDisplay, userLatLng, myLatLng);
			});
			
		} 
		
	}
	
	//TRACE ROUTE
	function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
	  
	  directionsService.route({
		origin: start,
		destination: end,
		travelMode: google.maps.TravelMode.DRIVING
	  }, function(response, status) {
		if (status === google.maps.DirectionsStatus.OK) {
		  directionsDisplay.setDirections(response);
		} else {
		  window.alert('Directions request failed due to ' + status);
		}
	  });
	}
	
	//SET PIN
	function createMarker(desc, latlng){

		var marker = new google.maps.Marker({position: latlng, map: map/*,icon: '/wp-content/themes/basewd/images/pin.png'*/});

		google.maps.event.addListener(marker, "click", function(){

		  if(infowindow)
			 infowindow.close();

			infowindow = new google.maps.InfoWindow({content: desc});

			infowindow.open(map, marker);

		});

		return marker;
	}
	
});