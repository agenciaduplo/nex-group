<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Application {

	public function before(){
		parent::before();

		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
			Cookie::set('ref', $ref);
			
		} else if(!empty(Cookie::get('ref'))) {
			
			$ref = Cookie::get('ref');
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'neximovel') === false) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			Cookie::set('ref', $ref);
			
		} else {
			$ref = 'nex_imovel';
		}
		
		$live = ORM::factory('Property')
							->select('name_index')
							->distinct(TRUE)
							->find_all()
							->as_array('name_index', 'name_index');
							
		View::bind_global('live', $live);
		
		$this->template->ref = $ref;
		
		$this->template->ogtitle = 'NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = 'A NEX tem o imóvel ideal para você. Apartamentos, casas e diversos tipos de imóveis do jeito que você procura, do tamanho da sua família e dos seus sonhos.';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Nex Imóvel - Apartamentos, Casas, Terrenos , Sala Comercial';
		
	}
	
	public function action_termos_de_uso()
	{
		$view = View::factory('home/termos_de_uso');
		
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		$view->beds = array('Dormitórios', '1', '2', '3', '4+');
		$view->suite = array('Suítes', '1', '2', '3', '4+');
		$view->parking = array('Vagas', '1', '2', '3', '4+');
		
		$this->template->origin = 'Termos de Uso';
		$this->template->pageClass = 'page-termos';
		$this->template->ogtitle = 'Termos de Uso | NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}

	public function action_criteo()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$properties = ORM::factory('Property')->where('compromisso','=',1)->find_all();
		
		foreach($properties as $k => $v){
			echo Controller_Application::friendly_seo_string($v->title) . ';';
			echo $v->title . ';';
			echo $v->type->name . ';';
			echo $v->medias->order_by('order')->find()->path . ';';
			echo URL::base(TRUE).'imovel/'.$v->sale.$v->id.'/'.Controller_Application::friendly_seo_string($v->title) . ';';
			echo $v->neighborhood . ';';
			echo $v->beds > 1 ? $v->beds . ' dorms.;' : $v->beds . ' dorm.;';
			echo $v->parking > 1 ? $v->parking . ' vagas;' : $v->parking . ' vaga;';
			echo "\n";
		}
		
		
	}
	
	public function action_index()
	{	

		$view = View::factory('home/index');
		
		$limit = !empty($_COOKIE['limit']) ? $_COOKIE['limit'] : 6;
		
		$view->properties = ORM::factory('Property')->where('enabled', '=', 1)->and_where('neximovel', '=', 1)->order_by('highlight', 'DESC')->order_by('value', 'ASC')->limit($limit)->find_all();
	
		$view->banner = ORM::factory('Place')->where('name', '=', 'Home')->find()->banners->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->find();
		
	
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		$view->beds = array('1', '2', '3', '4+');
		$view->suite = array('1', '2', '3', '4+');
		$view->parking = array('1', '2', '3', '4+');
		
		
		
		//$view->locations = ORM::factory('Type')->find_all()->as_array('id', 'name');
		header("Cache-Control: no-store, must-revalidate, max-age=0");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
		$this->template->origin = 'Home';
		$this->template->pageClass = 'page-home';
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
	
	public function action_load_more()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
			
		$post = $this->request->post();
		$result['content'] = '';
		$properties = ORM::factory('Property')->where('enabled', '=', 1)->and_where('neximovel', '=', 1)->order_by('highlight', 'DESC')->order_by('value', 'ASC')->limit(6)->offset($post['offset'])->find_all();
		
		$count = count($properties);
		
		if($count < 6){	
			$result['count'] = 'end';
			
		} else {
			$result['count'] = 'keep';
		}
		
		foreach($properties as $k => $p){ 
			$result['content'] .=  (string) View::factory('home/widget_item')->set('k', $k)->set('p', $p);
		}
		echo json_encode($result);
		
	}
	
	public function action_fb_connect()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST){
			
			$user = ORM::factory('User')->where('fb', '=', $this->request->post('login'))->find();
			
			if($user->loaded()){
				
				Auth::instance()->login($params['email'], $params['login']);
				
				echo json_encode(array('signup' => 'logged'));
				
			} else {
				
				$user = ORM::factory('User');
				
				$params['password'] = $params['login'];
				$params['password_confirm'] = $params['login'];
				$params['fb'] = $params['login'];
				$params['username'] = $params['email'];
				
				$user = ORM::factory('User');
				$user->create_user($params);
				
				$user->add('roles', array(1, 4));
				
				$body = View::factory('templates/email_cadastro')->bind('user', 'facebook');
				
				Mailer::instance()
					->to($params['email'])
					->from(array('neximovel@neximovel.com.br' => 'NEX Imóvel'))
					->subject('[NEX Imóvel] - Cadastro')
					->html($body)
					->send();
				
				Auth::instance()->login($params['username'], $params['login']);
				
				echo json_encode(array('signup' => 'signed'));
			}
			
			Notices::add('success', 'Você está logado.');
		}
	}
	
	public function action_signup()
	{
		//$this->auto_render = FALSE;
		//$this->profiler    = NULL;
		
		$view = View::factory('home/index');
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST){
			
			$user = ORM::factory('User')->where('username', '=', $this->request->post('email'))->find();
			
			if($user->loaded()){
				
				Notices::add('error', 'Usuário já cadastrado');
				//Auth::instance()->login($params['email'], $params['password']);
				$this->redirect('/');
				
				
			} else {
				
				$user = ORM::factory('User');
				
				$params['username'] = $params['email'];
				
				$user = ORM::factory('User');
				$user->create_user($params);
				
				$user->add('roles', array(1, 5));
				
				$body = View::factory('templates/email_cadastro')->bind('user', $params['username'])->bind('pass', $params['password']);
				
				Mailer::instance()
						->to($params['email'])
						->from(array('neximovel@neximovel.com.br' => 'NEX Imóvel'))
						->subject('[NEX Imóvel] - Cadastro')
						->html($body)
						->send();
					
				Auth::instance()->login($params['username'], $params['password']);
				
				Notices::add('success', 'Cadastro efetuado com sucesso.');
				$this->redirect('/');
				
			}
			
		}
	}
	
	public function action_new_session()
	{

		$this->auto_render = FALSE;
		$this->profiler    = NULL;
	
		//if already logged in, redirect to dashboard
		if(Auth::instance()->logged_in()){
			json_encode(array('response' => 'error',  'msg' => 'Usuário autenticado'));
			die;
		}

		if($this->request->method() == Request::POST){
			if(Auth::instance()->login($this->request->post('login_email'), $this->request->post('login_password'))){
	
				Notices::add('success', 'Você está logado.');
				echo json_encode(array('response' => 'success' , 'msg' => ''));
				die;
				
			} else {
				
				echo json_encode(array('response' => 'error',  'msg' => 'Usuário e/ou senha inválidos'));
				die;

			}
		}
	}
	
	public function action_my_list()
	{
		$view = View::factory('my_list/index');
		
		$user = Auth::instance()->get_user();

		$count = $user->properties->where('enabled', '=', 1)->count_all();
		$view->banner = ORM::factory('Place')->where('name', '=', 'Minha lista')->find()->banners->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->find();
		$pagi = Pagination::factory(array(
			'items_per_page'    =>  10,
			'total_items'       =>  $count,
		));
		
		$properties = $user->properties
							->where('enabled', '=', 1)
							->limit($pagi->items_per_page)
							->offset($pagi->offset)
							->find_all();

		$view->related = ORM::factory('Property')->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->limit(3)->find_all(); 
		$view->order = array('price' => 'Preço', 'date' => 'Data');
		
		$view->pagi = $pagi;
		$view->properties = $properties;
		$view->count = $count;
		$this->template->origin = 'Minha lista';
		$this->template->pageClass = 'page-my-list';
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
	
	public function action_add_to_list()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
		
			$property = ORM::factory('Property', $this->request->post('pid'));
			Auth::instance()->get_user()->add('properties', $property);
			
			echo json_encode(array('status' => 'success', 'msg' => ''));
			
		} catch (Exception $e){
			echo json_encode(array('status' => 'error', 'msg' => $e));
		}

	}
	
	public function action_remove_from_list()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$property = ORM::factory('Property', $this->request->post('pid'));
			Auth::instance()->get_user()->remove('properties', $property);
			
			echo json_encode(array('status' => 'success', 'msg' => ''));
			
		} catch (Exception $e){
			
			echo json_encode(array('status' => 'error', 'msg' => $e));
		}
	}
	
	public function action_newsletter()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$news = ORM::factory('Newsletter')->where('email', '=', $this->request->post('email'))->find();
			
			$news->email = $this->request->post('email');
			$news->name = $this->request->post('name');
			$news->origin = $this->request->post('origin');
			
			$news->save();
			
			echo json_encode(array('status' => 'success', 'msg' => ''));
			
		} catch (Exception $e){
			
			echo json_encode(array('status' => 'error', 'msg' => $e));
		}
	}
	
	public function action_call()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$news = ORM::factory('Call')->where('phone', '=', $this->request->post('phone'))->find();
			
			$news->phone = $this->request->post('phone');
			$news->name = $this->request->post('name');
			$news->origin = $this->request->post('origin');
			$params = $this->request->post('params');
			
			$news->save();
			
			//$body = "Nome: ".$news->name."<br> Telefone: ".$news->phone."<br>  Origem: ".$news->origin;
			
			/*$mail = Mailer::instance()
					->to('vendas@nexvendas.com.br')
					->from(array('neximovel@neximovel.com.br' => 'NEX Imóvel'))
					->subject('[NEX Imóvel] - Solicitação de Contato')
					->html($body)
					->send();*/
			
			// KONECTY
			$token = 'Ct34qzuuRqYMqmC7YRCMPMDEtxZmbUCaOR6VeQblPf4=';
		
			$ch = curl_init();
					
			$phone = str_replace(array('(',')', ' ', '-'), '', $news->phone);

			$dataObj = new stdClass;	
			$dataObj->data[0] = (object) array('name' => 'contact', 
											   'data' => (object) array(
													'name' => $news->name, 
													//'email' => strtolower($data['email']), 
													'phone' => (array) array($phone)));

				
			 
			//$dataObj->data[0]->data->queue = (object) array('_id' => "Dsxr53HL3dNYPA7Xf");
			
			$dataObj->data[1] = (object) array('name' => 'message',
											   'map' => (object) array('contact' => 'contact'),		
											   'data' => (object) array(
													'status' => "Nova", 
													'type' => "Formulário Web", 
													'subject' => 'Ligamos para você', 
													'body' => 'Quero que liguem para mim: '. $news->phone));

			if(!empty($_SERVER['HTTP_REFERER'])){
				$dataObj->data[1]->data->referrerURL = $_SERVER['HTTP_REFERER'];
			}
			if(isset($params['channel']) && !empty($params['channel'])){
				$dataObj->data[0]->data->channel = (object) array('identifier' => $params['channel']);
				$dataObj->data[1]->data->channel = (object) array('identifier' => $params['channel']);
			} else {
				$dataObj->data[0]->data->channel = (object) array('identifier' => "neximovel");
				$dataObj->data[1]->data->channel = (object) array('identifier' => "neximovel");
			}
			
			if(isset($params['utm_campaign']) && !empty($params['utm_campaign'])){
				$dataObj->data[0]->data->campaign = (object) array('identifier' => $params['utm_campaign']); 
				$dataObj->data[1]->data->campaign = (object) array('identifier' => $params['utm_campaign']); 
			} else {
				$dataObj->data[0]->data->campaign = (object) array('identifier' => 'organico'); 
				$dataObj->data[1]->data->campaign = (object) array('identifier' => 'organico');
			}

			if(isset($params['utm_source']) && !empty($params['utm_source'])){
				$dataObj->data[0]->data->source = (object) array('identifier' => $params['utm_source']); 
				$dataObj->data[1]->data->source = (object) array('identifier' => $params['utm_source']); 
			}

			if(isset($params['utm_medium']) && !empty($params['utm_medium'])){
				$dataObj->data[0]->data->medium = $params['utm_medium']; 
				$dataObj->data[1]->data->medium = $params['utm_medium']; 
			}
			
			$data = json_encode($dataObj);

			curl_setopt($ch, CURLOPT_URL, "https://nexgroup.konecty.com/rest/process/submit");

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_COOKIE, "authTokenId=$token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, true);

			$res = curl_exec($ch);

			//print_r($params);

			//ALVO DE CAMPANHA						
			$ch = curl_init();
			
			$info = json_decode($res);
			$contact_id = $info->processData->contact->_id; 
			$campaign_id = $info->processData->message->campaign->_id; 
			
			
			$dataTarget->data[0] = (object) array(
									   'name' => 'campaignTarget',	
									   'document' => 'CampaignTarget',
									   'data' => (object) array(
											'status' => 'Novo',
											'contact' => (object) array('_id' => $contact_id),
											'campaign' => (object) array('_id' => $campaign_id),
											'channel' => (object) array('_id' => 'wHE8iR73GenFErkQc')));
			
			$data = json_encode($dataTarget);
			
			curl_setopt($ch, CURLOPT_URL, "https://nexgroup.konecty.com/rest/process/submit");

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_COOKIE, "authTokenId=$token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, true);

			$res2 = curl_exec($ch);
				
			//ALVO DE CAMPANHA - END
					
			curl_close($ch);
			
			//KONECTY - END
			
			//echo json_encode(array('status' => 'success', 'msg' => 'Seu contato foi enviado. Em breve ligaremos para você.', 'mailer' => $mail));
			echo json_encode(array('status' => 'success', 'msg' => 'Seu contato foi enviado. Em breve ligaremos para você.'));
			
		} catch (Exception $e){
			
			echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
		}
	}
	
	public function action_interesse()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$post = $this->request->post();
			
			$contact = ORM::factory('Contact');
			$contact->user_data = json_encode($post);
			$contact->save();
			
			// KONECTY
			$token = 'Ct34qzuuRqYMqmC7YRCMPMDEtxZmbUCaOR6VeQblPf4=';
		
			$ch = curl_init();
					
			$phone = str_replace(array('(',')', ' ', '-'), '', $post['phone']);

			$dataObj = new stdClass;	
			$dataObj->data[0] = (object) array('name' => 'contact', 
											   'data' => (object) array(
													'name' => $post['name'], 
													//'email' => strtolower($data['email']), 
													'phone' => (array) array($phone)));

				
			 
			//$dataObj->data[0]->data->queue = (object) array('_id' => "Dsxr53HL3dNYPA7Xf");
			
			$dataObj->data[1] = (object) array('name' => 'message',
											   'map' => (object) array('contact' => 'contact'),		
											   'data' => (object) array(
													'status' => "Nova", 
													'type' => "Formulário Web", 
													'subject' => 'Tenho interesse', 
													'body' => $post['message']));

			if(!empty($_SERVER['HTTP_REFERER'])){
				$dataObj->data[1]->data->referrerURL = $_SERVER['HTTP_REFERER'];
			}
			if(isset($post['channel']) && !empty($post['channel'])){
				$dataObj->data[0]->data->channel = (object) array('identifier' => $post['channel']);
				$dataObj->data[1]->data->channel = (object) array('identifier' => $post['channel']);
			} else {
				$dataObj->data[0]->data->channel = (object) array('identifier' => "neximovel");
				$dataObj->data[1]->data->channel = (object) array('identifier' => "neximovel");
			}
			
			if(isset($post['utm_campaign']) && !empty($post['utm_campaign'])){
				$dataObj->data[0]->data->campaign = (object) array('identifier' => $post['utm_campaign']); 
				$dataObj->data[1]->data->campaign = (object) array('identifier' => $post['utm_campaign']); 
			} else {
				$dataObj->data[0]->data->campaign = (object) array('identifier' => 'organico'); 
				$dataObj->data[1]->data->campaign = (object) array('identifier' => 'organico');
			}

			if(isset($post['utm_source']) && !empty($post['utm_source'])){
				$dataObj->data[0]->data->source = (object) array('identifier' => $post['utm_source']); 
				$dataObj->data[1]->data->source = (object) array('identifier' => $post['utm_source']); 
			}

			if(isset($post['utm_medium']) && !empty($post['utm_medium'])){
				$dataObj->data[0]->data->medium = $post['utm_medium']; 
				$dataObj->data[1]->data->medium = $post['utm_medium']; 
			}
			
			$data = json_encode($dataObj);
			$log = $data;
			curl_setopt($ch, CURLOPT_URL, "https://nexgroup.konecty.com/rest/process/submit");

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_COOKIE, "authTokenId=$token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, true);

			$res = curl_exec($ch);

			//print_r($params);

			//ALVO DE CAMPANHA						
			$ch = curl_init();
			
			$info = json_decode($res);
			$contact_id = $info->processData->contact->_id; 
			$campaign_id = $info->processData->message->campaign->_id; 
			
			
			$dataTarget->data[0] = (object) array(
									   'name' => 'campaignTarget',	
									   'document' => 'CampaignTarget',
									   'data' => (object) array(
											'status' => 'Novo',
											'contact' => (object) array('_id' => $contact_id),
											'campaign' => (object) array('_id' => $campaign_id),
											'channel' => (object) array('_id' => 'wHE8iR73GenFErkQc')));
			
			$data = json_encode($dataTarget);
			
			curl_setopt($ch, CURLOPT_URL, "https://nexgroup.konecty.com/rest/process/submit");

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_COOKIE, "authTokenId=$token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, true);

			$res2 = curl_exec($ch);
				
			//ALVO DE CAMPANHA - END
					
			curl_close($ch);
			
			//KONECTY - END
			
			//echo json_encode(array('status' => 'success', 'msg' => 'Seu contato foi enviado. Em breve ligaremos para você.', 'mailer' => $mail));
			echo json_encode(array('status' => 'success', 'msg' => 'Seu contato foi enviado. Em breve ligaremos para você.', 'response' => $log));
			
		} catch (Exception $e){
			
			echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
		}
	}
	
	
	public function action_get_button()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$pid = $this->request->post('pid');
		$grid = $this->request->post('grid');
		
		echo View::factory('buttons/'.$this->request->param('id'))->bind('pid',$pid)->bind('grid',$grid);
	}
	
	public function action_destroy()
	{
		Auth::instance()->logout();
		$this->redirect('/');
	}
	
} // End Welcome
