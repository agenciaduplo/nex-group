<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Busca extends Controller_Application {
	
	public function before(){
		parent::before();

		/*$session = Session::instance();
		
		if(empty($session->get('search_query'))){
		
			$search_query = array('types' => '1', 
								  'location' =>'',
								  'beds' => 0,
								  'suite' => 0,
								  'parking' => 0,
								  'area' => 0,
								  'price' => 0,
								  'code' => 0);
			
			$session->set('search_query', $search_query);
		}*/ 
		
		
		$this->template->ogtitle = 'Busca | NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = 'NEX Imóvel';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Home';
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
			Cookie::set('ref', $ref);
			
		} else if(!empty(Cookie::get('ref'))) {
			
			$ref = Cookie::get('ref');
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'neximovel') === false) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			Cookie::set('ref', $ref);
			
		} else {
			$ref = 'nex_imovel';
		}
		
		$live = ORM::factory('Property')
					->select('name_index')
					->distinct(TRUE)
					->find_all()
					->as_array('name_index', 'name_index');
							
		View::bind_global('live', $live);
		
		$this->template->ref = $ref;
	}
	
	public function action_index()
	{
		
		if($this->request->method() == Request::GET){
			$post = $this->request->query();
		} else {
			$post = $this->request->post();
		}
		
		$view = View::factory('search/index');
		
		if(($this->request->method() != Request::POST && $this->request->method() != Request::GET) || (@$post['popstate'] == 'yes')){
			
			$view->the_query = Cookie::get('search_query');
			
		} else {
		
			$view->the_query = json_encode($post);
			
		}
		
		
		
		$view->banner = ORM::factory('Place')->where('name', '=', 'Resultado de busca')->find()->banners->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->find();
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		$view->beds = array('1', '2', '3', '4+');
		$view->suite = array('1', '2', '3', '4+');
		$view->parking = array('1', '2', '3', '4+');
		
		$view->areamin =  array(0 => '0 &sup2;', 50 => '50 &sup2;', 100 => '100 &sup2;', 200 => '200 &sup2;', 300 => '300 &sup2;', 400 => '400 &sup2;', 500 => '500 &sup2;');
		$view->areamax =  array(50 => '50 &sup2;', 100 => '100 &sup2;', 200 => '200 &sup2;', 300 => '300 &sup2;', 400 => '400 &sup2;', 500 => '500 &sup2;', 1000000001 => 'M&aacute;x.');
	
		$view->pricemin =  array(0 => 'R$ 0', 100000 => 'R$ 100.000', 200000 => 'R$ 200.000', 300000 => 'R$ 300.000', 400000 => 'R$ 400.000', 500000 => 'R$ 500.000', 600000 => 'R$ 600.000');
		$view->pricemax =  array(100000 => 'R$ 100.000', 200000 => 'R$ 200.000', 300000 => 'R$ 300.000', 400000 => 'R$ 400.000', 500000 => 'R$ 500.000', 600000 => 'R$ 600.000', 1000000001 => 'M&aacute;x.');
		
		if($this->request->method() == Request::POST && !isset($post['order']) && !isset($post['justload'])){
			
			Cookie::set('search_query', json_encode($post));
			
		}

		if($this->request->method() == Request::POST && isset($post['order'])){
		
			Cookie::set('search_order', $post['order']);
			$search_order = $post['order'];
			
		} else {
	
			$search_order = Cookie::get('search_order');
		}
		
		$search_query = json_decode(Cookie::get('search_query'), true);
		
		if(isset($post['order']) || isset($post['justload'])){ $post = $search_query; }
		
		$code = substr(@$post['code'], 2);
		
		$search = ORM::factory('Property', $code);
		
		if(!$search->loaded()){
		
			if(strlen($code) > 1){
				
				Notices::add('error', 'Código não encontrado');
				
			}
		
			$against = isset($post['location']) ? $post['location'] : '';
		
			$against = addslashes($against);
		
			$select = "MATCH (name_index) AGAINST ('$against' IN BOOLEAN MODE) ";
			
			$post['types'] = isset($post['types']) ? implode(',', $post['types']) : '1,2,3,5,6';
			
			$samelocation = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', 'IN', DB::expr('('.$post['types'].')'))
								->and_where(DB::expr("MATCH (name_index)"), 'AGAINST', DB::expr("('$against' IN BOOLEAN MODE)"))
								->and_where('sold', '=', 0)
								->and_where('neximovel', '=', 1)
								->order_by('value', 'ASC')
								->limit(3);
		
			if(isset($post['location']) && !empty($post['location'])){
				$search->where(DB::expr("MATCH (name_index)"), 'AGAINST', DB::expr("('$against' IN BOOLEAN MODE)"));
			}
		
			if(isset($post['types']) && !empty($post['types'])){
				
				$search->where('type_id', 'IN', DB::expr('('.$post['types'].')'));
			}
			
			if(isset($post['beds']) && !empty($post['beds'])){
				
				$beds = implode(',', $post['beds']);
				
				$search->and_where_open();
				$search->where('beds', 'IN', DB::expr("($beds)"));
				
				if(in_array(4, $post['beds'])){
					$search->or_where('beds', '>', 4);
				}
				$search->and_where_close();
				
				$samebeds = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', 'IN', DB::expr('('.$post['types'].')'))
								->and_where('beds', 'IN', DB::expr("($beds)"))
								->and_where('sold', '=', 0)
								->and_where('neximovel', '=', 1)
								->order_by('value', 'ASC')
								->limit(3);
				
			}
			
			if(isset($post['suite']) && !empty($post['suite'])){
				
				$suite = implode(',', $post['suite']);
				
				$search->and_where_open();
				$search->where('suite', 'IN', DB::expr("($suite)"));
				
				if(in_array(4, $post['suite'])){
					$search->or_where('suite', '>', 4);
				}
				$search->and_where_close();
			}
			
			if(isset($post['parking']) && !empty($post['parking'])){
				
				$parking = implode(',', $post['parking']);
				
				$search->and_where_open();
				$search->where('parking', 'IN', DB::expr("($parking)"));
				
				if(in_array(4, $post['parking'])){
					$search->or_where('parking', '>', 4);
				}
				$search->and_where_close();
			}
			
			if(isset($post['areamin']) && isset($post['areamax'])){
				
				
				$search->where('area', 'BETWEEN', DB::expr($post['areamin']." AND ".$post['areamax']));
				
				$area0 = $post['areamin'] - 10;
				$area1 = $post['areamax'] + 10;
				
				$samearea = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', 'IN', DB::expr('('.$post['types'].')'))
								->and_where('area', 'BETWEEN', DB::expr($area0." AND ".$area1))
								->and_where('sold', '=', 0)
								->and_where('neximovel', '=', 1)
								->order_by('value', 'ASC')
								->limit(3);
			}
			
			
			$others = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', 'IN', DB::expr('('.$post['types'].')'))
								->and_where('sold', '=', 0)
								->and_where('neximovel', '=', 1)
								->order_by('value', 'ASC')
								->limit(3);
			
			
			if(isset($post['pricemin']) && isset($post['pricemax'])){
				
				$search->where('value', 'BETWEEN', DB::expr($post['pricemin']." AND ".$post['pricemax']));
				
				$sameprice = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('type_id', 'IN', DB::expr('('.$post['types'].')'))
								->and_where('value', 'BETWEEN', DB::expr($post['pricemin']." AND ".$post['pricemax']))
								->and_where('sold', '=', 0)
								->and_where('neximovel', '=', 1)
								->order_by('value', 'ASC')
								->limit(3);
			}
			
			$select .= " AS relevance";
			$search->select(DB::expr("$select"))->and_where('enabled', '=', 1)->and_where('sold', '=', 0)->and_where('neximovel','=',1);
			
			$total = clone $search;
			$total = $total->count_all();
			
			if($search_order !== null && $search_order != 'relevance'){
				$search_order = str_replace('"', '', $search_order);
				if($search_order == NULL ){
					$search_order = 'ASC';
				}
				$search->order_by('value', DB::expr($search_order));
			} else {
				$search->order_by('relevance', 'DESC')
						->order_by('value', 'ASC');
			}
		
			$offset = isset($post['offset']) ? $post['offset'] : 0;
			
			$properties = $search->find_all();
			
			$count = count($properties);								
	
		} else {

			$this->redirect('imovel/'.$post['code'].'/'.Controller_Application::friendly_seo_string($search->title));
			
		}
		
		$view->post_order = @$search_order;
		$view->order = array('relevance' => 'Relevancia', 'ASC' => 'Menor preço', 'DESC' => 'Maior preço');
		
		$view->properties = $properties;
		$view->count = $total;
		$view->search = $post;
		
		$ids = !empty($properties->as_array(null,'id')) ? implode(',', $properties->as_array(null,'id')) : '(0)';
		
		
		
		$view->samelocation = isset($samelocation) ? $samelocation->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
		$view->samebeds = isset($samebeds) ? $samebeds->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
		$view->samearea = isset($samearea) ? $samearea->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
		$view->others = isset($others) ? $others->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
		$view->sameprice = isset($sameprice) ? $sameprice->where('id', 'NOT IN', DB::expr("($ids)"))->find_all() : null;
		
		$this->template->origin = 'Busca';
		$this->template->pageClass = 'page-my-list';
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
}