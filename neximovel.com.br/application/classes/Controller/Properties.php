<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Properties extends Controller_Application {

	public function before(){
		parent::before();

		if(empty(Cookie::get('search_query'))){
		
			$search_query = array('types' => 0, 
								  'location' =>'',
								  'beds' => 0,
								  'suite' => 0,
								  'parking' => 0,
								  'area' => 0,
								  'price' => 0,
								  'code' => 0);
			
			Cookie::set('search_query', json_encode($search_query));
		}
		
		if(empty(Cookie::get('search_order'))){
			
			Cookie::set('search_order', 'ASC');
		}
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
			Cookie::set('ref', $ref);
			
		} else if(!empty(Cookie::get('ref'))) {
			
			$ref = Cookie::get('ref');
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'neximovel') === false) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			Cookie::set('ref', $ref);
			
		} else {
			$ref = 'nex_imovel';
		}
		
		$this->template->ref = $ref;
		
		$this->template->ogtitle = 'NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = 'A NEX tem o imóvel ideal para você. Apartamentos, coberturas, casas e diversos tipos de imóveis do jeito que você procura, do tamanho da sua família e dos seus sonhos. Clique e confira!';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Nex Imóvel - Apartamentos, Casas, Terrenos , Sala Comercial';
		
	}
	
	public function action_existe_imovel()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$id = substr($this->request->post('code'), 2);
		
		$property = ORM::factory('Property', $id);
		
		if(!$property->enabled || !$property->loaded()){
			echo 'error';
		} else {
			echo $property->sale.$property->id;
		}
		
	}
	
	public function action_imovel()
	{
		
		
		$view = View::factory('imovel/index');
	
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		
		$id = substr($this->request->param('id'), 2);
		
		$view->property = ORM::factory('Property', $id);
		
		if(!$view->property->enabled || !$view->property->loaded() || $view->property->sold){
			Notices::add('error', 'Imóvel não encontrado');
			$this->redirect('/');
		}
		
		$view->property->viewed = $view->property->viewed + 1;
		$view->property->save();
		
		$view->related = $view->property->get_related();
		
		$this->template->pageClass = 'page-imovel';
		
		$this->template->ogtitle = $view->property->title.' | NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = $view->property->description;
		$this->template->ogimage = $view->property->medias->find()->path;
	
		$this->template->origin = $view->property->id;
		
		$view->search = json_decode(Cookie::get('search_query'), true);
		$view->beds = array('1', '2', '3', '4+');
		$view->suite = array('1', '2', '3', '4+');
		$view->parking = array('1', '2', '3', '4+');
		
		$view->areamin =  array(0 => '0 &sup2;', 50 => '50 &sup2;', 100 => '100 &sup2;', 200 => '200 &sup2;', 300 => '300 &sup2;', 400 => '400 &sup2;', 500 => '500 &sup2;');
		$view->areamax =  array(50 => '50 &sup2;', 100 => '100 &sup2;', 200 => '200 &sup2;', 300 => '300 &sup2;', 400 => '400 &sup2;', 500 => '500 &sup2;', 1000000001 => 'M&aacute;x.');
	
		$view->pricemin =  array(0 => 'R$ 0', 100000 => 'R$ 100.000', 200000 => 'R$ 200.000', 300000 => 'R$ 300.000', 400000 => 'R$ 400.000', 500000 => 'R$ 500.000', 600000 => 'R$ 600.000');
		$view->pricemax =  array(100000 => 'R$ 100.000', 200000 => 'R$ 200.000', 300000 => 'R$ 300.000', 400000 => 'R$ 400.000', 500000 => 'R$ 500.000', 600000 => 'R$ 600.000', 1000000001 => 'M&aacute;x.');
		
		$view->banner = $view->property->banners->where('enabled' ,'=', 1)->order_by(DB::expr('RAND()'))->find();
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
	
	public function action_gerarXML()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$property = ORM::factory('Property', substr($this->request->param('id'), 2));
		
		$xml = new SimpleXMLElement('<rss/>');
		
		$xml->addAttribute('version', '2.0');
		$xml->addAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
		
		$channel = $xml->addChild('channel');
		
		$channel->addChild('title', 'NEX Imóvel');
		$channel->addChild('link', 'http://neximovel.com.br');
		$channel->addChild('description', 'NEX Imóvel');
		
		$item = $channel->addChild('item');
		$item->addChild('title', $property->title);
		$item->addChild('link', URL::base(TRUE).'imovel/'.$property->sale.$property->id.'/'.Controller_Application::friendly_seo_string($property->title));
		$item->addChild('description', $property->description);
		$item->addChild('g:image_link', $property->medias->order_by('order')->find()->path);
		$item->addChild('g:price', $property->value);
		$item->addChild('g:condition', 'usado');
		$item->addChild('g:id', $property->sale.$property->id);
		
		$this->response->headers('Content-Type', 'text/xml');
		$this->response->body($xml->asXML());
	}
	
	public function action_media()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$property = ORM::factory('Property', $this->request->param('id'));
		
		$output = array();

		foreach ($property->medias->order_by('order')->find_all()->as_array() as $key => $value)
		{
			$output[$key] = $value->as_array();
		}
		
		$this->response->body(json_encode($output));
			
	}
	
} // End Properties
