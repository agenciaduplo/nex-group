 <?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cobertura extends Controller_Application {

	public function before(){
		parent::before();
		$this->template->ref = '';
		$this->template->ogtitle = 'NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = 'A NEX tem o imóvel ideal para você. Coberturas do jeito que você procura, do tamanho da sua família e dos seus sonhos.';
		$this->template->ogimage = URL::base(TRUE).'public/img/ogimage.jpg';
		
		$this->template->title .= 'Nex Imóvel - Coberturas';
	}
	
	public function action_index()
	{	

		$view = View::factory('cobertura/index');
		
		$view->properties = ORM::factory('Property')
								->where('enabled', '=', 1)
								->and_where('coberturas', '=', 1)
								->and_where('type_id', '=', 2)
								->order_by('highlight', 'DESC')
								->order_by('value', 'ASC')
								->find_all();
								
		$view->banner = ORM::factory('Place')->where('name', '=', 'Cobertura')->find()->banners->where('enabled', '=', 1)->order_by(DB::expr('RAND()'))->find();
		
		$this->template->origin = 'Cobertura';
		$this->template->pageClass = 'page-cobertura';
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
	
	public function action_imovel()
	{
		
		$view = View::factory('cobertura/imovel');
	
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		
		$id = substr($this->request->param('id'), 2);
		
		$view->property = ORM::factory('Property', $id);
		
		if(!$view->property->enabled || !$view->property->loaded()){
			Notices::add('error', 'Imóvel não encontrado');
			$this->redirect('/');
		}
		
		$view->property->viewed = $view->property->viewed + 1;
		$view->property->save();
		
		$view->related = $view->property->get_related();
		
		$this->template->pageClass = 'page-imovel';
		
		$this->template->ogtitle = $view->property->title.' | NEX Imóvel';
		View::set_global('pagetitle', $this->template->ogtitle);
		$this->template->ogdescription = $view->property->description;
		$this->template->ogimage = $view->property->medias->find()->path;
	
		$this->template->origin = $view->property->id;
		
		$view->banner = $view->property->banners->where('enabled' ,'=', 1)->order_by(DB::expr('RAND()'))->find();
		
		if($this->request->post('load') == 'ajax'){		
			$this->auto_render = FALSE;
			$this->profiler    = NULL;
			
			$this->response->body($view);
			
		} else {
			$this->template->content = $view;
		}
	}
} 