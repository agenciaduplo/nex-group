 <section class="conceitual">
	<div class="spacer"></div>
	</section>
<div class="spacer"></div>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
	<div class="row">
		<div class="spacer"></div>
			<div class="tcenter">
			<?php if($banner->loaded()){?>
				<a href="<?php echo $banner->link; ?>" target="_blank">
					<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
				</a>
			<?php } ?>	
			</div>
		<div class="spacer"></div>
	</div>
	</div>
</section>

<section class="im-items">
	<div class="container">
		<div class="">
			<h1 class="title ossemibold p15-mobile fs25">Chegou a hora de conquistar a sua cobertura, confira o que a Nex separou para você:</h1>
		</div>
		<div class="spacer"></div>
		<div class="row widgets">
			<?php 
				foreach($properties as $k => $p){ 
					echo View::factory('cobertura/widget_item')->set('k', $k)->set('p', $p);
				} 
			?>
		</div>
		<div class="spacer"></div>
		<div class="spacer hide-mobile"></div>
		
		<div class="spacer hide-mobile"></div>
	</div>
</section>
<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>