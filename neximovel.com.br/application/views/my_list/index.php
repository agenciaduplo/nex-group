<section class="breadcrumb-sec graybg">
<div class="container">
  <ul class="breadcrumb graybg no-padding-left no-margin">
    <li>
      <a href="<?php echo URL::base(TRUE) ?>">In&iacute;cio</a>
    </li>
	<li>
      Minha lista
    </li>
  </ul>
</div>
</section>
<section class="single-page-header graybg">
	<div class="container">
		<h3 class="redcolor osbold upper">Minha Lista</h3>
		<h4 class="ossemibold graycolor mb20">Revise e compare seus imóveis preferidos</h4>
		<div class="spacer"></div>
		<div class="controls semilightredbg left w100">
			<div class="spacer"></div>
			<div class="col-md-4 hide-mobile hide-tablet">
				<ul class="inline-list no-margin">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Exibir como</span></li>
					<li id="radio">
						<input type="radio" id="radio1" name="radio" value="list"><label for="radio1" class="whitecolor"><i class="glyphicon glyphicon-list whitecolor"></i> Lista</label>
						<input type="radio" id="radio2" name="radio" value="grid"><label for="radio2" class="whitecolor"><i class="glyphicon glyphicon-th-large whitecolor"></i> Grade</label>
					</li>
				</ul>
			</div>
			<div class="col-md-4 whitecolor tcenter">
				<strong class="osbold fs25 count"><?php echo $count; ?></strong>
				<span>imóveis favoritos</span>
			</div>
			<div class="col-md-4">
				<ul class="inline-list no-margin right">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Ordernar por</span></li>
					<li class="ctrlpad">
						<?php echo Form::select('order', $order, null, array('id' => 'order', 'class' => 'sel-input')); ?>
					</li>
				</ul>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
</section>
<div class="spacer"></div>
<section>
	<div class="container lazyloader">
		<?php 
			foreach($properties as $p){
				echo View::factory('my_list/list_item')->set('p', $p);
				echo View::factory('my_list/grid_item')->set('p', $p);
			} 
		?>
		<?php echo $pagi->render(); ?>
	</div>
	
	
</section>
<?php if(isset($banner) && !empty($banner)) { ?>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
		<div class="spacer"></div>
		<div class="tcenter">
			<a href="<?php echo $banner->link; ?>" target="_blank">
				<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
			</a>
		</div>
		<div class="spacer"></div>
	</div>
</section>
<?php } ?>
<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>