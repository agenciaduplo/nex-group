<div class="visible-xs">

	<?php echo View::factory('home/mobile_search_form')->set('types', $types)->set('beds', $beds)->set('suite', $suite)->set('parking', $parking); ?>

</div>

<section class="conceitual">



<div id="homeslider" class="flexslider">

  <ul class="slides">
	<li id="banner-compromisso" style="height:100%;width:100%;;background-size:cover;background-position:center center;cursor:pointer;" class="text-center" onClick="window.open('http://www.neximovel.com.br/compromisso?utm_source=neximovel&utm_medium=banner');"></li>
	<li id="banner-compromisso-2" style="height:100%;width:100%;;background-size:cover;background-position:center center;cursor:pointer;" class="text-center" onClick="window.open('http://www.neximovel.com.br/compromisso?utm_source=neximovel&utm_medium=banner');"></li>
	<?php 

			$feat = ORM::factory('Featured')->where('enabled', '=', 1)->order_by('order', 'ASC')->find_all();

			foreach($feat as $k => $f){ ?>

				<li style="height:100%;width:100%;background:url(<?php echo $f->medias->find()->path; ?>);background-size:cover;background-position:center center;" class="text-center">

					<h2 class="whitecolor text-shadow fs40 no-margin"><?php echo $f->title; ?></h2>

					<h3 class="whitecolor text-shadow fs30 no-margin"><?php echo $f->subtitle; ?></h3>

					<div class="spacer"></div>

					<div class="spacer"></div>

					<div class="spacer"></div>

					<div class="spacer"></div>

					<a href="<?php echo $f->link; ?>" class="btn-banner" target="_blank"><?php echo $f->button; ?></a>

				</li>

	<?php } ?>
				
	<!-- items mirrored twice, total of 12 -->

  </ul>

</div>	



</section>

<div class="spacer"></div>

<div class="spacer"></div>



<section class="im-items col--small">

	

	<div class="col-lg-4 col-md-4 col-xs-12 tcenter sticky hidden-xs">

		<?php echo View::factory('home/sidebar_form')->set('types', $types)->set('beds', $beds)->set('suite', $suite)->set('parking', $parking); ?>

	</div>

	

	<div class="col-lg-8 col-md-8">

		<div class="container pull-right imgmax100">

			<div class="row">

				<div class="spacer"></div>

					<div class="tcenter">

					<?php if($banner->loaded()){?>

						<a href="<?php echo !empty($banner->link) ? $banner->link : 'javascript:void(0);'; ?>" target="_blank">

							<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">

						</a>

					<?php } ?>	

					</div>

				<div class="spacer"></div>

			</div>

			<div class="">

				<h1 class="title ossemibold p15-mobile fs25">Im&oacute;veis em destaque</h1>

			</div>

			<div class="spacer"></div>

			<div class="row widgets">

				<?php 

					foreach($properties as $k => $p){ 

						echo View::factory('home/widget_item')->set('k', $k)->set('p', $p);

					} 

				?>

			</div>

			<div class="spacer"></div>

			<div class="spacer hide-mobile"></div>

			<div class="row">

				<div class="col-md-12 tcenter">

					<img class="loader hide" src="<?php echo URL::base(TRUE) ?>public/img/ajax-loader-7.gif" title="Loader">

					<div class="view-more">VISUALIZAR MAIS IM&Oacute;VEIS</div>

				</div>

			</div>

			<div class="spacer hide-mobile"></div>

		</div>

	</div>

	

</section>

<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>