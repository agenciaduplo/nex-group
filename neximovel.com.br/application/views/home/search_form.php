<?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal ajaxsubmit', 'enctype' => 'multipart/form-data')) ?>
	<div class="col-md-12 no-padding-mobile">
		<div class="row">
		
			<!-- TIPO DE IMOVEL -->
			<div class="col-md-6">
				<div class="row"><h4 class="redcolor ossemibold">TIPO DE IM&Oacute;VEL</h4></div>
				<div class="row">
					<?php foreach($types as $k => $t){ ?>
					<label>
						<input type="checkbox" name="types[]" value="<?php echo $k?>" />
						<span class="types"><?php echo $t; ?></span>
					</label>						
					<?php } ?>						
				</div>
			</div>
			
			<!-- LOCALIZACAO -->
			<div class="col-md-6 no-padding">
				<h4 class="redcolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
				<?php echo Form::input('location', null, array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false', 'autocomplete' => 'off')); ?>
				<ul class="dropdown-menu location-dropdown">
					<li>
						<p class="search-tip">
							Digite bairro ou cidade<br>
							Ex: Centro Hist&oacute;rico Porto Alegre
						</p>
						<nav class="live-search" style="display:none">
								<?php foreach($live as $v){ ?>
								<p class="no-margin"><?php echo $v; ?></p>
								<?php } ?>
						</nav>
						
					</li> 
				</ul>
			</div>
		
			<div class="spacer"></div>
			
			<!-- DORMITORIOS -->
			<div class="col-md-2 col-sm-6 beds icons mb10 no-padding-left">
				<div class="col-lg-12 col-md-12 col-sm-4 col-xs-4 no-padding">
					<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-beds-red.png">&nbsp;&nbsp;&nbsp;<br class="visible-xs" />Dormit&oacute;rios</h5>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 no-padding">
					<?php foreach($beds as $k => $t){ ?>
					<label>
						<input type="checkbox" name="beds[]" value="<?php echo $k+1; ?>" />
						<span class=" "><?php echo $t; ?></span>
					</label>						
					<?php } ?>						
				</div>
			</div>
			
			<!-- SUITE -->
			<div class="col-md-2 col-sm-6 suite icons mb10 no-padding-left">
				<div class="col-lg-12 col-md-12 col-sm-4 col-xs-4 no-padding">
					<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-suite-red.png">&nbsp;&nbsp;&nbsp;<br class="visible-xs" />Su&iacute;tes</h5>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 no-padding">
					<?php foreach($suite as $k => $t){ ?>
					<label>
						<input type="checkbox" name="suite[]" value="<?php echo $k+1; ?>" />
						<span class=" "><?php echo $t; ?></span>
					</label>						
					<?php } ?>						
				</div>
			</div>
			
			<!-- VAGAS -->
			<div class="col-md-2 col-sm-6 parking icons mb10 no-padding-left">
				<div class="col-lg-12 col-md-12 col-sm-4 col-xs-4 no-padding">
					<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-parking-red.png">&nbsp;&nbsp;&nbsp;<br class="visible-xs" />Vagas</h5>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 no-padding">
					<?php foreach($parking as $k => $t){ ?>
					<label>
						<input type="checkbox" name="parking[]" value="<?php echo $k+1; ?>" />
						<span class=" "><?php echo $t; ?></span>
					</label>						
					<?php } ?>						
				</div>
			</div>
		
			<!-- AREA -->
			<div class="col-md-2 col-sm-6 col-xs-12 area icons btn-group mb10 no-padding-left">
				
					<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-area-red.png">&nbsp;&nbsp;&nbsp;&Aacute;rea</h5>
				
				<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => '�rea')); ?>
				
					<?php echo Form::input('area', null, array('id' => 'area_input', 'type' => 'hidden')); ?>
					<?php echo Form::button('area_btn', '', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
						<span class="caret"></span> 
						<span class="sr-only">Toggle Dropdown</span> 
					</button>
					<ul class="dropdown-menu keepopen rangemenu">
						<li>
							<div class="col-md-6 col-xs-6 no-padding min-side radioset">
								<h5 class="text-center ossemibold">DE</h5>
								<label>
									<input type="radio" name="areamin" value="0" checked/>
									<span class=" ">0 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="50" />
									<span class=" ">50 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="100" />
									<span class=" ">100 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="200" />
									<span class=" ">200 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="300" />
									<span class=" ">300 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="400" />
									<span class=" ">400 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamin" value="500" />
									<span class=" ">500 m&sup2;</span>
								</label>
								
							</div>
							<div class="col-md-6 col-xs-6 no-padding max-side radioset">
								<h5 class="text-center ossemibold">AT&Eacute;</h5>
								
								<label>
									<input type="radio" name="areamax" value="50" />
									<span class=" ">50 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="100" />
									<span class=" ">100 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="200" />
									<span class=" ">200 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="300" />
									<span class=" ">300 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="400" />
									<span class=" ">400 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="500" />
									<span class=" ">500 m&sup2;</span>
								</label>
								<label>
									<input type="radio" name="areamax" value="1000000001" checked/>
									<span class=" ">M&aacute;x.</span>
								</label>
							</div>
						</li> 
					</ul>
			</div>
			<!-- PRICE -->
			<div class="col-md-2 col-sm-6 col-xs-12 price icons btn-group mb10 no-padding">
					
					<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-price-red.png">&nbsp;&nbsp;&nbsp;Faixa de Pre&ccedil;o</h5>
					
					<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
					<?php echo Form::button('price_btn', '', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
						<span class="caret"></span> 
						<span class="sr-only">Toggle Dropdown</span> 
					</button>
					<ul class="dropdown-menu keepopen rangemenu">
						<li>
							<div class="col-md-6 col-xs-6 no-padding min-side radioset">
								<h5 class="text-center ossemibold">DE</h5>
								<label>
									<input type="radio" name="pricemin" value="0" checked/>
									<span class=" ">R$ 0</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="100000" />
									<span class=" ">R$ 100.000</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="200000" />
									<span class=" ">R$ 200.000</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="300000" />
									<span class=" ">R$ 300.000</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="400000" />
									<span class=" ">R$ 400.000</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="500000" />
									<span class=" ">R$ 500.000</span>
								</label>
								<label>
									<input type="radio" name="pricemin" value="600000" />
									<span class=" ">R$ 600.000</span>
								</label>
								
							</div>
							<div class="col-md-6 col-xs-6 no-padding max-side radioset">
								<h5 class="text-center ossemibold">AT&Eacute;</h5>
								
								<label>
									<input type="radio" name="pricemax" value="100000" />
									<span class=" ">R$ 100.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="200000" />
									<span class=" ">R$ 200.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="300000" />
									<span class=" ">R$ 300.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="400000" />
									<span class=" ">R$ 400.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="500000" />
									<span class=" ">R$ 500.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="600000" />
									<span class=" ">R$ 600.000</span>
								</label>
								<label>
									<input type="radio" name="pricemax" value="1000000001" checked/>
									<span class=" ">M&aacute;x.</span>
								</label>
							</div>
						</li> 
					</ul>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12 mb10 no-padding-right no-padding-mobile">
				<h5 class="whitecolor ossemibold hidden-xs" style="line-height:18px;">&nbsp;</h5>
				<?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'C�digo do im�vel')); ?>
			</div>
		
			<div class="col-md-2 col-sm-12 col-xs-12 no-padding-right right no-padding-mobile">
				<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search', 'style' => 'height:50px;')); ?>
			</div>
		</div>
	</div>
	
	<?php echo Form::input('load', 'ajax', array('type' => 'hidden')); ?>
<?php echo Form::close() ?>