<?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal ajaxsubmit', 'enctype' => 'multipart/form-data')) ?>
<div class="spacer"></div>

<div class="col-md-12 location">
	<?php echo Form::input('location', $search['location'], array('id' => 'location', 'class' => 'text-input', 'placeholder' => 'Digite bairro ou cidade')); ?>
</div>
<div class="spacer"></div>
	<div class="col-md-12 text-left ps-types">
		
			<?php foreach($types as $k => $t){ ?>
			<label>
				<?php echo Form::checkbox('types[]', $k, in_array($k, (array) @$search['types']), array('class' => '')); ?>
				<span class="types"><?php echo $t; ?></span>
			</label>						
			<?php } ?>						
		
	</div>
	

<div class="spacer hide-tablet"></div>


	<div class="col-md-12 col-sm-12 beds icons text-left">
		
		
			<img src="<?php echo URL::base(TRUE) ?>public/img/icon-beds-red.png" style="padding:13px;background:#fff;"><span>&nbsp;&nbsp;&nbsp;</span>
			<?php foreach($beds as $k => $t){ ?>
			<label>
				<?php echo Form::checkbox('beds[]', $k+1, in_array($k+1,  (array) @$search['beds']), array('class' => '')); ?>
				<span class=" "><?php echo $t; ?></span>
			</label>						
			<?php } ?>						
		
		<div class="spacer hide show-mobile"></div>
	</div>

	<div class="col-md-12 col-sm-12 suite icons text-left">
		
		
			<img src="<?php echo URL::base(TRUE) ?>public/img/icon-suite-red.png" style="padding:13px;background:#fff;"><span>&nbsp;&nbsp;&nbsp;</span>
			<?php foreach($suite as $k => $t){ ?>
			<label>
				<?php echo Form::checkbox('suite[]', $k+1, in_array($k+1,  (array) @$search['suite']), array('class' => '')); ?>
				<span class=" "><?php echo $t; ?></span>
			</label>						
			<?php } ?>						
		
		<div class="spacer hide show-mobile"></div>
	</div>

	<div class="col-md-12 col-sm-12 parking icons text-left">
		
		
			<img src="<?php echo URL::base(TRUE) ?>public/img/icon-parking-red.png" style="padding:13px;background:#fff;"><span>&nbsp;&nbsp;&nbsp;</span>
			<?php foreach($parking as $k => $t){ ?>
			<label>
				<?php echo Form::checkbox('parking[]', $k+1, in_array($k+1,  (array) @$search['parking']), array('class' => '')); ?>
				<span class=" "><?php echo $t; ?></span>
			</label>						
			<?php } ?>						
		
	</div>

<div class="spacer"></div>

	<div class="col-md-12 col-sm-12 area icons clearfix">
		<?php //echo Form::select('area', $area, $search['area'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<?php echo Form::input('area', $search['area'], array('id' => 'area_input', 'type' => 'hidden')); ?>
		<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input left redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
			<span class="caret"></span> 
			<span class="sr-only">Toggle Dropdown</span> 
		</button>
		<ul class="dropdown-menu keepopen rangemenu">
								<li>
									<div class="col-md-6 no-padding min-side radioset">
										<h5 class="text-center ossemibold">DE</h5>
										<?php foreach($areamin as $k => $t){ ?>
										<label>
											<?php 
												$checkmin = $k == @$search['areamin'] ? true : false;
												echo Form::radio('areamin', $k, $checkmin, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
										
									</div>
									<div class="col-md-6 no-padding max-side radioset">
										<h5 class="text-center ossemibold">AT&Eacute;</h5>
										
										<?php foreach($areamax as $k => $t){ ?>
										<label>
											<?php 
												$checkmax = $k == @$search['areamax'] ? true : false;
												echo Form::radio('areamax', $k, $checkmax, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
								</li> 
							</ul>
	</div>
<div class="spacer"></div>
	<div class="col-md-12 col-sm-12 price icons clearfix">
		<?php //echo Form::select('price', $price, $search['price'], array('id' => 'price', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
		<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
		<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input left redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
			<span class="caret"></span> 
			<span class="sr-only">Toggle Dropdown</span> 
		</button>
		<ul class="dropdown-menu keepopen rangemenu">
								<li>
									<div class="col-md-6 no-padding min-side radioset">
										<h5 class="text-center ossemibold">DE</h5>
										<?php foreach($pricemin as $k => $t){ ?>
										<label>
											<?php 
												$checkmin = $k == @$search['pricemin'] ? true : false;
												echo Form::radio('pricemin', $k, $checkmin, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
									<div class="col-md-6 no-padding max-side radioset">
										<h5 class="text-center ossemibold">AT&Eacute;</h5>
										<?php foreach($pricemax as $k => $t){ ?>
										<label>
											<?php 
												$checkmax = $k == @$search['pricemax'] ? true : false;
												echo Form::radio('pricemax', $k, $checkmax, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
								</li> 
							</ul>
	</div>
<div class="spacer"></div>

<div class="col-md-12 code">
	<?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input', 'placeholder' => 'Código do imóvel')); ?>
</div>
<div class="spacer"></div>
<div class="col-md-12 col-sm-6">
	<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
</div>
<div class="spacer"></div>
<?php echo Form::input('load', 'ajax', array('type' => 'hidden')); ?>
<?php echo Form::close() ?>