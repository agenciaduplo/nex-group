<section class="conceitual">
	<div class="spacer"></div>
	<div class="container searchform">
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
			<div class="col-md-10 no-padding-mobile">
				<div class="row">
					<div class="col-md-6">
						<h4 class="whitecolor ossemibold">TIPO DE IM&Oacute;VEL</h4>
						<?php echo Form::select('types', $types, null, array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
					</div>
					<div class="col-md-6">
						<h4 class="whitecolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', null, array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
						<ul class="dropdown-menu">
							<li>
								<p>
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
							</li> 
						</ul>
					</div>
				
					<div class="spacer"></div>
					<div class="advanced">
						
							<div class="col-md-4 col-sm-6 beds icons mb10"><?php echo Form::select('beds', $beds, null, array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
							<div class="col-md-4 col-sm-6 suite icons mb10"><?php echo Form::select('suite', $suite, null, array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
							<div class="col-md-4 col-sm-6 parking icons mb10"><?php echo Form::select('parking', $parking, null, array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						
						
							
							<div class="col-md-4 col-sm-6 col-xs-12 area icons btn-group mb10">
								<!-- AREA -->
								<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
								
									<?php echo Form::input('area', null, array('id' => 'area_input', 'type' => 'hidden')); ?>
									<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
										<span class="caret"></span> 
										<span class="sr-only">Toggle Dropdown</span> 
									</button>
									<ul class="dropdown-menu">
										<li>
											<p>
											  <label for="amount-area">Área:</label>
											  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
											</p>
											<div id="slider-area-range"></div>
										</li> 
									</ul>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 price icons btn-group mb10">
								<!-- PRICE -->
									<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
									<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
										<span class="caret"></span> 
										<span class="sr-only">Toggle Dropdown</span> 
									</button>
									<ul class="dropdown-menu">
										<li>
											<p>
											  <label for="amount">Faixa de pre&ccedil;o:</label>
											  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
											</p>
											<div id="slider-range"></div>
										</li> 
									</ul>
							</div>
							<div class="col-md-4 col-sm-6 mb10"><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
						
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="row">
					<h4 class="whitecolor ossemibold">&nbsp;</h4>
					<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
				</div>
				<div class="spacer"></div>
				<div class="row"><?php echo Form::button('save', '+ BUSCA AVAN&Ccedil;ADA', array('type' => 'button', 'class' => 'btn btn-advanced')); ?></div>
			</div>
		<?php echo Form::close() ?>
	</div>
</section>
<section class="termos_de_uso">
	<div class="spacer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="title ossemibold"><strong>POL&Iacute;TICA DE PRIVACIDADE<br /> PORTAL NEX IM&Oacute;VEL</strong></h3>
				<p>Como parte integrante dos&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o do&nbsp;<strong>PORTAL NEX IM&Oacute;VEL</strong>, este documento, denominado Pol&iacute;tica de Privacidade, tem por finalidade estabelecer as regras sobre a obten&ccedil;&atilde;o, acesso,&nbsp;uso, armazenamento, transfer&ecirc;ncia, enriquecimento dos dados coletados dos&nbsp;<strong>USU&Aacute;RIOS</strong>, al&eacute;m do registro de suas atividades, de acordo com as leis em vigor no Brasil.</p>
				<p>Quando o Usu&aacute;rio aceita essa Pol&iacute;tica de Privacidade confere sua livre e expressa concord&acirc;ncia com os&nbsp;termos&nbsp;aqui estipulados.</p>
				<ol>
				<li><strong> Obten&ccedil;&atilde;o dos dados:</strong></li>
				</ol>
				<p>1.1. Os dados ser&atilde;o obtidos quando inseridos ou submetidos voluntariamente pelos USU&Aacute;RIOS no PORTAL NEX IM&Oacute;VEL, a exemplo, mas n&atilde;o se limitando ao envio de mensagem para contato seja este por telefone ou por mensagem eletr&ocirc;nica, cadastro de seus dados pessoais para realiza&ccedil;&atilde;o de determinada opera&ccedil;&atilde;o, ou ainda, por procedimentos da NEX VENDAS que adquiram dados de acesso e navega&ccedil;&atilde;o do USU&Aacute;RIO.</p>
				<p>1.2. A NEX VENDAS e NEXGROUP n&atilde;o s&atilde;o respons&aacute;veis pela veracidade ou falta dela nas informa&ccedil;&otilde;es prestadas pelo USU&Aacute;RIO ou pela desatualiza&ccedil;&atilde;o destas, quando &eacute; de responsabilidade do USU&Aacute;RIO prest&aacute;-las com exatid&atilde;o ou atualiz&aacute;-las.</p>
				<ol start="2">
				<li><strong>Uso&nbsp;dos Dados:</strong></li>
				</ol>
				<p>2.1. Os dados coletados dos USU&Aacute;RIOS poder&atilde;o ser utilizados para as seguintes finalidades:</p>
				<ol>
				<li>Identificar e autentic&aacute;-los adequadamente;</li>
				<li>Garantir a sua seguran&ccedil;a, tal qual a da&nbsp;<strong>NEX VENDAS e NEXGROUP</strong>;</li>
				</ol>
				<ul>
				<li>Atender adequadamente &agrave;s suas solicita&ccedil;&otilde;es e d&uacute;vidas;</li>
				</ul>
				<ol>
				<li>Manter atualizados seus cadastros para fins de contato por telefone, correio eletr&ocirc;nico, SMS, ou por outros meios de comunica&ccedil;&atilde;o;</li>
				<li>Aperfei&ccedil;oar o&nbsp;uso&nbsp;e a experi&ecirc;ncia interativa durante sua navega&ccedil;&atilde;o no&nbsp;<strong>PORTAL NEX IM&Oacute;VEL</strong>;</li>
				<li>Efetuar estat&iacute;sticas, estudos, pesquisas e levantamentos pertinentes &agrave;s atividades de seus comportamentos ao utilizarem o&nbsp;<strong>PORTAL NEX IM&Oacute;VEL</strong>, realizando tais opera&ccedil;&otilde;es de forma anonimizada;</li>
				</ol>
				<ul>
				<li>Promover os servi&ccedil;os da&nbsp;<strong>NEX VENDAS</strong>e de seus parceiros, al&eacute;m de informar sobre novidades, funcionalidades, conte&uacute;dos, not&iacute;cias e demais informa&ccedil;&otilde;es relevantes para a manuten&ccedil;&atilde;o do relacionamento com a&nbsp;<strong>NEX VENDAS</strong>;</li>
				<li>Resguardar a&nbsp;<strong>NEX VENDAS e NEXGROUP</strong>de direitos e obriga&ccedil;&otilde;es relacionadas ao&nbsp;uso&nbsp;do&nbsp;<strong>PORTAL NEX IM&Oacute;VEL</strong>;</li>
				</ul>
				<ol>
				<li>Colaborar e/ou cumprir ordem judicial ou requisi&ccedil;&atilde;o por autoridade administrativa.</li>
				</ol>
				<p>&nbsp;</p>
				<p>2.2. A base de dados formada por meio de coleta de dados no PORTAL NEX IM&Oacute;VEL &eacute; de propriedade e responsabilidade da NEX VENDAS e n&atilde;o ser&aacute; comercializada e/ou alugada para terceiros, sendo que seu&nbsp;uso, acesso, compartilhamento, quando necess&aacute;rios, ser&atilde;o feitos dentro dos limites e prop&oacute;sitos das atividades da NEX VENDAS, podendo, neste sentido, virem a ser disponibilizadas para acesso e/ou consulta de outras entidades de seu grupo econ&ocirc;mico.</p>
				<p>2.3. Desde j&aacute;, o USU&Aacute;RIO est&aacute; ciente que a NEX VENDAS e NEXGROUP poder&atilde;o promover enriquecimento da sua base de dados, adicionando informa&ccedil;&otilde;es oriundas de outras fontes leg&iacute;timas, o qual manifesta consentimento expresso ao concordar com os&nbsp;termos&nbsp;da presente Pol&iacute;tica de Privacidade.</p>
				<p>2.4. Internamente, os dados somente ser&atilde;o acessados por profissionais devidamente autorizados pela NEX VENDAS e NEXGROUP, respeitando os princ&iacute;pios de proporcionalidade, necessidade e relev&acirc;ncia para os objetivos do PORTAL NEX IM&Oacute;VEL, al&eacute;m do compromisso de confidencialidade e preserva&ccedil;&atilde;o da privacidade nos&nbsp;termos&nbsp;desta Pol&iacute;tica.</p>
				<p>2.5. A NEX VENDAS possui parceiros comerciais que podem oferecer servi&ccedil;os por meio de funcionalidades ou sites acessados no PORTAL NEX IM&Oacute;VEL. Os dados fornecidos pelo USU&Aacute;RIO a estes parceiros ser&atilde;o de responsabilidade destes, estando assim sujeitos &agrave;s suas pr&oacute;prias pr&aacute;ticas de obten&ccedil;&atilde;o e&nbsp;uso&nbsp;de dados, sem que caiba qualquer &ocirc;nus &agrave; NEX VENDAS ou NEXGROUP pelo tratamento de tais informa&ccedil;&otilde;es.</p>
				<ol start="3">
				<li><strong> Do Registro de Atividades:</strong></li>
				</ol>
				<p>3.1. A NEX VENDAS registrar&aacute; as atividades efetuadas pelo USU&Aacute;RIO no PORTAL NEX IM&Oacute;VEL, as quais s&atilde;o:<br /> <br /> - Endere&ccedil;o IP do USU&Aacute;RIO;<br /> <br /> - A&ccedil;&otilde;es efetuadas pelo USU&Aacute;RIO no PORTAL&nbsp;NEX IM&Oacute;VEL;<br /> <br /> - P&aacute;ginas acessadas pelo USU&Aacute;RIO no PORTAL&nbsp;NEX IM&Oacute;VEL;<br /> <br /> - Datas e hor&aacute;rios de cada a&ccedil;&atilde;o do USU&Aacute;RIO no PORTAL&nbsp;NEX IM&Oacute;VEL, al&eacute;m do acesso que fizer &agrave;s p&aacute;ginas e das ferramentas e funcionalidades que utilizar;<br /> <br /> - Informa&ccedil;&otilde;es sobre o dispositivo utilizado pelo USU&Aacute;RIO, vers&atilde;o de sistema operacional, navegador, dentre outros aplicativos instalados;<br /> <br /> - Session ID, quando dispon&iacute;vel.</p>
				<p>3.2. Outras tecnologias poder&atilde;o ser utilizadas para a obten&ccedil;&atilde;o de dados de navega&ccedil;&atilde;o do USU&Aacute;RIO, inclusive cookies, no entanto, respeitar&atilde;o sempre os&nbsp;termos&nbsp;desta Pol&iacute;tica e as op&ccedil;&otilde;es do USU&Aacute;RIO a respeito de sua coleta e armazenamento.</p>
				<ol start="4">
				<li><strong> Armazenamento dos Dados:</strong></li>
				</ol>
				<p>4.1. Os dados coletados estar&atilde;o armazenados em ambiente seguro e controlado, observado o estado da t&eacute;cnica dispon&iacute;vel. Todavia, considerando que nenhum sistema de seguran&ccedil;a &eacute; infal&iacute;vel, a NEX VENDAS se exime de quaisquer responsabilidades por eventuais danos e/ou preju&iacute;zos decorrentes de falhas, v&iacute;rus ou invas&otilde;es do banco de dados do PORTAL&nbsp;NEX IM&Oacute;VEL, salvo nos casos em que tiver dolo ou culpa.</p>
				<p>4.2. Os dados do USU&Aacute;RIO obtidos no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;poder&atilde;o ser armazenados em servidor pr&oacute;prio da NEX VENDAS, NEXGROUP ou de terceiro contratado para esse fim, sejam eles alocados no Brasil ou no exterior, podendo ainda ser armazenados por meios de tecnologia de cloud computing e/ou outras que se inventem no futuro, visando sempre a melhoria e aperfei&ccedil;oamento das atividades da NEX VENDAS.</p>
				<p>4.3. O USU&Aacute;RIO tem o direito de exigir da NEX VENDAS a exibi&ccedil;&atilde;o, retifica&ccedil;&atilde;o ou ratifica&ccedil;&atilde;o dos dados pessoais que lhe dizem respeito, por meio das ferramentas de contato disponibilizadas pelo PORTAL&nbsp;NEX IM&Oacute;VEL, notadamente por e-mail, chat on-line e telefone.</p>
				<p>4.4. Por essas mesmas ferramentas, o USU&Aacute;RIO poder&aacute; requerer a exclus&atilde;o de todos os seus dados pessoais coletados e registrados pela NEX VENDAS.</p>
				<p>4.5. A NEX VENDAS poder&aacute;, para fins de auditoria e preserva&ccedil;&atilde;o de direitos, permanecer com o hist&oacute;rico de registro dos dados do USU&Aacute;RIO pelo per&iacute;odo m&aacute;ximo de 5 (cinco) anos, podendo ser estendido por prazo maior nas hip&oacute;teses que a lei ou norma regulat&oacute;ria assim estabelecer ou para preserva&ccedil;&atilde;o de direitos, possuindo a NEX VENDAS faculdade de exclu&iacute;-los definitivamente segundo sua conveni&ecirc;ncia em prazo menor.</p>
				<p>4.5.1. Os dados preservados na condi&ccedil;&atilde;o acima indicada ter&atilde;o seu&nbsp;uso&nbsp;limitado &agrave;s hip&oacute;teses iii, viii e ix da cl&aacute;usula 3.1 da presente Pol&iacute;tica.</p>
				<ol start="5">
				<li><strong> Disposi&ccedil;&otilde;es Gerais:</strong></li>
				</ol>
				<p>5.1. O teor desta Pol&iacute;tica de Privacidade poder&aacute; ser atualizado ou modificado a qualquer momento, conforme a finalidade do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;ou conveni&ecirc;ncia de sua controladora, tal qual para adequa&ccedil;&atilde;o e conformidade legal de disposi&ccedil;&atilde;o de lei ou norma que tenha for&ccedil;a jur&iacute;dica equivalente, cabendo ao USU&Aacute;RIO verific&aacute;-la sempre que efetuar o acesso ao PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>5.2. O USU&Aacute;RIO dever&aacute; entrar em contato em caso de qualquer d&uacute;vida com rela&ccedil;&atilde;o &agrave;s disposi&ccedil;&otilde;es constantes desta Pol&iacute;tica de Privacidade por meio dos contatos expostos no PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>5.3. Caso empresas terceirizadas realizem o processamento de quaisquer dados coletados pelo PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; dever&atilde;o respeitar as condi&ccedil;&otilde;es aqui estipuladas e nas normas de Seguran&ccedil;a da Informa&ccedil;&atilde;o da NEXVENDAS, obrigatoriamente.</p>
				<p>5.4. Caso alguma disposi&ccedil;&atilde;o desta Pol&iacute;tica de Privacidade seja considerada ilegal ou ileg&iacute;tima por autoridade da localidade em que o USU&Aacute;RIO resida ou da sua conex&atilde;o &agrave; internet, as demais condi&ccedil;&otilde;es permanecer&atilde;o em pleno vigor e efeito.</p>
				<ol start="6">
				<li><strong> Lei Aplic&aacute;vel e Jurisdi&ccedil;&atilde;o</strong></li>
				</ol>
				<p>6.1. A presente Pol&iacute;tica de Privacidade est&aacute; vinculada aos&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o do Portal e ser&aacute; interpretada segundo a legisla&ccedil;&atilde;o brasileira, no idioma portugu&ecirc;s, sendo eleito o Foro da Comarca de Porto Alegre, no Rio Grande do Sul para dirimir qualquer lit&iacute;gio ou controv&eacute;rsia envolvendo o presente documento, salvo ressalva espec&iacute;fica de compet&ecirc;ncia pessoal, territorial ou funcional pela legisla&ccedil;&atilde;o aplic&aacute;vel.</p>
				<ol start="7">
				<li><strong> Gloss&aacute;rio</strong></li>
				</ol>
				<p>Acesso &agrave; base de dados: Processo de prestar informa&ccedil;&atilde;o ao Usu&aacute;rio.</p>
				<p>Compartilhamento: Processo de replica&ccedil;&atilde;o ou duplica&ccedil;&atilde;o da base de dados.</p>
				<p>Cookies: Arquivos enviados pelo servidor do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;para o computador dos USU&Aacute;RIOS, com a finalidade de identificar o computador e obter dados de acesso, como p&aacute;ginas navegadas ou links clicados, permitindo, desta forma, personalizar a navega&ccedil;&atilde;o destes, de acordo com o seu perfil.</p>
				<p>NEX VENDAS NEGOCIOS IMOBILIARIOS LTDA., pessoa jur&iacute;dica de direito privado inscrita no CNPJ sob o n&ordm; 18.226.104/0001-84, com sede na Rua Furriel Luis Ant&ocirc;nio Vargas, 250 salas 503, CEP 90470-130, em Porto Alegre/RS.</p>
				<p>IP: Abreviatura de Internet Protocol. &Eacute; um conjunto de n&uacute;meros que identifica o computador dos e usu&aacute;rios na Internet.</p>
				<p>Logs: Registros de atividades dos usu&aacute;rios efetuadas no PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>Cloud Computing: Ou computa&ccedil;&atilde;o em nuvem, &eacute; tecnologia de virtualiza&ccedil;&atilde;o de servi&ccedil;os constru&iacute;da a partir da interliga&ccedil;&atilde;o de mais de um servidor por meio de uma rede de informa&ccedil;&atilde;o comum (p.ex. a Internet), com objetivo de reduzir custos e aumentar a disponibilidade dos servi&ccedil;os sustentados.</p>
				<p>PORTAL&nbsp;NEX IM&Oacute;VEL: Designa o endere&ccedil;o eletr&ocirc;nico&nbsp;<a href="http://www.gafisa.com.br/">www.neximovel.com.br.</a></p>
				<p>USU&Aacute;RIOS: Pessoas que acessam ou interagem com as atividades oferecidas pelo PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>______________________________________</p>
				<p>&nbsp;</p>
				<p><strong>TERMOS&nbsp;DE&nbsp;USO&nbsp;E CONDI&Ccedil;&Otilde;ES DE NAVEGA&Ccedil;&Atilde;O<br /> PORTAL NEX IM&Oacute;VEL</strong></p>
				<p>Bem-vindo ao&nbsp;<strong>PORTAL NEX IM&Oacute;VEL</strong>. A seguir apresentamos a voc&ecirc;&nbsp;<strong>(USU&Aacute;RIO)</strong>&nbsp;os&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o, documento que relaciona as principais regras que devem ser observadas por todos que acessam o&nbsp;<strong>PORTAL&nbsp;NEX IM&Oacute;VEL</strong>&nbsp;ou utilizam suas funcionalidades.</p>
				<p>Como condi&ccedil;&atilde;o para acesso e&nbsp;uso&nbsp;do&nbsp;<strong>PORTAL&nbsp;NEX IM&Oacute;VEL</strong>, o&nbsp;<strong>USU&Aacute;RIO</strong>&nbsp;deve declarar que fez a leitura completa e atenta das regras deste documento e de sua Pol&iacute;tica de Privacidade, estando plenamente ciente e de acordo com elas.</p>
				<ol>
				<li><strong> Informa&ccedil;&otilde;es Gerais sobre o Portal</strong></li>
				</ol>
				<p>1.1. Este Portal &eacute; dedicado a publicar informa&ccedil;&otilde;es, ferramentas e consultas r&aacute;pidas vinculadas aos servi&ccedil;os prestados pela NEX VENDAS a seus clientes, notadamente de constru&ccedil;&atilde;o, incorpora&ccedil;&atilde;o e gest&atilde;o imobili&aacute;ria, dentre outros afins.</p>
				<p>1.2. Para acessar o PORTAL&nbsp;<strong>NEX IM&Oacute;VEL</strong>&nbsp;de forma segura &eacute; de inteira responsabilidade do USU&Aacute;RIO dispor de servi&ccedil;o de conex&atilde;o &agrave; internet, com antiv&iacute;rus e firewall habilitados.</p>
				<p>1.3. O PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;e suas funcionalidades s&atilde;o apresentadas aos USU&Aacute;RIOS na maneira como est&atilde;o dispon&iacute;veis, podendo passar por constantes aprimoramentos e atualiza&ccedil;&otilde;es, obrigando-se a NEX VENDAS a:</p>
				<p>1.3.1. Preservar a funcionalidade do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; com links n&atilde;o quebrados, utilizando layout que respeita a usabilidade e navegabilidade, sempre que poss&iacute;vel.</p>
				<p>1.3.2. Exibir as funcionalidades de maneira clara, completa, precisa e suficiente de modo que exista a exata percep&ccedil;&atilde;o das opera&ccedil;&otilde;es realizadas.</p>
				<p>1.3.3. Garantir, por meio do estado da t&eacute;cnica dispon&iacute;vel, o sigilo dos dados inseridos nas funcionalidades oferecidas no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; somente sendo acess&iacute;veis pela NEX VENDAS, por quem o USU&Aacute;RIO consentir, al&eacute;m de si pr&oacute;prio.</p>
				<p>1.3.4. Obter as licen&ccedil;as e autoriza&ccedil;&otilde;es necess&aacute;rias para a disponibiliza&ccedil;&atilde;o de conte&uacute;dos por meio do PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>A NEX VENDAS envida seus esfor&ccedil;os para a disponibilidade cont&iacute;nua e permanente do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; No entanto, pode ocorrer, eventualmente, alguma indisponibilidade tempor&aacute;ria decorrente de manuten&ccedil;&atilde;o necess&aacute;ria ou mesmo gerada por motivo de for&ccedil;a maior, como desastres naturais, falhas ou colapsos nos sistemas centrais de comunica&ccedil;&atilde;o e acesso &agrave; internet ou fatos de terceiro que fogem de sua esfera de vigil&acirc;ncia e responsabilidade.</p>
				<p>1.4.1. Se isso ocorrer, a NEX VENDAS far&aacute; o que estiver ao seu alcance para restabelecer o acesso ao PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;o mais breve poss&iacute;vel, dentro das limita&ccedil;&otilde;es t&eacute;cnicas de seus servi&ccedil;os e servi&ccedil;os de terceiros, dos quais a NEX VENDAS depende para ficar online.</p>
				<p>1.5. Eventuais procedimentos de manuten&ccedil;&atilde;o ser&atilde;o informados por meio dos canais oficiais de comunica&ccedil;&atilde;o da NEX VENDAS (a exemplo, mas n&atilde;o se limitando, perfis oficiais nas m&iacute;dias sociais, telefone de atendimento ou e-mail), caso seja necess&aacute;rio que este fique indispon&iacute;vel por longos per&iacute;odos.</p>
				<ol start="2">
				<li><strong> Funcionalidades do Portal</strong></li>
				</ol>
				<p>2.1. O PORTAL NEX VENDAS oferece aplica&ccedil;&otilde;es de internet com objetivos de criar ou fortalecer o relacionamento com clientes, promover a atua&ccedil;&atilde;o da companhia e de fornecer informa&ccedil;&otilde;es, detalhes e orienta&ccedil;&otilde;es sobre im&oacute;veis e sua comercializa&ccedil;&atilde;o a quem se interessar.</p>
				<p>2.2. Determinadas aplica&ccedil;&otilde;es podem possuir&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;ou Pol&iacute;tica de Privacidade pr&oacute;prios. Por isso, &eacute; indispens&aacute;vel a leitura de tais documentos antes de sua utiliza&ccedil;&atilde;o, pois os&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e a Pol&iacute;tica de Privacidade do site n&atilde;o alcan&ccedil;am tais aplica&ccedil;&otilde;es, a menos que esta disposi&ccedil;&atilde;o esteja expressa em tais documentos.</p>
				<ol start="3">
				<li><strong> Das responsabilidades e obriga&ccedil;&otilde;es dos USU&Aacute;RIOS</strong></li>
				</ol>
				<p>3.1. Os USU&Aacute;RIOS se obrigam a realizar navega&ccedil;&atilde;o &eacute;tica no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; adequada com os prop&oacute;sitos aqui estabelecidos, sempre respeitando as condi&ccedil;&otilde;es que regem a utiliza&ccedil;&atilde;o do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;e sua finalidade.</p>
				<p>3.2. Os USU&Aacute;RIOS se comprometem a fornecer dados cadastrais corretos, completos e atualizados para que a NEX VENDAS possa prestar seus servi&ccedil;os em conformidade com as leis e normas regulamentares em vigor, al&eacute;m de fornecer canal de contato apto a ser acionado pela NEX VENDAS para o melhor cumprimento dos servi&ccedil;os.</p>
				<p>3.3. Ao acessar o PORTAL GAFISA, os USU&Aacute;RIOS declaram que ir&atilde;o respeitar todos os direitos de propriedade intelectual de titularidade da NEX VENDAS, tal qual todos os direitos referentes a terceiros que porventura estejam, ou estiveram, de alguma forma, dispon&iacute;veis no PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>3.4. N&atilde;o &eacute; permitido o acesso as &aacute;reas de programa&ccedil;&atilde;o do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; seu banco de dados ou qualquer outro conjunto de informa&ccedil;&otilde;es que fa&ccedil;a parte da atividade de Webmastering.</p>
				<p>3.5. Tamb&eacute;m n&atilde;o &eacute; autorizado ao USU&Aacute;RIO realizar ou permitir engenharia reversa, nem traduzir, decompilar, copiar, modificar, reproduzir, alugar, sublicenciar, publicar, divulgar, transmitir, emprestar, distribuir ou, de outra maneira, dispor das ferramentas de consulta deste Portal e de suas funcionalidades.</p>
				<p>3.7. No PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;&eacute; proibida a utiliza&ccedil;&atilde;o, de aplicativos spider, ou de minera&ccedil;&atilde;o de dados, de qualquer tipo ou esp&eacute;cie, al&eacute;m de outro aqui n&atilde;o tipificado, mas que atue de modo automatizado, tanto para realizar opera&ccedil;&otilde;es massificadas ou para quaisquer outras finalidades.</p>
				<p>3.8. O descumprimento de quaisquer das obriga&ccedil;&otilde;es aqui estipuladas poder&aacute; acarretar na suspens&atilde;o das funcionalidades, conforme previsto neste documento.</p>
				<p>3.9. A eventual remo&ccedil;&atilde;o, bloqueio ou suspens&atilde;o de qualquer conte&uacute;do ou funcionalidade do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;em decorr&ecirc;ncia de alguma reclama&ccedil;&atilde;o, dever&aacute; ser sempre compreendida como demonstra&ccedil;&atilde;o de boa-f&eacute; e inten&ccedil;&atilde;o de solu&ccedil;&atilde;o amig&aacute;vel de conflitos, jamais, como reconhecimento de culpa ou de qualquer infra&ccedil;&atilde;o pela NEX VENDAS a direito de terceiro.</p>
				<p>3.10. A NEX VENDAS n&atilde;o envia e-mails ou outros tipos de comunica&ccedil;&atilde;o com links externos. Desta forma, caso o USU&Aacute;RIO receba alguma mensagem desse tipo deve estar ciente dos riscos ao clicar no link, pois pode ser tentativa de fraude conhecida como phishing.</p>
				<p>3.11. Na incid&ecirc;ncia de danos ao PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;ou a terceiros, o respons&aacute;vel se compromete a arcar com todas as obriga&ccedil;&otilde;es de indenizar o sujeito lesado.</p>
				<p>3.12. O USU&Aacute;RIO se obriga a ressarcir a NEX VENDAS de todas as condena&ccedil;&otilde;es e preju&iacute;zos que sofrer cuja origem for de atos praticados por meio de seu acesso, assumindo o polo passivo de a&ccedil;&atilde;o judicial ou procedimento administrativo e requerendo a exclus&atilde;o da NEX VENDAS, inclusive, devendo arcar totalmente com as despesas e custas processuais atinentes, deixando-a livre de preju&iacute;zos e &ocirc;nus.</p>
				<ol start="4">
				<li><strong> Da Isen&ccedil;&atilde;o e Limita&ccedil;&otilde;es de Responsabilidade da NEX VENDAS</strong></li>
				</ol>
				<p>4.1. FICA EXPRESSAMENTE ESTABELECIDO QUE A NEX VENDAS N&Atilde;O TEM A OBRIGA&Ccedil;&Atilde;O DE CONTROLAR TODAS AS A&Ccedil;&Otilde;ES EXECUTADAS PELOS USU&Aacute;RIOS NO&nbsp;USO&nbsp;DA PLATAFORMA E, POR CONSEGUINTE, A NEX VENDAS N&Atilde;O PODER&Aacute; SER RESPONSABILIZADA POR QUAISQUER ATOS DE SEUS USU&Aacute;RIOS, INCLUSIVE DE CAR&Aacute;TER ILEGAL, IMORAL OU ANTI&Eacute;TICO, PORVENTURA PERPETRADOS, CABENDO &Agrave; ESTES RESPONDEREM PESSOAL E SOLIDARIAMENTE POR EVENTUAIS RECLAMA&Ccedil;&Otilde;ES DE TERCEIROS OU DEMANDAS JUDICIAIS, DEVENDO MANTER A NEX VENDAS LIVRE DE QUALQUER RESPONSABILIDADE OU &Ocirc;NUS NESTE SENTIDO.</p>
				<p>4.2. As informa&ccedil;&otilde;es disponibilizadas no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;n&atilde;o podem ser interpretadas como consultoria, n&atilde;o sendo a NEX VENDAS respons&aacute;vel de qualquer forma pelas decis&otilde;es financeiras e sobre im&oacute;veis tomadas pelos USU&Aacute;RIOS.</p>
				<p>4.3. A NEX VENDAS n&atilde;o se responsabiliza por qualquer dano direto ou indireto ocasionado por eventos de terceiros, como ataque de hackers, falhas no sistema, no servidor ou na conex&atilde;o &agrave; internet, inclusive por a&ccedil;&otilde;es de softwares maliciosos como v&iacute;rus, cavalos de Tr&oacute;ia, e outros que possam, de algum modo, danificar o equipamento ou a conex&atilde;o dos USU&Aacute;RIOS em decorr&ecirc;ncia do acesso, utiliza&ccedil;&atilde;o ou navega&ccedil;&atilde;o no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; bem como a transfer&ecirc;ncia de dados, arquivos, imagens, textos, &aacute;udios ou v&iacute;deos contidos neste.</p>
				<p>4.4. Os USU&Aacute;RIOS n&atilde;o possuem qualquer direito para exigir a disponibilidade do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;conforme melhor lhes conv&ecirc;m, tampouco poder&atilde;o pleitear indeniza&ccedil;&atilde;o ou repara&ccedil;&atilde;o de danos em caso do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;permanecer fora do ar, independente da motiva&ccedil;&atilde;o.</p>
				<p>4.5. A NEX VENDAS n&atilde;o det&eacute;m qualquer responsabilidade pela navega&ccedil;&atilde;o dos USU&Aacute;RIOS nos links externos contidos no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; sendo seus deveres a leitura dos&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Pol&iacute;tica de Privacidade do Portal acessado e agir conforme o determinado.</p>
				<p>4.5.1. A NEX VENDAS n&atilde;o verifica, controla, aprova ou garante a adequa&ccedil;&atilde;o ou exatid&atilde;o das informa&ccedil;&otilde;es ou dados disponibilizados em tais links, n&atilde;o sendo, portanto, respons&aacute;vel por preju&iacute;zos, perdas ou danos ocorridos pela visita de tais sites, cabendo ao interessado verificar a confiabilidade das informa&ccedil;&otilde;es e dados ali exibidos antes de tomar alguma decis&atilde;o ou praticar algum ato.</p>
				<ol start="5">
				<li><strong> Direitos Autorais e Propriedade Intelectual do Portal</strong></li>
				</ol>
				<p>5.1. O&nbsp;uso&nbsp;comercial da express&atilde;o &ldquo;NEX IM&Oacute;VEL&rdquo; ou de palavras que se assemelham a estas, por&eacute;m com grafia diferenciada, como marcas, nomes empresariais ou nomes de dom&iacute;nio, al&eacute;m dos conte&uacute;dos das telas do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; assim como os programas de computador, bancos de dados, redes e seus arquivos, s&atilde;o de propriedade da NEX VENDAS e est&atilde;o resguardados pelas leis e tratados internacionais de prote&ccedil;&atilde;o &agrave; propriedade intelectual, incluindo, mas n&atilde;o se limitando, &agrave;s Leis Federais 9.609/1998, 9.610/1998, 9.279/1996 e 10.406/2002.</p>
				<p>5.2. Ao acessar o PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; os USU&Aacute;RIOS declaram que ir&atilde;o respeitar todos os direitos de propriedade intelectual da NEX VENDAS, incluindo, mas n&atilde;o se limitando, aos programas de computador, direitos autorais e direitos de propriedade industrial sobre as marcas, patentes, nomes de dom&iacute;nio, denomina&ccedil;&atilde;o social e desenhos industriais, depositados ou registrados em nome da NEX VENDAS, bem como de todos os direitos referentes a terceiros que porventura estejam, ou estiveram, de alguma forma, dispon&iacute;veis no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; O acesso ou&nbsp;uso&nbsp;das funcionalidades do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;n&atilde;o confere aos Visitantes ou USU&Aacute;RIOS quaisquer direitos ao&nbsp;uso&nbsp;dos nomes, t&iacute;tulos, palavras, frases, marcas, patentes, nomes de dom&iacute;nio, denomina&ccedil;&atilde;o social, obras liter&aacute;rias, art&iacute;sticas, l&iacute;tero-musicais, imagens, ilustra&ccedil;&otilde;es, dados e informa&ccedil;&otilde;es, dentre outras, que nele estejam ou estiveram dispon&iacute;veis.</p>
				<p>5.3. A reprodu&ccedil;&atilde;o dos conte&uacute;dos descritos anteriormente est&aacute; proibida, salvo com pr&eacute;via autoriza&ccedil;&atilde;o por escrito da NEX VENDAS ou caso se destinem ao&nbsp;uso&nbsp;exclusivamente pessoal e sem que em nenhuma circunst&acirc;ncia os USU&Aacute;RIOS adquiram qualquer direito sobre os mesmos.</p>
				<p>5.4. Os USU&Aacute;RIOS assumem toda e qualquer responsabilidade, de car&aacute;ter civil e/ou criminal, pela utiliza&ccedil;&atilde;o indevida das informa&ccedil;&otilde;es, textos, gr&aacute;ficos, marcas, patentes, nomes de dom&iacute;nio, obras, imagens, logotipos, enfim, de todo e qualquer direito de propriedade intelectual ou industrial existentes no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;ou de titularidade da NEX VENDAS.</p>
				<p>5.5. Qualquer outro tipo de utiliza&ccedil;&atilde;o de material autorizado, inclusive para fins editoriais, comerciais ou publicit&aacute;rios, s&oacute; poder&aacute; ser feito na forma e mediante pr&eacute;vio e expresso consentimento da NEX VENDAS. O&nbsp;uso&nbsp;comercial n&atilde;o autorizado poder&aacute; incorrer em infra&ccedil;&otilde;es c&iacute;veis e criminais.</p>
				<p>5.6. Qualquer reutiliza&ccedil;&atilde;o de material autorizado dever&aacute; ser objeto de nova autoriza&ccedil;&atilde;o da NEX VENDAS.</p>
				<p>5.7. A autoriza&ccedil;&atilde;o conferida para utiliza&ccedil;&atilde;o do material solicitado n&atilde;o poder&aacute; ser transferida a terceiros, mesmo que vinculados ao sujeito autorizado.</p>
				<p>5.8. A utiliza&ccedil;&atilde;o das funcionalidades oferecidas se dar&aacute; na forma de presta&ccedil;&atilde;o de servi&ccedil;os, n&atilde;o conferindo ao USU&Aacute;RIO nenhum direito sobre o programa de computador e/ou do banco de dados utilizados pela NEX VENDAS ou de suas estruturas inform&aacute;ticas que sustentam as aplica&ccedil;&otilde;es do PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>5.9. Quando os USU&Aacute;RIOS enviam ao PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;quaisquer conte&uacute;dos, tais como, mas n&atilde;o se limitando a coment&aacute;rios, v&iacute;deos, imagens, &aacute;udio e mensagens, declaram que det&eacute;m a titularidade dos direitos autorais sobre estes ou autoriza&ccedil;&atilde;o para sua utiliza&ccedil;&atilde;o e concedem &agrave; NEX VENDAS licen&ccedil;a irrevog&aacute;vel, perp&eacute;tua, mundial, irrestrita, isenta de royalties e n&atilde;o exclusiva de reprodu&ccedil;&atilde;o, adapta&ccedil;&atilde;o, modifica&ccedil;&atilde;o, tradu&ccedil;&atilde;o, publica&ccedil;&atilde;o, distribui&ccedil;&atilde;o ou exibi&ccedil;&atilde;o no pr&oacute;prio site ou em ambientes digitais que lhe perten&ccedil;am e estejam vinculados &agrave;s atividades do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; possibilitando a melhor utiliza&ccedil;&atilde;o da ferramenta ou ilustra&ccedil;&atilde;o de algum produto, servi&ccedil;o ou atividade por ele oferecidos.</p>
				<ol start="6">
				<li><strong> Privacidade dos usu&aacute;rios no Portal</strong></li>
				</ol>
				<p>6.1. A NEX VENDAS possui documento pr&oacute;prio, denominado Pol&iacute;tica de Privacidade, que regula o tratamento dado &agrave;s informa&ccedil;&otilde;es coletadas no PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>6.2. A Pol&iacute;tica de Privacidade &eacute; parte integrante e insepar&aacute;vel dos&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;e pode ser acessada no link encontrado em seu rodap&eacute;.</p>
				<p>6.3. Caso alguma disposi&ccedil;&atilde;o da Pol&iacute;tica de Privacidade conflite com qualquer outra do presente documento, dever&aacute; prevalecer o descrito na norma mais espec&iacute;fica.</p>
				<ol start="7">
				<li><strong> Disposi&ccedil;&otilde;es Gerais</strong></li>
				</ol>
				<p>7.1. Os USU&Aacute;RIOS poder&atilde;o se valer dos canais de &lsquo;Contato&rsquo; toda vez que presenciarem ou verificarem conte&uacute;do impr&oacute;prio no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp; seja ele notadamente il&iacute;cito ou contr&aacute;rio &agrave;s regras de&nbsp;uso&nbsp;aqui estipuladas.</p>
				<p>7.1.1. A den&uacute;ncia ser&aacute; sigilosa e preservar&aacute; a identidade do USU&Aacute;RIO.</p>
				<p>7.2. Os presentes&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o est&atilde;o sujeitos a constante melhoria e aprimoramento. Assim, a NEX VENDAS se reserva o direito de modific&aacute;-los a qualquer momento, conforme a finalidade do PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;ou conveni&ecirc;ncia de sua controladora, tal qual para adequa&ccedil;&atilde;o e conformidade legal de disposi&ccedil;&atilde;o de lei ou norma que tenha for&ccedil;a jur&iacute;dica equivalente, cabendo ao USU&Aacute;RIO verific&aacute;-la sempre que efetuar o acesso ao PORTAL&nbsp;NEX IM&Oacute;VEL.</p>
				<p>7.3. Ao navegar pelo PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;e utilizar suas funcionalidades, o USU&Aacute;RIO aceita guiar-se pelos&nbsp;Termos&nbsp;deUso&nbsp;e Condi&ccedil;&otilde;es de Navega&ccedil;&atilde;o e pela Pol&iacute;tica de Privacidade que se encontram vigentes na data de acesso. Por isso, &eacute; recomend&aacute;vel que o USU&Aacute;RIO se mantenha atualizado.</p>
				<p>7.4. A toler&acirc;ncia do eventual descumprimento de quaisquer das cl&aacute;usulas e condi&ccedil;&otilde;es do presente instrumento n&atilde;o constituir&aacute; nova&ccedil;&atilde;o das obriga&ccedil;&otilde;es aqui estipuladas e tampouco impedir&aacute; ou inibir&aacute; a exigibilidade das mesmas a qualquer tempo.</p>
				<p>7.5. Caso alguma disposi&ccedil;&atilde;o destes&nbsp;Termos&nbsp;de&nbsp;Uso&nbsp;ou da Pol&iacute;tica de Privacidade publicadas no PORTAL&nbsp;NEX IM&Oacute;VEL&nbsp;for julgada inaplic&aacute;vel ou sem efeito, o restante de ambos os documentos continuam a viger, sem a necessidade de medida judicial que declare tal assertiva.</p>
				<p>7.6. Este Portal tem como base a data e hor&aacute;rios oficiais de Bras&iacute;lia.</p>
				<ol start="8">
				<li><strong> Lei aplic&aacute;vel e Jurisdi&ccedil;&atilde;o</strong></li>
				</ol>
				<p>8.1. Os&nbsp;Termos&nbsp;e Condi&ccedil;&otilde;es de&nbsp;Uso&nbsp;aqui descritos s&atilde;o interpretados segundo a legisla&ccedil;&atilde;o brasileira, no idioma portugu&ecirc;s, sendo eleito o Foro da Comarca de Porto Alegre, no Rio Grande do Sul para dirimir qualquer lit&iacute;gio ou controv&eacute;rsia envolvendo o presente documento, salvo ressalva espec&iacute;fica de compet&ecirc;ncia pessoal, territorial ou funcional pela legisla&ccedil;&atilde;o aplic&aacute;vel.</p>
				<ol start="9">
				<li><strong> Gloss&aacute;rio</strong></li>
				</ol>
				<p>9.1. Para os fins deste documento, devem se considerar as seguintes defini&ccedil;&otilde;es e descri&ccedil;&otilde;es para seu melhor entendimento:</p>
				<ol type="a">
					<li>Anti-Spam: Sistema que evita correspond&ecirc;ncias n&atilde;o desejadas, como publicidade em massa, pelo bloqueio de mensagens ou as movendo para pasta espec&iacute;fica.</li>
					<li>Aplica&ccedil;&otilde;es de Internet: Conjunto de funcionalidades que podem ser acessadas por meio de um terminal conectado &agrave; internet.</li>
					<li>Crawler/Spider: Programas desenvolvidos para atuar ou obter informa&ccedil;&otilde;es de modo automatizado em Sites.</li>
					<li>Dados Cadastrais: Conjunto de informa&ccedil;&otilde;es pessoais de um USU&Aacute;RIO de modo que seja poss&iacute;vel identific&aacute;-lo, como, por exemplo, para pessoas f&iacute;sicas o n&uacute;mero do documento de identidade (RG ou RNE), cadastro de pessoa f&iacute;sica (CPF), endere&ccedil;o residencial ou comercial e nome completo e, para pessoas jur&iacute;dicas, o n&uacute;mero de inscri&ccedil;&atilde;o de Pessoa Jur&iacute;dica na Receita Federal do Brasil (CNPJ), Raz&atilde;o Social, endere&ccedil;o de sede, filiais, dentre outros.</li>
					<li>NEX VENDAS NEGOCIOS IMOBILIARIOS LTDA., pessoa jur&iacute;dica de direito privado inscrita no CNPJ sob o n&ordm; 18.226.104/0001-84, com sede na Rua Furriel Luis Ant&ocirc;nio Vargas, 250 salas 503, CEP 90470-130, em Porto Alegre/RS.</li>
					<li>Layout: Conjunto compreendido entre apar&ecirc;ncia, design e fluxos do PORTAL NEX IM&Oacute;VEL.</li>
					<li>Link: Terminologia para endere&ccedil;o de internet.</li>
					<li>PORTAL NEX IM&Oacute;VEL: Designa o Site direcionado pelo dom&iacute;nio&nbsp;<a href="http://www.gafisa.com.br/">www.neximovel.com.br.</a></li>
					<li>Site: Denomina&ccedil;&atilde;o para p&aacute;gina de internet.</li>
					<li>USU&Aacute;RIO: Pessoa que acessa ou interage com as atividades oferecidas pelo PORTAL NEX IM&Oacute;VEL.</li>
					<li>Spider: Programa desenvolvido para obter informa&ccedil;&otilde;es de modo automatizado na internet, navegando pela web (teia) como se fosse uma spider (aranha).</li>
					<li>Webmastering: Compreende a cria&ccedil;&atilde;o, programa&ccedil;&atilde;o, desenvolvimento, controle e disponibilidade de p&aacute;ginas de internet.</li>
				</ol>
				
			</div>
		</div>
	</div>
	<div class="spacer"></div>
<section>