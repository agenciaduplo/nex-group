<div class="col-md-4 col-sm-6 col-xs-12 im-item ">
<?php if(!$p->sold){ ?>
<a href="<?php echo URL::base(TRUE) ?>compromisso/<?php echo $p->sale.$p->id.'/'.Controller_Application::friendly_seo_string($p->title); ?>" class="ajaxload">
<?php } ?>
	<div class="col-md-6 col-sm-6 col-xs-6 no-padding relative">
		
			<?php if($p->sold){ ?>
			<div class="im-sold" ></div>
			<?php } else if($p->highlight){ ?>
			<div class="im-highlight" ></div>
			<?php } ?>
			<?php if(!empty($p->medias->order_by('order')->find()->thumb_200)){ ?>
			<img class="w100 <?php echo ($p->sold) ? 'sold' : ''; ?>" src="<?php echo $p->medias->order_by('order')->find()->thumb_200; ?>?ver=0.1">
			<?php } ?>
			<div class="im-lupa" ></div>
		
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6 relative">
		<div class="im-title osbold"><?php echo $p->title; ?></div>
		<div class="im-old-price osnormal"><?php echo $p->old_value > 0 ? '<span>'.number_format($p->old_value, 2, ',', '.').'</span>' : '&nbsp;'; ?></div>
		<div class="im-price ossemibold"><!--<span class="cipher osnormal">R$ </span><?php echo number_format($p->value, 2, ',', '.'); ?>--></div>
		<div class="im-line-1 oslight"><?php echo $p->type->name; ?> | <?php echo $p->area; ?> m&sup2;</div>
		<div class="im-line-2 oslight"><?php echo $p->city->name; ?></div>
		<?php if($p->beds > 0){ ?>
		<div class="im-line-3 oslight"><?php echo $p->beds > 1 ? $p->beds.' dormit&oacute;rios' :  $p->beds.' dormit&oacute;rio'; ?></div>
		<?php } ?>
		
	</div>
<?php if(!$p->sold){ ?>
	</a>
<?php } ?>
	<?php if($p->splash_id > 0){ 
				if(isset($_GET['splash']))
				{
					$splash = $_GET['splash'];
					
				} else if(!empty(Cookie::get('splash'))) {
					
					$splash = Cookie::get('splash');
					
				} else {
					$splash = 'sp_cont_1';
				}
		?>
		<div class="<?php echo $splash; ?>"><?php echo $p->splash->title; ?></div>
		<?php } ?>
</div>