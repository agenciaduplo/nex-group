 <section class="conceitual">
	<div class="spacer"></div>
	</section>
<div class="spacer"></div>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
	<div class="row">
		<div class="spacer"></div>
			<div class="tcenter">
			<?php if($banner->loaded()){?>
				<a href="<?php echo $banner->link; ?>" target="_blank">
					<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
				</a>
			<?php } ?>	
			</div>
		<div class="spacer"></div>
	</div>
	</div>
</section>

<section class="im-items">
	<div class="container">
		<div class="">
			<h1 class="title ossemibold p15-mobile fs25">É MAIS QUE PREÇO. É MAIS QUE CONDIÇÕES EXCLUSIVAS. É COMPROMISSO COM O MELHOR NEGÓCIO.</h1>
		</div>
		<div class="spacer"></div>
		<div class="row widgets">
			<?php 
				foreach($properties as $k => $p){ 
					echo View::factory('compromisso/widget_item')->set('k', $k)->set('p', $p);
				} 
			?>
		</div>
		<div class="spacer"></div>
		<div class="spacer hide-mobile"></div>
		
		<div class="spacer hide-mobile"></div>
	</div>
</section>
<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>