<?php $search_query = json_decode($the_query); ?> 
<section class="breadcrumb-sec graybg">
<div class="container">
  <ul class="breadcrumb graybg no-padding-left no-margin">
    <li>
      <a href="<?php echo URL::base(TRUE) ?>">In&iacute;cio</a>
    </li>
	<li>
      Busca
    </li>
  </ul>
</div>
</section>
<section class="single-page-header graybg">
	<div class="container searchform">
		<!-------------------------------BUSCA--------------------------------->
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal hide-mobile ajaxsubmit', 'enctype' => 'multipart/form-data')) ?>
			<div class="row">
				<div class="col-md-12 no-padding-mobile">
				
				
					<!-- TIPO DE IMOVEL -->
					<div class="col-md-6">
						<div class="row"><h4 class="redcolor ossemibold">TIPO DE IM&Oacute;VEL</h4></div>
						<div class="row">
							<?php foreach($types as $k => $t){ ?>
							<label>
								<?php echo Form::checkbox('types[]', $k, in_array($k, (array) @$search_query->types), array('class' => '')); ?>
								<span class="types"><?php echo $t; ?></span>
							</label>						
							<?php } ?>						
						</div>
					</div>
					
					<!-- LOCALIZACAO -->
					<div class="col-md-6 no-padding">
						<h4 class="redcolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', @$search_query->location, array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false', 'autocomplete' => 'off')); ?>
						<ul class="dropdown-menu location-dropdown">
							<li>
								<p class="search-tip">
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
								<nav class="live-search" style="display:none">
										<?php foreach($live as $v){ ?>
										<p class="no-margin"><?php echo $v; ?></p>
										<?php } ?>
								</nav>
								
							</li> 
						</ul>
					</div>
				
					<div class="spacer"></div>
					
					<!-- DORMITORIOS -->
					<div class="col-md-2 col-sm-6 beds icons mb10">
						<div class="row">
							<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-beds-red.png">&nbsp;&nbsp;&nbsp;Dormit&oacute;rios</h5>
						</div>
						<div class="row">
							<?php foreach($beds as $k => $t){ ?>
							<label>
								<?php echo Form::checkbox('beds[]', $k+1, in_array($k+1,  (array) @$search_query->beds), array('class' => '')); ?>
								<span class=" "><?php echo $t; ?></span>
							</label>						
							<?php } ?>						
						</div>
					</div>
					
					<!-- SUITE -->
					<div class="col-md-2 col-sm-6 suite icons mb10">
						<div class="row">
							<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-suite-red.png">&nbsp;&nbsp;&nbsp;Su&iacute;tes</h5>
						</div>
						<div class="row">
							<?php foreach($suite as $k => $t){ ?>
							<label>
								<?php echo Form::checkbox('suite[]', $k+1, in_array($k+1,  (array) @$search_query->suite), array('class' => '')); ?>
								<span class=" "><?php echo $t; ?></span>
							</label>						
							<?php } ?>						
						</div>
					</div>
					
					<!-- VAGAS -->
					<div class="col-md-2 col-sm-6 parking icons mb10">
						<div class="row">
							<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-parking-red.png">&nbsp;&nbsp;&nbsp;Vagas</h5>
						</div>
						<div class="row">
							<?php foreach($parking as $k => $t){ ?>
							<label>
								<?php echo Form::checkbox('parking[]', $k+1, in_array($k+1,  (array) @$search_query->parking), array('class' => '')); ?>
								<span class=" "><?php echo $t; ?></span>
							</label>						
							<?php } ?>						
						</div>
					</div>
				
					<!-- AREA -->
					<div class="col-md-2 col-sm-6 col-xs-12 area icons btn-group mb10 no-padding">
						
							<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-area-red.png">&nbsp;&nbsp;&nbsp;&Aacute;rea</h5>
						
						<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
						
							<?php echo Form::input('area', null, array('id' => 'area_input', 'type' => 'hidden')); ?>
							<?php echo Form::button('area_btn', '', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
								<span class="caret"></span> 
								<span class="sr-only">Toggle Dropdown</span> 
							</button>
							<ul class="dropdown-menu keepopen rangemenu">
								<li>
									<div class="col-md-6 no-padding min-side radioset">
										<h5 class="text-center ossemibold">DE</h5>
										<?php foreach($areamin as $k => $t){ ?>
										<label>
											<?php 
												$checkmin = $k == @$search_query->areamin ? true : false;
												echo Form::radio('areamin', $k, $checkmin, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
									<div class="col-md-6 no-padding max-side radioset">
										<h5 class="text-center ossemibold">AT&Eacute;</h5>
										<?php foreach($areamax as $k => $t){ ?>
										<label>
											<?php 
												$checkmax = $k == @$search_query->areamax ? true : false;
												echo Form::radio('areamax', $k, $checkmax, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
								</li> 
							</ul>
					</div>
					<!-- PRICE -->
					<div class="col-md-2 col-sm-6 col-xs-12 price icons btn-group mb10 no-padding">
							
							<h5 class="redcolor ossemibold"><img src="<?php echo URL::base(TRUE) ?>public/img/icon-price-red.png">&nbsp;&nbsp;&nbsp;Faixa de Pre&ccedil;o</h5>
							
							<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
							<?php echo Form::button('price_btn', '', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input redcolor osregular', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
								<span class="caret"></span> 
								<span class="sr-only">Toggle Dropdown</span> 
							</button>
							<ul class="dropdown-menu keepopen rangemenu">
								<li>
									<div class="col-md-6 no-padding min-side radioset">
										<h5 class="text-center ossemibold">DE</h5>
										<?php foreach($pricemin as $k => $t){ ?>
										<label>
											<?php 
												$checkmin = $k == @$search_query->pricemin ? true : false;
												echo Form::radio('pricemin', $k, $checkmin, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
									<div class="col-md-6 no-padding max-side radioset">
										<h5 class="text-center ossemibold">AT&Eacute;</h5>
										
									<?php foreach($pricemax as $k => $t){ ?>
										<label>
											<?php 
												$checkmax = $k == @$search_query->pricemax ? true : false;
												echo Form::radio('pricemax', $k, $checkmax, array('class' => '')); 
												?>
											<span class=" "><?php echo $t; ?></span>
										</label>
										<?php } ?>
									</div>
								</li> 
							</ul>
					</div>
					<div class="col-md-2 col-sm-6 mb10 no-padding"><h5 class="whitecolor ossemibold" style="line-height:18px;">&nbsp;</h5><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
				
					<div class="col-md-2 col-sm-12 pull-right">
						<div class="row">
							
							<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search', 'style' => 'height:50px;')); ?>
						</div>
					</div>
				
			</div>
			
				<?php echo Form::input('load', 'ajax', array('type' => 'hidden')); ?>
			</div>
		<?php echo Form::close() ?>		
		
		
		<!-------------------------------BUSCA--------------------------------->
	
	
		<h3 class="redcolor osbold upper hide show-mobile">Busca</h3>
		<h4 class="ossemibold graycolor mb20 hide show-mobile">Revise e compare seus imóveis preferidos</h4>
		<div class="spacer"></div>
		<div class="controls semilightredbg left w100">
			<div class="spacer"></div>
			<div class="col-lg-4 hide-mobile hide-tablet">
				<ul class="inline-list no-margin">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Exibir como</span></li>
					<li id="radio">
						<input type="radio" id="radio1" name="radio" value="list" checked="checked"><label for="radio1" class="whitecolor"><i class="glyphicon glyphicon-list whitecolor"></i> Lista</label>
						<input type="radio" id="radio2" name="radio" value="grid"><label for="radio2" class="whitecolor"><i class="glyphicon glyphicon-th-large whitecolor"></i> Grade</label>
					</li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-3 col-xs-6 whitecolor tcenter hide-mobile">
				<strong class="osbold fs25"><?php echo $count; ?></strong>
				<span>imóveis encontrados</span>
			</div> 
			<div class="col-lg-4 col-md-4 col-xs-6 whitecolor tcenter hide show-tablet hide-mobile"></div>
			<div class="col-lg-4 col-md-5 col-xs-12">
				<ul class="inline-list no-margin right">
					<li class="ctrlpad"><span class="whitecolor ossemibold">Ordernar por</span></li>
					<li class="ctrlpad">
						<?php echo Form::open('busca/index', array('id' => 'orderform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
							<?php echo Form::select('order', $order, @$post_order, array('id' => 'ordersearch', 'class' => 'sel-input')); ?>
							<?php echo Form::input('load', 'ajax', array('type' => 'hidden')); ?>
						<?php echo Form::close() ?>	
					</li>
				</ul>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
</section>
<div class="spacer"></div>
<section class="lazyloader">
	<div class="container">
		<div class="results">
		<?php 
			foreach($properties as $p){
				echo View::factory('search/list_item')->set('p', $p);
				echo View::factory('search/grid_item')->set('p', $p);
			} 
		?>
		</div>
		<div class="spacer">
		<div class="col-md-12 tcenter">
			<?php if ($count > 6){ ?>
				<img class="loader hide" src="<?php echo URL::base(TRUE) ?>public/img/ajax-loader-7.gif" title="Loader">
				<!--<div class="view-more-search hide">VISUALIZAR MAIS IM&Oacute;VEIS</div>-->
			<?php } ?>
			</div>
		</div>
	</div>
</section>
<section class="sugestions">
	<div class="container">
	
	<?php if(@$search['types'] == 5 || @$search['types'] == 6) { ?>
		
		<?php if(isset($samearea) && count($samearea) > 0) { ?>
		<div class="row">
			<h3 class="title ossemibold">Mesma area</h3>
		</div>
		<div class="row">
		<?php 
			foreach($samearea as $p){
				echo View::factory('home/widget_item')->set('p', $p);
			} 
		?>
		</div>
		<?php } ?>
	
	
	<?php } else { ?>
		
		<?php if(count($samelocation) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Mesma localização</h3>
			</div>
			<div class="row">
			<?php 
				foreach($samelocation as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
		
		<?php if(isset($samebeds) && count($samebeds) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Outros imóveis semelhantes</h3>
			</div>
			<div class="row">
			<?php 
				foreach($samebeds as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
		
		<?php if(isset($sameprice) && count($sameprice) > 0) { ?>
			<div class="row">
				<h3 class="title ossemibold">Mesma faixa de preço</h3>
			</div>
			<div class="row">
			<?php 
				foreach($sameprice as $p){
					echo View::factory('home/widget_item')->set('p', $p);
				} 
			?>
			</div>
		<?php } ?>
	<?php } ?>
	
	<?php if(isset($others) && count($others) > 0 && ($search['types'] == 5 || $search['types'] == 6 || ( count($samelocation) == 0  && count($samebeds) == 0 && count($sameprice) == 0 ))) { ?>
		<div class="">
			<h3 class="title ossemibold">Outras sugestões</h3>
		</div>
		<div class="row">
		<?php 
			foreach($others as $p){
				echo View::factory('home/widget_item')->set('p', $p);
			} 
		?>
		</div>
	<?php } ?>
	</div>
</section>

<?php if(isset($banner) && !empty($banner)) { ?>
<div class="spacer"></div>
<section class="banner">
	<div class="container">
		<div class="spacer"></div>
		<div class="tcenter">
			<a href="<?php echo $banner->link; ?>" target="_blank">
				<img src="<?php echo $banner->medias->find()->path; ?>" class="imgmax100">
			</a>
		</div>
		<div class="spacer"></div>
	</div>
</section>
<?php } ?>
<section class="single-page-header graybg hide show-mobile">
	<div class="container searchform">
		<!-------------------------------BUSCA--------------------------------->
		 <?php echo Form::open('busca/index', array('id' => 'searchform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
			<div class="col-md-10">
				
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">TIPO DE IM&Oacute;VEL</h4>
						<?php echo Form::select('types', $types, @$search['types'], array('id' => 'types', 'class' => 'text-input',  'data-rel' => 'chosen')); ?>
					</div>
					<div class="col-md-6">
						<h4 class="greencolor ossemibold">LOCALIZA&Ccedil;&Atilde;O</h4>
						<?php echo Form::input('location', @$search['location'], array('id' => 'location', 'class' => 'text-input dropdown-toggle', 'placeholder' => 'Digite bairro ou cidade', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false', 'required' => 'required')); ?>
						<ul class="dropdown-menu">
							<li>
								<p>
									Digite bairro ou cidade<br>
									Ex: Centro Histórico Porto Alegre
								</p>
							</li> 
						</ul>
					</div>
				
				<div class="spacer"></div>
				<div class="advanced">
					
						<div class="col-md-4 col-sm-6 beds icons mb10"><?php echo Form::select('beds', $beds, @$search['beds'], array('id' => 'beds', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 suite icons mb10"><?php echo Form::select('suite', $suite, @$search['suite'], array('id' => 'suite', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
						<div class="col-md-4 col-sm-6 parking icons mb10"><?php echo Form::select('parking', $parking, @$search['parking'], array('id' => 'parking', 'class' => 'text-input',  'data-rel' => 'chosen')); ?></div>
					
					
						
						<div class="col-md-4 col-sm-6 area icons btn-group mb10">
							<!-- AREA -->
							<?php //echo Form::input('area', null, array('id' => 'types', 'class' => 'text-input',  'type' => 'number', 'placeholder' => 'Área')); ?>
							
								<?php echo Form::input('area', @$search['area'], array('id' => 'area', 'type' => 'hidden')); ?>
								<?php echo Form::button('area_btn', 'Área', array('id' => 'area_btn', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount-area">Área:</label>
										  <input type="text" id="amount-area" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-area-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 price icons btn-group mb10">
							<!-- PRICE -->
								<?php echo Form::input('price', null, array('id' => 'price_input', 'type' => 'hidden')); ?>
								<?php echo Form::button('price_btn', 'Faixa de preço', array('id' => 'price', 'type' => 'button', 'class' => 'btn btn-default text-input', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false')); ?>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
									<span class="caret"></span> 
									<span class="sr-only">Toggle Dropdown</span> 
								</button>
								<ul class="dropdown-menu">
									<li>
										<p>
										  <label for="amount">Faixa de pre&ccedil;o:</label>
										  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
										</p>
										<div id="slider-range"></div>
									</li> 
								</ul>
						</div>
						<div class="col-md-4 col-sm-6 mb10"><?php echo Form::input('code', null, array('id' => 'code', 'class' => 'text-input email', 'placeholder' => 'Digite o código do imóvel')); ?></div>
					
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<h4 class="whitecolor ossemibold">&nbsp;</h4>
					<?php echo Form::button('save', 'BUSCAR', array('type' => 'submit', 'class' => 'btn btn-search')); ?>
				</div>
			</div>
		<?php echo Form::close() ?>		
	</div>
</section>
<script>
	search_query = '<?php echo json_encode($search); ?>';
	search_order = '<?php echo json_encode($post_order); ?>';
</script>
<?php echo Form::input('pagetitle', @$pagetitle, array('id' => 'pagetitle', 'type' => 'hidden')); ?>