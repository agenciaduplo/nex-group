<?php
return array(
		'trusted_hosts' => array(
		'localhost',
		'192.168.0.54',
		'neximovel.com.br',
		'neximovel.tempsite.ws',
		'neximovel.hospedagemdesites.ws',
		'neximovel.websiteseguro.com',
		'www.neximovel.com.br'
    ),
);
?>