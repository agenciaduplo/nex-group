$(document).ready(function(){
	
	google.maps.event.addDomListener(window, 'load', initialize());
	var map;
	var infowindow;
	
	function initialize(force) {
		if (document.getElementById('map-canvas') == null) return;
        
        $gmap = $('#map-canvas');
		var mapOptions = {
			zoom: 15,
			scrollwheel: false,
			//center: new google.maps.LatLng( $gmap.data( 'lat' ), $gmap.data( 'long' ) ),
			center: new google.maps.LatLng( '-30.0227431', '-51.1625598' ),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			panControl: false,
			zoomControl: true,
			scaleControl: false,

			streetViewControl: false,
			overviewMapControl: false,
			//disableDefaultUI: true
			
		}
		
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var myLatLng = new google.maps.LatLng( $gmap.data( 'lat' ), $gmap.data( 'long' ) );
		
		var address = $('#address').val();
		var state = $("#state option:selected").text();
		var city = $("#city_id option:selected").text();
		var lat = $('#lat').val();
		var lng = $('#lng').val();

		
		if(force) {
			latLng = new google.maps.LatLng( lat, lng );
			desc = '<strong></strong><br />';
			createMarker(desc, latLng);
			map.setZoom(15);
			map.setCenter(new google.maps.LatLng( lat, lng ));
			
		} else if(address){
			
			if(city != 'Selecione o estado') {
				address = address+' '+city+' '+state;
			}
			
			jQuery.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+address, function(data){
				//console.log(data);
				if(data.status != 'ZERO_RESULTS'){
					
					glat = data.results[0].geometry.location.lat;
					glng = data.results[0].geometry.location.lng;
					
					latLng = new google.maps.LatLng( glat, glng );
					desc = '<strong></strong><br />';
					createMarker(desc, latLng);
					map.setZoom(15);
					map.setCenter(new google.maps.LatLng( glat, glng ));
					
					$('#lat').val(glat);
					$('#lng').val(glng);
					//markers[i] = new google.maps.Marker({position: latLng});
				}
			});
		}
		
	}
	
	//PIN
	function createMarker(desc, latlng){

		var marker = new google.maps.Marker({position: latlng, map: map/*,icon: '/wp-content/themes/basewd/images/pin.png'*/});

		google.maps.event.addListener(marker, "click", function(){

		  if(infowindow)
			 infowindow.close();

			infowindow = new google.maps.InfoWindow({content: desc});

			infowindow.open(map, marker);

		});

		return marker;
	}
	
	$('#force_location').click(function(e){
		e.preventDefault();
		initialize(true);
	});
	
	$('#city_id').change(function(){
		initialize(false);
	});
	
	$('#address').on('blur', function(event){
		initialize(false);	
	});
	
	$('#cep').on('blur', function(event){
		var cep = $(this).val().replace('-', '');
		
		$.getJSON('https://viacep.com.br/ws/' + cep + '/json/', function(data){
			$('#address').val(data.logradouro);
			$('#neighborhood').val(data.bairro);
		
			var city = data.localidade;
		
			$.getJSON(base_url+'manager/ajax/get_state/'+data.uf, function(data){
				console.log(data);
				$('#state').val(data.id);
				
				$.post(base_url+'manager/ajax/get_cities',{state:data.id},function(data,status){
					var option = new Option('Selecione a cidade');
					$('#city_id').html(option);
					for (var i in data){
						//console.log(i);
						var option = new Option(data[i], i);
						$('#city_id').append(option);
					}
					$('#city_id').val($('#city_id option:contains('+city+')').val());
					
				}, 'json');
			});
			//$('#state').val();
			//$('#address').val(data.logradouro);
			
			initialize(false);
		});
	});
	
	
});