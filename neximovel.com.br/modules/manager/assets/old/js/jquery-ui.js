//variaveis
$(document).ready(function(){
	//Carrega menu
	$.ajax({
		url: 'manager/ajax/menu',
		type: "GET",
		dataType:'html',
		success: function(data){
			$("#myTab").html(data);
		},
		error:function(request,error,data){
			alert("Erro: " + data);
		}
	}).done(function(){
		$('#myTab a:first').tab('show');
		$('#myTab a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		});	
		$("#myTabContent .tab-pane:eq(0)").addClass("active");
	});
	
	//Carrega Widgets
	$.ajax({
		url: 'manager/ajax/widgets',
		type: "GET",
		dataType:'html',
		success: function(data){
			$("#myTabContent").html(data);
		},
		error:function(request,error,data){
			alert("Erro: " + data);
		}
	}).done(function(){
		$('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});	
	});
	
});
