//variaveis
$(document).ready(function(){
	//Carrega menu
	$.ajax({
		url: urlPath+'manager/ajax/menu',
		type: "GET",
		dataType:'html',
		success: function(data){
			$("#myTab").html(data);
		},
		error:function(request,error,data){
			alert("Erro menu: " + data);
		}
	}).done(function(){

		$.ajax({
			url: urlPath+'manager/ajax/widgets',
			type: "GET",
			dataType:'html',
			success: function(data){
				$("#tabsPagebuilder").append(data);
			},
			error:function(request,error,data){
				console.log(data);
				alert("Erro widgets: " + data);
			}
		}).done(function(){
			funcoesPadroes();
			tabsPagebuilder();
		});

	});
	//Carrega Widgets
	
});
