﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

function microtime(get_as_float) {
  var unixtime_ms = new Date().getTime();
  var sec = parseInt(unixtime_ms / 1000);
  return get_as_float ? (unixtime_ms / 1000) : (unixtime_ms - (sec * 1000)) / 1000 + ' ' + sec;
}

CKEDITOR.editorConfig = function(config) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';	
  config.filebrowserBrowseUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/browse.php?type=all&v=' + microtime();
  config.filebrowserImageBrowseUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/browse.php?type=images&v=' + microtime();
  config.filebrowserFlashBrowseUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/browse.php?type=flash&v=' + microtime();
  config.filebrowserUploadUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/upload.php?type=all&v=' + microtime();
  config.filebrowserImageUploadUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/upload.php?type=images&v=' + microtime();
  config.filebrowserFlashUploadUrl = '/PHP/desenvolvimento/la_salle/2013/ies/public/kcfinder/upload.php?type=flash&v=' + microtime();
  config.enterMode = CKEDITOR.ENTER_P;
  /*
  config.toolbar = [
    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo', 'Image', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'TextColor', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Link', 'Unlink', 'Anchor', 'Font', 'FontSize', 'Format', 'HorizontalRule']
  ];
  */

  config.toolbar = [
    { name: 'barra1', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo', 'Image', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'TextColor', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
    '/',
    { name: 'barra2', items: ['Link', 'Unlink', 'Anchor', 'Font', 'FontSize', 'Format', 'HorizontalRule', 'Table', 'Iframe']}
  ];

};