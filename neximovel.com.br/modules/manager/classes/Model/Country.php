<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Country extends ORM {
	protected $_has_many = array('states' => array('model' => 'State', 'foreign_key' => 'country_id'));
}