<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Splash extends ORM {

	//protected $_has_many = array('properties' => array('model' => 'Property', 'through' => 'properties_splashes'));
	
	public function create_splash($values)
	{
		return $this->values($values)->create();
	}

	public function update_splash($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_splash()
	{
		return $this->delete();
	}
} // End Splash Model