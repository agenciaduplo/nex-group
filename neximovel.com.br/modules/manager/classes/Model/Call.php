<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Call extends ORM {

	//protected $_belongs_to = array('campaign' => array('model' => 'Campaign', 'foreign_key' => 'campaign_id'));
	//protected $_has_many = array('medias' => array('model' => 'Media', 'through' => 'banners_medias'));
	
	
	public function create_call($values)
	{
		return $this->values($values)->create();
	}

	public function update_call($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_call()
	{
		return $this->delete();
	}
} // End Banner Model