<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Ctype extends ORM {

	protected $_has_many = array('campaigns' => array('model' => 'Campaign', 'foreign_key' => 'ctype_id'));

}