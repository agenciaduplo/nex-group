<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Contact extends ORM {

	public function create_contact($values)
	{
		return $this->values($values)->create();
	}

	public function update_contact($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_contact()
	{
		return $this->delete();
	}
} // End Banner Model