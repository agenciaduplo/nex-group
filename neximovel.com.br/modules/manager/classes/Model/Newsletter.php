<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Newsletter extends ORM {

	//protected $_belongs_to = array('campaign' => array('model' => 'Campaign', 'foreign_key' => 'campaign_id'));
	//protected $_has_many = array('medias' => array('model' => 'Media', 'through' => 'banners_medias'));
	
	
	public function create_newsletter($values)
	{
		return $this->values($values)->create();
	}

	public function update_newsletter($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_newletter()
	{
		return $this->delete();
	}
} // End Banner Model