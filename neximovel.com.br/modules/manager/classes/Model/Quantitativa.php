<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Quantitativa extends ORM {
	public function create_quantitativa($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_quantitativa($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}	
}