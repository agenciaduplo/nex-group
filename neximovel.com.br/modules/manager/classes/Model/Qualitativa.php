<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Qualitativa extends ORM {

	public function create_qualitativa($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_qualitativa($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}	
}
