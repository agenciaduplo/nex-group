<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Log extends ORM {

	//protected $_belongs_to = array('campaign' => array('model' => 'Campaign', 'foreign_key' => 'campaign_id'));

	public function create_log($values)
	{
		return $this->values($values)->create();
	}

	public function update_log($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Book Model