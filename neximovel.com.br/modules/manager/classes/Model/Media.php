<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Media extends ORM {

	protected $_has_many = array('properties' => array('model' => 'Property', 'through' => 'properties_medias'),
								 'banners' => array('model' => 'Banner', 'through' => 'banner_medias'));
	
	
} // End Auth User Model