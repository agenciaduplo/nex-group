<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Campaign extends ORM {

	protected $_belongs_to = array('ctype' => array('model' => 'Ctype', 'foreign_key' => 'ctype_id'),
								   'region' => array('model' => 'Region', 'foreign_key' => 'region_id'));

	protected $_has_many = array('books' => array('model' => 'Book', 'foreign_key' => 'campaign_id'));
								   
	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'name'         => 'Nome',
		);
	}

	public function create_campaign($values)
	{
		return $this->values($values)->create();
	}

	public function update_campaign($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}

	public function delete_campaign()
	{
		return $this->delete();
	}
	
} // End Faq Model
