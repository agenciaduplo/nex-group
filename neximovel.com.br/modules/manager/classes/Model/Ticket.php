<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Ticket extends ORM {

	protected $_belongs_to = array('dn' => array('model' => 'Dn', 'foreign_key' => 'dn_id'),
									'owner' => array('model' => 'User', 'foreign_key' => 'user_id'),
									'book' => array('model' => 'Book', 'foreign_key' => 'book_id')
							);
							
	public function create_ticket($values)
	{
		return $this->values($values)->create();
	}

	public function update_ticket($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Book Model