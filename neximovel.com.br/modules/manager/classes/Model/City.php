<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_City extends ORM {
	protected $_belongs_to = array('state' => array('model' => 'State', 'foreign_key' => 'state_id'));
	protected $_has_many = array('properties' => array('model' => 'Property', 'foreign_key' => 'city_id'));
}