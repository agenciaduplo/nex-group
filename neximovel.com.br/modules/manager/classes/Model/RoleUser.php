<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_RoleUser extends Model_Auth_User {
	protected $_table_name = 'roles_users';
	
	protected $_belongs_to = array('role' => array('model' => 'Role', 'foreign_key' => 'role_id'), 
								   'user' => array('model' => 'User', 'foreign_key' => 'user_id'));

} // End Auth User Model
