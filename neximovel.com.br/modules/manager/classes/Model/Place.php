<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Place extends ORM {

	//protected $_belongs_to = array('campaign' => array('model' => 'Campaign', 'foreign_key' => 'campaign_id'));
	protected $_has_many = array('banners' => array('model' => 'Banner', 'through' => 'banners_places'),
								 'properties' => array('model' => 'Property', 'through' => 'properties_places'));
	
	
	public function create_place($values)
	{
		return $this->values($values)->create();
	}

	public function update_place($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_place()
	{
		return $this->delete();
	}
} // End place Model