<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_State extends ORM {
	protected $_belongs_to = array('country' => array('model' => 'Country', 'foreign_key' => 'country_id'));
	
	protected $_has_many = array('cities' => array('model' => 'City', 'foreign_key' => 'state_id'));
}