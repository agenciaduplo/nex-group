<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Bcategory extends ORM {

	protected $_has_many = array('books' => array('model' => 'Book', 'foreign_key' => 'bcategory_id'));

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'name'         => 'Nome',
		);
	}

	public function create_bcategory($values)
	{
		return $this->values($values)->create();
	}

	public function update_bcategory($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Faq Model
