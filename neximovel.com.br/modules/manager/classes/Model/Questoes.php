<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Questoes extends ORM {
	protected $_table_name = 'questoes';

	public function create_questoes($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_questoes($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
}
