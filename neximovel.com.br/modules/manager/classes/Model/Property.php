<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Property extends ORM {
	
	
	
	protected $_has_many = array('medias' => array('model' => 'Media', 'through' => 'properties_medias'),
								 'users' => array('model' => 'User', 'through' => 'properties_users'),
								 'banners' => array('model' => 'Banner', 'through' => 'properties_banners'));
	
	protected $_belongs_to = array('city' => array('model' => 'City', 'foreign_key' => 'city_id'),
								   'splash' => array('model' => 'Splash', 'foreign_key' => 'splash_id'),
								   'type' => array('model' => 'Type', 'foreign_key' => 'type_id'));

								   
	public function create_property($values)
	{
		return $this->values($values)->create();
	}

	public function update_property($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function get_related()
	{
		if ($this->_loaded){
			
			if(!empty($this->related)){
				
				$arr = array_filter(json_decode($this->related));
				
				foreach($arr as $k => $v){
					$arr[$k] = substr($v, 2);
				}
				
				if(!empty($arr)){
					$related_ids = implode(',', $arr);
				} else {
					$related_ids = '(0)';
				}
				
			} else {
				$related_ids = '(0)';
			}
			$related = ORM::factory('Property')
							->where('enabled', '=', 1)
							->and_where('sold', '=', 0)
							->and_where('id', '!=', $this->id)
							->and_where_open()
								->where('id', 'IN', DB::expr("($related_ids)"))
								->or_where('beds', '=', $this->beds)
							->and_where_close()
							//->order_by(DB::expr('RAND()'))
							->limit(3)
							->find_all();
		
			return $related;
		}
	}
	
}