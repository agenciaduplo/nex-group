<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Supplier extends ORM {

	protected $_has_many = array('tickets' => array('model' => 'Ticket', 'foreign_key' => 'supplier_id'));

	public function create_supplier($values)
	{
		return $this->values($values)->create();
	}

	public function update_supplier($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Book Model