<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Codigo extends ORM {
	public function create_codigo($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_codigo($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}		
}
