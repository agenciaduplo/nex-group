<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {

	protected $_has_many = array('properties' => array('model' => 'Property', 'through' => 'properties_users'),
								 'roles' => array('model' => 'Role', 'through' => 'roles_users'));

	public function rules()
	{

		//$rules = parent::rules();
		$rules['name'] = array(
			array('not_empty')
		);
                $rules['email'] = array(
                        array('not_empty')
                );
                $rules['username'] = array(
                        array('not_empty')
                );
	
		return $rules;
	}


	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'username'         => 'Usuário',
			'email'            => 'E-mail',
			'password'         => 'Senha',
		);
	}

	/**
	 * Create a new user
	 *
	 * Example usage:
	 * ~~~
	 * $user = ORM::factory('User')->create_user($_POST, array(
	 *	'username',
	 *	'password',
	 *	'email',
	 * );
	 * ~~~
	 *
	 * @param array $values
	 * @param array $expected
	 * @throws ORM_Validation_Exception
	 */
	public function create_user($values, $expected = NULL)
	{
		// Validation for passwords
		$extra_validation = Model_User::get_password_validation($values)
			->rule('password', 'not_empty');
		return $this->values($values, $expected)->create($extra_validation);
	}

	/**
	 * Update an existing user
	 *
	 * [!!] We make the assumption that if a user does not supply a password, that they do not wish to update their password.
	 *
	 * Example usage:
	 * ~~~
	 * $user = ORM::factory('User')
	 *	->where('username', '=', 'kiall')
	 *	->find()
	 *	->update_user($_POST, array(
	 *		'username',
	 *		'password',
	 *		'email',
	 *	);
	 * ~~~
	 *
	 * @param array $values
	 * @param array $expected
	 * @throws ORM_Validation_Exception
	 */
	public function update_user($values, $expected = NULL)
	{
		if (empty($values['password']))
			unset($values['password'], $values['password_confirm']);

		// Validation for passwords
		$extra_validation = Model_User::get_password_validation($values);

		return $this->values($values, $expected)->update($extra_validation);
	}

	public function is_unique($email)
	{
           return count (ORM::factory('User')->where('email','=',$email)->find_all());
	}
	
	/*public function dns(){
	
		return ORM::factory('Dn')
					->join('INNER', 'dns_users')
					->on('dns_users.dn_id', '=', 'dns.id')
					->where('dns_users.user_id', '=', DB::expr($this->id))
					->;
		
	}*/
} // End Auth User Model
