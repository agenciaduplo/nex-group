<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Troca extends ORM {
	public function create_troca($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_troca($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}		
}
