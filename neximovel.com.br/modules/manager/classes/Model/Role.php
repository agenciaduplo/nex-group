<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Role extends Model_Auth_User {

	protected $_has_many = array('users' => array('model' => 'User', 'through' => 'roles_users'));
	
	
} // End Auth User Model