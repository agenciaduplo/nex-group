<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Dn extends ORM {

	protected $_has_many = array('users' => array('model' => 'User', 'through' => 'dns_users'));
	
	protected $_belongs_to = array('region' => array('model' => 'Region', 'foreign_key' => 'region_id'),
									'state' => array('model' => 'State', 'foreign_key' => 'state_id'));

	public function create_dn($values, $expected = NULL)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_dn($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}

	public function delete_dn()
	{
           return $this->delete();
	}
	
} // End
