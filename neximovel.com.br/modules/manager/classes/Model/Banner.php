<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Banner extends ORM {

	//protected $_belongs_to = array('campaign' => array('model' => 'Campaign', 'foreign_key' => 'campaign_id'));
	protected $_has_many = array('medias' => array('model' => 'Media', 'through' => 'banners_medias'),
								'places' => array('model' => 'Place', 'through' => 'banners_places'),
								'pages' => array('model' => 'Page', 'through' => 'pages_banners'));
	
	
	public function create_banner($values)
	{
		return $this->values($values)->create();
	}

	public function update_banner($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_banner()
	{
		return $this->delete();
	}
} // End Banner Model