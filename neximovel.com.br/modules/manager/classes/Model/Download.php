<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Download extends ORM {

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'name'         => 'Nome',
		);
	}

	public function create_download($values)
	{
		return $this->values($values)->create();
	}

	public function update_download($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Faq Model
