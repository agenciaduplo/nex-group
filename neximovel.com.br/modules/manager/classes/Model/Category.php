<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Category extends ORM {

	protected $_has_many = array('news' => array('model' => 'New', 'foreign_key' => 'category_id'));

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'name'         => 'Nome',
		);
	}

	public function create_category($values)
	{
		return $this->values($values)->create();
	}

	public function update_category($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Faq Model
