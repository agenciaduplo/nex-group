<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_DnUser extends ORM {

	protected $_table_name = 'dns_users';
	
	protected $_belongs_to = array('dns' => array('model' => 'Dn', 'foreign_key' => 'dn_id'), 
								   'users' => array('model' => 'User', 'foreign_key' => 'user_id'));

	public function create_dn_user($values, $expected = NULL)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_dn_user($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	

} // End
