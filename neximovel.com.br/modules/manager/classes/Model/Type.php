<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Type extends ORM {

	protected $_has_many = array('properties' => array('model' => 'Property', 'foreign_key' => 'type_id'));

}