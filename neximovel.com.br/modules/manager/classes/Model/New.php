<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_New extends ORM {

	protected $_belongs_to = array('category' => array('model' => 'Category', 'foreign_key' => 'category_id'));

	/**
	 * Labels for fields in this model
	 *
	 * @return array Labels
	 */
	public function labels()
	{
		return array(
			'name'         => 'Nome',
		);
	}

	public function create_new($values)
	{
		return $this->values($values)->create();
	}

	public function update_new($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
} // End Faq Model
