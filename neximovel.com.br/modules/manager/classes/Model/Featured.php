<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Featured extends ORM {

	protected $_table_name = 'featured';
	
	protected $_has_many = array('medias' => array('model' => 'Media', 'through' => 'featured_medias'));
	
	
	public function create_featured($values)
	{
		return $this->values($values)->create();
	}

	public function update_featured($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}
	
	public function delete_featured()
	{
		return $this->delete();
	}
} // End Featured Model