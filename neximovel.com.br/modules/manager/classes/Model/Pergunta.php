<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Pergunta extends ORM {
	public function create_pergunta($values, $expected)
	{
		return $this->values($values, $expected)->create();
	}

	public function update_pergunta($values, $expected = NULL)
	{
		return $this->values($values, $expected)->update();
	}	
}