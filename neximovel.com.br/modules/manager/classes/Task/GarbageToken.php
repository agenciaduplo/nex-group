<?php defined('SYSPATH') or die('No direct script access.');
 
class Task_GarbageToken extends Minion_Task
{
    protected function _execute(array $params)
    {
	$dir = DOCROOT.'/public/tokens/';

	if ($handle = opendir($dir)) {
	    while (FALSE !== ($file = readdir($handle))) {
		if ($file!='.' && $file!='..' && (time() - filectime($dir.$file)) > 3600) {
			unlink($dir.$file);
		}
	    }
	    closedir($handle);
	}
    }
}
?>
