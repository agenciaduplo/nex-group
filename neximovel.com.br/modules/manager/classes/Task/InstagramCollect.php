<?php   
class Task_InstagramCollect extends Minion_Task
{
        protected function _execute(array $params)
        {
            $hash = 'oigalera';
            $count = 10;
            $clientId = 'c6ce854c146646aaa53ff2b89043193d';

            $url = 'https://api.instagram.com/v1/tags/'.$hash.'/media/recent?count='.$count.'&client_id='.$clientId;   

            $ch = curl_init();                  
            curl_setopt($ch, CURLOPT_URL,$url);                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);  
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
            $result = curl_exec($ch);
            curl_close ($ch);

            $list = json_decode($result, true);

            $data = $list['data'];          
            if($data==null){
                return;
            }            

            foreach ($data as $v) {  
                $image = $v['images']['standard_resolution'];
                $image_name = explode('/',$image['url']);

		try {	
			ORM::factory('Media')->create_media(array('media_type' => 'PHOTO', 'content' => $image_name[(count($image_name)-1)],
								   'posted' => date('Y/m/d', $v['created_time']), 'background' => '',
								   'approved' => 0, 'moderator' => '', 'media_owner' => $this->fixText($v['caption']['from']['username']),
								   'media_company' => 'Instagram', 'foreign_id' => $v['id']),
							     array('media_type', 'content', 'posted', 'background',
								   'approved', 'moderator', 'media_owner', 'media_company',
								   'foreign_id'));
			$ch = curl_init($image['url']);
			$fp = fopen(DOCROOT.'/public/medias/instagram/'.$image_name[(count($image_name)-1)], 'wb');
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);

                        copy(DOCROOT.'/public/medias/instagram/'.$image_name[(count($image_name)-1)], DOCROOT.'/public/medias/instagram/original/'.$image_name[(count($image_name)-1)]); 
               }
               catch(Exception $e){}
            }
        }   

        private function fixText($t){
            $t = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
            '|[\x00-\x7F][\x80-\xBF]+'.
            '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
            '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
            '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
            ' ', $t );

            //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
            $t = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
            '|\xED[\xA0-\xBF][\x80-\xBF]/S',' ', $t );

            return $t;
        }
    }      
?>
