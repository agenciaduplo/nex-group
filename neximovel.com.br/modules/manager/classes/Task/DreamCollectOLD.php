<?php   
class Task_DreamCollect extends Minion_Task
{
        protected function _execute(array $params)
        {
	    $dir = DOCROOT.'public/medias/dream';

	    $data = $this->process_dir($dir, TRUE);

            if($data==false){
                return;
            }            

            foreach ($data as $item) {  
		try {
			ORM::factory('Media')->create_media(array('media_type' => 'PHOTO', 'content' => $item['filename'],
								   'posted' => date('Y/m/d H:i:s', strtotime($item['dirpath'])), 'background' => '',
								   'approved' => 0, 'moderator' => '', 'media_owner' => 'Dream Factory',
								   'media_company' => 'Dream', 'foreign_id' => $item['dirpath'].'/'.$item['filename']),
							     array('media_type', 'content', 'posted', 'background',
								   'approved', 'moderator', 'media_owner', 'media_company',
								   'foreign_id'));
               }
	       catch(Exception $e){}
	    }   
	}

        private function fixText($t){
            $t = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
            '|[\x00-\x7F][\x80-\xBF]+'.
            '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
            '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
            '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
            ' ', $t );

            //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
            $t = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
            '|\xED[\xA0-\xBF][\x80-\xBF]/S',' ', $t );

            return $t;
        }

	private function process_dir($dir,$recursive = FALSE) {
	    if (is_dir($dir)) {
	      for ($list = array(),$handle = opendir($dir); (FALSE !== ($file = readdir($handle)));) {
		if (($file != '.' && $file != '..') && (file_exists($path = $dir.'/'.$file))) {
		  if (is_dir($path) && ($recursive))
		    $list = array_merge($list, $this->process_dir($path, TRUE));
  		  else {
 		    $entry = array('filename' => $file, 'dirpath' => str_replace(DOCROOT.'public/medias/dream/', '', $dir));
            	    $list[] = $entry;
		 }
		}
	      }
	      closedir($handle);
	      return $list;
	    } 
	    else return FALSE;
	  }
    }      
?>
