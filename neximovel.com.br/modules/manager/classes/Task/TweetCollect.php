<?php defined('SYSPATH') or die('No direct script access.');

class Task_TweetCollect extends Minion_Task
{
        //https://github.com/jonhurlock/Twitter-Application-Only-Authentication-OAuth-PHP/blob/master/Oauth.php
        public function bearerGenerate() { 
            define('CONSUMER_KEY', 'B1YapJSVyIQ6EkgsKxxbAw');
            define('CONSUMER_SECRET', 'T0iz5NJOewEGK6edxjZ3RBB9gbKC5xL5H7c8f19L0co');

            $encoded_consumer_key = urlencode(CONSUMER_KEY);
            $encoded_consumer_secret = urlencode(CONSUMER_SECRET);                                            
            $bearer_token = $encoded_consumer_key.':'.$encoded_consumer_secret;   
            $base64_encoded_bearer_token = base64_encode($bearer_token);           

            $url = "https://api.twitter.com/oauth2/token";   
            $headers = array(
            "POST /oauth2/token HTTP/1.1",
            "Host: api.twitter.com",                                         
            "Authorization: Basic ".$base64_encoded_bearer_token."",
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
            "Content-Length: 29"
            ); 

            $ch = curl_init();                        
            curl_setopt($ch, CURLOPT_URL,$url); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);      
            curl_setopt($ch, CURLOPT_POST, 1); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials"); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                

            $header = curl_setopt($ch, CURLOPT_HEADER, 1); 
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $retrievedhtml = curl_exec ($ch);  
            curl_close($ch); // close the curl

            $output = explode("\n", $retrievedhtml);
            $bearer_token = '';
            foreach($output as $line) {
                if($line === false) {
                    // there was no bearer token
                } else{
                    $bearer_token = $line;
                }
            }
            $bearer_token = json_decode($bearer_token);
            $bearer_token = $bearer_token->{'access_token'};

            return $bearer_token;
        }

	protected function _execute(array $params) 
	{
            $hash = 'oigalera';
            $page = 1;                                
            $rpp = 10;

            $bearer_token = $this->bearerGenerate();
            $searchtag = '?q='.$hash.'-filter:retweets'; //sem retweets  
            $limit = "&count=".$rpp;        
            $type = "&result_type=recent";        
            $customUrl = $searchtag.$limit.$type;

            $search = "https://api.twitter.com/1.1/search/tweets.json".$customUrl;
            $search = str_replace(' ','%20',$search);   

            $headers = array(                                           
            "GET /1.1/search/tweets.json".$customUrl." HTTP/1.1", 
            "Host: api.twitter.com",                                         
            "Authorization: Bearer ".$bearer_token."",
            );

            $ch = curl_init($search);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);                                                                       
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
            $json = curl_exec($ch);             
            curl_close($ch);   
            $json = json_decode($json);                 
            $list = $json->statuses;    

            foreach($list as $tweet) {  
                $desc = $this->fixText($tweet->text);
                $user = $tweet->user->screen_name;
                $imageList = @$tweet->entities->media;

                if ($imageList) {
                    foreach ($imageList as $it) {
			$image_name = explode('/', $it->media_url);
			try {
				ORM::factory('Media')->create_media(array('media_type' => 'PHOTO', 'content' => $image_name[(count($image_name)-1)], 
							   'posted' => date('Y/m/d', strtotime($tweet->created_at)), 'background' => '', 
							   'approved' => 0, 'moderator' => '', 'media_owner' => $user, 
							   'media_company' => 'Twitter', 'foreign_id' => md5(microtime().'$%&*(((')),
						     array('media_type', 'content', 'posted', 'background',
							   'approved', 'moderator', 'media_owner', 'media_company',
							   'foreign_id'));

				$ch = curl_init($it->media_url.':large');
				$fp = fopen(DOCROOT.'/public/medias/twitter/'.$image_name[(count($image_name)-1)], 'wb');
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_exec($ch);
				curl_close($ch);
				fclose($fp);

				copy(DOCROOT.'/public/medias/twitter/'.$image_name[(count($image_name)-1)], DOCROOT.'/public/medias/twitter/original/'.$image_name[(count($image_name)-1)]);	
	               }
	               catch(Exception $e){}
                    }                                       
                }

		try {
			ORM::factory('Media')->create_media(array('media_type' => 'TEXT', 'content' => $this->fixText($tweet->text), 
						   'posted' => date('Y/m/d', strtotime($tweet->created_at)), 'background' => '', 
						   'approved' => 0, 'moderator' => '', 'media_owner' => $user, 
						   'media_company' => 'Twitter', 'foreign_id' => $tweet->id),
					     array('media_type', 'content', 'posted', 'background', 'approved', 'moderator', 
						   'media_owner', 'media_company', 'foreign_id'));
               }
               catch(Exception $e){}
            }
        }    

        private function fixText($t){
            $t = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
            '|[\x00-\x7F][\x80-\xBF]+'.
            '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
            '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
            '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
            ' ', $t );

            //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
            $t = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
            '|\xED[\xA0-\xBF][\x80-\xBF]/S',' ', $t );

            return $t;
        }
    }      
?>
