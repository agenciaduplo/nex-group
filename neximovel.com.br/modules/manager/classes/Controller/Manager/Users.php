<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Users extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - Usuários';
    if(!Auth::instance()->logged_in('admin') && $this->request->action() != 'profile'){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
    $view = View::factory('manager/users/index');
    $view->users = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(4,5)'))
               ->where('user.enabled', '=', 1)
               ->find_all();
    $this->template->content = $view;
  }
  
  public function action_new()
  {
    $this->template->title .= ' - novo';

    $user = ORM::factory('User');
	$attendants = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(3)'))
               ->where('user.enabled', '=', 1)
               ->find_all()->as_array('id', 'name');
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all()->as_array('name', 'description');

	$dns = ORM::factory('Dn')->order_by('name','ASC')->find_all()->as_array('id', 'name');
	
    $view = View::factory('manager/users/new');
    $view
      ->bind('user', $user)
	  ->bind('dns', $dns)
	  ->bind('attendants', $attendants)
      ->bind('roles', $roles)
      ->bind('errors', $errors);
    
    $this->template->content = $view;
  }
  
  public function action_create(){
    $user = ORM::factory('User');
	$dnUser = ORM::factory('DnUser');
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all()->as_array('name', 'description');
	$dns = ORM::factory('Dn')->order_by('name','ASC')->find_all()->as_array('id', 'name');
	
	$attendants = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(3)'))
               ->where('user.enabled', '=', 1)
               ->find_all()->as_array('id', 'name');
	
    $this->template->title .= ' - novo';
    $view = View::factory('manager/users/new');    
      
	$params = $this->request->post();
	
	$role = $this->request->post('role');
	
    if($this->request->method() == Request::POST) {
		try {
		  
			$params['email'] = $params['username'];
		  
			$user->create_user($params);

			//default login role
			$login_role = ORM::factory('role',array('name' => 'login'));
			$user->add('roles', $login_role);

			$login_role = ORM::factory('role',array('name' => $this->request->post('role')));
			$user->add('roles', $login_role);

			$att = ORM::factory('User', $user->attendant);
			
			$message = 'Login: '.$user->username.
						'<br>Senha: '.$this->request->post('password').
						'<br>Telefone: '.$user->telefone.
						'<br>Atendimento: '.$att->name.'<br>';

			
			foreach($params['dn_add'] as $k => $v){
				$user->add('dns', $v);
				
				$dn = ORM::factory('Dn', $v);
				
				$message .= '<br>=============================================<br>'.
							'DN: '.$dn->name.
							'<br>Nome fantasia: '.$dn->store_name.
							'<br>Razão social: '.$dn->corporate_name.
							'<br>Tipo: '.$dn->type.
							'<br>CNPJ: '.$dn->cnpj.
							'<br>IE: '.$dn->ie.
							'<br>IM: '.$dn->im.
							'<br>Endereço: '.$dn->address.
							'<br>Cidade: '.$dn->city.
							'<br>Estado: '.$dn->state->name.
							'<br>Regional: '.$dn->region->name.
							'<br>Site: '.$dn->site.
							'<br>Financeiro/Nome: '.$dn->financial_name.
							'<br>Financeiro/E-mail: '.$dn->financial_email.
							'<br>Financeiro/Fone: '.$dn->financial_phone.
							'<br>Marketing/Nome: '.$dn->marketing_name.
							'<br>Marketing/E-mail: '.$dn->marketing_email.
							'<br>Marketing/Fone: '.$dn->marketing_phone.
							'<br>Banco: '.$dn->bank.
							'<br>Agência: '.$dn->agency.
							'<br>Conta corrente: '.$dn->account;
			}

			Mailer::instance()
							->to($user->email)
							->from('sender@basewd.com')
							->subject('[ABRAFOR]Criado usuário para '. $user->name)
							//->attachments(array($attatch))
							->html($message)
							->send();
			
			Notices::add('success', 'Usuário adicionado com sucesso');
			$this->redirect('manager/users');
		} catch(ORM_Validation_Exception $e) {
			Notices::add('error', 'Confira os campos obrigatórios e suas regras de preenchimento antes de continuar');
			$errors = $e->errors('models');
		} catch(Kohana_Database_Exception $e) {
			Notices::add('error', 'Username e/ou email existentes, por favor escolha outro');
		}
    }

    $view
      ->bind('user', $user)
      ->bind('role', $role)
      ->bind('dns', $dns)
      ->bind('roles', $roles)
	  ->bind('attendants', $attendants)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
  
  public function action_edit()
  {
    $user = ORM::factory('User', $this->request->param('id'));
	
	$attendants = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(3)'))
               ->where('user.enabled', '=', 1)
               ->find_all()->as_array('id', 'name');
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all()->as_array('name', 'description');

	$dns = ORM::factory('Dn')->order_by('name','ASC')->find_all()->as_array('id', 'name');
	
    $view = View::factory('manager/users/edit');
    $view
      ->bind('user', $user)
	  ->bind('dns', $dns)
	  ->bind('attendants', $attendants)
      ->bind('roles', $roles)
      ->bind('errors', $errors);

    $this->template->title .= ' - '.$user->username;
    $this->template->content = $view;
  }
  
  public function action_update()
  {
	$params = $this->request->post();
	
    $user = ORM::factory('User' ,$this->request->param('id'));

	$attendants = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(3)'))
               ->where('user.enabled', '=', 1)
               ->find_all()->as_array('id', 'name');
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all()->as_array('name', 'description');

	$dns = ORM::factory('Dn')->order_by('name','ASC')->find_all()->as_array('id', 'name');
	
    if($this->request->method() == Request::POST) {
		try {
			
			$user->remove('dns');
			
			foreach($params['dn_add'] as $k => $v){
					$user->add('dns', $v);
			}
		  
			$params = $this->request->post();
			$params['email'] = $this->request->post('username');
			
			$user->update_user($params);
			Notices::add('success', 'Usuário alterado com sucesso');
			$this->redirect('manager/users');
		} catch(ORM_Validation_Exception $e) {
			Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
			$errors = $e->errors('models');
		}
			catch(Kohana_Database_Exception $e) {
			Notices::add('error', 'Username e/ou email existentes, por favor escolha outro');
		}
    }

    $view = View::factory('manager/users/edit');
    $view
      ->bind('user', $user)
	  ->bind('dns', $dns)
	  ->bind('attendants', $attendants)
      ->bind('roles', $roles)
      ->bind('errors', $errors);
    
    $this->template->title .= ' - '.$user->username;
    $this->template->content = $view;
  }

  public function action_delete()
  {
    $user = ORM::factory('User', $this->request->param('id'));
    DB::delete('roles_users')
          ->where('user_id', '=', $user->id)
          ->execute();
    
    if($user->delete()){
      Notices::add('info', 'Registro excluído com sucesso');
    }

    $this->redirect('manager/users');
  }
  	public function action_csv()
	{
		
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		fputcsv($output, array('Nome', 'Email', 'Origem'), ';');

		// fetch the data
		
		$rows = ORM::factory('user')->join('roles_users')
               ->on('roles_users.user_id', '=', 'user.id')
               ->where('roles_users.role_id', 'IN', DB::expr('(4,5)'))
               ->where('user.enabled', '=', 1)
               ->find_all();

		foreach($rows as $row){
			fputcsv($output, array($row->name, $row->email, $row->roles->where('name', '!=', 'login')->find()->description),';');
		}
		
		// loop over the rows, outputting them
		//while ($row = mysql_fetch_assoc($rows)) fputcsv($output, array($row->name, $row->email, $row->origin));
		
	}
} // End Manager users