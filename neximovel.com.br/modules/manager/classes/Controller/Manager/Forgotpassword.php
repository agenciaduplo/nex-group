<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Forgotpassword extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    //if already logged in, redirect to dashboard
    if(Auth::instance()->logged_in(array('manager')) || Auth::instance()->logged_in(array('admin'))){
      $this->redirect('manager');
    }
  }
  
  public function action_index(){

    $this->template->title .= ' - Forgot Password';
    $view = View::factory('manager/forgotpassword/index');    

    if($this->request->post('email') != ''){
        try {
                $user  = ORM::factory('User')->where('email', '=', $this->request->post('email'))->find();
                $pwd    = substr(md5(microtime()),0,8);
                $email = View::factory('manager/templates/email');
                $email->name= $user->name;
                $email->password = $pwd;
                $email->url = URL::site(NULL, TRUE);

                if($user->name != ''){
                        Mailer::instance()
                        ->to($user->email)
                        ->from('suporte@neximovel.com.br')
                        ->subject('NEX Imóvel - Nova Senha')
                        ->html($email->render())
                        ->send();

					$user->update_user(array('password' => $pwd, 'password_confirm' => $pwd), array('password'));
					Notices::add('success', 'Nova senha enviada.');

                }else{
					Notices::add('error', 'Email n&atilde;o cadastrado.');
                }

        } catch(ORM_Validation_Exception $e) {
          Notices::add('error', 'Erro inesperado. Tente novamente.');
		  $errors = $e->errors('models');
        }
    } else {
		if($this->request->method() == Request::POST) {
			Notices::add('error', 'E-mail inv&aacute;lido');
		}
	}
	
	$view->bind('errors', $errors);
	
    $this->template->content = $view;
  }
} // End Manager forgot password
