<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Downloads extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - Downloads';
		/*if(!Auth::instance()->logged_in('admin')){
		  $this->request->redirect('manager/dashboard');
		}*/
	}
	  
	public function action_index()
	{
		$view = View::factory('manager/downloads/index');
		$view->downloads = ORM::factory('Download')->find_all();

		$this->template->content = $view;

	}
	  
	public function action_new()
	{
		$this->template->title .= ' - Adicionar download';

		$view = View::factory('manager/downloads/new');
		$download = ORM::factory('Download');
		
		$view
		  ->bind('download', $download)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_create()
	{

		$downloads = ORM::factory('Download');
		$this->template->title .= ' - Adicionar download';
		$view = View::factory('manager/downloads/new');
		
		$params = $this->request->post();
		$params['dt_created'] = date('Y-m-d');
		  
		if($this->request->method() == Request::POST) {
			try {
		  
				//UPLOAD LOCAL
				$file = Upload::save($_FILES['file'], NULL, DOCROOT.'/public/downloads/');
					
				$file = explode('/', $file);
				$params['file'] = URL::base(TRUE, TRUE).'/public/downloads/'.$file[count($file)-1];
				
				$downloads->create_download($params);
				Notices::add('success', 'Download adicionado com sucesso');
				$this->redirect('manager/downloads');
			} 
			catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
				$errors = $e->errors('models');
			}
		}

		$view
		  ->bind('downloads', $downloads)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_edit()
	{
		$download = ORM::factory('Download' ,$this->request->param('id'));
		$view = View::factory('manager/downloads/edit');
		$view
		  ->bind('download', $download)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}
	  
	public function action_update()
	{
		$download = ORM::factory('Download' ,$this->request->param('id'));
		$view = View::factory('manager/downloads/edit');
		$view
		  ->bind('download', $download)
		  ->bind('errors', $errors);
		
		$params = $this->request->post();
		$params['dt_created'] = date('Y-m-d');
		
		if($this->request->method() == Request::POST) {
		  try {
			
			//UPLOAD LOCAL
			$file = Upload::save($_FILES['file'], NULL, DOCROOT.'/public/downloads/');
					
			$file = explode('/', $file);
			$params['file'] = URL::base(TRUE, TRUE).'/public/downloads/'.$file[count($file)-1];
		  
			$download->update_download($params);
			Notices::add('success', 'Download alterado com sucesso');
			$this->redirect('manager/downloads');
		  } catch(ORM_Validation_Exception $e) {
			Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
			$errors = $e->errors('models');
		  }
		}
		$this->template->content = $view;
	}
	
	public function action_list()
	{
	
		$view = View::factory('manager/downloads/list');

		$downloads = ORM::factory('Download')->where('enabled', '=', 1)->find_all();

		$view->bind('downloads', $downloads);
		
		$this->template->content = $view;
		
	}
	
	public function action_send_file()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$download = ORM::factory('Download',$this->request->param('id'));
		$file = $download->file;
		//$this->response->send_file($file);
		header("Content-Disposition: attachment; filename=".basename($file));
		readfile($file);
		exit;
		$this->redirect('manager/downloads/list');
	}
	
} // End Manager downloads
