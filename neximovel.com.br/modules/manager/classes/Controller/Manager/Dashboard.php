<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Dashboard extends Controller_Manager_Application {
  
   public function before(){
    parent::before();

    $this->template->title .= ' - Dashboard';
  }
 
  public function action_index(){ 
    $view = View::factory('manager/dashboard/index');
	
	$view->property_count = ORM::factory('Property')->count_all();
	$view->property_active_count = ORM::factory('Property')->where('enabled', '=', 1)->count_all();
	$view->highlights = ORM::factory('Property')->where('enabled', '=', 1)->and_where('highlight', '=', 1)->count_all();
	
    $this->template->content = $view;
  }
} // End Dashboard
