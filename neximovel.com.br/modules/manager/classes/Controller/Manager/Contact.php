<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Contact extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    //if already logged in, redirect to dashboard
    if(Auth::instance()->logged_in(array('manager')) || Auth::instance()->logged_in(array('admin'))){
      $this->redirect('manager');
    }
  }
  
  public function action_index(){
    $user = ORM::factory('User');
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all();
	$states = ORM::factory('State')->find_all()->as_array('id', 'name');
	
    $this->template->title .= ' - new';
    $view = View::factory('manager/contact/index');    
      
    if($this->request->method() == Request::POST) {
	
      try {
			$params = $this->request->post();
			$state = ORM::factory('State', $params['state']);
		
			$html = 'Nome: '.$params['name'].'<br>'.
					'Distribuidor: '.$params['distributor'].'<br>'.
					'Estado: '.$state->name.'<br>'.
					'Cidade: '.$params['city'].'<br>'.
					'Assunto: '.$params['subject'].'<br>'.
					'Email: '.$params['email'].'<br>'.
					'Mensagem: '.$params['message'];

			Mailer::instance()
					->to('abrafor@agenciaduplo.com.br')
					->from('no-reply@bookford.com.br')
					->subject('Book Abrafor - '. $params['subject'])
					->html($html)
					->send();
			

			Notices::add('success', 'Mensagem enviada com sucesso.');
			
		} catch(ORM_Validation_Exception $e) {
			Notices::add('error', 'Check the required fields and the filling rules before continue.');
        $errors = $e->errors('models');
		} catch(Kohana_Database_Exception $e) {
			Notices::add('error', 'Email already in use, please choose another.');
		}
    }

    $name = @$this->request->post('name');
    $view
      ->bind('user', $user)
      ->bind('name', $name)
      ->bind('roles', $roles)
	  ->bind('states', $states)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
} // End Manager users
