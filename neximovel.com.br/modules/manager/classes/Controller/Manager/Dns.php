<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Dns extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - DN\'s';
    if(!Auth::instance()->logged_in('admin') && $this->request->action() != 'profile'){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
    $view = View::factory('manager/dns/index');
    $view->dns = ORM::factory('Dn')->find_all();
    $this->template->content = $view;
  }
  
  public function action_new()
  {
    $this->template->title .= ' - novo';

	$view = View::factory('manager/dns/new');
	
    $view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
	$view->regions = ORM::factory('Region')->where('id', '<>', 9)->find_all()->as_array('id', 'name');
    
    $view->bind('errors', $errors);
    
    $this->template->content = $view;
  }
  
	public function action_create()
	{
		$dn = ORM::factory('Dn');
		
		$view = View::factory('manager/dns/new');
	
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		$view->regions = ORM::factory('Region')->where('id', '<>', 9)->find_all()->as_array('id', 'name');
	
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST) {
			
			if($this->isCnpjValid($params['cnpj'])){
			
				$dn->create_dn($params);
				Notices::add('success', 'DN adicionada com sucesso');
				$this->redirect('manager/dns');
				
			} else {
			
				Notices::add('error', 'CNPJ Inv&aacute;lido.');
				
			}
		}

		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('ctypes', $ctypes)
		  ->bind('regions', $regions)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_edit()
	{ 
		$view = View::factory('manager/dns/edit');
		
		$view->dn = ORM::factory('Dn', $this->request->param('id'));
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		$view->regions = ORM::factory('Region')->where('id', '<>', 9)->find_all()->as_array('id', 'name');

		$view->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_update()
	{ 
		$view = View::factory('manager/dns/edit');
		
		$dn = ORM::factory('Dn', $this->request->param('id'));
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		$view->regions = ORM::factory('Region')->where('id', '<>', 9)->find_all()->as_array('id', 'name');
		
		if($this->request->method() == Request::POST) {

			$params = $this->request->post();
			
			try {
				$dn->update_dn($params);
				Notices::add('success', 'DN editada com sucesso');
				$this->redirect('manager/dns');
			} catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigat�rios antes de continuar');
				$errors = $e->errors('models');
			} catch(Kohana_Database_Exception $e) {
				Notices::add('error', 'Username e/ou email existentes, por favor escolha outro');
			}
			
		}

		$view->dn = $dn;
		$view->bind('errors', $errors);

		$this->template->content = $view;
	}

	/**
	 * isCnpjValid
	 *
	 * Esta fun��o testa se um Cnpj � valido ou n�o. 
	 *
	 * @author	Raoni Botelho Sporteman <raonibs@gmail.com>
	 * @version	1.0 Debugada em 27/09/2011 no PHP 5.3.8
	 * @param	string		$cnpj			Guarda o Cnpj como ele foi digitado pelo cliente
	 * @param	array		$num			Guarda apenas os n�meros do Cnpj
	 * @param	boolean		$isCnpjValid	Guarda o retorno da fun��o
	 * @param	int			$multiplica 	Auxilia no Calculo dos D�gitos verificadores
	 * @param	int			$soma			Auxilia no Calculo dos D�gitos verificadores
	 * @param	int			$resto			Auxilia no Calculo dos D�gitos verificadores
	 * @param	int			$dg				D�gito verificador
	 * @return	boolean						"true" se o Cnpj � v�lido ou "false" caso o contr�rio
	 *
	 */
	 
	public function isCnpjValid($cnpj)
	{
			//Etapa 1: Cria um array com apenas os digitos num�ricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
			$j=0;
			for($i=0; $i<(strlen($cnpj)); $i++)
				{
					if(is_numeric($cnpj[$i]))
						{
							$num[$j]=$cnpj[$i];
							$j++;
						}
				}
			//Etapa 2: Conta os d�gitos, um Cnpj v�lido possui 14 d�gitos num�ricos.
			if(count($num)!=14)
				{
					$isCnpjValid=false;
				}
			//Etapa 3: O n�mero 00000000000 embora n�o seja um cnpj real resultaria um cnpj v�lido ap�s o calculo dos d�gitos verificares e por isso precisa ser filtradas nesta etapa.
			if ($num[0]==0 && $num[1]==0 && $num[2]==0 && $num[3]==0 && $num[4]==0 && $num[5]==0 && $num[6]==0 && $num[7]==0 && $num[8]==0 && $num[9]==0 && $num[10]==0 && $num[11]==0)
				{
					$isCnpjValid=false;
				}
			//Etapa 4: Calcula e compara o primeiro d�gito verificador.
			else
				{
					$j=5;
					for($i=0; $i<4; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);
					$j=9;
					for($i=4; $i<12; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);	
					$resto = $soma%11;			
					if($resto<2)
						{
							$dg=0;
						}
					else
						{
							$dg=11-$resto;
						}
					if($dg!=$num[12])
						{
							$isCnpjValid=false;
						} 
				}
			//Etapa 5: Calcula e compara o segundo d�gito verificador.
			if(!isset($isCnpjValid))
				{
					$j=6;
					for($i=0; $i<5; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);
					$j=9;
					for($i=5; $i<13; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);	
					$resto = $soma%11;			
					if($resto<2)
						{
							$dg=0;
						}
					else
						{
							$dg=11-$resto;
						}
					if($dg!=$num[13])
						{
							$isCnpjValid=false;
						}
					else
						{
							$isCnpjValid=true;
						}
				}
			//Trecho usado para depurar erros.
			/*
			if($isCnpjValid==true)
				{
					echo "<p><font color="GREEN">Cnpj � V�lido</font></p>";
				}
			if($isCnpjValid==false)
				{
					echo "<p><font color="RED">Cnpj Inv�lido</font></p>";
				}
			*/
			//Etapa 6: Retorna o Resultado em um valor booleano.
			return $isCnpjValid;			
	}
} // End Manager users