<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Newsletter extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - Newsletter';
		if(!Auth::instance()->logged_in()){
		  $this->redirect('manager/dashboard');
		}
	}
	  
	public function action_index()
	{
		$view = View::factory('manager/newsletter/index');
		$view->newsletters = ORM::factory('Newsletter')->find_all();

		$this->template->content = $view;
	}
	
	public function action_delete()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$newsletter = ORM::factory('Newsletter', $this->request->param('id'));
			$newsletter->delete();
			echo json_encode('deleted');
			
		}catch(Exception $e){
			echo json_encode($e);
		}
		
		exit;
		
	}
	
	public function action_csv()
	{
		
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		fputcsv($output, array('Nome', 'Email', 'Origem'), ';');

		// fetch the data
		
		$rows = ORM::factory('Newsletter')->find_all();

		foreach($rows as $row){
			fputcsv($output, array($row->name, $row->email, $row->origin),';');
		}
		
		// loop over the rows, outputting them
		//while ($row = mysql_fetch_assoc($rows)) fputcsv($output, array($row->name, $row->email, $row->origin));
		
	}
} // End Manager news
