<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Bcategories extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - Categorias';
    if(!Auth::instance()->logged_in('admin')){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
    $view = View::factory('manager/bcategories/index');
    $view->bcategories = ORM::factory('Bcategory')->find_all();

    $this->template->content = $view;

  }
  
  public function action_new()
  {
    $this->template->title .= ' - Nova Categoria';

    $view = View::factory('manager/bcategories/new');
    $bcategory = ORM::factory('Bcategory');

    $view
      ->bind('bcategory', $bcategory)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
  
  public function action_create(){

    $bcategories = ORM::factory('Bcategory');

    $this->template->title .= ' - Nova Categoria';
    $view = View::factory('manager/bcategories/new');    
      
    if($this->request->method() == Request::POST) {
      try {
        $bcategories->create_bcategory($this->request->post());
        Notices::add('success', 'Categoria adicionada com sucesso');
        $this->redirect('manager/bcategories');
      } 
      catch(ORM_Validation_Exception $e) {
        Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
        $errors = $e->errors('models');
      }
    }

    $view
      ->bind('bcategories', $bcategories)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
  
  public function action_edit()
  {
    $bcategory = ORM::factory('Bcategory' ,$this->request->param('id'));
    $view = View::factory('manager/bcategories/edit');
    $view->bind('bcategory', $bcategory)
		 ->bind('errors', $errors);
	  
    $this->template->content = $view;
  }
  
  public function action_update()
  {
    $bcategory = ORM::factory('Bcategory' ,$this->request->param('id'));
    $view = View::factory('manager/bcategories/edit');
    $view
      ->bind('bcategory', $bcategory)
      ->bind('errors', $errors);
    
    if($this->request->method() == Request::POST) {
      try {
        $bcategory->update_bcategory($this->request->post());
        Notices::add('success', 'Categoria alterada com sucesso');
        $this->redirect('manager/bcategories');
      } catch(ORM_Validation_Exception $e) {
        Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
        $errors = $e->errors('models');
      }
    }
    $this->template->content = $view;
  }
  
	public function action_disable_all()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		if($this->request->method() == Request::POST) {
		try {
			$cats = $this->request->post('disable_cat');
			$cats = '(' . implode(',', $cats) . ')';
			
			$dis = DB::update('bcategories')->set(array('enabled' => 0))->where('id', 'IN', DB::expr($cats))->execute();


		} catch(ORM_Validation_Exception $e) {
			
			$errors = $e->errors('models');
			
		}
    }
	}
} // End Manager Categories
