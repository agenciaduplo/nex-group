<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Campaigns extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - Campanhas';
		if(!Auth::instance()->logged_in('admin')){
		  $this->redirect('manager/dashboard');
		}
	}
	  
	public function action_index()
	{
		$view = View::factory('manager/campaigns/index');
		$view->campaigns = ORM::factory('Campaign')->find_all();

		$this->template->content = $view;

	}
	  
	public function action_new()
	{
		$this->template->title .= ' - Adicionar campanha';

		$view = View::factory('manager/campaigns/new');
		$campaign = ORM::factory('Campaign');
		$ctypes = ORM::factory('Ctype')->find_all()->as_array('id', 'name');
		$regions = ORM::factory('Region')->find_all()->as_array('id', 'name');
		
		$view
		  ->bind('campaign', $campaign)
		  ->bind('ctypes', $ctypes)
		  ->bind('regions', $regions)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_create()
	{

		$campaigns = ORM::factory('Campaign');
		$ctypes = ORM::factory('Ctype')->find_all()->as_array('id', 'name');
		$regions = ORM::factory('Region')->find_all()->as_array('id', 'name');
		$this->template->title .= ' - Adicionar Campanha';
		$view = View::factory('manager/campaigns/new');
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST) {
			try {
				$campaigns->create_campaign($params);
				Notices::add('success', 'Campanha adicionada com sucesso');
				$this->redirect('manager/campaigns');
			} 
			catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
				$errors = $e->errors('models');
			}
		}

		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('ctypes', $ctypes)
		  ->bind('regions', $regions)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_edit()
	{
		$ctypes = ORM::factory('Ctype')->find_all()->as_array('id', 'name');
		$regions = ORM::factory('Region')->find_all()->as_array('id', 'name');
		$campaign = ORM::factory('Campaign' ,$this->request->param('id'));
		$view = View::factory('manager/campaigns/edit');
		$view
		  ->bind('campaign', $campaign)
		  ->bind('ctypes', $ctypes)
		  ->bind('regions', $regions)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}
	  
	public function action_update()
	{
		$ctypes = ORM::factory('Ctype')->find_all()->as_array('id', 'name');
		$regions = ORM::factory('Region')->find_all()->as_array('id', 'name');
		$campaign = ORM::factory('Campaign' ,$this->request->param('id'));
		$view = View::factory('manager/campaigns/edit');
		$view
		  ->bind('campaign', $campaign)
		  ->bind('ctypes', $ctypes)
		  ->bind('regions', $regions)
		  ->bind('errors', $errors);
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST) {
		  try {
			$campaign->update_campaign($params);
			Notices::add('success', 'Campanha alterada com sucesso');
			$this->redirect('manager/campaigns');
		  } catch(ORM_Validation_Exception $e) {
			Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
			$errors = $e->errors('models');
		  }
		}
		$this->template->content = $view;
	}

	public function action_delete_campaign()
	{
		if($this->request->method() == 'POST')
		{
            //ORM::Factory('Campaign', $this->request->post('id'))->delete_campaign();

            			// Use primary key value
			$id = $this->request->post('id');

			// Delete the object
			DB::delete("campaigns")
			->where("id", '=', $id)
			->execute($this->_db);

            //ORM::Factory('Book', $this->request->post('id'))->delete_book();


		}
	}

} // End Manager campaigns
