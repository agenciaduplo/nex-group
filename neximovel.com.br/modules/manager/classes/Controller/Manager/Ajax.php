<?php defined('SYSPATH') or die('No direct script access.');



class Controller_Manager_Ajax extends Controller_Manager_Application {



  public function before()

  {

    	$this->auto_render = FALSE;

	$this->profiler    = NULL;

		

        if($this->auto_render === TRUE)

	  die('No direct script access.');

  }



  protected function array_to_xml(array $arr, SimpleXMLElement $xml)

  {

    foreach ($arr as $k => $v) {

        is_array($v)

            ? $this->array_to_xml($v, $xml->addChild($k))

            : $xml->addChild($k, $v);

    }

    return $xml;

  }

 

	public function action_exportxml(){

		$type = $this->request->param('id');
	
		$reports = Session::instance()->get('export');
		$tDate = Session::instance()->get('tDate');
		
		switch($type){
		
			case 'dn': 
			
			ob_start(); ?>
			
			<table>
				<thead>
				  <tr>
					<th>Nome da Campanha</th>
					<th>ID da Pe&ccedil;a</th>
					<th>M&ecirc;s de compet&ecirc;ncia</th>
					<th>Valor do Pedido</th>
					<th>Status</th>
				  </tr>
				</thead>   
				<tbody>
				
				<?php 
					setlocale(LC_MONETARY, 'pt_BR');
					foreach($reports as $t){ ?>
				  <tr>
					<td><?php echo $t->book->campaign->name; ?></td>
					<td><?php echo $t->id; ?></td>
					<td><?php echo date('m/Y', strtotime($t->ticket_date)); ?></td>
					<td><?php echo money_format('%.2n', $t->cost); ?></td>
					<td><?php echo $t->status; ?></td>
				  </tr>
				<?php } ?>
				</tbody>
			</table>

			<?php break;
			
			case 'consolidado':
			
				ob_start(); ?>
			
			<table>
				<thead>
				  <tr>
					<th>Nome do Distribuidor</th>
					<th>Quantidade de Pedidos</th>
					<th>Valor dos Pedidos Aprovados</th>
					<th>Valor dos Pedidos Reprovados</th>
				  </tr>
				</thead>   
				<tbody>
				
				<?php 
				setlocale(LC_MONETARY, 'pt_BR');
				foreach($reports as $d){ ?>
					<tr>
						<td><?php echo $d->name; ?></td>
						<td><?php echo $d->tickets->where('status', '!=', 'to-confirm')->and_where('ticket_date', '=', DB::expr("'$tDate'"))->count_all(); ?></td>
						<td><?php 
								  $sa = $d->tickets->select(DB::expr('SUM(ticket.cost) AS `sumaprov`'))->where('status', '=', 'aprovado')->find_all()->as_array(null, 'sumaprov');
								  echo  money_format('%.2n', $sa[0]); ?>
						</td>
						<td><?php 
								  $sr = $d->tickets->select(DB::expr('SUM(ticket.cost) AS `sumreprov`'))->where('status', '=', 'reprovado')->find_all()->as_array(null, 'sumreprov');
								  echo  money_format('%.2n', $sr[0]); ?></td>
					</tr>
			<?php } ?>
				</tbody>
			</table>
			
			<?php 
		
		}
		
		$data = ob_get_clean();


		$file="Report.xls";

		header("Pragma: public");

		header("Content-type: application/vnd.ms-excel");

		header("Content-Disposition: attachment; filename=$file");

		echo $data;

	}  

  public function action_avaliar(){

      if($this->request->method() == Request::POST) {

        try {

            $quali = ORM::Factory('Qualitativa', $this->request->post('id') );

            $quali->status = $this->request->post('status');

            $quali->update();

        }catch(Kohana_Database_Exception $e) {        

            echo $e;

        }

      }

  }

	public function action_get_state(){
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		$state = ORM::factory('State')->where('uf', '=', strtoupper($this->request->param('id')))->find()->as_array('id', 'name');
		
		echo json_encode($state);
	}
  
	public function action_get_cities(){
		if($this->request->method() == Request::POST) {
			$cities = ORM::factory('City')->where('state_id', '=', $this->request->post('state'))->find_all()->as_array('id', 'name');
			
			//array_unshift($cities, 'Selecione a cidade');
			echo json_encode($cities);
		}
	}

}

?>

