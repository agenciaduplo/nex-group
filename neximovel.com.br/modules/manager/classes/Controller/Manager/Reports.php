﻿<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Reports extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - Relat&oacute;rios';
		if(!Auth::instance()->logged_in('admin') && !Auth::instance()->logged_in('committee')){
		  $this->redirect('manager/dashboard');
		}
	}
	
	public function action_index()
	{
		$view = View::factory('manager/reports/index');
		
		if(Auth::instance()->logged_in('admin')){

			$view->dns = ORM::factory('Dn')->find_all()->as_array('id', 'name');
			$view->regions = ORM::factory('Region')->find_all()->as_array('id','name');
			
		} else if (Auth::instance()->logged_in('committee')){
	
	
			$view->regions = Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id');
			$region_ids = '(' . implode(',', Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id')) . ')';
			$view->dns = ORM::factory('Dn')->where('region_id', 'IN', DB::expr($region_ids))->find_all()->as_array('id', 'name');
		
		}
		
		$view->types = array(
			'' => 'Selecione',
			//'duplo' => 'Relat&oacute;rio Duplo XLS',
			'dn' 	=> 'Relat&oacute;rio por DN',
			//'consolidado' => 'Relat&oacute;rio Consolidado'
		);

		$view->months = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Mar&ccedil;o',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		$view->years = array(2014 => 2014, 2015 => 2015, 2016 => 2016, 2017 => 2017, 2018 => 2018);
		
		$this->template->content = $view;

	}
	
	public function action_report()
	{	
		$params = $this->request->post();
		if(empty($params['start_date']) || empty($params['end_date'])){
		
			//ESCOLHEU MES COMPETENCIA AO INVES DE PERIODO
			$sDate = date('Y-m-d', 0);
			$eDate = date('Y-m-d');
			
			$tDate = date('Y-m-d', strtotime($params['year'].'-'.$params['month'].'-01'));
			$tDateOper = '=';
			
		} else {
		
			//ESCOLHEU PERIODO AO INVES DE MES COMPETENCIA
			$sDate = date('Y-m-d', strtotime(str_replace('/', '-',$params['start_date'])));
			$eDate = date('Y-m-d', strtotime(str_replace('/', '-',$params['end_date'])));
		
			$tDate = date('Y-m-d', 0);
			$tDateOper = '>';
		}
		
		switch($params['rtype']){
			
			case 'dn':
				$view = View::factory('manager/reports/report_dn');
			
				$dns = $params['dn'][0] == 0 ? implode(',', ORM::factory('Ticket')->find_all()->as_array(NULL, 'id')) : implode(',',$params['dn']);
			
				if(Auth::instance()->logged_in('admin')){
			
					if($params['region'] == 0){
					
						$regions = '(' . implode(',', ORM::factory('Region')->find_all()->as_array(null,'id')) . ')';
					
					} else {
						$regions = '('.$params['region'].')';
					}
			
					$reports = ORM::factory('Ticket')
								->where('dn_id', 'IN', DB::expr('('.$dns.')'))
								->and_where('status', '!=', 'to-confirm')
								->and_where('status', '!=', 'cancelar')
								->and_where('region_id', 'IN', DB::expr($regions))
								->and_where('dt_created', 'BETWEEN', DB::expr("'$sDate' AND '$eDate'"))
								->and_where('ticket_date', $tDateOper, DB::expr("'$tDate'"))
								->find_all();
					
				} else if (Auth::instance()->logged_in('committee')){
			
					if($params['region'] == 0){
					
						$regions = '(' . implode(',', Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id')) . ')';
						
					} else {
						$regions = '('.$params['region'].')';
					}
					
					$reports = ORM::factory('Ticket')
								->where('dn_id', 'IN', DB::expr('('.$dns.')'))
								->and_where('status', '!=', 'to-confirm')
								->and_where('status', '!=', 'cancelar')
								->and_where('region_id', 'IN', DB::expr($regions))
								->and_where('dt_created', 'BETWEEN', DB::expr("'$sDate' AND '$eDate'"))
								->and_where('ticket_date', $tDateOper, DB::expr("'$tDate'"))
								->find_all();
				
				}
				
				ob_start(); ?>
				
						<table>
							<thead>
								<tr>
									<th>PP/PI</th>
									<th>Data provis&atilde;o</th>
									<th>Tipo de Mídia</th>
									<th>DN</th>
									<th>Nome do Distribuidor</th>
									<th>Regional</th>
									<th>Valor do Pedido</th>
									<th>Raz&atilde;o Social</th>
									<th>CNPJ</th>
									<th>Endere&ccedil;o</th>
									<th>Numero</th>
									<th>Complemento</th>
									<th>Bairro</th>
									<th>CEP</th>
									<th>Cidade</th>
									<th>UF</th>
									<th>Telefone</th>
									<th>E-mail</th>
									<th>Status</th>
									<th>Tipo</th>
								</tr>
							</thead>   
							<tbody>
							
							<?php 
								setlocale(LC_MONETARY, 'pt_BR');
								foreach($reports as $t){ 
								
								$s = json_decode($t->suppliers);
								foreach( $s as $key => $all ){
									foreach( $all as $i => $val ){
										$new[$i][$key] = $val;    
									}
									$s = $new;
								}
							?>
							  <tr>
									<td><?php echo $t->pp_pi; ?></td>
									<td><?php echo date('t/m/Y', strtotime($t->ticket_date)); ?></td>
									<td><?php echo $t->vehicle; ?></td>
									<td><?php echo $t->dn->name; ?></td>
									<td><?php echo $t->dn->store_name; ?></td>
									<td><?php echo $t->dn->region->name; ?></td>
									<td><?php echo money_format('%.2n', $t->cost); ?></td>
									<td><?php echo $s[0]['corporate_name'] ?></td>
									<td><?php echo $s[0]['cnpj'] ?></td>
									<td><?php echo $s[0]['supplier_address'] ?></td>
									<td><?php echo @$s[0]['number'] ?></td>
									<td><?php echo @$s[0]['complement'] ?></td>
									<td><?php echo @$s[0]['neighborhood'] ?></td>
									<td><?php echo @$s[0]['cep'] ?></td>
									<td><?php echo $s[0]['city'] ?></td>
									<td><?php echo $s[0]['state'] ?></td>
									<td><?php echo $s[0]['phone'] ?></td>
									<td><?php echo @$s[0]['email'] ?></td>
									<td><?php echo $t->status; ?></td>
									<td><?php echo $t->tType == 'mid' ? 'M&iacute;dia' : 'Produ&ccedil;&atilde;o'; ?></td>
							  </tr>
							  <tr>
									<td><?php echo $t->pp_pi; ?></td>
									<td><?php echo date('t/m/Y', strtotime($t->dt_created)); ?></td>
									<td><?php echo $t->vehicle; ?></td>
									<td><?php echo $t->dn->name; ?></td>
									<td><?php echo $t->dn->store_name; ?></td>
									<td><?php echo $t->dn->region->name; ?></td>
									<td><?php echo money_format('%.2n', ($t->ag_fee + $t->op_fee)); ?></td>
									<td>DUPLO M COMUNICAÇAO E MARKETING LTDA.</td>
									<td>01.606.903/0001-83</td>
									<td>AV. IPIRANGA</td>
									<td>6929</td>
									<td>3 ANDAR</td>
									<td>Jardim Botânico</td>
									<td>91530-001</td>
									<td>PORTO ALEGRE</td>
									<td>RS</td>
									<td>(51) 3322.3334</td>
									<td>contato@agenciaduplo.com.br</td>
									<td><?php echo $t->status; ?></td>
									<td><?php echo $t->tType == 'mid' ? 'M&iacute;dia' : 'Produ&ccedil;&atilde;o'; ?></td>
							  </tr>
							<?php } ?>
							</tbody>
						</table>
				
				<?php
				
				$data = ob_get_clean();

				$file="ReportDN.xls";

				header("Pragma: public");

				header("Content-type: application/vnd.ms-excel; charset=utf-8");

				header("Content-Disposition: attachment; filename=$file");

				echo $data;
				die;
				
				break;
			
			case 'consolidado':
				$view = View::factory('manager/reports/report_consolidado');
				
				if(Auth::instance()->logged_in('admin')){

					$reports = ORM::factory('User')
								->where('attendant', '!=', DB::expr("''"))
								->find_all();
					
				} else if (Auth::instance()->logged_in('committee')){
			
					//$regions = '(' . implode(',', Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id')) . ')';
					
					$reports = ORM::factory('User')
								->where('attendant', '!=', DB::expr("''"))
								//->and_where('region_id', 'IN', DB::expr($regions))
								->find_all();
				}

				$view->tDate = $tDate;
				break;
				
			case 'duplo':
				
				if(Auth::instance()->logged_in('admin')){

					$reports = ORM::factory('Ticket')
								->where('status', '!=', 'to-confirm')
								->and_where('status', '!=', 'cancelar')
								->and_where('dt_created', 'BETWEEN', DB::expr("'$sDate' AND '$eDate'"))
								->and_where('ticket_date', '=', DB::expr("'$tDate'"))
								->find_all();
					
				} else if (Auth::instance()->logged_in('committee')){
			
					$regions = '(' . implode(',', Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id')) . ')';
					
					$reports = ORM::factory('Ticket')
								->where('status', '!=', 'to-confirm')
								->and_where('status', '!=', 'cancelar')
								->and_where('region_id', 'IN', DB::expr($regions))
								->and_where('dt_created', 'BETWEEN', DB::expr("'$sDate' AND '$eDate'"))
								->and_where('ticket_date', '=', DB::expr("'$tDate'"))
								->find_all();
				}
				
				ob_start(); ?>
		
				<table>
					<thead>
					  <tr>
						<th>Data da cria&ccedil;&atilde;o</th>
						<th>Status</th>
						<th>ID</th>
						<th>DN</th>
						<th>PP/PI</th>
						<th>C&oacute;digo Solicita&ccedil;&atilde;</th>
						<th>M&ecirc;s Compet&ecirc;ncia</th>
						<th>Campanha</th>
						<th>Ve&iacute;culo</th>
						<th>Valor do Ve&iacute;culo</th>
						<th>NF Ve&iacute;culo</th>
						<th>NF DPM</th>
					  </tr>
					</thead>   
					<tbody>
					
					<?php 
						setlocale(LC_MONETARY, 'pt_BR');
						foreach($reports as $t){ ?>
					  <tr>
						<td><?php echo date('m/Y', strtotime($t->dt_created)); ?></td>
						<td><?php echo $t->status; ?></td>
						<td><?php echo $t->dn->id; ?></td>
						<td><?php echo $t->dn->name; ?></td>
						<td><?php echo $t->pp_pi; ?></td>
						<td><?php echo $t->id; ?></td>
						<td><?php echo date('m/Y', strtotime($t->ticket_date)); ?></td>
						<td><?php echo $t->book->campaign->name; ?></td>
						<td><?php echo $t->vehicle; ?></td>
						<td><?php echo money_format('%.2n', $t->cost + $t->ag_fee + $t->op_fee); ?></td>
						<td><?php echo $t->nf_vehicle; ?></td>
						<td><?php echo $t->nf_dpm; ?></td>
					  </tr>
					<?php } ?>
					</tbody>
				</table>

				<?php 
				$data = ob_get_clean();

				$file="Report.xls";

				header("Pragma: public");

				header("Content-type: application/vnd.ms-excel");

				header("Content-Disposition: attachment; filename=$file");

				echo $data;
				die;
				
				break;
			
			default: break;
		}

		
		Session::instance()->set('tDate', $tDate);
		Session::instance()->set('export', $reports->as_array());
		/*echo '<pre>';
		foreach($reports as $r){
			if($r->id == 166){
				$i = $r->tickets->select(DB::expr('SUM(ticket.cost) AS `sumaprov`'))->where('status', '=', 'aprovado')->and_where('ticket_date', '=', DB::expr("'$tDate'"))->find_all()->as_array();
				print_r($i);
			}
		}
		die();*/
 		$view->reports = $reports;
 
		$this->template->content = $view;
		
	}
}