﻿<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Tickets extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - Solicita&ccedil;&atilde;o de Verba';
    if(!Auth::instance()->logged_in() && $this->request->action() != 'profile'){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
   $view = View::factory('manager/tickets/index');
		
		if(Auth::instance()->logged_in('admin')){

			$tickets = ORM::factory('Ticket')->where('status', '!=', 'to-confirm')
											 ->and_where('status', '!=', 'cancelar')
											 ->order_by('id','DESC')
											 ->find_all();
			
		} else if (Auth::instance()->logged_in('committee')){
	
			$regions = '(' . implode(',', Session::instance()->get('auth_user')->dns->find_all()->as_array(NULL, 'region_id')) . ')';
			
			$tickets = ORM::factory('Ticket')->where('region_id', 'IN', DB::expr($regions))
											 ->and_where('status', '!=', 'to-confirm')
											 ->and_where('status', '!=', 'cancelar')
											 ->and_where('status', '!=', 'Novo')
											 ->order_by('id','DESC')
											 ->find_all();
		
		} else if (Auth::instance()->logged_in('distributor')){
		
			$tickets = ORM::factory('Ticket')->where('user_id', '=', Session::instance()->get('auth_user')->id )
											 ->and_where('status', '!=', 'to-confirm')
											 ->and_where('status', '!=', 'cancelar')
											 ->order_by('id','DESC')
											 ->find_all();
		
		}
		
		$months = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Mar&ccedil;o',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		$view->bind('months', $months)->bind('tickets', $tickets);
		
		$this->template->content = $view;
	}
  
	public function action_new()
	{
		$this->template->title .= ' - novo';
		
		$ticket = ORM::factory('Ticket');
		
		$book = ORM::factory('Book',$this->request->param('id'));
		
		$dns = ORM::factory('User', Auth::instance()->get_user()->id)->dns->find_all()->as_array('id','name');
		$months = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Mar&ccedil;o',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		$years = array(2014 => 2014, 2015 => 2015, 2016 => 2016, 2017 => 2017, 2018 => 2018);

		$states = ORM::factory('State')->find_all()->as_array('id','name');
		$regions = ORM::factory('Region')->find_all()->as_array('id','name');

		$view = View::factory('manager/tickets/new');
		
		$is_duplicate = false;
		
		$fields = $book->fields;
		$dn = null;
		$files = null;
		$suppliers = array();
		$abrafor_checked = false;
		$distribuidor_checked = false;
		
		$view
		  ->bind('dns', $dns)
		  ->bind('dn', $dn)
		  ->bind('book', $book)
		  ->bind('files', $files)
		  ->bind('ticket', $ticket)
		  ->bind('is_duplicate', $is_duplicate)
		  ->bind('regions', $regions)
		  ->bind('states', $states)
		  ->bind('months', $months)
		  ->bind('years', $years)
		  ->bind('fields', $fields)
		  ->bind('abrafor_checked', $abrafor_checked)
		  ->bind('distribuidor_checked', $distribuidor_checked)
		  ->bind('s', $suppliers)
		  ->bind('errors', $errors);
		
		$this->template->content = $view;
	}
  
	public function action_duplicate()
	{
		$this->template->title .= ' - duplicar';
		
		$ticket = ORM::factory('Ticket', $this->request->param('id'));
		
		$dn = json_decode($ticket->dn_info);
		$book = ORM::factory('Book', $ticket->book_id);
		$files = json_decode($ticket->files);
		$fields = json_decode($ticket->fields);
		$suppliers =  $this->rearrange(json_decode($ticket->suppliers));
		
		$dns = ORM::factory('User', Auth::instance()->get_user()->id)->dns->find_all()->as_array('id','name');
		$months = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Mar&ccedil;o',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		$years = array(2014 => 2014, 2015 => 2015, 2016 => 2016, 2017 => 2017, 2018 => 2018);

		$states = ORM::factory('State')->find_all()->as_array('id','name');
		$regions = ORM::factory('Region')->find_all()->as_array('id','name');

		$view = View::factory('manager/tickets/new');
		
		$is_duplicate = true;
		
		if($ticket->billing == 'abrafor' ){
			$abrafor_checked = true;
			$distribuidor_checked = false;
		} else {
			$abrafor_checked = false;
			$distribuidor_checked = true;
		}
		$cancel = $this->request->param('p');
		$view
		  ->bind('cancel', $cancel)
		  ->bind('dns', $dns)
		  ->bind('dn', $dn)
		  ->bind('is_duplicate', $is_duplicate)
		  ->bind('ticket', $ticket)
		  ->bind('files', $files)
		  ->bind('book', $book)
		  ->bind('regions', $regions)
		  ->bind('states', $states)
		  ->bind('months', $months)
		  ->bind('years', $years)
		  ->bind('fields', $fields)
		  ->bind('abrafor_checked', $abrafor_checked)
		  ->bind('distribuidor_checked', $distribuidor_checked)
		  ->bind('s', $suppliers[0])
		  ->bind('errors', $errors);
		
		$this->template->content = $view;
	}
  
	public function action_create()
	{
		$view = View::factory('manager/tickets/new');
		$ticket = ORM::factory('Ticket');
		
		//POST
		if($this->request->method() == Request::POST) {
	
			$dn = ORM::factory('Dn', $this->request->post('dn_id'));
			$book = ORM::factory('Book', $this->request->post('book_id'));

			
			$params = $this->request->post();
			$params['region_id'] = $dn->region_id;
			$params['user_id'] = Session::instance()->get('auth_user')->id;
			$params['cost'] = str_replace('.','',$params['cost']);
			$params['cost'] = str_replace(',','.',$params['cost']);
			$val = $params['cost'];
			if($params['tType'] == 'mid'){
			
				//$params['ag_fee'] = ((($val/100) * 25) + $val)/100 * 10;
				$params['ag_fee'] = ($val)/100 * 10;
			
			} else if ($params['tType'] == 'prod'){
			
				//$params['ag_fee'] = ((($val/100) * 15) + $val)/100 * 10;
				$params['ag_fee'] = ($val)/100 * 10;
				
			}
			$params['op_fee'] = 18;
			$params['status'] = 'to-confirm';
	
			//DADOS DA DN
			$params['dn_info'] = json_encode($this->request->post('dn_info'));

			//DADOS DA PECA
			$params['book_info'] = json_encode($this->request->post('book_info'));
			
			//DADOS DO FORNECEDOR
			$suppliers = $this->request->post('supplier');
			$params['suppliers'] = json_encode($suppliers);

			//TRATANDO OS CAMPOS CUSTOMIZADOS
			$labels = json_decode($book->fields);
			foreach($params['fields'] as $k => $v){
			
				$fields[$k]['label'] = $labels[$k];
				$fields[$k]['value'] = $v;

			}
			$params['fields'] = json_encode($fields);
			
			//FORMATANDO A DATA DE VEICULACAO PARA SALVAR NO BD
			$params['launch_date'] = date('Y-m-d', strtotime(str_replace('/', '-',$params['launch_date'])));
			
			//FORMATANDO O MES COMPETENCIA PARA SALVAR NO BD
			$date = $params['year'].'-'.$params['month'].'-01';
			$params['ticket_date'] = date('Y-m-d', strtotime($date));
			
			//DATA VENCIMENTO
			$dueDate = $params['year'].'-'.($params['month']).'-15';
			$params['due_date'] = date('Y-m-28', strtotime('+1 month', strtotime($dueDate)));

			if(file_exists($_FILES['files']['tmp_name'][0]) || is_uploaded_file($_FILES['files']['tmp_name'][0])){
				$arr = $this->rearrange($_FILES['files']);

				//UPLOAD ARQUIVOS
				$files = array();
				foreach($arr as $k => $v){
					$file = Upload::save($v, NULL, DOCROOT.'/public/uploads/tickets/files/');
						
					$file = explode('/', $file);
					$files[$k] = $file[count($file)-1];
				}
				
				if($this->request->post('is_duplicate')){
					$duplicate_of = ORM::factory('Ticket', $this->request->post('duplicate_of'));
					
					$files = array_merge($files, json_decode($duplicate_of->files,true));
				}
				
				$params['files'] = json_encode($files);
			}
			try {	
			
				foreach($params['supplier']['cnpj'] as $k => $v){

					$supplier = ORM::factory('Supplier')->where('cnpj', '=', $v)->find();
				
					$supplier->cnpj = $params['supplier']['cnpj'][$k];
					$supplier->corporate_name = $params['supplier']['corporate_name'][$k];
					$supplier->address = $params['supplier']['supplier_address'][$k];
					$supplier->number = $params['supplier']['supplier_number'][$k];
					$supplier->complement = $params['supplier']['supplier_complement'][$k];
					$supplier->neighborhood = $params['supplier']['supplier_neighborhood'][$k];
					$supplier->cep = $params['supplier']['supplier_cep'][$k];
					$supplier->city = $params['supplier']['city'][$k];
					$supplier->state = $params['supplier']['state'][$k];
					$supplier->ie = $params['supplier']['ie'][$k];
					$supplier->im = $params['supplier']['im'][$k];
					$supplier->name = $params['supplier']['name'][$k];
					$supplier->email = $params['supplier']['email'][$k];
					$supplier->phone = $params['supplier']['phone'][$k];

					$supplier->save();
				}
				
				$t = $ticket->create_ticket($params);

				if($this->request->param('p') == 'cancel'){
					$del = ORM::factory('Ticket', $this->request->param('id'));
					
					$del->status = 'cancelar';
					$del->save();
				}
				
				$this->redirect('manager/tickets/confirm/'.$t->id);
			} 
			catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
				$errors = $e->errors('models');
			}
		}

		$view->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_view(){

		$view = View::factory('manager/tickets/view');
		$ticket = ORM::factory('Ticket', $this->request->param('id'));
		$dn = json_decode($ticket->dn_info);
		$book = json_decode($ticket->book_info);
		$files = json_decode($ticket->files);
		$fields = json_decode($ticket->fields);
		$suppliers =  $this->rearrange(json_decode($ticket->suppliers));
	
		if($ticket->user_id != Session::instance()->get('auth_user')->id)
		{
			$this->redirect('manager');
		}
	
		$view->bind('dn', $dn)
			 ->bind('suppliers', $suppliers)
			 ->bind('fields', $fields)
			 ->bind('ticket', $ticket)
			 ->bind('files', $files)
			 ->bind('book', $book);
		
		$this->template->content = $view;
		
	}
	
	public function action_confirm(){

		$view = View::factory('manager/tickets/confirm');
		$ticket = ORM::factory('Ticket', $this->request->param('id'));
		$dn = json_decode($ticket->dn_info);
		$book = json_decode($ticket->book_info);
		$files = json_decode($ticket->files);
		$fields = json_decode($ticket->fields);
		$suppliers =  $this->rearrange(json_decode($ticket->suppliers));
	
		$view->bind('dn', $dn)
			 ->bind('suppliers', $suppliers)
			 ->bind('fields', $fields)
			 ->bind('ticket', $ticket)
			 ->bind('files', $files)
			 ->bind('book', $book);
		
		$this->template->content = $view;
		
	}
	
	public function action_confirmed()
	{
	
		$this->auto_render = false;
		$this->profiler = false;
		
		$ticket = ORM::factory('Ticket', $this->request->param('id'));
		$ticket->status = 'Novo';
		$ticket->dt_created = date('Y-m-d H:i:s');
		$ticket->save();
		
		$dn = json_decode($ticket->dn_info);
		$book = json_decode($ticket->book_info);
		$files = json_decode($ticket->files);
		$fields = json_decode($ticket->fields);
		$suppliers =  $this->rearrange(json_decode($ticket->suppliers));
		
		$html = 'Pedido #'.$ticket->id;

		// PDF
		$pdf = new PDF(array(
			'title' => 'PDF.php Demo',
			'author' => 'Shreve',
			'subject' => 'PDF Generation',
			'keywords' => 'github demo pdf class'
		));
		
		$pdf->item_header_row(array(
			'Dados da DN' => 2,
			'---' => 3
		));

		
		$rows[] =	array('Solicitação Nro.', $ticket->id);
		$rows[] =	array('DN', $ticket->dn->name);
		$rows[] = 	array('Tipo', $dn->type);
		$rows[] = 	array('Cidade', $dn->city);
		$rows[] = 	array('Estado', $dn->state);
		$rows[] = 	array('Regional', $dn->region);
		$rows[] = 	array('Nome Fantasia', $dn->store_name);
		$rows[] = 	array('Responsável', $dn->name);
		$rows[] = 	array('Telefone', $dn->phone);
		$rows[] = 	array('E-mail', $dn->email);
		$rows[] = 	array('Destinatário', $dn->attendant);
		$rows[] = 	array(' ', ' ');
		$rows[] = 	array('Dados da Campanha', 'header-row');
		$rows[] = 	array(' ', ' ');
		$rows[] = 	array('Título', $book->campaign_title);
		$rows[] = 	array('Mês competência', date('m/y',strtotime($ticket->ticket_date)));
		$rows[] = 	array(' ', ' ');
		$rows[] = 	array('Dados da Peça', 'header-row');
		$rows[] = 	array(' ', ' ');
		$rows[] = 	array('Peça', $book->book_name);
		$rows[] = 	array('Mini Briefing', $book->book_description);

		
		if(count($fields) > 0 && !empty($fields)){
			foreach($fields as $k => $v){
				$rows[] = array($v->label, $v->value);
			}
		}
			$rows[] = array('Quantidade', $ticket->quantity);
			$rows[] = array('Veículo/Produto', $ticket->vehicle);
			$rows[] = array('Nome do Veículo', $ticket->vehicle_name);
			$rows[] = array('Data da veiculação', $ticket->launch_date);
			$rows[] = array('Custo da veiculação', "R$ ".number_format($ticket->cost,2,",",""));
			$rows[] = array('Observações', $ticket->obs);
			
			/*for($i=0;$i<=10;$i++){
				$rows[] = array('', '');
 			}*/
		if(count($files) > 0 && !empty($files)){
			foreach($files as $k => $f){
				$rows[] = array('Arquivo '.$k, $f);
				$rows[] = array('', '');
			}
		}
		if(count($suppliers) > 0 && !empty($suppliers)){
			foreach($suppliers as $s){
				$rows[] = array(' ',' ');
				$rows[] = array('Fornecedor', 'header-row');
				$rows[] = array(' ',' ');
				$rows[] = array('CNPJ', $s['cnpj']);
				$rows[] = array('Razão Social',$s['corporate_name']);
				$rows[] = array('Endereço', $s['supplier_address'].', '.$s['supplier_number'].' '.$s['supplier_complement']);
				$rows[] = array('Cidade', $s['city']);
				$rows[] = array('Estado', $s['state']);
				$rows[] = array('IE', $s['ie']);
				$rows[] = array('IM', $s['im']);
				$rows[] = array('Responsável', $s['name']);
				$rows[] = array('Telefone', $s['phone']);
			}
		}
		
		
		$rows[] = array('--------------------','----------------------------');
		$rows[] = array('Vencimento', date('d/m/Y', strtotime($ticket->due_date)));
		$rows[] = array('Valor pedido', "R$ ".number_format($ticket->cost,2,",",""));
		$rows[] = array('Honorários agência', "R$ ".number_format($ticket->ag_fee,2,",",""));
		$rows[] = array('Operação do sistema', "R$ ".number_format($ticket->op_fee,2,",",""));
		$rows[] = array('Valor total',  "R$ ".number_format($ticket->cost + $ticket->op_fee + $ticket->ag_fee,2,",",""));
		$rows[] = array('Fatura contra', $ticket->billing);
		
		foreach($rows as $k => $r)
		{
		
			if(($k % 80) == 0 && $k != 0){
				$pdf->new_page();
			} else if($rows[$k-1][0] == 'Observações'){
				$pdf->new_page();
			}
			
			if($r[1] == 'header-row'){
			
				$pdf->item_header_row(array(
					$r[0] => 2,
					'---' => 3
				));
			
			} else {
			
				$pdf->item_row(array(
					$r[0] => 2,
					$r[1] => 3
				));
				
			}
		}
		
		$attatch = $pdf->render(DOCROOT.'public/uploads/tickets/pdf/'.$ticket->id);

		$admins = ORM::factory('User')->where('attendant', '=', 0)->find_all();

		foreach($admins as $a){
			Mailer::instance()
					->to($a->email)
					->from(array('no-reply@bookford.com.br' => 'Book Abrafor'))
					->subject('[ABRAFOR] - Solicitação de Verba - #'.$ticket->id)
					->attachments(array($attatch))
					->html($html)
					->send();
		}
		
		Notices::add('success', 'Solicita&ccedil;&atilde;o gerada com sucesso com sucesso');
		$this->redirect('manager/tickets');
	}
	
	public function action_get_dn()
	{
		$this->auto_render = false;
		$this->profiler = false;
		
		$dn = ORM::factory('Dn', $this->request->post('id'))->as_array();
		$att = ORM::factory('User', Auth::instance()->get_user()->attendant);
		
		$dn['state'] = ORM::factory('State', $dn['state_id'])->name;
		$dn['region'] = ORM::factory('Region', $dn['region_id'])->name;
		$dn['attendant'] = $att->name;
		$dn['phone'] = Auth::instance()->get_user()->telefone;
		$dn['email'] = Auth::instance()->get_user()->email;
		
		print_r(json_encode($dn));
		
	}
	
	public function action_get_supplier()
	{
		$this->auto_render = false;
		$this->profiler = false;
		
		$supplier = ORM::factory('Supplier')->where('cnpj', '=', $this->request->post('cnpj'))->find();
		
		if($supplier->loaded()){
			print_r(json_encode($supplier->as_array()));
		} else {
			echo 'notfound';
		}
		
	}
	
	public function action_status()
	{
		$view = View::factory('manager/tickets/status');
		$ticket = ORM::factory('Ticket', $this->request->param('id'));
		$log = ORM::factory('Log');
		$dn = json_decode($ticket->dn_info);
		$book = json_decode($ticket->book_info);
		$files = json_decode($ticket->files);
		$fields = json_decode($ticket->fields);
		$suppliers =  $this->rearrange(json_decode($ticket->suppliers));

		$months = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Mar&ccedil;o',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		$years = array(2014 => 2014, 2015 => 2015, 2016 => 2016, 2017 => 2017, 2018 => 2018);


		if(Auth::instance()->logged_in('admin')){
		
			$status = array(
				'aguardando' => 'Aguardando Aprova&ccedil;&atilde;o',
				'aprovado' => 'Aprovado',
				'reprovado'=> 'Reprovado',
				'processando'=> 'Processando',
				'cancelar' => 'Cancelar'
			);
			
			if($ticket->status == 'Novo'){
				Arr::unshift($status, '','Novo');
			}
			
			$disabled = 'enabled';
			
		} else if (Auth::instance()->logged_in('committee')){
	
			$status = array(
				'aprovado' => 'Aprovado',
				'reprovado'=> 'Reprovado'
			);
			
			$disabled = 'disabled';
		
		} else if (Auth::instance()->logged_in('distributor')){
			
			$this->redirect('manager/tickets');
			
		}
		
		/* POST */
		if($this->request->method() == Request::POST) {
			
			$pStatus = $this->request->post('status');
			
			//if($pStatus != $ticket->status){
			
				//Mudou status

				//PELO ADMIN
				if(Auth::instance()->logged_in('admin')){

					$message = Auth::instance()->get_user()->name.' alterou o status para '.$status[$pStatus].' - '.$this->request->post('obs');
				
					//FORMATANDO O MES COMPETENCIA PARA SALVAR NO BD
					$date = $this->request->post('year').'-'.$this->request->post('month').'-01';
					$ticket->ticket_date = date('Y-m-d', strtotime($date));
					
					//DATA VENCIMENTO
					$dueDate = $this->request->post('year').'-'.($this->request->post('month')).'-15';
					$ticket->due_date = date('Y-m-28', strtotime('+1 month', strtotime($dueDate)));

					//$ticket->launch_date = date('Y-m-d', strtotime(str_replace('/', '-',$this->request->post('launch_date'))));
					$ticket->status = $pStatus;
					
					// VALOR

					//echo $params['cost'] = $this->request->post('cost');

					//echo "<br>"; 


					$params['cost'] = str_replace('.','',$this->request->post('cost'));
					$params['cost'] = str_replace(',','.',$params['cost']);
					
					$val = $params['cost'];
					
					//if($this->request->post('tType') == 'mid'){
					
						//$params['ag_fee'] = ((($val/100) * 25) + $val)/100 * 10;
						//$ticket->ag_fee = ($val)/100 * 10;
					
					//} else if ($this->request->post('tType') == 'prod'){
					
						//$params['ag_fee'] = ((($val/100) * 15) + $val)/100 * 10;
						//$ticket->ag_fee = ($val)/100 * 10;
						
					//}
					
					$ticket->ag_fee = ($val)/100 * 10;

					$ticket->op_fee = 18;
					//echo $val; die();
					$ticket->cost = $val;

					$ticket->pp_pi = $this->request->post('pp_pi');
					$ticket->nf_vehicle = $this->request->post('nf_vehicle');
					$ticket->nf_dpm = $this->request->post('nf_dpm');
					$ticket->save();
					
					$rid = $ticket->region_id;
					
					//Seleciona o comite regional
					$users = ORM::factory('User')
									->join('dns_users','INNER')
									->on('dns_users.user_id', '=', 'user.id')
									->join('dns','INNER')
									->on('dns.id', '=', 'dns_users.dn_id')
									->join('roles_users','INNER')
									->on('roles_users.user_id', '=', 'user.id')
									->on('roles_users.role_id', '=', DB::expr(5))
									->where('dns.region_id', '=', $rid)
									->group_by('user.email')
									->find_all();
									
					
					if($pStatus == 'aguardando'){
						
						//$pdf = DOCROOT.'public/uploads/tickets/pdf/'.$ticket->id.'.pdf';
						//$file = Upload::save($_FILES['attatch'], $_FILES['attatch']['name'], DOCROOT.'public/uploads/');
						//$comment = $this->request->post('mail_comment');
						
						foreach($users as $user){
					
							/*Mailer::instance()
								->to($user->email)
								->from(array('no-reply@bookford.com.br' => 'Book Abrafor'))
								->subject('[ABRAFOR]Solicitação de verba - #'. $ticket->id.' - '.$ticket->dn->name)
								->attachments(array($file,$pdf))
								->html($comment.' - '.$message)
								->send();*/
								
						}
						
					} else {
					
						foreach($users as $user){
					
							/*Mailer::instance()
								->to($user->email)
								->from(array('no-reply@bookford.com.br' => 'Book Abrafor'))
								->subject('[ABRAFOR]Solicitação de verba - #'. $ticket->id.' - '.$ticket->dn->name)
								->html($message)
								->send();*/
								
						}
					}
					
					// PDF 

					// PDF
						$pdf = new PDF(array(
							'title' => 'PDF.php Demo',
							'author' => 'Shreve',
							'subject' => 'PDF Generation',
							'keywords' => 'github demo pdf class'
						));
						
						$pdf->item_header_row(array(
							'Dados da DN' => 2,
							'---' => 3
						));

						
						$rows[] =	array('Solicitação Nro.', $ticket->id);
						$rows[] =	array('DN', $ticket->dn->name);
						$rows[] = 	array('Tipo', $dn->type);
						$rows[] = 	array('Cidade', $dn->city);
						$rows[] = 	array('Estado', $dn->state);
						$rows[] = 	array('Regional', $dn->region);
						$rows[] = 	array('Nome Fantasia', $dn->store_name);
						$rows[] = 	array('Responsável', $dn->name);
						$rows[] = 	array('Telefone', $dn->phone);
						$rows[] = 	array('E-mail', $dn->email);
						$rows[] = 	array('Destinatário', $dn->attendant);
						$rows[] = 	array(' ', ' ');
						$rows[] = 	array('Dados da Campanha', 'header-row');
						$rows[] = 	array(' ', ' ');
						$rows[] = 	array('Título', $book->campaign_title);
						$rows[] = 	array('Mês competência', date('m/y',strtotime($ticket->ticket_date)));
						$rows[] = 	array(' ', ' ');
						$rows[] = 	array('Dados da Peça', 'header-row');
						$rows[] = 	array(' ', ' ');
						$rows[] = 	array('Peça', $book->book_name);
						$rows[] = 	array('Mini Briefing', $book->book_description);

						
						if(count($fields) > 0 && !empty($fields)){
							foreach($fields as $k => $v){
								$rows[] = array($v->label, $v->value);
							}
						}
							$rows[] = array('Quantidade', $ticket->quantity);
							$rows[] = array('Veículo/Produto', $ticket->vehicle);
							$rows[] = array('Nome do Veículo', $ticket->vehicle_name);
							$rows[] = array('Data da veiculação', $ticket->launch_date);
							$rows[] = array('Custo da veiculação', "R$ ".number_format($ticket->cost,2,",",""));
							$rows[] = array('Observações', $ticket->obs);
						if(count($files) > 0 && !empty($files)){
							foreach($files as $k => $f){
								$rows[] = array('Arquivo '.$k, $f);
							}
						}
						if(count($suppliers) > 0 && !empty($suppliers)){
							foreach($suppliers as $s){
								$rows[] = array(' ',' ');
								$rows[] = array('Fornecedor', 'header-row');
								$rows[] = array(' ',' ');
								$rows[] = array('CNPJ', $s['cnpj']);
								$rows[] = array('Razão Social',$s['corporate_name']);
								$rows[] = array('Endereço', $s['supplier_address']);
								$rows[] = array('Cidade', $s['city']);
								$rows[] = array('Estado', $s['state']);
								$rows[] = array('IE', $s['ie']);
								$rows[] = array('IM', $s['im']);
								$rows[] = array('Responsável', $s['name']);
								$rows[] = array('Telefone', $s['phone']);
							}
						}
						
						
						$rows[] = array('--------------------','----------------------------');
						$rows[] = array('Vencimento', date('d/m/Y', strtotime($ticket->due_date)));
						$rows[] = array('Valor pedido', "R$ ".number_format($ticket->cost,2,",",""));
						$rows[] = array('Honorários agência', "R$ ".number_format($ticket->ag_fee,2,",",""));
						$rows[] = array('Operação do sistema', "R$ ".number_format($ticket->op_fee,2,",",""));
						$rows[] = array('Valor total',  "R$ ".number_format($ticket->cost + $ticket->op_fee + $ticket->ag_fee,2,",",""));
						$rows[] = array('Fatura contra', $ticket->billing);
						
						foreach($rows as $k => $r)
						{
						
							if(($k % 40) == 0 && $k != 0){
								$pdf->new_page();
							}
							
							if($r[1] == 'header-row'){
							
								$pdf->item_header_row(array(
									$r[0] => 2,
									'---' => 3
								));
							
							} else {
							
								$pdf->item_row(array(
									$r[0] => 2,
									$r[1] => 3
								));
								
							}
						}
						
						$attatch = $pdf->render(DOCROOT.'public/uploads/tickets/pdf/'.$ticket->id);
					
					$this->redirect('manager/tickets');
				} 
				//PELO COMITE
				else if(Auth::instance()->logged_in('committee')){
					
					$attendant = ORM::factory('User', $ticket->owner->attendant);
					
					$s = ($pStatus == 'aprovado') ? ' deu OK ' : ' reprovou ';
					
					$message = Auth::instance()->get_user()->name.$s.' - '.$this->request->post('obs');
					
					/*Mailer::instance()
							->to($attendant->email)
							->from(array('no-reply@bookford.com.br' => 'Book Abrafor'))
							->subject('[ABRAFOR]Ticket #'. $ticket->id)
							//->attachments(array($attatch))
							->html($message)
							->send();*/
							
					$this->redirect('manager/tickets');
				}
				
			//} else {
				//Apenas comentou
				$message = Auth::instance()->get_user()->name.' - '.$this->request->post('obs');
				
			//}
			
			$params = array(
				'ticket_id' => $ticket->id,
				'log_date' => date('Y-m-d H:i:s'),
				'message' => $message
			);
			
			$log->create_log($params);
			
		}
	
		$logs = ORM::factory('Log')->where('ticket_id', '=', $ticket->id)->find_all()->as_array();
	
		$view->bind('dn', $dn)
			 ->bind('disabled', $disabled)
			 ->bind('logs', $logs)
			 ->bind('status', $status)
			 ->bind('suppliers', $suppliers)
			 ->bind('fields', $fields)
			 ->bind('ticket', $ticket)
			 ->bind('files', $files)
			 ->bind('months', $months)
			 ->bind('years', $years)
			 ->bind('book', $book);
	

	
		$this->template->content = $view;
	}
	
	public function rearrange( $arr )
	{
		if(empty($arr)){ return false; }
		foreach( $arr as $key => $all ){
			foreach( $all as $i => $val ){
				$new[$i][$key] = $val;    
			}    
		}
		return $new;
	}

} // End Manager users