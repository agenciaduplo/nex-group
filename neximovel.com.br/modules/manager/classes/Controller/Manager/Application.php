<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Application extends Controller_Template {

  public $template = 'manager/templates/layout';
  public $view = null;
  public $title = 'Manager';

  public function before(){
  parent::before();

	if (!(Auth::instance()->logged_in(array('committee')) || Auth::instance()->logged_in(array('admin')) || Auth::instance()->logged_in(array('distributor'))) 
		&& ($this->request->controller() != 'sessions' && $this->request->action() != 'new') 
		&& ($this->request->controller() != 'Forgotpassword')
		&& ($this->request->controller() != 'Contact')) {
			return $this->redirect('manager/login');
	}
    $this->template->title = $this->title;
  }
}
