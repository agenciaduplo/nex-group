<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Properties extends Controller_Manager_Application {
	
	public function before()
	{
		parent::before();

		$this->template->title .= 'Imóvel';
		if(!Auth::instance()->logged_in('admin')){
		  $this->redirect('manager/login');
		}

	}

	public function action_index()
	{
		$view = View::factory('manager/properties/index');
		$view->properties = ORM::factory('Property')->order_by('id', 'DESC')->find_all();
		$this->template->content = $view;
	}
	
	public function action_new()
	{
		$this->template->title .= ' - novo';

		$view = View::factory('manager/properties/new');
		$view->property = ORM::factory('Property');
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		array_unshift($view->states, 'Selecione o estado');
		$view->cities = array('0' => 'Selecione o estado');
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		
		$view->splashes = ORM::factory('Splash')->find_all()->as_array('id', 'title');
		array_unshift($view->splashes, 'Selecione');
		
		$view->banners = ORM::factory('Banner')->where('enabled', '=', 1)->find_all();
		$view->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_edit()
	{
		$this->template->title .= ' - editar';

		$view = View::factory('manager/properties/edit');

		$property = ORM::factory('Property', $this->request->param('id'));
		
		//echo '<pre>'; print_r($property->city);
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		array_unshift($view->states, 'Selecione o estado');
		
		$view->cities = ORM::factory('City')->where('state_id', '=', $property->city->state->id)->find_all()->as_array('id','name');
		//array_unshift($view->cities, 'Selecione a cidade');
		
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
		
		$view->splashes = ORM::factory('Splash')->find_all()->as_array('id', 'title');
		array_unshift($view->splashes, 'Selecione');
		
		$view->bind('errors', $errors);

		$view->related = json_decode($property->related);
		$view->banners = ORM::factory('Banner')->where('enabled', '=', 1)->find_all();
		$view->property = $property;
		$this->template->content = $view;
	}
	
	public function action_create()
	{
		$property = ORM::factory('Property');
		
		$view = View::factory('manager/properties/new');
	
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		$view->cities = array('0' => 'Selecione um estado');
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
	
	
		$params = $this->request->post();
		
		$type = ORM::factory('Type', $params['type_id']);
		$params['value'] = str_replace(',','.', str_replace('.', '', $params['value']));
		if(!empty($params['old_value'])){
			$params['old_value'] = str_replace(',','.', str_replace('.', '', $params['old_value']));
		}
		
		if($this->request->method() == Request::POST) {
			
			$property_title = $type->name;
		
			if($params['beds'] == 1 ){
				$property_title .= ' ' . $params['beds'] . ' dormitório';
			} else if($params['beds'] > 1 ){
				$property_title .= ' ' . $params['beds'] . ' dormitórios';
			}

			$city = ORM::factory('City', $params['city_id']);
			$params['city_name'] = $city->name;
			$params['name_index'] = $city->name .' '.$params['neighborhood'];
			
			$params['related'] = json_encode($params['related']);
			
			$property_title .= ' - ' . $params['neighborhood'];
			
			$params['title'] = $property_title;
			
			$property->create_property($params);
			
			if($property->id){
			
				foreach($params['media'] as $k => $v){
					$property->add('medias', $v);
				}
			
				if(!empty($params['banners'])){
					foreach($params['banners'] as $k => $v){
						$property->add('banners', $v);
					}
				}
			
				Notices::add('success', 'Im&oacute;vel adicionado com sucesso');
				$this->redirect('manager/properties');
				
			} else {
			
				Notices::add('error', 'Erro. Tente novamente');
				
			}
		}

		$view->bind('errors', $errors);

		$this->template->content = $view;
	}

	public function action_update()
	{
		$property = ORM::factory('Property', $this->request->param('id'));
		
		$view = View::factory('manager/properties/new');
	
		$view->states = ORM::factory('State')->find_all()->as_array('id', 'name');
		$view->cities = array('0' => 'Selecione um estado');
		$view->types = ORM::factory('Type')->find_all()->as_array('id', 'name');
	
		$params = $this->request->post();
		
		$city = ORM::factory('City', $params['city_id']);
		$params['city_name'] = $city->name;
		$params['name_index'] = $city->name .' '.$params['neighborhood'];
		
		$params['related'] = json_encode($params['related']);
		
		$params['value'] = str_replace(',','.', str_replace('.', '', $params['value']));
		if(!empty($params['old_value'])){
			$params['old_value'] = str_replace(',','.', str_replace('.', '', $params['old_value']));
		}
		
		if(!isset($params['sold'])){
			$params['sold'] = 0;
		} else {
			$params['dt_sale'] = date('Y-m-d');
		}
		
		if(!isset($params['highlight'])){
			$params['highlight'] = 0;
		}
		
		if(!isset($params['neximovel'])){
			$params['neximovel'] = 0;
		}
		
		if(!isset($params['coberturas'])){
			$params['coberturas'] = 0;
		}
		
		if(!isset($params['compromisso'])){
			$params['compromisso'] = 0;
		}
		
		if(!isset($params['enabled'])){
			$params['enabled'] = 0;
		}
		
		//print_r($params); die;
		if($this->request->method() == Request::POST) {
			
			$property->update_property($params);
			
			if($property->id){
			
				if(!empty($params['media'])){
					foreach($params['media'] as $k => $v){
						$property->add('medias', $v);
					}
				}
				if(!empty($params['delMedia'])){
					foreach($params['delMedia'] as $k => $v){
						$property->remove('medias', $v);
					}
				}
				
				$property->remove('banners');
				
				if(!empty($params['banners'])){
					foreach($params['banners'] as $k => $v){
						$property->add('banners', $v);
					}
				}
				
				Notices::add('success', 'Im&oacute;vel adicionado com sucesso');
				$this->redirect('manager/properties');
				
			} else {
			
				Notices::add('error', 'Erro. Tente novamente');
				
			}
		}

		$view->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_upload()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		if(file_exists($_FILES['file']['tmp_name']) || is_uploaded_file($_FILES['file']['tmp_name'])){
			//UPLOAD LOCAL
			$file = Upload::save($_FILES['file'], NULL, DOCROOT.'/public/upload/');
				
			$image = Image::factory($file)->resize(800, NULL)
										   //->crop(200, 200, NULL, FALSE)
										   ->save();
			
			$file = str_replace('\\', '/', $file);			
			$file = explode('/', $file);
			$uploaded_file = str_replace('index.php/', '', URL::base(TRUE, TRUE).'public/upload/'.$file[count($file)-1]);
			$docroot = DOCROOT.'/public/upload/'.$file[count($file)-1];
			
			$media = ORM::factory('Media');
			$media->path = $uploaded_file;
			$media->docroot = $docroot;
			$media->save();
			
			$save_2 = Image::factory($docroot);
			$save_4 = Image::factory($docroot);
			$save_s = Image::factory($docroot);
			
			
			//THUMB SLIDER
			if ( ($save_s->width / $save_s->height) > (190 / 128) )
			{
				$resized_w = (128 / $save_s->height) * $save_s->width;
				$offset_x = round(($resized_w - 190) / 2); 
				$offset_y = 0;
				
				$thumb = $save_s->resize(null, 128)
						 ->crop(190, 128, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_slider.jpg');
					 
				if($thumb){
					$media->thumb_slider = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_slider.jpg';
					$media->save();
				}
				
			} else {
				$resized_h = (190 / $save_s->width) * $save_s->height;
				$offset_x = 0;
				$offset_y = round(($resized_h - 128) / 2); 
				
				if(empty($media->thumb_slider)){
					$thumb = $save_s->resize(190, null)
						 ->crop(190, 128, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_slider.jpg');
						 
					if($thumb){
						$media->thumb_slider = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_slider.jpg';
						$media->save();
					}
					
				}
			}
			
			//THUMB 400
			if ( ($save_4->width / $save_4->height) > (400 / 400) )
			{
				$resized_w = (400 / $save_4->height) * $save_4->width;
				$offset_x = round(($resized_w - 400) / 2); 
				$offset_y = 0;
				
				$thumb = $save_4->resize(null, 400)
						 ->crop(400, 400, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_400.jpg');
					 
				if($thumb){
					$media->thumb_400 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_400.jpg';
					$media->save();
				}
				
			} else {
				$resized_h = (400 / $save_4->width) * $save_4->height;
				$offset_x = 0;
				$offset_y = round(($resized_h - 400) / 2); 
				
				if(empty($media->thumb_400)){
					$thumb = $save_4->resize(400, null)
						 ->crop(400, 400, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_400.jpg');
						 
					if($thumb){
						$media->thumb_400 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_400.jpg';
						$media->save();
					}
					
				}
			}
			
			//THUMB 200
			if ( ($save_2->width / $save_2->height) > (200 / 200) )
			{
				$resized_w = (200 / $save_2->height) * $save_2->width;
				$offset_x = round(($resized_w - 200) / 2); 
				$offset_y = 0;
				
				$thumb = $save_2->resize(null, 200)
						 ->crop(200, 200, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_200.jpg');
					 
				if($thumb){
					$media->thumb_200 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_200.jpg';
					$media->save();
				}
				
			} else {
				$resized_h = (200 / $save_2->width) * $save_2->height;
				$offset_x = 0;
				$offset_y = round(($resized_h - 200) / 2); 
				
				if(empty($media->thumb_200)){
					$thumb = $save_2->resize(200, null)
						 ->crop(200, 200, $offset_x, $offset_y)
						 ->save(DOCROOT.'/public/upload/thumbs/'.$media->id.'_200.jpg');
						 
					if($thumb){
						$media->thumb_200 = URL::base(TRUE, TRUE).'public/upload/thumbs/'.$media->id.'_200.jpg';
						$media->save();
					}
					
				}
			}
			
			
		}
		
		echo json_encode($media->id);
	}
	
	public function action_order()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
				
		$media = ORM::factory('Media', $this->request->post('media'));
		$media->order = $this->request->post('order');
		$media->save();

		echo json_encode('success');
	}
	
	public function action_delete()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$property = ORM::factory('Property', $this->request->param('id'));
			$property->delete();
			echo json_encode('deleted');
			
		}catch(Exception $e){
			echo json_encode($e);
		}
		
		exit;
		
	}
  
}