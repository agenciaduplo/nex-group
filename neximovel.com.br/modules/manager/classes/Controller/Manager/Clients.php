<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Clients extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - Clientes';
    if(!Auth::instance()->logged_in('admin') && $this->request->action() != 'profile'){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
    $view = View::factory('manager/clients/index');
    $view->users = ORM::factory('user')->join('roles_users')
				       ->on('roles_users.user_id', '=', 'user.id')
				       ->where('roles_users.role_id', '=', 5)
				       ->where('user.enabled', '=', 1)
				       ->find_all();
    $this->template->content = $view;
  }
  
/*  public function action_edit()
  {
    $session = Session::instance();
    $user = ORM::factory('User' ,$this->request->param('id'));
    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all();

    $view = View::factory('manager/clients/edit');
    $view
      ->bind('user', $user)
      ->bind('roles', $roles)
      ->bind('errors', $errors);

    $this->template->title .= ' - '.$user->username;
    $this->template->content = $view;
  }
  
  public function action_update()
  {
    $session = Session::instance();
    $user = ORM::factory('User' ,$this->request->param('id'));

    $roles = ORM::factory('Role')->where('name', '<>','login')->find_all();

    if($this->request->method() == Request::POST) {
      try {
        $user->update_user($this->request->post(), array('name', 'email', 'username', 'password', 'cpf', 'rg', 'enabled', 'phone', 'department', 'birthday', 'celphone',
                                                         'zipcode', 'address', 'neighborhood', 'number', 'complement', 'city', 'state', 'companion_name', 'companion_rg',
                                                         'companion_department', 'companion_document', 'companion_birthday'));

        Notices::add('success', 'Cliente alterado com sucesso');
        $this->redirect('manager/clients');
      } catch(ORM_Validation_Exception $e) {
        Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
        $errors = $e->errors('models');
      }
        catch(Kohana_Database_Exception $e) {
        Notices::add('error', 'Email existente, por favor escolha outro');
      }
    }

    $view = View::factory('manager/clients/edit');
    $view
      ->bind('user', $user)
      ->bind('roles', $roles)
      ->bind('errors', $errors);
    
    $this->template->title .= ' - '.$user->username;
    $this->template->content = $view;
  }
*/
}
