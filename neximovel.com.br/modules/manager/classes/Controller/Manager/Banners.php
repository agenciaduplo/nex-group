<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Banners extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - Banner';
		if(!Auth::instance()->logged_in()){
		  $this->redirect('manager/dashboard');
		}
	}
	  
	public function action_index()
	{
		$view = View::factory('manager/banners/index');
		$view->banners = ORM::factory('Banner')->find_all();

		$this->template->content = $view;

	}
	  
	public function action_new()
	{
		$this->template->title .= ' - Adicionar banner';

		$view = View::factory('manager/banners/new');
		$banner = ORM::factory('Banner');
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		
		$view
		  ->bind('places', $places)
		  ->bind('banner', $banner)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_create()
	{

		$banner = ORM::factory('Banner');
		
		$this->template->title .= ' - Adicionar banner';
		$view = View::factory('manager/banners/new');
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST) {
			try {
			
				$banner->create_banner($params); 
			
				if($banner->id){
			
					foreach($params['media'] as $k => $v){
						$banner->add('medias', $v);
					}

					
				} else {
				
					Notices::add('error', 'Erro. Tente novamente');
					
				}
			
				Notices::add('success', 'Banner adicionado com sucesso');
				$this->redirect('manager/banners');
			} 
			catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
				$errors = $e->errors('models');
			}
		}

		$view
		  ->bind('banner', $banner)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_edit()
	{
		$banner = ORM::factory('Banner' ,$this->request->param('id'));
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		$view = View::factory('manager/banners/edit');
		$view
		  ->bind('places', $places)
		  ->bind('banner', $banner)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}
	  
	public function action_update()
	{
		$banner = ORM::factory('Banner' ,$this->request->param('id'));
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		$view = View::factory('manager/banners/edit');
		$view
		  ->bind('banner', $banner)
		  ->bind('errors', $errors);
		
		$params = $this->request->post();
		
		if(!isset($params['enabled'])){
			$params['enabled'] = 0;
		}
		
		if($this->request->method() == Request::POST) {
			
			if($banner->id){
			
				$banner->update_banner($params);
			
				if(!empty($params['places'])){
					foreach($params['places'] as $k => $v){
						$banner->add('places', $v);
					}
				}
			
				if(!empty($params['media'])){
					foreach($params['media'] as $k => $v){
						$banner->add('medias', $v);
					}
				}
				if(!empty($params['delMedia'])){
					foreach($params['delMedia'] as $k => $v){
						$banner->remove('medias', $v);
					}
				}
				Notices::add('success', 'Banner editado com sucesso');
				
			} else {
			
				Notices::add('error', 'Erro. Tente novamente');
				
			}
		}
		
		$view
		  ->bind('places', $places)
		  ->bind('banner', $banner)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}

	public function action_list_banners()
	{
		$user = Session::instance()->get('auth_user');
	
		$reg = $user->dns->group_by('region_id')->find_all()->as_array(null, 'region_id');

		$view = View::factory('manager/banners/list_banners');
		$campaigns = ORM::factory('Campaign')->where('ctype_id', '=', 1)->find_all();

		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('reg', $reg)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_list_brindes()
	{
		$user = Session::instance()->get('auth_user');
		$reg = $user->dns->group_by('region_id')->find_all()->as_array(null, 'region_id');
		
		$view = View::factory('manager/banners/list_banners');
		$campaigns = ORM::factory('Campaign')->where('ctype_id', '=', 2)->find_all();

		
		
		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('reg', $reg)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_preview()
	{
		$view = View::factory('manager/banners/preview');
		
		$banner = ORM::factory('Banner', $this->request->param('id'));
		
		$view->bind('banner', $banner);
		
		$this->template->content = $view;
	}
	
	public function action_delete()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$banner = ORM::factory('Banner', $this->request->param('id'));
			$banner->delete();
			echo json_encode('deleted');
			
		}catch(Exception $e){
			echo json_encode($e);
		}
		
		exit;
		
	}
	
	public function action_upload()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		if(file_exists($_FILES['file']['tmp_name']) || is_uploaded_file($_FILES['file']['tmp_name'])){
			//UPLOAD LOCAL
			$file = Upload::save($_FILES['file'], NULL, DOCROOT.'/public/upload/');
				
			$image = Image::factory($file)->resize(728, 90)
										   //->crop(200, 200, NULL, FALSE)
										   ->save();
			
			$file = str_replace('\\', '/', $file);			
			$file = explode('/', $file);
			$uploaded_file = str_replace('index.php/', '', URL::base(TRUE, TRUE).'public/upload/'.$file[count($file)-1]);
			
			$media = ORM::factory('Media');
			$media->path = $uploaded_file;
			$media->save();
		}
		
		echo json_encode($media->id);
	}
	
} // End Manager banners
