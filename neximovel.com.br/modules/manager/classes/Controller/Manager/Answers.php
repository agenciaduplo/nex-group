<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Answers extends Controller_Manager_Application {
  
  public function before(){
    parent::before();
    $this->template->title .= ' - Respostas';
    if(!Auth::instance()->logged_in('admin') && $this->request->action() != 'profile'){
      $this->request->redirect('manager/dashboard');
    }
  }
  
  public function action_index(){
    $session = Session::instance();
    $view = View::factory('manager/answers/index');
    
    /*
    SELECT *,SUM(peso)
    FROM quantitativas
    INNER JOIN questoes
    ON quantitativas.resposta = questoes.id
    INNER JOIN users
    ON users.id = quantitativas.usuario
    INNER JOIN qualitativas
    ON qualitativas.usuario = users.id
    GROUP BY qualitativas.resposta,quantitativas.create_at
    */
    $view->answers = ORM::factory('Quantitativa')
    ->select( DB::expr('*,SUM(questoes.peso) AS total') )
    ->join('questoes','INNER')
    ->on('quantitativa.resposta','=','questoes.id')  
    ->join('users','INNER')
    ->on('users.id','=','quantitativa.usuario')  
    //->join('qualitativas','INNER')
    //->on('qualitativas.usuario','=','users.id')      
    ->group_by('quantitativa.create_at')
    ->find_all();

    $quali = ORM::factory('Qualitativa')->find_all();

    $view->quali = array();
    foreach ($quali as $value) {
        $view->quali[$value->key]['status']     = $value->status;
        $view->quali[$value->key]['resposta']   = $value->resposta;
        $view->quali[$value->key]['id']         = $value->id;
    }
    
    $this->template->content = $view;
  }
}
