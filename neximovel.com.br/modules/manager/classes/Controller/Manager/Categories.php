<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Categories extends Controller_Manager_Application {
  
  public function before(){
    parent::before();

    $this->template->title .= ' - Categorias';
    if(!Auth::instance()->logged_in('admin')){
      $this->redirect('manager/dashboard');
    }
  }
  
  public function action_index()
  {
    $view = View::factory('manager/categories/index');
    $view->categories = ORM::factory('Category')->find_all();

    $this->template->content = $view;

  }
  
  public function action_new()
  {
    $this->template->title .= ' - Nova Categoria';

    $view = View::factory('manager/categories/new');
    $category = ORM::factory('Category');

    $view
      ->bind('category', $category)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
  
  public function action_create(){

    $categories = ORM::factory('Category');

    $this->template->title .= ' - Nova Categoria';
    $view = View::factory('manager/categories/new');    
      
    if($this->request->method() == Request::POST) {
      try {
        $categories->create_category($this->request->post());
        Notices::add('success', 'Categoria adicionada com sucesso');
        $this->redirect('manager/categories');
      } 
      catch(ORM_Validation_Exception $e) {
        Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
        $errors = $e->errors('models');
      }
    }

    $view
      ->bind('categories', $categories)
      ->bind('errors', $errors);

    $this->template->content = $view;
  }
  
  public function action_edit()
  {
    $category = ORM::factory('Category' ,$this->request->param('id'));
    $view = View::factory('manager/categories/edit');
    $view
      ->bind('category', $category)
      ->bind('errors', $errors);
    $this->template->content = $view;
  }
  
  public function action_update()
  {
    $category = ORM::factory('Category' ,$this->request->param('id'));
    $view = View::factory('manager/categories/edit');
    $view
      ->bind('category', $category)
      ->bind('errors', $errors);
    
    if($this->request->method() == Request::POST) {
      try {
        $category->update_category($this->request->post());
        Notices::add('success', 'Categoria alterada com sucesso');
        $this->redirect('manager/categories');
      } catch(ORM_Validation_Exception $e) {
        Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
        $errors = $e->errors('models');
      }
    }
    $this->template->content = $view;
  }
} // End Manager Categories
