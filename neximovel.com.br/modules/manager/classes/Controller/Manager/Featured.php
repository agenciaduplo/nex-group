<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Manager_Featured extends Controller_Manager_Application {
  
	public function before()
	{
		parent::before();

		$this->template->title .= ' - featured';
		if(!Auth::instance()->logged_in()){
		  $this->redirect('manager/dashboard');
		}
	}
	  
	public function action_index()
	{
		$view = View::factory('manager/featured/index');
		$view->featured = ORM::factory('Featured')->order_by('order', 'ASC')->find_all();

		$this->template->content = $view;

	}
	  
	public function action_new()
	{
		$this->template->title .= ' - Adicionar featured';

		$view = View::factory('manager/featured/new');
		$featured = ORM::factory('Featured');
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		
		$view
		  ->bind('places', $places)
		  ->bind('featured', $featured)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_create()
	{

		$featured = ORM::factory('Featured');
		
		$this->template->title .= ' - Adicionar featured';
		$view = View::factory('manager/featured/new');
		
		$params = $this->request->post();
		
		if($this->request->method() == Request::POST) {
			try {
			
				$featured->create_featured($params); 
			
				if($featured->id){
			
					foreach($params['media'] as $k => $v){
						$featured->add('medias', $v);
					}

					
				} else {
				
					Notices::add('error', 'Erro. Tente novamente');
					
				}
			
				Notices::add('success', 'Destaque adicionado com sucesso');
				$this->redirect('manager/featured');
			} 
			catch(ORM_Validation_Exception $e) {
				Notices::add('error', 'Confira os campos obrigatórios antes de continuar');
				$errors = $e->errors('models');
			}
		}

		$view
		  ->bind('featured', $featured)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	  
	public function action_edit()
	{
		$featured = ORM::factory('Featured' ,$this->request->param('id'));
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		$view = View::factory('manager/featured/edit');
		$view
		  ->bind('places', $places)
		  ->bind('featured', $featured)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}
	  
	public function action_update()
	{
		$featured = ORM::factory('Featured' ,$this->request->param('id'));
		$places = ORM::factory('Place')->find_all()->as_array('id','name');
		
		$view = View::factory('manager/featured/edit');
		$view
		  ->bind('featured', $featured)
		  ->bind('errors', $errors);
		
		$params = $this->request->post();
		
		if(!isset($params['enabled'])){
			$params['enabled'] = 0;
		}
		
		if($this->request->method() == Request::POST) {
			
			if($featured->id){
			
				$featured->update_featured($params);
			
				if(!empty($params['places'])){
					foreach($params['places'] as $k => $v){
						$featured->add('places', $v);
					}
				}
			
				if(!empty($params['media'])){
					foreach($params['media'] as $k => $v){
						$featured->add('medias', $v);
					}
				}
				if(!empty($params['delMedia'])){
					foreach($params['delMedia'] as $k => $v){
						$featured->remove('medias', $v);
					}
				}
				Notices::add('success', 'Destaque editado com sucesso');
				
			} else {
			
				Notices::add('error', 'Erro. Tente novamente');
				
			}
		}
		
		$view
		  ->bind('places', $places)
		  ->bind('featured', $featured)
		  ->bind('errors', $errors);
		$this->template->content = $view;
	}

	public function action_list_featured()
	{
		$user = Session::instance()->get('auth_user');
	
		$reg = $user->dns->group_by('region_id')->find_all()->as_array(null, 'region_id');

		$view = View::factory('manager/featured/list_featured');
		$campaigns = ORM::factory('Campaign')->where('ctype_id', '=', 1)->find_all();

		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('reg', $reg)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_list_brindes()
	{
		$user = Session::instance()->get('auth_user');
		$reg = $user->dns->group_by('region_id')->find_all()->as_array(null, 'region_id');
		
		$view = View::factory('manager/featured/list_featured');
		$campaigns = ORM::factory('Campaign')->where('ctype_id', '=', 2)->find_all();

		
		
		$view
		  ->bind('campaigns', $campaigns)
		  ->bind('reg', $reg)
		  ->bind('errors', $errors);

		$this->template->content = $view;
	}
	
	public function action_preview()
	{
		$view = View::factory('manager/featured/preview');
		
		$featured = ORM::factory('Featured', $this->request->param('id'));
		
		$view->bind('featured', $featured);
		
		$this->template->content = $view;
	}
	
	public function action_delete()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		try{
			
			$featured = ORM::factory('Featured', $this->request->param('id'));
			$featured->delete();
			echo json_encode('deleted');
			
		}catch(Exception $e){
			echo json_encode($e);
		}
		
		exit;
		
	}
	
	public function action_upload()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
		
		if(file_exists($_FILES['file']['tmp_name']) || is_uploaded_file($_FILES['file']['tmp_name'])){
			//UPLOAD LOCAL
			$file = Upload::save($_FILES['file'], NULL, DOCROOT.'/public/upload/featured/');
				
			$image = Image::factory($file)//->resize(728, 90)
										   //->crop(200, 200, NULL, FALSE)
										   ->save();
			
			$file = str_replace('\\', '/', $file);			
			$file = explode('/', $file);
			$uploaded_file = str_replace('index.php/', '', URL::base(TRUE, TRUE).'public/upload/featured/'.$file[count($file)-1]);
			
			$media = ORM::factory('Media');
			$media->path = $uploaded_file;
			$media->save();
		}
		
		echo json_encode($media->id);
	}
	
	public function action_order()
	{
		$this->auto_render = FALSE;
		$this->profiler    = NULL;
				
		$media = ORM::factory('Featured', $this->request->post('media'));
		$media->order = $this->request->post('order');
		$media->save();

		echo json_encode('success');
	}
	
} // End Manager featured
