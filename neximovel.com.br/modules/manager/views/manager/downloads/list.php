<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/downloads/list'; ?>">Downloads</a> <span class="divider">/</span>
    </li>
  </ul>
  <hr>
</div>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>T&iacute;tulo</th>
			<th>Data</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($downloads as $download): ?>
          <tr>
            <td><?php echo $download->name; ?></td>
			<td><?php echo date('d/m/Y', strtotime($download->dt_created)); ?></td>
            <td class="actions">
				<a href="<?php echo URL::base(true) . 'manager/downloads/send_file/' . $download->id; ?>" class="btn btn-info btn-setting download_file">
					<i class="icon-download icon-black"></i>
					<span style="display:none"><?php echo $download->terms; ?></span>
				</a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
<div class="modal hide fade" id="terms-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">X</button>
		<h3>Voc&ecirc; concorda com os termos de uso?</h3>
    </div>
    <div class="modal-body">
		<p>Termos de Uso</p>
		<p id="pterms"></p>
    </div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">N&atilde;o concordo</a>
        <a href="" id="confirm-link" class="btn btn-primary">Concordo</a>
    </div>
</div>