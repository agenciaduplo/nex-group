<fieldset>
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">T&iacute;tulo</label>
		<div class="controls">
			<?php echo Form::input('name', @$download->name, array('id' => 'name', 'class' => 'input-xxlarge')) ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get(@$errors, 'file') ? 'error' : '' ?>">
		<label class="control-label" for="file">Arquivo</label>
		<div class="controls">
		  <?php echo Form::file('file', array('id' => 'file', 'class' => 'input-xxlarge')) ?>
		  <small class="help-block"><a href="<?= @$download->file ?>">Arquivo</a></small>
		</div>
	</div>
	
	<?php if($edit){ ?>
	<!--<div class="control-group">
		<div class="controls">
			<img src="<?= $download->file ?>" style="height:150px">
		</div>
	</div>-->
	<? } ?>
	
	<div class="control-group <?php echo Arr::get($errors, 'enabled') ? 'error' : '' ?>">
		<label class="control-label" for="enabled">Ativo</label>
		<div class="controls">
			<?php echo Form::select('enabled', array( 0 => 'N&atilde;o', 1 => 'Sim'), @$download->enabled, array('id' => 'enabled', 'class' => 'input-xxlarge')) ?>
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'terms') ? 'error' : '' ?>">
		<div class="controls">
		  <?php echo Form::textarea('terms', @$download->terms, array('id' => 'terms', 'class' => 'input-xxlarge tinymce', 'placeholder' => 'Termos de Uso')) ?>
		</div>
	</div>
	
	<div class="form-actions">
		<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
		ou <?php echo Html::anchor('manager/downloads', 'cancelar') ?> 
	</div>
</fieldset>
