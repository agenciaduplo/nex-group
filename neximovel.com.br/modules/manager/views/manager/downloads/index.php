<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Downloads</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/downloads'; ?>">Adicionar/Editar</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>

<a href="<?php echo URL::base(true) . 'manager/downloads/new' ?>" class="btn btn-info">Adicionar</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>T&iacute;tulo</th>
			<th>Data</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($downloads as $download): ?>
          <tr>
            <td><?php echo $download->name; ?></td>
			<td><?php echo date('d/m/Y', strtotime($download->dt_created)); ?></td>
            <td class="actions">
              <a class="btn" href="<?php echo URL::base(true) . 'manager/downloads/edit/' . $download->id; ?>">
                <i class="icon-edit icon-black"></i>  
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
