<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/tickets'; ?>">Solicita&ccedil;&atilde;o de Verba</a>
    </li>
  </ul>
  <hr>
</div> 
<?php echo View::factory('manager/templates/notices')
                                                      ->set('messages', Notices::get()) 
                                                      ->set('errors', $errors)?>
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-edit"></i><span class="break"></span>Solicita&ccedil;&atilde;o de Verba</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
    <?php echo Form::open('manager/tickets/create/'.@$ticket->id.'/'.@$cancel, array('id' => 'ticket_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
    <?php 
      echo View::factory('manager/tickets/_form')
        ->set('dns', $dns)
        ->set('dn', $dn)
		->set('is_duplicate', $is_duplicate)
		->set('ticket', $ticket)
		->set('book', $book)
		->set('files', $files)
		->set('regions', $regions)
		->set('states', $states)
		->set('months', $months)
		->set('years', $years)
		->set('edit', false)
        ->set('fields', $fields) 
		->bind('abrafor_checked', $abrafor_checked)
		->bind('distribuidor_checked', $distribuidor_checked)
        ->set('s', $s) 
        ->set('errors', $errors) 
    ?>
    <?php echo Form::close() ?>

    </div>
  </div><!--/span-->

</div><!--/row-->
