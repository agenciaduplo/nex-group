<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/dns'; ?>">Solicita&ccedil;&otilde;es de Verba</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()); ?>

<?php if(!Auth::instance()->logged_in('admin')){ ?>
<a href="<?php echo URL::base(true) . 'manager/books/list_books'; ?>" class="btn btn-info">Solicitar Book </a>

<a href="<?php echo URL::base(true) . 'manager/books/list_books'; ?>" class="btn btn-info">Solicitar Brinde</a>
<?php } ?>
<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
		<?php 
			if(Auth::instance()->logged_in('admin')){
		
				echo View::factory('manager/tickets/_admin_list')
					->set('tickets', $tickets)
					->set('months', $months); 
					
			} else if(Auth::instance()->logged_in('committee')){ 
			
				echo View::factory('manager/tickets/_committee_list')
						->set('tickets', $tickets)
						->set('months', $months); 
						
			} else if(Auth::instance()->logged_in('distributor')){ 
			
				echo View::factory('manager/tickets/_distributor_list')
						->set('tickets', $tickets)
						->set('months', $months); 
			} 
			
		?>
    </div>

  </div><!--/span-->

</div><!--/row-->