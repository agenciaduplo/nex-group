 <table class="table table-striped table-bordered bootstrap-datatable distributor">
        <thead>
          <tr>
			<th>ID</th>
            <th>Campanha</th>
			<th>Pe&ccedil;a</th>
			<th>Nome Fantasia</th>
            <th>DN</th>
			<th>Status</th>
			<th>Criado em</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($tickets as $t): ?>
          <tr>
            <td>#<?php echo $t->id; ?></td>
            <td><?php echo $t->book->campaign->name; ?></td>
			<td><?php echo $t->book->name; ?></td>
			<td><?php echo $t->dn->store_name; ?></td>
            <td><?php echo $t->dn->name ?></td>
			<td style="text-transform:capitalize;"><?php echo $t->status == 'aguardando' ? 'Aguardando aprova&ccedil;&atilde;o' : $t->status; ?></td>
			<td><?php echo date('d/m/Y - H:i:s', strtotime($t->dt_created)); ?></td>
            <td class="actions">
				
				<a target="_blank" class="btn" href="<?php echo URL::base(true) . 'public/uploads/tickets/pdf/' . $t->id .'.pdf'; ?>" title="PDF">
					<i class="icon-file icon-red"></i>
				</a>
				  
				<a class="btn" href="<?php echo URL::base(true) . 'manager/tickets/view/' . $t->id; ?>" title="Visualizar">
					<i class="icon-eye-open icon-black"></i>
				</a>
				<?php if($t->status == 'aprovado') { ?>
				<a class="btn btn-info" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $t->id; ?>" title="Duplicar">
					<i class="icon-plus icon-white"></i>
				</a>
				<?php } ?>
				<?php if($t->status == 'Novo') { ?>
				<a class="btn btn-danger duplicate-cancel" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $t->id. '/cancel'; ?>" title="Duplicar & Cancelar">
					<i class="icon-remove-circle icon-white"></i>
				</a>
				<?php } ?>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
                <th></th>
                <th></th>
				<th></th>
                <th></th>
                <th>Todos</th>
				<th>Todos</th>
                <th></th>
                <th></th>
          </tr>
        </tfoot>
</table>
<div id="duplicate-cancel-dialog" title="Deletar campanha?" style="display:none">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta solicitação será cancelada e uma nova criada. Você tem certeza de que deseja fazer isto?</p>
</div>