 <table class="table table-striped table-bordered bootstrap-datatable">
        <thead>
          <tr>
			<th>ID</th>
            <th>Campanha</th>
			<th>Pe&ccedil;a</th>
			<th>Valor</th>
			<th>Status</th>
			<th>Criado em</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php 
			setlocale(LC_MONETARY, 'pt_BR');
			foreach($tickets as $t): ?>
          <tr>
            <td>#<?php echo $t->id; ?></td>
            <td><?php echo $t->book->campaign->name; ?></td>
			<td><?php echo $t->book->name; ?></td>
			<td><?php echo money_format('%.2n', $t->cost); ?>
			</td>
			<td style="text-transform:capitalize;"><?php echo $t->status == 'aguardando' ? 'Aguardando aprova&ccedil;&atilde;o' : $t->status; ?></td>
			<td><?php echo date('d/m/Y - H:i:s', strtotime($t->dt_created)); ?></td>
            <td class="actions">
				
				<a target="_blank" class="btn" href="<?php echo URL::base(true) . 'public/uploads/tickets/pdf/' . $t->id .'.pdf'; ?>">
					<i class="icon-file icon-red"></i>
				</a>
				
				<a class="btn" href="<?php echo URL::base(true) . 'manager/tickets/status/' . $t->id; ?>">
					<i class="icon-edit icon-black"></i>  
				</a>
				<?php if($t->status == 'aprovado') { ?>
				<a class="btn btn-info" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $t->id; ?>" title="Duplicar">
					<i class="icon-plus icon-white"></i>
				</a>
				<?php } ?>
				<?php if($t->status == 'Novo') { ?>
				<a class="btn btn-danger duplicate-cancel" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $t->id. '/cancel'; ?>" title="Duplicar & Cancelar">
					<i class="icon-remove-circle icon-white"></i>
				</a>
				<?php } ?>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
                <th></th>
                <th></th>
				<th></th>
                <th></th>
				<th>Todos</th>
                <th></th>
                <th></th>
          </tr>
        </tfoot>
</table>