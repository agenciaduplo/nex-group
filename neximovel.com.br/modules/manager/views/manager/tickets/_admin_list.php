 <table class="table table-striped table-bordered bootstrap-datatable admin">
        <thead>
          <tr>
            <th>ID</th>
            <th>Distribuidor</th>
			<th>DN</th>
			<th>Regional</th>
			<th>M&ecirc;s</th>
            <th>Ano</th>
			<th>Status</th>
			<th>Criado em</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($tickets as $t): ?>
          <tr>
            <td>#<?php echo $t->id; ?></td>
            <td><?php echo $t->owner->name; ?></td>
			<td><?php echo $t->dn->name; ?></td>
			<td><?php echo $t->dn->region->name; ?></td>
			<td><?php echo $months[idate('m', strtotime($t->ticket_date))]; ?></td>
            <td><?php echo date('Y', strtotime($t->ticket_date)); ?></td>
			<td style="text-transform:capitalize;"><?php echo $t->status == 'aguardando' ? 'Aguardando aprova&ccedil;&atilde;o' : $t->status; ?></td>
			<td><?php echo date('d/m/Y - H:i:s', strtotime($t->dt_created)); ?></td>
            <td class="actions">
				
				<a target="_blank" class="btn" href="<?php echo URL::base(true) . 'public/uploads/tickets/pdf/' . $t->id .'.pdf'; ?>">
					<i class="icon-file icon-red"></i>PDF
				</a>

				<a class="btn" href="<?php echo URL::base(true) . 'manager/tickets/status/' . $t->id; ?>">
					<i class="icon-edit icon-black"></i>  
				</a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
                <th></th>
                <th></th>
				<th></th>
				<th></th>
                <th>Todos</th>
                <th>Todos</th>
				<th>Todos</th>
                <th></th>
                <th></th>
          </tr>
        </tfoot>
</table>