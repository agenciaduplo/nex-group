<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/tickets'; ?>">Solicita&ccedil;&atilde;o de Verba</a>
    </li>
  </ul>
  <hr>
</div> 
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-edit"></i><span class="break"></span>Confirmar Solicita&ccedil;&atilde;o de Verba</h2>
    </div>
    <div class="box-content">
<fieldset class="form-horizontal">

	<div class="control-group">
		<label class="control-label" for="dn_id">DN</label>
		<div class="controls">
		  <?php echo $ticket->dn->name ?>
		  <small class="help-block"></small>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Tipo</label>
		<div class="controls">
			<?php echo $dn->type; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="city">Cidade</label>
		<div class="controls">
			<?php echo $dn->city; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="state">Estado</label>
		<div class="controls">
			<?php echo $dn->state; ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="region">Regional</label>
		<div class="controls">
			<?php echo $dn->region ;?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="store_name">Nome Fantasia</label>
		<div class="controls">
			<?php echo $dn->store_name; ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="name">Respons&aacute;vel</label>
		<div class="controls">
			<?php echo $dn->name ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="phone">Telefone</label>
		<div class="controls">
		  <?php echo $dn->phone ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="email">E-mail</label>
		<div class="controls">
		  <?php echo $dn->email ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="attendant">Destinat&aacute;rio</label>
		<div class="controls">
		  <?php echo $dn->attendant ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<h1>Dados da Campanha:</h1>

	<div class="control-group">
		<label class="control-label" for="campaign_title">T&iacute;tulo</label>
		<div class="controls">
		  <?php echo $book->campaign_title; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="dn_add">M&ecirc;s de compet&ecirc;ncia</label>
		<div class="controls">
		  <?php echo date('m/y',strtotime($ticket->ticket_date)); ?>
		  <small class="help-block"></small>
		</div>
	</div>

	<h1>Dados da Pe&ccedil;a:</h1>

	<div class="control-group">
		<label class="control-label" for="book_name">Pe&ccedil;a</label>
		<div class="controls">
		  <?php echo $book->book_name ?>
		  <small class="help-block"></small> 
		</div>
	</div>	

	<div class="control-group">
		<label class="control-label" for="book_description">Mini Briefing</label>
		<div class="controls">
		  <?php echo $book->book_description ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php foreach($fields as $k => $v){ ?>

		<div class="control-group">
		<label class="control-label" for="book_description"><?php echo $v->label; ?></label>
		<div class="controls">
		  <?php echo $v->value; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php } ?>
	
	<div class="control-group">
		<label class="control-label" for="quantity">Quantidade</label>
		<div class="controls">
		  <?php echo $ticket->quantity; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="vehicle">Veiculo/Produto:</label>
		<div class="controls">
		  <?php echo $ticket->vehicle; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="vehicle_name">Nome do Veiculo</label>
		<div class="controls">
		  <?php echo $ticket->vehicle_name; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="launch_date">Data da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo $ticket->launch_date; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="cost">Custo da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo $ticket->cost; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="obs">Observa&ccedil;&otilde;es</label>
		<div class="controls">
		  <?php echo $ticket->obs; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	<?php if(count($files) > 0 && !empty($files)){ ?>
	<div class="control-group clone-file-block">
		<label class="control-label" for="files">Refer&ecirc;ncias</label>
		<div class="controls">
		  <?php foreach($files as $file){ ?>
				<a href="<?php echo URL::base(TRUE, TRUE).'/public/uploads/tickets/files/'.$file; ?>" download><?php echo $file; ?></a><br>
			<?php } ?>
		  <small class="help-block"></small>
		</div>
	</div>
	<?php } ?>
	
	<?php if(count($suppliers) > 0 && !empty($suppliers)){ ?>
	<h1>Fornecedor</h1>
		<?php foreach($suppliers as $s){ ?> 
		
		<div class="supplier-block clone-block">
			<hr>
			<div class="control-group">
				<label class="control-label" for="supplier_cnpj">CNPJ</label>
				<div class="controls">
				  <?php echo $s['cnpj'] ?>
				  <small class="help-block"></small> 
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="supplier_im">Raz&atilde;o Social</label>
				<div class="controls">
				  <?php echo $s['corporate_name'] ?>
				  <small class="help-block"></small> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_address">Endere&ccedil;o</label>
				<div class="controls">
				  <?php echo $s['supplier_address'] ?>
				  <small class="help-block"></small> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_city">Cidade</label>
				<div class="controls">
				  <?php echo $s['city'] ?>
				  <small class="help-block"></small> 
				</div>
			  </div>  
			  
			<div class="control-group">
				<label class="control-label" for="supplier_state">Estado</label>
				<div class="controls">
				  <?php echo $s['state'] ?>
				  <small class="help-block"></small>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_ie">IE</label>
				<div class="controls">
				  <?php echo $s['ie'] ?>
				  <small class="help-block"></small> 
				</div>
			</div>  
			  
			<div class="control-group">
				<label class="control-label" for="supplier_im">IM</label>
				<div class="controls">
				  <?php echo $s['im'] ?>
				  <small class="help-block"></small> 
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="supplier_name">Respons&aacute;vel</label>
				<div class="controls">
				  <?php echo $s['name'] ?>
				  <small class="help-block"></small>
				</div>
			</div>	
			
			<div class="control-group">
				<label class="control-label" for="supplier_phone">Telefone</label>
				<div class="controls">
				  <?php echo $s['phone'] ?>
				  <small class="help-block"></small>
				</div>
			</div>
		</div>
		<?php } ?>
	<?php } ?>
	<hr>
  
	<div class="control-group">
		<label class="control-label" for="type">Vencimento</label>
		<div class="controls">
			<?php  echo date('d/m/Y', strtotime($ticket->due_date)) ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<?php setlocale(LC_MONETARY, 'pt_BR'); ?>
	<div class="control-group">
		<label class="control-label" for="type">Valor l&iacute;quido</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->cost); ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="type">Honor&aacute;rios ag&ecirc;ncia</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->ag_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Opera&ccedil;&atilde;o do sistema</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->op_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="type">Valor total</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->cost + $ticket->op_fee + $ticket->ag_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="type">Fatura contra</label>
		<div class="controls">
			<?php  echo $ticket->billing; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	<?php echo Form::open('manager/tickets/confirmed/'.$ticket->id, array('id' => 'ticket_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
		<div class="form-actions">
			<?php echo Form::button('save', 'Confirmar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
			ou <a href="javascript:window.history.go(-1);">Voltar</a> 
		</div>
	<?php echo Form::close() ?>
</fieldset>


    </div>
  </div><!--/span-->

</div><!--/row-->
