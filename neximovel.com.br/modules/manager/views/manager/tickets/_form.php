<fieldset>

	<div class="control-group <?php echo Arr::get($errors, 'dn_id') ? 'error' : '' ?>">
		<label class="control-label" for="dn_id">DN</label>
		<div class="controls">
		  <?php echo Form::select('dn_id', Arr::unshift($dns, '','Selecione...'), @$ticket->dn_id, array('id' => 'dn_select', 'class' => 'input-xlarge', 'required' =>'required')) ?>
		  <small class="help-block"></small>
		</div>
	</div>
	<div class="control-group <?php echo Arr::get($errors, 'type') ? 'error' : '' ?>">
		<label class="control-label" for="type">Tipo</label>
		<div class="controls">
			<?php echo Form::input('type', @$dn->type, array('id' => 'type', 'class' => 'input-xxlarge type', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[type]', @$dn->type, array('class' => 'type')) ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'city') ? 'error' : '' ?>">
		<label class="control-label" for="city">Cidade</label>
		<div class="controls">
			<?php echo Form::input('city', @$dn->city, array('id' => 'city', 'class' => 'input-xxlarge city', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[city]', @$dn->city, array('id' => 'city', 'class' => 'input-xxlarge city')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'state') ? 'error' : '' ?>">
		<label class="control-label" for="state">Estado</label>
		<div class="controls">
			<?php echo Form::input('state', @$dn->state, array('id' => 'state', 'class' => 'input-xxlarge state', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[state]', @$dn->state, array('id' => 'state', 'class' => 'input-xxlarge state')) ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'region') ? 'error' : '' ?>">
		<label class="control-label" for="region">Regional</label>
		<div class="controls">
			<?php echo Form::input('region', @$dn->region, array('id' => 'region', 'class' => 'input-xxlarge region', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[region]', @$dn->region, array('id' => 'region', 'class' => 'input-xxlarge region')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'store_name') ? 'error' : '' ?>">
		<label class="control-label" for="store_name">Nome Fantasia</label>
		<div class="controls">
			<?php echo Form::input('store_name', @$dn->store_name, array('id' => 'store_name', 'class' => 'input-xxlarge store_name', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[store_name]', @$dn->store_name, array('id' => 'store_name', 'class' => 'input-xxlarge store_name')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">Respons&aacute;vel</label>
		<div class="controls">
			<?php echo Form::input('name', @$dn->name, array('id' => 'name', 'class' => 'input-xxlarge name', 'disabled' => 'true')) ?>
			<?php echo Form::hidden('dn_info[name]', @$dn->name, array('id' => 'name', 'class' => 'input-xxlarge name')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'phone')? 'error' : '' ?>">
		<label class="control-label" for="phone">Telefone</label>
		<div class="controls">
		  <?php echo Form::input('phone', @$dn->phone, array('id' => 'phone', 'class' => 'input-xlarge phone', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('dn_info[phone]', @$dn->phone, array('id' => 'phone', 'class' => 'input-xlarge')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'email')? 'error' : '' ?>">
		<label class="control-label" for="email">E-mail</label>
		<div class="controls">
		  <?php echo Form::input('email', @$dn->email, array('id' => 'email', 'class' => 'input-xlarge email', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('dn_info[email]', @$dn->email, array('id' => 'email', 'class' => 'input-xlarge email')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'attendant')? 'error' : '' ?>">
		<label class="control-label" for="attendant">Destinat&aacute;rio</label>
		<div class="controls">
		  <?php echo Form::input('attendant', @$dn->attendant, array('id' => 'attendant', 'class' => 'input-xlarge attendant', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('dn_info[attendant]', @$dn->attendant, array('id' => 'attendant', 'class' => 'input-xlarge attendant')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<h1>Dados da Campanha:</h1>

	<div class="control-group <?php echo Arr::get($errors, 'campaign_title')? 'error' : '' ?>">
		<label class="control-label" for="campaign_title">T&iacute;tulo</label>
		<div class="controls">
		  <?php echo Form::input('campaign_title', $book->campaign->name, array('id' => 'campaign_title', 'class' => 'input-xxlarge campaign_title', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('book_info[campaign_title]', $book->campaign->name, array('id' => 'campaign_title', 'class' => 'input-xxlarge campaign_title')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'month') ? 'error' : '' ?>">
		<label class="control-label" for="dn_add">M&ecirc;s de compet&ecirc;ncia</label>
		<div class="controls">
		  <?php echo Form::select('month', @$months, date('m'), array('id' => 'month', 'placeholder' => 'Selecione...', 'class' => 'input-xlarge')) ?>
		  <?php echo Form::select('year', @$years, date('Y'), array('id' => 'year', 'placeholder' => 'Selecione...', 'class' => 'input-xlarge')) ?>
		  <small class="help-block"></small>
		</div>
	</div>

	<h1>Dados da Pe&ccedil;a:</h1>

	<div class="control-group <?php echo Arr::get($errors, 'book_name')? 'error' : '' ?>">
		<label class="control-label" for="book_name">Pe&ccedil;a</label>
		<div class="controls">
		  <?php echo Form::input('book_name', $book->name, array('id' => 'book_name', 'class' => 'input-xxlarge book_name', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('book_info[book_name]', $book->name, array('id' => 'book_name', 'class' => 'input-xxlarge book_name')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>	

	<div class="control-group <?php echo Arr::get($errors, 'book_description')? 'error' : '' ?>">
		<label class="control-label" for="book_description">Mini Briefing</label>
		<div class="controls">
		  <?php echo Form::input('book_description', $book->description, array('id' => 'book_description', 'class' => 'input-xxlarge book_description', 'disabled' => 'true')) ?>
		  <?php echo Form::hidden('book_info[book_description]', $book->description, array('id' => 'book_description', 'class' => 'input-xxlarge book_description')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>

		<?php
			if(!$is_duplicate){
				foreach(json_decode($fields) as $k => $v){ 
		?>

		<div class="control-group">
			<label class="control-label" for="book_description"><?php echo $v; ?></label>
			<div class="controls">
			  <?php echo Form::input("fields[$k]", null, array('id' => "filed$k", 'class' => 'input-xxlarge', 'required' =>'required')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		<?php 
				} 
			} else {  
				foreach($fields as $k => $v){
		?>
		<div class="control-group">
			<label class="control-label" for="book_description"><?php echo $v->label; ?></label>
			<div class="controls">
			  <?php echo Form::input("fields[$k]", $v->value, array('id' => "filed$k", 'class' => 'input-xxlarge', 'required' =>'required')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		<?php } } ?>
		
	<div class="control-group <?php echo Arr::get($errors, 'quantity')? 'error' : '' ?>">
		<label class="control-label" for="quantity">Quantidade</label>
		<div class="controls">
		  <?php echo Form::input('quantity', @$ticket->quantity, array('id' => 'quantity', 'class' => 'input-small', 'type' => "number",  'min' => "0", 'step' => "1", 'required' =>'required')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'vehicle')? 'error' : '' ?>">
		<label class="control-label" for="vehicle">Veiculo/Produto:</label>
		<div class="controls">
		  <?php echo Form::input('vehicle', @$ticket->vehicle, array('id' => 'vehicle', 'class' => 'input-xxlarge', 'required' =>'required')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'vehicle_name')? 'error' : '' ?>">
		<label class="control-label" for="vehicle_name">Nome do Veiculo</label>
		<div class="controls">
		  <?php echo Form::input('vehicle_name', @$ticket->vehicle_name, array('id' => 'vehicle_name', 'class' => 'input-xxlarge', 'required' =>'required')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'launch_date')? 'error' : '' ?>">
		<label class="control-label" for="launch_date">Data da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo Form::input('launch_date', date('d/m/Y', strtotime(@$ticket->launch_date)), array('id' => 'launch_date', 'class' => 'input-small dpicker', 'required' =>'required')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'type') ? 'error' : '' ?>">
		<label class="control-label" for="dn_id">Tipo</label>
		<div class="controls">
		  <?php echo Form::select('tType', array('mid' => 'M&iacute;dia', 'prod' => 'Produ&ccedil;&atilde;o'), $ticket->tType, array('id' => 'tType', 'class' => 'input-xlarge')) ?>
		  <small class="help-block"></small>
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'cost')? 'error' : '' ?>">
		<label class="control-label" for="cost">Custo da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo Form::input('cost', @$ticket->cost, array('id' => 'cost', 'class' => 'input-small', 'required' =>'required')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'obs')? 'error' : '' ?>">
		<label class="control-label" for="obs">Observa&ccedil;&otilde;es</label>
		<div class="controls">
		  <?php echo Form::textarea('obs', @$ticket->obs, array('id' => 'obs', 'class' => 'input-xlarge')) ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group clone-file-block">
		<label class="control-label" for="files">Refer&ecirc;ncias</label>
		<div class="controls">
		<?php if(count($files) > 0 && !empty($files)){ ?>
			  <?php foreach($files as $file){ ?>
					<a href="<?php echo URL::base(TRUE, TRUE).'/public/uploads/tickets/files/'.$file; ?>" download><?php echo $file; ?></a><br><br>
				<?php } ?>
		<?php } ?>
		  <?php echo Form::file('files[]', array('class' => 'input-xxlarge files')) ?>
		  <?php echo Form::button('btn_delete_file', 'X', array('id' => 'btn_delete_file', 'type' => 'button', 'class' => 'btn btn-danger btn_delete_file')) ?>
		  <small class="help-block"></small>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="btn_add_file"></label>
		<div class="controls">
		  <?php echo Form::button('btn_add_file', '+ Arquivo', array('id' => 'btn_add_file', 'type' => 'button', 'class' => 'btn btn-info')) ?>
		  <small class="help-block"></small>
		</div>
	  </div>
	
	<h1>Fornecedor</h1>

	<div class="supplier-block clone-block">
		<hr>
		<div class="control-group <?php echo Arr::get($errors, 'supplier_cnpj')? 'error' : '' ?>">
			<label class="control-label" for="supplier_cnpj">CNPJ</label>
			<div class="controls">
			  <?php echo Form::input('supplier[cnpj][]', @$s['cnpj'], array('id' => 'supplier_cnpj', 'class' => 'input-xlarge cnpj', 'required' =>'required')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>

		<div class="control-group <?php echo Arr::get($errors, 'supplier_corporate_name')? 'error' : '' ?>">
			<label class="control-label" for="supplier_im">Raz&atilde;o Social</label>
			<div class="controls">
			  <?php echo Form::input('supplier[corporate_name][]', @$s['corporate_name'], array('id' => 'supplier_corporate_name', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_address')? 'error' : '' ?>">
			<label class="control-label" for="supplier_address">Endere&ccedil;o</label>
			<div class="controls">
			  <?php echo Form::input('supplier[supplier_address][]', @$s['supplier_address'], array('id' => 'supplier_address', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_number')? 'error' : '' ?>">
			<label class="control-label" for="supplier_number">N&ordm;</label>
			<div class="controls">
			  <?php echo Form::input('supplier[supplier_number][]', @$s['supplier_number'], array('id' => 'supplier_number', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_complement')? 'error' : '' ?>">
			<label class="control-label" for="supplier_complement">Complemento</label>
			<div class="controls">
			  <?php echo Form::input('supplier[supplier_complement][]', @$s['supplier_complement'], array('id' => 'supplier_complement', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_neighborhood')? 'error' : '' ?>">
			<label class="control-label" for="supplier_neighborhood">Bairro</label>
			<div class="controls">
			  <?php echo Form::input('supplier[supplier_neighborhood][]', @$s['supplier_neighborhood'], array('id' => 'supplier_neighborhood', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>		
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_cep')? 'error' : '' ?>">
			<label class="control-label" for="supplier_cep">CEP</label>
			<div class="controls">
			  <?php echo Form::input('supplier[supplier_cep][]', @$s['supplier_cep'], array('id' => 'supplier_cep', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>		
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_city')? 'error' : '' ?>">
			<label class="control-label" for="supplier_city">Cidade</label>
			<div class="controls">
			  <?php echo Form::input('supplier[city][]', @$s['city'], array('id' => 'supplier_city', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		  </div>  
		  
		<div class="control-group <?php echo Arr::get($errors, 'supplier_state') ? 'error' : '' ?>">
			<label class="control-label" for="supplier_state">Estado</label>
			<div class="controls">
			  <?php echo Form::input('supplier[state][]', @$s['state'], array('id' => 'supplier_state', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small>
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_ie')? 'error' : '' ?>">
			<label class="control-label" for="supplier_ie">IE</label>
			<div class="controls">
			  <?php echo Form::input('supplier[ie][]', @$s['ie'], array('id' => 'supplier_ie', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>  
		  
		<div class="control-group <?php echo Arr::get($errors, 'supplier_im')? 'error' : '' ?>">
			<label class="control-label" for="supplier_im">IM</label>
			<div class="controls">
			  <?php echo Form::input('supplier[im][]', @$s['im'], array('id' => 'supplier_im', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small> 
			</div>
		</div>

		<div class="control-group <?php echo Arr::get($errors, 'supplier_name') ? 'error' : '' ?>">
			<label class="control-label" for="supplier_name">Respons&aacute;vel</label>
			<div class="controls">
			  <?php echo Form::input('supplier[name][]', @$s['name'], array('id' => 'supplier_name', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small>
			</div>
		</div>	
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_email') ? 'error' : '' ?>">
			<label class="control-label" for="supplier_email">E-mail</label>
			<div class="controls">
			  <?php echo Form::input('supplier[email][]', @$s['email'], array('id' => 'supplier_email', 'class' => 'input-xlarge')) ?>
			  <small class="help-block"></small>
			</div>
		</div>
		
		<div class="control-group <?php echo Arr::get($errors, 'supplier_phone') ? 'error' : '' ?>">
			<label class="control-label" for="supplier_phone">Telefone</label>
			<div class="controls">
			  <?php echo Form::input('supplier[phone][]', @$s['phone'], array('id' => 'supplier_phone', 'class' => 'input-xlarge phone')) ?>
			  <small class="help-block"></small>
			</div>
		</div>
	</div>
	
	<!--<div class="control-group <?php echo Arr::get($errors, 'btn_add_supplier') ? 'error' : '' ?>">
		<label class="control-label" for="btn_add_supplier"></label>
		<div class="controls">
		  <?php echo Form::button('btn_add_supplier', '+ Fornecedor', array('id' => 'btn_add_supplier', 'type' => 'button', 'class' => 'btn btn-info')) ?>
		  <small class="help-block"></small>
		</div>
	</div>-->
	
	<hr>
  
	<div class="control-group <?php echo Arr::get($errors, 'type') ? 'error' : '' ?>">
		<label class="control-label" for="type">Faturamento</label>
		<div class="controls controls-new">
			<label class="control-label">
			<?php echo Form::radio('billing', 'abrafor', $abrafor_checked, array('id' => 'abrafor', 'class' => 'input-xxlarge')) ?>
			Abrafor
			</label>
			<label class="control-label">
			<?php echo Form::radio('billing', 'distribuidor', $distribuidor_checked, array('id' => 'distribuidor', 'class' => 'input-xxlarge')) ?>
			Distribuidor
			</label>
			<small class="help-block"></small> 
		</div>
	</div>
  
  <div class="form-actions">
	<input type="hidden" name="book_id" value="<?php echo $book->id; ?>">
	<input type="hidden" name="is_duplicate" value="<?php echo $is_duplicate; ?>">
	<input type="hidden" name="duplicate_of" value="<?php echo @$ticket->id; ?>">
    <?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
    ou <?php echo Html::anchor('manager/users', 'cancelar') ?> 
  </div>
</fieldset>
