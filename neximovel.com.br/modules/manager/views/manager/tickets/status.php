<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/tickets'; ?>">Solicita&ccedil;&atilde;o de Verba</a>
    </li>
  </ul>
  <hr>
</div>
<?php
if(Auth::instance()->logged_in('committee')){													  
?>
<?php if($t->status == 'aprovado') {?>												  
<a class="btn btn-info" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $ticket->id; ?>" title="Duplicar">
	<i class="icon-plus icon-white"></i> Duplicar
</a>
<?php } ?>
<?php if($ticket->status == 'Novo') {?>
<a class="btn btn-danger duplicate-cancel" href="<?php echo URL::base(true) . 'manager/tickets/duplicate/' . $ticket->id. '/cancel'; ?>" title="Duplicar & Cancelar">
	<i class="icon-remove-circle icon-white"></i> Duplicar & Cancelar
</a>
<?php } }?> 
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-edit"></i><span class="break"></span>Confirmar Solicita&ccedil;&atilde;o de Verba</h2>
    </div>
    <div class="box-content">
<?php echo Form::open('manager/tickets/status/'.$ticket->id, array('id' => 'ticket_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>    	
<fieldset class="form-horizontal">

	<div class="control-group">
		<label class="control-label" for="dn_id">Solicitação Nro.</label>
		<div class="controls">
		  #<?php echo $ticket->id; ?>
		  <small class="help-block"></small>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="dn_id">DN</label>
		<div class="controls">
		  <?php echo $ticket->dn->name; ?>
		  <small class="help-block"></small>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Tipo</label>
		<div class="controls">
			<?php echo $dn->type; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="city">Cidade</label>
		<div class="controls">
			<?php echo $dn->city; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="state">Estado</label>
		<div class="controls">
			<?php echo $dn->state; ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="region">Regional</label>
		<div class="controls">
			<?php echo $dn->region ;?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="store_name">Nome Fantasia</label>
		<div class="controls">
			<?php echo $dn->store_name; ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="name">Respons&aacute;vel</label>
		<div class="controls">
			<?php echo $dn->name; ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="phone">Telefone</label>
		<div class="controls">
		  <?php echo $dn->phone; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="email">E-mail</label>
		<div class="controls">
		  <?php echo $dn->email; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="attendant">Destinat&aacute;rio</label>
		<div class="controls">
		  <?php echo $dn->attendant; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<h1>Dados da Campanha:</h1>

	<div class="control-group">
		<label class="control-label" for="campaign_title">T&iacute;tulo</label>
		<div class="controls">
		  <?php echo $book->campaign_title; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php if(Auth::instance()->logged_in('admin')){ ?>

	<div class="control-group <?php echo Arr::get(@$errors, 'month') ? 'error' : ''; ?>">
		<label class="control-label" for="dn_add">M&ecirc;s de compet&ecirc;ncia</label>
		<div class="controls">
		  <?php echo Form::select('month', @$months, date('m',strtotime($ticket->ticket_date)), array('id' => 'month', 'placeholder' => 'Selecione...', 'class' => 'input-xlarge')); ?>
		  <?php echo Form::select('year', @$years, date('Y',strtotime($ticket->ticket_date)), array('id' => 'year', 'placeholder' => 'Selecione...', 'class' => 'input-xlarge')); ?>
		  <small class="help-block"></small>
		</div>
	</div>


	<?php } else { ?>

	<div class="control-group">
		<label class="control-label" for="dn_add">M&ecirc;s de compet&ecirc;ncia</label>
		<div class="controls">
		  <?php echo date('m-y',strtotime($ticket->ticket_date)); ?>
		  <small class="help-block"></small>
		</div>
	</div>

	<?php } ?>

	
	<h1>Dados da Pe&ccedil;a:</h1>

	<div class="control-group">
		<label class="control-label" for="book_name">Pe&ccedil;a</label>
		<div class="controls">
		  <?php echo $book->book_name; ?>
		  <small class="help-block"></small> 
		</div>
	</div>	

	<div class="control-group">
		<label class="control-label" for="book_description">Mini Briefing</label>
		<div class="controls">
		  <?php echo $book->book_description; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php foreach($fields as $k => $v){ ?>

		<div class="control-group">
		<label class="control-label" for="book_description"><?php echo $v->label; ?></label>
		<div class="controls">
		  <?php echo $v->value; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php } ?>
	
	<div class="control-group">
		<label class="control-label" for="quantity">Quantidade</label>
		<div class="controls">
		  <?php echo $ticket->quantity; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="vehicle">Veiculo/Produto:</label>
		<div class="controls">
		  <?php echo $ticket->vehicle; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="vehicle_name">Nome do Veiculo</label>
		<div class="controls">
		  <?php echo $ticket->vehicle_name; ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	

	<?php 

	$data = explode("-", $ticket->launch_date);

	$data = ''.$data[2].'/'.$data[1].'/'.$data[0].'';

	 ?>

	<div class="control-group">
		<label class="control-label" for="launch_date">Data da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo $data; ?>
		  <small class="help-block"></small> 
		</div>
	</div>


	<!--div class="control-group <?php echo Arr::get(@$errors, 'launch_date')? 'error' : ''; ?>">
		<label class="control-label" for="launch_date">Data da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo Form::input('launch_date', $data, array('id' => 'launch_date', 'class' => 'input-small dpicker', 'required' =>'required')); ?>
		  <small class="help-block"></small> 
		</div>
	</div-->

	<div class="control-group">
		<label class="control-label" for="cost">Tipo</label>
		<div class="controls">
		  <?php 

		  echo Form::input('tType', $ticket->tType, array('id' => 'tType', 'class' => 'input-small dpicker', 'type' =>'hidden'));

		  switch ($ticket->tType) {
		  	case 'mid':
		  		echo "Mídia";
		  		break;
		  	
		  	case 'prod':
		  		echo "Produção";
		  		break;
		  }


 ?>
		  <small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="cost">Custo da veicula&ccedil;&atilde;o</label>
		<div class="controls">
		  <?php echo number_format($ticket->cost,2,",",""); ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="obs">Observa&ccedil;&otilde;es</label>
		<div class="controls">
		  <?php echo $ticket->obs; ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<?php if(count($files) > 0 && !empty($files)){ ?>
	<div class="control-group clone-file-block">
		<label class="control-label" for="files">Refer&ecirc;ncias</label>
		<div class="controls">
		  <?php foreach($files as $file){ ?>
				<a href="<?php echo URL::base(TRUE, TRUE).'/public/uploads/tickets/files/'.$file; ?>" download><?php echo $file; ?></a><br>
			<?php } ?>
		  <small class="help-block"></small>
		</div>
	</div>
	<?php } ?>

	<?php if(count($suppliers) > 0 && !empty($suppliers)){ ?>
		<h1>Fornecedor</h1>
		<?php foreach($suppliers as $s){ ?> 
		
		<div class="supplier-block clone-block">
			<hr>
			<div class="control-group">
				<label class="control-label" for="supplier_cnpj">CNPJ</label>
				<div class="controls">
				  <?php echo $s['cnpj']; ?>
				  <small class="help-block"></small> 
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="supplier_im">Raz&atilde;o Social</label>
				<div class="controls">
				  <?php echo $s['corporate_name']; ?>
				  <small class="help-block"></small> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_address">Endere&ccedil;o</label>
				<div class="controls">
				  <?php $s['supplier_address']; ?>
				  <small class="help-block"></small> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_city">Cidade</label>
				<div class="controls">
				  <?php echo $s['city']; ?>
				  <small class="help-block"></small> 
				</div>
			  </div>  
			  
			<div class="control-group">
				<label class="control-label" for="supplier_state">Estado</label>
				<div class="controls">
				  <?php echo $s['state']; ?>
				  <small class="help-block"></small>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="supplier_ie">IE</label>
				<div class="controls">
				  <?php echo $s['ie']; ?>
				  <small class="help-block"></small> 
				</div>
			</div>  
			  
			<div class="control-group">
				<label class="control-label" for="supplier_im">IM</label>
				<div class="controls">
				  <?php echo $s['im']; ?>
				  <small class="help-block"></small> 
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="supplier_name">Respons&aacute;vel</label>
				<div class="controls">
				  <?php echo $s['name']; ?>
				  <small class="help-block"></small>
				</div>
			</div>	
			
			<div class="control-group">
				<label class="control-label" for="supplier_phone">Telefone</label>
				<div class="controls">
				  <?php echo $s['phone']; ?>
				  <small class="help-block"></small>
				</div>
			</div>
		</div>
		<?php } ?>
	<?php } ?>
	<hr>
  
	<div class="control-group">
		<label class="control-label" for="type">Vencimento</label>
		<div class="controls">
			<?php  echo date('d/m/Y', strtotime($ticket->due_date)); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	<?php setlocale(LC_MONETARY, 'pt_BR'); ?>

	<div class="control-group">
		<label class="control-label" for="cost">Valor pedido</label>
		<div class="controls">
		  <?php echo Form::input('cost', $ticket->cost, array('id' => 'cost', 'class' => 'input-small', 'required' =>'required')); ?>
		  <small class="help-block"></small> 
		</div>
	</div>

	<!--div class="control-group">
		<label class="control-label" for="type">Valor pedido</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->cost); ?>
			<small class="help-block"></small> 
		</div>
	</div-->
  
	<div class="control-group">
		<label class="control-label" for="type">Honor&aacute;rios ag&ecirc;ncia</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->ag_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Opera&ccedil;&atilde;o do sistema</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->op_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="type">Valor total</label>
		<div class="controls">
			<?php  echo money_format('%.2n', $ticket->cost + $ticket->op_fee + $ticket->ag_fee); ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="type">Fatura contra</label>
		<div class="controls">
			<?php  echo $ticket->billing; ?>
			<small class="help-block"></small> 
		</div>
	</div>
	<hr>
	
	<div class="control-group">
		<label class="control-label" for="type">N&uacute;mero PP/PI</label>
		<div class="controls">
			<?php echo Form::input('pp_pi', $ticket->pp_pi, array('id' => 'pp_pi', 'class' => 'input-xxlarge type', $disabled => $disabled)); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">NF Ve&iacute;culo</label>
		<div class="controls">
			 <?php echo Form::input('nf_vehicle', $ticket->nf_vehicle, array('id' => 'nf_vehicle', 'class' => 'input-xxlarge type', $disabled => $disabled)); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">NF DPM</label>
		<div class="controls">
			 <?php echo Form::input('nf_dpm', $ticket->nf_dpm, array('id' => 'nf_dpm', 'class' => 'input-xxlarge type', $disabled => $disabled)); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Status</label>
		<div class="controls">
			 <?php echo Form::select('status', $status, $ticket->status, array('id' => 'status_select', 'class' => 'input-xlarge')); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="type">Log</label>
		<div class="controls">
			 <?php echo Form::input('obs', null, array('id' => 'type', 'class' => 'input-xxlarge type')); ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<?php if($ticket->status == 'Novo'){ ?>
	
	<div class="control-group">
		<label class="control-label" for="type">Anexo</label>
		<div class="controls">
			<?php //echo Form::file('attatch', array('class' => 'input-xxlarge files', 'required' => 'required')); ?>
			<?php echo Form::file('attatch', array('class' => 'input-xxlarge files')); ?>
			<?php echo Form::input('mail_comment', null, array('id' => 'mail_comment', 'class' => 'input-xxlarge')); ?>
			<small class="help-block">(Arquivo e coment&aacute;rio para anexar no e-mail que ser&aacute; enviado ao comit&ecirc;)</small> 
		</div>
	</div>
	<?php } ?>
	
	<hr>
	
	<div class="control-group">
		<label class="control-label" for="type"></label>
		<div class="controls">
			<?php foreach($logs as $l){?>
			<small class="help-block"><? echo date('d/m/Y', strtotime($l->log_date)); ?> - <? echo date('H:i', strtotime($l->log_date)); ?> - <?php echo $l->message; ?></small>
			<?php } ?>
		</div>
	</div>
	
		<div class="form-actions">
			<?php echo Form::button('save', 'Confirmar', array('type' => 'submit', 'class' => 'btn btn-info')); ?>
			ou <a href="javascript:window.history.go(-1);">Voltar</a> 
		</div>
	<?php echo Form::close(); ?>
</fieldset>


    </div>
  </div><!--/span-->

</div><!--/row-->
<div id="duplicate-cancel-dialog" title="Deletar campanha?" style="display:none">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta solicitação será cancelada e uma nova criada. Você tem certeza de que deseja fazer isto?</p>
</div>