<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <?php echo HTML::image('assets/img/logo-duplo-login.png', array('class' => 'logo-login')); ?>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                Please login with your Username and Password.
            </div>
            <?php echo Form::open('manager/login', array('class' => 'form-horizontal')) ?>
                <fieldset>
					<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <?php echo Form::input('username', null, array('id' => 'user_username', 'class' => 'input-large span10 form-control', 'placeholder' => 'E-mail ou Usuário')) ?>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <?php echo Form::password('password', null, array('id' => 'user_password', 'class' => 'input-large span10 form-control', 'placeholder' => 'Senha')) ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="input-prepend">
                        <p>
							<a href="manager/forgotpassword">Esqueci minha senha</a>
							<br />
							<a href="manager/contact">Entre em contato</a> 
						</p>
						  <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </p>
                </fieldset>
            <?php echo Form::close() ?>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->