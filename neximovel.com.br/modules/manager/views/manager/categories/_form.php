<fieldset>
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">Nome da categoria</label>
		<div class="controls">
			<?php echo Form::input('name', @$category->name, array('id' => 'name', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">Ativa</label>
		<div class="controls">
			<?php echo Form::select('enabled', array( 0 => 'N&atilde;o', 1 => 'Sim'), @$category->enabled, array('id' => 'enabled', 'class' => 'input-xxlarge')) ?>
		</div>
	</div>
	
	<div class="form-actions">
		<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
		ou <?php echo Html::anchor('manager/categories', 'cancelar') ?> 
	</div>
</fieldset>
