<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/reports'; ?>">Relat&oacute;rios</a>
    </li>
  </ul>
  <hr>
</div> 
<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-edit"></i><span class="break"></span>Alterar</h2>
    </div>
    <div class="box-content">
    <?php echo Form::open('manager/reports/report/', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
    <?php 
      echo View::factory('manager/reports/_form')
					->set('regions', $regions)
					->set('types', $types)
					->set('months', $months)
					->set('years', $years)
					->set('dns', $dns); 
    ?>
    <?php echo Form::close() ?>
    </div>
  </div><!--/span-->
</div><!--/row-->