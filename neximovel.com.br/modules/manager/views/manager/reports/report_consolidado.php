<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Campanhas</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/reports'; ?>">Consolidado</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>

<a href="<?php echo URL::base(true) . 'manager/ajax/exportxml/consolidado'; ?>" target="_blank" class="btn btn-info">Exportar XLS</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>Nome do Distribuidor</th>
			<th>Quantidade de Pedidos</th>
			<th>Valor dos Pedidos Aprovados</th>
			<th>Valor dos Pedidos Reprovados</th>
          </tr>
        </thead>   
        <tbody>
		
        <?php 
			setlocale(LC_MONETARY, 'pt_BR');
			foreach($reports as $d): ?>
          <tr>
            <td><?php echo $d->name; ?></td>
			<td><?php echo $d->tickets->where('status', '!=', 'to-confirm')->and_where('ticket_date', '=', DB::expr("'$tDate'"))->count_all(); ?></td>
			<td><?php 
					  $sa = $d->tickets->select(DB::expr('SUM(ticket.cost) AS `sumaprov`'))->where('status', '=', 'aprovado')->and_where('ticket_date', '=', DB::expr("'$tDate'"))->find_all()->as_array(null, 'sumaprov');
					  echo  money_format('%.2n', $sa[0]); ?>
			</td>
			<td><?php 
					  $sr = $d->tickets->select(DB::expr('SUM(ticket.cost) AS `sumreprov`'))->where('status', '=', 'reprovado')->and_where('ticket_date', '=', DB::expr("'$tDate'"))->find_all()->as_array(null, 'sumreprov');
					  echo  money_format('%.2n', $sr[0]); ?></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
