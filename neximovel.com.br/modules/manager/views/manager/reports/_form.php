<fieldset>
	<div class="control-group <?php echo Arr::get(@$errors, 'rtype') ? 'error' : '' ?>">
        <label class="control-label" for="rtype">Nome</label>
        <div class="controls">
              <?php echo Form::select('rtype', $types, null, array('id' => 'rtype', 'class' => 'input-large', 'required' => 'true')) ?>
        </div>
    </div>
	
	<div class="control-group clone-block <?php echo Arr::get(@$errors, 'dn') ? 'error' : '' ?>">
        <label class="control-label" for="type">DN</label>
        <div class="controls">
              <?php echo Form::select('dn[]', Arr::unshift($dns, 0, "Todas as DN's"), null, array('id' => 'dn', 'class' => 'input-large dn-combo', 'disabled' => 'disabled', 'required' => 'true')) ?>
			  <?php echo Form::button('btn_delete_dn', '- DN', array('id' => 'btn_delete_dn', 'type' => 'button', 'class' => 'btn btn-danger btn_delete_dn')) ?>
        </div>
    </div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'btn_add_dn') ? 'error' : '' ?>">
		<label class="control-label" for="btn_add_dn"></label>
		<div class="controls">
		  <?php echo Form::button('btn_add_dn', '+ DN', array('id' => 'btn_add_dn', 'type' => 'button', 'class' => 'btn btn-info', 'disabled' => 'disabled')) ?>
		  <small class="help-block"></small>
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'regions') ? 'error' : '' ?>">
        <label class="control-label" for="type">Regionais</label>
        <div class="controls">
              <?php echo Form::select('region', Arr::unshift($regions, 0, "Todas as regionais"), null, array('id' => 'region', 'class' => 'input-large regions-combo')) ?>
        </div>
    </div>
	
	<hr>
	<div class="control-group <?php echo Arr::get(@$errors, 'month') ? 'error' : '' ?>">
		<div class="controls">
			<table>
				<tr style="height:75px;">
					<td>
						<label>
							<input type="radio" name="choice" value="month" class="choice" checked="checked">
							M&ecirc;s de compet&ecirc;ncia
						</label>
					</td>
					<td>
						<label>
							<input type="radio" name="choice" value="period" class="choice">
							Per&iacute;odo
							</label>
					</td>
				</tr>
				<tr><td style="padding-right: 100px;">
					  <?php echo Form::select('month', @$months, null, array('id' => 'month', 'placeholder' => 'Selecione...', 'class' => 'input-medium month', 'required' => 'true')) ?>
					  <?php echo Form::select('year', @$years, null, array('id' => 'year', 'placeholder' => 'Selecione...', 'class' => 'input-medium month', 'required' => 'true')) ?>
					  <small class="help-block"></small>
				
				</td><td>
						
							  <?php echo Form::input('start_date', null, array('id' => 'start_date', 'class' => 'input-small dpicker period', 'disabled' => 'disabled')) ?> - 
							  <?php echo Form::input('end_date', null, array('id' => 'end_date', 'class' => 'input-small dpicker period', 'disabled' => 'disabled')) ?>
							  <small class="help-block"></small> 
				</td></tr>
			</table>
		</div>
	</div>
	
	<div class="form-actions">
		<?php echo Form::button('save', 'Gerar relat&oacute;rio', array('type' => 'submit', 'class' => 'btn btn-info')) ?> 
	</div>
</fieldset>
