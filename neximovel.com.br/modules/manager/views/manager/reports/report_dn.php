<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Relat&oacute;rios</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/reports'; ?>">Por DN</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>

<a href="<?php echo URL::base(true) . 'manager/ajax/exportxml/dn'; ?>" target="_blank" class="btn btn-info">Exportar XLS</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>PP/PI</th>
			<th>Data provis&atilde;o</th>
			<th>Observa&ccedil;&atilde;o</th>
			<th>DN</th>
			<th>Nome do Distribuidor</th>
			<th>Regional</th>
			<th>Valor do Pedido</th>
            <th>Raz&atilde;o Social</th>
            <th>CNPJ</th>
            <th>Endere&ccedil;o</th>
            <th>Cidade</th>
            <th>UF</th>
            <th>CEP</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Status</th>
            <th>Tipo</th>
          </tr>
        </thead>   
        <tbody>
		
        <?php 
			setlocale(LC_MONETARY, 'pt_BR');
			foreach($reports as $t){ 
			
				$s = json_decode($t->suppliers);
				foreach( $s as $key => $all ){
					foreach( $all as $i => $val ){
						$new[$i][$key] = $val;    
					}
					$s = $new;
				}
		?>
          <tr>
            <td><?php echo $t->pp_pi; ?></td>
			<td><?php echo date('m/Y', strtotime($t->ticket_date)); ?></td>
            <td><?php echo $t->obs; ?></td>
			<td><?php echo $t->dn->name; ?></td>
			<td><?php echo $t->dn->store_name; ?></td>
			<td><?php echo $t->dn->region->name; ?></td>
			<td><?php echo money_format('%.2n', $t->cost); ?></td>
			<td><?php echo $s[0]['corporate_name'] ?></td>
			<td><?php echo $s[0]['cnpj'] ?></td>
			<td><?php echo $s[0]['supplier_address'] ?></td>
			<td><?php echo @$s[0]['city'] ?></td>
			<td><?php echo @$s[0]['state'] ?></td>
			<td><?php echo @$s[0]['cep'] ?></td>
			<td><?php echo @$s[0]['phone'] ?></td>
			<td><?php echo @$s[0]['email'] ?></td>
			<td><?php echo $t->status; ?></td>
			<td><?php echo $t->tType == 'mid' ? 'M&iacute;dia' : 'Produ&ccedil;&atilde;o'; ?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
