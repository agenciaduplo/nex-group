<fieldset>

	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">DN</label>
		<div class="controls">
			<?php echo Form::input('name', @$dn->name, array('id' => 'name', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'store_name') ? 'error' : '' ?>">
		<label class="control-label" for="store_name">Nome Fantasia</label>
		<div class="controls">
			<?php echo Form::input('store_name', @$dn->store_name, array('id' => 'store_name', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'corporate_name') ? 'error' : '' ?>">
		<label class="control-label" for="corporate_name">Raz&atilde;o Social</label>
		<div class="controls">
			<?php echo Form::input('corporate_name', @$dn->corporate_name, array('id' => 'corporate_name', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
  
	<div class="control-group <?php echo Arr::get($errors, 'type') ? 'error' : '' ?>">
		<label class="control-label" for="type">Tipo</label>
		<div class="controls">
			<label class="control-label" style="width:65px">
			<?php echo Form::radio('type', 'matriz', @$dn->type == 'matriz' ? true : false ,array('id' => 'matriz', 'class' => 'input-xxlarge')) ?>
			Matriz
			</label>
			<label class="control-label">
			<?php echo Form::radio('type', 'filial', @$dn->type == 'filial' ? true : false ,array('id' => 'filial', 'class' => 'input-xxlarge')) ?>
			Filial
			</label>
			<small class="help-block"></small> 
		</div>
	</div>
  

  <div class="control-group <?php echo Arr::get($errors, 'cnpj')? 'error' : '' ?>">
    <label class="control-label" for="cnpj">CNPJ</label>
    <div class="controls">
      <?php echo Form::input('cnpj', @$dn->cnpj, array('id' => 'cnpj', 'class' => 'input-xlarge cnpj', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'ie')? 'error' : '' ?>">
    <label class="control-label" for="ie">IE</label>
    <div class="controls">
      <?php echo Form::input('ie', @$dn->ie, array('id' => 'ie', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>  
  
  <div class="control-group <?php echo Arr::get($errors, 'im')? 'error' : '' ?>">
    <label class="control-label" for="im">IM</label>
    <div class="controls">
      <?php echo Form::input('im', @$dn->im, array('id' => 'im', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'address')? 'error' : '' ?>">
    <label class="control-label" for="address">Endere&ccedil;o</label>
    <div class="controls">
      <?php echo Form::input('address', @$dn->address, array('id' => 'address', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'city')? 'error' : '' ?>">
    <label class="control-label" for="city">Cidade</label>
    <div class="controls">
      <?php echo Form::input('city', @$dn->city, array('id' => 'city', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>  
  
  <div class="control-group <?php echo Arr::get($errors, 'state') ? 'error' : '' ?>">
    <label class="control-label" for="state">Estado</label>
    <div class="controls">
      <?php echo Form::select('state_id', $states, @$dn->state_id, array('id' => 'state_id', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small>
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'region_id') ? 'error' : '' ?>">
    <label class="control-label" for="region">Regional</label>
    <div class="controls">
      <?php echo Form::select('region_id', $regions, @$dn->region_id, array('id' => 'region_id', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small>
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'site')? 'error' : '' ?>">
    <label class="control-label" for="site">Site</label>
    <div class="controls">
      <?php echo Form::input('site', @$dn->site, array('id' => 'site', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'financial_name')? 'error' : '' ?>">
    <label class="control-label" for="financial_name">Financeiro - Nome</label>
    <div class="controls">
      <?php echo Form::input('financial_name', @$dn->financial_name, array('id' => 'financial_name', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>  
  
  <div class="control-group <?php echo Arr::get($errors, 'financial_email')? 'error' : '' ?>">
    <label class="control-label" for="financial_email">Financeiro - E-mail</label>
    <div class="controls">
      <?php echo Form::input('financial_email', @$dn->financial_email, array('id' => 'financial_email', 'class' => 'input-xlarge', 'required' => 'required', 'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'financial_phone')? 'error' : '' ?>">
    <label class="control-label" for="financial_phone">Financeiro - Telefone</label>
    <div class="controls">
      <?php echo Form::input('financial_phone', @$dn->financial_phone, array('id' => 'financial_phone', 'class' => 'input-xlarge phone', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'marketing_name')? 'error' : '' ?>">
    <label class="control-label" for="marketing_name">Marketing - Nome</label>
    <div class="controls">
      <?php echo Form::input('marketing_name', @$dn->marketing_name, array('id' => 'marketing_name', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>  
  
  <div class="control-group <?php echo Arr::get($errors, 'marketing_email')? 'error' : '' ?>">
    <label class="control-label" for="marketing_email">Marketing - E-mail</label>
    <div class="controls">
      <?php echo Form::input('marketing_email', @$dn->marketing_email, array('id' => 'marketing_email', 'class' => 'input-xlarge', 'required' => 'required', 'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'marketing_phone')? 'error' : '' ?>">
    <label class="control-label" for="marketing_phone">Marketing - Telefone</label>
    <div class="controls">
      <?php echo Form::input('marketing_phone', @$dn->marketing_phone, array('id' => 'marketing_phone', 'class' => 'input-xlarge phone', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'bank')? 'error' : '' ?>">
    <label class="control-label" for="bank">Banco</label>
    <div class="controls">
      <?php echo Form::input('bank', @$dn->bank, array('id' => 'bank', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'agency')? 'error' : '' ?>">
    <label class="control-label" for="bank">Ag&ecirc;ncia</label>
    <div class="controls">
      <?php echo Form::input('agency', @$dn->agency, array('id' => 'agency', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'account')? 'error' : '' ?>">
    <label class="control-label" for="account">Conta Corrente</label>
    <div class="controls">
      <?php echo Form::input('account', @$dn->account, array('id' => 'account', 'class' => 'input-xlarge', 'required' => 'required')) ?>
      <small class="help-block"></small> 
    </div>
  </div>
  
  <div class="form-actions">
    <?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
    ou <?php echo Html::anchor('manager/users', 'cancelar') ?> 
  </div>
</fieldset>
