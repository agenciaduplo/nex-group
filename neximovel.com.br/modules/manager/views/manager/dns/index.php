<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/dns'; ?>">DN's</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())?>

<a href="<?php echo URL::base(true) . 'manager/dns/new' ?>" class="btn btn-info">Adicionar</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable users">
        <thead>
          <tr>
			<th>DN</th>
            <th>Nome</th>
            <th>CNPJ</th>
      			<th>Regional</th>
            <th class="actions">Ações</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($dns as $dn): ?>
          <tr>
            <td><?php echo $dn->name; ?></td>
            <td><?php echo $dn->store_name; ?></td>
            <td><?php echo $dn->cnpj; ?></td>
      			<td><?php echo $dn->region->name; ?></td>
            <td class="actions">
              <!-- <a class="btn btn-success" href="#">
                <i class="icon-zoom-in icon-white"></i>  
              </a> -->
              <a class="btn" href="<?php echo URL::base(true) . 'manager/dns/edit/' . $dn->id; ?>">
                <i class="icon-edit icon-black"></i>  
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Todos</th>
                <th></th>
          </tr>
        </tfoot>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
