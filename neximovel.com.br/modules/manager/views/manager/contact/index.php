<?php echo HTML::image('assets/img/logo-duplo-login.png', array('class' => 'logo-login')); ?>

<div class="box-content contact">
<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>
  <div class="clearfix"></div>
  <?php echo Form::open('manager/contact', array('class' => 'form-horizontal')) ?>
    <fieldset>
      <div class="controls">
        <h3 class="contact-title">Ol&aacute;! Tire suas d&uacute;vidas com a Duplo preenchendo os dados abaixo. Responderemos assim que poss&iacute;vel.</h3>
        <br>
      </div>   
  <div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::input('name', null, array('id' => 'name', 'class' => 'input-xxlarge', 'placeholder' => 'Nome', 'required' => 'required')) ?>
      <small class="help-block"></small>
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'email') ? 'error' : '' ?>">
    <div class="controls" id="required-email">
        <?php echo Form::input('email', null, array('id' => 'email', 'class' => 'input-xxlarge', 'placeholder' => 'Email', 'required' => 'required', 'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")) ?>
      <small class="help-block"></small>
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'distributor') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::input('distributor', null, array('id' => 'distributor', 'class' => 'input-xxlarge', 'placeholder' => 'Distribuidor', 'required' => 'required')) ?>
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'city') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::input('city', null, array('id' => 'city', 'class' => 'input-xxlarge', 'placeholder' => 'Cidade', 'required' => 'required')) ?>
    </div>
  </div>

  <div class="control-group <?php echo Arr::get($errors, 'state') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::select('state', $states, null, array('id' => 'state', 'class' => 'input-xxlarge', 'placeholder' => 'Selecione um estado', 'required' => 'required')) ?>
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'subject') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::input('subject', null, array('id' => 'subject', 'class' => 'input-xxlarge', 'placeholder' => 'Assunto', 'required' => 'required')) ?>
    </div>
  </div>
  
  <div class="control-group <?php echo Arr::get($errors, 'message') ? 'error' : '' ?>">
    <div class="controls">
      <?php echo Form::textarea('message', null, array('id' => 'message', 'class' => 'input-xxlarge', 'placeholder' => 'Mensagem', 'required' => 'required')) ?>
    </div>
  </div>

      <div class="button-login">  
        <button type="submit" class="btn btn-primary"><i class="icon-off icon-white"></i> Enviar</button>
      </div>
    <p>
        <a href="manager/forgotpassword">Esqueci minha senha</a>
    <br />
    <a href="manager/">Voltar para login</a> 

  
  </div>
  <?php echo Form::close() ?>
