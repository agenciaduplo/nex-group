<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/banners'; ?>">Banners</a>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/banners'; ?>">Listar banners</a>
    </li>
  </ul>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())?>

<div class="row-fluid">    
	<div class="box col-md-12">
		<div class="box-inner">
  
			<div class="box-header well" data-original-title>
			  <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
			</div>
			<div class="box-content">
			  <table class="table table-striped table-bordered bootstrap-datatable">
				<thead>
				  <tr>
					<th>Imagem</th>
					<th>T&iacute;tulo</th>
					<th>Status</th>
					<th class="actions">Ações</th>
				  </tr>
				</thead>   
				<tbody>
				<?php foreach($banners as $b): ?>
				  <tr>
					<td class="td-imo-img"><img class="jqthumb" src="<?php echo utf8_encode($b->medias->find()->path); ?>" width="100"></td>
					<td><?php echo $b->title; ?></td>
					<td><?php echo $b->enabled == 1 ? '<span class="label-success label label-default">Ativo</span>' : '<span class="label-default label label-danger">Desativado</span>'; ?></td>
					<td class="actions">
					  <a class="btn btn-info" href="<?php echo URL::base(true) . 'manager/banners/edit/' . $b->id; ?>">
						<i class="glyphicon glyphicon-edit icon-white"></i> Editar  
					  </a>
					  <a class="btn btn-danger btn-delete" data-href="<?php echo URL::base(true) . 'manager/banners/delete/' . $b->id; ?>">
						<i class="glyphicon glyphicon-trash icon-white"></i> Excluir  
					  </a>
					</td>
				  </tr>
				<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
			  </table>            
			</div>
		</div>

	</div><!--/span-->

</div><!--/row-->
