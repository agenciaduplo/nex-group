<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/banners'; ?>">Banners</a>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/banners/edit'; ?>">Editar Banner</a>
    </li>
  </ul>
</div> 

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())->set('errors', $errors); ?>

<div class="row-fluid">
    <?php echo Form::open('manager/banners/update/'.$banner->id, array( 'id' => 'property_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
    <?php 
      echo View::factory('manager/banners/_form')
		->set('edit', true)
        ->set('places', $places) 
        ->set('banner', $banner) 
        ->set('errors', $errors) 
    ?>
    <?php echo Form::close() ?>

</div>