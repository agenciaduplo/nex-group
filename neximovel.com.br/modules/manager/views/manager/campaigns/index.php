<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Campanhas</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/campaigns'; ?>">Adicionar/Editar</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>

<a href="<?php echo URL::base(true) . 'manager/campaigns/new' ?>" class="btn btn-info">Adicionar</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>Nome</th>
			<th>Regional</th>
			<th>Tipo</th>
			<th>Status</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($campaigns as $campaign): ?>
          <tr>
            <td><?php echo $campaign->name; ?></td>
			<td><?php echo $campaign->region->name; ?></td>
			<td><?php echo $campaign->ctype->name; ?></td>
			<td><?php echo ($campaign->enabled == 1) ? 'Ativa' : 'Inativa'; ?></td>
            <td class="actions">
              <a class="btn" href="<?php echo URL::base(true) . 'manager/campaigns/edit/' . $campaign->id; ?>">
                <i class="icon-edit icon-black"></i>  
              </a>

              <a class="btn deleteCampaign" href="#" data-id="<?php echo $campaign->id; ?>">
                <i class="icon-trash icon-red"></i>
              </a>

            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>            
    </div>

<div id="dialog-confirm" title="Deletar campanha?" style="display:none">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta campanha será removida. Você tem certeza?</p>
</div>

  </div><!--/span-->

</div><!--/row-->
