<fieldset>
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">T&iacute;tulo</label>
		<div class="controls">
			<?php echo Form::input('name', @$campaign->name, array('id' => 'name', 'class' => 'input-xxlarge')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'type') ? 'error' : '' ?>">
        <label class="control-label" for="ctype_id">Categoria</label>
        <div class="controls">
              <?php echo Form::select('ctype_id', $ctypes, @$campaign->ctype_id, array('id' => 'ctype_id', 'class' => 'input-large')) ?>
        </div>
    </div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'region') ? 'error' : '' ?>">
        <label class="control-label" for="region_id">Regional</label>
        <div class="controls">
              <?php echo Form::select('region_id', $regions, @$campaign->region_id, array('id' => 'region_id', 'class' => 'input-large')) ?>
        </div>
    </div>
	
	<div class="control-group <?php echo Arr::get($errors, 'enabled') ? 'error' : '' ?>">
		<label class="control-label" for="enabled">Ativa</label>
		<div class="controls">
			<?php echo Form::select('enabled', array( 0 => 'N&atilde;o', 1 => 'Sim'), @$campaign->enabled, array('id' => 'enabled', 'class' => 'input-large')) ?>
		</div>
	</div>
	
	<div class="form-actions">
		<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
		ou <?php echo Html::anchor('manager/campaigns', 'cancelar') ?> 
	</div>
</fieldset>
