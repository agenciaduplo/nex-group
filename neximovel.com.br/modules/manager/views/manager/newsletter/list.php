<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/news/list'; ?>">Novidades</a> <span class="divider">/</span>
    </li>
  </ul>
  <hr>
</div>
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-list"></i><span class="break"></span>NOVIDADES</h2>
    </div>
    <div class="box-content">
		<ul id="news-categories">
		<li><a href="manager/news/list/">Todas</a></li>
		<?php foreach($categories as $c): ?>
			<li><a href="manager/news/list/<?php echo $c->id; ?>"><?php echo $c->name; ?></a></li>
		<?php endforeach; ?>
		</ul>
    
    <?php foreach($news as $new){ ?>
      <div class="wrap-news-item">
        <a href="manager/news/single/<?php echo $new->id ?>"><img class="img-thumbnail" src="<?php echo $new->image; ?>" /></a>

        <div class="box-info">
          <h5><?php echo $new->name; ?></h5>
          <p><?php echo substr(strip_tags($new->description), 0, 50).'...'; ?></p>
        </div>
      </div>
    <?php } ?>
    
    <?php echo $pagination->render(); ?>
		<div style="clear:both"></div>
    </div>

  </div><!--/span-->

</div><!--/row-->
