<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/news/list'; ?>">Novidades</a> <span class="divider">/</span>
    </li>
  </ul>
  <hr>
</div>
<style>

</style>
<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-list"></i><span class="break"></span>NOVIDADES</h2>
    </div>
    <div class="box-content">
      <div class="image-single">
        <img src="<?php echo $new->image; ?>" />
        <p><?php echo $new->name; ?></p>
        <p><?php echo date('d/m/Y', strtotime($new->dt_news)); ?></p>
        <p><?php echo $new->category->name; ?></p>
        <p><?php echo $new->author; ?></p>
      </div>

      <div class="description-single">
        <hr>
        <p><?php echo $new->description; ?></p>
      </div>
    </div>
  </div><!--/span-->

</div><!--/row-->
