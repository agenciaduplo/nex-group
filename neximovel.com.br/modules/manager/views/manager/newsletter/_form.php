<fieldset>
	<div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : '' ?>">
		<label class="control-label" for="name">T&iacute;tulo</label>
		<div class="controls">
			<?php echo Form::input('name', @$new->name, array('id' => 'name', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'description') ? 'error' : '' ?>">
		<div class="controls">
		  <?php echo Form::textarea('description', @$new->description, array('id' => 'description', 'class' => 'input-xxlarge tinymce')) ?>
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'dt_news') ? 'error' : '' ?>">
		<label class="control-label" for="dt_news">Data</label>
		<div class="controls">
		  <?php 
				$dt = !empty($new->dt_news) ? date('d/m/Y', strtotime($new->dt_news)) : date('d/m/Y');
				echo Form::input('dt_news', $dt, array('id' => 'dt_news', 'class' => 'input-xxlarge dpicker', 'required' => 'required')) ?>
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'image') ? 'error' : '' ?>">
		<label class="control-label" for="image">Imagem</label>
		<div class="controls">
		  <?php
			if($edit){
				echo Form::file('image', array('id' => 'image', 'class' => 'input-xxlarge'));
			} else {
				echo Form::file('image', array('id' => 'image', 'class' => 'input-xxlarge', 'required' => 'required')); 
			}
			?>
		  <small class="help-block">Tamanho (200x200)</small>
		</div>
	</div>
	
	<?php if($edit){ ?>
	<div class="control-group">
		<div class="controls">
			<img src="<?= $new->image ?>" style="height:150px">
		</div>
	</div>
	<? } ?>
	
	<div class="control-group <?php echo Arr::get($errors, 'author') ? 'error' : '' ?>">
		<label class="control-label" for="author">Autor</label>
		<div class="controls">
			<?php echo Form::input('author', @$new->author, array('id' => 'author', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
			<small class="help-block"></small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get(@$errors, 'category') ? 'error' : '' ?>">
        <label class="control-label" for="category_id">Categoria</label>
        <div class="controls">
              <?php echo Form::select('category_id', $categories, @$new->category_id, array('id' => 'category_id', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
        </div>
    </div>
	
	<div class="control-group <?php echo Arr::get($errors, 'enabled') ? 'error' : '' ?>">
		<label class="control-label" for="enabled">Ativa</label>
		<div class="controls">
			<?php echo Form::select('enabled', array( 0 => 'N&atilde;o', 1 => 'Sim'), @$new->enabled, array('id' => 'enabled', 'class' => 'input-xxlarge', 'required' => 'required')) ?>
		</div>
	</div>
	
	<div class="form-actions">
		<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
		ou <?php echo Html::anchor('manager/news', 'cancelar') ?> 
	</div>
</fieldset>
