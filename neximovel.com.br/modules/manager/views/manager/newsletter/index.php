<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Newsletter</a>
    </li>
  </ul>
</div>

<a href="<?php echo URL::base(true) . 'manager/newsletter/csv' ?>" class="btn btn-info">Exportar</a>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>
<div class="row-fluid">    
	<div class="box col-md-12">
		<div class="box-inner">
  
			<div class="box-header well" data-original-title>
			  <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
			</div>
			<div class="box-content">
			  <table class="table table-striped table-bordered bootstrap-datatable">
				<thead>
				  <tr>
					<th>Nome</th>
					<th>E-mail</th>
					<th>Origem</th>
					<th>Excluir</th>
				  </tr>
				</thead>   
				<tbody>
				<?php foreach($newsletters as $n){ ?>
				  <tr>
					<td><?php echo $n->name; ?></td>
					<td><?php echo $n->email; ?></td>
					<td><?php echo is_numeric($n->origin) ? ORM::factory('Property', $n->origin)->sale.ORM::factory('Property', $n->origin)->id : $n->origin; ?></td>
					<td class="actions">
					  <a class="btn btn-danger btn-delete" data-href="<?php echo URL::base(true) . 'manager/newsletter/delete/' . $n->id; ?>">
						<i class="glyphicon glyphicon-trash icon-white"></i> 
					  </a>
					</td>
				  </tr>
				<?php } ?>
				</tbody>
				<tfoot>
				  <tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
				  </tr>
				</tfoot>
			  </table>            
			</div>
		</div>

	</div><!--/span-->

</div><!--/row-->
