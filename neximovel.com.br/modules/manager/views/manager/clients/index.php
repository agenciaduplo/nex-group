<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/clients'; ?>">Clientes</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())?>

<a href="<?php echo URL::base(true) . 'manager/ajax/exportxml'; ?>" target="_blank" class="btn btn-info">Exportar XLS</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>CPF</th>
            <th>Nome</th>
            <th>Email</th>            
            <th>Data nasc.</th>
            <th>CEP</th>
            <th>Endereço</th>
            <th>Celular</th>
            <th>Telefone</th>
            <th>Sexo</th>            
          </tr>
        </thead>   
        <tbody>
        <?php foreach($users as $user): ?>
          <tr>
            <td><?php echo $user->cpf; ?></td>            
            <td><?php echo $user->name; ?></td>
            <td><?php echo $user->email; ?></td>            
            <td><?php echo date('d/m/Y',strtotime($user->nascimento)); ?></td>
            <td><?php echo $user->cep; ?></td>
            <td>
                <?php 
                  echo $user->endereco,',',$user->numero,' ',$user->complemento,' ',$user->cidade,' ',$user->estado;
                ?>
            </td>
            <td><?php echo $user->celular; ?></td>
            <td><?php echo $user->telefone; ?></td>
            <td><?php echo $user->sexo == 1 ? 'Masculino' : 'Feminino' ; ?></td>            
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
              <th></th>
              <th></th>                
              <th></th>
              <th></th> 
              <th></th>
              <th></th> 
              <th></th>
              <th></th> 
              <th></th>
          </tr>
        </tfoot>
      </table>            
    </div>

  </div><!--/span-->

</div><!--/row-->
