<ul class="nav nav-pills nav-stacked main-menu">   
	<!-- MENU ADMINISTRADOR -->
	<?php if(Auth::instance()->logged_in('admin')): ?>
	<li class="nav-header">Principal</li>
	<li <?php if (Request::current()->controller() == 'Dashboard'): ?>class="active"<?php endif ?>>
		<?php echo HTML::anchor('manager', '<i class="glyphicon glyphicon-eye-open"></i><span> Dashboard</span>', array('class' => 'ajax-link')) ?>
	</li>
	<li class="accordion">
		<a href="#"><i class="glyphicon glyphicon-home"></i><span> Im&oacute;veis</span></a>
		<ul class="nav nav-pills nav-stacked">
			<li><?php echo HTML::anchor('manager/properties', 'Listar im&oacute;veis', array('class' => 'ajax-link')) ?></li>
			<li><?php echo HTML::anchor('manager/properties/new', 'Cadastrar im&oacute;vel', array('class' => 'ajax-link')) ?></li>
			<li><?php echo HTML::anchor('manager/properties/import', 'Importar im&oacute;veis', array('class' => 'ajax-link')) ?></li>
		</ul>
	</li>
	<li class="accordion">
		<a href="#"><i class="glyphicon glyphicon-picture"></i><span> Banners</span></a>
		<ul class="nav nav-pills nav-stacked">
			<li><?php echo HTML::anchor('manager/banners', 'Listar banners', array('class' => 'ajax-link')) ?></li>
			<li><?php echo HTML::anchor('manager/banners/new', 'Cadastrar banner', array('class' => 'ajax-link')) ?></li>
		</ul>
	</li>
	<li class="accordion">
		<a href="#"><i class="glyphicon glyphicon-picture"></i><span> Destaques</span></a>
		<ul class="nav nav-pills nav-stacked">
			<li><?php echo HTML::anchor('manager/featured', 'Listar destaques', array('class' => 'ajax-link')) ?></li>
			<li><?php echo HTML::anchor('manager/featured/new', 'Cadastrar destaque', array('class' => 'ajax-link')) ?></li>
		</ul>
	</li>
	<li>
		<?php echo HTML::anchor('manager/users', '<i class="glyphicon glyphicon-user"></i><span> Cadastros</span>', array('class' => 'ajax-link')) ?>
	</li>
	<li>
		<?php echo HTML::anchor('manager/newsletter', '<i class="glyphicon glyphicon-envelope"></i><span> Newsletter</span>', array('class' => 'ajax-link')) ?>
	</li>
<?php endif; ?>
</ul>