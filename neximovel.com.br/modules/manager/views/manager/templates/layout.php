<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Free HTML5 Bootstrap Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="<?php echo URL::base(TRUE) ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo URL::base(TRUE) ?>assets/css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosenImage.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/animate.min.css' rel='stylesheet'>
    <link href='<?php echo URL::base(TRUE) ?>assets/css/dropzone.css' rel='stylesheet'>

	<script> var base_url = '<?php echo URL::base(TRUE) ?>'; </script>
    <!-- jQuery -->
    <script src="<?php echo URL::base(TRUE) ?>assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo URL::base(TRUE) ?>assets/http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

  <body class="<?php echo @$pageClass; ?>">
    <?php if(Auth::instance()->logged_in()): ?>
        <!-- topbar starts -->
		<div class="navbar navbar-default" role="navigation">

			<div class="navbar-inner">
				<button type="button" class="navbar-toggle pull-left animated flip">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html"> <img alt="DUPLO" src="<?php echo URL::base(TRUE) ?>assets/img/logo-duplo.png" class="hidden-xs"/>
					<span></span></a>

				<!-- user dropdown starts -->
				<div class="btn-group pull-right">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo Auth::instance()->get_user()->username; ?></span>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<!--<li><a href="#">Profile</a></li>-->
						<li class="divider"></li>
						<li><?php echo HTML::anchor('manager/logout', '<i class="icon-off"></i> Sair'); ?></li>
					</ul>
				</div>
				<!-- user dropdown ends -->

				<!-- theme selector starts -->
				<div class="btn-group pull-right theme-container animated tada" style="display:none">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-tint"></i><span
							class="hidden-sm hidden-xs"> Theme</span>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" id="themes">
						<li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
						<li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
						<li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
						<li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
						<li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
						<li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
						<li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
						<li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
						<li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
					</ul>
				</div>
				<!-- theme selector ends -->

				<ul class="collapse navbar-collapse nav navbar-nav top-menu">
					<li><a href="<?php echo URL::base(TRUE) ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
					<!--<li class="dropdown">
						<a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
								class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
							<li class="divider"></li>
							<li><a href="#">One more separated link</a></li>
						</ul>
					</li>
					<li>
						<form class="navbar-search pull-left">
							<input placeholder="Search" class="search-query form-control col-md-10" name="query"
								   type="text">
						</form>
					</li>-->
				</ul>

			</div>
		</div>
		<!-- topbar ends -->
    <?php endif; ?>
   
    <div class="container-fluid">
    <?php if(Auth::instance()->logged_in()): ?>
		<div class="ch-container">
			<div class="row">
			
				<!-- left menu starts -->
				<div class="col-sm-2 col-lg-2">
					<div class="sidebar-nav">
						<div class="nav-canvas">
							<div class="nav-sm nav nav-stacked">

							</div>
							<?php echo View::factory('manager/templates/_menu'); ?>
						</div>
					</div>
				</div>
				<div id="content" class="col-lg-10 col-sm-10">
					<?php echo $content ?>
				</div>
			</div>
		</div>
      <?php 
        //Login begin
        else: 
      ?>

          <?php echo $content ?>

      <?php 
        //Login begin
        endif; 
      ?>
		<!--<footer class="row">
			<p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://agenciaduplo.com.br" target="_blank">DUPLO</a> 2016</p>

			<p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
					href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
		</footer>-->
	
    </div><!--/.fluid-container-->


</div><!--/.fluid-container-->

<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Excluir im&oacute;vel</h3>
				</div>
				<div class="modal-body">
				<p>Voc&ecirc; tem certeza que deseja remover este im&oacute;vel?</p>
				</div>
				<div class="modal-footer">
				<a href="#" class="btn btn-default" data-dismiss="modal" data-action="cancel">Cancelar</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal" data-action="delete">Excluir</a>
			</div>
		</div>
	</div>
</div>

<!-- external javascript -->

<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- library for cookie management -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo URL::base(TRUE) ?>assets/bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo URL::base(TRUE) ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo URL::base(TRUE) ?>assets/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/chosen/chosenImage.jquery.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.history.js"></script>
<!-- input mask plugin -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/jquery.mask.min.js"></script>
<!-- GOOGLE MAPS -->
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?v=3.exp&#038;#038;ver=3.7.1'></script>
<script src="<?php echo URL::base(TRUE) ?>assets/js/gmap.js"></script>
<!-- Drag&Drop upload -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/dropzone.js"></script>

<script src="<?php echo URL::base(TRUE) ?>assets/js/jqthumb.min.js"></script>
<script src="<?php echo URL::base(TRUE) ?>assets/js/bootstrap-tooltip.js"></script>
<script src="<?php echo URL::base(TRUE) ?>assets/js/bootstrap-confirmation.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo URL::base(TRUE) ?>assets/js/charisma.js?ver=5.0"></script>


</body>
</html>
