<div class="box col-md-7">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Dados do Destaque</h2>
		</div>
		<div class="box-content">
			<fieldset>
			
				<div class="col-md-6 <?php echo Arr::get($errors, 'title') ? 'error' : ''; ?>">
					<label class="control-label" for="title">T&iacute;tulo*</label>
					<?php echo Form::input('title', $featured->title, array('id' => 'title', 'class' => 'form-control')); ?>
					<small class="help-block"></small>
				</div>
				
				<div class="col-md-6 <?php echo Arr::get($errors, 'subtitle') ? 'error' : ''; ?>">
					<label class="control-label" for="subtitle">Subt&iacute;tulo*</label>
					<?php echo Form::input('subtitle', $featured->subtitle, array('id' => 'subtitle', 'class' => 'form-control')); ?>
					<small class="help-block"></small>
				</div>
				
				<div class="col-md-6 <?php echo Arr::get($errors, 'link') ? 'error' : ''; ?>">
					<label class="control-label" for="link">Link</label>
					<?php echo Form::input('link', $featured->link, array('id' => 'link', 'class' => 'form-control')); ?>
					<small class="help-block"></small>
				</div>
				
				<div class="col-md-6 <?php echo Arr::get($errors, 'button') ? 'error' : ''; ?>">
					<label class="control-label" for="button">Texto do botão</label>
					<?php echo Form::input('button', $featured->button, array('id' => 'button', 'class' => 'form-control')); ?>
					<small class="help-block"></small>
				</div>
				
				<div class="form-group col-md-12 <?php echo Arr::get($errors, 'title') ? 'error' : ''; ?>">
					<div class="dropzone-man dz-featured" id="myAwesomeDropzone" >
						<div class="dz-message">Arraste e solte o featured aqui ou<br /> clique para carregar.</div>
					</div>
					<script>
						$(document).ready(function(){
								var myAwesomeDropzone = new Dropzone("div#myAwesomeDropzone", { url: "<?php echo URL::base(TRUE) ?>manager/featured/upload", 
									maxFiles: 1,
									accept: function(file, done) {
										console.log("uploaded");
										done();
									},
									init: function() {
										this.on("maxfilesexceeded", function(file){
											this.removeFile(file);
										});
									},
									removedfile: function(file) {
										
										var mi = document.createElement("input");
											mi.setAttribute('type', 'hidden');
											mi.setAttribute('name', 'delMedia[]');
											mi.setAttribute('value', file.id);
											
										$('#property_form').append(mi);
											
										var _ref;
										return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
									}
								});
								
								<?php foreach($featured->medias->find_all() as $k => $v) { ?>
									// Create the mock file:
									var mockFile_<?php echo $v->id; ?> = { name: "Mídia", size: 12345, id: <?php echo $v->id; ?>};

									// Call the default addedfile event handler
									myAwesomeDropzone.emit("addedfile", mockFile_<?php echo $v->id; ?>);

									// And optionally show the thumbnail of the file:
									myAwesomeDropzone.emit("thumbnail", mockFile_<?php echo $v->id; ?>, "<?php echo $v->path; ?>");
									
									myAwesomeDropzone.createThumbnailFromUrl(mockFile_<?php echo $v->id; ?>, "<?php echo $v->path; ?>");
									
									myAwesomeDropzone.emit("complete", mockFile_<?php echo $v->id; ?>);
								<?php } ?>
						});
					</script>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<!--<div class="box col-md-5">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Link</h2>
		</div>
		<div class="box-content">
			<label class="control-label" for="link"></label>
			<?php echo Form::input('link', $featured->link, array('id' => 'link', 'class' => 'form-control')); ?>
			<small class="help-block"></small>
		</div>
	</div>
</div>-->
<div class="box col-md-12">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Ações</h2>
		</div>
		<div class="box-content">
			<fieldset>
				<div class="col-md-1 <?php echo Arr::get($errors, 'enabled') ? 'error' : ''; ?>">
					<label class="control-label" for="status">Ativo</label>
					<?php echo Form::checkbox('enabled', '1', (bool) @$featured->enabled, array('id' => 'enabled', 'class' => 'form-control iphone-toggle', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="spacer"></div>
				<div class="form-actions col-md-12">
					<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')); ?>
					ou <?php echo Html::anchor('manager/properties', 'cancelar'); ?> 
				</div>
			</fieldset>
		</div>
	</div>
</div>