<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/featured'; ?>">Destaques</a>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/featured/new'; ?>">Cadastrar Destaque</a>
    </li>
  </ul>
</div> 

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())->set('errors', $errors); ?>

<div class="row-fluid">
    <?php echo Form::open('manager/featured/create', array( 'id' => 'property_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
    <?php 
      echo View::factory('manager/featured/_form')
		->set('edit', false)
        ->set('places', $places) 
        ->set('featured', $featured) 
        ->set('errors', $errors) 
    ?>
    <?php echo Form::close() ?>

</div>
