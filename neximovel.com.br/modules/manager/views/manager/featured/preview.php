<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/'; ?>">Home</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/tickets'; ?>">Solicita&ccedil;&atilde;o de Verba</a>
    </li>
  </ul>
  <hr>
</div> 
<div class="row-fluid">
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="icon-edit"></i><span class="break"></span>Confirmar Solicita&ccedil;&atilde;o de Verba</h2>
    </div>
    <div class="box-content">
	<fieldset>
		<center>
			<div class="control-group">
				<label class="control-label" for=""></label>
				<div class="controls">
				  <img src="<? echo $book->preview; ?>">
				  <small class="help-block"></small>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for=""></label>
				<div class="controls">
				  <small class="help-block"><? echo $book->description; ?></small>
				</div>
			</div>
		</center>
			<?php echo Form::open('manager/tickets/new/'.$book->id, array('id' => 'ticket_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>
		<div class="form-actions form-preview">
			<?php echo Form::button('save', 'solicitar', array('type' => 'submit', 'class' => 'btn btn-info')) ?>
			ou <a href="javascript:window.history.go(-1);">Voltar</a>
		</div>
	<?php echo Form::close() ?>
	</fieldset>
    </div>
  </div><!--/span-->

</div><!--/row-->
