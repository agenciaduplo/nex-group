<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Dados do Im&oacute;vel</h2>
		</div>
		<div class="box-content">
			<fieldset>
				
				<div class="form-group row-fluid">
				
					<div class="col-md-4 <?php echo Arr::get($errors, 'type_id') ? 'error' : ''; ?>">
						<label class="control-label" for="type_id">Tipo</label>
						<?php echo Form::select('type_id', $types, @$property->type_id, array('id' => 'type_id', 'class' => 'form-control mright','data-rel' => ' ')); ?>
						<small class="help-block"></small>
					</div>  
					
					<div class="col-md-2 <?php echo Arr::get($errors, 'beds') ? 'error' : ''; ?>">
						<label class="control-label" for="beds">Dormit&oacute;rios</label>
						<?php echo Form::input('beds', @$property->beds, array('id' => 'beds', 'class' => 'form-control', 'type' => 'number', 'min' => 0)); ?>
						<small class="help-block"></small>
					</div>  
					
					<div class="col-md-2 <?php echo Arr::get($errors, 'suite') ? 'error' : ''; ?>">
						<label class="control-label" for="suite">Suites</label>
						<?php echo Form::input('suite', @$property->suite, array('id' => 'suite', 'class' => 'form-control', 'type' => 'number', 'min' => 0)); ?>
						<small class="help-block"></small>
					</div>  
					
					<div class="col-md-2 <?php echo Arr::get($errors, 'parking') ? 'error' : ''; ?>">
						<label class="control-label" for="parking">Vagas</label>
						<?php echo Form::input('parking', @$property->parking, array('id' => 'parking', 'class' => 'form-control', 'type' => 'number', 'min' => 0)); ?>
						<small class="help-block"></small>
					</div> 
					
					<div class="col-md-2 <?php echo Arr::get($errors, 'area') ? 'error' : ''; ?>">
						<label class="control-label" for="area">Metragem</label>
						<?php echo Form::input('area', @$property->area, array('id' => 'area', 'class' => 'form-control ', 'type' => 'number', 'min' => 0)); ?>
						<small class="help-block"></small>
					</div> 
  
					
				</div>  
				<div class="form-group row-fluid">
					<div class="col-md-4 <?php echo Arr::get($errors, 'value') ? 'error' : ''; ?>">
						<label class="control-label" for="value">Pre&ccedil;o por</label>
						<div class="input-group">	
							<span class="input-group-addon"><b class="red">R$</b></span>
							<?php echo Form::input('value', @$property->value, array('id' => 'value', 'class' => 'form-control money')); ?>
						</div>
					</div>
					
					<div class="col-md-4 <?php echo Arr::get($errors, 'old_value') ? 'error' : ''; ?>">
						<label class="control-label" for="old_value">Pre&ccedil;o de</label>
						<div class="input-group">	
							<span class="input-group-addon"><b class="red">R$</b></span>
							<?php echo Form::input('old_value', @$property->old_value, array('id' => 'old_value', 'class' => 'form-control money')); ?>
						</div>
					</div>
					
					<div class="col-md-4 <?php echo Arr::get($errors, 'sale') ? 'error' : ''; ?>">
						<label class="control-label" for="_sale"></label>
						<div class="input-group">	
							<label class="control-label" for="sale">
							<?php 
								if($property->sale == 'DA'){
									$da = true;
									$av = false;
								} else if ($property->sale == 'AV'){
									$da = false;
									$av = true;
								} else {
									$da = false;
									$av = false;
								}
							?> 
							<?php echo Form::radio('sale', 'DA', $da, array('id' => 'sale', 'class' => '', 'required' => 'true')); ?>
							Dação
							</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="control-label" for="sale">
							<?php echo Form::radio('sale', 'AV', $av, array('id' => 'sale', 'class' => '', 'required' => 'true')); ?>
							Avulso
							</label>
						</div>
					</div>
					
				</div> 
					
				<div class="form-group row-fluid">
					<div class="col-md-6 <?php echo Arr::get($errors, 'splash_id') ? 'error' : ''; ?>">
						<label class="control-label" for="splash_id">Splash</label>
						<?php echo Form::select('splash_id', $splashes,@$property->splash_id, array('id' => 'splash_id', 'class' => 'form-control')); ?>
					</div>
				</div>
					
				<?php if($edit){ ?>
				<div class="form-group col-md-12 <?php echo Arr::get($errors, 'title') ? 'error' : ''; ?>">
					<label class="control-label" for="title">T&iacute;tulo*</label>
					<?php echo Form::input('title', @$property->title, array('id' => 'title', 'class' => 'form-control')); ?>
					<small class="help-block"></small>
				</div>
				<?php } ?>
				<div class="form-group col-md-12 <?php echo Arr::get($errors, 'description') ? 'error' : ''; ?>">
					<label class="control-label" for="description">Descri&ccedil;&atilde;o*</label>
					<?php echo Form::textarea('description', @$property->description, array('id' => 'description', 'class' => 'form-control', 'rows' => 5)); ?>
					<small class="help-block">Campo obrigatório.</small>
				</div>
				<div class="form-group col-md-12 <?php echo Arr::get($errors, 'obs') ? 'error' : ''; ?>">
					<label class="control-label" for="obs">Caracter&iacute;sticas*</label>
					<?php echo Form::textarea('obs', @$property->obs, array('id' => 'obs', 'class' => 'form-control', 'rows' => 5)); ?>
					<small class="help-block"></small>
				</div>
					
			</fieldset>
		</div>
	</div>
</div>

<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Localiza&ccedil;&atilde;o</h2>
		</div>
		<div class="box-content">
			<fieldset>
				<div class="form-group row-fluid">
					<div class="col-md-4 <?php echo Arr::get($errors, 'cep') ? 'error' : ''; ?>">
						<label class="control-label" for="cep">CEP*</label>
						<?php echo Form::input('cep', @$property->cep, array('id' => 'cep', 'class' => 'form-control cep', 'required' => 'required')); ?>
						<small class="help-block">Campo obrigatório.</small>
					</div>
				</div>
				<div class="form-group row-fluid">
					<div class="col-md-8 <?php echo Arr::get($errors, 'address') ? 'error' : ''; ?>">
						<label class="control-label" for="address">Endere&ccedil;o*</label>
						<?php echo Form::input('address', @$property->address, array('id' => 'address', 'class' => 'form-control', 'required' => 'required')); ?>
						<small class="help-block">Campo obrigatório.</small>
					</div>
					
					<div class="col-md-4 <?php echo Arr::get($errors, 'neighborhood') ? 'error' : ''; ?>">
						<label class="control-label" for="neighborhood">Bairro*</label>
						<?php echo Form::input('neighborhood', @$property->neighborhood, array('id' => 'neighborhood', 'class' => 'form-control', 'required' => 'required')); ?>
						<small class="help-block">Campo obrigatório.</small>
					</div>
				</div>
				
				<div class="form-group row-fluid">
					<div class="col-md-6 <?php echo Arr::get($errors, 'state') ? 'error' : ''; ?>">
						<label class="control-label" for="state">Estado</label>
						<?php echo Form::select('state', $states,@$property->city->state_id, array('id' => 'state', 'class' => 'form-control', 'required' => 'required')); ?>
					</div>
					
					<div class="col-md-6 <?php echo Arr::get($errors, 'city_id') ? 'error' : ''; ?>">
						<label class="control-label" for="city_id">Cidade</label>
						<?php echo Form::select('city_id', $cities, @$property->city_id, array('id' => 'city_id', 'class' => 'form-control', 'required' => 'required')); ?>
					</div>
				</div>
				
				<div class="form-group row-fluid">
					<div class="col-md-4 <?php echo Arr::get($errors, 'lat') ? 'error' : ''; ?>">
						<label class="control-label" for="lat">Latitude</label>
						<?php echo Form::input('lat', @$property->lat, array('id' => 'lat', 'class' => 'form-control', 'required' => 'required')); ?>
					</div>
					
					<div class="col-md-4 <?php echo Arr::get($errors, 'lng') ? 'error' : ''; ?>">
						<label class="control-label" for="lng">Longitude</label>
						<?php echo Form::input('lng', @$property->lng, array('id' => 'lng', 'class' => 'form-control', 'required' => 'required')); ?>
					</div>
					
					<div class="col-md-2">
						<label class="control-label" for="force_location">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<?php echo Form::button('force_location', 'Localizar', array('id' => 'force_location', 'class' => 'btn btn-success btn-sm')); ?>
					</div>
				</div>
				<div class="form-group row-fluid">
					<div id="map-canvas"></div>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<div class="box col-md-12">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>M&iacute;dia</h2>
		</div>
		<div class="box-content">
			<fieldset>
					
					<?php if(count($property->medias->find_all()) > 0){ ?>
						<div class="dropzone-man" id="myAwesomeDropzone"></div>
						
						<script>
							$(document).ready(function(){
								var myAwesomeDropzone = new Dropzone("div#myAwesomeDropzone", { url: "<?php echo URL::base(TRUE) ?>manager/properties/upload", 
									removedfile: function(file) {
										
										
										var mi = document.createElement("input");
											mi.setAttribute('type', 'hidden');
											mi.setAttribute('name', 'delMedia[]');
											mi.setAttribute('value', file.id);
										$('#property_form').append(mi);
											
										var _ref;
										return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
									}
								});
								
								<?php foreach($property->medias->order_by('order')->find_all() as $k => $v) { ?>
									// Create the mock file:
									var mockFile_<?php echo $v->id; ?> = { name: "<?php echo $k+1; ?>", size: 00000, id: <?php echo $v->id; ?>};

									// Call the default addedfile event handler
									myAwesomeDropzone.emit("addedfile", mockFile_<?php echo $v->id; ?>);

									// And optionally show the thumbnail of the file:
									myAwesomeDropzone.emit("thumbnail", mockFile_<?php echo $v->id; ?>, "<?php echo $v->path; ?>");
									
									myAwesomeDropzone.createThumbnailFromUrl(mockFile_<?php echo $v->id; ?>, "<?php echo $v->path; ?>");
									
									myAwesomeDropzone.emit("complete", mockFile_<?php echo $v->id; ?>);
								<?php } ?>
							});
							
							
						</script>
					<?php } else { ?>		
						<div action="<?php echo URL::base(TRUE) ?>manager/properties/upload" class="dropzone" id="myAwesomeDropzone"></div>
					<?php } ?>
				
			</fieldset>
		</div>
	</div>
</div>

<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Banner</h2>
		</div>
		<div class="box-content">
			<label class="control-label" for="places">Escolha opcionalmente banners que aparecerão nos detalhes do imóvel</label>
			<?php //echo Form::select('banners[]', $banners, null, array('class' => 'chosen-select w100 p10', 'multiple' => '', 'data-rel' => 'chosen', 'data-placeholder' => "Escolha a colocação deste banner", 'tabindex' => "-1")); ?>
			<select name="banners[]" class="chosen-select w100 p10" data-rel="chosen" data-placeholder="Escolha um banner" tabindex="-1" multiple>
				<?php foreach($banners as $b){ ?>
					<?php if($property->has('banners', $b->id)){ ?>
					<option selected="selected" value="<?php echo $b->id; ?>" data-img-src="<?php echo $b->medias->find()->path; ?>"><?php echo $b->title; ?></option>
					<?php } else {?>
					<option value="<?php echo $b->id; ?>" data-img-src="<?php echo $b->medias->find()->path; ?>"><?php echo $b->title; ?></option>
					<?php }?>
				<?php }?>
			</select>
			<small class="help-block"><?php echo Html::anchor('manager/banners/new', 'Cadastrar banner'); ?></small>
		</div>
	</div>
</div>
<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Imóveis relacionados</h2>
		</div>
		<div class="box-content">
			<fieldset>
				<div class="col-md-4">
					<label class="control-label" for="related_1">Imóvel 1</label>
					<?php echo Form::input('related[]', @$related[0], array('id' => 'related_1', 'class' => 'form-control')); ?>
					<small class="help-block">Código do imóvel</small>
				</div>
				<div class="col-md-4">
					<label class="control-label" for="related_2">Imóvel 2</label>
					<?php echo Form::input('related[]', @$related[1], array('id' => 'related_2', 'class' => 'form-control')); ?>
					<small class="help-block">Código do imóvel</small>
				</div>
				<div class="col-md-4">
					<label class="control-label" for="related_3">Imóvel 3</label>
					<?php echo Form::input('related[]', @$related[2], array('id' => 'related_3', 'class' => 'form-control')); ?>
					<small class="help-block">Código do imóvel</small>
				</div>
			</fieldset>
		</div>
	</div>
</div>
<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Destino</h2>
		</div>
		<div class="box-content">
			<fieldset>
				<div class="col-md-4 <?php echo Arr::get($errors, 'neximovel') ? 'error' : ''; ?>">
					<label class="control-label" for="status">Neximovel</label>
					<?php echo Form::checkbox('neximovel', '1', (bool) @$property->neximovel, array('id' => 'neximovel', 'class' => 'form-control iphone-toggle', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="col-md-4 <?php echo Arr::get($errors, 'coberturas') ? 'error' : ''; ?>">
					<label class="control-label" for="highlight">Coberturas</label>
					<?php echo Form::checkbox('coberturas', '1', (bool) @$property->coberturas, array('id' => 'coberturas', 'class' => 'form-control iphone-toggle', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="col-md-4 <?php echo Arr::get($errors, 'compromisso') ? 'error' : ''; ?>">
					<label class="control-label" for="status">Compromisso</label>
					<?php echo Form::checkbox('compromisso', '1', (bool) @$property->compromisso, array('id' => 'compromisso', 'class' => 'form-control iphone-toggle', 'data-no-uniform' => 'true', )); ?>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<div class="box col-md-6">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>Ações</h2>
		</div>
		<div class="box-content">
			<fieldset>
				<div class="col-md-4 <?php echo Arr::get($errors, 'enabled') ? 'error' : ''; ?>">
					<label class="control-label" for="status">Ativo</label>
					<?php echo Form::checkbox('enabled', '1', (bool) @$property->enabled, array('id' => 'enabled', 'class' => 'form-control iphone-toggle', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="col-md-4 <?php echo Arr::get($errors, 'highlight') ? 'error' : ''; ?>">
					<label class="control-label" for="highlight">Destaque</label>
					<?php echo Form::checkbox('highlight', '1', (bool) @$property->highlight, array('id' => 'highlight', 'class' => 'form-control', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="col-md-4 <?php echo Arr::get($errors, 'enabled') ? 'error' : ''; ?>">
					<label class="control-label" for="status">Vendido</label>
					<?php echo Form::checkbox('sold', '1', (bool) @$property->sold, array('id' => 'sold', 'class' => 'form-control', 'data-no-uniform' => 'true', )); ?>
				</div>
				<div class="spacer"></div>
				<div class="spacer"></div>
				<div class="form-actions col-md-12">
					<?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')); ?>
					ou <?php echo Html::anchor('manager/properties', 'cancelar'); ?> 
				</div>
			</fieldset>
		</div>
	</div>
</div>
