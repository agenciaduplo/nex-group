<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/properties'; ?>">Im&oacute;veis</a>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/properties'; ?>">Listar Im&oacute;veis</a>
    </li>
  </ul>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())?>

<div class="row-fluid">    
	<div class="box col-md-12">
		<div class="box-inner">
  
			<div class="box-header well" data-original-title>
			  <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
			</div>
			<div class="box-content">
			  <table class="table table-striped table-bordered bootstrap-datatable">
				<thead>
				  <tr>
					<th>Imagem</th>
					<th>ID</th>
					<th>Título</th>
					<th>Descri&ccedil;&atilde;o</th>
					<th>Data</th>
					<th>Status</th>
					<th class="actions">Ações</th>
				  </tr>
				</thead>   
				<tbody>
				<?php foreach($properties as $p): ?>
				  <tr>
					<td class=""><img class="" src="<?php echo $p->medias->order_by('order')->find()->thumb_200; ?>" width="100"></td>
					<td><?php echo $p->sale.$p->id; ?></td>
					<td><?php echo $p->title; ?></td>
					<td><?php echo Text::limit_chars($p->description, 50, '...', true); ?></td>
					<td><?php echo date('d/m/Y - H:i:s', strtotime($p->dt_created)); ?></td>
					<td><?php echo $p->enabled == 1 ? '<span class="label-success label label-default">Ativo</span>' : '<span class="label-default label label-danger">Desativado</span>'; ?></td>
					<td class="actions">
					  <a class="btn btn-info" href="<?php echo URL::base(true) . 'manager/properties/edit/' . $p->id; ?>">
						<i class="glyphicon glyphicon-edit icon-white"></i> Editar  
					  </a>
					  <a class="btn btn-danger btn-delete" data-href="<?php echo URL::base(true) . 'manager/properties/delete/' . $p->id; ?>">
						<i class="glyphicon glyphicon-trash icon-white"></i> Excluir  
					  </a>
					</td>
				  </tr>
				<?php endforeach; ?>
				</tbody>
				<tfoot>
				  <tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
				  </tr>
				</tfoot>
			  </table>            
			</div>
		</div>

	</div><!--/span-->

</div><!--/row-->
