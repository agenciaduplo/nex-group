<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/properties'; ?>">Im&oacute;veis</a>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/properties'; ?>">Listar Im&oacute;veis</a>
    </li>
  </ul>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())?>

<div class="row-fluid">    
	<div class="box col-md-12">
		<div class="box-inner">
  
			<div class="box-header well" data-original-title>
			  <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
			</div>
			<div class="box-content">
			  <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
				<thead>
				  <tr>
					<th>Imagem</th>
					<th>ID</th>
					<th>Título</th>
					<th class="actions">Ações</th>
				  </tr>
				</thead>   
				<tbody>
				<?php foreach($properties as $p){ ?>
					<?php if(isset($p['developmentPictures']) &&  !in_array($p['_id'], $imported)){ ?>
					  <tr>
						<td class=""><img class="" src="https://cdn-my.konecty.com/rest/image/crop/200/200/nexgroup/<?php echo $p['developmentPictures'][0]['key']; ?>" width="100"></td>
						<td><?php echo $p['_id']; ?></td>
						<td><?php echo $p['development']['name']; ?></td>
						<td class="actions">
						  <a class="btn btn-info btn-import" data-href="<?php echo URL::base(true) . 'manager/properties/get_property/' . $p['_id']; ?>">
							<i class="glyphicon glyphicon-edit icon-white"></i> Importar  
						  </a>
						</td>
					  </tr>
					<?php } ?>
				<?php } ?>
				</tbody>
				<tfoot>
				  <tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
				  </tr>
				</tfoot>
			  </table>            
			</div>
		</div>

	</div><!--/span-->

</div><!--/row-->
 