<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager/properties'; ?>">Im&oacute;veis</a>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/properties/new'; ?>">Editar Im&oacute;vel</a>
    </li>
  </ul>
</div> 

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get())->set('errors', $errors); ?>

<div class="row-fluid">
	<?php echo Form::open('manager/properties/update/'.$property->id, array('id' => 'property_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) ?>				
				<?php 
				  echo View::factory('manager/properties/_form')
					->set('edit', true)
					->set('property', $property)
					->set('cities', $cities)
					->set('states', $states)
					->set('types', $types)
					->set('splashes', $splashes)
					->set('banners', $banners)
					->set('related', $related)
					->set('errors', $errors); 
				?>
	<?php echo Form::close() ?>
</div>