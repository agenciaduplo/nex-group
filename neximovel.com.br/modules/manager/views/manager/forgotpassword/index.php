<?php echo HTML::image('assets/img/logo-duplo-login.png', array('class' => 'logo-login')); ?>
<div class="login-box">
  <?php echo Form::open('manager/forgotpassword', array('class' => 'form-horizontal')) ?>
    <?php echo HTML::image('assets/img/logo-box-login.png', array('class' => '')); ?>
    <fieldset>
       
      <?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>    
      <h3>Informe seu e-mail para receber uma nova senha</h3>
      <div class="input-prepend <?php echo Arr::get($errors, 'email') ? 'error' : '' ?>" title="Password">
        <span class="add-on"><i class="icon-lock"></i></span>        
        <?php echo Form::input('email', null, array('id' => 'email', 'class' => 'input-large span10', 'placeholder' => 'Email')) ?>
      </div>
      <div class="clearfix"></div>
      <!-- <label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label> -->

      <div class="button-login">  
        <button type="submit" class="btn btn-success"><i class="icon-off icon-white"></i> Enviar nova senha</button>
      </div>
      <div class="clearfix"></div>
      <p class="forgotpassword"><a href="manager/login">&lt;&lt; voltar para o login</a></p>

  <?php echo Form::close() ?>
</div><!--/span-->
