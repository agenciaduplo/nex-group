<div>
  <ul class="breadcrumb">
    <li>
      <a href="#">Home</a>
    </li>
    <li>
      <a href="#">Dashboard</a>
    </li>
  </ul>
</div>

<div class=" row">
	<div class="col-md-3 col-sm-3 col-xs-6">
		<a data-toggle="tooltip" title="" class="well top-block" href="#" data-original-title="">
			<i class="glyphicon glyphicon-home blue"></i>
			<div>Im&oacute;veis</div>
			<div><?php echo $property_count; ?></div>
			<span class="notification"><?php echo $property_active_count; ?></span>
		</a>
	</div>
	
	<div class="col-md-3 col-sm-3 col-xs-6">
		<a data-toggle="tooltip" title="" class="well top-block" href="#" data-original-title="">
			<i class="glyphicon glyphicon-star green"></i>
			<div>Destaques</div>
			<div><?php echo $highlights; ?></div>
			<span class="notification green"><?php echo $highlights; ?></span>
		</a>
	</div>
	<!--
	<div class="col-md-3 col-sm-3 col-xs-6">
		<a data-toggle="tooltip" title="" class="well top-block" href="#" data-original-title="$340.000,00 em novas vendas.">
			<i class="glyphicon glyphicon-shopping-cart yellow"></i>
			<div>Vendas</div>
			<div>$1.332.000,00</div>
			<span class="notification yellow">$340.000,00</span>
		</a>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6">
		<a data-toggle="tooltip" title="" class="well top-block" href="#" data-original-title="12 new messages.">
			<i class="glyphicon glyphicon-envelope red"></i>
			<div>Mensagens</div>
			<div>25</div>
			<span class="notification red">12</span>
		</a>
	</div>
	-->
</div>
