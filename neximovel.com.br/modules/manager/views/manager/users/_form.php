<fieldset>
<?php if(!$edit) { ?>
	<div class="control-group <?php echo Arr::get($errors, 'role') ? 'error' : ''; ?>">
		<label class="control-label" for="role">Tipo</label>
		<div class="controls">
		  <?php echo Form::select('role', $roles, @$role, array('id' => 'role', 'class' => 'input-xxlarge')); ?>
		  <small class="help-block"></small>
		</div>
	</div>
<?php } ?>
	  <div class="control-group <?php echo Arr::get($errors, 'name') ? 'error' : ''; ?>">
		<label class="control-label" for="user_name">Nome*</label>
		<div class="controls">
		  <?php echo Form::input('name', $user->name, array('id' => 'user_name', 'class' => 'input-xxlarge')); ?>
		  <small class="help-block">Campo obrigatório. Máximo 255 caracteres</small> 
		</div>
	  </div>

	<div class="control-group <?php echo Arr::get($errors, 'username')? 'error' : ''; ?>">
		<label class="control-label" for="user_username">E-mail*</label>
		<div class="controls">
		  <?php echo Form::input('username', $user->username, array('id' => 'user_username', 'class' => 'input-xlarge')); ?>
		  <small class="help-block">Campo obrigatório. Máximo 255 caracteres</small> 
		</div>
	</div>
	
	<div class="control-group <?php echo Arr::get($errors, 'phone')? 'error' : ''; ?>">
		<label class="control-label" for="phone">Telefone</label>
		<div class="controls">
		  <?php echo Form::input('telefone', $user->telefone, array('id' => 'telefone', 'class' => 'input-xlarge phone')); ?>
		  <small class="help-block"></small> 
		</div>
	</div>
	

	<div class="control-group <?php echo Arr::path($errors, 'password') ? 'error' : ''; ?>">
		<label class="control-label" for="user_password">Senha*</label>
		<div class="controls">
		  <?php echo Form::password('password', null, array('id' => 'user_password', 'class' => 'input-medium')); ?>
		  <small class="help-block">Campo obrigatório. Mínimo 8 caracteres</small>  
		</div>
	</div>

	<div class="control-group <?php echo Arr::path($errors, 'password_confirm') ? 'error' : ''; ?>">
		<label class="control-label" for="user_password_confirm">Confirmação da senha*</label>
		<div class="controls">
		  <?php echo Form::password('password_confirm', null, array('id' => 'user_password_confirm', 'class' => 'input-medium')); ?>
		  <small class="help-block">Campo obrigatório.</small>
		</div>
	</div>

	<div class="control-group <?php echo Arr::get($errors, 'attendant') ? 'error' : ''; ?>">
		<label class="control-label" for="status">Atendimento</label>
		<div class="controls">
		  <?php echo Form::select('attendant', $attendants, @$user->attendant, array('id' => 'attendant', 'class' => 'input-xxlarge')); ?>
		  <small class="help-block"></small>
		</div>
	</div>
	  
	<div class="control-group <?php echo Arr::get($errors, 'enabled') ? 'error' : ''; ?>">
		<label class="control-label" for="status">Ativo</label>
		<div class="controls">
		  <?php echo Form::select('enabled', array('1' => 'Sim', '0' => 'Não'), $user->enabled, array('id' => 'enabled', 'class' => 'input-xxlarge')); ?>
		  <small class="help-block">Em caso de desativação o usuário não poderá mais ser reativado</small>
		</div>
	</div>
  
  
 
	<div id="dn_block" style="display:<?php echo (($edit && ($user->has('roles', array(5)) || $user->has('roles', array(6)) )) || @$role == 'committee' || @$role == 'distributor') ? 'block' : 'none'; ?>">

		<?php 
			if($edit){
				foreach($user->dns->find_all() as $dn){ 
		?>
				<div class="control-group clone-block">
					<label class="control-label" for="dn_add">Adicionar DN</label>
					<div class="controls">
					  <?php echo Form::select('dn_add[]', $dns, $dn->id, array('id' => 'dn_add', 'class' => 'input-xlarge')); ?>
					  <?php echo Form::button('btn_delete_dn', '- DN', array('id' => 'btn_delete_dn', 'type' => 'button', 'class' => 'btn btn-danger btn_delete_dn')); ?>
					  <small class="help-block"></small>
					</div>
				</div>
		<?php
				}
			} else { 
		?>
				<div class="control-group clone-block">
					<label class="control-label" for="dn_add">Adicionar DN</label>
					<div class="controls">
					  <?php echo Form::select('dn_add[]', $dns, null, array('id' => 'dn_add', 'class' => 'input-xlarge')); ?>
					  <?php echo Form::button('btn_delete_dn', '- DN', array('id' => 'btn_delete_dn', 'type' => 'button', 'class' => 'btn btn-danger btn_delete_dn')); ?>
					  <small class="help-block"></small>
					</div>
				</div>
		<?php } ?>
	  <div class="control-group <?php echo Arr::get($errors, 'btn_add_dn') ? 'error' : ''; ?>">
		<label class="control-label" for="btn_add_dn"></label>
		<div class="controls">
		  <?php echo Form::button('btn_add_dn', '+ DN', array('id' => 'btn_add_dn', 'type' => 'button', 'class' => 'btn btn-info')); ?>
		  <small class="help-block"></small>
		</div>
	  </div>
	</div>
	
  <div class="form-actions">
    <?php echo Form::button('save', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-info')); ?>
    ou <?php echo Html::anchor('manager/users', 'cancelar'); ?> 
  </div>
</fieldset>
