<div>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/users'; ?>">Usuários</a>
    </li>
  </ul>
</div>

<a href="<?php echo URL::base(true) . 'manager/users/csv' ?>" class="btn btn-info">Exportar</a>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>
<div class="row-fluid">    
	<div class="box col-md-12">
		<div class="box-inner">
  
			<div class="box-header well" data-original-title>
			  <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
			</div>
			<div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable users">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Usuário</th>
            <th>Tipo</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($users as $user): ?>
          <tr>
            <td><?php echo $user->name; ?></td>
            <td><?php echo $user->username; ?></td>
            <td>
              <?php 
                $roles = $user->roles->find_all();				
				echo @$roles[1]->description;				
              ?>
            </td>
           
          </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
                <th></th>
                <th></th>
                <th></th>
          </tr>
        </tfoot>
      </table>            
    </div>
		</div>

	</div><!--/span-->

</div><!--/row-->
