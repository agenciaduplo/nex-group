<div>
  <hr>
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo URL::base(true) . 'manager'; ?>">Home</a> <span class="divider">/</span>
    </li>
	<li>
      <a href="<?php echo URL::base(true) . 'manager/books'; ?>">Book</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo URL::base(true) . 'manager/bcategories'; ?>">Categorias</a>
    </li>
  </ul>
  <hr>
</div>

<?php echo View::factory('manager/templates/notices')->set('messages', Notices::get()) ?>

<a href="<?php echo URL::base(true) . 'manager/bcategories/new' ?>" class="btn btn-info">Adicionar</a>

<a href="<?php echo URL::base(true) . 'manager/bcategories/update' ?>" class="btn btn-danger disable-bcategories">Desativar</a>

<div class="row-fluid">    
  <div class="box span12">
    <div class="box-header" data-original-title>
      <h2><i class="fa fa-list"></i><span class="break"></span>Listagem</h2>
      <!-- <div class="box-icon">
        <a href="#" class="btn-setting"><i class="icon-wrench"></i></a>
        <a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
        <a href="#" class="btn-close"><i class="icon-remove"></i></a>
      </div> -->
    </div>
    <div class="box-content">
      <table class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
          <tr>
            <th>Nome da categoria</th>
			<th>Status</th>
			<th>Desativar</th>
            <th class="actions">A&ccedil;&otilde;es</th>
          </tr>
        </thead>   
        <tbody>
        <?php foreach($bcategories as $bcategory): ?>
          <tr>
            <td><?php echo $bcategory->name; ?></td>
			<td><?php echo $bcategory->enabled ? 'Ativa' : 'Inativa'; ?></td>
			<td><?php echo Form::checkbox('disable_cat[]', $bcategory->id, array( 'class' => 'input-xxlarge')) ?></td>
            <td class="actions">
              <a class="btn" href="<?php echo URL::base(true) . 'manager/bcategories/edit/' . $bcategory->id; ?>">
                <i class="icon-edit icon-black"></i>  
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>            
    </div>
<div id="dialog-confirm" title="Desativar categorias?" style="display:none">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Estas categorias ser&atilde;o desativadas. Você tem certeza?</p>
</div>
  </div><!--/span-->

</div><!--/row-->
