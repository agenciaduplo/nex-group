/*
Navicat MySQL Data Transfer

Source Server         : [NEX] NEX Imóvel (HML)
Source Server Version : 50630
Source Host           : neximovel.mysql.dbaas.com.br:3306
Source Database       : neximovel

Target Server Type    : MYSQL
Target Server Version : 50630
File Encoding         : 65001

Date: 2016-11-23 09:39:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `featured_medias`
-- ----------------------------
DROP TABLE IF EXISTS `featured_medias`;
CREATE TABLE `featured_medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `featured_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of featured_medias
-- ----------------------------
INSERT INTO featured_medias VALUES ('9', '1592', '16');
INSERT INTO featured_medias VALUES ('10', '1593', '19');
INSERT INTO featured_medias VALUES ('11', '1594', '20');
