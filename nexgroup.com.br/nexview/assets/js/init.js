$(document).ready(function()
{

    $("#list_empreendimento").change(function(){
        if($(this).val() == 'all')
        {
          window.location = base_url+"kanban/all-tasks";
          return true;
        }

        window.location = base_url+$(this).attr('data-nivel')+"/task/"+$(this).val();
        return true;
    });


    $('#botao').click(function(){
        if($('#colunas-swift').hasClass("on-swift"))
        {
            $('#colunas-swift').removeClass('on-swift');
            $('#colunas-swift').addClass('off-swift');
        }
        else if($('#colunas-swift').hasClass("off-swift"))
        {
            $('#colunas-swift').removeClass('off-swift');
            $('#colunas-swift').addClass('on-swift');
        }
    });


    //  -------- dragHandle -------- //

    $('.table-dnd').tableDnD({
        onDrop: function(table, row)
        {
            var rows = table.tBodies[0].rows;
            var debugStr = "";

            for(var i = 0; i < rows.length; i++)
            {
                debugStr += rows[i].id;

                if(i != (rows.length-1)) { debugStr += ","; }
            }

            console.log(debugStr);

            gravaOrdemProjetos(debugStr);
        },
        dragHandle: ".dragHandle"
    });


    $("#table tr").hover(function(){
        $(this.cells[0]).addClass('showDragHandle');
    },function(){
        $(this.cells[0]).removeClass('showDragHandle');
    });


    //-------- HEADER MENU MOBILE --------//
    $("#abrir").click(function(){
        var displayMenu = $("#menu").css('display');

        if(displayMenu == "none")
        {
            $("#menu").animate({
                height: "show", opacity: "toggle"
            }, { duration: "fast" });
        }
        else
        {
            $("#menu").animate({
                height: "hide", opacity: "toggle"
            }, { duration: "fast" });
        }
    });


    //-------- CARROSEUL LISTA DE EMPREENDIMENTOS HOME --------//
    $('#foo4').carouFredSel({
        circular: false,
        responsive: true,
        prev: '#prev4',
        next: '#next4',
        mousewheel: false,
        swipe: {
            onTouch: true
        },
        scroll: {
            fx           : "cover-fade",
            items        : 1,
            easing       : "linear",
            duration     : 1500,                         
            pauseOnHover : true
        },
        pagination: "#pager4",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });

});
// end $(document).ready();


function gravaOrdemProjetos(debugStr)
{
    new imageLoader(cImageSrc, 'startAnimation()');

    var response = '';

    $.ajax({
        type: "POST",
        url: base_url + "home/gravaOrdemProjetos",
        data: {
            idprojetos: debugStr
        },
        success: function(response)
        {
            stopAnimation();
        },
        error: function(request, status, error){
            console.log(request.responseText);
        }
    });
}