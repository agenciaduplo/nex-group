$(document).ready(function()
{
	$('.setor').click(function(event)
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		var classe = '';

		if(this.checked)
		{
			$('input[setor="'+ this.value +'"]').each(function()
			{
				$(this).prop('checked', true);
				$(this).removeClass('disabled');
				// $(this).parent().show();

				classe = $(this).attr('class').split(' ');

				$('.marco', $(this).parent().parent()).prop('checked', false);

				if($('.'+classe[2]).length == $('.'+classe[2]+':checked').length)
					$('.marco', $(this).parent().parent()).prop('checked', true);

				$('.marco', $(this).parent().parent()).removeClass('disabled');
				// $('.marco', $(this).parent().parent()).parent().parent().show(); // .box
			});		
		}
		else
		{
			$('input[setor="'+ this.value +'"]').each(function()
			{
				$(this).prop('checked', false);
				$(this).addClass('disabled');
				// $(this).parent().hide();

				$('.marco', $(this).parent().parent()).prop('checked', false);
				$('.marco', $(this).parent().parent()).addClass('disabled');

				classe = $(this).attr('class').split(' ');

				if($('.'+classe[2]).length == ($('.disabled', $('.'+classe[2]).parent().parent()).length - 1))
				{
					// $('.marco', $(this).parent().parent()).parent().parent().hide(); // .box
				}
			});

		}

		Filtro.init();
		Filtro.updateRestricoes();
	});


	$('.marco_all').click(function(event)
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		if(this.checked)
		{
			$('.setor').prop('checked', true);
			$('.marco').prop('checked', true);
			$('.tarefa').prop('checked', true);
		}
		else
		{
			$('.setor').prop('checked', false);
			$('.marco').prop('checked', false);
			$('.tarefa').prop('checked', false);
		}

		Filtro.init();
		Filtro.updateRestricoes();
	});

	$('.marco').click(function(event)
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		if(this.checked)
			$('.'+$(this).attr("group")).prop('checked', true);
		else
			$('.'+$(this).attr("group")).prop('checked', false);

		Filtro.init();
		Filtro.updateRestricoes();
	});


	$('.tarefa').click(function(event)
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		if(this.checked)
		{
			var classe = $(this).attr('class').split(' ');

			// Todas tarefas do marcos estão checked(true), deixar o marco das tarefas checked(true)
			if($('.'+classe[2]).length == $('.'+classe[2]+':checked').length)
			{
				$('.marco', $(this).parent().parent()).prop('checked', true);
			}
		}
		else
		{
			// Define checked(false) para o marco
			$('.marco', $(this).parent().parent()).prop('checked', false);		
		}

		Filtro.init();
		Filtro.updateRestricoes();
	});

	$("#data_inicio").change(function()
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		if($(this).val() != "" && !valData($(this).val()))
		{
			$(this).addClass("error");
			$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", false);
			return false;
		}
		
		$(this).removeClass("error");

		Filtro.init();
		Filtro.updateRestricoes();
	});

	$("#data_fim").change(function()
	{
		$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", true);
		new imageLoader(cImageSrc, 'startAnimation()');

		if($(this).val() != "" && !valData($(this).val()))
		{
			$(this).addClass("error");
			$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", false);
			return false;
		}

		$(this).removeClass("error");

		Filtro.init();
		Filtro.updateRestricoes();
	});

});


var Filtro  = {
	setores: [],
	arr_marco_id: [],
	arr_tarefa_id: [],
	init: function()
	{
		console.log('init() starded...');

		this.setores  		= [];
		this.arr_marco_id  	= [];
		this.arr_tarefa_id 	= [];

		$('.setor:checked').each(function()
		{
			Filtro.setores.push(this.value);
		});

		$('.marco:checked').each(function()
		{
			Filtro.arr_marco_id.push(this.value);
		});

		// Define checked true para "Todas as Atividades" quando todos marcos estiverem selecionados
		if($('.marco').length == $('.marco:checked').length)
			$('.marco_all').prop('checked', true);
		else
			$('.marco_all').prop('checked', false);

		// Define checked true para todas as ativides pertencente ao grupo do marco selecionado
		$('.marco:checked').each(function()
		{
			var classe = $(this).attr("group");
			$('.tarefa.'+classe).prop('checked', true);
		});


		var classe = '';

		$('.tarefa').each(function()
		{
			classe = $(this).attr('class').split(' ');

			if($('.'+classe[2]).length > $('.'+classe[2]+':checked').length)
			{
				if($(this).is(':checked') && $.inArray("disabled", classe) == -1)
					Filtro.arr_tarefa_id.push(this.value);
			}
		});

		console.log(this.setores);
		console.log(this.arr_marco_id);
		console.log(this.arr_tarefa_id);
	},
	updateRestricoes: function()
	{
		console.log('updateRestricoes() starded...');

		$.ajax({
			type: "POST",
			url:  base_url+"filtros/filtraRestricoesXHR/",
			data: {
				idprojeto: 			$("#idprojeto").val(),
				idprojeto_tarefas: 	$("#idprojeto_tarefas").val(),
				setores: 			Filtro.setores,
				arr_marco_id:  		Filtro.arr_marco_id,
				arr_tarefa_id: 		Filtro.arr_tarefa_id,
				data_inicio: 		$("#data_inicio").val(),
				data_final: 		$("#data_fim").val()
			},
			success: function(msg)
			{
				$("#restricoes_load").html(msg).promise().done(function(){
					$('input[type=checkbox], input[type=text]', '#filtros').prop("disabled", false);
					stopAnimation();
				});
			},
			error: function(request, status, error)
			{
				alert('Falha ao realizar filtro');
				console.log(request.responseText);
				stopAnimation();
			}
		});
	}
}







// function valData()
// String data 'dd/mm/yyyy'
function valData(data)
{ 
	ret = true;

	if(data.length == 10)
	{
		day   = data.substring(0,2);
		month = data.substring(3,5);
		year  = data.substring(6,10);

		if((month==01) || (month==03) || (month==05) || (month==07) || (month==08) || (month==10) || (month==12))
		{ //mes com 31 dias
			if((day < 01) || (day > 31))
			{
				ret = false;
			}
		}
		else if((month==04) || (month==06) || (month==09) || (month==11))
		{
			//mes com 30 dias
			if((day < 01) || (day > 30)){
				ret = false;
			}
		}
		else if((month==02))
		{ 
			//February and leap year
			if((year % 4 == 0) && ( (year % 100 != 0) || (year % 400 == 0)))
			{
				if((day < 01) || (day > 29)){
					ret = false;
				}
			}
			else
			{
				if( (day < 01) || (day > 28) ){
					ret = false;
				}
			}
		}
	}
	else
	{
		ret = false;
	}

	return ret;
}