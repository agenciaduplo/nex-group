<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restricoes extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}


	function index()
	{
		$this->auth->check();
		redirect(base_url() . 'restricoes/task');
	}


	function task($idprojeto, $idprojeto_tarefas = "")
	{
		$this->auth->check();

		$this->load->model('projetos_model','projeto');
		
		$idprojeto = (int) $idprojeto;
		$idprojeto_tarefas = (int) $idprojeto_tarefas;
		
		if(intval($idprojeto) == 0)
		{
			show_404();
			exit;
		}


		$user_filters = $this->projeto->getUserFilters($idprojeto, "R");

		$data = array();
		$data["user_filters"]["setores"] = array();
		$data["user_filters"]["marcos"]   = array();
		$data["user_filters"]["tarefas"] = array();

		if($user_filters)
		{
			foreach($user_filters as $filter)
			{
				if($filter->tipo == "S")     $keyname = "setores";
				elseif($filter->tipo == "M") $keyname = "marcos";
				elseif($filter->tipo == "T") $keyname = "tarefas";

				$data["user_filters"][$keyname] = explode("//", $filter->filtro);
			}
		}


		include("function/ordena_restricoes.php");

		$restricoes = $this->projeto->getCardsRestricoes($idprojeto, $idprojeto_tarefas, "", "", implode(",", $data["user_filters"]["marcos"]), implode(",", $data["user_filters"]["tarefas"]));

		usort($restricoes, "ordenaRestricoes");


		$marco = "";

		if($idprojeto_tarefas != "")
		{
			$marco = $this->projeto->getMarcoByTask($idprojeto_tarefas);
			$marco = $marco[0];
		}

		$id = ($idprojeto > 0) ? $idprojeto : "";

		$marcs = $this->projeto->getMarcos($id, true);
		$tot_marcs = count($marcs);

		$marcos = array();

		for($i = 0; $i < $tot_marcs; ++$i)
		{
			$marcos[$marcs[$i]->idprojeto_tarefas] = $marcs[$i]->nome;
		}


		$data['marco'] 			   = $marco;
		$data['marcos'] 		   = $marcos;
		$data['project'] 		   = $this->projeto->getProjeto($idprojeto);
		$data['projetos'] 		   = $this->projeto->getProjetosListByUser($this->encrypt->decode($_SESSION['project']['idusuario']));
		$data['f_atividades'] 	   = $this->projeto->getAllForProject($idprojeto);
		$data['restricoes'] 	   = $restricoes;
		$data['idprojeto_tarefas'] = $idprojeto_tarefas;
		$data['todos_projetos']    = ($id != "") ? 0 : 1;

		$data['title']	  		   = ($idprojeto > 0)  ? 'Restrições - ' . $data['project'][0]->nome . ' - NexView' : 'Restrições - NexView';
		$data['pag'] 			   = 'restricoes';
		$data['setores'] 		   = array();


		$filter_marco = array();
		$filter_tarefas = array();

		foreach($this->projeto->getCardsRestricoes($idprojeto, $idprojeto_tarefas) as $row)
		{
			$tarefa = $this->projeto->getTarefaByID($row->idtarefa);

			if($tarefa->marco == 1)
			{
				// filter::Marcos
				if( ! array_key_exists($tarefa->nome, $filter_marco))
					$filter_marco[$tarefa->nome][] = $tarefa->idprojeto_tarefas;
			}
			else
			{
				// filter::Marcos
				if( ! array_key_exists($tarefa->nome_marco, $filter_marco))
					$filter_marco[$tarefa->nome_marco][] = $tarefa->idmother_tarefa;

				// filter::Tarefas
				if( ! array_key_exists($tarefa->nome, $filter_tarefas))
				{
					$filter_tarefas[$tarefa->nome_marco][$tarefa->nome]['id'][]  = $tarefa->idprojeto_tarefas;
					$filter_tarefas[$tarefa->nome_marco][$tarefa->nome]['setor'] = $tarefa->setores;
				}

				// filter::Setores
				if( ! in_array($tarefa->setores, $data['setores']))
					$data['setores'][] = $tarefa->setores;
			}
		}

		$data['filter_marco'] 	= $filter_marco;
		$data['filter_tarefas'] = $filter_tarefas;

		sort($data['setores']);


		$this->load->view('restricoes', $data);
	}

}

/* End of file restricoes.php */
/* Location: ./application/controllers/restricoes.php */
