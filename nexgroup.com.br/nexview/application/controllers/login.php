<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{

	function index()
	{
		$data['title'] = 'Login - NexView';
    	$data['pag'] = 'login';

		$this->load->view('login', $data);
	}

	function entrar()
	{
		$username = $this->input->post('nv_email');
		$password = $this->input->post('nv_senha');

		if($this->auth->try_login($username, $password))
		{
			$idusuario = $this->encrypt->decode($_SESSION['project']['idusuario']);

			redirect('home');
		}
		else
		{
		  redirect('login');
		}
	}

  	function sair()
	{
    	$this->auth->logout();
    	redirect('login/');
	}
}
/* End of file login.php */
/* Location: ./application/controllers/login.php */
