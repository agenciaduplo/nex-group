<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kanban extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function index()
	{
		$this->auth->check();
		redirect(base_url() . 'kanban/task');
	}

	function task($idprojeto = 0, $idprojeto_tarefas = "")
	{
		$this->auth->check();

		$this->load->model('projetos_model','projeto');

		$idprojeto = (int) $idprojeto;

		
		$marco = "";

		if($idprojeto_tarefas != "")
		{
			$marco = $this->projeto->getMarcoByTask($idprojeto_tarefas);
			$marco = $marco[0];
		}

		$id = ($idprojeto > 0) ? $idprojeto : "";

		$marcs = $this->projeto->getMarcos($id, true);
		$tot_marcs = count($marcs);

		$marcos = array();

		for($i = 0; $i < $tot_marcs; ++$i)
		{
			$marcos[$marcs[$i]->idprojeto_tarefas] = $marcs[$i]->nome;
		}

		$data = array();
		$data['marco'] 				= $marco;
		$data['marcos'] 			= $marcos;
		$data['project'] 			= ($idprojeto > 0) ? $this->projeto->getProjeto($idprojeto) : $this->projeto->getAllProjetos();
		$data['projetos'] 			= $this->projeto->getProjetosList();

		$data['f_atividades'] 		= $this->projeto->getAllForProject($idprojeto);
		$data['nex_ativis'] 		= $this->projeto->getProxTasks($idprojeto);
		$data['idprojeto_tarefas'] 	= $idprojeto_tarefas;
		$data['todos_projetos'] 	= ($id != "") ? 0 : 1;

		$data['title']	  			= ($idprojeto > 0)  ? 'Tasks - ' . $data['project'][0]->nome . ' - NexView' : 'Tasks - Todos Projetos - NexView';
		$data['pag'] 				= 'kanban';
		$data['setores']			= array();


		// filter::Marcos
		$filter_marco = array();

		foreach ($this->projeto->getAllMarcosRepety($idprojeto, $idprojeto_tarefas) as $row)
		{
			$filter_marco[$row->nome][] = $row->idprojeto_tarefas;
		}


		// filter::Tarefas
		$filter_tarefas = array();

		foreach ($filter_marco as $key => $marco)
		{
			foreach($this->projeto->getAllTarefasRepety(implode(",", $marco)) as $tarefa)
			{
				$filter_tarefas[$key][$tarefa->nome]['id'][] = $tarefa->idprojeto_tarefas;
				$filter_tarefas[$key][$tarefa->nome]['setor'] = $tarefa->setores;

				if( ! in_array($tarefa->setores, $data['setores']))
					$data['setores'][] = $tarefa->setores;
			}
		}


		$data['filter_marco'] 	= $filter_marco;
		$data['filter_tarefas'] = $filter_tarefas;

		sort($data['setores']);


		$user_filters = $this->projeto->getUserFilters($idprojeto, "K");
		
		$data["user_filters"]["setores"] = array();
		$data["user_filters"]["marcos"]   = array();
		$data["user_filters"]["tarefas"] = array();

		if($user_filters)
		{
			foreach($user_filters as $filter)
			{
				if($filter->tipo == "S")     $keyname = "setores";
				elseif($filter->tipo == "M") $keyname = "marcos";
				elseif($filter->tipo == "T") $keyname = "tarefas";

				$data["user_filters"][$keyname] = explode("//", $filter->filtro);
			}
		}


		$task = $this->projeto->getCardsKanban($idprojeto, $idprojeto_tarefas, "", "", implode(",", $data["user_filters"]["marcos"]), implode(",", $data["user_filters"]["tarefas"]));

		$tot_st = count($task);

		for($i = 0; $i < $tot_st; ++$i)
		{
			if($task[$i]->inicio_real == "" && strtotime($task[$i]->data_inicio) <= strtotime(date('Y-m-d'))) { $data['task_atrasado'][]  = $task[$i]; }
			elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real == "") { $data['task_andamento'][] = $task[$i]; }
			elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real != "") { $data['task_concluido'][] = $task[$i]; }

			$restricao = $this->projeto->getIdRestricao($task[$i]->idprojeto_tarefas);
			$restricao = ($restricao) ? $restricao[0]->idtarefa_restricao : "";

			$data['restricao'][$task[$i]->idprojeto_tarefas] = $restricao;
		}


		$this->load->view('kanban', $data);
	}

}

/* End of file kanban.php */
/* Location: ./application/controllers/kanban.php */
