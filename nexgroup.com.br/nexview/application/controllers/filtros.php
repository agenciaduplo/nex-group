<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtros extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function index()
	{
		@session_start();
	}

	function filtro()
	{
		@session_start();

		$this->load->model('projetos_model','projeto');

		if($this->input->post('marco') == "all" || $this->input->post('sel_ativs') || $this->input->post("marco") == 0)
		{
			$pag = $this->input->post('pag');

			$idprojeto_tarefas = $this->input->post('idprojeto_tarefas');

			$marco = $this->input->post('marco');

			$idprojeto = $this->input->post('idprojeto');

			/*
			|--------------------------------------------------------------------------
			| Filtro Setores (Responsável da tarefa)
			|--------------------------------------------------------------------------
			*/

			$sel_setors = $this->input->post('sel_setors');
			$sel_setors = ($sel_setors != "") ? implode(",", $sel_setors) : "";
			$sel_setors = ($sel_setors != "") ? str_replace(",", "','", $sel_setors) : "";

			/*
			|--------------------------------------------------------------------------
			| Filtro por Ativadades (Responsável da tarefa)
			|--------------------------------------------------------------------------
			*/

			$sel_ativs = $this->input->post('sel_ativs');
			$sel_ativs = ($sel_ativs != "") ? implode(",", $sel_ativs) : "";

			/*
			|--------------------------------------------------------------------------
			| Filtro por data
			|--------------------------------------------------------------------------
			*/

			$data_inicio = $this->input->post("data_inicio");
			$data_fim    = $this->input->post("data_fim");

			if($data_inicio != "")
			{
				$data_inicio = str_replace("/", "-", $data_inicio);
				$data_inicio = date("Y-m-d", strtotime($data_inicio));
			}

			if($data_fim != "")
			{
				$data_fim = str_replace("/", "-", $data_fim);
				$data_fim = date("Y-m-d", strtotime($data_fim));
			}


			if($marco != "all")
			{
				$task = $this->projeto->getTasksFiltro($idprojeto, $sel_ativs, $data_inicio, $data_fim, $sel_setors);
			}
			else
			{
				$task = $this->projeto->getAllForProject($idprojeto, true, $data_inicio, $data_fim);
			}


			$marco = "";

			if($idprojeto_tarefas != "")
			{
				$marco = $this->projeto->getMarcoByTask($idprojeto_tarefas);
				$marco = $marco[0];
			}


			$marcs = $this->projeto->getMarcos($idprojeto, true);

			$tot_marcs = count($marcs);

			$marcos = array();

			for($i = 0; $i < $tot_marcs; ++$i)
			{
				$marcos[$marcs[$i]->idprojeto_tarefas] = $marcs[$i]->nome;
			}

			$data = array();
			$data['marco'] 		= $marco;
			$data['marcos'] 	= $marcos;
			$data['nex_ativis'] = $this->projeto->getProxTasks($idprojeto, $sel_ativs);
			$data['pag'] 		= 'filtros';

			$tot_st = count($task);

			if($pag == 'kanban')
			{
				for($i = 0; $i < $tot_st; ++$i)
				{
					if($task[$i]->inicio_real == "" && strtotime($task[$i]->data_inicio) < strtotime(date('Y-m-d'))) { $data['task_atrasado'][]  = $task[$i]; }
					elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real == "") { $data['task_andamento'][] = $task[$i]; }
					elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real != "") { $data['task_concluido'][] = $task[$i]; }

					$restricao = $this->projeto->getIdRestricao($task[$i]->idprojeto_tarefas);
					$restricao = ($restricao) ? $restricao[0]->idtarefa_restricao : "";

					$data['restricao'][$task[$i]->idprojeto_tarefas] = $restricao;
				}

				$this->load->view('ajax.kanban.tasks.php', $data);
			}
			else
			{
				include("function/ordena_restricoes.php");

				for($i = 0; $i < $tot_st; ++$i)
				{
					$restricao = $this->projeto->getIdRestricao($task[$i]->idprojeto_tarefas, true);
					$tot_ret   = count($restricao);

					for($j = 0; $j < $tot_ret; ++$j)
					{
						$restri[] = $restricao[$j];
					}
				}

				usort($restri, "ordenaRestricoes");

				$data = array(
					'pag'               => 'restricoes',
					"project"           => $this->projeto->getProjeto($idprojeto),
					'projetos'          => $this->projeto->getProjetosList(),

					//filtros:
					"setores"           => $this->projeto->getSetores($idprojeto),
					"f_atividades"      => $this->projeto->getAllForProject($idprojeto),
					"marco"             => $marco,
					"idprojeto_tarefas" => $idprojeto_tarefas,

					'restricoes'        => $restri
				);

				$this->load->view('ajax.restricoes.php', $data);
			}
		}

	} //filtro();


	function filtraCardsKanbanXHR()
	{
		$this->load->model('projetos_model','projeto');

		$idprojeto = (int) $this->input->post('idprojeto');
		$idprojeto_tarefas = (int) $this->input->post('idprojeto_tarefas');

		/*
		|--------------------------------------------------------------------------
		| Filtro Setores
		|--------------------------------------------------------------------------
		*/
		$setores_filter = $this->input->post('setores');
		$setores_filter = ($setores_filter != "") ? implode("//", $setores_filter) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Marcos
		|--------------------------------------------------------------------------
		*/
		$marco = $this->input->post('arr_marco_id');
		
		$marco_filter = ($marco != "") ? implode("//", $marco) : "";
		$marco = ($marco != "") ? implode(",", $marco) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Tarefas
		|--------------------------------------------------------------------------
		*/
		$tarefas = $this->input->post('arr_tarefa_id');

		$tarefas_filter = ($tarefas != "") ? implode("//", $tarefas) : "";
		$tarefas = ($tarefas != "") ? implode(",", $tarefas) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Datas
		|--------------------------------------------------------------------------
		*/
		$data_inicial = $this->input->post('data_inicio');
		if($data_inicial != "") { $data_inicial = date("Y-m-d", strtotime(str_replace("/", "-", $data_inicial))); }

		$data_final = $this->input->post('data_final');
		if($data_final != "") { $data_final = date("Y-m-d", strtotime(str_replace("/", "-", $data_final))); }


		/*
		|--------------------------------------------------------------------------
		| Filtros do Usuário
		|
		| Insere ou atualiza a tabela de filtros do usuário
		|--------------------------------------------------------------------------
		*/
		$this->projeto->UpdateUserFilters($idprojeto, "K", $setores_filter, $marco_filter, $tarefas_filter);



		$task = $this->projeto->getCardsKanban($idprojeto, $idprojeto_tarefas, $data_inicial, $data_final, $marco, $tarefas);


		$marco = "";

		if($idprojeto_tarefas != "")
		{
			$marco = $this->projeto->getMarcoByTask($idprojeto_tarefas);
			$marco = $marco[0];
		}

		$id = ($idprojeto > 0) ? $idprojeto : "";

		$marcs = $this->projeto->getMarcos($id, true);
		$tot_marcs = count($marcs);
		$marcos = array();

		for($i = 0; $i < $tot_marcs; ++$i)
		{
			$marcos[$marcs[$i]->idprojeto_tarefas] = $marcs[$i]->nome;
		}

		$data = array();
		$data['marco'] 		    = $marco;
		$data['marcos'] 	    = $marcos;
		$data['nex_ativis']     = $this->projeto->getProxTasks($id, '');
		$data['todos_projetos'] = ($id != "") ? 0 : 1;
		$data['pag'] 		    = 'filtros';

		$tot_st = count($task);

		for($i = 0; $i < $tot_st; ++$i)
		{
			if($task[$i]->inicio_real == "" && strtotime($task[$i]->data_inicio) < strtotime(date('Y-m-d'))) { $data['task_atrasado'][]  = $task[$i]; }
			elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real == "") { $data['task_andamento'][] = $task[$i]; }
			elseif($task[$i]->inicio_real != "" && $task[$i]->fim_real != "") { $data['task_concluido'][] = $task[$i]; }

			$restricao = $this->projeto->getIdRestricao($task[$i]->idprojeto_tarefas);
			$restricao = ($restricao) ? $restricao[0]->idtarefa_restricao : "";

			$data['restricao'][$task[$i]->idprojeto_tarefas] = $restricao;
		}

		$this->load->view('ajax.kanban.tasks.php', $data);
	}


	function filtraRestricoesXHR()
	{
		$this->load->model('projetos_model','projeto');

		$idprojeto = (int) $this->input->post('idprojeto');
		$idprojeto_tarefas = (int) $this->input->post('idprojeto_tarefas');

		/*
		|--------------------------------------------------------------------------
		| Filtro Setores
		|--------------------------------------------------------------------------
		*/
		$setores_filter = $this->input->post('setores');
		$setores_filter = ($setores_filter != "") ? implode("//", $setores_filter) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Marcos
		|--------------------------------------------------------------------------
		*/
		$marco = $this->input->post('arr_marco_id');

		$marco_filter = ($marco != "") ? implode("//", $marco) : "";
		$marco = ($marco != "") ? implode(",", $marco) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Tarefas
		|--------------------------------------------------------------------------
		*/
		$tarefas = $this->input->post('arr_tarefa_id');

		$tarefas_filter = ($tarefas != "") ? implode("//", $tarefas) : "";
		$tarefas = ($tarefas != "") ? implode(",", $tarefas) : "";

		/*
		|--------------------------------------------------------------------------
		| Filtro Datas
		|--------------------------------------------------------------------------
		*/
		$data_inicial = $this->input->post('data_inicio');
		if($data_inicial != "") { $data_inicial = date("Y-m-d", strtotime(str_replace("/", "-", $data_inicial))); }

		$data_final = $this->input->post('data_final');
		if($data_final != "") { $data_final = date("Y-m-d", strtotime(str_replace("/", "-", $data_final))); }


		/*
		|--------------------------------------------------------------------------
		| Filtros do Usuário
		|
		| Insere ou atualiza a tabela de filtros do usuário
		|--------------------------------------------------------------------------
		*/
		$this->projeto->UpdateUserFilters($idprojeto, "R", $setores_filter, $marco_filter, $tarefas_filter);



		include("function/ordena_restricoes.php");

		$restricoes = $this->projeto->getCardsRestricoes($idprojeto, $idprojeto_tarefas, $data_inicial, $data_final, $marco, $tarefas);

		usort($restricoes, "ordenaRestricoes");

		$marco = "";

		if($idprojeto_tarefas != "")
		{
			$marco = $this->projeto->getMarcoByTask($idprojeto_tarefas);
			$marco = $marco[0];
		}

		$id = ($idprojeto > 0) ? $idprojeto : "";

		$marcs = $this->projeto->getMarcos($id, true);
		$tot_marcs = count($marcs);
		$marcos = array();

		for($i = 0; $i < $tot_marcs; ++$i)
		{
			$marcos[$marcs[$i]->idprojeto_tarefas] = $marcs[$i]->nome;
		}

		$data = array();
		$data['marco'] 		= $marco;
		$data['marcos'] 	= $marcos;
		$data['pag'] 		= 'filtros';

		$data['project'] 	= $this->projeto->getProjeto($idprojeto);
		$data['restricoes'] = $restricoes;

		$this->load->view('ajax.restricoes.php', $data);
	}

}
/* End of file filtros.php */
/* Location: ./application/controllers/filtros.php */
