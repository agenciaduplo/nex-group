<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function original()
	{
		$this->load->view('orig_home');
	}

	function index()
	{
		$this->auth->check();

		$this->load->model('projetos_model','projeto');

		$data = array();
		// $data['marc_tit'] = $this->projeto->getMarcos("", true, true); // old array
		$data['sub_task'] = $this->projeto->getIncompletSubTarefas(true);
		$data['title']	  = 'Projetos - NexView';
		$data['pag'] 	  = 'home';


		// MARCOS GERAL
		$data['marcos_geral'] = array();
		$i = 0;
		foreach ($this->projeto->getMarcos("", true, true) as $row)
		{
			switch($row->nome) {
				case 'Marco: Pré-viabilidade'                 : $i = 0; break;
				case 'Marco: Terreno Adquirido'               : $i = 1; break;
				case 'Marco: Urbanístico Aprovado'            : $i = 2; break;
				case 'Marco: Arquitetura Aprovado'            : $i = 3; break;
				case 'Marco: Elétrico Aprovado'               : $i = 4; break;
				case 'Marco: Hidro Aprovado'                  : $i = 5; break;
				case 'Marco: Pluvial Aprovado'                : $i = 6; break;
				case 'Marco: PPCI Aprovado'                   : $i = 7; break;
				case 'Marco: LI Emitida'                      : $i = 8; break;
				case 'Marco: Registro de Incorporação Emitido': $i = 9; break;
				case 'Marco: Lançamento'                      : $i = 10; break;
				case 'Marco: Inicio de Obra'                  : $i = 11; break;
			}

			$data['marcos_geral'][$i]['nome'] = $row->nome;
		}

		// Ordena o array pela ordem da key definida acima
		ksort($data['marcos_geral']);

		// PROJETOS
		$i = 0;
		foreach ($this->projeto->getAllProjetosOnly() as $row)
		{
			$data['projetos'][$i]['idprojeto'] 		 = $row->idprojeto;
			$data['projetos'][$i]['nome'] 			 = $row->nome;
			$data['projetos'][$i]['progresso'] 		 = $row->progresso;
			$data['projetos'][$i]['data_atualizado'] = $row->data_atualizado;
			$data['projetos'][$i]['data_status'] 	 = $row->data_status;

			$i++;
		}


		// SUB-PROJETOS
		$data['subprojetos'] = array();
		$i = 0;
		foreach($this->projeto->getAllSubProjetos() as $row)
		{
			$pi = $row->idprojeto_mother;

			$data['subprojetos'][$pi][$i]['idprojeto']		 = $row->idprojeto;
			$data['subprojetos'][$pi][$i]['nome'] 			 = $row->nome;
			$data['subprojetos'][$pi][$i]['progresso'] 		 = $row->progresso;
			$data['subprojetos'][$pi][$i]['data_atualizado'] = $row->data_atualizado;
			$data['subprojetos'][$pi][$i]['data_status'] 	 = $row->data_status;

			$i++;
		}


		// MARCOS DOS PROJETOS
		$marcos = $this->projeto->getMarcos("", false, false, array('idprojeto_tarefas', 'idprojeto', 'nome', 'inicio_real', 'inicio_meta', 'progresso'));
		$mcount = count($marcos);

		for($i = 0; $i < $mcount; ++$i)
		{
			$idprojeto = $marcos[$i]->idprojeto;
			$nome      = $marcos[$i]->nome;

			foreach($data['marcos_geral'] as $k => $v) if($nome == $v['nome']) $data['marcos'][$idprojeto][$k] = $marcos[$i];

			ksort($data['marcos'][$idprojeto]);
		}

		// var_dump($data['marcos'][16]);
		// exit;

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		$data['ua'] = '';
		if(preg_match('/Firefox/i', $user_agent))
			$data['ua'] = 'firefox';


		$this->load->view('home', $data);
	}


	function gravaOrdemProjetos()
	{
		@session_start();

		$this->load->model('projetos_model','projeto');
		$this->projeto->gravaOrdemProjetos($this->input->post("idprojetos"));
	}





	// FUNÇÃO PARA ATUALIZAR LISTA DE USUÁRIOS RELACIONADOS COM OS PROJETOS

	function users_update()
	{
		echo "<pre>";
		
		$arr_projetos = $this->db->query("SELECT idprojeto FROM projetos")->result();
		$arr_users    = $this->db->query("SELECT idusuario FROM usuarios")->result();

		foreach($arr_users as $user)
		{
			foreach($arr_projetos as $proj)
			{
				$sql = "SELECT idusuario_projeto FROM usuario_projeto WHERE idusuario = " . $user->idusuario . " && idprojeto = " . $proj->idprojeto;

				// Verifica se o usuário já está relacionado ao projeto, se NÃO estiver add o usuário
				if(count($this->db->query($sql)->result()) == 0)
				{
					$query = $this->db->query("SELECT MAX(ordem) AS ordem FROM usuario_projeto WHERE idusuario = " . $user->idusuario);

					$ordem = 1;
					if($query->num_rows() > 0)
					{
						$row = $query->row(); 
						$ordem = (int) $row->ordem + 1;
					}

					$sql = "INSERT INTO usuario_projeto (idusuario, idprojeto, ordem) VALUES (" . $user->idusuario . "," . $proj->idprojeto . "," . $ordem .")";
					
					$this->db->query($sql);

					var_dump($sql); 
				}
			}
			echo "<br/><br/>";
		}

		echo "</pre>";
	}
	// END users_update();

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
