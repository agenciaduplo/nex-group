<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function original()
	{
		$this->load->view('orig_home');
	}

	function index()
	{
		$this->auth->check();

		$this->load->model('projetos_model','projeto');

		$data = array();
		$data['marc_tit'] = $this->projeto->getMarcos("", false, true);
		$data['sub_task'] = $this->projeto->getIncompletSubTarefas(true);
		$data['title']	  = 'Projetos - NexView';
		$data['pag'] 	  = 'home';

		$i = 0;
		foreach ($this->projeto->getAllProjetosOnly() as $row)
		{
			$data['projetos'][$i]['idprojeto'] 		  = $row->idprojeto;
			$data['projetos'][$i]['nome'] 			  = $row->nome;
			$data['projetos'][$i]['progresso'] 		  = $row->progresso;
			$data['projetos'][$i]['data_atualizado']  = $row->data_atualizado;
			$data['projetos'][$i]['data_status'] 	  = $row->data_status;

			$i++;
		}

		$data['subprojetos'] = array();
		$i = 0;
		foreach($this->projeto->getAllSubProjetos() as $row)
		{
			$pi = $row->idprojeto_mother;

			$data['subprojetos'][$pi][$i]['idprojeto']		  = $row->idprojeto;
			$data['subprojetos'][$pi][$i]['nome'] 			  = $row->nome;
			$data['subprojetos'][$pi][$i]['progresso'] 		  = $row->progresso;
			$data['subprojetos'][$pi][$i]['data_atualizado']  = $row->data_atualizado;
			$data['subprojetos'][$pi][$i]['data_status'] 	  = $row->data_status;

			$i++;
		}

		$marcos     = $this->projeto->getMarcos("");
		$tot_marcs  = count($marcos);

		for($i = 0; $i < $tot_marcs; ++$i)
		{
			$idprojeto = $marcos[$i]->idprojeto;
			$data['marcos'][$idprojeto][] = $marcos[$i];
		}

		$this->load->view('home', $data);
	}


	function gravaOrdemProjetos()
	{
		@session_start();

		$this->load->model('projetos_model','projeto');
		$this->projeto->gravaOrdemProjetos($this->input->post("idprojetos"));
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
