<ul class="plane_2 box-planes" >
	<h2>Atrasado</h2>

	<?php
	if(isset($task_atrasado) && count($task_atrasado) > 0)
	{
		foreach($task_atrasado as $atrasado)
		{
			$class = ($restricao[$atrasado->idprojeto_tarefas] != "") ? "restricao-ativo" : "";
		?>
		<div class="box <?=$class?>"><!-- BOX -->
			<div class="top">
				<h3><?=$atrasado->nome?></h3>
				<h4><?=str_replace("Marco: ", "",$marcos[$atrasado->idmother_tarefa])?></h4>
			</div>
			<div class="content">
				<p class="full"><b>Setor(es) Responsável:</b></p>
				<p><span><?=($atrasado->setores != "") ? $atrasado->setores : 'Não Definido'?></span></p>
				<p class="blast full">
					<b>Início:</b> <?=$this->utilidades->data_extenso($atrasado->data_inicio)?><br>
					<b>Término:</b> <?=$this->utilidades->data_extenso($atrasado->data_fim)?><br><br>

					<b>Início (Meta):</b> <?=$this->utilidades->data_extenso($atrasado->inicio_meta)?><br>
					<b>Término (Meta):</b> <?=$this->utilidades->data_extenso($atrasado->fim_meta)?><br><br>

					<b>Início (Real):</b> <?=$this->utilidades->data_extenso($atrasado->inicio_real)?><br>
					<b>Término (Real):</b> <?=$this->utilidades->data_extenso($atrasado->fim_real)?>
				</p>

				<?php if($todos_projetos == 1) : ?>
				<div class="sayname">
					<p>Tarefa do projeto </p>
					<a href="#">
						<?=$atrasado->projeto?></a>
					</a>
				</div>
				<?php endif; ?>

				<?php if($class != "") { ?>
				<a href="<?=base_url()?>restricoes/task/<?=$atrasado->idprojeto?>/<?=$atrasado->idprojeto_tarefas?>">
					<i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
				</a>
				<?php } ?>
			</div>
		</div>
		<?php
		}
	}
	?>
</ul>

<ul class="plane_3 box-planes" >
	<h2>Em andamento</h2>

	<?php
	if(isset($task_andamento) && count($task_andamento) > 0)
	{
		foreach($task_andamento as $andamento)
		{
			$class = ($restricao[$andamento->idprojeto_tarefas] != "") ? "restricao-ativo" : "";
		?>
		<div class="box <?=$class?>"><!-- BOX -->
			<div class="top">
				<h3><?=$andamento->nome?></h3>
				<h4><?=str_replace("Marco: ", "", @$marcos[$andamento->idmother_tarefa])?></h4>
			</div>
			<div class="content">
				<p class="full"><b>Setor(es) Responsável:</b></p>
				<p><span><?=$andamento->setores?></span></p>
				<p class="blast full">
					<b>Início:</b> <?=$this->utilidades->data_extenso($andamento->data_inicio)?><br>
					<b>Término:</b> <?=$this->utilidades->data_extenso($andamento->data_fim)?><br><br>

					<b>Início (Meta):</b> <?=$this->utilidades->data_extenso($andamento->inicio_meta)?><br>
					<b>Término (Meta):</b> <?=$this->utilidades->data_extenso($andamento->fim_meta)?><br><br>

					<b>Início (Real):</b> <?=$this->utilidades->data_extenso($andamento->inicio_real)?><br>
					<b>Término (Real):</b> <?=$this->utilidades->data_extenso($andamento->fim_real)?>
				</p>

				<?php if($todos_projetos == 1) : ?>
				<div class="sayname">
					<p>Tarefa do projeto </p>
					<a href="#">
						<?=$andamento->projeto?></a>
					</a>
				</div>
				<?php endif; ?>


				<?php if($class != "") { ?>
				<a href="<?=base_url()?>restricoes/task/<?=$andamento->idprojeto?>/<?=$andamento->idprojeto_tarefas?>">
					<i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
				</a>
				<?php } ?>
			</div>
		</div>
		<?php
		}
	}
	?>
</ul>


<ul class="plane_4 box-planes" >
	<h2>Concluído</h2>

	<?php
	if(isset($task_concluido) && count($task_concluido) > 0)
	{
		foreach($task_concluido as $concluido)
		{
			$class = ($restricao[$concluido->idprojeto_tarefas] != "") ? "restricao-ativo" : "";

		?>
		<div class="box <?=$class?>"><!-- BOX -->
			<div class="top">
				<h3><?=$concluido->nome?></h3>
				<h4><?=str_replace("Marco: ", "",$marcos[$concluido->idmother_tarefa])?></h4>
			</div>
			<div class="content">
				<p class="full"><b>Setor(es) Responsável:</b></p>
				<p><span><?=$concluido->setores?></span></p>
				<p class="blast full">
					<b>Início:</b> <?=$this->utilidades->data_extenso($concluido->data_inicio)?><br>
					<b>Término:</b> <?=$this->utilidades->data_extenso($concluido->data_fim)?><br><br>

					<b>Início (Meta):</b> <?=$this->utilidades->data_extenso($concluido->inicio_meta)?><br>
					<b>Término (Meta):</b> <?=$this->utilidades->data_extenso($concluido->fim_meta)?><br><br>

					<b>Início (Real):</b> <?=$this->utilidades->data_extenso($concluido->inicio_real)?><br>
					<b>Término (Real):</b> <?=$this->utilidades->data_extenso($concluido->fim_real)?>
				</p>

				<?php if($todos_projetos == 1) : ?>
				<div class="sayname">
					<p>Tarefa do projeto </p>
					<a href="#">
							<?=$concluido->projeto?></a>
					</a>
				</div>
				<?php endif; ?>

				<?php if($class != "") { ?>
				<a href="<?=base_url()?>restricoes/task/<?=$concluido->idprojeto?>/<?=$concluido->idprojeto_tarefas?>">
					<i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
				</a>
				<?php } ?>
			</div>
		</div>
		<?php
		}
	}
	?>
</ul>

<ul class="plane_5" >
	<h3>Próximos</h3>
	<?php

	$data = "";

	if(isset($nex_ativis) && count($nex_ativis) > 0)
	{
		foreach($nex_ativis as $nex)
		{
			if($data != $nex->data_inicio) { ?>
			<h5><?=date("d/m/Y", strtotime($nex->data_inicio))?></h5>
			<?php } ?>
			<div class="box">
				<p><?=$nex->nome?></p>
				<?php
				$label = "";
				if(isset($marcos[$nex->idmother_tarefa]))
				{
					$label = str_replace("Marco:", "", $marcos[$nex->idmother_tarefa]);
				}
				?>
				<span><?=$label?></span>
				<?php if($todos_projetos == 1) : ?>
				<span>(<?=$nex->projeto?>)</span>
				<?php endif; ?>
			</div>
		<?php
		$data = $nex->data_inicio;
		}
	}
	else
	{
	?>
	<h5>Sem Atividades Futuras</h5>
	<?php } ?>
</ul>
