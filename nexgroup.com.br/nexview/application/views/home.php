<?php include("includes/header.php"); ?>
<body>  
	
	<?php include("includes/header_fixo.php"); ?>
	
	<section id="geral">

		<div class="row">

			<div id="fotos" class="list_carousel clickroll" style="overflow: hidden;">

				<div style="padding: 25px 20px 30px;">
					<div style="float: right; font-size: 1.4em; color: #d3d3d3; font-weight: 800; margin-left: 8px; line-height: 26px;">
						Modo Apresentação
					</div>
					<div class="onoffswitch">
						<input type="checkbox" name="modo_apresentacao" class="onoffswitch-checkbox" id="input-modo-apresentacao-onoff">
						<label class="onoffswitch-label" for="input-modo-apresentacao-onoff">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>


				<table id="table-marcos" style="width:100%; margin-top: 30px;">
					<tbody>
						<tr>
							<td class="doble dobles"></td>
							<?php
							// Gera as colunas com os nome dos marcos

							$marcos_cols_cont = count($marcos_geral); // Total de marcos (colunas)

							foreach($marcos_geral as $key => $value)
							{
								$marco_nome = str_replace("Marco: ", "", $value['nome']);

								if($marco_nome == "Registro de Incorporação Emitido") { $marco_nome = "Registro de Inc. Emitido"; }
								
								if($marco_nome == "PPCI Aprovado") { $marco_nome = "PPCI<br />Aprovado"; }

								echo '<td id="marco-coll-'.$key.'" class="dobles liv">'.$marco_nome.'</td>';
							}
							?>
							<td class="dobles" width="7%"></td>
						</tr>
					</tbody>
				</table>
				<?php
				// ######################  VARIÁVEIS DE CONFIGURAÇÃO ##########################

				// Contador das linhas dos projetos <tr>
				$projeto_i = 0; 

				// Total de projetos
				$projetos_total = count($projetos); 

				// Total de projetos por página de exibição (não é contabilizado sub-projetos)
				$projetos_por_page = 6;

				// Total de página de exibição dos projetos;
				// Cada página é um bloco <li> 1º nível da <ul id="foo4">;
				$pages_total = ceil($projetos_total / $projetos_por_page); 

				// ###########################################################################

				?>
				<ul id="foo4" style="margin-top: 20px;">
					
					<?php for($i = 0; $i < $pages_total; $i++) : // FOR_PAGES: ?>
					
					<li>
						<table class="table-dnd" style="width:100%">
							
								<?php for($p = $projeto_i; $p < $projetos_total; $p++) : // FOR_PROJETOS: ?>

								<tr id="<?=$projetos[$p]['idprojeto']?>">
									<td class="noborder">
										<table>
											<tr>
												<td class="doble title dragHandle">
													<?php
													// Nome do projeto <h2>
													$projeto_nome = explode('-', $projetos[$p]['nome']);
													$projeto_nome = (count($projeto_nome) > 1) ? $projeto_nome[0].'<span>'.$projeto_nome[1].'<span>' : '<span>'.implode(' ', $projeto_nome).'<span>';
													?>
													<h2><?=$projeto_nome?></h2>
													<p>Data de status: <?=date("d/m/Y", strtotime($projetos[$p]['data_status']))?></p>
												</td>
												<?php 

												$cards_i = 0; // Contador dos cards <td> do projeto 

												foreach($marcos[$projetos[$p]['idprojeto']] as $key => $card)
												{
													$cor = "";

													// Se o index do card não for o mesmo da coluna do marco repectivo, mostra um card <td> vazio
													if($key != $cards_i)
													{
														?>
														<td class="bloc">
															<a href="<?=site_url()?>kanban/task/<?=$projetos[$p]['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
														</td>
														<?php
														$cards_i++;
													}

													// Define a cor do card pelas condições do seu progresso
													if(intval($card->progresso) >= 100) { $cor = "green"; }
													elseif($card->inicio_real != null && $card->inicio_real != "") 	{ $cor = "yelow"; }
													elseif(($card->inicio_meta < $projetos[$p]['data_status']) && 
														  ($card->inicio_real == null || $card->inicio_real == ""))	{ $cor = "red"; }

													// Ícone de alerta
													$icon_warning_show = false;
													if(array_key_exists($card->idprojeto_tarefas, $sub_task)) // Array $sub_task -> tarefas incompletas
													{ 
														if($sub_task[$card->idprojeto_tarefas] == 1) { $icon_warning_show = true; }
													}
													?>
													<td class="bloc <?=$cor?>" title="<?=$card->nome?>" style="height: 100px !important;">
														<a href="<?=site_url()?>kanban/task/<?=$projetos[$p]['idprojeto'].'/'.$card->idprojeto_tarefas?>" style="display: block; width: 100%; height: 100%; position: absolute;">
															<?=($icon_warning_show) ? '<i class="icon icon-warning-sign"></i>' : null?>
														</a>
													</td>
													<?php
													$cards_i++;
												}

												// Adicona cards <td> que faltam para preecher a linha do projeto <tr>
												if($cards_i < $marcos_cols_cont) 
												{
													while ($cards_i < $marcos_cols_cont)
													{
														?>
														<td class="bloc">
															<a href="<?=site_url()?>kanban/task/<?=$projetos[$p]['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
														</td>
														<?php
														$cards_i++;
													}
												}
												?>

												<td class="bloc ir-projeto">
													<a href="<?=site_url()?>kanban/task/<?=$projetos[$p]['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
												</td>

											</tr>
										</table>
										<?php 
										// SUBPROJETOS

										if(array_key_exists($projetos[$p]['idprojeto'], $subprojetos)) :
										foreach($subprojetos[$projetos[$p]['idprojeto']] as $sp) :
										?>
											<table class="sub-projeto nodrag">
												<tr>
													<td class="doble title">
														<?php
														// Nome do projeto <h2>
														$subprojeto_nome = explode('-', $sp['nome']);
														$subprojeto_nome = (count($subprojeto_nome) > 1) ? $subprojeto_nome[0].'<span>'.$subprojeto_nome[1].'<span>' : implode(' ', $subprojeto_nome);
														?>
														<h2><?=$subprojeto_nome?></h2>
														<p>Atualizado: <?=date("d/m/Y", strtotime($sp['data_atualizado']))?></p>
													</td>
													<?php 

													$cards_si = 0; // Contador dos cards <td> do projeto 

													foreach($marcos[$sp['idprojeto']] as $key => $card)
													{
														$cor = "";

														// Se o index do card não for o mesmo da coluna do marco repectivo, mostra um card <td> vazio
														if($key != $cards_si)
														{
															?>
															<td class="bloc">
																<a href="<?=site_url()?>kanban/task/<?=$sp['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
															</td>
															<?php
															$cards_si++;
														}

														// Define a cor do card pelas condições do seu progresso
														if(intval($card->progresso) >= 100) { $cor = "green"; }
														elseif($card->inicio_real != null && $card->inicio_real != "")  { $cor = "yelow"; }
														elseif(($card->inicio_meta < $sp['data_status']) && 
															  ($card->inicio_real == null || $card->inicio_real == "")) { $cor = "red"; }

														// Ícone de alerta
														$icon_warning_show = false;
														if(array_key_exists($card->idprojeto_tarefas, $sub_task)) // Array $sub_task -> tarefas incompletas
														{ 
															if($sub_task[$card->idprojeto_tarefas] == 1) { $icon_warning_show = true; }
														}
														?>
														<td class="bloc <?=$cor?>" title="<?=$card->nome?>" style="height: 65px !important;">
															<a href="<?=site_url()?>kanban/task/<?=$sp['idprojeto'].'/'.$card->idprojeto_tarefas?>" style="display: block; width: 100%; height: 100%; position: absolute;">
																<?=($icon_warning_show) ? '<i class="icon icon-warning-sign"></i>' : null?>
															</a>
														</td>
														<?php
														$cards_si++;
													}

													// Adicona cards <td> que faltam para preecher a linha do projeto <tr>
													if($cards_si < $marcos_cols_cont) 
													{
														while ($cards_si < $marcos_cols_cont)
														{
															?>
															<td class="bloc">
																<a href="<?=site_url()?>kanban/task/<?=$sp['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
															</td>
															<?php
															$cards_si++;
														}
													}
													?>

													<td class="bloc ir-projeto">
														<a href="<?=site_url()?>kanban/task/<?=$sp['idprojeto']?>" style="display: block; width: 100%; height: 100%; position: absolute;"></a>
													</td>

												</tr>
											</table>
										<?php
										endforeach;
										endif;
										?>
									</td>

								</tr> <!-- Linha do projeto; --> 

								<?php
								
								$projeto_i++;

								if($projeto_i >= $projetos_por_page && $projeto_i % $projetos_por_page == 0) { break 1; }

								endfor; // FOR_PROJETOS;
								?>
							
						</table>
					</li>

					<?php endfor; // FOR_PAGES; ?>

				</ul>

				<div class="clearfix"></div>

				<div class="navegation">
					<a id="prev4" class="prev" href="#fotos"></a>
					<div id="pager4" class="pager"></div>
					<a id="next4" class="next" href="#fotos"></a>
				</div>

			</div> <!-- #fotos -->

		</div> <!-- .row; -->
	</section> <!-- #geral; -->

	<?php include("includes/js_end.php"); ?>

	<script>
	$(document).ready(function(){
		var cwrapper = $(".caroufredsel_wrapper");
		cwrapper.width(cwrapper.width() - 7);

		$("#fotos").width(cwrapper.width());
		$("#foo4 > li").width(cwrapper.width());

		$("#input-modo-apresentacao-onoff").change(function(){
			if($(this).is(":checked"))
				$("#foo4").trigger("configuration", {auto: true});
			else
				$("#foo4").trigger("configuration", {auto: false});
		})

	});
	</script>

</body>
</html>