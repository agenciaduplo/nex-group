<?php include("includes/header.php"); ?>
<body>

	<?php include("includes/header_interno.php"); ?>

	<section id="<?=$pag?>">
		<div id="colunas-swift" class="row100 on-swift">

			<?php include("includes/filtro.php"); ?>

			<ul class="plane_2 box-planes" >
				<table id="table" style="width:100%">
					<thead>
						<td class="big">Descrição da Restrição</td>
						<td class="color_1">Responsável</td>
						<td class=" ">Data Limite de Remoção</td>
						<td class="small color_1">Data de início da atividade vinculada a restrição</td>

						<?php
						$project_h    = $project[0];
						$data_status  = $project_h->data_status;

						include("includes/restricoes.trata.datas.php");
						?>

						<td class="week color_week_1"><?=$sem_i1[2]?><br/>a<br/><b><?=$sem_f1[2]?></b></td>
						<td class="week color_week_2"><?=$sem_i2[2]?><br/>a<br/><b><?=$sem_f2[2]?></b></td>
						<td class="week color_week_3"><?=$sem_i3[2]?><br/>a<br/><b><?=$sem_f3[2]?></b></td>
						<td class="week color_week_4"><?=$sem_i4[2]?><br/>a<br/><b><?=$sem_f4[2]?></b></td>
						<td class="week color_week_5"><?=$sem_i5[2]?><br/>a<br/><b><?=$sem_f5[2]?></b></td>
						<td class="week color_week_6"><?=$sem_i6[2]?><br/>a<br/><b><?=$sem_f6[2]?></b></td>
						<td class="week color_week_7"><?=$sem_i7[2]?><br/>a<br/><b><?=$sem_f7[2]?></b></td>
						<td class="week color_week_8"><?=$sem_i8[2]?><br/>a<br/><b><?=$sem_f8[2]?></b></td>

						<td class="status color_1">Status</td>
					</thead>
					<tbody id="restricoes_load"><?php include("ajax.restricoes.php"); ?></tbody>
				</table>
			</ul>

		</div>
	</section>

	<?php include("includes/js_end.php"); ?>

	<script src="<?=base_url()?>assets/js/jquery.filtros.restricoes.js"></script>

	<script>
	$(document).ready(function()
	{
		$(function()
		{
			$("#data_inicio").datepicker({dateFormat: "dd/mm/yy"}); 
			$("#data_fim").datepicker({dateFormat: "dd/mm/yy"}); 
		});

		// $(".ativs").each(function() { this.checked = true; });

		// $("#ativs_sel_all").attr('checked', "checked");

		Filtro.init();
	});
	</script> 

</body>
</html>