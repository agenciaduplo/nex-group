<?php include("includes/header.php"); ?>
<body>

	<?php include("includes/header_interno.php"); ?>

	<section id="<?=$pag?>">
		<div id="colunas-swift" class="row100 on-swift">

			<?php
			include("includes/filtro.php");

			$this->load->library('utilidades'); 
			?>

			<div id="tasks_kanban"><?php include("ajax.kanban.tasks.php"); ?></div>

		</div>
	</section>

	<?php include("includes/js_end.php"); ?>

	<script src="<?=base_url()?>assets/js/jquery.filtros.kanban.js"></script>

	<script>
	$(document).ready(function()
	{
		$(function()
		{
			$("#data_inicio").datepicker({dateFormat: "dd/mm/yy"}); 
			$("#data_fim").datepicker({dateFormat: "dd/mm/yy"}); 
		});

		// $(".ativs").each(function() { this.checked = true; });

		// $("#ativs_sel_all").attr('checked', "checked");

		bgColunasKanban();
		Filtro.init();
	});
	</script> 
</body>
</html>