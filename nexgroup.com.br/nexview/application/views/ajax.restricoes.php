<?php
$project      = $project[0];
$data_status  = $project->data_status;

include("includes/restricoes.trata.datas.php");

if(count($restricoes) > 0)
{
	foreach($restricoes as $restricao)
	{
		$data_inicio     = ($restricao->data_inicio != "") ? date("d/m/Y", strtotime($restricao->data_inicio)) : "";
		$data_restr[0]   = strtotime($restricao->data);
		$data_restr[1]   = date("d/m", $data_restr[0]);
		$colspan         = false;

		include("includes/restricoes.datas.php"); ?>

		<tr class="info">
			<td class="big "><?=$restricao->nome?><br />(<?=$restricao->nome_tarefa?>)</td>
			<td class="color_1"><?=$restricao->responsavel?><br/>(<?=$restricao->setores?>)</td>
			<td><b><?=date("d/m/Y", strtotime($restricao->data))?></b></td>
			<td class="color_1"><?=$data_inicio?></td>
			<?php if($restricao->status == 'Resolvido') { ?>
				<td colspan="9" class="week color_week_1 resolvido"><?=$restricao->status?></td>
			<?php } else if($colspan) { ?>
				<td colspan="9" class="week color_week_1 atrasado">Atrasado</td>
			<?php } else { ?>
				<td class="week color_week_1 <?=$act1?>"><?=$val1?></td>
				<td class="week color_week_2 <?=$act2?>"><?=$val2?></td>
				<td class="week color_week_3 <?=$act3?>"><?=$val3?></td>
				<td class="week color_week_4 <?=$act4?>"><?=$val4?></td>
				<td class="week color_week_5 <?=$act5?>"><?=$val5?></td>
				<td class="week color_week_6 <?=$act6?>"><?=$val6?></td>
				<td class="week color_week_7 <?=$act7?>"><?=$val7?></td>
				<td class="week color_week_8 <?=$act8?>"><?=$val8?></td>
				<td class="pendente <?=$bg_color?> active"><?=$restricao->status?></td>
			<?php } ?>
		</tr>
	<?php
	}
} else { ?>
	<tr class="info">
		<td class="big " colspan="15">Tarefa sem restrição.</td>
	</tr>
<?php } ?>
