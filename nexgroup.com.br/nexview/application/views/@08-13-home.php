<?php include("includes/header.php"); ?>
<body>  
	
	<?php include("includes/header_fixo.php"); ?>
	
	<section id="geral">

		<div class="row">

			<div id="fotos" class="list_carousel clickroll">

				<table id="table-marcos" style="width:100%;margin-top: 30px;">
					<tbody>
						<tr style="cursor: move;"><td class="doble dobles"></td>
							<?php
							// Gera as colunas com os nome dos marcos

							$marcos_cols_cont = count($marc_tit); // Total de marcos

							foreach($marc_tit as $marco)
							{
								$marco_nome = str_replace("Marco: ", "", $marco->nome);

								if($marco_nome == "Registro de Incorporação Emitido") { $marco_nome = "Registro de Inc. Emitido"; }
								
								if($marco_nome == "PPCI Aprovado") { $marco_nome = "PPCI <br /> Aprovado"; }

								echo '<td class="liv">'.$marco_nome.'</td>';
							}
							?>
						</tr>
					</tbody>
				</table>
				<?php
				// ######################  VARIÁVEIS DE CONFIGURAÇÃO ##########################

				// Contador das linhas dos projetos <tr>
				$projeto_i = 0; 

				// Total de projetos
				$projetos_total = count($projetos); 

				// Total de projetos por página de exibição (não é contabilizado sub-projetos)
				$projetos_por_page = 6;

				// Total de página de exibição dos projetos;
				// Cada página é um bloco <li> 1º nível da <ul id="foo4">;
				$pages_total = ceil($projetos_total / $projetos_por_page); 

				// ###########################################################################

				?>
				<ul id="foo4" style="margin-top: 20px;">
					
					<?php for($i = 0; $i < $pages_total; $i++) : // FOR_PAGES: ?>
					
					<li>
						<table class="table-dnd" style="width:100%">
							
								
								<?php for($p = $projeto_i; $p < $projetos_total; $p++) : // FOR_PROJETOS: ?>

								<tr id="<?=$projetos[$p]['idprojeto']?>">
									<td class="noborder">

										<table>
											<tr>
												<td class="doble title dragHandle">
													<?php
													// Nome do projeto <h2>
													$projeto_nome = explode('-', $projetos[$p]['nome']);
													$projeto_nome = (count($projeto_nome) > 1) ? $projeto_nome[0].'<span>'.$projeto_nome[1].'<span>' : '<span>'.implode(' ', $projeto_nome).'<span>';
													?>
													<h2><?=$projeto_nome?></h2>
													<p>Atualizado: <?=date("d/m/Y", strtotime($projetos[$p]['data_atualizado']))?></p>
												</td>
												<?php 

												$cards_i = 0; // Contador dos cards <td> do projeto 

												foreach($marcos[$projetos[$p]['idprojeto']] as $card)
												{
													$cor = "";

													// Define a cor do card pelas condições do seu progresso
													if(intval($card->progresso) >= 100) { $cor = "green"; }
													elseif($card->inicio_real != null && $card->inicio_real != "") 	{ $cor = "yelow"; }
													elseif(($card->inicio_meta < $projetos[$p]['data_status']) && 
														  ($card->inicio_real == null || $card->inicio_real == ""))	{ $cor = "red"; }

													// Ícone de alerta
													$icon_warning_show = false;
													if(array_key_exists($card->idprojeto_tarefas, $sub_task)) // Array $sub_task -> tarefas incompletas
													{ 
														if($sub_task[$card->idprojeto_tarefas] == 1) { $icon_warning_show = true; }
													}

													// Gera o card <td>
													$card_td  = '<td class="bloc '.$cor.'" onclick="goTask('.$projetos[$p]['idprojeto'].','.$card->idprojeto_tarefas.')">';
													$card_td .= ($icon_warning_show) ? '<i class="icon icon-warning-sign"></i>' : '';
													$card_td .= '</td>';

													echo $card_td;

													$cards_i++;
												}

												// Adicona cards <td> que faltam para preecher a linha do projeto <tr>
												if($cards_i < $marcos_cols_cont) 
												{
													while ($cards_i < $marcos_cols_cont)
													{
														echo '<td class="bloc" onclick="goProject('.$projetos[$p]['idprojeto'].')"></td>';
														$cards_i++;
													}
												}
												?>

												<td class="bloc ir-projeto" onclick="goProject(<?=$projetos[$p]['idprojeto']?>)"></td>

											</tr>
										</table>
										<?php 
										// SUBPROJETOS

										if(array_key_exists($projetos[$p]['idprojeto'], $subprojetos)) :
										foreach($subprojetos[$projetos[$p]['idprojeto']] as $sp) :
										?>
											<table class="sub-projeto nodrag">
												<tr>
													<td class="doble title">
														<?php
														// Nome do projeto <h2>
														$subprojeto_nome = explode('-', $sp['nome']);
														$subprojeto_nome = (count($subprojeto_nome) > 1) ? $subprojeto_nome[0].'<span>'.$subprojeto_nome[1].'<span>' : implode(' ', $subprojeto_nome);
														?>
														<h2><?=$subprojeto_nome?></h2>
														<p>Atualizado: <?=date("d/m/Y", strtotime($sp['data_atualizado']))?></p>
													</td>
													<?php 

													$cards_si = 0; // Contador dos cards <td> do projeto 

													foreach($marcos[$sp['idprojeto']] as $card)
													{
														$cor = "";

														// Define a cor do card pelas condições do seu progresso
														if(intval($card->progresso) >= 100) { $cor = "green"; }
														elseif($card->inicio_real != null && $card->inicio_real != "")  { $cor = "yelow"; }
														elseif(($card->inicio_meta < $sp['data_status']) && 
															  ($card->inicio_real == null || $card->inicio_real == "")) { $cor = "red"; }

														// Ícone de alerta
														$icon_warning_show = false;
														if(array_key_exists($card->idprojeto_tarefas, $sub_task)) // Array $sub_task -> tarefas incompletas
														{ 
															if($sub_task[$card->idprojeto_tarefas] == 1) { $icon_warning_show = true; }
														}

														// Gera o card <td>
														$card_td  = '<td class="bloc '.$cor.'" onclick="goTask('.$sp['idprojeto'].','.$card->idprojeto_tarefas.')">';
														$card_td .= ($icon_warning_show) ? '<i class="icon icon-warning-sign"></i>' : '';
														$card_td .= '</td>';

														echo $card_td;

														$cards_si++;
													}

													// Adicona cards <td> que faltam para preecher a linha do projeto <tr>
													if($cards_si < $marcos_cols_cont) 
													{
														while ($cards_si < $marcos_cols_cont)
														{
															echo '<td class="bloc" onclick="goProject('.$sp['idprojeto'].')"></td>';
															$cards_si++;
														}
													}
													?>

													<td class="bloc ir-projeto" onclick="goProject(<?=$sp['idprojeto']?>)"></td>

												</tr>
											</table>
										<?php
										endforeach;
										endif;
										?>
									</td>

								</tr> <!-- Linha do projeto; --> 

								<?php
								
								$projeto_i++;

								if($projeto_i >= $projetos_por_page && $projeto_i % $projetos_por_page == 0) { break 1; }

								endfor; // FOR_PROJETOS;
								?>
							
						</table>
					</li>

					<?php endfor; // FOR_PAGES; ?>

				</ul>
				<div class="clearfix"></div>
				<div class="navegation">
					<a id="prev4" class="prev" href="#fotos"></a>
					<div id="pager4" class="pager"></div>
					<a id="next4" class="next" href="#fotos"></a>
				</div>
				<!-- <p class="finalizados">Acessar os <a href="<?=base_url()?>home/concluidos/">projetos finalizados ></a></p> -->
			</div> <!-- #fotos -->

		</div> <!-- .row; -->
	</section> <!-- #geral; -->

	<?php include("includes/js_end.php"); ?>

	<script>
	function goTask(idprojeto, idprojeto_tarefa){
		location.href = "<?=site_url()?>kanban/task/"+idprojeto+"/"+idprojeto_tarefa;
	}

	function goProject(idprojeto){
		location.href = "<?=site_url()?>kanban/task/"+idprojeto+"/";
	}
	</script>

</body>
</html>