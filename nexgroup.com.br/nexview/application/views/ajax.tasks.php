              <ul class="plane_2 box-planes" >
                <h2>Atrasado</h2>

  <?php       if(isset($task_atrasado)){
                foreach($task_atrasado as $atrasado){
                  $restricao = ($atrasado->idtarefa_restricao != "") ? "restricao-ativo" : ""; ?>
                    <div class="box <?=$restricao?>"><!-- BOX -->
                      <div class="top">
                        <h3><?=$atrasado->nome?></h3>
<?php                   if($pag == "filtros"){ ?>
                          <h4><?=str_replace("Marco: ", "",$marcos[$atrasado->idmother_tarefa])?></h4>
<?php                   }else{ ?>
                          <h4><?=str_replace("Marco: ", "",$atrasado->nome_marco)?></h4>
<?php                   } ?>
                      </div>
                      <div class="content">
                        <p class="full"><b>Setor(es) Responsável:</b></p>
                        <p><span><?=$atrasado->setores?></span></p>
                        <p class="blast full">
                          <b>Início:</b> <?=$this->utilidades->data_extenso($atrasado->data_inicio)?><br>
                          <b>Término:</b> <?=$this->utilidades->data_extenso($atrasado->data_fim)?><br><br>

                          <b>Início (Meta):</b> <?=$this->utilidades->data_extenso($atrasado->inicio_meta)?><br>
                          <b>Término (Meta):</b> <?=$this->utilidades->data_extenso($atrasado->fim_meta)?><br><br>

                          <b>Início (Real):</b> <?=$this->utilidades->data_extenso($atrasado->inicio_real)?><br>
                          <b>Término (Real):</b> <?=$this->utilidades->data_extenso($atrasado->fim_real)?>
                        </p>

<?php                   if($restricao != ""){ ?>
                          <a href="<?=base_url()?>restricoes/task/<?=$atrasado->idprojeto?>/<?=$atrasado->idprojeto_tarefas?>">
                            <i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
                          </a>
<?php                   } ?>
                      </div>
                    </div>
  <?php         }
              } ?>
              </ul>

              <ul class="plane_3 box-planes" >
                <h2>Em andamento</h2>

  <?php       if(isset($task_andamento)){
                foreach($task_andamento as $andamento){
                  $restricao = ($andamento->idtarefa_restricao != "") ? "restricao-ativo" : ""; ?>
                  <div class="box <?=$restricao?>"><!-- BOX -->
                    <div class="top">
                      <h3><?=$andamento->nome?></h3>
<?php                 if($pag == "filtros"){ ?>
                        <h4><?=str_replace("Marco: ", "",$marcos[$andamento->idmother_tarefa])?></h4>
<?php                 }else{ ?>
                        <h4><?=str_replace("Marco: ", "",$andamento->nome_marco)?></h4>
<?php                 } ?>
                    </div>
                    <div class="content">
                      <p class="full"><b>Setor(es) Responsável:</b></p>
                      <p><span><?=$andamento->setores?></span></p>
                      <p class="blast full">
                        <b>Início:</b> <?=$this->utilidades->data_extenso($andamento->data_inicio)?><br>
                        <b>Término:</b> <?=$this->utilidades->data_extenso($andamento->data_fim)?><br><br>

                        <b>Início (Meta):</b> <?=$this->utilidades->data_extenso($andamento->inicio_meta)?><br>
                        <b>Término (Meta):</b> <?=$this->utilidades->data_extenso($andamento->fim_meta)?><br><br>

                        <b>Início (Real):</b> <?=$this->utilidades->data_extenso($andamento->inicio_real)?><br>
                        <b>Término (Real):</b> <?=$this->utilidades->data_extenso($andamento->fim_real)?>
                      </p>

<?php                   if($restricao != ""){ ?>
                          <a href="<?=base_url()?>restricoes/task/<?=$andamento->idprojeto?>/<?=$andamento->idprojeto_tarefas?>">
                            <i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
                          </a>
<?php                   } ?>
                    </div>
                  </div>
  <?php         }
              } ?>
              </ul>


              <ul class="plane_4 box-planes" >
                <h2>Concluído</h2>

  <?php       if(isset($task_concluido)){
                foreach($task_concluido as $concluido){
                  $restricao = ($concluido->idtarefa_restricao != "") ? "restricao-ativo" : ""; ?>
                  <div class="box <?=$restricao?>"><!-- BOX -->
                    <div class="top">
                      <h3><?=$concluido->nome?></h3>
<?php                 if($pag == "filtros"){ ?>
                        <h4><?=str_replace("Marco: ", "",$marcos[$concluido->idmother_tarefa])?></h4>
<?php                 }else{ ?>
                        <h4><?=str_replace("Marco: ", "",$concluido->nome_marco)?></h4>
<?php                 } ?>
                    </div>
                    <div class="content">
                      <p class="full"><b>Setor(es) Responsável:</b></p>
                      <p><span><?=$concluido->setores?></span></p>
                      <p class="blast full">
                        <b>Início:</b> <?=$this->utilidades->data_extenso($concluido->data_inicio)?><br>
                        <b>Término:</b> <?=$this->utilidades->data_extenso($concluido->data_fim)?><br><br>

                        <b>Início (Meta):</b> <?=$this->utilidades->data_extenso($concluido->inicio_meta)?><br>
                        <b>Término (Meta):</b> <?=$this->utilidades->data_extenso($concluido->fim_meta)?><br><br>

                        <b>Início (Real):</b> <?=$this->utilidades->data_extenso($concluido->inicio_real)?><br>
                        <b>Término (Real):</b> <?=$this->utilidades->data_extenso($concluido->fim_real)?>
                      </p>
<?php                   if($restricao != ""){ ?>
                          <a href="<?=base_url()?>restricoes/task/<?=$concluido->idprojeto?>/<?=$concluido->idprojeto_tarefas?>">
                            <i class="icon icon-warning-sign"></i><p>Ver restrições desta atividade</p>
                          </a>
<?php                   } ?>
                    </div>
                  </div>
  <?php         }
              } ?>
              </ul>

              <ul class="plane_5" >
                <h3>Próximos</h3>
  <?php         $data = "";

                if(count($nex_ativis) > 0){
                  foreach($nex_ativis as $nex){ 
                    if($data != $nex->data_inicio){ ?>
                      <h5><?=date("d/m/Y", strtotime($nex->data_inicio))?></h5>
  <?php             } ?>
                    <div class="box"> <!-- BOX -->
                      <p><?=$nex->nome?></p>
<?php                   if(isset($marcos[$nex->idmother_tarefa])){
                          $label = str_replace("Marco:", "", $marcos[$nex->idmother_tarefa]);
                        }else{
                          $label = "";
                        } ?>
                      <span><?=$label?></span>
                    </div>
  <?php             $data = $nex->data_inicio; 
                  }
                }else{ ?>
                  <h5>Sem Atividades Futuras</h5>
  <?php         } ?>
              </ul>