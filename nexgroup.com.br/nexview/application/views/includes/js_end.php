<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    
<!-- optionally include helper plugins -->
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/helper-plugins/jquery.mousewheel.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/helper-plugins/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/helper-plugins/jquery.transit.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.tablednd.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/init.js?ac=on"></script>


<div id="loaderImage-content">
	<div id="loaderImage-middle">
		<div id="loaderImage"></div>
		<span>NexView</span>
	</div>
</div>

<script type="text/javascript">
	var cSpeed=10;
	var cWidth=135;
	var cHeight=30;
	var cTotalFrames=20;
	var cFrameWidth=135;
	var cImageSrc=base_url+'assets/img/preload/sprites.png';
	
	var cImageTimeout=false;
	var cIndex=0;
	var cXpos=0;
	var cPreloaderTimeout=false;
	var SECONDS_BETWEEN_FRAMES=0;
	
	function startAnimation(){
		document.getElementById('loaderImage').style.marginTop='7px';
		document.getElementById('loaderImage').style.float='left';
		document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
		document.getElementById('loaderImage').style.width=cWidth+'px';
		document.getElementById('loaderImage').style.height=cHeight+'px';
		
		//FPS = Math.round(100/(maxSpeed+2-speed));
		FPS = Math.round(100/cSpeed);
		SECONDS_BETWEEN_FRAMES = 1 / FPS;
		
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);
	
		$("#loaderImage-content").fadeIn(500);	
	}
	
	function continueAnimation(){
		
		cXpos += cFrameWidth;
		//increase the index so we know which frame of our animation we are currently on
		cIndex += 1;
		 
		//if our cIndex is higher than our total number of frames, we're at the end and should restart
		if (cIndex >= cTotalFrames) {
			cXpos =0;
			cIndex=0;
		}
		
		if(document.getElementById('loaderImage'))
			document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';
		
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
	}
	
	function stopAnimation(){
		clearTimeout(cPreloaderTimeout);
		cPreloaderTimeout=false;
		$("#loaderImage-content").fadeOut(500);	
	}
	
	function imageLoader(s, fun)
	{
		clearTimeout(cImageTimeout);
		cImageTimeout=0;
		genImage = new Image();
		genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
		genImage.onerror=new Function('alert(\'Could not load the image\')');
		genImage.src=s;
	}

</script>