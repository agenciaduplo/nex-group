<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->
<!--[if IE 8]>     <html class="ie8"> <![endif]-->
<!--[if IE 9]>     <html class="ie9"> <![endif]-->
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="robots" content="noindex, nofollow">

	<title><?=@$title?></title>

	<link rel="shortcut icon" href="<?=base_url()?>favicon.ico" type="image/x-icon" />

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">

	<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome.css">

	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> <!-- BAIXAR PARA ASSETS  -->

	<!--[if IE 7]>
		<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome/css/font-awesome-ie7.css">
	<![endif]-->

	<script src="<?=base_url()?>assets/js/jquery-1.7.1.js"></script>
	<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  <!-- BAIXAR PARA ASSETS  -->

	<script type="text/javascript"> base_url = "<?=base_url()?>"; </script>
</head>
