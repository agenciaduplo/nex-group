<nav class="header-fixo">
	<div class="row">

		<div id="logo-content">
			<a href="<?=base_url()?>" class="logo">
				<img src="<?=base_url()?>assets/img/logo.png"> <h1 class="title">Nex Group View</h1>
			</a>
			<div style="clear: both;"></div>
			<span><?=VERSION?></span>
		</div>

		<div class="log">
			<?php
			$user_avatar = base_url() . 'assets/img/login.png';

			if($_SESSION['project']['avatar'] != "")
				$user_avatar = base_url() . 'assets/uploads/usuarios/avatar/' . $_SESSION['project']['avatar'];
			?>

      		<figure style="background: url(<?=$user_avatar?>)"></figure>

			<div class="loginfo">
				<p><?=$_SESSION['project']['nome']?></p>
				<a href="<?=site_url()?>login/sair/">Sair</a>
			</div>
		</div>

		<div class="back-kan">
			<?php $link = ($pag == 'restricoes') ? "javascript:history.go(-1)" : site_url(); ?>
			<i class="icon icon-long-arrow-left"></i>
			<a href="<?=$link?>">voltar</a>
		</div>

		<div class="right">
			<ul id="info-kan">
				<li><p>Trocar<br>Empreendimento:</p></li>
				<li>
					<select id="list_empreendimento" data-nivel="<?=$pag?>">
						<?php
						if($pag == 'kanban')
						{
							echo ($todos_projetos == 1) ? '<option value="0" selected="selected"><p>Todos Projetos</p></option>' : '<option value="0"><p>Todos Projetos</p></option>';
						}

						arsort($projetos);

						foreach($projetos as $proj)
						{
							$chk = ($todos_projetos == 0 && $project[0]->idprojeto == $proj->idprojeto) ? "selected='selected'" : "";
							echo '<option value="' . $proj->idprojeto . '" ' . $chk . '><p>' . $proj->nome . '</p></option>';
						}
						?>
					</select>
				</li>
			</ul>
			<?php
				$kanban_url 		= site_url() . 'kanban/task/' . $project[0]->idprojeto . '/';
				$kanban_active 		= '';

				$restricoes_url 	= 'javascript:void(0)';
				$restricoes_active 	= 'active';

				if($pag == 'kanban')
				{
					$kanban_url 		= 'javascript:void(0)';
					$kanban_active 		= 'active';

					$restricoes_active 	= '';

					if($todos_projetos == 0)
					{
						$restricoes_url = site_url() . 'restricoes/task/' . $project[0]->idprojeto . '/';
					}
				}
			?>
			<div id="link-kan">
				<a href="<?=$kanban_url?>" class="<?=$kanban_active?>">
					<i class="icon icon-reorder rotate"></i>
					<p>Kanban</p>
				</a>
				<a href="<?=$restricoes_url?>" class="<?=$restricoes_active?> no-margin-left">
					<i class="icon icon-warning-sign"></i>
					<p>Restrições</p>
				</a>
			</div>
		</div>

	</div>
</nav>
