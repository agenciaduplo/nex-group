<ul class="plane_1" >
	<div class="top">
		<a id="botao"><i class="icon icon-reorder"></i></a>
		<i class="icon icon-wrench"></i>
	</div>
	<div id="conteudo-filtro" class="content">
		<form id="filtros">
			<input type="hidden" name="idprojeto_tarefas" id="idprojeto_tarefas" value="<?=$idprojeto_tarefas?>" />
			<input type="hidden" name="idprojeto" id="idprojeto" value="<?=(@$todos_projetos == 0) ? (int) $project[0]->idprojeto : 0?>" />

			<h3>Filtrar Por Setor:</h3>

			<?php foreach($setores as $setor) { ?>
			<label>
				<input type="checkbox" id="<?=$setor->idprojeto_tarefas?>" name="sel_setors[]" value="<?=$setor->setores?>" onclick="filtroAtividade(0, '<?=$pag?>')" checked />
				<p><?=$setor->setores?></p>
			</label>
			<?php } ?>

			<h3 style="margin:10% 0 7% 0">Filtrar Por Atividade:</h3>

			<label>
				<input type="checkbox" name="ativs_sel_all" id="ativs_sel_all" value="ativs_todas" pag="<?=$pag?>" /> 
				<p>Todas as Atividades</p>
			</label>

			<?php

			$i = 0;
			$tot_filtros = count($f_atividades);

			for($i = 0; $i < $tot_filtros; ++$i)
			{
				$ativs  = $f_atividades[$i];

				$marc   = ($ativs->marco == 1) ? true : false;
				$n_marc = isset($f_atividades[$i+1]->marco) ? $f_atividades[$i+1]->marco : -1;
				$p_marc = isset($f_atividades[$i-1]->marco) ? $f_atividades[$i-1]->marco : -1;

				if($marc)
				{
					if($n_marc == 0){
						echo ($i > 0 && $p_marc == 0) ? "</div>" : "";
						echo "<div class='box'>";
					}else if($n_marc == 1 && $p_marc == 0){
						echo "</div>";
					}
				}

				$chk = "";
				
				if($idprojeto_tarefas != "")
				{
					$chk = ($ativs->idprojeto_tarefas == $marco->idprojeto_tarefas) ? "checked" : "";
					$chk = ($chk != "checked" && $ativs->idmother_tarefa == $marco->idprojeto_tarefas)  ? "checked" : $chk;
				}

				$class = ($marc) ? "" : "class='small'";
				$span  = ($marc) ? "" : "<span></span>";
				$nome  = str_replace("Marco: ", "", $ativs->nome);
				$add   = ($marc) ? 'group="ck'.$ativs->idprojeto_tarefas.'"' : "";
				$add2  = ($marc) ? 'marco ' : "";

				$j = $i+1;
				?>

				<label <?=$class?>>
					<?=$span?>
					<input type="checkbox" id="<?=$ativs->idprojeto_tarefas?>" name="sel_ativs[]" class="ativs <?=$add2?>ck<?=$ativs->idmother_tarefa?>" value="<?=$ativs->idprojeto_tarefas?>" pag="<?=$pag?>" <?=$chk?> <?=$add?> onclick="filtroAtividade(<?=$ativs->marco?>, '<?=$pag?>')" />
					<p><?=$nome?></p>
				</label>

				<?php 
				if($i+1 == $tot_filtros && $ativs->marco == 0) { echo '</div>'; }
			} 
			?>

			<h3 style="margin:10% 0 7% 0">Filtrar Por Data:</h3>

			<input name="data_inicio" id="data_inicio" type="text" class="antigo_select" pag="<?=$pag?>" placeholder="De:">
			<input name="data_fim" id="data_fim" type="text" class="antigo_select" pag="<?=$pag?>" placeholder="Até:">
		</form>
	</div>
</ul>

<?php //if($idprojeto_tarefas == "") { ?>
	<!-- <script type="text/javascript">
		$(document).ready(function(){
			$(".ativs").each(function(){
				this.checked = true;
			});

			$("#ativs_sel_all").attr('checked', "checked");

			filtroAtividade("all", "<?=$pag?>");
		});
	</script> -->
<?php //} ?>