<nav class="header-fixo">
    <div class="row">

        <div id="logo-content">
            <a href="<?=base_url()?>" class="logo">
                <img src="<?=base_url()?>assets/img/logo.png"> <h1 class="title">Nex Group View</h1>
            </a>
            <div style="clear: both;"></div>
            <span><?=VERSION?></span>
        </div>

        <div class="log">
            <?php
            $user_avatar = base_url() . 'assets/img/login.png';

            if($_SESSION['project']['avatar'] != "")
                $user_avatar = base_url() . 'assets/uploads/usuarios/avatar/' . $_SESSION['project']['avatar'];
            ?>

            <figure style="background: url(<?=$user_avatar?>)"></figure>

            <div class="loginfo">
                <p><?=$_SESSION['project']['nome']?></p>
                <a href="<?=site_url()?>login/sair/">Sair</a>
            </div>
        </div>

        <div class="right">
            <ul id="info">
                <li class="icon-info-a"><p>Mostra pendências <br>nas tarefas</p></li>
                <li class="icon-info-b"><p>Clique para mais<br>Informações</p></li>
                <li class="icon-info-c"><p>Mude a ordem dos<br>empreendimentos</p></li>
            </ul>

            <ul id="info-colors">
                <li><span class="icon-info-a"></span><p>Concluído</p></li>
                <li><span class="icon-info-b"></span><p>Em andamento</p></li>
                <li><span class="icon-info-c"></span><p>Atrasado</p></li>
            </ul>
        </div>

    </div>
</nav>