<ul class="plane_1" >
	<div class="top">
		<a id="botao"><i class="icon icon-reorder"></i></a>
		<!-- <i class="icon icon-wrench"></i> -->
	</div>
	<div id="conteudo-filtro" class="content">
		<form id="filtros">
			<input type="hidden" name="idprojeto_tarefas" id="idprojeto_tarefas" value="<?=$idprojeto_tarefas?>" />
			<input type="hidden" name="idprojeto" id="idprojeto" value="<?=(@$todos_projetos == 0) ? (int) $project[0]->idprojeto : 0?>" />

			<h3>Filtrar Por Setor:</h3>

			<?php foreach($setores as $setor => $value) : ?>
			<label>
				<input type="checkbox" name="setor_id[]" value="<?=$value?>" class="setor" <?php if(in_array($value, $user_filters["setores"])) echo 'checked="checked"';?> />
				<p><?=$value?></p>
			</label>
			<?php endforeach; ?>

			<h3 style="margin:10% 0 7% 0">Filtrar Por Atividade:</h3>

			<label><input type="checkbox" name="marco_all" value="0" class="marco_all" pag="<?=$pag?>" /><p>Todas as Atividades</p></label>

			<?php 
			$g = 0;
			foreach($filter_marco as $key => $marco_ids) {
				if(array_key_exists($key, $filter_tarefas)) {
			?>
				<!-- BOX: -->
				<div class="box">

					<!-- MARCO: -->
					<label><input type="checkbox" name="marco_id[]" value="<?=implode(',', $marco_ids)?>" class="ativs marco ck" pag="<?=$pag?>" group="ck<?=$g?>" <?php if(in_array(implode(',', $marco_ids), $user_filters["marcos"])) echo 'checked="checked"';?> ><p><?=str_replace("Marco: ", "", $key)?></p></label>
					<!-- MARCO; -->

					<?php foreach($filter_tarefas[$key] as $key_t => $tarefa) : ?>
					
					<!-- TAREFA: -->	
					<label class="small">
						<span></span>
						<input type="checkbox" name="atividade_id[]" value="<?=implode(',', $tarefa['id'])?>" class="ativs tarefa ck<?=$g?>" setor="<?=$tarefa['setor']?>" pag="<?=$pag?>" <?php if(in_array(implode(',', $tarefa['id']), $user_filters["tarefas"])) echo 'checked="checked"';?> ><p><?=$key_t?></p>
					</label>
					<!-- TAREFA; -->

					<?php endforeach; ?>

				</div>
				<!-- BOX; -->

			<?php } else { ?>

				<!-- MARCO SEM TAREFAS: -->
				<label><input type="checkbox" name="marco_id[]" value="<?=implode(',', $marco_ids)?>" class="ativs marco ck" pag="<?=$pag?>" group="ck<?=$g?>" <?php if(in_array(implode(',', $marco_ids), $user_filters["marcos"])) echo 'checked="checked"';?> ><p><?=str_replace("Marco: ", "", $key)?></p></label>
				<!-- MARCO SEM TAREFAS; -->

			<?php
				}
				$g++; 
	 		}
		 	?>

		 	<br style="clear: both;" />

			<h3 style="margin:10% 0 7% 0">Filtrar Por Data:</h3>

			<input name="data_inicio" id="data_inicio" type="text" class="antigo_select" pag="<?=$pag?>" placeholder="De:">
			<input name="data_fim" id="data_fim" type="text" class="antigo_select" pag="<?=$pag?>" placeholder="Até:">
		</form>
	</div>
</ul>