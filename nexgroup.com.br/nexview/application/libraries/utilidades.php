<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class utilidades{
	
	var $CI;
	
	function utilidades(){	
		$this->CI =& get_instance();
	}
	
	function data_extenso($data){
    if($data == ""){
      return "";
    }

    $mes      = explode("-", $data);
    $mes      = $mes[1];    

		switch($mes){
			case '01':
				$month = "Janeiro";
				break;
			case '02':
				$month = "Fevereiro";
				break;
			case '03':
				$month = "Março";
				break;
			case '04':
				$month = "Abril";
				break;
			case '05':
				$month = "Maio";
				break;
			case '06':
				$month = "Junho";
				break;
			case '07':
				$month = "Julho";
				break;
			case '08':
				$month = "Agosto";
				break;
			case '09':
				$month = "Setembro";
				break;
			case '10':
				$month = "Outubro";
				break;
			case '11':
				$month = "Novembro";
				break;
			case '12':
				$month = "Dezembro";
				break;
			default:
				$month = "";
				break;
    }

    return date("d", strtotime($data))." de ".$month." de ".date("Y", strtotime($data));
	}
}

?>