<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos_model extends CI_Model{

  function __construct(){
    // Call the Model constructor
    parent::__construct();
  }

  function getProjetos($order=false, $ativos=true)
  {
    if(!$order)
    {
      $ativos = ($ativos) ? " WHERE progresso != '100' " : "";
      $sql = "SELECT * FROM projetos {$ativos} ORDER BY data_atualizado DESC";
    }
    else
    {
      $ativos     = ($ativos) ? " AND p.progresso != '100' " : "";

      $idusuario  = $this->encrypt->decode($_SESSION['project']['idusuario']);

      $sql = "SELECT
                p.*
              FROM
                projetos AS p
              INNER JOIN
                usuario_projeto AS up
              ON
                up.idprojeto = p.idprojeto
              WHERE
                up.idusuario = {$idusuario}
              {$ativos}
              ORDER BY
                up.ordem
              ASC";
    }
    return $this->db->query($sql)->result();
  }

  function getAllProjetosOnly()
  {
    $idusuario  = $this->encrypt->decode($_SESSION['project']['idusuario']);

    $sql = "SELECT
                p.*
              FROM
                projetos AS p
              INNER JOIN
                usuario_projeto AS up
              ON
                up.idprojeto = p.idprojeto
              WHERE
                p.idprojeto_mother IS NULL AND up.idusuario = {$idusuario}
              ORDER BY
                up.ordem
              ASC";

    return $this->db->query($sql)->result();
  }

  function getAllSubProjetos()
  {
    $sql = "SELECT p.* FROM projetos AS p WHERE p.idprojeto_mother > 0 ORDER BY p.idprojeto_mother, p.nome";

    return $this->db->query($sql)->result();
  }

  function getSubProjetosByIdProjeto($idprojeto)
  {
    $sql = "SELECT
                p.*
              FROM
                projetos AS p
              WHERE
                p.idprojeto_mother = {$idprojeto}
          ";

    return $this->db->query($sql)->result();
  }

  function getProjetosConcluidos(){
    $sql = "SELECT * FROM projetos WHERE progresso = '100' ORDER BY data_atualizado DESC";
    return $this->db->query($sql)->result();
  }

  function getProjetosList()
  {
    $sql = "SELECT idprojeto, nome FROM projetos";
    return $this->db->query($sql)->result();
  }

  function getProjetosListByUser($id_usuario)
  {
    $sql = "SELECT p.idprojeto, p.nome FROM projetos p INNER JOIN usuario_projeto up ON up.idprojeto = p.idprojeto WHERE up.idusuario = " . (int) $id_usuario;
    return $this->db->query($sql)->result();
  }

  function getProjeto($idprojeto){
    $sql = "SELECT * FROM projetos WHERE idprojeto = {$idprojeto} LIMIT 1";
    return $this->db->query($sql)->result();
  }

  function getAllProjetos()
  {
    $idusuario = $this->encrypt->decode($_SESSION['project']['idusuario']);

    $sql = "SELECT p.* FROM projetos p INNER JOIN usuario_projeto up ON p.idprojeto = up.idprojeto WHERE up.idusuario = " . (int) $idusuario;
    return $this->db->query($sql)->result();
  }



  function getMarcos($idprojeto = "", $just_name = false, $group = false, $arr_fields = array("*"))
  {
    $campo  = ($just_name) ? "idprojeto_tarefas, nome" : implode(",", $arr_fields);
    $group  = ($group)     ? "GROUP BY nome"           : "";

    if(is_int($idprojeto) && $idprojeto == 0)
    {
      $sql = "SELECT {$campo} FROM projeto_tarefas {$group} ORDER BY idprojeto_tarefas ASC";
    }
    else
    {
      $where = 'ativo = "S"';
      $where .= ($idprojeto != "") ? " AND idprojeto = {$idprojeto} " : " AND marco = 1 ";

      $sql = "SELECT {$campo} FROM projeto_tarefas WHERE {$where} {$group} ORDER BY idprojeto_tarefas ASC";
    }

    return $this->db->query($sql)->result();
  }



  function getMarcoByTask($idprojeto_tarefas){
    $sql = "SELECT * FROM projeto_tarefas WHERE idprojeto_tarefas = {$idprojeto_tarefas} AND ativo = 'S' LIMIT 1";
    return $this->db->query($sql)->result();
  }

  function getAllMarcos()
  {
    $sql = "SELECT * FROM projeto_tarefas WHERE marco = 1 AND ativo = 'S'";
    return $this->db->query($sql)->result();
  }







  /**
   * getAllMarcosRepety()
   *
   * Retorna os marcos que tiverem terefas com seu id
   */
  function getAllMarcosRepety($idprojeto = 0, $idprojeto_tarefa = 0)
  {
    // $sql = "SELECT idprojeto_tarefas, nome FROM projeto_tarefas WHERE marco = 1 ORDER BY nome ASC";

    $and_projeto = (intval($idprojeto) > 0) ? " AND p1.idprojeto = " . $idprojeto : "";
    $and_projeto_tarefea = (intval($idprojeto_tarefa) > 0) ? " AND p2.idmother_tarefa = " . $idprojeto_tarefa : "";

    $sql = "SELECT DISTINCT
              p1.idprojeto_tarefas, p1.nome
            FROM
              projeto_tarefas p1
            WHERE
              p1.marco = 1 $and_projeto AND p1.ativo = 'S'
            GROUP BY idprojeto_tarefas
            HAVING (SELECT
                    COUNT(p2.idprojeto_tarefas)
                  FROM
                      projeto_tarefas p2
                  WHERE
                      p2.data_inicio <= NOW() AND p2.idmother_tarefa = p1.idprojeto_tarefas $and_projeto_tarefea) > 0
            ORDER BY nome ASC
            ";

    return $this->db->query($sql)->result();
  }

  // $idmother_tarefa LIST
  function getAllTarefasRepety($idmother_tarefa)
  {
    $sql = "SELECT
              idprojeto_tarefas, nome, setores
            FROM
              projeto_tarefas
            WHERE
              idmother_tarefa IN($idmother_tarefa)
              AND ativo = 'S' AND data_inicio <= NOW()
            ORDER BY nome ASC";

    return $this->db->query($sql)->result();
  }


  function getTarefaByID($idtarefa)
  {
    $sql = "SELECT
              pt.idprojeto_tarefas,
              pt.nome,
              pt.setores,
              pt.marco,
              pt.idmother_tarefa,
              ptt.nome AS nome_marco
            FROM
              projeto_tarefas AS pt
            LEFT JOIN
              projeto_tarefas ptt ON pt.idmother_tarefa = ptt.idprojeto_tarefas
            WHERE
              pt.ativo = 'S'
              AND pt.idprojeto_tarefas = " . (int) $idtarefa;

    return $this->db->query($sql)->row();
  }






  function getAllTasksByUser($id_usuario)
  {

    $id_usuario = (int) $id_usuario;

    $sql = "SELECT pt.*
            FROM
              projeto_tarefas pt
            INNER JOIN
              usuario_projeto up ON up.idprojeto = pt.idprojeto
            WHERE
              up.idusuario = {$id_usuario}
              AND pt.marco = 0
              AND pt.ativo = 'S'
            ";

    return $this->db->query($sql)->result();
  }

  function getAllForProject($idprojeto = 0, $marco=false, $data_inicio = "", $data_fim = "", $sel_setors = "", $restricoes = false){
    $and          = ($marco) ? " AND pt.marco = 0 " : "";
    $sinal        = ($data_fim    != "") ? ">=" : "=";
    $data_inicio  = ($data_inicio != "") ? " AND pt.data_inicio {$sinal} '".$data_inicio."' "  : "";
    $data_fim     = ($data_fim    != "") ? " AND pt.data_inicio <= '".$data_fim."' "           : "";
    $sel_setors   = ($sel_setors  != "") ? " AND pt.setores IN ('".$sel_setors."') "           : "";

    if($restricoes)
    {
      $sql = "SELECT DISTINCT
                pt.*,
                ptr.idtarefa_restricao
              FROM
                projeto_tarefas AS pt
              LEFT JOIN
                projeto_tarefas_restricoes AS ptr
                ON
                  ptr.idtarefa = pt.idprojeto_tarefas
              WHERE
                pt.idprojeto = {$idprojeto}
                AND pt.ativo = 'S'
                {$and}
                {$data_inicio}
                {$data_fim}
                {$sel_setors}
              ORDER BY
                pt.idprojeto_tarefas ASC";
    }
    else
    {
      if(intval($idprojeto) > 0)
      {
        $sql = "SELECT DISTINCT
                  pt.*, ptt.nome AS nome_marco
                FROM
                  projeto_tarefas AS pt
                LEFT JOIN
                  projeto_tarefas ptt ON pt.idmother_tarefa = ptt.idprojeto_tarefas
                WHERE
                  pt.idprojeto = {$idprojeto} {$and} {$data_inicio} {$data_fim} {$sel_setors} AND pt.ativo = 'S'
                ORDER BY
                  pt.idprojeto_tarefas ASC";
      }
      else
      {
        $where_no_projeto = '';

        if($marco) { $where_no_projeto = ' marco = 0'; }

        if($data_inicio != "")
        {
          $where_no_projeto .= ($where_no_projeto != '') ? " AND pt.data_inicio {$sinal} '".$data_inicio."' " : "pt.data_inicio {$sinal} '".$data_inicio."'";
        }

        if($data_fim != "")
        {
          $where_no_projeto .= ($where_no_projeto != '') ? " AND pt.data_inicio <= '".$data_fim."'" : "pt.data_inicio <= '".$data_fim."'";
        }

        if($sel_setors != "")
        {
          $where_no_projeto .= ($where_no_projeto != '') ? " AND pt.setores IN ('".$sel_setors."')" : "pt.setores IN ('".$sel_setors."')";
        }

        if($where_no_projeto != '') { $where_no_projeto = 'WHERE' . $where_no_projeto; }

        // $data_inicio  = ($data_inicio != "") ? " AND pt.data_inicio {$sinal} '".$data_inicio."' "  : "";
        // $data_fim     = ($data_fim    != "") ? " AND pt.data_inicio <= '".$data_fim."' "           : "";
        // $sel_setors   = ($sel_setors  != "") ? " AND pt.setores IN ('".$sel_setors."') "           : "";

        // $idusuario = (int) $this->encrypt->decode($_SESSION['project']['idusuario']);
        // AND up.idusuario = {$idusuario}

        $sql = "SELECT DISTINCT
                  pt.idprojeto_tarefas,
                  pt.idprojeto,
                  pt.nome,
                  pt.setores,
                  pt.data_inicio,
                  pt.data_fim,
                  pt.inicio_real,
                  pt.fim_real,
                  pt.inicio_meta,
                  pt.fim_meta,
                  pt.progresso
                FROM
                  projeto_tarefas AS pt
                INNER JOIN projetos p ON p.idprojeto = pt.idprojeto
                INNER JOIN usuario_projeto up ON p.idprojeto = up.idprojeto
                {$where_no_projeto} AND pt.ativo = 'S'
                ORDER BY
                  pt.idprojeto_tarefas ASC
                ";


      }
    }

    // echo $sql;
    // exit;

    return $this->db->query($sql)->result();
  }

  function getIncompletSubTarefas($boolean = false)
  {
    $sql = "SELECT * FROM projeto_tarefas WHERE marco = 0 AND progresso < 100 AND ativo = 'S' GROUP BY idmother_tarefa";

    $res = $this->db->query($sql)->result();
    $tot = count($res);
    $ret = "";

    if(!$boolean){
      for($i=0;$i<$tot;++$i){
        $ret[$res[$i]->idmother_tarefa] = $res[$i];
      }
    }else{
      for($i=0;$i<$tot;++$i){
        $ret[$res[$i]->idmother_tarefa] = true;
      }
    }

    return $ret;
  }

  function getSubTarefas($idmother_tarefa, $marco = 0, $restricoes = false)
  {
    if($restricoes)
    {
      $sql = "SELECT
                pt.*,
                ptr.idtarefa_restricao,
                (SELECT nome FROM projeto_tarefas WHERE idprojeto_tarefas = {$idmother_tarefa} AND marco = 1 LIMIT 1) AS nome_marco
              FROM
                projeto_tarefas AS pt
              LEFT JOIN
                projeto_tarefas_restricoes AS ptr
                ON
                  ptr.idtarefa = pt.idprojeto_tarefas
              WHERE
                pt.idmother_tarefa = {$idmother_tarefa}
                AND pt.marco = {$marco}
                AND pt.ativo = 'S'
              ";
    }
    else
    {
      $sql = "SELECT
                *,
                (SELECT nome FROM projeto_tarefas WHERE idprojeto_tarefas = {$idmother_tarefa} AND marco = 1 AND ativo = 'S' LIMIT 1) AS nome_marco
              FROM
                projeto_tarefas AS pf
              WHERE
                idmother_tarefa = {$idmother_tarefa}
                AND pf.ativo = 'S'
                AND marco = {$marco}
              ";
    }

    return $this->db->query($sql)->result();
  }

  function getProxTasks($idprojeto = 0, $ids_tarefas = "")
  {
    $WHERE = 'WHERE pt.data_inicio > "' . date("Y-m-d") . '" AND pt.marco = 0 AND pt.ativo = "S"';

    if($ids_tarefas != "") { $WHERE .= " AND pt.idmother_tarefa IN ({$ids_tarefas}) "; }

    if(intval($idprojeto) > 0) { $WHERE .= " AND pt.idprojeto = " . (int) $idprojeto; }

    $sql = "SELECT
                pt.nome,
                pt.data_inicio,
                pt.idmother_tarefa,
                p.nome AS 'projeto'
            FROM
                projeto_tarefas pt
            INNER JOIN projetos p ON pt.idprojeto = p.idprojeto
            {$WHERE}
            ORDER BY pt.data_inicio ASC
            ";

    return $this->db->query($sql)->result();
  }

  function getTasksFiltro($idprojeto, $idprojeto_tarefas, $data_inicio = "", $data_fim = "", $sel_setors = "", $restricoes = false)
  {
    $sinal        = ($data_fim    != "") ? ">=" : "=";
    $data_inicio  = ($data_inicio != "") ? " AND pt.data_inicio {$sinal} '".$data_inicio."' "  : "";
    $data_fim     = ($data_fim    != "") ? " AND pt.data_inicio <= '".$data_fim."' "           : "";
    $sel_setors   = ($sel_setors  != "") ? " AND pt.setores IN ('".$sel_setors."') "           : "";

    if($restricoes){
      $sql = "SELECT
                pt.*,
                ptr.idtarefa_restricao
              FROM
                projeto_tarefas AS pt
              LEFT JOIN
                projeto_tarefas_restricoes AS ptr
              ON
                ptr.idtarefa = pt.idprojeto_tarefas
              WHERE
                pt.idprojeto = {$idprojeto}
                AND pt.idprojeto_tarefas IN ({$idprojeto_tarefas})
                AND pt.marco = 0 {$data_inicio} {$data_fim} {$sel_setors}
                AND pt.ativo = 'S'
              ";
    }else{

      // echo 'Tarefas: <br/>';
      // var_dump($idprojeto_tarefas);
      // echo '<br/>';
      $idprojeto_tarefas = (string) $idprojeto_tarefas;

      $sql = "SELECT
                pt.*
              FROM
                projeto_tarefas AS pt
              WHERE
                pt.idprojeto = {$idprojeto}
              AND pt.idprojeto_tarefas IN ({$idprojeto_tarefas})
              AND pt.marco = 0
              AND pt.ativo = 'S'
              {$data_inicio} {$data_fim} {$sel_setors}";
    }


    // echo $sql;
    // exit;

    return $this->db->query($sql)->result();
  }



  function getCardsKanban($idprojeto = 0, $idprojeto_tarefas = 0, $data_inicio = "", $data_fim = "", $marco = "", $tarefas = "", $area = 'kanban')
  {
    if($area == 'restricoes')
    {
      $WHERE = 'WHERE pt.ativo = "S" AND pt.idprojeto = ' . $idprojeto;
    }
    else
    {
      $WHERE = 'WHERE pt.ativo = "S" AND pt.marco = 0';

      if($idprojeto > 0) { $WHERE .= ' AND pt.idprojeto = ' . $idprojeto; }
    }

    if($idprojeto_tarefas > 0)
    {
      $WHERE .= ' AND pt.idmother_tarefa = ' . $idprojeto_tarefas;
      $marco = '' ;
    }

    if($data_inicio != "" && $data_fim != "")
    {
      $WHERE .= " AND (pt.data_inicio >= '" . $data_inicio . "' AND pt.data_fim <= '" . $data_fim . "')";
    }
    else
    {
      if($data_inicio != "") { $WHERE .= " AND pt.data_inicio >= '" . $data_inicio . "'"; }
      if($data_fim != "")    { $WHERE .= " AND pt.data_fim <= '" . $data_fim . "'"; }
    }

    if($marco != "" && $tarefas != "")
    {
      $WHERE .= ' AND (pt.idmother_tarefa IN(' . $marco . ') OR pt.idprojeto_tarefas IN(' . $tarefas . '))';
    }
    else
    {
       if($marco != "")    { $WHERE .= ' AND pt.idmother_tarefa IN(' . $marco . ')'; }
       if($tarefas != "")  { $WHERE .= ' AND pt.idprojeto_tarefas IN(' . $tarefas . ')'; }
    }

    $sql = "SELECT
                pt.idprojeto_tarefas,
                pt.idprojeto,
                p.nome AS 'projeto',
                pt.nome,
                pt.setores,
                pt.data_inicio,
                pt.data_fim,
                pt.inicio_real,
                pt.fim_real,
                pt.progresso,
                pt.duracao_dias,
                pt.idmother_tarefa,
                pt.inicio_meta,
                pt.fim_meta
            FROM
                projeto_tarefas pt
            INNER JOIN
                projetos p ON pt.idprojeto = p.idprojeto
            $WHERE
            ";

    return $this->db->query($sql)->result();
  }


  function getCardsRestricoes($idprojeto = 0, $idprojeto_tarefas = 0, $data_inicio = "", $data_fim = "", $marco = "", $tarefas = "")
  {

    $WHERE = 'WHERE pt.ativo = "S" AND pt.idprojeto = ' . $idprojeto;

    if($idprojeto_tarefas > 0)
    {
      $WHERE .= ' AND pt.idprojeto_tarefas = ' . $idprojeto_tarefas;
      $marco = '' ;
    }

    if($data_inicio != "" && $data_fim != "")
    {
      $WHERE .= " AND (pt.data_inicio >= '" . $data_inicio . "' AND pt.data_fim <= '" . $data_fim . "')";
    }
    else
    {
      if($data_inicio != "") { $WHERE .= " AND pt.data_inicio >= '" . $data_inicio . "'"; }
      if($data_fim != "")    { $WHERE .= " AND pt.data_fim <= '" . $data_fim . "'"; }
    }

    if($marco != "" && $tarefas != "")
    {
      $WHERE .= ' AND ( ( pt.idmother_tarefa IN(' . $marco . ') OR ptr.idtarefa IN(' . $marco . ') ) OR ptr.idtarefa IN(' . $tarefas . ') )';
    }
    else
    {
       if($marco != "")    { $WHERE .= ' AND ( pt.idmother_tarefa IN(' . $marco . ') OR ptr.idtarefa IN(' . $marco . ') )'; }
       if($tarefas != "")  { $WHERE .= ' AND ptr.idtarefa IN(' . $tarefas . ')'; }
    }

    $sql = "SELECT
              ptr.*,
              pt.data_inicio,
              pt.nome AS nome_tarefa,
              pt.setores
            FROM
              projeto_tarefas_restricoes ptr
            INNER JOIN
              projeto_tarefas pt ON ptr.idtarefa = pt.idprojeto_tarefas
            $WHERE
            ";

    // echo $sql;
    return $this->db->query($sql)->result();
  }







  function getSetores($idprojeto = 0)
  {
    $where_projeto = (intval($idprojeto) > 0) ? 'idprojeto = ' . (int) $idprojeto . ' AND' : '';

    $sql = "SELECT * FROM projeto_tarefas WHERE {$where_projeto} marco = 0 AND setores IS NOT NULL AND ativo = 'S' AND GROUP BY setores";

    return $this->db->query($sql)->result();
  }


  function getRestricoes($idtarefa){
    $sql = "SELECT
              ptr.*,
              pt.inicio_meta,
              pt.nome AS nome_tarefa,
              pt.setores
            FROM
              projeto_tarefas_restricoes AS ptr
            INNER JOIN
              projeto_tarefas AS pt
            ON
              pt.idprojeto_tarefas = ptr.idtarefa
            WHERE
              idtarefa = {$idtarefa}
              AND pt.ativo = 'S'
              ";
    return $this->db->query($sql)->result();
  }

  function getRestricoesProject($idprojeto){
    $sql = "SELECT
                ptr.*,
                pt.data_inicio,
                pt.nome AS nome_tarefa,
                pt.setores
              FROM
                projeto_tarefas AS pt
              INNER JOIN
                projeto_tarefas_restricoes AS ptr
              ON
                ptr.idtarefa = pt.idprojeto_tarefas
              WHERE
                pt.idprojeto = {$idprojeto}
                AND pt.ativo = 'S'
              ORDER BY
                pt.idprojeto_tarefas ASC";

    return $this->db->query($sql)->result();
  }

  function getRestricoesFiltro($idprojeto, $idprojeto_tarefas, $data_inicio = "", $data_fim = "", $sel_setors = ""){
    $sinal        = ($data_fim    != "") ? ">=" : "=";
    $data_inicio  = ($data_inicio != "") ? " AND pt.data_inicio {$sinal} '".$data_inicio."' "  : "";
    $data_fim     = ($data_fim    != "") ? " AND pt.data_inicio <= '".$data_fim."' "           : "";
    $sel_setors   = ($sel_setors  != "") ? " AND pt.setores IN ('".$sel_setors."') "           : "";

    $sql = "SELECT
              pt.idprojeto_tarefas,
              pt.nome AS nome_tarefa,
              ptr.*
            FROM
              projeto_tarefas AS pt
            LEFT JOIN
              projeto_tarefas_restricoes AS ptr
            ON
              ptr.idtarefa = pt.idprojeto_tarefas
            WHERE
              pt.idprojeto = {$idprojeto}
            AND
              pt.idprojeto_tarefas IN ({$idprojeto_tarefas})
              AND pt.ativo = 'S'
            AND
              pt.marco = 0 {$data_inicio} {$data_fim} {$sel_setors}";

    return $this->db->query($sql)->result();
  }

  function getAllForProjectRestricoes($idprojeto, $marco=false, $data_inicio = "", $data_fim = "", $sel_setors = ""){
    $and          = ($marco) ? " AND marco = 0 " : "";
    $sinal        = ($data_fim    != "") ? ">=" : "=";
    $data_inicio  = ($data_inicio != "") ? " AND pt.data_inicio {$sinal} '".$data_inicio."' "  : "";
    $data_fim     = ($data_fim    != "") ? " AND pt.data_inicio <= '".$data_fim."' "           : "";
    $sel_setors   = ($sel_setors  != "") ? " AND pt.setores IN ('".$sel_setors."') "           : "";

    $sql = "SELECT
              pt.idprojeto_tarefas,
              pt.setores,
              pt.nome AS nome_tarefa,
              ptr.*
            FROM
              projeto_tarefas AS pt
            LEFT JOIN
              projeto_tarefas_restricoes AS ptr
            ON
              ptr.idtarefa = pt.idprojeto_tarefas
            WHERE
              pt.idprojeto = {$idprojeto}
              AND pt.ativo = 'S'
            {$and}
            {$data_inicio}
            {$data_fim}
            {$sel_setors}
            ORDER BY
              pt.idprojeto_tarefas ASC";

    return $this->db->query($sql)->result();
  }

  function getIdRestricao($idtarefa, $all = false){
    $campos = ($all) ? "*.ptr" : ".ptr";

    if($all){
      $sql = "SELECT
                ptr.*,
                pt.inicio_meta,
                pt.setores,
                pt.nome AS nome_tarefa
              FROM
                projeto_tarefas_restricoes AS ptr
              INNER JOIN
                projeto_tarefas AS pt
              ON
                ptr.idtarefa = pt.idprojeto_tarefas
              WHERE
                ptr.idtarefa = {$idtarefa}
                AND pt.ativo = 'S'
              ";
    }else{
      $sql = "SELECT
                idtarefa_restricao
              FROM
                projeto_tarefas_restricoes
              WHERE
                idtarefa = {$idtarefa}
              LIMIT 1";
    }
    return $this->db->query($sql)->result();
  }




function getUserFilters($idprojeto, $area)
{
  $data = array(
    'idusuario' => $this->encrypt->decode($_SESSION['project']['idusuario']),
    'idprojeto' => $idprojeto,
    'area'      => $area
  );

  $query = $this->db->get_where('usuarios_filtros', $data);

  return $query->result();
}



function UpdateUserFilters($idprojeto, $area, $setores, $marcos, $tarefas)
{
    $data = array(
        'idusuario' => $this->encrypt->decode($_SESSION['project']['idusuario']),
        'idprojeto' => (int) $idprojeto,
        'area'      => $area
    );

    if(isset($setores))
    {
        $data['tipo'] = "S";

        $query = $this->db->get_where('usuarios_filtros', $data, 1);

        $data['filtro'] = $setores;

        if($query->num_rows() > 0)
        {
            $row = $query->row();

            $this->db->where('id', $row->id);
            $this->db->update('usuarios_filtros', $data); 
        }
        else
        {
            $this->db->insert('usuarios_filtros', $data);
        }
    }


    if(isset($marcos))
    {
        unset($data['filtro']);

        $data['tipo'] = "M";

        $query = $this->db->get_where('usuarios_filtros', $data, 1);

        $data['filtro'] = $marcos;

        if($query->num_rows() > 0)
        {
            $row = $query->row();

            $this->db->where('id', $row->id);
            $this->db->update('usuarios_filtros', $data); 
        }
        else
        {
            $this->db->insert('usuarios_filtros', $data);
        }
    }


    if(isset($tarefas))
    {
        unset($data['filtro']);

        $data['tipo'] = "T";

        $query = $this->db->get_where('usuarios_filtros', $data, 1);

        $data['filtro'] = $tarefas;

        if($query->num_rows() > 0)
        {
            $row = $query->row();

            $this->db->where('id', $row->id);
            $this->db->update('usuarios_filtros', $data); 
        }
        else
        {
            $this->db->insert('usuarios_filtros', $data);
        }
    }


}  //  UserFiltersUpdate();





  function gravaOrdemProjetos($idprojetos)
  {
    $idusuario  = $this->encrypt->decode($_SESSION['project']['idusuario']);
    $idprojetos = explode(",", $idprojetos);

    $this->db->where('idusuario', $idusuario);
    $this->db->delete('usuario_projeto');

    foreach($idprojetos as $i => $idprojeto)
    {
      $this->db->insert('usuario_projeto',
        array(
          'idusuario_projeto' => "",
          'idusuario'         => $idusuario,
          'idprojeto'         => $idprojeto,
          'ordem'             => $i
        )
      );
    }
    
  }



}

/* End of file projetos_model.php */
/* Location: ./system/application/model/projetos_model.php */
