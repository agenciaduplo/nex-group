<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] 	= "home";
$route['404_override'] 			= 'home/error';


/* End of file routes.php */
/* Location: ./application/config/routes.php */

$route['nao-encontrou-seu-imovel']							= "home/index";

$route['casas'] 											= "home/casas";

$route['dhz-construcoes'] 									= "home/index/dhz";
$route['capa-engenharia'] 									= "home/index/capa";
$route['egl-engenharia'] 									= "home/index/egl";
$route['lomando-aita'] 										= "home/index/lomando";

$route['imoveis2/(:num)/(:any)'] 							= "imoveis/visualizar2/$1";

$route['noticias/(:num)/(:any)'] 							= "noticias/visualizar/$1";
$route['noticias'] 											= "noticias/lista";
$route['noticias/(:num)'] 									= "noticias/lista/$1";

$route['noticias-cotidiano'] 								= "noticias/rsslista";

$route['clippings/(:num)/(:any)'] 							= "clippings/visualizar/$1";
$route['clippings'] 										= "clippings/lista";
$route['clippings/(:num)'] 									= "clippings/lista/$1";

$route['navegue-pelo-mapa'] 								= "navegue/index";

$route['central-de-vendas'] 								= "central/index";
$route['ligamos-para-voce'] 								= "central/ligamos";
$route['ligamos-para-voce/(:num)'] 							= "central/ligamos/$1";
$route['atendimento-email'] 								= "central/email";
$route['atendimento-email/(:num)'] 							= "central/email/$1";
$route['nao-encontrei'] 									= "central/naoEncontrou";

$route['contato'] 											= "contato/index";
$route['contato/fale-conosco']								= "contato/index";
$route['contato/venda-seu-terreno']							= "contato/venda_seu_terreno";
$route['contato/trabalhe-conosco'] 							= "contato/trabalhe_conosco";
$route['contato/seja-um-fornecedor'] 						= "contato/seja_um_fornecedor";

$route['Imoveis'] 											= "imoveis/lista";
$route['Apartamentos'] 										= "imoveis/lista";
$route['Casas'] 											= "imoveis/lista";
$route['Imobiliarias'] 										= "imoveis/lista";

$route['imoveis'] 											= "imoveis/lista";
$route['imoveis/(:num)'] 									= "imoveis/lista/$1";
$route['imoveis/(:any)'] 									= "imoveis/visualizar/$1";

$route['buscar'] 											= "home/buscaGeral";
$route['buscar/(:num)/(:any)'] 								= "imoveis/buscaGeral/$1";

$route['imoveis/buscarImoveis'] 							= "imoveis/buscaImovel";
$route['imoveis/buscarImoveis/(:num)/(:any)'] 				= "imoveis/buscaImovel/$1";

$route['empresa/obras-realizadas'] 							= "empresa/obras_realizadas";
$route['empresa/obras-realizadas/capa-engenharia'] 			= "empresa/obras_realizadas_capa";
$route['empresa/obras-realizadas/capa-engenharia/(:num)'] 	= "empresa/obras_realizadas_capa/$1";
$route['empresa/obras-realizadas/egl-engenharia'] 			= "empresa/obras_realizadas_egl";
$route['empresa/obras-realizadas/egl-engenharia/(:num)'] 	= "empresa/obras_realizadas_egl/$1";
$route['empresa/obras-realizadas/dhz-construcoes'] 			= "empresa/obras_realizadas_dhz";
$route['empresa/obras-realizadas/dhz-construcoes/(:num)'] 	= "empresa/obras_realizadas_dhz/$1";
$route['empresa/obras-realizadas/lomando-aita'] 			= "empresa/obras_realizadas_lomando";
$route['empresa/obras-realizadas/lomando-aita/(:num)'] 		= "empresa/obras_realizadas_lomando/$1";

$route['revista-nexday'] 									= "revista/index";

$route['3-grandes-lancamentos-2012/(:any)']					= "home/tres_grandes_lancamentos";
$route['3-grandes-lancamentos-2012']						= "home/tres_grandes_lancamentos";

$route['casas-de-2-e-3-dormitorios/(:any)']					= "home/casas_dois_tres_dormitorios";
$route['casas-de-2-e-3-dormitorios']						= "home/casas_dois_tres_dormitorios";

$route['moradas-e-terra-nova-alvorada/(:any)']				= "home/moradas_e_terra_nova_alvorada";
$route['moradas-e-terra-nova-alvorada']						= "home/moradas_e_terra_nova_alvorada";

$route['moradas-parque-do-lago-gravatai-rs/(:any)'] 		= "home/moradas_parque_do_lago";
$route['moradas-parque-do-lago-gravatai-rs'] 				= "home/moradas_parque_do_lago";


$route['feijoadinha-do-chacara'] 							= "home/feijoada_do_chacara";
// $route['nexchange'] 										= "nexchange/index";

$route['editorial-zh-nexchange'] 							= "nexchange/enviaNexChange/Publi_editorial_ZH";


$route['oferta/([a-z0-9-]+)'] 								= "ofertas/interna/$1";

$route['nexchange-auxiliadora']								= "nexchange_auxiliadora/index";
 
$route['chacara-das-nascentes']								= "chacara/index"; 
$route['chacara-das-nascentes/(:any)']						= "chacara/$1"; 

$route['singolo']											= "singolo/index"; 
$route['singolo/(:any)']									= "singolo/$1"; 

$route['my-urban']											= "myurban/index"; 
$route['my-urban/(:any)']									= "myurban/$1"; 

$route['max-haus']											= "maxhaus/index"; 
$route['max-haus/(:any)']									= "maxhaus/$1"; 

$route['amores-da-brava']									= "amoresdabrava/index"; 
$route['amores-da-brava/(:any)']							= "amoresdabrava/$1"; 