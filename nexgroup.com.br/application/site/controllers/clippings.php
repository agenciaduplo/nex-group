<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings extends CI_Controller {

	public function index()
	{
		redirect('clippings/lista/');
	}
	
	function lista ($offset = "")
	{
		
		$this->load->model('clippings_model', 'model');
		$this->load->model('noticias_model', 'modelNoticias');
		$this->load->model('imoveis_model', 'modelImoveis');
		
		$config = array (
			'uri_segment'		=> 2,
			'base_url'			=> 'http://www.nexgroup.com.br/clippings/',
			'total_rows'		=> $this->model->numClippings(),
			'first_link'        => '<<',
			'last_link'			=> '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '5'
		);
		
		$this->pagination->initialize($config); 
		
			$tags = $this->modelImoveis->getTags(20);
	 		if($tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'Clippings - NEX GROUP';
	 			
		$data = array(
			'title'					=> $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,
		
			'page'					=> 'clippings',
			
			'ultimas' 				=> $this->modelNoticias->getNoticias(25),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'clippings'  			=> $this->model->getListaClippings($offset),
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'paginacao'	  			=> $this->pagination->create_links(),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('clippings/clippings',$data);
	}
	
	public function visualizar($id)
	{
		$this->load->model('clippings_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
	
		$clipping = $this->model->getClipping($id);	
		$tags = explode(' ',$clipping->titulo);
		$tag = '';
		$excluir = array('a','e','em','do','de','da','um','como','é','ou','que','uma','para','no','na','se','até');
		foreach ($tags as $row):
			if(!in_array(strtolower($row), $excluir)){
				$tag .= $row.',';
			}
		endforeach;
		$data = array(
			'title'					=> "$clipping->titulo - Clippings - NEX GROUP",
			'keywords'				=> $tag,
			'description'			=> $clipping->titulo,
			'page'					=> 'clippings',
			
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'clipping'  				=> $clipping,
			
			'ultimas' 				=> $this->model->getClippings(3),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('clippings/clipping',$data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */