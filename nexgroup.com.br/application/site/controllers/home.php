<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	function index()
	{
		$this->load->model('noticias_model', 'modelNoticias');
		$this->load->model('revistas_model', 'modelRevista');
		$this->load->model('destaques_model', 'modelDestaque');

		$this->load->model('imoveis_model', 'modelImoveis');
		
		$this->load->model('ofertas_model', 'modelOfertas');
		
		$ofertas = $this->modelOfertas->getOfertas();
		
		// $ip = $this->input->ip_address();

	 	// $arrEstados = array();
	 	// $arrCidades	= array();
	 	// $arrTipos 	= array(); 
	 	$arrImoveisBusca = array();

	 	foreach($this->modelImoveis->buscaTodosImoveis() as $row)
	 	{
			// if( ! isset($arrEstados[$row->id_estado])) { $arrEstados[$row->id_estado] = $row->estado; }

			// if( ! isset($arrCidades[$row->id_cidade]['id'])) 		{ $arrCidades[$row->id_cidade]['id'] 		= $row->id_cidade; }
			// if( ! isset($arrCidades[$row->id_cidade]['id_estado'])) { $arrCidades[$row->id_cidade]['id_estado'] = $row->id_estado; }
			// if( ! isset($arrCidades[$row->id_cidade]['cidade'])) 	{ $arrCidades[$row->id_cidade]['cidade'] 	= $row->cidade; }

			// if( ! isset($arrTipos[$row->id_tipo_imovel])) { $arrTipos[$row->id_tipo_imovel] = $row->tipo_imovel; }
			
			$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
			$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
			$url = site_url().'imoveis/'.@$url;
			
			$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
			$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
			$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);

			$imagem = base_url().'assets/site/img/bg-default.png';

			// $iv_file_headers = array();
			// $iv_file_headers[0] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo);
			// $iv_file_headers[1] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo);
			// $iv_file_headers[2] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo);

			if(count($fotos1) 	  > 0 ) $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo;
			elseif(count($fotos2) > 0 ) $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo;
			elseif(count($fotos3) > 0 ) $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo;

			// if(count($fotos1) > 0 && file_exists('uploads/imoveis/midias/'.$fotos1[0]->arquivo)) 	 {$imagem = base_url().'uploads/imoveis/midias/'.$fotos1[0]->arquivo;}
			// elseif(count($fotos2) > 0 && file_exists('uploads/imoveis/midias/'.$fotos2[0]->arquivo)) {$imagem = base_url().'uploads/imoveis/midias/'.$fotos2[0]->arquivo;}
			// elseif(count($fotos3) > 0 && file_exists('uploads/imoveis/midias/'.$fotos3[0]->arquivo)) {$imagem = base_url().'uploads/imoveis/midias/'.$fotos3[0]->arquivo;}

	 		array_push($arrImoveisBusca, array(
	 			'id' 		    => $row->id_empreendimento,
	 			'nome' 		  	=> $row->empreendimento,
	 			'chamada' 		=> $row->chamada_empreendimento,
	 			'chamada2'		=> $row->segunda_chamada_empreendimento,
	 			'url' 		  	=> $url,
	 			'lat' 		  	=> $row->latitude,
	 			'lon' 		  	=> $row->longitude,
	 			'pin' 		  	=> base_url().'uploads/imoveis/pin/'.$row->pin,
	 			'id_estado' 	=> $row->id_estado,
	 			'id_cidade' 	=> $row->id_cidade,
	 			'id_tipo' 		=> $row->id_tipo_imovel,
	 			'id_status' 	=> $row->id_status,
	 			'imagem'	  	=> $imagem,
	 			'cidade'	  	=> $row->cidade,
	 			'dormitorios' 	=> $row->dormitorio,
	 			'metragem'  	=> $row->metragem,
	 			'vagas'     	=> $row->vagas
	 		));
	 	}	

		$data = array(
			'page'			=> 'home',
			'destaqueHome'	=> $this->modelImoveis->getEmpreendimentosEmDestaque(),
			'destaques'		=> $this->modelDestaque->getDestaques(),
			'revistaAtual'	=> $this->modelRevista->getRevista(),
			'noticias' 		=> $this->modelNoticias->getNoticias(10),
			
			'visitados'		=> $this->imoveis_visitados->getImoveisVisitados(),
			'favoritos'		=> $this->imoveis_visitados->getImoveisFavoritos(),
			'mais_vistos'	=> $this->modelImoveis->getEmpreendimentosMaisVistos(),
			'ofertas'		=> $ofertas,
			'tipo_ofertas'	=> $this->modelOfertas->getTipos(),
			// 'arrEstados' 	=> $arrEstados,
			// 'arrCidades' 	=> $arrCidades,
			// 'arrTipos' 		=> $arrTipos,
			'arrImoveisBusca' => $arrImoveisBusca
		);

		// foreach ($arrCidades as $key => $value) {
		// 	var_dump($key);
		// 	var_dump($value);
		// }

		// exit();

		$this->load->view('home/home',$data);
	}
	


	function cadastraNewsletter(){
		if(!$this->agent->is_robot()){
			$this->load->model('contatos_model', 'model');
			$this->db->reconnect();
			
			@session_start();
				
			
			$cadastrado = $this->model->getNewsletter($this->input->post('email'));
			
			if(!$cadastrado){
				$data = array(
					'nome'				=> $this->input->post('nome'),
					'email'				=> $this->input->post('email'),
					'origem'			=> $_SESSION['origem'],
					'url'				  => $_SESSION['url'],
					'ip'				  => $this->input->ip_address(),
					'user_agent'	=> $this->input->user_agent()
				);	
				$this->model->setNewsletter($data);
			}else{
				echo 'cadastrado';
			}
			/*
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'mail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			//Inicio da Mensagem
				$nome				= $this->input->post('nome');
				$email				= $this->input->post('email');
				
				$data				= date("d/m/Y H:i:s");
			
			ob_start();
			
			?>
				<html>
					<head>
						<title>Nex Group</title>
					</head>
					<body>
						<table>
							<tr align="center">
					 			<td align="center" colspan="2">
					 				<a target="_blank" href="http://www.nexgroup.com.br">
					 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
					 				</a>
					 			</td>
							 </tr>
							<tr>
								<td colspan='2'>Cadastro Newsletter em <?php echo $data; ?></td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td>Nome:</td>
								<td><?php echo $nome; ?></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><?php echo $email; ?></td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
							</tr>
						</table>
					</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
		
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");
			$this->email->to('douglas.otto@divex.com');	
			
			$this->email->subject('Cadastro newsletter');
			$this->email->message("$conteudo");
			
			$this->email->send();*/
			
			//===================================================================
			//Termina o envio do email	
			
		}
	}
	

	function error(){
		$this->load->model('imoveis_model', 'model_imoveis');
		$this->db->reconnect();
		
		$tags = $this->model_imoveis->getTags(13);
	 		if($tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'NEX GROUP - 404';
	 	
		$data = array(
			'title'					  => $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,
			'page'					  => 'home',
		);
		
		$this->load->view('home/error',$data);
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */