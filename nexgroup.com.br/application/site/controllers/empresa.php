<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends CI_Controller {

	public function index()
	{
		$this->load->model('empresa_model', 'model');
		$this->load->model('imoveis_model', 'modelImoveis');


 		$arr_responsabilidades_fotos = array();

 		if(count($this->model->getResponsabilidadesSocialFotos()) > 0)
 		{
 			$i = 0;
 			foreach($this->model->getResponsabilidadesSocialFotos() as $foto)
 			{
 				$arr_responsabilidades_fotos[$foto->id_responsabilidade][$i]['id_foto'] 	= $foto->id_foto;
 				$arr_responsabilidades_fotos[$foto->id_responsabilidade][$i]['foto']		= $foto->foto;
 				$arr_responsabilidades_fotos[$foto->id_responsabilidade][$i]['legenda'] 	= $foto->legenda;
 				$i++;
 			}
 		}

 		$tags = $this->modelImoveis->getTags(3);

		$data = array(
			'title'				=> 'A Empresa - Nex Group',
			'keywords'			=> $tags->keywords,
			'description'		=> $tags->description,
			'page' 				=> 'empresa',
			'canonical' 		=> 'http://www.nexgroup.com.br/newnex/empresa',

			'visitados'			=> $this->imoveis_visitados->getImoveisVisitados(),
			
			'responsabilidades'			=> $this->model->getResponsabilidadesSocial(),
			'responsabilidades_fotos' 	=> $arr_responsabilidades_fotos,

			'obras_todas'		=> $this->modelImoveis->getEmpreendimentosByEmpresa(),
			'obras_capa'		=> $this->modelImoveis->getEmpreendimentosByEmpresa(1),
			'obras_dhz'			=> $this->modelImoveis->getEmpreendimentosByEmpresa(3),
			'obras_egl'			=> $this->modelImoveis->getEmpreendimentosByEmpresa(2),
			'obras_lomando'		=> $this->modelImoveis->getEmpreendimentosByEmpresa(4)
		);
		
		$this->load->view('empresa/empresa', $data);
	}

}