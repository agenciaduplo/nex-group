<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {

	public function index()
	{
		redirect('noticias/lista/');
	}
	
	function lista ($offset = "")
	{
		
		$this->load->model('noticias_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
		$this->load->model('imoveis_model', 'modelImoveis');
		
		$config = array (
			'uri_segment'		=> 2,
			'base_url'			=> 'http://www.nexgroup.com.br/noticias/',
			'total_rows'		=> $this->model->numNoticias(),
			'first_link'        => '<<',
			'last_link'			=> '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '5'
		);
		
		$this->pagination->initialize($config); 
		
			$tags = $this->modelImoveis->getTags(5);
	 		if($tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'Not&iacute;cias - NEX GROUP';
	 			
		$data = array(
			'title'					=> $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,
		
			'page'					=> 'noticias',
			
			'todas' 				=> $this->model->getNoticias(100),
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'noticias'  			=> $this->model->getListaNoticias($offset),
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'paginacao'	  			=> $this->pagination->create_links(),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('noticias/noticias',$data);
	}

	function rsslista ($offset = "")
	{
		
		$this->load->model('noticias_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
		$this->load->model('imoveis_model', 'modelImoveis');
		
		$tags = $this->modelImoveis->getTags(5);
 		if($tags->title)
 			$title = $tags->title;
 		else 	
 			$title = 'Not&iacute;cias Cotidiano - NEX GROUP';
	 			
		$data = array(
			'title'					=> $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,
		
			'page'					=> 'noticias',
			
			'ultimas' 				=> $this->model->getNoticias(5),
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'noticias'  			=> $this->model->getListaNoticias($offset),
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'paginacao'	  			=> $this->pagination->create_links(),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('noticias/noticias.rss.php',$data);
	}
	
	public function visualizar($id)
	{
		$this->load->model('noticias_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
	
		$noticia = $this->model->getNoticia($id);	
		$tags = explode(' ',$noticia->titulo);
		$tag = '';
		$excluir = array('a','e','em','do','de','da','um','como','é','ou','que','uma','para','no','na','se','até');
		foreach ($tags as $row):
			if(!in_array(strtolower($row), $excluir)){
				$tag .= $row.',';
			}
		endforeach;
		$data = array(
			'title'					=> "$noticia->titulo - Not&iacute;cias - NEX GROUP",
			'keywords'				=> $tag,
			'description'			=> $noticia->titulo,
			'page'					=> 'noticias',
			
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'noticia'  				=> $noticia,
			'galeria' 				=> $this->model->getImagens($id),
			'ultimas' 				=> $this->model->getNoticias(10),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('noticias/noticia',$data);
	}
	
	function publicacoes($mes,$offset = "")
	{
		$this->load->model('noticias_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
		$config = array (
			'base_url'			=> 'http://www.nexgroup.com.br/noticias/lista',
			'total_rows'		=> $this->model->numNoticias(),
			'first_link'        => '<<',
			'last_link'			=> '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '10'
		);
		
		
		$this->pagination->initialize($config); 
		
		$data = array(
			'title'					=> 'Not&iacute;cias - NEX GROUP',	
			'page'					=> 'noticias',
			
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'noticias'  			=> $this->model->getPublicacoesMes($mes,$offset),
			'paginacao'	  			=> $this->pagination->create_links(),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('noticias/noticias',$data);
	}
	
	function rss ($titleRss)
	{	
		$this->load->model('noticias_model', 'model');
		$this->load->model('revistas_model', 'modelRevista');
		
		$data = array (
			'title'					=> "Not&iacute;cias cotidiano - NEX GROUP",
			'page'					=> 'noticias',
			'tituloId'				=> $titleRss,
			
			'ultimas' 				=> $this->model->getNoticias(10),
			'revisaAtual'			=> $this->modelRevista->getRevista(),
			'publicacoes'			=> $this->model->getPublicacoes(),
			'blacklist'				=> $this->model->blacklist()
		);
		
		$this->load->view('noticias/noticia.rss.php', $data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */