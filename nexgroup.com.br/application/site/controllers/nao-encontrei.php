<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NaoEncontrei extends CI_Controller {

	public function index()
	{
		$data = array(
			'title'					=> 'Contato | Nexgroup',	
			
			'classeHome' 			=> '',
			'classeImoveis' 		=> '',
			'classeEmpresa' 		=> '',
			'classeNoticias' 		=> '',
			'classeCentral' 		=> '',
			'classeContato' 		=> ''
		);
		$this->load->view('nao-encontrei/nao-encontrei',$data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */