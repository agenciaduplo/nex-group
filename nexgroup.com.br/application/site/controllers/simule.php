<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simule extends CI_Controller {

	public function index(){
		if($this->input->post()){
			switch ($this->input->post('banco')){
				case 'Bradesco': redirect('http://www.bradescoimoveis.com.br/Site/SimuladorFinanciamento/SimuladorFinanciamento.aspx'); break;
				case 'Banrisul': redirect('http://www.banrisul.com.br/brz/link/brzw01hw.asp?css=bob&secao_id=1083'); break;
				case 'Caixa': redirect('http://www8.caixa.gov.br/siopiinternet/simulaOperacaoInternet.do?method=inicializarCasoUso'); break;
				case 'Santander': redirect('http://www.santander.com.br/portal/gsb/script/templates/GCMRequest.do?page=496'); break;
				case 'Bancodobrasil': redirect('https://www42.bb.com.br/portalbb/creditoImobiliario/Proposta,2,2250,2250.bbx'); break;
				default: redirect(); break;
			}
		} else {
			redirect();
		}
	}
}
/* End of file simule.php */
/* Location: ./application/controllers/simule.php */