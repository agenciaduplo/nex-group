<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Revista extends CI_Controller {

    public function index() {
        $this->load->model('revistas_model', 'model');
        $this->load->model('noticias_model', 'modelNoticias');
        $this->load->model('contatos_model', 'modelContato');

        $tags = $this->modelContato->getTags(18);
        if ($tags->title)
            $title = $tags->title;
        else
            $title = 'Revista NEX DAY - NEX GROUP';

        $data = array(
            'title' => $title,
            'keywords' => $tags->keywords,
            'description' => $tags->description,
            'page' => "revista",
            'revistaAtual' => $this->model->getRevista(),
            'outrasEdicoes' => $this->model->getRevistas(),
            'noticias' => $this->modelNoticias->getNoticias(3),
        );
        $this->load->view('revista/revista', $data);
    }

    function download($filename) {
        $this->load->helper('download');

        $data = file_get_contents("./uploads/revista/pdf/" . $filename);

        force_download($filename, $data);
    }

    function download2($filename) {

        define('DIR_DOWNLOAD', './uploads/revista/pdf/');

        $arquivo = $filename;

        if (isset($arquivo) && file_exists(DIR_DOWNLOAD . $arquivo)) {
            $arquivo = DIR_DOWNLOAD . $arquivo; 

            header('Content-Disposition: attachment; filename="'.$filename.'"');
//            header('Content-type: octet/stream');
            header("Content-type: application/pdf");
//            header("Content-Type: application/download");
//            header("Content-Description: File Transfer");
            header('Content-Length: ' . filesize($arquivo));
            readfile($arquivo);
        }
        exit;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */