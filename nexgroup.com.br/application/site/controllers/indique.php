<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indique extends CI_Controller {

	public function index()
	{
		$data = array(
			'title'					=> 'Contato - NEX GROUP',	
			
			'classeHome' 			=> '',
			'classeImoveis' 		=> '',
			'classeEmpresa' 		=> '',
			'classeNoticias' 		=> '',
			'classeCentral' 		=> '',
			'classeContato' 		=> ''
		);
		$this->load->view('indique/indique',$data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */