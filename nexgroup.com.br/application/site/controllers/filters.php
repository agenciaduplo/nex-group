<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filters extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	
	function atualizaFiltrosXHR()
	{
		$this->load->model('imoveis_model', 'model');

		$dados = $this->model->getEstadoCidadeByEmpreendimentoStatus($this->input->post('status'));

		$arr_estados = array();
		$arr_cidades = array();
		$arr_tipos 	 = array();

		foreach ($dados as $value)
		{
			if($value->tipo == 'estado')
			{
				$arr_estados[$value->id] = $value->nome;
			}
			elseif($value->tipo == 'cidade')
			{
				$arr_cidades[$value->id]['cidade'] 	  = $value->nome;
				$arr_cidades[$value->id]['id_estado'] = $value->outro;
			}
			elseif($value->tipo == 'tipo_imovel')
			{
				$arr_tipos[$value->id] = $value->nome;
			}
		}

		echo json_encode(array('estados' => $arr_estados, 'cidades' => $arr_cidades, 'tipos' => $arr_tipos));
	}

	
	function LoadSelectEstadosXHR()
	{
		$arr_estados = $this->input->post('estados');

		$html = '<option value="-">Filtrar por Estado:</option>';

		foreach($arr_estados as $key => $value)
		{
			$html .= '<option value="'.$key.'">'.$value.'</option>';
		}

		echo $html;
	}


	function LoadSelectCidadesXHR()
	{
		$arr_cidades = $this->input->post('cidades');

		$html = '<option value="-">Filtrar por Cidade: Selecione um estado.</option>';

		foreach($arr_cidades as $key => $value)
		{
			$html .= '<option class="cidades uf-'.$value['id_estado'].'" value="'.$key.'" style="display: none;">'.$value['cidade'].'</option>';
		}

		echo $html;
	}


	function LoadSelectTiposXHR()
	{
		$arr_tipos = $this->input->post('tipos');

		$html = '<option value="-">Filtrar por Tipo:</option>';

		foreach($arr_tipos as $key => $value)
		{
			$html .= '<option value="'.$key.'">'.$value.'</option>';
		}

		echo $html;
	}
	

}

/* End of file filters.php */
/* Location: ./application/controllers/filters.php */