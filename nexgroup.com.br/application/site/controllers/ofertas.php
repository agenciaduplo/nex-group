<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');

		$this->load->model('ofertas_model', 'model');
	}

	public function index()
	{
		if(!isset($_GET['dev']))
			header('Location:http://www.neximovel.com.br');
		
		$tags['keywords'] 	 = '';
		$tags['description'] = 'Página de ofertas da Nex.';

		$tags = (object) $tags;

		$title = 'Classificados Nex Vendas - NEX GROUP';
	 			
		$ofertas = $this->model->getOfertas();

		$data = array(
			'title'			=> $title,
			'keywords'		=> $tags->keywords,
			'description'	=> $tags->description,		
			'ofertas'		=> $ofertas,
			'tipo_ofertas'	=> $this->model->getTipos(),
			'page'			=> 'ofertas'
		);

		$data['visitados']	= $this->ofertas_visitadas->getOfertasVisitadas();
		$data['favoritos']	= $this->ofertas_visitadas->getOfertasFavoritadas();
		
		$this->load->view('ofertas/ofertas', $data);
	}

	function interna($url)
	{
		
		//header('Location:http://www.neximovel.com.br');
		
		$oferta = $this->model->getOfertByURL($url);

		if(count($oferta) != 1)
			show_404();

		if($this->ofertas_visitadas->gravaCookie($oferta->id_oferta))
		{
			$this->model->addVisita($oferta->id_oferta);
		}


		$favoritado = false;

		if(isset($_COOKIE['favorito_ofertas']))
		{
			$arr_fav = explode(',', $_COOKIE['favorito_ofertas']);

			if(in_array($oferta->id_oferta, $arr_fav)){
				$favoritado = true;
			}
		}
			
			

		$data = array();
		$data['title']			= $oferta->tag_title . ' - OFERTAS - NEX GROUP';
		$data['keywords']		= $oferta->tag_keywords;
		$data['description']	= $oferta->tag_description;
		$data['oferta']			= $oferta;
		$data['oferta_images']	= $this->model->getImagens($oferta->id_oferta);
		$data['favoritado']		= $favoritado;
		$data['page']			= 'ofertas';

		$data['visitados']		= $this->ofertas_visitadas->getOfertasVisitadas();
		$data['favoritos']		= $this->ofertas_visitadas->getOfertasFavoritadas();

		$this->load->view('ofertas/ofertas_interna', $data);
	}


	function favoritar($id_oferta)
	{
		$this->model->addFavorito($id_oferta);
		
		echo "true;";
		exit;
	}

	function removerFavorito()
	{
		if(isset($_COOKIE['favorito_ofertas']))
		{
			$arr_fav = explode(',', $_COOKIE['favorito_ofertas']);

			// echo "array normal <br/>";
			// var_dump($arr_fav); 

			$arr_fav = array_diff($arr_fav, (array) $this->input->post('id_oferta'));

			// echo "<br/>array novo <br/>";
			// var_dump($arr_fav); 

			if(count($arr_fav) >= 1) 
			{
				$arr_fav = implode(',', $arr_fav);
				echo json_encode(array('erro' => 0, 'favoritos' => $arr_fav));
				exit;
				// setcookie('favorito');
				// setcookie('favorito', $arr_fav, (time() + (30*24*60*60*1000)));
			}
			
			echo json_encode(array('erro' => 0, 'favoritos' => ''));
			exit;
			
			
			// echo "<br/><br/>" . $arr_fav;	
		}

		echo json_encode(array('erro' => 1));
		exit;
	}

}
/* End of file ofertas.php */
/* Location: ./application/controllers/ofertas.php */
