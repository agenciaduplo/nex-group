<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busca extends CI_Controller {

	public function index()
	{
		// $this->output->enable_profiler(true);

		// $sections = array(
		// 	'config'  => false,
		// 	'controller_info' => false,
		// 	'http_headers'=> false,
		// 	'queries' => true,
		// 	'get' => false,
		// 	'post' => false,
		// 	'uri_string' => false
		// );

		// $this->output->set_profiler_sections($sections);

		$this->load->model('busca_model','model');

		$arr_dados = array();

		if($this->input->get('q') && strlen($this->input->get('q')) > 0)
		{
			$arr_search = $this->model->executeSearch($this->input->get('q'));
			$arr_dados 	= $this->model->getLinks($arr_search);
		}
		
		$data = array(
			'title'	=> '"' . $this->input->get('q') . '" - Busca Nex Group',	
			'dados'	=> $arr_dados,
			'page'	=> 'busca'
		);

		$this->load->view('busca/busca', $data);
	}
	
}
/* End of file busca.php */
/* Location: ./application/controllers/busca.php */