<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interesse extends CI_Controller {

	public function index()
	{
		$data = array(
			'title'					=> 'Interesse - NEX GROUP',	
			
			'classeHome' 			=> '',
			'classeImoveis' 		=> '',
			'classeEmpresa' 		=> '',
			'classeNoticias' 		=> '',
			'classeCentral' 		=> '',
			'classeContato' 		=> ''
		);   
		$this->load->view('interesse/interesse',$data);
	}
	
	public function cadastraInteresse()
	{
		if (!$this->agent->is_robot()){
			$this->load->model('interesse_model','model');
			/*SENHAS EMPRESAS
			 * CAPA 			= }3_$%wB`7Atbdk1w
			 * EGL 				= `w~;ULm3CL{[AfV$	
			 * DHZ				= 7>k}m#AKZEXs|1{M	
			 * Lomando Aita		= HS^SM&*hoCV(dv)W	
			*/
			$data = $_POST;
			
			$senhas = array(
							'capa' 		=>"}3_$%wB`7Atbdk1w",
							'egl' 		=>"`w~;ULm3CL{[AfV$",
							'dhz' 		=>"7>k}m#AKZEXs|1{M",
							'Lomando'	=>"HS^SM&*hoCV(dv)W"
			);
			
			if($data['senha']){
				if($data['senha'] == $senhas['capa']){
					$empresa = 1;
				}elseif($data['senha'] == $senhas['egl']){
					$empresa = 2;
				}elseif($data['senha'] == $senhas['dhz']){
					$empresa = 3;
				}elseif($data['senha'] == $senhas['Lomando']){
					$empresa = 4;
				}
				
				$data['id_estado'] = 21;
				
				if($data['id_empreendimento']){
					$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
					if($empreendimento){
						$data['id_empreendimento']	= $empreendimento;
						unset( $data['senha']);
						unset( $data['emp']);
						
						$this->model->cadastraInteresse($data);
					}	
				}
				
			}
		}
		
	}
	
	
	public function cadastraIndique()
	{
		if (!$this->agent->is_robot()){
			$this->load->model('interesse_model','model');
			/*SENHAS EMPRESAS
			 * CAPA 			= }3_$%wB`7Atbdk1w
			 * EGL 				= `w~;ULm3CL{[AfV$	
			 * DHZ				= 7>k}m#AKZEXs|1{M	
			 * Lomando Aita		= HS^SM&*hoCV(dv)W	
			*/
			$data = $_POST;
			
			$senhas = array(
							'capa' 		=>"}3_$%wB`7Atbdk1w",
							'egl' 		=>"`w~;ULm3CL{[AfV$",
							'dhz' 		=>"7>k}m#AKZEXs|1{M",
							'Lomando'	=>"HS^SM&*hoCV(dv)W"
			);
			
			if($data['senha']){
				if($data['senha'] ==$senhas['capa']){
					$empresa = 1;
				}elseif($data['senha'] ==$senhas['egl']){
					$empresa = 2;
				}elseif($data['senha'] ==$senhas['dhz']){
					$empresa = 3;
				}elseif($data['senha'] ==$senhas['Lomando']){
					$empresa = 4;
				}
				if($data['id_empreendimento']){
					$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
					if($empreendimento){
						$data['id_empreendimento']	= $empreendimento;
						unset( $data['senha']);
						unset( $data['emp']);
						$this->model->cadastraIndique($data);
					}	
				}
				
			}
		}
		
	}
	
	public function cadastraContato()
	{
		if ($this->agent->is_robot()){
			$this->load->model('interesse_model','model');
			/*SENHAS EMPRESAS
			 * CAPA 			= }3_$%wB`7Atbdk1w
			 * EGL 				= `w~;ULm3CL{[AfV$	
			 * DHZ				= 7>k}m#AKZEXs|1{M	
			 * Lomando Aita		= HS^SM&*hoCV(dv)W	
			*/
			$data = $_POST;
			
			$senhas = array(
							'capa' 		=>"}3_$%wB`7Atbdk1w",
							'egl' 		=>"`w~;ULm3CL{[AfV$",
							'dhz' 		=>"7>k}m#AKZEXs|1{M",
							'Lomando'	=>"HS^SM&*hoCV(dv)W"
			);
			
			if($data['senha']){
				if($data['senha'] ==$senhas['capa']){
					$empresa = 1;
				}elseif($data['senha'] ==$senhas['egl']){
					$empresa = 2;
				}elseif($data['senha'] ==$senhas['dhz']){
					$empresa = 3;
				}elseif($data['senha'] ==$senhas['Lomando']){
					$empresa = 4;
				}
				
				
				if($data['id_empreendimento']){
					$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
					$estado = $this->model->buscaEstado($data['uf']);
					if($empreendimento){
						unset( $data['uf']);
						$data['id_estado']	= $estado;
						$data['id_empreendimento']	= $empreendimento;
						unset( $data['senha']);
						unset( $data['emp']);
						$this->model->cadastraContato($data);
					}	
				}
				
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */