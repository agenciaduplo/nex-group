<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maxhaus extends CI_Controller {
	
	public function index() {
		
		@session_start();
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				
				if(isset($_GET['devel'])){
					$ref[] = $_GET['utm_campaign'].'_'.$_GET['devel'];
				} else {
					$ref[] = $_GET['utm_campaign'];
				}
				
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			$_SESSION['MAXHAUS']['REFERER'] = $ref;
			
		} else if(isset($_SERVER['HTTP_REFERER'])) {
			
			if(isset($_GET['devel'])){
				$ref = $_SERVER['HTTP_REFERER'].'_'.$_GET['devel'];
			} else {
				$ref = $_SERVER['HTTP_REFERER'];
			}

			$_SESSION['MAXHAUS']['REFERER'] = $ref;
			
			
		} else {
			
			if(isset($_GET['devel'])){
				$ref = 'direto_'.$_GET['devel'];
			} else {
				$ref = 'direto';
			}
			
			$_SESSION['MAXHAUS']['REFERER'] = $ref;
		}
		
		$data = array(
			'title' => "Max Haus | NEX",
			'description' => "Mais do que pronto para morar, pronto para você realizar.",
			'keywords' => "Max haus, Max haus porto alegre, Max haus Nex, MAXKITCHEN, MAXKITCHEN, AUTOHAUS, apartamento alto padrão, apartamento, apartamento porto Alegre, Nex Group, elevadores",
			'ogurl' => 'http://www.nexgroup.com.br/max-haus',
			'ogimage' => 'maxhaus-cpc-2.jpg',
			'empClass' => "maxhaus",
			'pagedir' => "maxhaus",
			'UA' => "UA-69113303-5",
			'ref' => $ref
		);
		
		$this->load->view('maxhaus/index',$data);
		
	}
	
	public function envia(){
		
		header("Access-Control-Allow-Origin: http://nexgroup.com.br");
		
		if(!$this->agent->is_robot()){
			
			$this->load->model('contatos_model','model');
			//Inicia o envio do email
			//===================================================================

			$this->load->library('email');

			$config['protocol'] = 'sendmail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n";

			$this->email->initialize($config);

			@session_start();
			
			$nome  = $this->input->post('nome');
			$email = $this->input->post('email');
			$telefone   = $this->input->post('telefone');
			$msg   = $this->input->post('msg');
			$midia   = $this->input->post('ref');
			
			$data = array(
				'data_envio' 		  => date("Y-m-d H:i:s"),
				'nome'				  => mb_strtoupper($nome),
				'email'				  => mb_strtoupper($email),
				'comentarios'	 	  => mb_strtoupper($msg),
				'telefone'			  => $telefone,
				'id_empreendimento'	  => 82,
				'forma_contato'		  => "Email",
				'origem'			  => @$_SESSION['MAXHAUS']['REFERER'],
				'url'				  => $midia,
				'ip'				  => $this->input->ip_address(),
				'user_agent'		  => $this->input->user_agent(),
				'email_enviado_for'   => "vendas@nexvendas.com.br"
			);
			
			$this->model->setInteresse($data);
		
			ob_start();
			?>
			<html>
			<head>
				<title>Nex Group</title>
			</head>
			<body>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;">Enviado via Max Haus.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$nome?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$email?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$telefone?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Mensagem:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$msg?></span></td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
			</body>
			</html>
			<?
		
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");
			$this->email->to("vendas@nexvendas.com.br");
			$this->email->bcc("nexduplo@gmail.com");
			$this->email->subject('Novo contato Max Haus');
			$this->email->message("$conteudo");
			
			//===================================================================
			//Termina o envio do email	

			if($this->email->send()){
				$response = array("erro" => 0, "msg" => "sent");
			} else {
				$response = array("erro" => 1, "msg" => "not sent");
			}

			$this->load->library('curl');
			
			$this->curl->create("http://nex.hypnobox.com.br/email.receber.php");

			$post_data = array(
				"id_produto" => 26,
				"midia" => $midia, 
				"nome" 		 => $nome, 
				"email" 	 => strtolower($email),
				"ddd_residencial" => substr($telefone, 1, 2),
				"tel_residencial" => substr($telefone, 5),
				"mensagem"  => $msg,
				"assunto"	 => "Max Haus"
			);
			
			$this->curl->post($post_data);
			$res = $this->curl->execute();
			
			$response['curl_info'] = $this->curl->info; // array;
			$response['curl_response'] = $res; // array;
			echo json_encode($response);
			exit;
		}
	}
	
	public function response() {
		$this->load->view('maxhaus/response'); 
	}
}	 