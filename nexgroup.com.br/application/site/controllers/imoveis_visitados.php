

		<section class="vistos-home">
          <div class="row">

            <div class="top">
             <ul class="left">
               <li class="nomobile"><i class="icon icon-th"></i></li>
               <li>
                 <h5>IMÓVEIS QUE VOCÊ JÁ VIU</h5>
                 <h6><i>Os empreendimentos já visitados por você em nosso site</i></h6>
               </li>
             </ul >
              <ul class="right nomobile">
              	<?php $active = false;?>
              
                <?php if(count(@$visitados) > 0){ ?>
                	<?php $active = true;?>
                	<li class="active"><a href="javascript:void(0);" id="recentes" onclick="selecionarAba(this);">Mais recentes</a></li>
                <?php } ?>
    
                <?php if(!empty($favoritos) && count(@$favoritos) > 0){ ?>
                	<li <?php echo (!$active) ? 'class="active"' : '' ?>><a href="javascript:void(0);" id="favoritos" onclick="selecionarAba(this);">Favoritos</a></li>
                	<?php $active = true; ?>
                <?php } ?>
                
                <li <?php echo (!$active) ? 'class="active"' : '' ?>><a href="javascript:void(0);" id="mais" onclick="selecionarAba(this);" class="last">Mais visitados</a></li>
              </ul>
            </div>

            
            <?php $active = false;?>
            <div class="list_carousel">
	            <?php if(!empty($visitados) && count(@$visitados) > 0){ ?>
	            	<div id="tab-recentes" <?php echo ($active) ? 'style="display:none;"' : '' ?>>
		                <ul id="foo10">
		                	<?php $i=0; ?>
							<?php foreach (@$visitados as $row){ ?>
								<?php 
									$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
									$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
									$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;
									
									$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
									$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
									$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);
									if(count($fotos1) > 0 && file_exists('uploads/imoveis/midias/'.$fotos1[0]->arquivo)){
										$imagem = base_url().'uploads/imoveis/midias/'.$fotos1[0]->arquivo;
									}else if(count($fotos2) > 0 && file_exists('uploads/imoveis/midias/'.$fotos2[0]->arquivo)){
										$imagem = base_url().'uploads/imoveis/midias/'.$fotos2[0]->arquivo;
									}else if(count($fotos3) > 0 && file_exists('uploads/imoveis/midias/'.$fotos3[0]->arquivo)){
										$imagem = base_url().'uploads/imoveis/midias/'.$fotos3[0]->arquivo;
									}else{
										$imagem = base_url().'assets/site/img/bg-default.png';
									}
								?>
			                    <li class="box scales" style="background:url(<?php echo $imagem; ?>)"><!-- CARD -->
			                      <a href="<?php echo $url; ?>">
			                        <div class="left">
			                          <h2><?php echo @$row->empreendimento?></h2>
			                          <h3><i><?php echo @$row->chamada_empreendimento?></i></h3>
			                          <div href="<?php echo $url; ?>" class="btn btn-white button">Conheça</div>
			                        </div>
			                        <div class="pix"></div>
			                        <div class="black"></div>
			                      </a>
			                    </li>
			                    <?php $i++;?>
		                    <?php } ?>
		                    
		                    <?php for($j= $i; $j<5; $j++){?>
	                    		<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                    	<?php } ?>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="navegation">
		                    <a id="prev10" class="prev nomobile" href="#"></a>
		                    <div id="pager10" class="pager"></div>
		                    <a id="next10" class="next nomobile" href="#"></a>
		                </div>
	            	</div>
	            	<?php $active = true;?>
            	<?php } ?>
            	
            	
            	<?php if(!empty($mais_vistos) && count(@$mais_vistos) > 0){ ?>
	            	<div id="tab-mais" <?php echo ($active) ? 'style="display:none;"' : '' ?>>
		                <ul id="foo12">
		                	<?php $i=0; ?>
							<?php foreach (@$mais_vistos as $row){ ?>
								<?php 
									$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
									$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
									$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;
								?>
			                    <li class="box scales" style="background:url(<?=base_url()?>uploads/imoveis/fachada/visualizar/<?=@$row->imagem_fachada?>)"><!-- CARD -->
			                      <a href="<?php echo $url; ?>">
			                        <div class="left">
			                          <h2><?php echo @$row->empreendimento?></h2>
			                          <h3><i><?php echo @$row->chamada_empreendimento?></i></h3>
			                          <div href="<?php echo $url; ?>" class="btn btn-white button">Conheça</div>
			                        </div>
			                        <div class="pix"></div>
			                        <div class="black"></div>
			                      </a>
			                    </li>
			                    <?php $i++;?>
		                    <?php } ?>
		                    <?php for($j= $i; $j<5; $j++){?>
	                    		<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                    	<?php } ?>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="navegation">
		                    <a id="prev12" class="prev nomobile" href="#"></a>
		                    <div id="pager12" class="pager"></div>
		                    <a id="next12" class="next nomobile" href="#"></a>
		                </div>
	            	</div>
	            	<?php $active = true;?>
            	<?php } ?>
            	
            	
            	<?php if(!empty($favoritos) && count(@$favoritos) > 0){ ?>
	            	<div id="tab-favoritos" <?php echo ($active) ? 'style="display:none;"' : '' ?>>
		                <ul id="foo11">
		                	<?php $i=0; ?>
							<?php foreach (@$favoritos as $row){ ?>
								<?php 
									$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
									$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
									$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;
								?>
			                    <li class="box scales" style="background:url(<?=base_url()?>uploads/imoveis/fachada/visualizar/<?=@$row->imagem_fachada?>)"><!-- CARD -->
			                      <a href="<?php echo $url; ?>">
			                        <div class="left">
			                          <h2><?php echo @$row->empreendimento?></h2>
			                          <h3><i><?php echo @$row->chamada_empreendimento?></i></h3>
			                          <div href="<?php echo $url; ?>" class="btn btn-white button">Conheça</div>
			                        </div>
			                        <div class="pix"></div>
			                        <div class="black"></div>
			                      </a>
			                    </li>
			                    <?php $i++;?>
		                    <?php } ?>
		                    <?php for($j= $i; $j<5; $j++){?>
	                    		<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                    	<?php } ?>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="navegation">
		                    <a id="prev11" class="prev nomobile" href="#"></a>
		                    <div id="pager11" class="pager"></div>
		                    <a id="next11" class="next nomobile" href="#"></a>
		                </div>
	            	</div>
            	<?php } ?>
            </div>

          </div>
        </section>
