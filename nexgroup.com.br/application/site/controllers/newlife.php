<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newlife extends CI_Controller {

	public function index()
	{
		@session_start();

		$_SESSION['NEWLIFE']['REFERER'] = @$_SERVER['HTTP_REFERER'];	
		
		$this->load->view('newlife/index');
		
	}

	function newLifeEnviaInteresse() {
		if(!$this->agent->is_robot())
		{
			$nome	= $this->input->post('nome');
			$email	= $this->input->post('email');
			$tel	= $this->input->post('telefone');
			$cidade	= $this->input->post('cidade');
			$msg	= $this->input->post('msg');

			if(empty($nome)) {
				$response = array("erro" => 1, "msg" => "Preencha o campo nome!");
				echo json_encode($response);
				exit;
			}

			if(empty($email)) {
				$response = array("erro" => 1, "msg" => "Preencha o campo email!");
				echo json_encode($response);
				exit;
			}
			
			if(empty($tel)) {
				$response = array("erro" => 1, "msg" => "Preencha o campo telefone!");
				echo json_encode($response);
				exit;
			}


			if (!preg_match("/^[a-zA-ZãÃáÁàÀêÊéÉèÈíÍìÌôÔõÕóÓòÒúÚùÙûÛçÇºª ]+$/", $nome)) {
				$response = array("erro" => 1, "msg" => "Nome inválido, preencha o campo nome corretamente!");
				echo json_encode($response);
				exit;
			}


			$this->load->helper('email');

			if (!valid_email($email)) {
				$response = array("erro" => 1, "msg" => "Email inválido, preencha o campo email corretamente!");
				echo json_encode($response);
				exit;
			}


			if (!preg_match("/^\([0-9]{2}\) [0-9]{4}\-[0-9]{4}$/", $tel)) {
				$response = array("erro" => 1, "msg" => "Telefone inválido, preencha o campo nome corretamente!");
				echo json_encode($response);
				exit;
			}


			@session_start();

			$this->load->model('contatos_model','model');

			$data = array(
				'nome'				=> $nome,
				'email'				=> $email,
				'telefone'			=> $tel,
				'bairro_cidade'		=> $cidade,
				'comentarios'		=> $msg,
				'id_empreendimento'	=> 102,
				'id_estado'			=> 21,
				'forma_contato'		=> "Email",	
				'origem'			=> @$_SESSION['origem'],
				'url'				=> @$_SESSION['url'],
				'ip'				=> $this->input->ip_address(),
				'user_agent'		=> $this->input->user_agent()
				);

			$this->model->setInteresse($data);

			$response = array("erro" => 0);

			echo json_encode($response);
		}
	}

}

/* End of file newlife.php */
/* Location: ./application/controllers/newlife.php */