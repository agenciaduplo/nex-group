<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');

		$this->load->model('contatos_model', 'model');
		$this->load->library('user_agent');
	}

	public function index()
	{
		$this->load->model('contatos_model', 'model');
		$this->load->model('imoveis_model', 'modelImoveis');
		
		$tags = $this->model->getTags(6);
			if($tags->title)
				$title = $tags->title;
			else 	
				$title = 'Fale conosco - Contato - NEX GROUP';
				
		$data = array(
			'title'					=> $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,		
			'page'					=> 'contato',
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'estados'				=> $this->model->getEstados()
		);
		
		$this->load->view('contato/fale-conosco',$data);
	}
	
	// function visualizar_mapa()
	// {
	// 	$data = array(
	// 		'title'					=> 'Visualizar no mapa - NEX GROUP',		
	// 		'page'					=> 'contato'
	// 	);
	// 	$this->load->view('contato/mapa_nex');
	// }	


	// ==============================================================
	// ======================= XHR FUNCTIONS ========================
	// ==============================================================

	/**
	* Function getCidades()
	*
	* Retorna lista de cidades
	* Utilizado para popular campo <select>
	*
	* @return (array) (html)
	*/
	function getCidades($id_estado)
	{
		$this->load->model('contatos_model', 'model');
		
		$cidades = $this->model->getCidades($id_estado);
		foreach ($cidades as $cidade):
			echo "<option value='".$cidade->id_cidade."'>".$cidade->cidade."</option>";
		endforeach;
	}

	/**
	* Function ligamosParaVoceXHR()
	*
	* Envia formulário de atendimento (Modal Ligamos para você)
	* Registra novo atendimento no banco (tabela atendimentos)
	* Envia email de retorno para reponsável pelo atendimento da nex
	*
	* @return (json) ($response)
	*/
	function ligamosParaVoceXHR()
	{
		$this->load->library('user_agent');

		if ($this->agent->is_referral() && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{	
				header('Content-type: application/json');

				//	REGISTRO DE PEDIDO DE CONTATO (LIGAMOS PARA VOCÊ)
				//
				//  Validação do dados
				//===================================================================

				if($this->input->post('nome') == "")
				{
					echo json_encode(array("erro" => 1));
					exit;
				}

				//	Fim da validação
				//===================================================================
				//	Prepara os dados

				/*
				|--------------------------------------------------------------------------
				| URL de Origem
				|--------------------------------------------------------------------------
				*/

				$origem = $this->agent->referrer();

				if($origem)
				{
					$this->load->library('utilidades');

					if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
					{
						$origem = $this->input->post('refer'); 
					}
				}
				else
				{
					$origem = $this->input->post('refer');
				}

				/*
				|--------------------------------------------------------------------------
				| URL de Campanha
				|--------------------------------------------------------------------------
				*/	

				$camp = 1; // libera visualização da campanha no email

				$ex_url = explode("?", $origem);

				parse_str(@$ex_url[1], $output);

				$url  = 	   @$output['utm_source'];
				$url .= "__" . @$output['utm_medium'];
				$url .= "__" . @$output['utm_content'];
				$url .= "__" . @$output['utm_campaign'];

				if($url == "______" || $url == "")
				{
					$url 	= $this->input->post('refer');
					$origem = $this->agent->referrer();

					$camp = 0; // oculta visualização da campanha no email
				}

				/*
				|--------------------------------------------------------------------------
				| Descrição
				|--------------------------------------------------------------------------
				*/	

				$descricao = ($this->input->post('contate_me') == 1) ? "Contate-me Agora." : "Contate-me em outro horário.";

				if($this->input->post('horario') && $this->input->post('contate_me') == 2)
				{
					$descricao .= ' Horário para contato: ' . $this->input->post('horario');
				}

				//	Fim da preparação
				//===================================================================
				//	Efetua o registro na base

				$data = array();

				$data['data_envio']   		= date('Y-m-d H:i:s');
				$data['nome']				= $this->input->post('nome');
				$data['descricao']			= $descricao;
				$data['telefone']			= $this->input->post('telefone');
				$data['id_empreendimento']	= (int) $this->input->post('id_empreendimento');
				$data['forma_contato']		= 'Telefone';
				$data['origem']				= $origem;
				$data['url']				= $url;
				$data['ip']					= $this->input->ip_address();
				$data['user_agent']			= $this->input->user_agent();

				$this->model->setAtendimentoTelefone($data);  

				$empreendimento	= $this->model->getEmpreendimento($data['id_empreendimento']);

				//	Fim do registro na base, prepara para enviar email de retorno 
				//===================================================================
				//	
				//	EMAIL DE RETORNO
				//
				//	Configuração do email

				$this->load->library('email');
				$Email = new CI_Email();

				$config['protocol'] = 'mail';

				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['newline']  = "\r\n";
				$config['crlf'] 	= "\r\n";

				$Email->initialize($config);

				//	Fim da Configuração do email	
				//===================================================================
				//	Corpo do Email

				ob_start();
				?>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;">Ligamos para Você. Enviado via site.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
					</tr>
					<tr>
						<td><strong>Forma de Contato:</strong> Contato por telefone</td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
					</tr>
					<?php if(@$empreendimento->empreendimento) : ?>
					<tr>
						<td><strong>Empreendimento:</strong> <?=@$empreendimento->empreendimento?></td>
					</tr>
					<?php endif; ?>
					<?php if($camp == 1) : ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Origem via Campanha Digital:</strong><br/>
							<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px">
							<strong>URL da Campanha:</strong><br/>
							<?=$origem?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td style="padding-top:10px">
							<strong>Mensagem:</strong><br/> 
							<?=$data['descricao']?>
						</td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
				<?
				$conteudo = ob_get_contents();
				ob_end_clean();

				//	Fim Corpo do Email
				//===================================================================
				//	Recepientes

				$Email->from("noreply@nexgroup.com.br", "Nex Group");
				// $Email->to('gabriel.oribes@divex.com.br');	
				$Email->to('vendas@nexvendas.com.br');	
				$Email->bcc('gabriel.oribes@divex.com.br');
				$Email->subject('Ligamos para você');
				$Email->message("$conteudo");

				//	Fim Recepientes  
				//===================================================================
				//	Envia Email

				if(@$Email->send())
				{
					$response = array('erro' => 0); // Success
				}
				else
				{
					$response = array('erro' => 101); // Fail
				}

				//  Termina o envio do email  
				//===================================================================

				echo json_encode($response);
				exit;				
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}
	}

	/**
	* Function getEmpreendimentosXHR()
	*
	* Retorna lista de empreedimentos ativos 
	* Utilizado para popular campo <select>
	*
	* @return (array) ($data) ($id_empreendimento, $empreendimento)
	*/
	function getEmpreendimentosXHR()
	{
		if ($this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{
				$this->load->model('imoveis_model', 'imoveis');

				$data = array();
				$data['empreendimentos'] = $this->imoveis->getEmpreendimentoToSelect();

				$this->load->view('loads/load-empreendimentos-select', $data);
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is XHR! Access Fail.
			show_404();
			exit;
		}
	}

	/**
	* Function entreEmContatoXHR()
	*
	* Envia formulário de atendimento (Modal Entre em Contato)
	* Registra novo atendimento no banco (tabela atendimentos)
	* Opção de cadastrar novo registro em newsletter
	* Envia email de retorno para reponsável pelo atendimento da nex
	*
	* @return (json) ($response)
	*/
	function entreEmContatoXHR()
	{
		$this->load->library('user_agent');

		if ($this->agent->is_referral() && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{
				header('Content-type: application/json');

				//	REGISTRO DE CONTATO (ENTRE EM CONTATO)
				//
				//  Validação do dados
				//===================================================================

				$this->load->helper('email');

				if($this->input->post('nome') == "")
				{
					echo json_encode(array("erro" => 1));
					exit;
				}

				if(!valid_email($this->input->post('email')))
				{
					echo json_encode(array("erro" => 2));
					exit;
				}

				if($this->input->post('telefone') == "" || $this->input->post('telefone') == "(__) ____-_____")
				{
					echo json_encode(array("erro" => 3));
					exit;
				}

				if(strlen($this->input->post('mensagem')) < 20)
				{
					echo json_encode(array("erro" => 4));
					exit;
				}

				//	Fim da validação
				//===================================================================
				//	Prepara os dados
			
				/*
				|--------------------------------------------------------------------------
				| URL de Origem
				|--------------------------------------------------------------------------
				*/

				$origem = $this->agent->referrer();

				if($origem)
				{
					$this->load->library('utilidades');

					if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
					{
						$origem = $this->input->post('refer'); 
					}
				}
				else
				{
					$origem = $this->input->post('refer');
				}

				/*
				|--------------------------------------------------------------------------
				| URL de Campanha
				|--------------------------------------------------------------------------
				*/	

				$camp = 1; // libera visualização da campanha no email

				$ex_url = explode("?", $origem);

				parse_str(@$ex_url[1], $output);

				$url  = 	   @$output['utm_source'];
				$url .= "__" . @$output['utm_medium'];
				$url .= "__" . @$output['utm_content'];
				$url .= "__" . @$output['utm_campaign'];

				if($url == "______" || $url == "")
				{
					$url 	= $this->input->post('refer');
					$origem = $this->agent->referrer();

					$camp = 0; // oculta visualização da campanha no email
				}

				/*
				|--------------------------------------------------------------------------
				| Forma do Contato
				|--------------------------------------------------------------------------
				*/

				$fc = ($this->input->post('contato_tipo') == 2) ? 'Telefone' : 'E-mail';

				//	Fim da preparação
				//===================================================================
				//	Efetua o registro na base

				$data = array();
				$data['data_envio']   		= date('Y-m-d H:i:s');
				$data['nome']				= $this->input->post('nome');
				$data['email']				= $this->input->post('email');
				$data['telefone']			= $this->input->post('telefone');
				$data['forma_contato']		= $fc;
				$data['descricao']			= $this->input->post('mensagem');
				$data['id_empreendimento']	= (int) $this->input->post('id_empreendimento');
				$data['origem']				= $origem;
				$data['url']				= $url;
				$data['ip']					= $this->input->ip_address();
				$data['user_agent']			= $this->input->user_agent();

				$this->model->setAtendimento($data);  

				if($this->input->post('newsletter') && $this->input->post('newsletter') == 'S')
				{
					if(!$this->model->getNewsletter($data['email']))
					{
						$data_news = array();
						$data_news['nome'] 			= $data['nome'];	
						$data_news['email'] 		= $data['email'];	
						$data_news['data_cadastro'] = $data['data_envio'];	
						$data_news['ip'] 			= $data['ip'];	
						$data_news['url'] 			= $data['url'];	
						$data_news['origem'] 		= $data['origem'];	
						$data_news['user_agent'] 	= $data['user_agent'];

						$this->model->setNewsletter($data_news);
					}					
				}

				$empreendimento = false;

				if($data['id_empreendimento'] > 0)
					$empreendimento	= $this->model->getEmpreendimento($data['id_empreendimento']);

				//	Fim do registro na base, prepara para enviar email de retorno 
				//===================================================================
				//	
				//	EMAIL DE RETORNO
				//
				//	Configuração do email

				$this->load->library('email');
				$Email = new CI_Email();

				$config['protocol'] = 'mail';

				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['newline']  = "\r\n";
				$config['crlf'] 	= "\r\n";

				$Email->initialize($config);

				//	Fim da Configuração do email	
				//===================================================================
				//	Corpo do Email

				ob_start();
				?>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;">Atendimento por E-mail. Enviado via site.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Forma de Contato:</strong> <?=$fc?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['email']?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
					</tr>
					<?php if (@$empreendimento->empreendimento) : ?>
					<tr>
						<td style="padding-top:5px"><strong>Empreendimento de Interesse:</strong> <span style="font-size: 16px;"><?=@$empreendimento->empreendimento?></span></td>
					</tr>
					<?php endif; ?>
					<?php if($camp == 1) : ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Origem via Campanha Digital:</strong><br/>
							<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px">
							<strong>URL da Campanha:</strong><br/>
							<?=$origem?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Mensagem:</strong><br/> 
							<?=nl2br($data['descricao'])?>
						</td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
				<?
				$conteudo = ob_get_contents();
				ob_end_clean();

				//	Fim Corpo do Email
				//===================================================================
				//	Recepientes

				// @$emails = $this->model->getEnviaEmail((int) @$empreendimento->id_empreendimento, 1);

				// if(@$emails)
				// {
				// 	$list     = array();
				// 	$list_cc  = array();
				// 	$list_bcc = array();

				// 	foreach ($emails as $email)
				// 	{
				// 		if($email->tipo == 'para')
				// 		{
				// 			$list[] = $email->email;
				// 		}
				// 		else if($email->tipo == 'cc')
				// 		{	
				// 			$list_cc[] = $email->email;
				// 		}
				// 		else
				// 		{
				// 			$list_bcc[] = $email->email;
				// 		}
				// 	}

				// 	if($list)
				// 		$Email->to($list);
					
				// 	if($list_cc)
				// 		$Email->cc($list_cc);
					
				// 	if($list_bcc)
				// 		$Email->bcc($list_bcc);	
				// }
				// else
				// {	
				// 	$Email->to('mellobandeira@yahoo.com.br');	
				// 	$Email->to('saldanha@capa.com.br');	
				// 	$Email->bcc('gabriel.oribes@divex.com.br');
				// }

				$Email->from("noreply@nexgroup.com.br", "Nex Group");
				$Email->to('vendas@nexvendas.com.br');	
				$Email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br');
				$Email->subject('Atendimento por E-mail');
				$Email->message("$conteudo");

				//	Fim Recepientes  
				//===================================================================
				//	Envia Email

				if(@$Email->send())
				{
					$response = array('erro' => 0); // Success
				}
				else
				{
					$response = array('erro' => 101); // Fail
				}

				//  Termina o envio do email  
				//===================================================================

				echo json_encode($response);
				exit;				
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}	
	}


	/**
	* Function interesseNoEmpreendimentoXHR()
	*
	* Envia formulário de interesse (Modal Tenho Interesse)
	* Registra novo interesse em empreendimento no banco (tabela interesses)
	* Opção de cadastrar novo registro em newsletter
	* Envia email de retorno para reponsável pelo atendimento da nex (roleta)
	*
	* @return (json) ($response)
	*/
	function interesseNoEmpreendimentoXHR()
	{
		if ($this->agent->is_referral() && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{
				header('Content-type: application/json');

				//	REGISTRO DE INTERESSE NO EMPREENDIMENTO
				//
				//  Validação do dados
				//===================================================================

				$this->load->helper('email');

				if($this->input->post('nome') == "")
				{
					echo json_encode(array("erro" => 1));
					exit;
				}

				if(!valid_email($this->input->post('email')))
				{
					echo json_encode(array("erro" => 2));
					exit;
				}

				if($this->input->post('telefone') == "" || $this->input->post('telefone') == "(__) ____-_____")
				{
					echo json_encode(array("erro" => 3));
					exit;
				}

				//	Fim da validação
				//===================================================================
				//	Prepara os dados
			
				/*
				|--------------------------------------------------------------------------
				| URL de Origem
				|--------------------------------------------------------------------------
				*/

				$origem = $this->agent->referrer();

				if($origem)
				{
					$this->load->library('utilidades');

					if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
					{
						$origem = $this->input->post('refer'); 
					}
				}
				else
				{
					$origem = $this->input->post('refer');
				}

				/*
				|--------------------------------------------------------------------------
				| URL de Campanha
				|--------------------------------------------------------------------------
				*/	

				$camp = 1; // libera visualização da campanha no email

				$ex_url = explode("?", $origem);

				parse_str(@$ex_url[1], $output);

				$url  = 	   @$output['utm_source'];
				$url .= "__" . @$output['utm_medium'];
				$url .= "__" . @$output['utm_content'];
				$url .= "__" . @$output['utm_campaign'];

				if($url == "______" || $url == "")
				{
					$url 	= $this->input->post('refer');
					$origem = $this->agent->referrer();

					$camp = 0; // oculta visualização da campanha no email
				}

				/*
				|--------------------------------------------------------------------------
				| Forma do Contato
				|--------------------------------------------------------------------------
				*/

				$fc = ($this->input->post('contato_tipo') == 2) ? 'Telefone' : 'E-mail';

				/*
				|--------------------------------------------------------------------------
				| Roleta de Emails Nex
				|--------------------------------------------------------------------------
				*/

				$email_nex    = "vendas@nexvendas.com.br";
      			$email_out    = "lancamentos@vendasbbsul.com.br";
				$email_go     = $email_out;

				$id_empresa   = $this->model->getIdEmpresa($this->input->post('id_empreendimento'));
				$id_empresa   = $id_empresa[0]->id_empresa;

				$id_cidade    = $this->model->getIdCidade($this->input->post('id_empreendimento'));
				$id_cidade    = $id_cidade[0]->id_cidade;
	 
				if($this->input->post('id_empreendimento') == 102 || $this->input->post('id_empreendimento') == 104) // exclusão new life e FWD
				{
					$email_go = $email_nex;
				}
				elseif($this->input->post('id_empreendimento') == 84) // amores de brava
				{
					$email_go  = "bonato@amoresdabrava.com.br,";
					$email_go .= "tassia@aldeia.biz,";
					$email_go .= "natalia@aldeia.biz,";
					$email_go .= "cristiane.gandini@nexgroup.com.br,";
					$email_go .= "felipe@taroii.com.br";
				}
				else
				{
					$email_for = $this->model->verificaLastEmailSendFor(3);

					if(@$output['utm_source'] != "" && @$output['utm_medium'] != "" && @$output['utm_campaign'] != "")
					{ 
						if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7)) // verifica se a cidade é porto alegre e que as empresas NÃO SÃO "Capamax Nex Group" e "Capamax" 
						{
							$email_for =  $this->model->verificaLastEmailSendFor(1, "interesses", $url);

							foreach($email_for as $emf)
							{
								if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == "")
								{
									$email_go = $email_out;
									break;
								}
								else
								{
									$email_go = $email_nex;
									break;
								}
							}
						}
						else
						{
							$email_go = $email_nex;
						}
					}
					else if($id_cidade == 4237 && ($id_empresa != 5 && $id_empresa != 7))
					{
						$i     = 0;
						$flag  = false;

						foreach($email_for as $emf)
						{
							++$i;
							if($emf->email_enviado_for == $email_out)
							{
								$email_go = $email_nex;
								break;
							}
						}
					}
					else
					{
						$email_go = $email_nex;
					}
				}

				//	Fim da preparação
				//===================================================================
				//	Efetua o registro na base

				$data = array();
				$data['data_envio']   		= date('Y-m-d H:i:s');
				$data['nome']				= $this->input->post('nome');
				$data['email']				= $this->input->post('email');
				$data['telefone']			= $this->input->post('telefone');
				$data['forma_contato']		= $fc;
				$data['comentarios']		= $this->input->post('mensagem');
				$data['id_empreendimento']	= (int) $this->input->post('id_empreendimento');
				$data['origem']				= $origem;
				$data['url']				= $url;
				$data['ip']					= $this->input->ip_address();
				$data['user_agent']			= $this->input->user_agent();
				$data['email_enviado_for']	= $email_go;

				$interesse = $this->model->setInteresse($data);  

				if($this->input->post('newsletter') && $this->input->post('newsletter') == 'S')
				{
					if(!$this->model->getNewsletter($data['email']))
					{
						$data_news = array();
						$data_news['nome'] 			= $data['nome'];	
						$data_news['email'] 		= $data['email'];	
						$data_news['data_cadastro'] = $data['data_envio'];	
						$data_news['ip'] 			= $data['ip'];	
						$data_news['url'] 			= $data['url'];	
						$data_news['origem'] 		= $data['origem'];	
						$data_news['user_agent'] 	= $data['user_agent'];

						$this->model->setNewsletter($data_news);
					}					
				}

				$empreendimento	= $this->model->getEmpreendimento($data['id_empreendimento']);

				//	Fim do registro na base, prepara para enviar email de retorno 
				//===================================================================
				//	
				//	EMAIL DE RETORNO
				//
				//	Configuração do email

				$this->load->library('email');
				$Email = new CI_Email();

				$config['protocol'] = 'mail';

				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['newline']  = "\r\n";
				$config['crlf'] 	= "\r\n";

				$Email->initialize($config);

				//	Fim da Configuração do email	
				//===================================================================
				//	Corpo do Email

				ob_start();
				?>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;">Contato de Interesse. Enviado via site.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Forma de Contato:</strong> <?=$fc?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['email']?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
					</tr>
					<?php if (@$empreendimento->empreendimento) : ?>
					<tr>
						<td style="padding-top:5px"><strong>Empreendimento de Interesse:</strong> <span style="font-size: 16px;"><?=@$empreendimento->empreendimento?></span></td>
					</tr>
					<?php endif; ?>
					<?php if($camp == 1) : ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Origem via Campanha Digital:</strong><br/>
							<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px">
							<strong>URL da Campanha:</strong><br/>
							<?=$origem?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Mensagem:</strong><br/> 
							<?=nl2br($data['comentarios'])?>
						</td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
				<?
				$conteudo = ob_get_contents();
				ob_end_clean();

				//	Fim Corpo do Email
				//===================================================================
				//	Recepientes



				$grupos = $this->model->getGrupos($empreendimento->id_empreendimento);
				// $total = 0;
				$total = count($grupos);

				$emails_txt = "";

				if($total == 1)
				{
					$emails = $this->model->getGruposEmails($empreendimento->id_empreendimento, '001');

					$emails_txt = "";

					foreach ($emails as $email)
					{
						$grupo = $email->grupo;

						$list[] = $email->email;

						$emails_txt = $emails_txt . $email->email . ", ";
					}

					$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
				}
				else if($total > 1)
				{
					foreach ($grupos as $row)
					{
						if($row->rand == 1)
						{
							$atual = $row->ordem;

							if($atual == $total)
							{
								$this->model->updateRand($empreendimento->id_empreendimento, '001');

								$emails = $this->model->getGruposEmails($empreendimento->id_empreendimento, '001');

								$emails_txt = "";

								foreach ($emails as $email)
								{
									$grupo = $email->grupo;

									$list[] = $email->email;

									$emails_txt = $emails_txt . $email->email . ", ";
								}

								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
							}
							else
							{
								$atualizar = "00" . $atual + 1;

								$this->model->updateRand($empreendimento->id_empreendimento, $atualizar);

								$emails = $this->model->getGruposEmails($empreendimento->id_empreendimento, $atualizar);

								$emails_txt = "";

								foreach ($emails as $email)
								{
									$grupo = $email->grupo;

									$list[] = $email->email;

									$emails_txt = $emails_txt . $email->email . ", ";
								}

								$this->model->grupo_interesse($interesse, $grupo, $emails_txt);
							}
						}
					}
				}

				$empNome = strtoupper($empreendimento->empreendimento);

				if($email_go == $email_out)
				{
					$Email->to($email_go);
				}
				else
				{
					$emails_txt = ($emails_txt != "") ? $emails_txt."," : "";

					$Email->to($emails_txt . "{$email_go}");
				}
					
				// $copia_oculta = array("ricardo@divex.com.br", "gabriel.oribes@divex.com.br");
				// $Email->bcc($copia_oculta);

				$Email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br, ricardo@divex.com.br');

				$Email->from("noreply@nexgroup.com.br", "Nex Group");
				$Email->subject('Interesse enviado via Portal - Empreendimento ' . $empNome);
				$Email->message("$conteudo");

				//	Fim Recepientes  
				//===================================================================
				//	Envia Email

				if(@$Email->send())
				{
					$response = array('erro' => 0); // Success
				}
				else
				{
					$response = array('erro' => 101); // Fail
				}

				//  Termina o envio do email  
				//===================================================================

				echo json_encode($response);
				exit;	
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}	
	}


	/**
	* Function ContatoClienteXHR()
	*
	* Envia formulário de atendimento (Modal Atendimento Cliente Nex)
	* Registra novo contato no banco (tabela contatos)
	* Envio de retorno com condicional (Atualização de Cadastro, Informações Financeiras, Pagamentos e Boletos, Elogios, Sugestões e Reclamações)
	* Opção de cadastrar novo registro em newsletter
	*
	* @return (json) ($response)
	*/
	function ContatoClienteXHR()
	{
		if (($this->agent->is_referral() && $this->agent->referrer() == base_url() . 'contato') && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{

				header('Content-type: application/json');

				//	REGISTRO DE CONTATO (ENTRE EM CONTATO)
				//
				//  Validação do dados
				//===================================================================

				$this->load->helper('email');

				if($this->input->post('nome') == "")
				{
					echo json_encode(array("erro" => 1));
					exit;
				}

				if(!valid_email($this->input->post('email')))
				{
					echo json_encode(array("erro" => 2));
					exit;
				}

				if($this->input->post('telefone') == "" || $this->input->post('telefone') == "(__) ____-_____")
				{
					echo json_encode(array("erro" => 3));
					exit;
				}

				//	Fim da validação
				//===================================================================
				//	Prepara os dados
			
				/*
				|--------------------------------------------------------------------------
				| URL de Origem
				|--------------------------------------------------------------------------
				*/

				$origem = $this->agent->referrer();

				if($origem)
				{
					$this->load->library('utilidades');

					if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
					{
						$origem = $this->input->post('refer'); 
					}
				}
				else
				{
					$origem = $this->input->post('refer');
				}

				/*
				|--------------------------------------------------------------------------
				| URL de Campanha
				|--------------------------------------------------------------------------
				*/	

				$camp = 1; // libera visualização da campanha no email

				$ex_url = explode("?", $origem);

				parse_str(@$ex_url[1], $output);

				$url  = 	   @$output['utm_source'];
				$url .= "__" . @$output['utm_medium'];
				$url .= "__" . @$output['utm_content'];
				$url .= "__" . @$output['utm_campaign'];

				if($url == "______" || $url == "")
				{
					$url 	= $this->input->post('refer');
					$origem = $this->agent->referrer();

					$camp = 0; // oculta visualização da campanha no email
				}

				/*
				|--------------------------------------------------------------------------
				| Forma do Contato
				|--------------------------------------------------------------------------
				*/

				$fc = ($this->input->post('contato_tipo') == 2) ? 'Telefone' : 'E-mail';

				//	Fim da preparação
				//===================================================================
				//	Efetua o registro na base

				$data = array();
				$data['data_envio']   		= date('Y-m-d H:i:s');
				$data['nome']				= $this->input->post('nome');
				$data['email']				= $this->input->post('email');
				$data['telefone']			= $this->input->post('telefone');
				$data['forma_contato']		= $fc;
				$data['comentarios']		= $this->input->post('mensagem');
				$data['origem']				= $origem;
				$data['url']				= $url;
				$data['ip']					= $this->input->ip_address();
				$data['user_agent']			= $this->input->user_agent();

				$this->model->setFaleConosco($data);  

				if($this->input->post('newsletter') && $this->input->post('newsletter') == 'S')
				{
					if(!$this->model->getNewsletter($data['email']))
					{
						$data_news = array();
						$data_news['nome'] 			= $data['nome'];	
						$data_news['email'] 		= $data['email'];	
						$data_news['data_cadastro'] = $data['data_envio'];	
						$data_news['ip'] 			= $data['ip'];	
						$data_news['url'] 			= $data['url'];	
						$data_news['origem'] 		= $data['origem'];	
						$data_news['user_agent'] 	= $data['user_agent'];

						$this->model->setNewsletter($data_news);
					}					
				}

				//	Fim do registro na base, prepara para enviar email de retorno 
				//===================================================================
				//	
				//	EMAIL DE RETORNO
				//
				//	Configuração do email

				$this->load->library('email');
				$Email = new CI_Email();

				$config['protocol'] = 'mail';

				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['newline']  = "\r\n";
				$config['crlf'] 	= "\r\n";

				$Email->initialize($config);

				//	Fim da Configuração do email	
				//===================================================================
				//	Corpo do Email

				$mail_to = '';
				$mail_subject = '';

				switch ($this->input->post('setor')) {
					case 1:
						$mail_to = 'relacionamento@nexgroup.com.br';
						$mail_subject = 'Dados Cadastrais';
						break;
					case 2:
						$mail_to = 'relacionamento@nexgroup.com.br';
						$mail_subject = 'Informações Financeiras';
						break;
					case 3:	
						$mail_to = 'relacionamento@nexgroup.com.br';	
						$mail_subject = 'Pagamentos e 2° via de boletos';
						break;
					case 4:
						$mail_to = 'relacionamento@nexgroup.com.br';	
						$mail_subject = 'Elogios, sugestões e reclamações';
						break;
					default:
						$mail_to = 'relacionamento@nexgroup.com.br';			
						$mail_subject = 'Outros';	
						break;
				}

				ob_start();
				?>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;"><?=$mail_subject?>. Enviado via site.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Forma de Contato:</strong> <?=$fc?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['email']?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
					</tr>
					<?php if($camp == 1) : ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Origem via Campanha Digital:</strong><br/>
							<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px">
							<strong>URL da Campanha:</strong><br/>
							<?=$origem?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Mensagem:</strong><br/> 
							<?=nl2br($data['comentarios'])?>
						</td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
				<?
				$conteudo = ob_get_contents();
				ob_end_clean();

				//	Fim Corpo do Email
				//===================================================================
				//	Recepientes

				mb_convert_encoding($mail_subject, "UTF-8");

				$Email->to($mail_to);	
				$Email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br');
				$Email->subject('Atendimento Cliente Nex - ' . $mail_subject);
				$Email->from("noreply@nexgroup.com.br", "Nex Group");
				$Email->message("$conteudo");

				//	Fim Recepientes  
				//===================================================================
				//	Envia Email

				if(@$Email->send())
				{
					$response = array('erro' => 0); // Success
				}
				else
				{
					$response = array('erro' => 101); // Fail
				}

				//  Termina o envio do email  
				//===================================================================

				echo json_encode($response);
				exit;	
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}
	}


	/**
	* Function atendimentoNexXHR()
	*
	* Envia formulário de atendimento (Modal Atendimento Nex Group)
	* Registra novo contato no banco (tabela contatos)
	* Envio de retorno com condicional (Seja um Fornecedor, Venda seu Terreno ou Sou um Corretor e Quero Vender Nex)
	* Opção de cadastrar novo registro em newsletter
	*
	* @return (json) ($response)
	*/
	function atendimentoNexXHR()
	{
		if (($this->agent->is_referral() && $this->agent->referrer() == base_url() . 'contato') && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{
				header('Content-type: application/json');

				//	REGISTRO DE CONTATO (ENTRE EM CONTATO)
				//
				//  Validação do dados
				//===================================================================

				$this->load->helper('email');

				if($this->input->post('nome') == "")
				{
					echo json_encode(array("erro" => 1));
					exit;
				}

				if(!valid_email($this->input->post('email')))
				{
					echo json_encode(array("erro" => 2));
					exit;
				}

				if($this->input->post('telefone') == "" || $this->input->post('telefone') == "(__) ____-_____")
				{
					echo json_encode(array("erro" => 3));
					exit;
				}

				//	Fim da validação
				//===================================================================
				//	Prepara os dados
			
				/*
				|--------------------------------------------------------------------------
				| URL de Origem
				|--------------------------------------------------------------------------
				*/

				$origem = $this->agent->referrer();

				if($origem)
				{
					$this->load->library('utilidades');

					if($this->utilidades->valid_url_format($this->input->post('refer')) && $this->input->post('refer') != $origem)
					{
						$origem = $this->input->post('refer'); 
					}
				}
				else
				{
					$origem = $this->input->post('refer');
				}

				/*
				|--------------------------------------------------------------------------
				| URL de Campanha
				|--------------------------------------------------------------------------
				*/	

				$camp = 1; // libera visualização da campanha no email

				$ex_url = explode("?", $origem);

				parse_str(@$ex_url[1], $output);

				$url  = 	   @$output['utm_source'];
				$url .= "__" . @$output['utm_medium'];
				$url .= "__" . @$output['utm_content'];
				$url .= "__" . @$output['utm_campaign'];

				if($url == "______" || $url == "")
				{
					$url 	= $this->input->post('refer');
					$origem = $this->agent->referrer();

					$camp = 0; // oculta visualização da campanha no email
				}

				/*
				|--------------------------------------------------------------------------
				| Forma do Contato
				|--------------------------------------------------------------------------
				*/

				$fc = ($this->input->post('contato_tipo') == 2) ? 'Telefone' : 'E-mail';

				//	Fim da preparação
				//===================================================================
				//	Efetua o registro na base

				$data = array();
				$data['data_envio']   		= date('Y-m-d H:i:s');
				$data['nome']				= $this->input->post('nome');
				$data['email']				= $this->input->post('email');
				$data['telefone']			= $this->input->post('telefone');
				$data['forma_contato']		= $fc;
				$data['comentarios']		= $this->input->post('mensagem');
				$data['origem']				= $origem;
				$data['url']				= $url;
				$data['ip']					= $this->input->ip_address();
				$data['user_agent']			= $this->input->user_agent();

				$this->model->setFaleConosco($data);  

				if($this->input->post('newsletter') && $this->input->post('newsletter') == 'S')
				{
					if(!$this->model->getNewsletter($data['email']))
					{
						$data_news = array();
						$data_news['nome'] 			= $data['nome'];	
						$data_news['email'] 		= $data['email'];	
						$data_news['data_cadastro'] = $data['data_envio'];	
						$data_news['ip'] 			= $data['ip'];	
						$data_news['url'] 			= $data['url'];	
						$data_news['origem'] 		= $data['origem'];	
						$data_news['user_agent'] 	= $data['user_agent'];

						$this->model->setNewsletter($data_news);
					}					
				}

				//	Fim do registro na base, prepara para enviar email de retorno 
				//===================================================================
				//	
				//	EMAIL DE RETORNO
				//
				//	Configuração do email

				$this->load->library('email');
				$Email = new CI_Email();

				$config['protocol'] = 'mail';

				$config['mailtype'] = 'html';
				$config['charset'] 	= 'utf-8';
				$config['newline']  = "\r\n";
				$config['crlf'] 	= "\r\n";

				$Email->initialize($config);

				//	Fim da Configuração do email	
				//===================================================================
				//	Corpo do Email

				$mail_to = '';
				$mail_subject = '';

				switch ($this->input->post('setor')) {
					case 1:
						$mail_to = 'marketing@nexgroup.com.br';
						$mail_subject = 'Seja um Fornecedor';
						break;
					case 2:
						$mail_to = 'marketing@nexgroup.com.br';
						$mail_subject = 'Venda seu Terreno';
						break;
					case 4:
						$mail_to = 'marketing@nexgroup.com.br';	
						$mail_subject = 'Sou Corretor e Quero Vender Nex';
						break;
					default:
						$mail_to = 'marketing@nexgroup.com.br';			
						$mail_subject = 'Outros';	
						break;
				}

				ob_start();
				?>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;"><?=$mail_subject?>. Enviado via site.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Forma de Contato:</strong> <?=$fc?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['email']?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
					</tr>
					<?php if($camp == 1) : ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Origem via Campanha Digital:</strong><br/>
							<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px">
							<strong>URL da Campanha:</strong><br/>
							<?=$origem?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td style="padding-top:15px">
							<strong>Mensagem:</strong><br/> 
							<?=nl2br($data['comentarios'])?>
						</td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
				<?
				$conteudo = ob_get_contents();
				ob_end_clean();

				//	Fim Corpo do Email
				//===================================================================
				//	Recepientes

				$Email->to($mail_to);	
				$Email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br');
				$Email->subject('Venha Para a Rede Nex - ' . $mail_subject);
				$Email->from("noreply@nexgroup.com.br", "Nex Group");
				$Email->message("$conteudo");

				//	Fim Recepientes  
				//===================================================================
				//	Envia Email

				if(@$Email->send())
				{
					$response = array('erro' => 0); // Success
				}
				else
				{
					$response = array('erro' => 101); // Fail
				}

				//  Termina o envio do email  
				//===================================================================

				echo json_encode($response);
				exit;				
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}
	}


	/**
	* Function trabalheConoscoXHR()
	*
	* Envia formulário de atendimento (Modal Atendimento Nex Group)
	* Registra novo currículo (trabalhe conosco) no banco (tabela jobs)
	* Faz o upload do arquivo para /uploads/trabalhe_conosco/
	* Opção de cadastrar novo registro em newsletter
	* Envia email com currículo em anexo para reponsável pelo atendimento da nex
	*
	* @return (json) ($response)
	*/
	function trabalheConoscoXHR()
	{
		if(($this->agent->is_referral() && $this->agent->referrer() == base_url() . 'contato') && $this->input->is_ajax_request())
		{
			if(!$this->agent->is_robot())
			{
				header('Content-type: application/json');

				//	REGISTRO DE ATENDIMENTO NEX (upload currículo) PART 1
				//
				//  TRABALHE CONOSCO
				//===================================================================

				$data = array();

				if(isset($_GET['files']))
				{  
					$error = false;
					$files = array();
					$id    = 0;

					$uploaddir = './uploads/trabalhe_conosco/';

					foreach($_FILES as $file)
					{
						$info = pathinfo($file['name']);

						if($info['extension'] == 'pdf' || $info['extension'] == 'doc' || $info['extension'] == 'docx')
						{
							$file_name = md5($file['name'] . date('Ymd-His') ) . '.' .$info['extension'];

							if(move_uploaded_file($file['tmp_name'], $uploaddir . basename($file_name)))
							{
								$files[] = $uploaddir . $file_name;

								$data = array();
								$data['data_envio'] = date('Y-m-d H:i:s');
								$data['curriculo']	= $file_name;

								$id = $this->model->setTrabalheConosco($data);
								break;
							}
							else
							{
								$error = true;
							}
						}
						else
						{
							echo json_encode(array('error' => 1, 'msg' => utf8_encode('Tipo de arquivo inválido')));
							exit;
						}		
					}

					$response = ($error) ? array('error' => 1, 'msg' => 'Houve um erro ao carregar seu arquivo!') : array('files' => $files, 'id_curriculo' => $id);

					//	Fim envio do arquvio	
					//===================================================================

					echo json_encode($response);
					exit;
				}
				else
				{
					//  REGISTRO DE ATENDIMENTO NEX (registro na base) PART 2
					// 
					//  Validação do dados
					//===================================================================

					$this->load->helper('email');

					if($this->input->post('atendinex_nome') == "")
					{
						echo json_encode(array("error" => 1, "msg" => "Por favor, preencha o campo NOME corretamente."));
						exit;
					}

					if(!valid_email($this->input->post('atendinex_email')))
					{
						echo json_encode(array("error" => 1, "msg" => "Email inválido! Por favor, digite seu EMAIL corretamente."));
						exit;
					}

					if($this->input->post('atendinex_telefone') == "" || $this->input->post('atendinex_telefone') == "(__) ____-_____")
					{
						echo json_encode(array("error" => 1, "msg" => "Por favor, preencha o campo TELEFONE corretamente."));
						exit;
					}

					// Carrega o registro do currículo pelo ID no objeto
					$curriculo = $this->model->getCurriculo((int) $this->input->post('id_curriculo'));

					if($curriculo)
					{
						//	Fim da validação
						//===================================================================
						//	Prepara os dados

						/*
						|--------------------------------------------------------------------------
						| URL de Origem
						|--------------------------------------------------------------------------
						*/

						$origem = $this->agent->referrer();

						if($origem)
						{
							$this->load->library('utilidades');

							if($this->utilidades->valid_url_format($this->input->post('atendinex_refer')) && $this->input->post('atendinex_refer') != $origem)
							{
								$origem = $this->input->post('atendinex_refer'); 
							}
						}
						else
						{
							$origem = $this->input->post('atendinex_refer');
						}

						/*
						|--------------------------------------------------------------------------
						| URL de Campanha
						|--------------------------------------------------------------------------
						*/	

						$camp = 1; //  1 => libera visualização da campanha no email; 0 => oculta;

						$ex_url = explode("?", $origem);

						parse_str(@$ex_url[1], $output);

						$url  = 	   @$output['utm_source'];
						$url .= "__" . @$output['utm_medium'];
						$url .= "__" . @$output['utm_content'];
						$url .= "__" . @$output['utm_campaign'];

						if($url == "______" || $url == "")
						{
							$url 	= $this->input->post('atendinex_refer');
							$origem = $this->agent->referrer();

							$camp = 0; // oculta visualização da campanha no email
						}

						/*
						|--------------------------------------------------------------------------
						| Forma do Contato
						|--------------------------------------------------------------------------
						*/

						$fc = ($this->input->post('atendinex_tipo') == 2) ? 'Telefone' : 'E-mail';

						//	Fim da preparação
						//===================================================================
						//	Efetua o registro na base

						$data = array();
						$data['nome'] 		 = $this->input->post('atendinex_nome');
						$data['email'] 		 = $this->input->post('atendinex_email');
						$data['comentarios'] = $this->input->post('atendinex_massege');
						$data['telefone'] 	 = $this->input->post('atendinex_telefone');
						$data['origem']		 = $origem;
						$data['url']		 = $url;
						$data['ip'] 		 = $this->input->ip_address();
						$data['user_agent']  = $this->input->user_agent();

						$this->model->updateTrabalheConosco($data, $curriculo->id_job);

						if($this->input->post('atendinex_newsletter') && $this->input->post('atendinex_newsletter') == 'S')
						{
							if(!$this->model->getNewsletter($data['email']))
							{
								$data_news = array();
								$data_news['nome'] 			= $data['nome'];	
								$data_news['email'] 		= $data['email'];	
								$data_news['data_cadastro'] = date('Y-m-d H:i:s');	
								$data_news['ip'] 			= $data['ip'];	
								$data_news['url'] 			= $data['url'];	
								$data_news['origem'] 		= $data['origem'];	
								$data_news['user_agent'] 	= $data['user_agent'];

								$this->model->setNewsletter($data_news);
							}					
						}

						//	Fim do registro na base, prepara para enviar email de retorno 
						//===================================================================
						//	
						//	EMAIL DE RETORNO
						//
						//	Configuração do email

						$this->load->library('email');
						$Email = new CI_Email();

						$config['protocol'] = 'mail';

						$config['mailtype'] = 'html';
						$config['charset'] 	= 'utf-8';
						$config['newline']  = "\r\n";
						$config['crlf'] 	= "\r\n";

						$Email->initialize($config);

						//	Fim da Configuração do email	
						//===================================================================
						//	Corpo do Email

						ob_start();
						?>
						<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
							<tr>
								<td style="background: #b72025;text-align: center;padding: 3px 0;">
									<a target="_blank" href="http://www.nexgroup.com.br">
										<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
									</a>
								</td>
							</tr>
							<tr>
								<td style="padding-top:15px">
									<h3 style="font-size:16px;margin:0;color: #333;">Trabalhe Conosco. Currículo enviado via site.</h3><br/>
									Realizado em <?=date("d/m/Y H:i:sa")?>
								</td>
							</tr>
							<tr>
								<td style="padding-top:15px"><strong>Nome:</strong> <?=$data['nome']?></td>
							</tr>
							<tr>
								<td style="padding-top:5px"><strong>Forma de Contato:</strong> <?=$fc?></td>
							</tr>
							<tr>
								<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['email']?></span></td>
							</tr>
							<tr>
								<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$data['telefone']?></span></td>
							</tr>
							<?php if($camp == 1) : ?>
							<tr>
								<td style="padding-top:15px">
									<strong>Origem via Campanha Digital:</strong><br/>
									<span style="padding-top:10px;font-size: 14px;"><?=$url?></span>
								</td>
							</tr>
							<tr>
								<td style="padding-top:5px">
									<strong>URL da Campanha:</strong><br/>
									<?=$origem?>
								</td>
							</tr>
							<?php endif; ?>
							<tr>
								<td style="padding-top:15px">
									<strong>Mensagem:</strong><br/> 
									<?=nl2br($data['comentarios'])?>
								</td>
							</tr>
							<tr>
								<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
									Resposta automática de retorno do envio via site. Não responda este email.<br/>
									Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
									http://www.nexgroup.com.br<br/>
									&copy; NEX GROUP <?=date('Y')?><br/>
								</td>
							</tr>
						</table>
						<?
						$conteudo = ob_get_contents();
						ob_end_clean();

						//	Fim Corpo do Email
						//===================================================================
						//	Recepientes
						
						$Email->from("noreply@nexgroup.com.br", "Nex Group");
						$Email->to('marta@nexgroup.com.br');
						$Email->bcc('gabriel.oribes@divex.com.br, andre.wille@divex.com.br');
						$Email->subject('Trabalhe Conosco');

						$Email->attach('./uploads/trabalhe_conosco/' . $curriculo->curriculo);

						$Email->message("$conteudo");

						//	Fim Recepientes  
						//===================================================================
						//	Envia Email

						if(@$Email->send())
						{
							$response = array('erro' => 0); // Success
						}
						else
						{
							$response = array('erro' => 101); // Fail
						}

						//  Termina o envio do email  
						//===================================================================

						echo json_encode($response);
						exit;

					}
					else
					{
						echo json_encode(array('error' => 3, 'msg' => utf8_encode('Registro do Currículo não encontrado!'))); // Fail
						exit;
					}
				}
			}
			else
			{
				exit; // robot escape!
			}
		}
		else
		{
			// Not is referral or XHR! Access Fail.
			show_404();
			exit;
		}	
	}


}
/* End of file contato.php */
/* Location: ./application/controllers/contato.php */