<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Integracao extends CI_Controller {

	public function index()
	{
		$this->load->view('home/index');
	}
	
	public function cadastraInteresse()
	{
		$this->load->model('integracao_model','model');
		
		$data 		= $_POST;
		$empresa  	= $this->getEmpresa($data['senha']);
			
		$data['id_estado'] = 21;
			
		if($data['id_empreendimento'])
		{
			$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
			
			if($empreendimento)
			{
				$data['id_empreendimento']	= $empreendimento;
				
				unset( $data['senha']);
				unset( $data['emp']);
				
				$this->enviaEmail($data);
				$this->model->cadastraInteresse($data);	
			}	
		}
	}
	
	public function cadastraIndique()
	{
		$this->load->model('integracao_model','model');
		
		$data 		= $_POST;
		$empresa  	= $this->getEmpresa($data['senha']);
		
		if($data['id_empreendimento'])
		{
			$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
			
			if($empreendimento)
			{
				$data['id_empreendimento']	= $empreendimento;
				
				unset($data['senha']);
				unset($data['emp']);
				
				$this->model->cadastraIndique($data);
			}	
		}
	}
	
	public function cadastraContato()
	{
		$this->load->model('integracao_model','model');
		
		$data 		= $_POST;
		$empresa  	= $this->getEmpresa($data['senha']);
		
		if(@$data['id_empreendimento'])
		{
			$empreendimento = $this->model->buscaEmpreendimento($data['id_empreendimento'],$empresa);
				
			if(@$data['uf'])
			{
				$estado = $this->model->buscaEstado($data['uf']);
			}
			else
			{ 
				$estado = 21;
			}	
					
			if($empreendimento)
			{
				unset( $data['uf']);
				
				$data['id_estado']			= $estado;
				$data['id_empreendimento']	= $empreendimento;
					
				unset($data['senha']);
				unset($data['emp']);
					
				if($data['id_empreendimento'] != 75)
				{
					$this->enviaEmailContato($data);
				}	
				if(@$data['setor']) 
				{
					unset($data['setor']);
				}	
					
				$this->model->cadastraContato($data);
			}	
		}
		else
		{
			unset( $data['uf']);
					
			$data['id_estado']			= '21';
			$data['id_empreendimento']	= '';
					
			unset( $data['senha']);
			unset( $data['emp']);
			unset($data['setor']);
					
			//$this->enviaEmailContato($data);
			@$this->model->cadastraContato($data);
		}
	}
	
	public function cadastraTerreno()
	{
		$this->load->model('integracao_model','model');
		
		$data 		= $_POST;
		$empresa  	= $this->getEmpresa($data['senha']);
		
		if($empresa)
		{
			unset($data['senha']);
			$this->model->cadastraTerreno($data);
		}
	}
	
	public function cadastraFornecedor()
	{
		$this->load->model('integracao_model','model');
		
		$data 		= $_POST;
		$empresa  	= $this->getEmpresa($data['senha']);
		
		if($empresa)
		{
			unset( $data['senha']);
			$this->model->cadastraFornecedor($data);
		}
	}
	
	public function getEmpresa($senha)
	{
		/*SENHAS EMPRESAS
		 * CAPA 			= }3_$%wB`7Atbdk1w
		 * EGL 				= `w~;ULm3CL{[AfV$	
		 * DHZ				= 7>k}m#AKZEXs|1{M	
		 * Lomando Aita		= HS^SM&*hoCV(dv)W	
		*/
		$senhas = array
					(
						'capa' 		=>"}3_$%wB`7Atbdk1w",
						'egl' 		=>"`w~;ULm3CL{[AfV$",
						'dhz' 		=>"7>k}m#AKZEXs|1{M",
						'Lomando'	=>"HS^SM&*hoCV(dv)W"
					);
		
		if($senha)
		{
			if($senha ==$senhas['capa'])
			{
				$empresa = 1;
			}
			elseif($senha ==$senhas['egl'])
			{
				$empresa = 2;
			}
			elseif($senha ==$senhas['dhz'])
			{
				$empresa = 3;
			}
			elseif($senha ==$senhas['Lomando'])
			{
				$empresa = 4;
			}
		}
		return $empresa;
	}
	
	function enviaEmail($data)
	{
		
		$this->load->model('contatos_model','model2');
		
		$this->load->library('email');
		
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		$nome 				= $data['nome'];
		$faixa_etaria 		= $data['faixa_etaria'];
		$estado_civil 		= $data['estado_civil'];
		$bairro_cidade 		= $data['bairro_cidade'];
		$profissao 			= $data['profissao'];
		$telefone 			= $data['telefone'];
		$email				= $data['email'];
		$comentarios 		= $data['comentarios'];
		$contato 			= $data['forma_contato'];
		$url 				= $data['url'];
		$origem 			= $data['origem'];
		$ip 				= $data['ip'];
		$user_agent			= $data['user_agent'];

		$id_empreendimento = $data['id_empreendimento'];
		
		$empreendimento		= $this->model2->getEmpreendimento($id_empreendimento);
		$data				= date("d/m/Y H:i:s");
		
		ob_start();
		
		?>
			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
				 			<td align="center" colspan="2">
				 				<a target="_blank" href="http://www.nexgroup.com.br">
				 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
				 				</a>
				 			</td>
						 </tr>
						<tr>
							<td colspan='2'>Interesse enviado em <?php echo $data; ?></td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
		    				<td colspan="2">Interesse enviado por <strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  			</tr>
						  <tr>
						    <td width="30%">&nbsp;</td>
						    <td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
						    <td width="30%">Empreendimento:</td>
						    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
						    <td width="30%">IP:</td>
						    <td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
						    <td width="30%">E-mail:</td>
						    <td><strong><?=@$email?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Faixa Etária:</td>
						    <td><strong><?=$faixa_etaria?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Estado Civil:</td>
						    <td><strong><?=$estado_civil?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Bairro e Cidade:</td>
						    <td><strong><?=$bairro_cidade?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Profissão:</td>
						    <td><strong><?=$profissao?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Telefone:</td>
						    <td><strong><?=$telefone?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%">Forma de Contato:</td>
						    <td><strong><?=$contato?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Comentários:</td>
						    <td valign="top"><strong><?=$comentarios?></strong></td>
						  </tr>
						  <tr>
						    <td width="30%" valign="top">Origem:</td>
						    <td valign="top"><strong><?=@$data['origem'];?></strong></td>
						  </tr>		
						  <tr>
						    <td width="30%" valign="top">URL:</td>
						    <td valign="top"><strong><?=@$data['url'];?></strong></td>
						  </tr>							  					  					  
						  <tr>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						  </tr>
						<tr>
							<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						</tr>
					</table>
				</body>
			</html>
		<?php
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
			
		$tipo 	= 1;
		$emails = $this->model2->getEnviaEmail($id_empreendimento, @$setor, $tipo);
		
		foreach ($emails as $email)
		{
			if($email->tipo === 'para')
			{
				$list[] = $email->email;
			}
			else if($email->tipo === 'cc')
			{
				$list_cc[] = $email->email;
			}
			else
			{
				$list_bcc[] = $email->email;
			}
		}
		
		$list_bcc[] = 'testes@divex.com.br';	
		$this->email->from("noreply@nexgroup.com.br", "Nex Group");
		
		 
		if($nome=='teste123')
		{
			$this->email->to('testes@divex.com.br');
		}
		else
		{
		 	if(@$list)
		 	{
				$this->email->to($list);
			}
			if(@$list_cc)
			{
				$this->email->cc($list_cc);
			}
			if(@$list_bcc)
			{
				$this->email->bcc($list_bcc);
			}
				
		}
		
		$this->email->bcc('testes@divex.com.br');
		$this->email->subject('Interesse enviado via site');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email	
	}
	
	function enviaEmailContato($data)
	{
		
		$this->load->model('contatos_model','model2');
		
		$this->load->library('email');
		
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		//Inicio da Mensagem
					
		$nome					= @$data['nome'];
		$email					= @$data['email'];
		$endereco				= @$data['endereco'];
		$cidade					= @$data['cidade'];
		$uf						= @$data['uf'];
		$cep					= @$data['cep'];
		$telefone				= @$data['telefone'];
		$comentarios			= @$data['comentarios'];
		$id_empreendimento 		= @$data['id_empreendimento'];
		
		$empreendimento		= $this->model2->getEmpreendimento(@$id_empreendimento);
		$dataCad			= date("d/m/Y H:i:s");
		
		//exit;
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title><?=@$empreendimento->empreendimento?></title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b><?=@$empreendimento->empreendimento?></b> :: Contato via Site</td>
			  </tr>
			  <tr>
			    <td colspan="2">Contato enviado por <strong><?=@$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
               <tr>
                    <td width="30%">Empreendimento:</td>
                    <td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
              </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=@$this->input->ip_address()?></strong></td>
			  </tr>				  
			  <tr>
			    <td width="30%">E-mail:</td>
			    <td><strong><?=@$email?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Endereço:</td>
			    <td><strong><?=@$endereco?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Cidade:</td>
			    <td><strong><?=@$cidade?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">UF:</td>
			    <td><strong><?=@$uf?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">CEP:</td>
			    <td><strong><?=@$cep?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Telefone:</td>
			    <td><strong><?=@$telefone?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=@$comentarios?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Origem:</td>
			    <td valign="top"><strong><?=@$_SESSION['origem'];?></strong></td>
			  </tr>		
			  <tr>
			    <td width="30%" valign="top">URL:</td>
			    <td valign="top"><strong><?=@$_SESSION['url'];?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.nexgroup.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
			
		$tipo 	= 1;
		$setor = @$data['setor'];
		
		if(@$setor == 'comercial')
		{
			$enviaSetor = 5;
		}
		elseif(@$setor == 'financeiro')
		{
			$enviaSetor = 6;
		}
		elseif(@$setor == 'engenharia')
		{
			$enviaSetor = 7;
		}
		else
		{
			$enviaSetor = 8;
		}
		
		
		if($empreendimento->id_empreendimento == 34)
		{
			$emails = $this->model2->getEnviaEmailEgl($id_empreendimento,$enviaSetor, $tipo);
		}
		else
		{
			$emails = $this->model2->getEnviaEmail($id_empreendimento);
		}
		
		foreach ($emails as $email)
		{
			if($email->tipo === 'para')
			{
				$list[] 	= $email->email;
			}
			else if($email->tipo === 'cc')
			{	
				$list_cc[] 	= $email->email;
			}
			else
			{
				$list_bcc[] = $email->email;
			}
		}
			
		$this->email->from("noreply@nexgroup.com.br", "NEX GROUP");
		
		$list[] = 'testes@divex.com.br';
			
		if($nome=='teste123')
		{
			$this->email->to('testes@divex.com.br');
		}
		else
		{
		 	if(@$list)
		 	{
				$this->email->to($list);
			}
			if(@$list_cc)
			{	
				$this->email->cc($list_cc);
			}
			if(@$list_bcc)
			{	
				$this->email->bcc($list_bcc);
			}
		}
		
		
		$this->email->subject('Contato enviado via site');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */