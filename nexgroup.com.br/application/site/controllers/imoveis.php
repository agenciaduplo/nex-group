<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imoveis extends CI_Controller {

	public function index($offset = "")
	{
		redirect('imoveis/lista/');
		
	}
	
	public function buscar($palavraChave = ""){
		$post = $this->input->post('palavra-chave');
		if(!empty($post)){
			redirect(base_url().'imoveis/buscar/'.$this->input->post('palavra-chave'));
			exit();
		}
		
		self::lista($palavraChave);
	}
	
	public function lista($offset = "")
	{
		if($this->input->get('q'))
		{
			var_dump($this->input->get('q'));
			exit;
		}

		$this->load->model('imoveis_model', 'model');
		$this->load->model('imoveis_model', 'modelImoveis');
		$this->db->reconnect();
		
		
		$config = array (
			'uri_segment'		=> 2,
			'base_url'			=> 'http://www.nexgroup.com.br/imoveis/',
			'total_rows'		=> $this->model->numImoveis(),
			'first_link'        => '<<',
			'last_link'			=> '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '15'
		);
		
		$this->pagination->initialize($config); 
		
		$tags = $this->model->getTags(2);
 		if(@$tags->title)
 			$title = $tags->title;
 		else 	
 			$title = 'Im&oacute;veis - NEX GROUP';
	 		
	 		
	 		
		$todosImoveis = $this->modelImoveis->buscaTodosImoveis();
	 	// $arrEstados = array();
	 	// $arrCidade 	= array();
	 	// $arrTipos 	= array();
	 	$arrLatLon = array();
	 	$arrImoveisBusca = array();
	 	foreach($todosImoveis as $row)
	 	{
	 		// if(!isset($arrEstados[$row->id_estado])){
	 		// 	$arrEstados[$row->id_estado] = $row->estado;
	 		// }
	 		
	 		// if(!isset($arrCidades[$row->id_cidade])){
	 		// 	$arrCidades[$row->id_cidade] = $row->cidade;
	 		// }
	 		
	 		// if(!isset($arrTipos[$row->id_tipo_imovel])){
	 		// 	$arrTipos[$row->id_tipo_imovel] = $row->tipo_imovel;
	 		// }
	 		
 			$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
 			$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
 			//$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;
 			$url = site_url().'imoveis/'.@$url;
 			
	 		$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
 			$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
 			$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);
 			if(count($fotos1) > 0 && file_exists('uploads/imoveis/midias/'.$fotos1[0]->arquivo)){
 				$imagem = base_url().'uploads/imoveis/midias/'.$fotos1[0]->arquivo;
 			}else if(count($fotos2) > 0 && file_exists('uploads/imoveis/midias/'.$fotos2[0]->arquivo)){
 				$imagem = base_url().'uploads/imoveis/midias/'.$fotos2[0]->arquivo;
 			}else if(count($fotos3) > 0 && file_exists('uploads/imoveis/midias/'.$fotos3[0]->arquivo)){
 				$imagem = base_url().'uploads/imoveis/midias/'.$fotos3[0]->arquivo;
 			}else{
 				$imagem = base_url().'assets/site/img/bg-default.png';
 			}
 			
	 		array_push($arrImoveisBusca, array(
	 			'id' 			=> $row->id_empreendimento,
	 			'nome' 			=> $row->empreendimento,
	 			'chamada' 		=> $row->chamada_empreendimento,
	 			'chamada2'		=> $row->segunda_chamada_empreendimento,
	 			'url' 			=> $url,
	 			'lat' 			=> $row->latitude,
	 			'lon' 			=> $row->longitude,
	 			'pin' 			=> base_url().'uploads/imoveis/pin/'.$row->pin,
	 			'cidade'		=> $row->cidade,
	 			'estado'		=> $row->estado,
	 			'id_estado' 	=> $row->id_estado,
	 			'id_cidade' 	=> $row->id_cidade,
	 			'id_tipo' 		=> $row->id_tipo_imovel,
	 			'id_status' 	=> $row->id_status,
	 			'imagem'		=> $imagem,
	 			// 'cidade'	=> $row->cidade,
	 			'dormitorios' 	=> $row->dormitorio,
	 			'metragem' 		=> $row->metragem,
	 			'vagas' 		=> $row->vagas,
	 		));
	 	}
	 	
	 			
		$data = array(
			'title'					=> @$title,
			'keywords'				=> @$tags->keywords,
			'description'			=> @$tags->description,
			
			'page'					=> "imoveis",
			
			// 'imoveis'				=> $this->model->getAllImoveis(),
			// 'estados'				=> $this->model->getEstados(),
			// 'cidades'				=> $this->model->getCidades('21'),
			// 'tipo_imoveis'			=> $this->model->getTiposImoveis(),
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'paginacao'	  			=> $this->pagination->create_links(),
				
			'palavraChave' 			=> urldecode($offset),
				
			// 'todosImoveis'			=> $todosImoveis,
			// 'lancamentosAll'		=> $this->modelImoveis->getLancamentos(),
			// 'prontoPraMorar'		=> $this->modelImoveis->getProntoPraMorar(),
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			'favoritos'				=> $this->imoveis_visitados->getImoveisFavoritos(),
			'mais_vistos'			=> $this->modelImoveis->getEmpreendimentosMaisVistos(),
			
			// 'arrEstados' 			=> $arrEstados,
			// 'arrCidades' 			=> $arrCidades,
			// 'arrTipos' 				=> $arrTipos,
			'arrImoveisBusca'		=> $arrImoveisBusca,
		
		);
		
		$this->load->view('imoveis/imoveis', $data);
	}
	
	public function visualizar ($id_empreendimento)
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();

		$iv = $this->model->getEmpreendimento($id_empreendimento);
		
		if($iv->id_empreendimento)
		{
			$id_empreendimento = $iv->id_empreendimento;
		}
		$this->imoveis_visitados->gravaCookie($id_empreendimento);
		
		
		if($iv->id_empreendimento)
		{
			
			$this->model->addVisita($id_empreendimento);

			if(isset($_COOKIE['favorito']))
			{
				$arr_fav = explode(',', $_COOKIE['favorito']);

				if(in_array($id_empreendimento, $arr_fav)){
					$favoritado = true;
				}else{
					$favoritado = false;
				}
			}
			else
			{
				$favoritado = false;
			}
			
			// if(isset($_COOKIE['favorito']) && strpos($_COOKIE['favorito'], ','.$id_empreendimento ) !== false){
			
			
			$data = array(
				'title'					=> "$iv->empreendimento - $iv->cidade / $iv->uf - Imóveis - NEX GROUP",	
				'page'					=> "imovel",
				'pagetag'				=> 'imovelvisualizar',
				'proximidades'			=> $this->model->getProximidades(),
				'tipos_proximidades'	=> $this->model->getTiposProximidades(),
				'imovel'				=> $iv,
				// 'imovel'				=> $this->model->getEmpreendimento($id_empreendimento),
				'semelhantes'			=> $this->model->getEmpreendimentoSemelhante($id_empreendimento),
				'destaques'				=> $this->model->getDestaques($id_empreendimento),
				'destaquesSuperior'		=> $this->model->getFotos($id_empreendimento, 30),
				'itens'					=> $this->model->getItens($id_empreendimento),
				'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento),
				'fases'					=> $this->model->getFases($id_empreendimento),
				'favoritado'			=> $favoritado,
			);
			
// 			var_dump($this->model->getFotos($id_empreendimento, 30));
// 			exit;
			
			
// 			var_dump($this->model->getTiposProximidades());
// 			exit;
// 			var_dump(
// 				$this->model->getFases($id_empreendimento),
// 				$this->model->getItensFases(31),
// 				$this->model->getItensFasesSomado(31),
// 				$this->model->getItensFases(109)
// 			);
// 			exit;

			
// 			if($id_empreendimento == 90):
// 				$this->load->view('imoveis/imoveis-visualizar-maxhaus',$data);
// 			else:
// 				$this->load->view('imoveis/imoveis-visualizar',$data);
// 			endif;

			$this->load->view('imoveis/imoveis-visualizar',$data);
		}
		else
		{
			redirect('imoveis');
		}
	}

	function removerFavorito()
	{
		if(isset($_COOKIE['favorito']))
		{
			$arr_fav = explode(',', $_COOKIE['favorito']);

			// echo "array normal <br/>";
			// var_dump($arr_fav); 

			$arr_fav = array_diff($arr_fav, (array) $this->input->post('id_imovel'));

			// echo "<br/>array novo <br/>";
			// var_dump($arr_fav); 

			if(count($arr_fav) >= 1) 
			{
				$arr_fav = implode(',', $arr_fav);
				echo json_encode(array('erro' => 0, 'favoritos' => $arr_fav));
				exit;
				// setcookie('favorito');
				// setcookie('favorito', $arr_fav, (time() + (30*24*60*60*1000)));
			}
			else
			{
				echo json_encode(array('erro' => 0, 'favoritos' => ''));
				exit;
			}
			
			// echo "<br/><br/>" . $arr_fav;	
		}

		echo json_encode(array('erro' => 1));
		exit;
	}

	
	public function pre_visualizar ($id_empreendimento)
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$this->imoveis_visitados->gravaCookie($id_empreendimento);
		
		$iv = $this->model->getEmpreendimentoPre($id_empreendimento);
		
		$data = array(
			
			'title'					=> "$iv->empreendimento - $iv->cidade / $iv->uf - Imóveis - NEX GROUP",	
			'page'					=> "imovel",
			'pagetag'				=> 'imovelvisualizar',
			'imovel'				=> $this->model->getEmpreendimentoPre($id_empreendimento),
			'semelhantes'			=> $this->model->getEmpreendimentoSemelhante($id_empreendimento),
			'destaques'				=> $this->model->getDestaques($id_empreendimento),
			'itens'					=> $this->model->getItens($id_empreendimento),
			'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento),
			'fases'					=> $this->model->getFases($id_empreendimento)
			
		);
		
		$this->load->view('imoveis/imoveis-visualizar',$data);
	}
	
	public function visualizar2 ($id_empreendimento)
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$this->imoveis_visitados->gravaCookie($id_empreendimento);
		
		$iv = $this->model->getEmpreendimento($id_empreendimento);
		
		$data = array(
			
			'title'					=> "$iv->empreendimento - $iv->cidade / $iv->uf - Imóveis - NEX GROUP",	
			'page'					=> "imovel",
			
			'imovel'				=> $this->model->getEmpreendimento($id_empreendimento),
			'semelhantes'			=> $this->model->getEmpreendimentoSemelhante($id_empreendimento),
			'destaques'				=> $this->model->getDestaques($id_empreendimento),
			'itens'					=> $this->model->getItens($id_empreendimento),
			'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento),
			'fases'					=> $this->model->getFases($id_empreendimento)
			
		);
		
		$this->load->view('imoveis/imoveis-visualizar3',$data);
	}
	
	public function favoritar($id_empreendimento)
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$this->model->addFavorito($id_empreendimento);
		
		echo "true;";
		exit;
	}
		
	public function buscaGeral($offset = "")
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$_SESSION['busca'] = $this->input->post('Busca');
		$busca = $_SESSION['busca'];
		$config = array (
			'base_url'			=> 'http://www.nexgroup.com.br/imoveis/buscaGeral',
			'total_rows'		=> $this->model->numImoveisBuscaGeral($busca),
			'first_link'        => '<<',
			'last_link'			=> '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '15'
		);
		
		$this->pagination->initialize($config); 
		$tags = $this->model->getTags(11);
	 		if(@$tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'Im&oacute;veis - NEX GROUP';
		$data = array(
			
			'title'					=> @$title,
			'keywords'				=> @$tags->keywords,
			'description'			=> @$tags->description,	
		
			'page'					=> "imoveis",
			
			'imoveis'				=> $this->model->getImoveisBuscaGeral($busca,$offset),
			'estados'				=> $this->model->getEstados(),
			'tipo_imoveis'			=> $this->model->getTiposImoveis(),
			'paginacao'	  			=> $this->pagination->create_links()
		
		);
		
		$this->load->view('imoveis/imoveis', $data);
	}
	
	public function buscaImovel($offset = "")
	{
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		@session_start();
		
		
		if($this->input->post('cidades') || $this->input->post('tipos')){
			$_SESSION['cidade'] = $this->input->post('cidades');
			$_SESSION['tipo'] 	= $this->input->post('tipos');
		}

		$cidade = $_SESSION['cidade'];
		$tipo 	= $_SESSION['tipo'];

		if($cidade==false && $tipo=='' && $offset=='')
			redirect('imoveis');


		$config = array(
			'base_url'			  => 'http://www.nexgroup.com.br/imoveis/buscaImovel',
			'total_rows'		  => $this->model->numImoveisBuscaImovel($cidade,$tipo),
			'first_link'      => '<<',
			'last_link'			  => '>>',
			'first_tag_open'	=> '<li>',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li>',
			'last_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li>',
			'next_tag_close'	=> '</li>',
			'prev_tag_open'		=> '<li>',
			'prev_tag_close'	=> '</li>',
			'num_tag_open'		=> '<li>',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="Active">',
			'cur_tag_close'		=> '</li>',
			'per_page'			=> '15'
		);
		
		$this->pagination->initialize($config); 
		$tags = $this->model->getTags(12);
	 		if(@$tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'Im&oacute;veis - NEX GROUP';
	 			
		$data = array(
			
			'title'					=> @$title,
			'keywords'				=> @$tags->keywords,
			'description'			=> @$tags->description,	
			'page'					=> "imoveis",
			
			'imoveis'				=> $this->model->getImoveisBuscaImovel($cidade,$tipo,$offset),
			'estados'				=> $this->model->getEstados(),
			'cidades'				=> $this->model->getCidades('21'),
			'tipo_imoveis'			=> $this->model->getTiposImoveis(),
			'paginacao'	  			=> $this->pagination->create_links(),
			
			'cidadeSel'				=> $cidade,
			//'estadoSel'				=> $cidade,
			'tipoSel'				=> $tipo
		
		);
		
		$this->load->view('imoveis/imoveis', $data);
	}
	
	public function interesse($id_empreendimento)
	{
    $this->load->model('imoveis_model', 'model');
    $imovel = $this->model->getEmpreendimento($id_empreendimento);
		$data = array(
			'id_empreendimento'					=> $id_empreendimento,
      'empreendimento'            => $imovel->empreendimento
		);
		
		$this->load->view('interesse/interesse',$data);
	}
	
	public function interesse_promo($id_empreendimento)
	{
		$data = array(
			'id_empreendimento'					=> $id_empreendimento
		);
		
		$this->load->view('interesse/interesse_promo',$data);
	}
	
	public function indique($id_empreendimento)
	{
		$data = array(
			'id_empreendimento'					=> $id_empreendimento
		);
		
		$this->load->view('indique/indique',$data);
	}
	
	function enviarInteresse()
	{
		if(!$this->agent->is_robot()) {
			
			$this->load->model('contatos_model', 'model');
			$this->db->reconnect();
			
			@session_start();
			$data = array (
				
				'nome'				=> $this->input->post('nome'),
				'email'				=> $this->input->post('email'),
				'comentarios'		=> $this->input->post('descricao'),
				'telefone'			=> $this->input->post('telefone'),
				'id_empreendimento'	=> $this->input->post('empreendimento'),
				'forma_contato'		=> $this->input->post('contato'),
				'origem'			=> $_SESSION['origem'],
				'url'				=> $_SESSION['url'],
				'ip'				=> $this->input->ip_address(),
				'user_agent'		=> $this->input->user_agent()
			);

			$interesse 			= $this->model->setInteresse($data);
			$id_empreendimento 	= $this->input->post('empreendimento');
			$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);

			$data_envio 		= date("d/m/Y H:i:s");
			$nome 				= $this->input->post('nome');
			$email_envio		= $this->input->post('email');
			$comentarios		= $this->input->post('descricao');
			$telefone			= $this->input->post('telefone');
			$ip 				= $this->input->ip_address();

			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'mail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);

			$grupos = $this->model->getGrupos($id_empreendimento);
			$total = 0;
			$total = count($grupos);
			
			if($total == 1) // caso só tenho um grupo cadastrado
			{
				$emails = $this->model->getGruposEmails($id_empreendimento, '001');
				
				$emails_txt = "";
				foreach ($emails as $email)
				{
					$grupo = $email->grupo;
					$list[] = $email->email;

					$emails_txt = $emails_txt.$email->email.", ";
				}
				$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

			}
			else if($total > 1) // caso tenha mais de um grupo cadastrado
			{
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;
						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');
							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							
							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse,$grupo, $emails_txt);
						
						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);
							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_interesse($interesse,$grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
			}
			/* envia email via roleta */
      $this->email->to($list);
			$this->email->bcc('marketing@reweb.com.br');

			$empNome = strtoupper($empreendimento->empreendimento);

			ob_start();

			?>

			<html>
				<head>
					<title>Nex Group</title>
				</head>
				<body>
					<table>
						<tr align="center">
							<td align="center" colspan="2">
								<a target="_blank" href="http://www.nexgroup.com.br">
									<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
								</a>
							</td>
						 </tr>
						<tr>
							<td colspan='2'>Interesse enviado em <?php echo $data_envio; ?> via Portal</td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>
						
						<tr>
							<td colspan="2">Interesse enviado por <strong><?=@$nome?></strong> em <strong><?=$data_envio?></strong> as <strong><?=date('H:i:s')?></strong></td>
						</tr>
						  <tr>
							<td width="30%">&nbsp;</td>
							<td width="70%">&nbsp;</td>
						  </tr>
						   <tr>
							<td width="30%">Empreendimento:</td>
							<td width="70%"><?=@$empreendimento->empreendimento?> / <?=@$empreendimento->cidade?></td>
						  </tr>
						  <tr>
							<td width="30%">IP:</td>
							<td><strong><?=@$ip?></strong></td>
						  </tr>				  
						  <tr>
							<td width="30%">E-mail:</td>
							<td><strong><?=@$email_envio?></strong></td>
						  </tr>
						  <tr>
							<td width="30%">Telefone:</td>
							<td><strong><?=@$telefone?></strong></td>
						  </tr>
						  <tr>
							<td width="30%" valign="top">Comentários:</td>
							<td valign="top"><strong><?=@$comentarios?></strong></td>
						  </tr>																  
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
						  </tr>
					</table>
				</body>
			</html>

			<?php

			$html_email = ob_get_contents();
			ob_end_clean();

			$this->email->bcc('marketing@reweb.com.br');
			$this->email->from("noreply@nexgroup.com.br", "$empNome - Nex Group");
			$this->email->subject('Interesse enviado via Portal');
			//$this->email->message($this->load->view('tpl/email-interesse',array('dados' => $dataEmail),true));
			$this->email->message("$html_email");
			
			$this->email->send();
		}
	}
	
	function enviarIndique()
	{
		if(!$this->agent->is_robot())
		{
			$this->load->model('contatos_model', 'model');
			$this->db->reconnect();
			
			@session_start();
			$data = array (
				
				'nome_remetente'		=> $this->input->post('nome_remetente'),
				'email_remetente'		=> $this->input->post('email_remetente'),
				'nome_destinatario'		=> $this->input->post('nome_destinatario'),
				'email_destinatario'	=> $this->input->post('email_destinatario'),
				'comentarios'			=> $this->input->post('descricao'),
				'id_empreendimento'		=> $this->input->post('empreendimento'),
				'origem'				=> $_SESSION['origem'],
				'url'					=> $_SESSION['url'],
				'ip'					=> $this->input->ip_address(),
				'user_agent'			=> $this->input->user_agent()
	
			);
			
			$indique = $this->model->setIndique($data);
			$id_empreendimento = $this->input->post('empreendimento');
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'mail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			//Inicio da Mensagem
				$nome_remetente		= $this->input->post('nome_remetente');
				$email_remetente	= $this->input->post('email_remetente');
				$nome_destinatario	= $this->input->post('nome_destinatario');
				$email_destinatario	= $this->input->post('email_destinatario');
				$mensagem			= $this->input->post('descricao');
				$forma_contato		= $this->input->post('contato');
				$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
				$data				= date("d/m/Y H:i:s");
				
				$newUrl = $empreendimento->empreendimento."-".$empreendimento->cidade."-".$empreendimento->estado;
				$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
				
			ob_start();
			
			?>
				<html>
					<head>
						<title>Nex Group</title>
					</head>
					<body>
						<table>
							<tr align="center">
					 			<td align="center" colspan="2">
					 				<a target="_blank" href="http://www.nexgroup.com.br">
					 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
					 				</a>
					 			</td>
							 </tr>
							<tr>
								<td colspan='2'>Indicação enviada em <?php echo @$data; ?></td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
						    	<td colspan="2">Olá <?=@$nome_destinatario?>, acesse o site do <?=@$empreendimento->empreendimento?><br/><strong><a href="<?=site_url()?>imoveis/<?=@$empreendimento->id_empreendimento?>/<?=@$url?>">Clique aqui</a></strong></td>
						 	 </tr>
							  <tr>
							    <td width="30%">&nbsp;</td>
							    <td width="70%">&nbsp;</td>
							  </tr>
							  <tr>
							    <td>Indicação enviada por:</td>
							  	<td> <strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong></td>
							  </tr>
							  <tr>
							    <td>Para:</td>
							  	<td> <strong><?=@$nome_destinatario?> / <?=@$email_destinatario?></strong></td>
							  </tr>
							  <tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							  <tr>
							    <td width="30%" valign="top">Comentários:</td>
							    <td valign="top"><strong><?=@$mensagem?></strong></td>
							  </tr>	
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
							</tr>
						</table>
					</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem


			//roleta
			$grupos = $this->model->getGrupos($id_empreendimento);
			$total = 0;
			$total = count($grupos);
			
			if($total == 1) // caso só tenho um grupo cadastrado
			{
				$emails = $this->model->getGruposEmails($id_empreendimento, '001');
				
				$emails_txt = "";
				foreach ($emails as $email)
				{
					$grupo = $email->grupo;
					$list[] = $email->email;

					$emails_txt = $emails_txt.$email->email.", ";
				}
				$this->model->grupo_indique($indique, $grupo, $emails_txt);

			}
			else if($total > 1) // caso tenha mais de um grupo cadastrado
			{
				foreach ($grupos as $row):

					if($row->rand == 1):
						$atual = $row->ordem;
						if($atual == $total):

							$this->model->updateRand($id_empreendimento, '001');
							$emails = $this->model->getGruposEmails($id_empreendimento, '001');
							
							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_indique($indique, $grupo, $emails_txt);
						
						else:

							$atualizar = "00".$atual+1;
							$this->model->updateRand($id_empreendimento, $atualizar);
							$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

							$emails_txt = "";
							foreach ($emails as $email)
							{
								$grupo = $email->grupo;
								$list[] = $email->email;

								$emails_txt = $emails_txt.$email->email.", ";
							}
							$this->model->grupo_indique($indique, $grupo, $emails_txt);

						endif;	
					endif;
				endforeach;
			}
			/* envia email via roleta */
			
			
			$this->email->from("noreply@nexgroup.com.br", "$nome_remetente");
      
			$this->email->to($email_destinatario);	
			$this->email->bcc("marketing@reweb.com.br");
			
			$this->email->subject("$empreendimento->empreendimento - Indicação enviada para você");
			$this->email->message("$conteudo");
			
			if($this->email->send())
			{
				echo "foi";
			}
			else {
				echo "nao";
			}

			//envia para corretores
			$this->email->from("noreply@nexgroup.com.br", "$nome_remetente");
			$this->email->to($list);	
			$this->email->cc("marketing@reweb.com.br");
			$this->email->subject("$empreendimento->empreendimento - Indique enviado via site");
			$this->email->message("$conteudo");

			if($this->email->send())
			{
				echo "foi";
			}
			else {
				echo "nao";
			}
			//envia para corretores


			
			//===================================================================
			//Termina o envio do email	
		}
	}
	
	function getCidades($id_estado){

		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$cidades = $this->model->getCidades($id_estado);
		
		foreach ($cidades as $cidade):
			echo "<option value='".$cidade->id_cidade."'>".$cidade->cidade."</option>";
		endforeach;
	}
	
	function download ($id_midia)
	{
		$this->load->helper('download');
		$this->load->model('imoveis_model', 'model');
		$this->db->reconnect();
		
		$midia = $this->model->getMidia($id_midia);
	
		force_download("./uploads/imoveis/midias/$midia->arquivo");
	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */