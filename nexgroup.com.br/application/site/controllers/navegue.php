<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navegue extends CI_Controller {

	public function index()
	{
		$this->load->model('imoveis_model', 'modelImoveis');
		
		$tags = $this->modelImoveis->getTags(7);
	 		if($tags->title)
	 			$title = $tags->title;
	 		else 	
	 			$title = 'Navegue pelo Mapa - NEX GROUP';
	 			
		$data = array(
			'title'					=> $title,
			'keywords'				=> $tags->keywords,
			'description'			=> $tags->description,		
			'page'					=> 'navegue-pelo-mapa',
			'visitados'				=> $this->imoveis_visitados->getImoveisVisitados()
		);
		$this->load->view('navegue/navegue',$data);
	}
	
	function carregaEmpreendimentos()
	{
		$this->load->model('navegue_model', 'model');
		$this->load->helper('xml');
		$imoveis = $this->model->getImoveis();
		
		 $dom = xml_dom();
		 
		 $markers = xml_add_child($dom ,'markers') ;
		
		foreach ($imoveis as $imovel):
			//$url = $this->utilidades->sanitize_title_with_dashes(@$imovel->empreendimento);
			
			$newUrl = $imovel->empreendimento."-".$imovel->cidade."-".$imovel->estado;
			$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
			$href = site_url().'imoveis/'.$imovel->id_empreendimento.'/'.$url;
			$link = "<a href='$href'>Saiba mais</a>";
			 $marker = xml_add_child ( $markers , 'marker' , '');		
			 xml_add_attribute ( $marker , 'name' , $imovel->empreendimento);
			 xml_add_attribute ( $marker , 'chamada' , $imovel->chamada_empreendimento);
			 xml_add_attribute ( $marker , 'logo' , $imovel->logotipo);
			 xml_add_attribute ( $marker , 'link' , $link );
			 xml_add_attribute ( $marker , 'lat' , $imovel->latitude );
			 xml_add_attribute ( $marker , 'lng' , $imovel->longitude );
			 xml_add_attribute ( $marker , 'type' ,$imovel->id_empresa);
		endforeach;
		
		
		return xml_print($dom) ; 
		 
		
	}
	
	function carregaEmpreendimentoId($id)
	{
		$this->load->model('navegue_model', 'model');
		$this->load->helper('xml');
		$imovel = $this->model->getImovel($id);
		
		//$url = $this->utilidades->sanitize_title_with_dashes(@$imovel->empreendimento);
		$newUrl = $imovel->empreendimento."-".$imovel->cidade."-".$imovel->estado;
		$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
		
		$href = site_url().'imoveis/'.$imovel->id_empreendimento.'/'.$url;
		$link = "<a href='$href'>Saiba mais</a>";
		 $dom = xml_dom();
		 
		 $markers = xml_add_child($dom ,'markers') ;
		
		
			 $marker = xml_add_child ( $markers , 'marker' , '');		
			 xml_add_attribute ( $marker , 'name' , $imovel->empreendimento);
			 xml_add_attribute ( $marker , 'chamada' , $imovel->chamada_empreendimento);
			 xml_add_attribute ( $marker , 'logo' , $imovel->logotipo);
			 xml_add_attribute ( $marker , 'link' , $link );
			 xml_add_attribute ( $marker , 'lat' , $imovel->latitude );
			 xml_add_attribute ( $marker , 'lng' , $imovel->longitude );
			 xml_add_attribute ( $marker , 'type' ,$imovel->id_empresa);
			 xml_add_attribute ( $marker , 'pin' ,$imovel->pin);
		
		
		
		return xml_print($dom) ; 
		 
		
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */