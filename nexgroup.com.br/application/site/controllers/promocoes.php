<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocoes extends CI_Controller {

	public function index()
	{
		$this->load->model('promocoes_model', 'model');
	 			
		$data = array(
			//'visitados'				=> $this->imoveis_visitados->getImoveisVisitados(),
			//'estados'				=> $this->model->getEstados()
		);
		
		$this->load->view('promocoes/promocoes',$data);
	}
 
 function tres_anos () {
     $this->load->view('promocoes/promocao-tres-anos');
 }
	
	function email_marketing ()
	{
		$this->load->view('promocoes/email_marketing');
	}
	
	function EnviaDados ()
	{
		//echo "oi";
		
		@session_start();
		if(!$this->agent->is_robot()){
			
			$this->load->model('promocoes_model', 'model');
			$data = array (
				
				'nome'				=> $this->input->post('nome'),
				'email'				=> $this->input->post('email'),
				'setor'				=> $this->input->post('setor'),
				'interesse'			=> $this->input->post('interesse'),
				'origem'			=> $_SESSION['origem'],
				'url'				=> $_SESSION['url'],
				'ip'				=> $this->input->ip_address(),
				'user_agent'		=> $this->input->user_agent()
	
			);
			
			$this->model->setPromocaoDescontos($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'mail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			//Inicio da Mensagem
				$nome				= $this->input->post('nome');
				$email				= $this->input->post('email');
				$setor				= $this->input->post('setor');
				$interesse			= $this->input->post('interesse');
				$data				= date("d/m/Y H:i:s");
			
			ob_start();
			
			?>
				<html>
					<head>
						<title>Nex Group</title>
					</head>
					<body>
						<table>
							<tr align="center">
					 			<td align="center" colspan="2">
					 				<a target="_blank" href="http://www.nexgroup.com.br">
					 					<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
					 				</a>
					 			</td>
							 </tr>
							<tr>
								<td colspan='2'>Descontos na sua direção enviado em <?php echo $data; ?></td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td>Nome:</td>
								<td><?php echo @$nome; ?></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><?php echo @$email; ?></td>
							</tr>
							<tr>
								<td>Setor:</td>
								<td><?php echo @$setor; ?></td>
							</tr>
							<tr>
								<td>Interesse:</td>
								<td><?php echo @$interesse; ?></td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
							</tr>
						</table>
					</body>
				</html>
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");
			
			/*
			$this->email->to('saldanha@capa.com.br');
			$this->email->to('lissandro@nexgroup.com.br');
			$this->email->to('gustavo.ci@nexgroup.com.br');
			$this->email->to('atendimento@divex.com.br');
			*/
			
			//$list = array('bruno.freiberger@divex.com.br', 'freibergergarcia@gmail.com');
			$list = array('saldanha@capa.com.br', 'lissandro@nexgroup.com.br', 'gustavo.ci@nexgroup.com.br');
			$this->email->to($list);
			$this->email->cc('vendas@nexvendas.com.br');
				
			$this->email->subject('Descontos na sua direção - NEX GROUP');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
		}
		
	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */