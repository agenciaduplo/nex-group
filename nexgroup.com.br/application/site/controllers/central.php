<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Central extends CI_Controller {

    public function index() {
        $this->load->model('contatos_model', 'model');
        $this->load->model('imoveis_model', 'modelImoveis');

        $tags = $this->model->getTags(4);
        if ($tags->title)
            $title = $tags->title;
        else
            $title = 'Central de Vendas - NEX GROUP';

		@session_start();
		
		if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
		} else if(isset($_SESSION['origem'])){
			$ref = $_SESSION['origem'];
			
		}else if(isset($_SERVER['HTTP_REFERER'])) {
			
			$ref = @$_SERVER['HTTP_REFERER'];
			
			
		} else {
			$ref = 'direto';
		}
		
        $data = array(
            'title' => $title,
            'keywords' => $tags->keywords,
            'description' => $tags->description,
            'page' => "central-de-vendas",
            'visitados' => $this->imoveis_visitados->getImoveisVisitados(),
            'empreendimentos' => $this->model->getEmpreendimentos(),
            'estados' => '',
			'ref' => $ref
        );

        $this->load->view('central/central', $data);
    }

    public function ligamos($id = '') {
        $this->load->model('contatos_model', 'model');

        $data = array(
            'title' => 'Ligamos para Você | Nexgroup',
            'empreendimentos' => $this->model->getEmpreendimentos(),
            'page' => "ligamos-para-voce",
            'id_empreendimento' => $id,
            'estados' => ''
        );

        $this->load->view('central/atendimentoPhone', $data);
    }

    public function conversao() {
        $this->load->model('contatos_model', 'model');

        $data = array(
            'title' => 'Conversao'
        );

        //if ($_SESSION['url'] == "google-adwords__cpc____maxhaus") {
        $this->load->view('adwords/conversao', $data);
        //}
    }

    public function email($id = '') {
        $this->load->model('contatos_model', 'model');

        $data = array(
            'title' => 'Atendimento por email | Nexgroup',
            'empreendimentos' => $this->model->getEmpreendimentos(),
            'page' => "atendimento-por-email",
            'id_empreendimento' => $id,
            'estados' => ''
        );

        $this->load->view('central/atendimentoEmail', $data);
    }

    public function naoEncontrou() {
        $this->load->model('contatos_model', 'model');

        $tags = $this->model->getTags(19);
        if ($tags->title)
            $title = $tags->title;
        else
            $title = 'Não encontrou seu imóvel | Nexgroup';

        $data = array(
            'title' => $title,
            'keywords' => $tags->keywords,
            'description' => $tags->description,
            'page' => "nao-encontrou-seu-imóvel",
            'estados' => ''
        );

        $this->load->view('nao-encontrei/nao-encontrei', $data);
    }

    function enviarAtendimento() {
        if (!$this->agent->is_robot()) {
            $this->load->model('contatos_model', 'model');
            @session_start();
            $data = array(
                'nome' => $this->input->post('nome'),
                'email' => $this->input->post('email'),
                'descricao' => $this->input->post('descricao'),
                'telefone' => $this->input->post('telefone'),
                'id_empreendimento' => $this->input->post('empreendimento'),
                'forma_contato' => $this->input->post('contato'),
                'origem' => $_SESSION['origem'],
                'url' => $_SESSION['url'],
                'ip' => $this->input->ip_address(),
                'user_agent' => $this->input->user_agent()
            );

            $this->model->setAtendimento($data);
            $id_empreendimento = $this->input->post('empreendimento');
            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            //Inicio da Mensagem
            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $tel = $this->input->post('telefone');
            $mensagem = $this->input->post('descricao');
            $forma_contato = $this->input->post('contato');
            $empreendimento = @$this->model->getEmpreendimento($id_empreendimento);
            $data = date("d/m/Y H:i:s");

            ob_start();
            ?>
            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Atendimento enviado em <?php echo $data; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nome:</td>
                            <td><?php echo $nome; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><?php echo $email; ?></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td>
                            <td><?php echo $tel; ?></td>
                        </tr>
                        <tr>
                            <td>Forma de Contato:</td>
                            <td><?php echo $forma_contato; ?></td>
                        </tr>
                        <tr>
                            <td>Empreendimento:</td>
                            <td><?php echo @$empreendimento->empreendimento; ?></td>
                        </tr>
                        <tr>
                            <td>Mensagem:</td>
                            <td><?php echo $mensagem; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
                        </tr>
                    </table>
                </body>
            </html>
            <?

            $conteudo = ob_get_contents();
            ob_end_clean();
            //Fim da Mensagem

            $tipo 		= 1;
            @$emails 	= $this->model->getEnviaEmail($id_empreendimento, $tipo);

            if($nome == 'teste123' || $nome == 'TESTE123')
            {
            $this->email->to('testes@divex.com.br');	
            }
            else if(@$emails)
            {
            foreach ($emails as $email)
            {
            if($email->tipo == 'para')
            {
            $list[] = $email->email;
            }
            else if($email->tipo == 'cc')
            {	
            $list_cc[] = $email->email;
            }
            else
            {
            $list_bcc[] = $email->email;
            }
            }

            if($list)
            {
            $this->email->to($list);
            }
            if($list_cc)
            {
            $this->email->cc($list_cc);
            }
            if($list_bcc)
            {
            $this->email->bcc($list_bcc);
            }		

            }
            else
            {	
            $this->email->to('mellobandeira@yahoo.com.br');	
            $this->email->to('saldanha@capa.com.br');	
            $this->email->bcc('testes@divex.com.br');
            }

            $this->email->from("noreply@nexgroup.com.br", "Nex Group");
            $this->email->subject('Atendimento enviado via site');
            $this->email->message("$conteudo");

            $this->email->send();

            //===================================================================
            //Termina o envio do email	

            }
            }

            function enviarAtendimentoTelefone()
            {
            if(!$this->agent->is_robot()){
            $this->load->model('contatos_model', 'model');
            @session_start();
            $data = array (

            'nome'				=> $this->input->post('nome'),
            'email'				=> $this->input->post('email'),
            'descricao'			=> $this->input->post('descricao'),
            'telefone'			=> $this->input->post('telefone'),
            'id_empreendimento'	=> $this->input->post('empreendimento'),
            'forma_contato'		=> $this->input->post('contato'),
            'origem'			=> $_SESSION['origem'],
            'url'				=> $_SESSION['url'],
            'ip'				=> $this->input->ip_address(),
            'user_agent'		=> $this->input->user_agent()

            );

            $this->model->setAtendimentoTelefone($data);

            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] 	= 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $id_empreendimento = $this->input->post('empreendimento');
            //Inicio da Mensagem
            $nome				= $this->input->post('nome');
            $email				= $this->input->post('email');
            $tel				= $this->input->post('telefone');
            $mensagem			= $this->input->post('descricao');
            $forma_contato		= $this->input->post('contato');
            $empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
            $data				= date("d/m/Y H:i:s");

            ob_start();

            ?>
            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Atendimento enviado em <?php echo $data; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nome:</td>
                            <td><?php echo $nome; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><?php echo $email; ?></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td>
                            <td><?php echo $tel; ?></td>
                        </tr>
                        <tr>
                            <td>Forma de Contato:</td>
                            <td><?php echo $forma_contato; ?></td>
                        </tr>
            <?php if (@$empreendimento->empreendimento): ?>
                            <tr>
                                <td>Empreendimento:</td>
                                <td><?php echo @$empreendimento->empreendimento; ?></td>
                            </tr>
            <?php endif; ?>
                        <tr>
                            <td>Mensagem:</td>
                            <td><?php echo $mensagem; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
                        </tr>
                    </table>
                </body>
            </html>
            <?

            $conteudo = ob_get_contents();
            ob_end_clean();
            //Fim da Mensagem

            $tipo 		= 1;
            @$emails 	= $this->model->getEnviaEmail($id_empreendimento, $tipo);

            if($nome == 'teste123' || $nome == 'TESTE123')
            {
            $this->email->to('testes@divex.com.br');	
            }
            else if(@$emails)
            {
            foreach ($emails as $email)
            {
            if($email->tipo == 'para')
            {
            $list[] = $email->email;
            }
            else if($email->tipo == 'cc')
            {	
            $list_cc[] = $email->email;
            }
            else
            {
            $list_bcc[] = $email->email;
            }
            }

            if($list)
            {
            $this->email->to($list);
            }
            if($list_cc)
            {
            $this->email->cc($list_cc);
            }
            if($list_bcc)
            {
            $this->email->bcc($list_bcc);
            }		

            }
            else
            {	
            $this->email->to('mellobandeira@yahoo.com.br');	
            $this->email->to('saldanha@capa.com.br');	
            $this->email->bcc('testes@divex.com.br');
            }

            $this->email->from("noreply@nexgroup.com.br", "Nex Group");
            $this->email->subject('Ligamos para você enviado via site');
            $this->email->message("$conteudo");

            $this->email->send();

            //===================================================================
            //Termina o envio do email	
            }
            }

            function enviarAtendimentoEmail()
            {
            if(!$this->agent->is_robot())
            {
            $this->load->model('contatos_model', 'model');
            @session_start();
            $data = array (

            'nome'				=> $this->input->post('nome'),
            'email'				=> $this->input->post('email'),
            'descricao'			=> $this->input->post('descricao'),
            'telefone'			=> $this->input->post('telefone'),
            'id_empreendimento'	=> $this->input->post('empreendimento'),
            'forma_contato'		=> $this->input->post('contato'),
            'origem'			=> $_SESSION['origem'],
            'url'				=> $_SESSION['url'],
            'ip'				=> $this->input->ip_address(),
            'user_agent'		=> $this->input->user_agent()

            );

            $this->model->setAtendimentoEmail($data);
            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] 	= 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $id_empreendimento 	= $this->input->post('empreendimento');
            //Inicio da Mensagem
            $nome				= $this->input->post('nome');
            $email				= $this->input->post('email');
            $tel				= $this->input->post('telefone');
            $mensagem			= $this->input->post('descricao');
            $empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
            $forma_contato		= $this->input->post('contato');
            $data				= date("d/m/Y H:i:s");

            ob_start();

            ?>
            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Atendimento enviado em <?php echo $data; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nome:</td>
                            <td><?php echo $nome; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><?php echo $email; ?></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td>
                            <td><?php echo $tel; ?></td>
                        </tr>
                        <tr>
                            <td>Forma de Contato:</td>
                            <td><?php echo $forma_contato; ?></td>
                        </tr>
            <?php if (@$empreendimento->empreendimento): ?>
                            <tr>
                                <td>Empreendimento:</td>
                                <td><?php echo @$empreendimento->empreendimento; ?> - <?php echo @$empreendimento->cidade; ?></td>
                            </tr>
            <?php endif; ?>
                        <tr>
                            <td>Mensagem:</td>
                            <td><?php echo $mensagem; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
                        </tr>
                    </table>
                </body>
            </html>
            <?

            $conteudo = ob_get_contents();
            ob_end_clean();
            //Fim da Mensagem

            $tipo 		= 1;
            @$emails 	= $this->model->getEnviaEmail($id_empreendimento, $tipo);

            if($nome == 'teste123' || $nome == 'TESTE123')
            {
            $this->email->to('testes@divex.com.br');	
            }
            else if(@$emails)
            {
            foreach ($emails as $email)
            {
            if($email->tipo == 'para')
            {
            $list[] = $email->email;
            }
            else if($email->tipo == 'cc')
            {	
            $list_cc[] = $email->email;
            }
            else
            {
            $list_bcc[] = $email->email;
            }
            }

            if($list)
            {
            $this->email->to($list);
            }
            if($list_cc)
            {
            $this->email->cc($list_cc);
            }
            if($list_bcc)
            {
            $this->email->bcc($list_bcc);
            }		

            }
            else
            {	
            $this->email->to('mellobandeira@yahoo.com.br');	
            $this->email->to('saldanha@capa.com.br');	
            $this->email->bcc('testes@divex.com.br');
            }

            $this->email->from("noreply@nexgroup.com.br", "Nex Group");
            $this->email->subject('Atendimento por email enviado via site');
            $this->email->message("$conteudo");

            $this->email->send();

            //===================================================================
            //Termina o envio do email	

            }
            }

            function enviarAtendimentoUnico()
            {
            if(!$this->agent->is_robot() && $_POST) {

            $this->load->model('contatos_model', 'model');
            $this->db->reconnect();

            @session_start();
            $data = array (
            'nome'				=> @$this->input->post('nome'),
            'email'				=> @$this->input->post('email'),
            'descricao'			=> @$this->input->post('descricao'),
            'telefone'			=> @$this->input->post('telefone'),
            'id_empreendimento'	=> @$this->input->post('empreendimento'),
            'forma_contato'		=> @$this->input->post('contato'),
            'origem'			=> @$_SESSION['origem'],
            'url'				=> @$_SESSION['url'],
            'ip'				=> @$this->input->ip_address(),
            'user_agent'		=> @$this->input->user_agent()
            );

            //$interesse 			= $this->model->setInteresse($data);
            $atendimento 		= $this->model->setAtendimentoUnico($data);
            $id_empreendimento 	= $this->input->post('empreendimento');
            $empreendimento		= $this->model->getEmpreendimento($id_empreendimento);

            $data_envio 		= date("d/m/Y H:i:s");
            $nome 				= $this->input->post('nome');
            $email_envio		= $this->input->post('email');
            $comentarios		= $this->input->post('descricao');
            $telefone			= $this->input->post('telefone');
            $ip 				= $this->input->ip_address();

            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] 	= 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $grupos = $this->model->getGrupos($id_empreendimento);
            $total = 0;
            $total = count($grupos);

            if($total == 1) // caso só tenho um grupo cadastrado
            {
            $emails = $this->model->getGruposEmails($id_empreendimento, '001');

            $emails_txt = "";
            foreach ($emails as $email)
            {
            $grupo = $email->grupo;
            $list[] = $email->email;

            $emails_txt = $emails_txt.$email->email.", ";
            }
            $this->model->grupo_atendimento($atendimento,$grupo, $emails_txt);

            }
            else if($total > 1) // caso tenha mais de um grupo cadastrado
            {
            foreach ($grupos as $row):

            if($row->rand == 1):
            $atual = $row->ordem;
            if($atual == $total):

            $this->model->updateRand($id_empreendimento, '001');
            $emails = $this->model->getGruposEmails($id_empreendimento, '001');

            $emails_txt = "";
            foreach ($emails as $email)
            {
            $grupo = $email->grupo;
            $list[] = $email->email;

            $emails_txt = $emails_txt.$email->email.", ";
            }
            $this->model->grupo_atendimento($atendimento,$grupo, $emails_txt);

            else:

            $atualizar = "00".$atual+1;
            $this->model->updateRand($id_empreendimento, $atualizar);
            $emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

            $emails_txt = "";
            foreach ($emails as $email)
            {
            $grupo = $email->grupo;
            $list[] = $email->email;

            $emails_txt = $emails_txt.$email->email.", ";
            }
            $this->model->grupo_atendimento($atendimento,$grupo, $emails_txt);

            endif;	
            endif;
            endforeach;
            }
            /* envia email via roleta */

            if($data['nome'] == "Teste123") {
            $this->email->to('testes@divex.com.br');
            } else {
            $this->email->to($list);
            }

            $empNome = strtoupper($empreendimento->empreendimento);

            ob_start();

            ?>

            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Atendimento enviado em <?php echo $data_envio; ?> via Portal</td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>

                        <tr>
                            <td colspan="2">Atendimento enviado por <strong><?= @$nome ?></strong> em <strong><?= $data_envio ?></strong> as <strong><?= date('H:i:s') ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%">&nbsp;</td>
                            <td width="70%">&nbsp;</td>
                        </tr>
            <?php if (@$empreendimento->id_empreendimento == 81): ?>
                            <tr>
                                <td width="30%">Empreendimento:</td>
                                <td width="70%">Nenhum empreendimento selecionado pelo prospect!</td>
                            </tr>
            <?php else: ?>
                            <tr>
                                <td width="30%">Empreendimento:</td>
                                <td width="70%"><?= @$empreendimento->empreendimento ?> / <?= @$empreendimento->cidade ?></td>
                            </tr>
            <?php endif; ?>
                        <tr>
                            <td width="30%">IP:</td>
                            <td><strong><?= @$ip ?></strong></td>
                        </tr>				  
                        <tr>
                            <td width="30%">E-mail:</td>
                            <td><strong><?= @$email_envio ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%">Telefone:</td>
                            <td><strong><?= @$telefone ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%" valign="top">Comentários:</td>
                            <td valign="top"><strong><?= @$comentarios ?></strong></td>
                        </tr>																  
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
                        </tr>
                    </table>
                </body>
            </html>

            <?php
            $html_email = ob_get_contents();
            ob_end_clean();

            $this->email->bcc('testes@divex.com.br');
            $this->email->from("noreply@nexgroup.com.br", "$empNome - Nex Group");
            $this->email->subject('Atendimento enviado via Portal');
            $this->email->message("$html_email");

            $this->email->send();
        }
    }

    function enviarAtendimentoUnicoDhz() {
        if (!$this->agent->is_robot() && $_POST) {

            $this->load->model('contatos_model', 'model');
            $this->db->reconnect();

            @session_start();
            $data = array(
                'nome' => @$this->input->post('nome'),
                'email' => @$this->input->post('email'),
                'descricao' => @$this->input->post('descricao'),
                'telefone' => @$this->input->post('telefone'),
                'id_empreendimento' => @$this->input->post('empreendimento'),
                'forma_contato' => @$this->input->post('contato'),
                'origem' => @$this->input->post('origem'),
                'url' => @$this->input->post('url'),
                'ip' => @$this->input->ip_address(),
                'user_agent' => @$this->input->user_agent()
            );

            //$interesse 			= $this->model->setInteresse($data);
            $atendimento = $this->model->setAtendimentoUnico($data);
            $id_empreendimento = $this->input->post('empreendimento');
            $empreendimento = $this->model->getEmpreendimento($id_empreendimento);

            $data_envio = date("d/m/Y H:i:s");
            $nome = $this->input->post('nome');
            $email_envio = $this->input->post('email');
            $comentarios = $this->input->post('descricao');
            $telefone = $this->input->post('telefone');
            $ip = $this->input->ip_address();

            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $grupos = $this->model->getGrupos($id_empreendimento);
            $total = 0;
            $total = count($grupos);

            if ($total == 1) { // caso só tenho um grupo cadastrado
                $emails = $this->model->getGruposEmails($id_empreendimento, '001');

                $emails_txt = "";
                foreach ($emails as $email) {
                    $grupo = $email->grupo;
                    $list[] = $email->email;

                    $emails_txt = $emails_txt . $email->email . ", ";
                }
                $this->model->grupo_atendimento($atendimento, $grupo, $emails_txt);
            } else if ($total > 1) { // caso tenha mais de um grupo cadastrado
                foreach ($grupos as $row):

                    if ($row->rand == 1):
                        $atual = $row->ordem;
                        if ($atual == $total):

                            $this->model->updateRand($id_empreendimento, '001');
                            $emails = $this->model->getGruposEmails($id_empreendimento, '001');

                            $emails_txt = "";
                            foreach ($emails as $email) {
                                $grupo = $email->grupo;
                                $list[] = $email->email;

                                $emails_txt = $emails_txt . $email->email . ", ";
                            }
                            $this->model->grupo_atendimento($atendimento, $grupo, $emails_txt);

                        else:

                            $atualizar = "00" . $atual + 1;
                            $this->model->updateRand($id_empreendimento, $atualizar);
                            $emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

                            $emails_txt = "";
                            foreach ($emails as $email) {
                                $grupo = $email->grupo;
                                $list[] = $email->email;

                                $emails_txt = $emails_txt . $email->email . ", ";
                            }
                            $this->model->grupo_atendimento($atendimento, $grupo, $emails_txt);

                        endif;
                    endif;
                endforeach;
            }
            /* envia email via roleta */

            if ($data['nome'] == "Teste123") {
                $this->email->to('testes@divex.com.br');
            } else {
                $this->email->to($list);
            }

            $empNome = strtoupper($empreendimento->empreendimento);

            ob_start();
            ?>

            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Atendimento enviado em <?php echo $data_envio; ?> via Portal</td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>

                        <tr>
                            <td colspan="2">Atendimento enviado por <strong><?= @$nome ?></strong> em <strong><?= $data_envio ?></strong> as <strong><?= date('H:i:s') ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%">&nbsp;</td>
                            <td width="70%">&nbsp;</td>
                        </tr>
            <?php if (@$empreendimento->id_empreendimento == 81): ?>
                            <tr>
                                <td width="30%">Empreendimento:</td>
                                <td width="70%">Nenhum empreendimento selecionado pelo prospect!</td>
                            </tr>
            <?php else: ?>
                            <tr>
                                <td width="30%">Empreendimento:</td>
                                <td width="70%"><?= @$empreendimento->empreendimento ?> / <?= @$empreendimento->cidade ?></td>
                            </tr>
            <?php endif; ?>
                        <tr>
                            <td width="30%">IP:</td>
                            <td><strong><?= @$ip ?></strong></td>
                        </tr>				  
                        <tr>
                            <td width="30%">E-mail:</td>
                            <td><strong><?= @$email_envio ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%">Telefone:</td>
                            <td><strong><?= @$telefone ?></strong></td>
                        </tr>
                        <tr>
                            <td width="30%" valign="top">Comentários:</td>
                            <td valign="top"><strong><?= @$comentarios ?></strong></td>
                        </tr>																  
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
                        </tr>
                    </table>
                </body>
            </html>

            <?php
            $html_email = ob_get_contents();
            ob_end_clean();

            $this->email->bcc('testes@divex.com.br');
            $this->email->from("noreply@nexgroup.com.br", "$empNome - Nex Group");
            $this->email->subject('Atendimento enviado via Portal');
            $this->email->message("$html_email");

            $this->email->send();
        }
    }

    function EnviarnaoEncontrou() {
        if (!$this->agent->is_robot()) {
            $this->load->model('contatos_model', 'model');
            @session_start();
            $data = array(
                'nome' => $this->input->post('nome'),
                'email' => $this->input->post('email'),
                'descricao' => $this->input->post('descricao'),
                'telefone' => $this->input->post('telefone'),
                'forma_contato' => $this->input->post('contato'),
                'origem' => $_SESSION['origem'],
                'url' => $_SESSION['url'],
                'ip' => $this->input->ip_address(),
                'user_agent' => $this->input->user_agent()
            );

            $this->model->setAtendimento($data);
            //Inicia o envio do email
            //===================================================================

            $this->load->library('email');

            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            //Inicio da Mensagem
            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $tel = $this->input->post('telefone');
            $mensagem = $this->input->post('descricao');
            $forma_contato = $this->input->post('contato');
            $data = date("d/m/Y H:i:s");

            ob_start();
            ?>
            <html>
                <head>
                    <title>Nex Group</title>
                </head>
                <body>
                    <table>
                        <tr align="center">
                            <td align="center" colspan="2">
                                <a target="_blank" href="http://www.nexgroup.com.br">
                                    <img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2'>Não encontrei imóvel enviado em <?php echo $data; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nome:</td>
                            <td><?php echo $nome; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><?php echo $email; ?></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td>
                            <td><?php echo $tel; ?></td>
                        </tr>
                        <tr>
                            <td>Forma de Contato:</td>
                            <td><?php echo $forma_contato; ?></td>
                        </tr>
                        <tr>
                            <td>Mensagem:</td>
                            <td><?php echo $mensagem; ?></td>
            </tr>
            <tr>
                <td colspan='2'>&nbsp;</td>
            </tr>
            <tr>
                <td colspan='2'>&nbsp;</td>
            </tr>
            <tr>
                <td colspan='2'><a href='http://www.nexgroup.com.br'>http://www.nexgroup.com.br</a></td>
            </tr>
        </table>
    </body>
</html>
<?

$conteudo = ob_get_contents();
ob_end_clean();
//Fim da Mensagem

$tipo 		= 1;
@$emails 	= $this->model->getEnviaEmail($id_empreendimento, $tipo);

if(@$emails)
{

foreach ($emails as $email)
{
if($email->tipo == 'para')
{
$list[] = $email->email;
}
else if($email->tipo == 'cc')
{	
$list_cc[] = $email->email;
}
else
{
$list_bcc[] = $email->email;
}
}

if(@$list)
{
$this->email->to($list);
}
if(@$list_cc)
{
$this->email->cc($list_cc);
}
if(@$list_bcc)
{
$this->email->bcc($list_bcc);
}

}
else
{
if($nome=='teste123')
{
$this->email->to('testes@divex.com.br');	
}
else
{	
$this->email->to('mellobandeira@yahoo.com.br');	
$this->email->to('saldanha@capa.com.br');	
$this->email->bcc('testes@divex.com.br');
}
}

$this->email->from("noreply@nexgroup.com.br", "Nex Group");
$this->email->subject('Não encontrei imóvel enviado via site');
$this->email->message("$conteudo");

$this->email->send();

//===================================================================
//Termina o envio do email	
}
}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */