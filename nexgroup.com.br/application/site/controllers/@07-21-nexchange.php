<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nexchange extends CI_Controller {

	
	function index($envio = "") {

		@session_start();

		$_SESSION['NEXCHANGE']['REFERER'] = @$_SERVER['HTTP_REFERER'];

		$data = array(
			'title' => "Nexchange",
		);

		if($envio == "cadastro-enviado") 
		{
			$data['liberar_mensagem'] = 1;

			$this->load->view('nexchange/nexchange.retorno.php',$data);
		}
		else 
		{
			$data['liberar_mensagem'] = 0;

			$this->load->view('nexchange/nexchange',$data);
		}
	}

	function enviaNexChange($campanha = "") {

		if(!$this->agent->is_robot()){
			$this->load->model('contatos_model','model');
			//Inicia o envio do email
			//===================================================================

			$this->load->library('email');

			$config['protocol'] = 'sendmail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n";

			$this->email->initialize($config);

			@session_start();

			$retorno = "json";
			if($campanha != ""){
				$campanha_url = $campanha;
				$retorno = "html";
			} else {
				$retorno = "json";
				if(@$_SESSION['url'] != "") {
					$campanha_url = $_SESSION['url'];
				}
			}

			$nome	= $this->input->post('nome');
			$email	= $this->input->post('email');
			$ddd	= $this->input->post('telefone_area');
			$tel	= $this->input->post('telefone');
			// $msg			= $this->input->post('comentarios');

			$ddd = ($ddd != "DDD") ? $ddd : "";
			$tel = ($tel != "TELEFONE") ? " ".$tel : "";

			$telefone = $ddd . $tel;
			$cidade	  = $this->input->post('cidade');
			$cidade   = ($cidade == "CIDADE:" || $cidade == "") ? "NÃO INFORMADA" : $cidade;
			$data	  = date("d/m/Y H:i:s");

			$email_nex = "vendas@nexvendas.com.br";			// 3 envios para nex
			$email_out = "lancamentos@vendasbbsul.com.br";	// 1 envio para fora

			// $email_nex = "teste1@divex.com.br";
			// $email_out = "teste2@divex.com.br";

			$email_go	= $email_nex;
			$id_cidade = 4237;

			$url = isset($_SESSION['url']) ? $_SESSION['url'] : '______';
			$url = explode("__",$url);

			$utm_source 	= $url[0];
			$utm_medium   = $url[1];
			$utm_campaign = $url[3];

			if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
				$email_for =  $this->model->verificaLastEmailSendFor(1, "interesses",$campanha_url);
				foreach($email_for as $emf){
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$email_go = $email_out;
						break;
					}else{
						$email_go = $email_nex;
						break;
					}
				}
			}else{ // Porto Alegre | 1 quarto dos interesses para fora.
				$email_for =  $this->model->verificaLastEmailSendFor(3, "interesses");
				$i = 0;
				$flag = false;

				foreach($email_for as $emf){
					++$i;
					if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
						$flag = true;
					}else if($emf->email_enviado_for == $email_out && $i = 1){
						$email_go = $email_nex;
						break;
					}else{
						$flag = false;
					}

					if($flag){
						$email_go = $email_out;
					}else{
						$email_go = $email_nex;
					}
				}
			}

			$data = array(
				'data_envio' 				=> date("Y-m-d H:i:s"),
				'nome'							=> mb_strtoupper($nome),
				'email'							=> mb_strtoupper($email),
				'comentarios'	 			=> mb_strtoupper($cidade),
				'telefone'					=> $telefone,
				'id_empreendimento'	=> 103,
				'id_estado'					=> 21,
				'forma_contato'			=> "Email",
				'origem'						=> @$_SESSION['NEXCHANGE']['REFERER'],
				'url'								=> $campanha_url,
				'ip'								=> $this->input->ip_address(),
				'user_agent'				=> $this->input->user_agent(),
				'email_enviado_for' => $email_go
				);


			$this->model->setInteresse($data);

			ob_start();

			?>
			<html>
			<head>
				<title>Nex Group</title>
			</head>
			<body>
				<table>
					<tr>
						<td colspan="2">
							<a title="Acesse o portal Nex Group" href="http://www.nexgroup.com.br" target="_blank" >
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" />
							</a>
						</td>
					</tr>
					<tr>
						<td colspan='2'>Enviado em <?php echo date("d/m/Y") . " às " . date("H:i"); ?></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td>Nome:</td>
						<td><?php echo $nome; ?></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><?php echo $email; ?></td>
					</tr>
					<tr>
						<td>Telefone:</td>
						<td><?php echo $telefone; ?></td>
					</tr>
					<tr>
						<td>Cidade:</td>
						<td><?php echo $cidade; ?></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='2'>
							<a  title="Acesse o portal Nex Group" href='http://www.nexgroup.com.br' target="_blank">http://www.nexgroup.com.br</a>
						</td>
					</tr>
				</table>
			</body>
			</html>
			<?

			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");

			// if($email == "anita.chiele@divex.com.br" || $email == "gabriel.oribes@divex.com.br") {
			// 	$this->email->to('anita.chiele@divex.com.br, gabriel.oribes@divex.com.br');
			// }else{
			$this->email->to("{$email_go}");
			$this->email->bcc("gabriel.oribes@divex.com.br, andre.wille@divex.com.br, ricardo@divex.com.br");
			// }
		
			$this->email->subject('Nex Group - Novo contato Nex Change');
			$this->email->message("$conteudo");

			//===================================================================
			//Termina o envio do email	

			if($this->email->send())
				$response = array("erro" => 0, "msg" => "teste");
			else
				$response = array("erro" => 1, "msg" => "teste");

			if($retorno == "json") {
				echo json_encode($response);
				exit;
			} else if($retorno == "html") {
				redirect('nexchange/cadastro-enviado');
			} else {
				redirect('home');
			}

		}
	}



}

/* End of file nexchange.php */
/* Location: ./application/controllers/nexchange.php */