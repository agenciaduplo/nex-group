<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nexchange extends CI_Controller {

	
	function index($envio = "") {

		@session_start();

		$_SESSION['NEXCHANGE']['REFERER'] = @$_SERVER['HTTP_REFERER'];

		$data = array(
			'title' => "Nexchange",
		);

		if($envio == "cadastro-enviado") 
		{
			$data['liberar_mensagem'] = 1;

			$this->load->view('nexchange/nexchange.retorno.php',$data);
		}
		else 
		{
			$data['liberar_mensagem'] = 0;

			$this->load->view('nexchange/nexchange',$data);
		}
	}

	function enviaNexChange($campanha = "") {

		if(!$this->agent->is_robot()){
			$this->load->model('contatos_model','model');
			//Inicia o envio do email
			//===================================================================

			$this->load->library('email');

			$config['protocol'] = 'sendmail';
			$config['mailtype'] = 'html';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n";

			$this->email->initialize($config);

			@session_start();

			$retorno = "json";
			if($campanha != ""){
				$campanha_url = $campanha;
				$retorno = "html";
			} else {
				$retorno = "json";
				if(@$_SESSION['url'] != "") {
					$campanha_url = $_SESSION['url'];
				}
			}

			$nome  = $this->input->post('nome');
			$email = $this->input->post('email');
			$ddd   = $this->input->post('telefone_area');
			$tel   = $this->input->post('telefone');

			$ddd = ($ddd != "DDD") ? $ddd : "";
			$tel = ($tel != "TELEFONE") ? " ".$tel : "";

			$telefone = $ddd . $tel;
			$cidade	  = $this->input->post('cidade');
			$cidade   = ($cidade == "CIDADE:" || $cidade == "") ? "NÃO INFORMADA" : $cidade;
			$data	  = date("d/m/Y H:i:s");

			switch ($this->input->post('como_chegou')) {
				case '1':
					$como_chegou = "Tv Com";
					break;
				case '2':
					$como_chegou = "Rádio";
					break;
				case '3':
					$como_chegou = "Indicação";
					break;
				case '4':
					$como_chegou = "Folheto";
					break;
				case '5':
					$como_chegou = "Internet";
					break;
				case '6':
					$como_chegou = "Site da Nex Group";
					break;
				case '7':
					$como_chegou = "Anúncio em jornal/revista";
					break;
				default:
					$response = array("erro" => 1, "msg" => "Selecione uma opção de como você chegou aqui.");
					echo json_encode($response);
					exit;
			}

			// ROLETA DE EMAILS :

			// $email_nex = "vendas@nexvendas.com.br";			// 3 envios para nex
			// $email_out = "lancamentos@vendasbbsul.com.br";	// 1 envio para fora

			// $email_go  = $email_nex;
			// $id_cidade = 4237;

			// $url = isset($_SESSION['url']) ? $_SESSION['url'] : '______';
			// $url = explode("__",$url);

			// $utm_source   = $url[0];
			// $utm_medium   = $url[1];
			// $utm_campaign = $url[3];

			// if($utm_source != "" && $utm_medium != "" && $utm_campaign != ""){ // 50%
			// 	$email_for =  $this->model->verificaLastEmailSendFor(1, "interesses",$campanha_url);
			// 	foreach($email_for as $emf){
			// 		if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
			// 			$email_go = $email_out;
			// 			break;
			// 		}else{
			// 			$email_go = $email_nex;
			// 			break;
			// 		}
			// 	}
			// }else{ // Porto Alegre | 1 quarto dos interesses para fora.
			// 	$email_for =  $this->model->verificaLastEmailSendFor(3, "interesses");
			// 	$i = 0;
			// 	$flag = false;

			// 	foreach($email_for as $emf){
			// 		++$i;
			// 		if($emf->email_enviado_for == $email_nex || $emf->email_enviado_for == ""){
			// 			$flag = true;
			// 		}else if($emf->email_enviado_for == $email_out && $i = 1){
			// 			$email_go = $email_nex;
			// 			break;
			// 		}else{
			// 			$flag = false;
			// 		}

			// 		if($flag){
			// 			$email_go = $email_out;
			// 		}else{
			// 			$email_go = $email_nex;
			// 		}
			// 	}
			// }

			// ROLETA DE EMAILS END;

			$data = array(
				'data_envio' 		  => date("Y-m-d H:i:s"),
				'nome'				  => mb_strtoupper($nome),
				'email'				  => mb_strtoupper($email),
				'comentarios'	 	  => mb_strtoupper($cidade),
				'telefone'			  => $telefone,
				'id_empreendimento'	  => 103,
				'id_estado'			  => 21,
				'forma_contato'		  => "Email",
				'origem_form_interno' => $como_chegou, 
				'origem'			  => @$_SESSION['NEXCHANGE']['REFERER'],
				'url'				  => $campanha_url,
				'ip'				  => $this->input->ip_address(),
				'user_agent'		  => $this->input->user_agent(),
				'email_enviado_for'   => "vendas@nexvendas.com.br"
			);

			$this->model->setInteresse($data);

			ob_start();
			?>
			<html>
			<head>
				<title>Nex Group</title>
			</head>
			<body>
				<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
					<tr>
						<td style="background: #b72025;text-align: center;padding: 3px 0;">
							<a target="_blank" href="http://www.nexgroup.com.br">
								<img width="90px" border="0" alt="NEX GROUP" src="http://www.nexgroup.com.br/assets/site/img/logo-white.png" style="margin-top: 5px;">
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px">
							<h3 style="font-size:16px;margin:0;color: #333;">Enviado via nexchange.</h3><br/>
							Realizado em <?=date("d/m/Y H:i:sa")?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Nome:</strong> <?=$nome?></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Email:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$email?></span></td>
					</tr>
					<tr>
						<td style="padding-top:5px"><strong>Telefone:</strong> <span style="font-size: 16px;text-decoration: underline;"><?=$telefone?></span></td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Cidade:</strong> <?=$cidade?></td>
					</tr>
					<tr>
						<td style="padding-top:15px"><strong>Como chegou ao nexchange:</strong> <?=$como_chegou?></td>
					</tr>
					<tr>
						<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
							Resposta automática de retorno do envio via site. Não responda este email.<br/>
							Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
							http://www.nexgroup.com.br<br/>
							&copy; NEX GROUP <?=date('Y')?><br/>
						</td>
					</tr>
				</table>
			</body>
			</html>
			<?

			$conteudo = ob_get_contents();
			ob_end_clean();
			//Fim da Mensagem
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group");
			// $this->email->to("{$email_go}");
			$this->email->to("vendas@nexvendas.com.br");
			$this->email->bcc("nexduplo@gmail.com");
			$this->email->subject('Novo contato Nex Change');
			$this->email->message("$conteudo");

			//===================================================================
			//Termina o envio do email	

			if($this->email->send())
				$response = array("erro" => 0, "msg" => "teste");
			else
				$response = array("erro" => 1, "msg" => "teste");


			$this->load->library('curl');

			// $url = "http://nex.hypnobox.com.br/email.receber.php";

			// Start session (also wipes existing/previous sessions)
			$this->curl->create("http://nex.hypnobox.com.br/email.receber.php");

			$post_data = array(
				"id_produto" => 33,
				"referencia" => $campanha_url, 
				"nome" 		 => $nome, 
				"email" 	 => strtolower($email),
				"descricao"  => "Cidade: " . $cidade . "<br/>Como chegou: " . $como_chegou,
				"interesse"	 => "Nex Change"
			);

			if($ddd != "")
				$post_data["ddd_residencial"] = substr($ddd, 1);

			if($tel != "")
				$post_data["tel_residencial"] = $tel;


			$this->curl->post($post_data);
			$this->curl->execute();
				
			if($retorno == "json") {
				$response['curl_info'] = $this->curl->info; // array;
				echo json_encode($response);
				exit;
			} else if($retorno == "html") {
				redirect('nexchange/cadastro-enviado');
			} else {
				redirect('home');
			}

		}
	}



}

/* End of file nexchange.php */
/* Location: ./application/controllers/nexchange.php */