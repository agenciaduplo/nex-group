<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imoveis_model extends CI_Model {	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	//transferencia roleta
	//==============================================================================================================

	function getEmails ($id_empreendimento)
	{
		$sql = "SELECT *
				FROM emails
				WHERE id_empreendimento = $id_empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}

	function criarGrupo ($id_empreendimento, $nomeGrupo)
	{		
		$this->db->set('id_empreendimento', $id_empreendimento);
		$this->db->set('rand', '1');
		$this->db->set('nome', $nomeGrupo);
		$this->db->set('ordem', '001');
		$this->db->insert('grupos');

		return $this->db->insert_id();
	}

	function setEmailGrupo ($email, $id_grupo)
	{		
		$this->db->set('grupo', $id_grupo);
		$this->db->set('email', $email);
		$this->db->insert('grupos_emails');
	}

	function verificaGrupos ($id_empreendimento)
	{
		$sql = "SELECT *
				FROM grupos
				WHERE id_empreendimento = $id_empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();		
	}

	function addVisita($id_empreendimento){
		$sql = "UPDATE empreendimentos SET qtd_visita = qtd_visita + 1 WHERE id_empreendimento = $id_empreendimento";
		$query = $this->db->query($sql);
	}
	
	function addFavorito($id_empreendimento){
		$sql = "UPDATE empreendimentos SET qtd_favorito = qtd_favorito + 1 WHERE id_empreendimento = $id_empreendimento";
		$query = $this->db->query($sql);
	}
	
	function buscaTodosImoveis ()
	{		
		// $sql = "SELECT *, e.id_status AS status_geral,
		// 		(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
		// 		FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
		// 		WHERE e.id_empresa = ep.id_empresa
		// 		AND e.id_cidade = c.id_cidade
		// 		AND c.id_estado = es.id_estado
		// 		AND e.id_tipo_imovel = tp.id_tipo_imovel
		// 		AND e.id_status = s.id_status 
		// 		AND f.id_empreendimento = e.id_empreendimento
		// 		AND e.ativo = 'S'
		// 		GROUP BY e.id_empreendimento
		// 		ORDER BY status_geral ASC
		// 	   ";	


		// (SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios

		$sql = "SELECT 
				e.id_empreendimento,
				e.empreendimento,
				e.id_tipo_imovel,
				t.tipo_imovel,
				e.chamada_empreendimento,
				e.segunda_chamada_empreendimento,
				e.latitude,
				e.longitude,
				e.pin,
				c.id_cidade,
				c.cidade,
				es.id_estado,
				es.estado,
				e.dormitorio,
				e.metragem,
				e.vagas,
				e.id_status
				FROM empreendimentos e 
				INNER JOIN cidades c ON e.id_cidade = c.id_cidade
				INNER JOIN estados es ON c.id_estado = es.id_estado
				INNER JOIN tipos_imoveis t ON e.id_tipo_imovel = t.id_tipo_imovel
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
				ORDER BY e.id_status = 1 DESC, e.id_status = 2 OR e.id_status = 6, e.empreendimento";
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}

	//==============================================================================================================
	//transferencia roleta


	
	function numImoveis ()
	{		
		//$this->db->select('*')->from('empreendimentos')->where('ativo', 'S');		
		
		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
	}
	
	function getAllImoveis ($offset = 0)
	{
		$this->db->flush_cache();
		
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}

		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento 
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
				ORDER BY FIELD(e.id_empreendimento,86, 5, 82, 71, 61, 87, 83, 33, 36, 9, 68, 2, 1, 90, 8 ,14, 15, 16, 18, 20,
					23, 24, 25, 26, 28, 29, 30, 32, 84, 27)
				LIMIT $offset,15
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function numImoveisBuscaGeral ($keyword)
	{		
		
		//$this->db->select('*')->from('empreendimentos')->like('empreendimento',$keyword);		
		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE  empreendimento LIKE '%$keyword%'
				AND e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
	}
	
	function getImoveisBuscaGeral ($keyword, $offset = 0)
	{
		$this->db->flush_cache();
		
		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}
		
		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				AND e.empreendimento LIKE '%$keyword%'
				GROUP BY e.id_empreendimento
				ORDER BY e.empreendimento ASC
				LIMIT $offset,15
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getImoveisBuscaGeral2 ($keyword)
	{
		$this->db->flush_cache();
		
				
		/*$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND e.ativo = 'S'
				AND e.empreendimento LIKE '%$keyword%'
				OR e.endereco_empreendimento LIKE '%$keyword%'
				OR e.chamada_empreendimento LIKE '%$keyword%'
				
				ORDER BY e.empreendimento ASC
			   ";	*/
		
		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				AND (c.cidade LIKE '%$keyword%'
				OR e.empreendimento LIKE '%$keyword%'
				OR e.descricao LIKE '%$keyword%'
				OR e.chamada_empreendimento LIKE '%$keyword%'
				OR e.endereco_empreendimento LIKE '%$keyword%'
				OR es.estado LIKE '%$keyword%')	
				GROUP BY e.id_empreendimento
				ORDER BY e.empreendimento ASC
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();	
		//echo $sql;exit;	
		return $query->result();
		
	}
	
	function getNumImoveisBuscaGeral2 ($keyword)
	{
		$this->db->flush_cache();
		
		/*$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND e.ativo = 'S'
				AND e.empreendimento LIKE '%$keyword%'
				OR e.endereco_empreendimento LIKE '%$keyword%'
				OR e.chamada_empreendimento LIKE '%$keyword%'	
				ORDER BY e.empreendimento ASC
			   ";	*/
				
		$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND e.ativo = 'S'
				AND (c.cidade LIKE '%$keyword%'
				OR e.empreendimento LIKE '%$keyword%'
				OR e.descricao LIKE '%$keyword%'
				OR e.chamada_empreendimento LIKE '%$keyword%'
				OR e.endereco_empreendimento LIKE '%$keyword%'
				OR es.estado LIKE '%$keyword%')	
				GROUP BY e.id_empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
		
	}
	
	function numImoveisBuscaImovel ($cidade='',$tipo='')
	{		
		if($cidade!='' && $tipo  !=''){
			$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE  e.id_cidade = $cidade 
				AND e.id_tipo_imovel = $tipo
				AND e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
			   ";	
		
			//$this->db->select('*')->from('empreendimentos')->where('id_cidade',$cidade)->where('id_tipo_imovel',$tipo);		
		}else if($cidade !=''){ 
			$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE  e.id_cidade = $cidade 
				AND e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
			   ";
			
			//$this->db->select('*')->from('empreendimentos')->where('id_cidade',$cidade);
		}else{
			$sql = "SELECT *, e.id_status AS status_geral
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_tipo_imovel = $tipo
				AND e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status 
				AND f.id_empreendimento = e.id_empreendimento
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
			   ";
			
			//$this->db->select('*')->from('empreendimentos')->where('id_tipo_imovel',$tipo);		
		}	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
	}
	
	function getImoveisBuscaImovel ($cidade='',$tipo='',$offset = 0)
	{
		$this->db->flush_cache();
		
		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}
		
		//echo $cidade;
		//echo "-".$tipo;
		
		if($cidade != '' && $tipo != '')
		{
			$sql = "SELECT *, e.id_status AS status_geral
					FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
					WHERE  e.id_cidade = $cidade 
					AND e.id_tipo_imovel = $tipo
					AND e.id_empresa = ep.id_empresa
					AND e.id_cidade = c.id_cidade
					AND c.id_estado = es.id_estado
					AND e.id_tipo_imovel = tp.id_tipo_imovel
					AND e.id_status = s.id_status 
					AND f.id_empreendimento = e.id_empreendimento
					AND e.ativo = 'S'
					GROUP BY e.id_empreendimento
					ORDER BY e.empreendimento ASC
					LIMIT $offset,15
				   ";
		}
		else if($cidade !='')
		{
			$sql = "SELECT *, e.id_status AS status_geral
					FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
					WHERE  e.id_cidade = $cidade 
					AND e.id_empresa = ep.id_empresa
					AND e.id_cidade = c.id_cidade
					AND c.id_estado = es.id_estado
					AND e.id_tipo_imovel = tp.id_tipo_imovel
					AND e.id_status = s.id_status 
					AND f.id_empreendimento = e.id_empreendimento
					AND e.ativo = 'S'
					GROUP BY e.id_empreendimento
					ORDER BY e.empreendimento ASC
					LIMIT $offset,15
				   ";
		}
		else
		{
			$sql = "SELECT *, e.id_status AS status_geral
					FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
					WHERE e.id_tipo_imovel = $tipo
					AND e.id_empresa = ep.id_empresa
					AND e.id_cidade = c.id_cidade
					AND c.id_estado = es.id_estado
					AND e.id_tipo_imovel = tp.id_tipo_imovel
					AND e.id_status = s.id_status 
					AND f.id_empreendimento = e.id_empreendimento
					AND e.ativo = 'S'
					GROUP BY e.id_empreendimento
					ORDER BY e.empreendimento ASC
					LIMIT $offset,15
				   ";
		}	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	// function getEmpreendimentosByStatus($status = 1, $limit = 0) {
	// 	$this->db->flush_cache();

	// 	$sql = "SELECT *, e.id_status AS status_geral 
	// 				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
	// 				WHERE e.id_empresa = ep.id_empresa
	// 				AND e.id_cidade = c.id_cidade
	// 				AND c.id_estado = es.id_estado
	// 				AND e.id_tipo_imovel = tp.id_tipo_imovel
	// 				AND e.id_status = s.id_status
	// 				AND f.id_empreendimento = e.id_empreendimento
	// 				AND f.id_status =1
	// 				AND e.ativo =  'S' AND e.id_empreendimento IN (86, 82, 71,87, 83, 36)
	// 				GROUP BY e.id_empreendimento
	// 				ORDER BY RAND()
	// 				LIMIT $limit
	// 			   ";

	// 	$query = $this->db->query($sql);
	// 	$this->db->close();
				
	// 	return $query->result();
	// }

	function getLancamentos($limit = "")
	{
		$this->db->flush_cache();
		
		if($limit):			
		
			$sql = "SELECT *, e.id_status AS status_geral,
					(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
					FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
					WHERE e.id_empresa = ep.id_empresa
					AND e.id_cidade = c.id_cidade
					AND c.id_estado = es.id_estado
					AND e.id_tipo_imovel = tp.id_tipo_imovel
					AND e.id_status = s.id_status
					AND f.id_empreendimento = e.id_empreendimento
					-- AND f.id_status =1
					AND e.id_status = 1
					AND e.ativo =  'S'
					-- AND e.ativo =  'S' AND e.id_empreendimento IN (86, 82, 71,87, 83, 36)
					GROUP BY e.id_empreendimento
					ORDER BY RAND()
					LIMIT $limit
				   ";
			   
		else:
		
			$sql = "SELECT *, e.id_status AS status_geral,
					(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
					FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
					WHERE e.id_empresa = ep.id_empresa
					AND e.id_cidade = c.id_cidade
					AND c.id_estado = es.id_estado
					AND e.id_tipo_imovel = tp.id_tipo_imovel
					AND e.id_status = s.id_status
					AND f.id_empreendimento = e.id_empreendimento
					-- AND f.id_status =1
					AND e.id_status = 1
					AND e.ativo =  'S'
					-- AND e.ativo =  'S' AND e.id_empreendimento IN (86, 82, 71,87, 83, 36)
					GROUP BY e.id_empreendimento
					ORDER BY RAND()
				   ";
		
		endif;		

		
		$query = $this->db->query($sql);
		
// 		var_dump($query->result());
// 		exit;
		
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getProntoPraMorar()
	{
		$this->db->flush_cache();
		
		// $sql = "SELECT *, e.id_status AS status_geral,
		// 		(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
		// 		FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
		// 		WHERE e.id_empresa = ep.id_empresa
		// 		AND e.id_cidade = c.id_cidade
		// 		AND c.id_estado = es.id_estado
		// 		AND e.id_tipo_imovel = tp.id_tipo_imovel
		// 		AND e.id_status = s.id_status
		// 		AND e.id_status = f.id_status
		// 		AND f.id_empreendimento = e.id_empreendimento
		// 		-- AND e.id_status = 2
		// 		AND e.ativo  = 'S'
		// 		AND f.id_status = 2 
		// 		-- AND e.ativo = 'S' AND e.id_empreendimento IN (2, 9, 68, 27, 32, 15, 1, 14, 28, 25, 8, 26, 24)
		// 		GROUP BY e.id_empreendimento
		// 		-- ORDER BY FIELD(e.id_empreendimento,2, 9, 68, 27, 32, 15, 1, 14, 28, 25, 8, 26, 24)
		// 		ORDER BY e.empreendimento
		// 	   ";	

		$sql = "SELECT 
				e.id_empreendimento,
				e.empreendimento,
				e.id_tipo_imovel,
				t.tipo_imovel,
				e.chamada_empreendimento,
				e.segunda_chamada_empreendimento,
				e.latitude,
				e.longitude,
				e.pin,
				c.id_cidade,
				c.cidade,
				es.id_estado,
				es.estado,
				e.dormitorio,
				e.metragem,
				e.vagas,
				e.id_status
				FROM empreendimentos e 
				INNER JOIN cidades c ON e.id_cidade = c.id_cidade
				INNER JOIN estados es ON c.id_estado = es.id_estado
				INNER JOIN tipos_imoveis t ON e.id_tipo_imovel = t.id_tipo_imovel
				AND e.ativo = 'S'
				AND e.id_status =2
				GROUP BY e.id_empreendimento
				ORDER BY  FIELD(e.id_status, 3,4,5,2,6), e.destaque_home DESC, e.id_empreendimento DESC";
		
		$query = $this->db->query($sql);		
		return $query->result();
		
	}
	
	function getProntoEmSeisMeses()
	{
		$this->db->flush_cache();
		
		$sql = "SELECT *, e.id_status AS status_geral,
				(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				-- AND e.id_status = f.id_status
				AND f.id_empreendimento = e.id_empreendimento
				AND e.id_status = 3 
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
				ORDER BY e.empreendimento
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getSeisAdozeMeses()
	{
		$this->db->flush_cache();
		
		$sql = "SELECT *, e.id_status AS status_geral,
				(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND f.id_empreendimento = e.id_empreendimento
				AND f.id_status = 4 
				AND e.ativo = 'S'
				GROUP BY e.id_empreendimento
				ORDER BY RAND()
			   ";	
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
		
	}
	
	function getMaisDeDozeMeses()
	{
		$this->db->flush_cache();
		
		$sql = "SELECT *, e.id_status AS status_geral,
				(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s, fases f
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND f.id_empreendimento = e.id_empreendimento
				AND f.id_status = 5 
				AND e.ativo = 'S' AND e.id_empreendimento IN (5, 9, 61, 30, 33, 18)
				GROUP BY e.id_empreendimento
				ORDER BY FIELD(e.id_empreendimento, 61, 30, 33, 18)
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getEmpreendimento($id_empreendimento)
	{		
		$sql = "SELECT *, e.id_status AS status_geral,
				(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status AND e.ativo = 'S'
				AND (e.id_empreendimento = '$id_empreendimento' OR e.url_amigavel = '$id_empreendimento')

			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->row();
	}
	
	
	function getEmpreendimentosDestaqueHome()
	{
		$sql = "SELECT *, e.id_status AS status_geral,
			(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
			FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
			WHERE e.id_empresa = ep.id_empresa
			AND e.id_cidade = c.id_cidade
			AND c.id_estado = es.id_estado
			AND e.id_tipo_imovel = tp.id_tipo_imovel
			AND e.id_status = s.id_status AND e.ativo = 'S'
			AND e.destaque_home = 1
			ORDER BY destaque_home_ordem
		";
		
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->result();
	}

	function getEmpreendimentosEmDestaque()
	{
		$sql = "SELECT 
					e.id_empreendimento,
					e.empreendimento,
					e.chamada_empreendimento,
					e.logotipo,
					e.id_status,
					e.status_label,
					c.cidade,
					es.estado
				FROM
				empreendimentos e
				    INNER JOIN
				cidades c ON c.id_cidade = e.id_cidade
				    INNER JOIN
				estados es ON es.id_estado = c.id_estado
				WHERE
					e.ativo = 'S' AND e.destaque_home = 1
				ORDER BY destaque_home_ordem";

		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getEmpreendimentosByEmpresa($id_empresa = 0)
	{
		$where = '';

		if(intval($id_empresa) > 0)
		{
			$where = 'AND O.id_empresa IN ('. (int) $id_empresa .')';
		}

		$sql = "SELECT
					O.id_empresa, O.empreendimento, O.fachada  
				FROM
					obras_realizadas O
				WHERE
					O.ativo = 'S' $where 
				ORDER BY O.data_habite_se DESC, O.empreendimento
				";

		// $sql = "SELECT O.id_empresa, O.empreendimento, O.fachada  
		// 		FROM obras_realizadas O
		// 		LEFT JOIN cidades ON O.id_cidade = cidades.id_cidade
		// 		LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
		// 		WHERE O.id_empresa IN ($id_empresa)
		// 		ORDER BY ordem
		// 		";


		// $sql = "SELECT *, e.id_status AS status_geral,
		// 		(SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
		// 		FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
		// 		WHERE e.id_empresa = ep.id_empresa
		// 		AND e.id_cidade = c.id_cidade
		// 		AND c.id_estado = es.id_estado
		// 		AND e.id_tipo_imovel = tp.id_tipo_imovel
		// 		AND e.id_status = s.id_status 
		// 		AND e.ativo = 'S'
		// 		AND e.id_status = 2
		// 		AND e.id_empresa IN ($id_empresa)
		// ";
	
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}
	
	
	function getEmpreendimentosMaisVistos()
	{
		$sql = "SELECT 
					e.id_empreendimento,
					e.empreendimento,
					e.chamada_empreendimento,
					c.cidade,
					es.estado
				FROM
					empreendimentos e,
					cidades c,
					estados es
				WHERE
					e.id_cidade = c.id_cidade
				    AND c.id_estado = es.id_estado
				    AND e.ativo = 'S'
				ORDER BY qtd_visita DESC
				LIMIT 10";
	
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->result();
	}
	
	
	
	function getEmpreendimentoPre($id_empreendimento)
	{		
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND e.id_empreendimento = $id_empreendimento

			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->row();
	}
	
	function getEmpreendimentoSemelhante($id_empreendimento)
	{		
		$sql = "SELECT *
				FROM empreendimentos_semelhantes
				INNER JOIN empreendimentos 
				ON empreendimentos.id_empreendimento = empreendimentos_semelhantes.id_semelhante
				LEFT JOIN empresas ON empreendimentos.id_empresa = empresas.id_empresa
				LEFT JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade
				LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
				WHERE empreendimentos_semelhantes.id_empreendimento = $id_empreendimento
				";
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}

	function getEmpreendimentoToSelect()
	{
		$sql = "SELECT id_empreendimento, empreendimento 
				FROM empreendimentos 
				WHERE ativo = 'S' 
				AND id_empreendimento != 103 
				AND id_empreendimento != 104
				ORDER BY empreendimento ASC";

		$query = $this->db->query($sql);

		$this->db->close();
				
		return $query->result();
	}
	
	function getDestaques ($id_empreendimento)
	{
		$sql = "SELECT *
				FROM destaques_empreendimentos
				WHERE id_empreendimento = $id_empreendimento
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->result();	
	
	}
	
	function getItens ($id_empreendimento)
	{
		$sql = "SELECT *
				FROM itens
				WHERE id_empreendimento = $id_empreendimento
				ORDER BY posicao ASC
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
			
		return $query->result();	
	
	}
	
	function getTiposFotos ($id_empreendimento)
	{		
		$sql =  "SELECT DISTINCT (m.id_tipo_midia) AS id_midia,  tp.tipo_midia as tipo
				 FROM midias m, tipos_midias tp
				 WHERE m.id_tipo_midia = tp.id_tipo_midia
				 AND m.id_empreendimento = $id_empreendimento
				 ORDER BY tp.ordem ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getFotos ($id_empreendimento, $id_tipo)
	{		
		$sql =  "SELECT * 
				 FROM midias
				 WHERE id_tipo_midia = $id_tipo 
				 AND id_empreendimento = $id_empreendimento
				 ORDER BY posicao ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getFases($id_empreendimento)
	{
		$sql =  "SELECT * 
				 FROM fases
				 WHERE
				 	id_empreendimento = $id_empreendimento
				 	AND
				 	mostrar_fase = 'S'
				 ORDER BY ordem DESC, titulo ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getItensFases ($id_fase)
	{
		$sql =  "SELECT * 
				 FROM itens_fases
				 WHERE id_fase = $id_fase
				 ORDER BY posicao ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getItensFasesSomado ($id_fase)
	{
		$sql =  "SELECT COUNT(*) as total, SUM(porcentagem) as soma
		FROM itens_fases
		WHERE id_fase = $id_fase
		ORDER BY posicao ASC
		";
	
		$query = $this->db->query($sql);
		$this->db->close();
	
		return $query->result();
	}
	
	function getFotosObra ($id_fase,$id_empreendimento)
	{
		$sql =  "SELECT * 
				 FROM fotos_fases ff, empreendimentos e, fases f
				 WHERE f.id_empreendimento = e.id_empreendimento
				 AND f.id_empreendimento = $id_empreendimento
				 AND e.id_empreendimento = $id_empreendimento
				 AND ff.id_fase = $id_fase
				 AND ff.id_fase = f.id_fase
				 ORDER BY ff.posicao ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getFotosObrasByEmpreendimentoID($id_empreendimento)
	{
		$id_empreendimento = (int) $id_empreendimento;

		$sql = "SELECT 
				    ff.legenda, ff.imagem
				FROM
				    fotos_fases ff
				        INNER JOIN
				    fases f ON ff.id_fase = f.id_fase
				WHERE
				    f.id_empreendimento = $id_empreendimento
				ORDER BY ff.id_fase , ff.posicao
				";

		$query = $this->db->query($sql);
						
		return $query->result();
	}
	
	function numObrasRealizadas()
	{
		$this->db->select('*')->from('obras_realizadas');
		$this->db->where('id_empresa','1');	
		$this->db->or_where('id_empresa','5');
		
					
		$query = $this->db->count_all_results();
		$this->db->close();
		
		return $query;
	}
	
	function numObrasRealizadas_egl()
	{
		$this->db->select('*')->from('obras_realizadas')->where('id_empresa','2');		
		
		$query = $this->db->count_all_results();
		$this->db->close();
		
		return $query;
	}
	
	function numObrasRealizadas_dhz()
	{
		$this->db->select('*')->from('obras_realizadas')->where('id_empresa','3');		
		
		
		$query = $this->db->count_all_results();
		$this->db->close();
		
		return $query;
	}
	
	function numObrasRealizadas_lomando()
	{
		$this->db->select('*')->from('obras_realizadas')->where('id_empresa','4');		
		
		$query = $this->db->count_all_results();
		$this->db->close();
		
		return $query;
	}
	
	function getObrasRealizadas($limit = 0,$offset = 0)
	{
		
		if($limit)
		{
			$sql = "SELECT * 
					FROM obras_realizadas
					LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
					LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
					WHERE fachada != ''
					ORDER BY RAND() 
					LIMIT 3
					";
		}
		else
		{
			if($offset)
			{
				
			}
			else {
				$offset = 0;
			}
			$sql = "SELECT * 
					FROM obras_realizadas
					LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
					LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
					WHERE id_empresa = '1' OR id_empresa = '5'
					ORDER BY fachada desc
					LIMIT $offset,12
					";
		}
		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getObrasRealizadas_capa($offset = 0){
		
			if($offset)
			{
				
			}
			else {
				$offset = 0;
			}
			$sql = "SELECT * 
					FROM obras_realizadas
					LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
					LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
					WHERE id_empresa = '1' OR id_empresa = '5'
					ORDER BY fachada desc
					LIMIT $offset,12
					";
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	function getObrasRealizadas_egl($offset = 0){
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}
		$sql = "SELECT * 
				FROM obras_realizadas
				LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
				LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
				WHERE id_empresa = '2'
				ORDER BY fachada desc
				LIMIT $offset,12
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getObrasRealizadas_dhz($offset = 0){
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}
		
		$sql = "SELECT * 
				FROM obras_realizadas
				LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
				LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
				WHERE id_empresa = '3'
				ORDER BY fachada desc
				LIMIT $offset,12
				";
		
		$query = $this->db->query($sql);	
		$this->db->close();
			
		return $query->result();
		
	}
	
	function getObrasRealizadas_lomando($offset = 0)
	{
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}
		
		$sql = "SELECT * 
				FROM obras_realizadas
				LEFT JOIN cidades ON obras_realizadas.id_cidade = cidades.id_cidade
				LEFT JOIN estados ON  cidades.id_estado = estados.id_estado
				WHERE id_empresa = '4'
				ORDER BY fachada desc
				LIMIT $offset,12
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getEstados ()
	{
		$sql =  "SELECT distinct(estados.id_estado), estado, uf 
				FROM empreendimentos
				INNER JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade
				LEFT JOIN estados ON cidades.id_estado = estados.id_estado
				";
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getCidades($id_estado)
	{
		$sql = "select distinct(cidades.id_cidade), cidade 
				from empreendimentos
				inner join cidades ON empreendimentos.id_cidade = cidades.id_cidade
				left join estados ON cidades.id_estado = estados.id_estado
				where estados.id_estado = $id_estado
				";
		
		$query = $this->db->query($sql);
		$this->db->close();	
		
		return $query->result();
	}	
	
	function getTiposImoveis ()
	{
		$sql =  "SELECT * 
				 FROM tipos_imoveis
				 ORDER BY tipo_imovel ASC
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getMidia ($id_midia)
	{
		$sql =  "SELECT * 
				 FROM midias
				 WHERE id_midia = $id_midia
				";
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->row();
	}
	
	function getTags($id_secao)
	{
		$this->db->select('*')->from('tags')->where('id_cms_secoes',$id_secao);
		
		$query = $this->db->get();
		$this->db->close();
		
		return $query->row();
	}
	
	
	function getProximidades ()
	{
		$this->db->flush_cache();
	
		$sql = "SELECT p.*, t.tipo_proximidade, t.pin
			FROM proximidades p
			LEFT JOIN tipos_proximidades t ON t.id_tipo_proximidade = p.id_tipo_proximidade
			ORDER BY data_cadastro desc
		";
	
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	function getTiposProximidades ()
	{
		$this->db->flush_cache();
	
		$sql = "SELECT * 
			FROM tipos_proximidades 
			ORDER BY tipo_proximidade asc
		";
	
		$query = $this->db->query($sql);
		return $query->result();
	}



	function getEstadoCidadeByEmpreendimentoStatus($id_status)
	{
		$id_status = (int) $id_status;

		$WHERE_STATUS = '';

		if($id_status > 0)
			$WHERE_STATUS = 'AND e.id_status = ' . $id_status;

		$sql = "SELECT
					'estado' AS tipo,
					uf.id_estado AS id,
					uf.estado AS nome,
					null AS outro
				FROM
					empreendimentos e
				INNER JOIN
					cidades c ON e.id_cidade = c.id_cidade
				INNER JOIN
					estados uf ON c.id_estado = uf.id_estado
				WHERE
					e.ativo = 'S' $WHERE_STATUS

				UNION

				SELECT
					'cidade' AS tipo,
					c.id_cidade AS id,
					cidade AS nome,
					c.id_estado AS outro
				FROM
					empreendimentos e
				INNER JOIN
					cidades c ON e.id_cidade = c.id_cidade
				WHERE
					e.ativo = 'S' $WHERE_STATUS
				
				UNION
				
				SELECT
					'tipo_imovel' AS tipo,
					t.id_tipo_imovel AS id,
					t.tipo_imovel AS nome,
					null AS outro
				FROM
					empreendimentos e
				INNER JOIN
					tipos_imoveis t ON e.id_tipo_imovel = t.id_tipo_imovel
				WHERE
					e.ativo = 'S' $WHERE_STATUS
				";

		$query = $this->db->query($sql);
		
		return $query->result();
	}

	
	
}





/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	