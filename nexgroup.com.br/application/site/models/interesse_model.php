<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interesse_model  extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
 	function cadastraInteresse($data)
 	{
		$this->db->set($data)->insert('interesses');
	}
		
	function cadastraIndique($data)
	{
		$this->db->set($data)->insert('indique');
	}
	
	function cadastraContato($data)
	{
		$this->db->set($data)->insert('contatos');
	
	
	function buscaEmpreendimento($id,$empresa)
	{
		$this->db->select('*')->from('integracoes');
		$this->db->where('id_emp_site',$id);
		$this->db->where('id_empresa',$empresa);
		$query = $this->db->get();
		
		if($this->db->count_all_results() > 0){
			return $query->row()->id_empreendimento;
		}else{
			return false;
		}
	}
	
	function buscaEstado($sigla)
	{
		$this->db->select('*')->from('estados');
		$this->db->where('uf',$sigla);
		$query = $this->db->get();
		
		if($this->db->count_all_results() > 0){
			return $query->row()->id_estado;
		}else{
			return 21;
		}
	}			
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */