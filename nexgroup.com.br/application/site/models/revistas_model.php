<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revistas_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
#-----------------------------------------------------------------------------------#
	
	function numRevistas ()
	{		
		$this->db->select('*')->from('revistas');		
		$this->db->close();
		
		return $this->db->count_all_results();
	}
	#-----------------------------------------------------------------------------------#		
	function getListaRevistas ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM revistas
				ORDER BY data_cadastro desc
				LIMIT $offset,10
			   ";		
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#		
	function getRevistas ()
	{		
		$this->db->flush_cache();		

		$sql = "SELECT *
				FROM revistas
				ORDER BY id_revista desc
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getRevista()
	{		
		$this->db->select('arquivo, capa, pageflip')->from('revistas')->order_by('id_revista','desc')->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	