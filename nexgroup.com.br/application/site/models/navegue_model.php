<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navegue_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
#-----------------------------------------------------------------------------------#		
	
	function getImoveis()
	{		
		$sql = "SELECT *
				FROM empreendimentos e, cidades c, estados es
				WHERE latitude <> ''
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.ativo = 'S' 
				";		
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
		
	}
	
	function getImovel($id)
	{		
		$sql = "SELECT *
				FROM empreendimentos e, cidades c, estados es
				WHERE id_empreendimento =$id
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				
				";		
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->row();
		
	}
	
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	