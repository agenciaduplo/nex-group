<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios_model extends CI_Model {	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getEmpreendimentos ()
	{
		$sql = "SELECT *
				FROM empreendimentos
				WHERE id_empresa <> 5
				AND ativo = 'S'
				ORDER BY empreendimento ASC
			";
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}

	function getLogInteresses ($id)
	{		
		$sql = "SELECT *
				FROM grupos_interesses
				WHERE id_interesse = $id 
			";
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->row();
	}

	function getLogAtendimentos ($id)
	{		
		$sql = "SELECT *
				FROM grupos_atendimentos
				WHERE id_atendimento = $id 
			";
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->row();
	}

	function getInteresses ()
	{		
		$sql = "SELECT * 
				FROM  interesses 
				WHERE email LIKE  '%atendimento@divex%'
				";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}

	function delLogAtendimento ($id_atendimento)
	{		
	    $this->db->where('id_atendimento', $id_atendimento);
	    $this->db->delete('grupos_atendimentos');
	}

	function delAtendimento ($id_atendimento)
	{		
	    $this->db->where('id_atendimento', $id_atendimento);
	    $this->db->delete('atendimentos');
	}

	function delLogInteresse ($id_interesse)
	{		
	    $this->db->where('id_interesse', $id_interesse);
	    $this->db->delete('grupos_interesses');
	}

	function delInteresse ($id_interesse)
	{		
	    $this->db->where('id_interesse', $id_interesse);
	    $this->db->delete('interesses');
	}

	function getAtendimentos ()
	{		
		$sql = "SELECT * 
				FROM  atendimentos 
				WHERE email LIKE  '%atendimento@divex%'
				";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->result();
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */	