<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocoes_model  extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function setPromocaoDescontos($data)
	{
		$this->db->set($data)->insert('promocoes');
	}
	
	
	function getCurriculo ($id_job)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM jobs
				WHERE id_job = $id_job
			   ";
			   
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	
	
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */