<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
#-----------------------------------------------------------------------------------#

    function addVisita($id_oferta){
        $sql = "UPDATE ofertas SET qtd_visitado = qtd_visitado + 1 WHERE id_oferta = $id_oferta";
        $query = $this->db->query($sql);
    }
    
    function addFavorito($id_oferta){
        $sql = "UPDATE ofertas SET qtd_favoritado = qtd_favoritado + 1 WHERE id_oferta = $id_oferta";
        $query = $this->db->query($sql);
    }


    function getOfertas()
    {
    	$sql = "SELECT 
    			o.*,
    			ot.tipo,
    			c.cidade
    			FROM ofertas o
    			INNER JOIN ofertas_tipo ot ON o.id_ofertas_tipo = ot.id_ofertas_tipo
    			LEFT JOIN cidades c ON o.id_cidade = c.id_cidade 
    			WHERE o.ativo = 'S'
    			ORDER BY data_cadastro DESC";

    	$query = $this->db->query($sql);		
		return $query->result();
    }



    function getOfertByURL($url)
    {
        $sql = "SELECT 
                o.*,
                ot.tipo,
                c.cidade
                FROM ofertas o
                INNER JOIN ofertas_tipo ot ON o.id_ofertas_tipo = ot.id_ofertas_tipo
                LEFT JOIN cidades c ON o.id_cidade = c.id_cidade 
                WHERE o.ativo = 'S' AND o.url = '$url'
                ";

        $query = $this->db->query($sql);        
        return $query->row();
    }

    function getOfertByID($id_oferta)
    {
        $id_oferta = (int) $id_oferta;

        $sql = "SELECT 
                o.*,
                ot.tipo,
                c.cidade
                FROM ofertas o
                INNER JOIN ofertas_tipo ot ON o.id_ofertas_tipo = ot.id_ofertas_tipo
                LEFT JOIN cidades c ON o.id_cidade = c.id_cidade 
                WHERE o.ativo = 'S' AND o.id_oferta = $id_oferta
                ";

        $query = $this->db->query($sql);        
        return $query->row();
    }





    function getImagens($id_oferta)
	{
		$sql = "SELECT *
				FROM ofertas_imagens
				WHERE id_oferta = $id_oferta
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function getTipos()
	{
		$sql = "SELECT DISTINCT OT.* 
				FROM ofertas_tipo OT 
				INNER JOIN ofertas O ON OT.id_ofertas_tipo = O.id_ofertas_tipo 
				ORDER BY OT.tipo ASC";

    	$query = $this->db->query($sql);		
		return $query->result();
	}

}


/* End of file ofertas_model.php */
/* Location: ./system/application/model/ofertas_model.php */
	