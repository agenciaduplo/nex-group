<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contatos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function setAcessoCorretor($data)
	{
		$this->db->set($data)->insert('acessos_corretor');
	}

	function setAtendimentoUnico ($data)
	{
		$this->db->set($data)->insert('atendimentos');
		return $this->db->insert_id();
	}	
	
	function setAtendimento($data)
	{
		$this->db->set($data)->insert('atendimentos');
	}
	
	function setNewsletter($data)
	{
		$this->db->set($data)->insert('newsletter');
	}
	
	function setAtendimentoEmail($data)
	{
		$this->db->set($data)->insert('atendimentos');
	}
	
	function setAtendimentoTelefone($data)
	{
		$this->db->set($data)->insert('atendimentos');
	}
		
	function setFaleConosco($data)
	{
		$this->db->set($data)->insert('contatos');
	}
	
	function setInteresse($data)
	{
		$this->db->set($data)->insert('interesses');
		return $this->db->insert_id();
	}
	
	function setIndique($data)
	{
		$this->db->set($data)->insert('indique');
		return $this->db->insert_id();
	}
	
	function setTerreno($data)
	{
		//=====================================================================
		//INICIO UPLOAD ARQUIVO PDF
		//=====================================================================
			$config['upload_path']		= './uploads/terrenos/';
			$config['allowed_types']	= 'jpg|gif|png|bmp';
			$config['max_size']			= '15000';
			$config['encrypt_name']     = TRUE;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			
	        //executa o upload (setar o nome do campo aqui)
			if (!$this->upload->do_upload('foto')){
				//caso não consiga fazer o upload
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('resposta', "Erro ao inserir arquivo".$this->upload->display_errors());
			}
			else{
				//caso consiga
				$file_data 	= $this->upload->data();
	        	$file_name 	= $file_data['file_name'];
	        	
				$dataArchive = array('upload_data' => $file_data);
			
				$this->session->set_flashdata('resposta_terreno', 'Registro inserido com sucesso!');
					
				$this->db->set('imagem', $file_name);
			
			}
			
		//=====================================================================
		//FIM UPLOAD ARQUIVO PDF
		//=====================================================================
		$this->db->set($data);
		$this->db->insert('terrenos');
	}
	
	function setTrabalhe($data)
	{
		
		//=====================================================================
		//INICIO UPLOAD ARQUIVO PDF
		//=====================================================================
			$config['upload_path']		= './uploads/trabalhe_conosco/';
			$config['allowed_types']	= 'pdf|doc|docx';
			$config['max_size']			= '15000';
			$config['encrypt_name']     = TRUE;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			
	        //executa o upload (setar o nome do campo aqui)
			if (!$this->upload->do_upload('curriculum')){
				//caso não consiga fazer o upload
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('resposta', "Erro ao inserir arquivo".$this->upload->display_errors());
			}
			else{
				//caso consiga
				$file_data 	= $this->upload->data();
	        	$file_name 	= $file_data['file_name'];
	        	
				$dataArchive = array('upload_data' => $file_data);
			
				$this->session->set_flashdata('resposta_trabalhe', 'Registro inserido com sucesso!');
					
				$this->db->set('curriculo', $file_name);
			
			}
			
		//=====================================================================
		//FIM UPLOAD ARQUIVO PDF
		//=====================================================================

		$this->db->set($data)->insert('jobs');
		return $this->db->insert_id();
	}

	function setTrabalheConosco($data)
	{
		$this->db->set($data)->insert('jobs');
		return $this->db->insert_id();
	}

	function updateTrabalheConosco($data, $id)
	{
		$this->db->update('jobs', $data, "id_job = " . $id);
	}
	


	function setFornecedor($data)
	{
		$this->db->set($data)->insert('fornecedores');
	}
	
	function getNewsletter($email)
	{
		$this->db->select('*')->from('newsletter')->where('email',$email);
	
		$query = $this->db->get();
		$count = $query->num_rows();
		if($count > 0)
			return true;
		else 
			return false;
		
	}
	
	function getEmpreendimentos()
	{
		$this->db->select('*')->from('empreendimentos')->where('ativo','S');
		$this->db->order_by('empreendimento','asc');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getEmpreendimento($id_empreendimento)
	{
		$this->db->select('*')->from('empreendimentos');
		$this->db->join('cidades', 'cidades.id_cidade = empreendimentos.id_cidade');
		$this->db->join('estados', 'estados.id_estado = cidades.id_estado');
		$this->db->where('id_empreendimento',$id_empreendimento);
	
		$query = $this->db->get();
		
		return $query->row();
	}
	
	function getEstados()
	{
		$this->db->select('*')->from('estados');		
		$query = $this->db->get();
		
		return $query->result();
	}	
		
	function getEstado($id_estado)
	{
		$this->db->select('*')->from('estados')->where('id_estado',$id_estado);
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
	function getCidades($id_estado)
	{
		$this->db->select('*')->from('cidades')->where('id_estado',$id_estado);
		
		$query = $this->db->get();
		
		return $query->result();
	}	
	
	function getCidade($id_cidade)
	{
		$this->db->select('*')->from('cidades')->where('id_cidade',$id_cidade);		
		$query = $this->db->get();
		
		return $query->row();
	}	
	
	function getEnviaEmail($id_empreendimento, $tipo)
	{
		$this->db->select('*')->from('emails');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_tipo_form',$tipo);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	
	function getEnviaEmailEgl($id_empreendimento, $enviaSetor)
	{
		$this->db->select('*')->from('emails');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_tipo_form',3);
		$this->db->where('id_setor', $enviaSetor);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	
	function getCurriculo ($id_job)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM jobs
				WHERE id_job = $id_job
			   ";
		
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function getTags($id_secao)
	{
		$this->db->select('*')->from('tags')->where('id_cms_secoes',$id_secao);
		
		$query =$this->db->get();
		$this->db->close();
		
		return $query->row();
	}



	/* envia email via roleta */
	
	function grupo_indique($indique, $grupo, $emails_txt){
		$this->db->insert('grupos_indiques',array(
			'id_indique' => $indique,
			'id_grupo' => $grupo,
			'emails' => $emails_txt
		));		
	}
	function grupo_interesse($interesse,$grupo, $emails_txt){
		$this->db->insert('grupos_interesses',array(
			'id_interesse' => $interesse,
			'id_grupo' => $grupo,
			'emails' => $emails_txt
		));
	}
	function grupo_atendimento($atendimento,$grupo, $emails_txt){
		$this->db->insert('grupos_atendimentos',array(
			'id_atendimento' => $atendimento,
			'id_grupo' => $grupo,
			'emails' => $emails_txt
		));
	}
	function getGrupos ($id_empreendimento) 
	{
		$sql = "SELECT *
				FROM grupos
				WHERE id_empreendimento = $id_empreendimento
		";
		return $this->db->query($sql)->result();
	}

	function updateRand ($id_empreendimento, $ordem)
	{
		$sql = "UPDATE `grupos` SET `rand`='0' WHERE (`id_empreendimento`='$id_empreendimento')";
		$this->db->query($sql);
		$set = "UPDATE `grupos` SET `rand`='1' WHERE `ordem`='$ordem' AND `id_empreendimento`='$id_empreendimento' LIMIT 1";
		$this->db->query($set);
	}

	function getGruposEmails ($id_empreendimento, $ordem)
	{
		 $sql = "SELECT e.email, e.grupo as grupo
				FROM grupos_emails e, grupos g
				WHERE g.id_empreendimento = $id_empreendimento
				AND g.ordem = '$ordem'
				AND g.id = e.grupo
		";
		return $this->db->query($sql)->result();
	}
	/* envia email via roleta */



	function verificaLastEmailSendFor($limit = 4, $table="interesses",$camp=""){
		$camp = ($camp != "") ? " WHERE url = '".$camp."'" : "";
		$sql  = "SELECT email_enviado_for FROM {$table} {$camp} ORDER BY data_envio DESC LIMIT {$limit}";
		$ret  = $this->db->query($sql)->result();

		return $ret;
	}

	function getIdEmpresa($id_empreendimento){
		$sql  = "SELECT id_empresa FROM empreendimentos WHERE id_empreendimento = {$id_empreendimento} LIMIT 1";
		$ret  = $this->db->query($sql)->result();

		return $ret;
	}

	function getIdCidade($id_empreendimento){
		$sql = "SELECT id_cidade FROM empreendimentos WHERE id_empreendimento = {$id_empreendimento} LIMIT 1";
		$ret = $this->db->query($sql)->result();

		return $ret;
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */