<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busca_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function executeSearch($q)
	{
		$q = mysql_real_escape_string(trim($q));

		$query_search_words = explode(' ', $q); 			// Array com as palavras da query da busca
		$total_search_words	= count($query_search_words);	// Total de palavras da query da busca

		$arr_ids_empreendimentos  = array();
		$arr_ids_ofertas		  = array();
		$arr_ids_noticias 		  = array();
		$arr_ids_institucional	  = array();
		$arr_ids_creches 		  = array();

		// $arr_words_idf		  = array(); // Termos IDF. Palavra encontrada mais frequente nos registros ** NÃO APAGAR ** 

		foreach($query_search_words as $word)
		{
			// KEY do array é o termo, o VALUE é o ID do registro

			$arr_ids_empreendimentos[$word] = self::SearchFoundWords('empreendimentos', 'id_empreendimento', 'descricao', $word, 1);
			$arr_ids_ofertas[$word]		  	= self::SearchFoundWords('ofertas', 'id_oferta', 'titulo', $word, 1);
			$arr_ids_noticias[$word] 	  	= self::SearchFoundWords('noticias', 'id_noticia', 'titulo', $word, 1);
			$arr_ids_institucional[$word] 	= self::SearchFoundWords('empresa_institucionais', 'id_institucional', 'titulo', $word, 1);
			$arr_ids_creches[$word] 	  	= self::SearchFoundWords('responsabilidades_social', 'id_responsabilidade', 'nome', $word, 1);

			//  ** NÃO APAGAR ** 
			// KEY do array é o termo, o VALUE é a soma de todos registros encontrados com o termo
			// $arr_words_idf[$word] = count($arr_ids_ofertas[$word]) + 
			// 						count($arr_ids_noticias[$word]) +
			// 						count($arr_ids_institucional[$word]) +
			// 						count($arr_ids_creches[$word]);
			//  ** NÃO APAGAR ** 
		}

		// arsort($arr_words_idf); // Ordena o array pelo maior ao menor total do termo encontrado  ** NÃO APAGAR ** 


		// Total de registro encontrados de cada tabela
		$total_empreendimentos = (sizeof($arr_ids_empreendimentos, 1) > $total_search_words) ? sizeof($arr_ids_empreendimentos, 1) - $total_search_words : 0;
		$total_ofertas 		   = (sizeof($arr_ids_ofertas, 1) 	   	  > $total_search_words) ? sizeof($arr_ids_ofertas, 1) 	  	   - $total_search_words : 0;
		$total_noticias 	   = (sizeof($arr_ids_noticias, 1)	   	  > $total_search_words) ? sizeof($arr_ids_noticias, 1) 	   - $total_search_words : 0;
		$total_institucional   = (sizeof($arr_ids_institucional, 1)   > $total_search_words) ? sizeof($arr_ids_institucional, 1)   - $total_search_words : 0;
		$total_creches 		   = (sizeof($arr_ids_creches, 1) 	   	  > $total_search_words) ? sizeof($arr_ids_creches, 1) 	  	   - $total_search_words : 0;

		$total_all = $total_empreendimentos + $total_ofertas + $total_noticias + $total_institucional + $total_creches;

		// var_dump('imoveis: ' 		. $total_empreendimentos);
		// var_dump('ofertas: ' 		. $total_ofertas);
		// var_dump('noticias: ' 		. $total_noticias);
		// var_dump('institucional: ' 	. $total_institucional);
		// var_dump('creches: ' 		. $total_creches);

		 // Array de retorno dos dados
		$arr_result = array(
					'empreendimentos' => array(),
					'ofertas' 		  => array(),
					'institucionais'  => array(),
					'noticias'        => array(),
					'creches'         => array());

		/*
		|--------------------------------------------------------------------------
		| EMPREENDIMENTOS
		|--------------------------------------------------------------------------
		*/

		if($total_empreendimentos > 0)
		{
			// echo 'EMPREENDIMENTOS<br/>';

			$arr_ranked_empreendimentos = array();

			// Percorre o array de registros das OFERTAS
			foreach($arr_ids_empreendimentos as $key => $row)
			{
				foreach($row as $empreendimento)
				{		
					$arr_title_words = explode(" ", $empreendimento->titulo);

					// Cria o array com o total da frequência de termo por registro  Ex.: ID => 1 PALAVRAS ENCONTRADAS => 3 
					if(array_key_exists($empreendimento->id, $arr_ranked_empreendimentos) && array_key_exists($key, $arr_ranked_empreendimentos[$empreendimento->id]))
					{
						$arr_ranked_empreendimentos[$empreendimento->id]['words'][$key] += 1 ;
					}
					else
					{
						$arr_ranked_empreendimentos[$empreendimento->id]['words'][$key] = 1;

						// Verifica outros termos que não foram somados
						foreach($arr_title_words as $word)
						{
							if(in_array($word, $query_search_words) && $word != $key)
							{
								$arr_ranked_empreendimentos[$empreendimento->id]['words'][$word] = 1;
								// $arr_words_idf[$word] += 1;  ** NÃO APAGAR ** 
							}
						}
					}

					// $arr_ranked_empreendimentos[$empreendimento->id]['title'] = $empreendimento->titulo;   // Titulo do registro

					$total_found_words = count($arr_ranked_empreendimentos[$empreendimento->id]['words']); // Total de palavras encontradas (PE)

					// RANK 1 (TF-IDF)
					//==========================================================================================================
					$total_terms = (($total_found_words * $total_found_words) * $total_found_words) / $total_search_words;

					$arr_ranked_empreendimentos[$empreendimento->id]['rank'] = self::getRank($total_all, $total_empreendimentos, $total_terms);

					// RANK 2 (quantidade do termo / quantidade de palavras)
					//==========================================================================================================
					$tpt = count($arr_title_words);
					$pne = $tpt - $total_found_words;

					$arr_ranked_empreendimentos[$empreendimento->id]['rank2'] = self::getRank2($pne, $tpt);
				}
			}

			$arr_ranked_empreendimentos = self::array_sort($arr_ranked_empreendimentos, 'rank2', SORT_DESC);

			// var_dump($arr_ranked_empreendimentos);

			$arr_result['empreendimentos'] = $arr_ranked_empreendimentos;
		}

		/*
		|--------------------------------------------------------------------------
		| OFERTAS
		|--------------------------------------------------------------------------
		*/

		if($total_ofertas > 0)
		{
			// echo 'OFERTAS<br/>';

			$arr_ranked_ofertas = array();

			// Percorre o array de registros das OFERTAS
			foreach($arr_ids_ofertas as $key => $row)
			{
				foreach($row as $ofertas)
				{		
					$arr_title_words = explode(" ", $ofertas->titulo);

					// Cria o array com o total da frequência de termo por registro  Ex.: ID => 1 PALAVRAS ENCONTRADAS => 3 
					if(array_key_exists($ofertas->id, $arr_ranked_ofertas) && array_key_exists($key, $arr_ranked_ofertas[$ofertas->id]))
					{
						$arr_ranked_ofertas[$ofertas->id]['words'][$key] += 1 ;
					}
					else
					{
						$arr_ranked_ofertas[$ofertas->id]['words'][$key] = 1;

						// Verifica outros termos que não foram somados
						foreach($arr_title_words as $word)
						{
							if(in_array($word, $query_search_words) && $word != $key)
							{
								$arr_ranked_ofertas[$ofertas->id]['words'][$word] = 1;
								// $arr_words_idf[$word] += 1;  ** NÃO APAGAR ** 
							}
						}
					}

					// $arr_ranked_ofertas[$ofertas->id]['title'] = $ofertas->titulo; 			// Titulo do registro

					$total_found_words = count($arr_ranked_ofertas[$ofertas->id]['words']); // Total de palavras encontradas (PE)

					// RANK 1 (TF-IDF)
					//==========================================================================================================
					$total_terms = (($total_found_words * $total_found_words) * $total_found_words) / $total_search_words;

					$arr_ranked_ofertas[$ofertas->id]['rank'] = self::getRank($total_all, $total_ofertas, $total_terms);

					// RANK 2 (quantidade do termo / quantidade de palavras)
					//==========================================================================================================
					$tpt = count($arr_title_words);
					$pne = $tpt - $total_found_words;

					$arr_ranked_ofertas[$ofertas->id]['rank2'] = self::getRank2($pne, $tpt);
				}
			}

			$arr_ranked_ofertas = self::array_sort($arr_ranked_ofertas, 'rank2', SORT_DESC);

			// var_dump($arr_ranked_ofertas);

			$arr_result['ofertas'] = $arr_ranked_ofertas;
		}
		
		/*
		|--------------------------------------------------------------------------
		| INSTITUCIONAL
		|--------------------------------------------------------------------------
		*/

		if($total_institucional > 0)
		{
			// echo 'INSTITUCIONAL<br/>';

			$arr_ranked_institucional = array();

			// Percorre o array de registros INSTITUCIONAIS
			foreach($arr_ids_institucional as $key => $row)
			{
				foreach($row as $institucional)
				{
					$arr_title_words = explode(" ", $institucional->titulo);

					// Cria o array com o total da frequência de termo por registro  Ex.: ID => 1 PALAVRAS ENCONTRADAS => 3 
					if(array_key_exists($institucional->id, $arr_ranked_institucional) && array_key_exists($key, $arr_ranked_institucional[$institucional->id]))
					{
						$arr_ranked_institucional[$institucional->id]['words'][$key] += 1 ;
					}
					else
					{
						$arr_ranked_institucional[$institucional->id]['words'][$key] = 1;

						$arr_title_words = explode(" ", $institucional->titulo);

						// Verifica outros termos que não foram somados
						foreach($arr_title_words as $word)
						{
							if(in_array($word, $query_search_words) && $word != $key)
							{
								$arr_ranked_institucional[$institucional->id]['words'][$word] = 1;
								// $arr_words_idf[$word] += 1;  ** NÃO APAGAR ** 
							}
						}
					}

					$arr_ranked_institucional[$institucional->id]['title'] = $institucional->titulo; 	// Titulo do registro
					$arr_ranked_institucional[$institucional->id]['area']  = $institucional->area; 		// Area do registro

					$total_found_words = count($arr_ranked_institucional[$institucional->id]['words']); // Total de palavras encontradas (PE)

					// RANK 1 (TF-IDF)
					//==========================================================================================================
					$total_terms = (($total_found_words * $total_found_words) * $total_found_words) / $total_search_words;

					$arr_ranked_institucional[$institucional->id]['rank'] = self::getRank($total_all, $total_institucional, $total_terms);

					// RANK 2 (quantidade do termo / quantidade de palavras)
					//==========================================================================================================
					$tpt = count($arr_title_words);
					$pne = $tpt - $total_found_words;

					$arr_ranked_institucional[$institucional->id]['rank2'] = self::getRank2($pne, $tpt);
				}
			}

			$arr_ranked_institucional = self::array_sort($arr_ranked_institucional, 'rank2', SORT_DESC);

			// var_dump($arr_ranked_institucional);

			$arr_result['institucionais'] = $arr_ranked_institucional;
		}

		/*
		|--------------------------------------------------------------------------
		| NOTÍCIAS
		|--------------------------------------------------------------------------
		*/

		if($total_noticias > 0)
		{
			// echo 'NOTICIAS<br/>';

			$arr_ranked_noticias = array();

			// Percorre o array de registros das NOTÍCIAS
			foreach($arr_ids_noticias as $key => $row)
			{
				foreach($row as $noticia)
				{
					$arr_title_words = explode(" ", $noticia->titulo);

					// Cria o array com o total da frequência de termo por registro  Ex.: ID => 1 PALAVRAS ENCONTRADAS => 3 
					if(array_key_exists($noticia->id, $arr_ranked_noticias) && array_key_exists($key, $arr_ranked_noticias[$noticia->id]))
					{
						$arr_ranked_noticias[$noticia->id]['words'][$key] += 1 ;
					}
					else
					{
						$arr_ranked_noticias[$noticia->id]['words'][$key] = 1;

						// Verifica outros termos que não foram somados
						foreach($arr_title_words as $word)
						{
							if(in_array($word, $query_search_words) && $word != $key)
							{
								$arr_ranked_noticias[$noticia->id]['words'][$word] = 1;
								// $arr_words_idf[$word] += 1;  ** NÃO APAGAR ** 
							}
						}
					}

					// $arr_ranked_noticias[$noticia->id]['title'] = $noticia->titulo; 		 // Titulo do registro

					$total_found_words = count($arr_ranked_noticias[$noticia->id]['words']); // Total de palavras encontradas (PE)

					// RANK 1 (TF-IDF)
					//==========================================================================================================
					$total_terms = (($total_found_words * $total_found_words) * $total_found_words) / $total_search_words;

					$arr_ranked_noticias[$noticia->id]['rank'] = self::getRank($total_all, $total_noticias, $total_terms);

					// RANK 2 (quantidade do termo / quantidade de palavras)
					//==========================================================================================================
					$tpt = count($arr_title_words);
					$pne = $tpt - $total_found_words;

					$arr_ranked_noticias[$noticia->id]['rank2'] = self::getRank2($pne, $tpt);
				}
			}

			$arr_ranked_noticias = self::array_sort($arr_ranked_noticias, 'rank2', SORT_DESC);

			// var_dump($arr_ranked_noticias);

			$arr_result['noticias'] = $arr_ranked_noticias;
		}

		/*
		|--------------------------------------------------------------------------
		| CRECHES
		|--------------------------------------------------------------------------
		*/

		if($total_creches > 0)
		{
			// echo 'CRECHES<br/>';

			$arr_ranked_creches = array();

			// Percorre o array de registros das CRECHES (Responsabilidades Sociais)
			foreach($arr_ids_creches as $key => $row)
			{
				foreach($row as $creche)
				{
					$arr_title_words = explode(" ", $creche->titulo);

					// Cria o array com o total da frequência de termo por registro  Ex.: ID => 1 PALAVRAS ENCONTRADAS => 3 
					if(array_key_exists($creche->id, $arr_ranked_creches) && array_key_exists($key, $arr_ranked_creches[$creche->id]))
					{
						$arr_ranked_creches[$creche->id]['words'][$key] += 1 ;
					}
					else
					{
						$arr_ranked_creches[$creche->id]['words'][$key] = 1;

						// Verifica outros termos que não foram somados
						foreach($arr_title_words as $word)
						{
							if(in_array($word, $query_search_words) && $word != $key)
							{
								$arr_ranked_creches[$creche->id]['words'][$word] = 1;
								// $arr_words_idf[$word] += 1;  ** NÃO APAGAR ** 
							}
						}
					}

					$arr_ranked_creches[$creche->id]['title'] = $creche->titulo; 		 	// Titulo do registro

					$total_found_words = count($arr_ranked_creches[$creche->id]['words']); 	// Total de palavras encontradas (PE)

					// RANK 1 (TF-IDF)
					//==========================================================================================================
					$total_terms = (($total_found_words * $total_found_words) * $total_found_words) / $total_search_words;

					$arr_ranked_creches[$creche->id]['rank'] = self::getRank($total_all, $total_creches, $total_terms);

					// RANK 2 (quantidade do termo / quantidade de palavras)
					//==========================================================================================================
					$tpt = count($arr_title_words);
					$pne = $tpt - $total_found_words;

					$arr_ranked_creches[$creche->id]['rank2'] = self::getRank2($pne, $tpt);
				}
			}

			$arr_ranked_creches = self::array_sort($arr_ranked_creches, 'rank2', SORT_DESC);

			// var_dump($arr_ranked_creches);

			$arr_result['creches'] = $arr_ranked_creches;
		}

		return $arr_result;

	} // end executeSearch();


	function getLinks($array)
	{
		$result = array();
		$i = 0;
		foreach ($array as $key => $value)
		{
			if(count($value) > 0)
			{
				foreach ($value as $id => $registro)
				{
					if($key == 'empreendimentos')
					{
						$sql = "SELECT
					 				e.id_empreendimento,
					 				e.empreendimento,
									c.cidade,
					 				es.estado
					 			FROM
					 				empreendimentos e 
					 			INNER JOIN cidades c ON e.id_cidade = c.id_cidade
								INNER JOIN estados es ON c.id_estado = es.id_estado
					 			WHERE
									id_empreendimento = ".$id;

				 		$query = $this->db->query($sql);
				  		$row = $query->row(); 

				 		$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
			  			$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
			 			$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;

			 			$result[$i]['tipo']  = 'empreendimento';
			  			$result[$i]['title'] = $row->empreendimento;
			  			$result[$i]['link']  = $url;
					}
					elseif($key == "ofertas")
					{
						$sql = "SELECT titulo, url FROM ofertas WHERE id_oferta = ".$id;

						$query = $this->db->query($sql);
				  		$row = $query->row(); 

						$result[$i]['tipo']  = 'oferta';
			  			$result[$i]['title'] = $row->titulo;
			  			$result[$i]['link']  = site_url().'oferta/'.$row->url;
					}
					elseif($key == "institucionais")
					{
						$result[$i]['tipo']  = 'institucional';
			  			$result[$i]['title'] = $registro['title'];

			  			$a = ($registro['area'] == 'diretoria') ? 'gestao' : $registro['area'];

			  			$result[$i]['link']  = site_url().'empresa#ancora-'.$a;
					}
					elseif($key == "noticias")
					{
						$sql = "SELECT
									n.id_noticia,
									n.titulo
								FROM
									noticias n
								WHERE
									id_noticia = ".$id;

						$query = $this->db->query($sql);
				  		$row = $query->row(); 

						$url= $this->utilidades->sanitize_title_with_dashes($row->titulo);
						$url = site_url().'noticias/'.$row->id_noticia.'/'.$url;

						$result[$i]['tipo']  = 'notícia';
			  			$result[$i]['title'] = $row->titulo;
			  			$result[$i]['link']  = $url;
					}
					elseif($key == "creches")
					{
						$result[$i]['tipo']  = 'responsabilidade social';
			  			$result[$i]['title'] = $registro['title'];
			  			$result[$i]['link']  = site_url().'empresa#ancora-responsabilidade';
					}

					$i++;	
				}
			}
		}

		return $result;
	}



	protected function SearchFoundWords($table, $id_field, $match_field, $q, $resut_type = 0)
	{
		$mode = (strlen($q) >= 3) ? 'IN BOOLEAN MODE' : 'IN NATURAL LANGUAGE MODE';
		$id_field = $id_field;
		$match_field = $match_field;
		$q = $q.'*';

		$sql = "SELECT $id_field AS id, $match_field AS titulo FROM $table WHERE MATCH ($match_field) AGAINST ('$q' $mode)";

		if($table == 'empresa_institucionais')
			$sql = "SELECT $id_field AS id, $match_field AS titulo, area FROM $table WHERE MATCH ($match_field) AGAINST ('$q' $mode)";

		if($table == 'empreendimentos')
			$sql = "SELECT $id_field AS id, $match_field AS titulo FROM $table WHERE MATCH ($match_field) AGAINST ('$q' $mode) AND ativo = 'S'";

		$query = $this->db->query($sql);

		if($resut_type == 1)
		{
			return $query->result();
		}

		return $query->num_rows;
	}


	protected function getRank($total_registros, $registros_correspondentes, $frequencia_do_termo)
	{
		$idf  = (float) log10($total_registros / $registros_correspondentes);

		$rank = (float) $frequencia_do_termo * ($idf * $idf);

		return $rank;
	}

	/**
	 * getRank2(int, int)
	 * 
	 * @param int $total_pne Total de palavras não encontradas no registro
	 * @param int $total_pt  Total de palavras do campo da busca (SELECT)
	 * 
	 * @return float
	 */
	protected function getRank2($total_pne, $total_pt)
	{
		$rank = log1p(($total_pne - $total_pt) / $total_pt);

		return abs((float) $rank);
	}




	private function array_sort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
				asort($sortable_array);
				break;
				case SORT_DESC:
				arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

}


/* End of file busca_model.php */
/* Location: ./system/application/model/busca_model.php */