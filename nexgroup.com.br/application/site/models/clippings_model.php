<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function blacklist(){
		$sql = "SELECT blacklist.`name` FROM blacklist";
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#
	
	function numClippings ()
	{		
		$this->db->select('*')->from('clippings');
		$this->db->close();		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#	
			
	function getListaClippings ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM clippings
				ORDER BY data_cadastro desc
				LIMIT $offset,5
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
#-----------------------------------------------------------------------------------#	
	
	function getClippings ($limit)
	{		
		$this->db->flush_cache();		

		
		$sql = "SELECT *
				FROM clippings
				ORDER BY data_cadastro desc
				LIMIT $limit
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getClipping($id_clipping)
	{
		$this->db->flush_cache();			
		
		$sql = "SELECT *
				FROM clippings
				WHERE id_clipping = $id_clipping
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->row();		

	}
	
	function getImagens($id_clipping)
	{
		$sql = "SELECT *
				FROM galerias
				WHERE id_clipping = $id_clipping
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();		
	}
	
	function getPublicacoes()
	{
		
		$sql = "SELECT MONTH(data_cadastro) as mes,data_cadastro, count(*) as num FROM clippings GROUP BY mes";
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
		
		//$this->db->select('*')->from('clippings');
	}
	
	function getPublicacoesMes($mes,$offset = 0)
	{
		$this->db->flush_cache();		
		
		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM clippings
				WHERE MONTH(data_cadastro) = $mes
				ORDER BY data_cadastro desc
				LIMIT $offset,10
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getClippingsBuscaGeral ($keyword)
	{
		$this->db->flush_cache();
		
				
		$sql = "SELECT *
				FROM clippings
				WHERE ativo = 'S'
				AND titulo LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY titulo ASC
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getNumClippingsBuscaGeral ($keyword)
	{
		$this->db->flush_cache();
		
				
		$sql = "SELECT *
				FROM clippings
				WHERE ativo = 'S'
				AND titulo LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY titulo ASC
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
		
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	