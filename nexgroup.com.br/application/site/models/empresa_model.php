<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
   
	function getResponsabilidadesSocial()
	{
		$sql = "SELECT
					r.id_responsabilidade,
					r.nome,
					r.ano,
					r.imagem_destaque
				FROM
					responsabilidades_social r 
				WHERE 
					r.ativo = 'S'
				ORDER BY r.ordem
				";

		$query = $this->db->query($sql);		
		return $query->result();
	}

	function getResponsabilidadesSocialFotos()
	{
		$sql = "SELECT
				f.id_foto,
				f.id_responsabilidade,
				f.foto,
				f.legenda
				FROM responsabilidades_social_fotos f
				";
				
		$query = $this->db->query($sql);		
		return $query->result();
	}





}


/* End of file empresa_model.php */
/* Location: ./system/application/model/empresa_model.php */
	