<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function blacklist(){
		$sql = "SELECT blacklist.`name` FROM blacklist";
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#
	
	function numNoticias ()
	{		
		$this->db->select('*')->from('noticias');
		$this->db->close();		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#	
			
	function getListaNoticias ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM noticias
				ORDER BY data_cadastro desc
				LIMIT $offset,5
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
#-----------------------------------------------------------------------------------#	
	
	function getNoticias($limit)
	{		
		$sql = "SELECT 
					n.id_noticia, n.data_cadastro, n.titulo, n.imagem_destaque
				FROM
					noticias n
				WHERE
					n.ativo = 'S'
				ORDER BY n.data_cadastro DESC
				LIMIT $limit";		
		
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getNoticia($id_noticia)
	{
		$this->db->flush_cache();			
		
		$sql = "SELECT *
				FROM noticias
				WHERE id_noticia = $id_noticia
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->row();		

	}
	
	function getImagens($id_noticia)
	{
		$sql = "SELECT *
				FROM galerias
				WHERE id_noticia = $id_noticia
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();		
	}
	
	function getPublicacoes()
	{
		
		$sql = "SELECT MONTH(data_cadastro) as mes,data_cadastro, count(*) as num FROM noticias GROUP BY mes";
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
		
		//$this->db->select('*')->from('noticias');
	}
	
	function getPublicacoesMes($mes,$offset = 0)
	{
		$this->db->flush_cache();		
		
		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM noticias
				WHERE MONTH(data_cadastro) = $mes
				ORDER BY data_cadastro desc
				LIMIT $offset,10
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getNoticiasBuscaGeral ($keyword)
	{
		$this->db->flush_cache();
		
				
		$sql = "SELECT *
				FROM noticias
				WHERE ativo = 'S'
				AND titulo LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY titulo ASC
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
		
	}
	
	function getNumNoticiasBuscaGeral ($keyword)
	{
		$this->db->flush_cache();
		
				
		$sql = "SELECT *
				FROM noticias
				WHERE ativo = 'S'
				AND titulo LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY titulo ASC
			   ";	
		
		$query = $this->db->query($sql);
		$this->db->close();
		
		return $query->num_rows;
		
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	