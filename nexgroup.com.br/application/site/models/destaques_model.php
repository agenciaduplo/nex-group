<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaques_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
#-----------------------------------------------------------------------------------#
	
	function numDestaques ()
	{		
		$this->db->select('*')->from('destaques');		
		$this->db->close();
		
		return $this->db->count_all_results();
	}
	#-----------------------------------------------------------------------------------#		
	function getListaDestaques ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else 
		{
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM destaques
				ORDER BY data_cadastro desc
				LIMIT $offset,10
			   ";		
		
		$query = $this->db->query($sql);		
		$this->db->close();
		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#		
	function getDestaques ()
	{		
		$this->db->flush_cache();		

		$sql = "SELECT *
				FROM destaques
				ORDER BY data_cadastro desc
			   ";		
		
		$query = $this->db->query($sql);
		$this->db->close();
				
		return $query->result();
	}
	
	function getDestaque()
	{		
		$this->db->select('*')->from('destaques')->order_by('id_destaque','desc')->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	