<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex,nofollow">
	<title>Nexchange - Cadastro realizado com sucesso</title>
	<style type="text/css">
		.nexchange-response-page {font-family: "Arial";margin: 0;padding: 0;}
		/*.nexchange-response-page {font-family: "montserrat_bold,Arial,Helvetica,sans-serif";margin: 0;padding: 0;}*/
		.nexchange-response-page .response-content {width: 300px;background-color: #F4F4F4;padding: 15px 0;}
		.nexchange-response-page .response-content > h3 {margin: 0;padding: 0;font-size: 20px;color: #333;text-align: center;}
		.nexchange-response-page .response-content > p {width: 200px;margin: 10px auto 0 auto;padding: 0;font-size: 13px;color: #333;}
	</style>
</head>
<body class="nexchange-response-page">
	<div class="response-content">
		<h3>CADASTRO ENVIADO</h3>
		<p>
			Seu cadastro no Nex Change, foi realizado com sucesso!<br/>
			Em breve entraremos em contato com você.<br/>
		</p>
	</div>
</body>
</html>