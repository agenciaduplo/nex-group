<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
		<title>Google Maps - Nex group</title>
		<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
	
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
			var map;
		  	function initialize() 
		  	{	
		  		var myLatlng = new google.maps.LatLng(-30.027238,-51.182107);
		    	var myOptions = {
		      		zoom: 14,
		      		center: myLatlng,
		      		mapTypeId: google.maps.MapTypeId.ROADMAP
		    	}
		    	
		    	map = new google.maps.Map(document.getElementById("map_nex"), myOptions);
				
		    	var marker = new google.maps.Marker({
		        	position: myLatlng, 
		        	map: map, 
		        	title: "Nex Group"
		    	});   		    	
		  	}
		</script>
		
	</head>
	
	<body onload="initialize()">
		
		<div style="width:100%;height:100%" id="map_nex"></div>
	
	</body>

</html>