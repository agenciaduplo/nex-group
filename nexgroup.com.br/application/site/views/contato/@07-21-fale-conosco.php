<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-fixo");  ?>

<?php $this->load->view("includes/modal_atendimento_nex"); ?>
<?php $this->load->view("includes/modal_contato_cliente"); ?>
<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>
<?php $this->load->view("includes/modal_mapa");  ?>

<!-- contato -->
<section id="contato">
	<div class="box triple">
		<img src="<?=base_url()?>assets/site/img/contato-comprar.png" alt="">
		<h2>Quero ter um Nex</h2>
		<i>
			- Informações sobre empreendimentos<br>
			- Trocar meu imóvel usado<br>
			- Conversar com o corretor<br><br>
		</i>
		<a class="btn small btn-red button nomargin-left" rel="contact, nofollow" href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">Converse agora</a>
		<a class="btn small btn-red button click_contato" rel="nofollow" href="javascript:void(0);">ENVIAR CONTATO</a>
	</div>

	<div class="box triple">
		<img src="<?=base_url()?>assets/site/img/contato-info.png" alt="">
		<h2>Já tenho um Nex</h2>
		<i>
			- Atualizar seus cadastros<br>
			- Verificar informações financeiras<br>
			- 2ª via de boletos<br>
			- Elogios, sugestões e reclamações
		</i>
		<a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" class="btn small btn-red button nomargin-left" target="_blank">PORTAL DO CLIENTE</a> <!-- ÁREA DO CLIENTE NEX -->
		<a class="btn small btn-red button click_contato_cliente" rel="nofollow" href="javascript:void(0);">ENVIAR CONTATO</a>
	</div>

	<div class="box triple">
		<img src="<?=base_url()?>assets/site/img/contato-interesse.png" alt="">
		<h2>Venha para a Rede Nex</h2>
		<i>
			- Seja um fornecedor<br>
			- Venda seu terreno<br>
			- Trabalhe conosco<br>
			- Sou corretor e quero vender Nex
		</i>
		<a class="btn small btn-red button nomargin-left" rel="contact, nofollow" href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">Converse agora</a>
		<a class="btn small btn-red button click_atendimento_nex" rel="nofollow" href="javascript:void(0);">ENVIAR CONTATO</a>
	</div>

	<div class="box full">
		<div class="row">
			<img src="<?=base_url()?>assets/site/img/contato-chegar.png" alt="" class="left">
			<div class="right-full">
				<h2>COMO CHEGAR NA NEX</h2>
				<i>
					Furriel Luiz Antônio Vargas, 250<br>
					9° andar - CEP: 90470.130<br>
					Porto Alegre - RS - Brasil<br>
					(51) 3378-7800
				</i>
				<!-- <a class="btn small btn-red button nomargin-left click_mapa">Ver mapa</a> -->
				<a href="https://www.google.com/maps/dir//R.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil/@-30.0276634,-51.1828499,17z/data=!4m13!1m4!3m3!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2sR.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil!3b1!4m7!1m0!1m5!1m1!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2m2!1d-51.1828499!2d-30.0276634" class="btn small btn-red button" target="_blank" style="margin: 1em 0 0 0;">Como Chegar</a>
			</div>
		</div>
	</div>

</section>
<!-- /contato -->

<?php $this->load->view("includes/footer"); ?>

<script type="text/javascript">
	mapaNex();
</script>