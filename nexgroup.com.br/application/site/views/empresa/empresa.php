<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-new"); ?>
<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<!-- DESTAQUE -->
<section id="destaque" class="destaque-empresa">
	<div class="list_carousel ">
		<ul id="">
			<li class="box scales paral" style="background:url(<?=base_url()?>assets/site/img/nex5.jpg) right; height: 702px;">
				<div class="box2">
					<div class="left">
						<h2>NEXGROUP</h2>
						<h3><i>Construir é a nossa vida.</i></h3>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
		</ul>
	</div>
</section>

<!-- EMPREENDIMENTOS INTERNA -->
<section id="empreendimentos-interna" class="pages-empresa">
	<div id="ancora-topo" style="float: left; width: 98%; margin-top: -80px;"></div>

	<div id="ancora3"></div>
	<div id="ancora4"></div>

	<!-- SUB MENU -->
	<section class="submenu-home relativo">
		<div class="row">
			<div class="left clickroll">
				<ul>
					<li class="active nomobile"><a href="#ancora-apresentacao" id="btn-apresentacao" onclick="selecioneMenuInterna(this);">Apresentação<span></span></a></li>
					<li class="nomobile"><a href="#ancora-realizacoes" id="btn-realizacoes" onclick="selecioneMenuInterna(this);">Realizações<span></span></a></li>
					<li class="nomobile"><a href="#ancora-gestao" id="btn-gestao" onclick="selecioneMenuInterna(this);">Gestão Corporativa<span></span></a></li>

					<li class="nomobile"><a href="#ancora-obras" id="btn-obras" onclick="selecioneMenuInterna(this);">Obras Realizadas<span></span></a></li>
					<li class="nomobile"><a href="#ancora-responsabilidade" id="btn-responsabilidade" onclick="selecioneMenuInterna(this);">Responsabilidade<span></span></a></li>

					<li class="nomobile"><a href="#ancora-rh" id="btn-rh" onclick="selecioneMenuInterna(this);" style="padding: 1.6em 1.8em 2.3em 1em;">RH<span></span></a></li>


				</ul>
			</div>
			<div class="right">
				<a href="<?php echo site_url(); ?>contato" class="btn small btn-red button right">Entrar em contato</a>
			</div>
		</div>
	</section>

	<!-- BREADCAMPS  -->
	<section class="breadcamps">
		<div class="row ">
			<div class="bread columns">
				<a href="<?=base_url()?>" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
				<ul class="breadcrumbs">
					<li><a href="<?=base_url()?>">Home</a></li>
					<li><i class="icon icon-angle-right"></i></li>
					<li class="unavailable"><a href="#">A Empresa</a></li>
				</ul>
			</div>
		</div>
	</section>

	<!-- APRESENTAÇÃO  -->
	<div id="ancora-apresentacao" style="float: left; width: 98%; margin-top: -200px;"></div>
	<div id="ancora-apresentacao2" style="float: left; width: 98%; margin-top:0px;"></div>

	<section class="apresentacao">
		<div class="row">

			<img src="<?=base_url()?>/assets/site/img/logo2.png" alt="">

			<i class="text-emp">
				A Nex Group é uma grande incorporadora, resultado da união de 4 construtoras gaúchas: Capa Engenharia, DHZ Construções, EGL Engenharia e Lomando Aita Engenharia. <br><br>
				São muitos anos de experiência somados, levando os melhores empreendimentos imobiliários para diferentes regiões do país. Mais que transformar desafios em satisfações para os seus clientes, a Nex oferece produtos acima das expectativas, atendendo sempre a todos os diferentes momentos e a diferentes necessidades.<br/>
				E dessa forma constrói sucessos.
				<ul class="addthis_toolbox addthis_default_style " style="margin:2em 0 3em 0;">
					<!-- <li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_facebook_like" fb:like:layout="button_count"></a></li> -->
					<li><div class="fb-like" data-href="https://www.facebook.com/NexGroup" data-send="false" data-show-faces="true" data-font="arial"></div></li>
					<li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_tweet"></a></li>
					<li><g:plusone size="medium"></g:plusone></li>
					<!--<li><a class="addthis_counter addthis_pill_style"></a></li>-->
				</ul>
			</i>

			<h2>EMPRESAS QUE COMPÕEM NEX GROUP:</h2>

			<div class="subox radius"> 
				<span><figure class="capa"></figure></span>
				<p style="display:;">
					Atuando no mercado da construção civil desde 1985, e acreditando que construir é investir na qualidade de vida das pessoas, a Capa Engenharia tornou-se responsável por mais de 50 empreendimentos residenciais e comerciais. Durante esses anos a Capa construiu mais de 1 milhão de metros quadrados em solo gaúcho e catarinense.
				</p>  
			</div>  
			<div class="subox radius"> 
				<span><figure class="dhz"></figure></span>
				<p style="display:;">
					Desde 1976, a DHZ prima pela credibilidade e eficiência de seus empreendimentos no segmento residencial de renda média.
					Construiu mais de 450.000 m² distribuídos em 2.235 unidades entregues.
				</p>  
			</div>  
			<div class="subox radius"> 
				<span><figure class="egl"></figure></span>
				<p style="display:;">
					Com mais de 50 imóveis entregues a sociedade desde 1986, a incorporadora EGL Engenharia consolidou-se na construção de empreendimentos residenciais e comerciais para o segmento de média renda, promovendo a satisfação a quem habita e rentabilidade a quem investe.
				</p>  
			</div>  
			<div class="subox radius"> 
				<span><figure class="lomando"></figure></span>
				<p style="display:;">
					A Lomando Aita construiu sua trajetória na construção civil, desde 1983, com empreendimentos residenciais. A empresa especializou-se em construções de baixo custo, voltadas para os segmentos de baixa e média renda, atuando fortemente no Programa de Atendimento Residencial (PAR) e Minha Casa, Minha Vida.
				</p>  
			</div>  

		</div>
	</section>
	<!-- .apresentacao -->

	<!-- REALIAZAÇÕES  -->
	<div id="ancora-realizacoes" style="float: left; width: 98%; margin-top: -120px;"></div>

	<section class="realizacoes">
		<div class="row">
			<h2>REALIZAÇÕES<br><i>Transformar desafios em realizações. Isso é construir.</i></h2>
			<ul style="padding-bottom: 60px;">
				<li>
					<span>1</span>
					<p>
						<b>Quantas unidades foram entregues em 2015?</b><br>
						Mais de 1.085 unidades foram entregues pela Nex Group em v&aacute;rios segmentos.
					</p>
				</li>
				<li>
					<span>2</span>
					<p>
						<b>Qual o volume de obras que está sendo executado neste momento pela Nex Group?</b><br>
						Atualmente mais de 4786 unidades estão em construção.
					</p>
				</li>
				<li>
					<span>3</span>
					<p>
						<b>Qual a projeção de unidades a serem entregues em 2016?</b><br>
						Mais de 2.687 unidades serão entregues.
					</p>
				</li>
				<li>
					<span>4</span>
					<p>
						<b>E para 2015, qual a meta em VGV de lançamentos e vendas?</b><br>
						A meta é de R$ 600 milhões, tanto para vendas como para lançamentos.
					</p>
				</li>
				<li>
					<span>5</span>
					<p>
						<b>Em que regiões a Nex Group atua?</b><br>
						Nos estados do RS e SC.
					</p>
				</li>
				<li>
					<span>6</span>
					<p>
						<b>Em quais segmentos a Nex Group atua?</b><br>
						Basicamente, em 4 segmentos distintos: no MCMV (Minha Casa Minha Vida); no segmento econômico; no segmento de média renda; e no segmento de alta e altíssima renda, construindo condomínios residenciais horizontais, verticais e prédios comerciais.
					</p>
				</li>
			</ul>
		</div>
	</section>
	<!-- .realizacoes -->


	<!-- GESTAO CORPORATIVA  -->
	<div id="ancora-gestao" style="float: left; width: 98%; margin-top: -120px;"></div>

	<section class="gestao-corp">
		<div class="row">
			<h2>GESTÃO CORPORATIVA<br><i></i></h2>

			<div class="left-corpo">

				<h3>Visão, Missão e Valores</h3>

				<div class="abas-mis">
					<div class="top">
						<a id="MostraAba1" class="active">Visão</a>	
						<a id="MostraAba2" class="">Missão</a>	
						<a id="MostraAba3" class="">Valores</a>	
					</div>
					<div class="down">
						<p id="Aba1" class="uno"> <!-- Visão -->
							Crescer com rentabilidade de forma sustentável através da melhoria contínua de seus processos, capacitação de seus colaboradores e  com responsabilidade sócio-ambiental.					
						</p>
						<p id="Aba2" class="dos" style="display:none;"> <!-- Missão -->
							Satisfazer as expectativas de todos os envolvidos gerando crescimento sustentável, credibilidade e processos produtivos e gerenciais qualificados.
						</p>
						<p id="Aba3" class="tres" style="display:none;"> <!-- Valores -->
							• Busca pela competência com eficácia <br>
							• Responsabilidade<br>
							• Busca contínua de satisfação dos clientes internos e externos<br>
							• Honestidade<br>
							• Seriedade<br>
							• Dedicação
						</p>
					</div>
				</div>

			</div>

			<div class="right-corpo">
				<h3>Política da Qualidade</h3>
				<p>A política da Nex Group é fornecer aos seus clientes produtos que atendam suas expectativas, através da melhoria contínua de seus processos e do desenvolvimento de seus colaboradores.</p>
			</div>

			<div class="all-corpo">
				<h3>Principais Gestores</h3>

				<ul>
					<li>
						<h2>Carlos Alberto de Moraes Schettert</h2>
						<p>Presidente</p>
					</li>
					<li>
						<h2>Gustavo Schettert Moreira</h2>
						<p>Diretor de Planejamento</p>
					</li>
					<li>
						<h2>Leonardo Ribeiro<br>Martins</h2>
						<p>Diretor de Produção</p>
					</li>
					<li>
						<h2>Vanderlei Evandro Tamiosso</h2>
						<p>Diretor Administrativo Financeiro</p>
					</li>
				</ul>
				<ul>
					<li>
						<h2>Marcus Vinicius Amaro Batista</h2>
						<p>Diretor Comercial</p>
					</li>
					<li>
						<h2>Carlos Alberto<br>Aita</h2>
						<p>Gerente de Novos Negócios</p>
					</li>
					<li>
						<h2>José Luiz Lima<br>Lomando</h2>
						<p>Gerente Geral de Obras</p>
					</li>


				</ul>


				<!--
				<div class="list_carousel list-obras" id="id_empresa-1">
					<ul id="foo_gestao">
						<li>
								<div class="box scales" style="background:url(<?=base_url()?>assets/site/img/diretoria/01.jpg)"></div>
								<div class="left"><h2><b>carlos alberto schettert</b><br>Presidente</h2></div>
								<div class="pix"></div>
						</li>
						<li>
								<div class="box scales" style="background:url(<?=base_url()?>assets/site/img/diretoria/02.jpg)"></div>
								<div class="left"><h2><b>gUSTAVO SCHETTERT</b><br>Diretor de Planejamento</h2></div>
								<div class="pix"></div>
						</li>
						<li>
								<div class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></div>
								<div class="left"><h2><b>leonardo martins</b><br>Diretor de Produção</h2></div>
								<div class="pix"></div>
						</li>
						<li>
								<div class="box scales" style="background:url(<?=base_url()?>assets/site/img/diretoria/04.jpg)"></div>
								<div class="left"><h2><b>vanderlei tamiosso</b><br>Diretor Administrativo Financeiro</h2></div>
								<div class="pix"></div>
						</li>
						<li>
								<div class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></div>
								<div class="left"><h2><b>vinícius batista</b><br>Diretor Comercial</h2></div>
								<div class="pix"></div>
						</li>

					</ul>
					<div class="clearfix"></div>
					<div class="navegation">
						<a href="javascript:;" id="prev_gestao" class="prev nomobile" rel="nofollow"></a>
						<div id="pager_gestao" class="pager"></div>
						<a href="javascript:;" id="next_gestao" class="next nomobile" rel="nofollow"></a>
					</div>				
				</div>
-->

			</div>



		</div> <!-- .row -->
	</section>
	<!-- GESTAO CORPORATIVA -->


	<!-- OBRAS REALIAZAÇÕES  -->
	<div id="ancora-obras" style="float: left; width: 98%; margin-top: -120px;"></div>
	<section class="obras-realizadas">
		<div class="row">
			
			<h2>OBRAS REALIZADAS<br><i>4 construtoras diferentes, um só objetivo: inovar a cada nova realização.</i></h2>
			
			<div class="top">
				<ul class="right">
					<li>
						<select onchange="mostrarObrasPorEmpresa(this)">
							<option value="0">Todas Empresas</option>
							<option value="1">Capa Engenharia</option>
							<option value="2">DHZ Construções</option>
							<option value="3">EGL Engenharia</option>
							<option value="4">Lomando, Aita Engenharia</option>
						</select>
					</li>
				</ul>
			</div>

			<div id="list-obras-container">

				<?php $total_obras = count($obras_todas); ?>

				<!-- todas obras -->
				<div class="list_carousel list-obras" id="id_empresa-0">

					<h3 style="color: #333; text-align: center;">Todas Empresas (<?=$total_obras?> obras)</h3>

					<ul class="carouselObras">
						<?php
						$i=0;
						foreach($obras_todas as $row) :

						if($row->fachada)
							$img_fachada = UPLOADS_PATH . 'obras_realizadas/' . $row->fachada;
						else
							$img_fachada = base_url() . 'assets/site/img/bg-default.png';
						?>
						<li class="box scales" style="background:url(<?=$img_fachada?>)">
							<div class="left">
								<h2><?=$row->empreendimento?></h2>
							</div>
						</li>
						<?php
						$i++;
						endforeach;

						for($j = $i; $j < 5; $j++) :
							echo '<li class="box scales" style="background:url(' . base_url() . 'assets/site/img/bg-default.png)"></li>';
						endfor;
						?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation ObrasCarouselNav">
						<span class="obraPrev prev nomobile"></span>
						<div class="obraPager pager"></div>
						<span class="obraNext next nomobile"></span>
					</div>
				</div>

				<!-- obras_capa -->
				<div class="list_carousel list-obras" id="id_empresa-1" style="opacity: 0;">

					<h3 style="color: #333; text-align: center;">Capa Engenharia (<?=count($obras_capa).' de '.$total_obras?> obras)</h3>

					<ul id="foo11">
						<?php
						$i=0;
						foreach($obras_capa as $row) :

						if($row->fachada)
							$img_fachada = UPLOADS_PATH . 'obras_realizadas/' . $row->fachada;
						else
							$img_fachada = base_url() . 'assets/site/img/bg-default.png';
						?>
						<li class="box scales" style="background:url(<?=$img_fachada?>)">
							<div class="left">
								<h2><?=$row->empreendimento?></h2>
							</div>
						</li>
						<?php
						$i++;
						endforeach;

						for($j = $i; $j < 5; $j++) :
							echo '<li class="box scales" style="background:url(' . base_url() . 'assets/site/img/bg-default.png)"></li>';
						endfor;
						?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation ObrasCarouselNav">
						<span id="prev11" class="prev nomobile"></span>
						<div id="pager11" class="pager"></div>
						<span id="next11" class="next nomobile"></span>
					</div>
				</div>

				<!-- obras_dhz -->
				<div class="list_carousel list-obras" id="id_empresa-2" style="opacity: 0;">

					<h3 style="color: #333; text-align: center;">DHZ Construções (<?=count($obras_dhz).' de '.$total_obras?> obras)</h3>

					<ul id="foo13">
						<?php
						$i=0;
						foreach($obras_dhz as $row) :

						if($row->fachada)
							$img_fachada = UPLOADS_PATH . 'obras_realizadas/' . $row->fachada;
						else
							$img_fachada = base_url() . 'assets/site/img/bg-default.png';
						?>
						<li class="box scales" style="background:url(<?=$img_fachada?>)">
							<div class="left">
								<h2><?=$row->empreendimento?></h2>
							</div>
						</li>
						<?php
						$i++;
						endforeach;

						for($j = $i; $j < 5; $j++) :
							echo '<li class="box scales" style="background:url(' . base_url() . 'assets/site/img/bg-default.png)"></li>';
						endfor; 
						?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation ObrasCarouselNav">
						<span id="prev13" class="prev nomobile"></span>
						<div id="pager13" class="pager"></div>
						<span id="next13" class="next nomobile"></span>
					</div>
				</div>

				<!-- obras_egl -->
				<div class="list_carousel list-obras" id="id_empresa-3" style="opacity: 0;">

					<h3 style="color: #333; text-align: center;">EGL Engenharia (<?=count($obras_egl).' de '.$total_obras?> obras)</h3>

					<ul id="foo12">
						<?php
						$i=0;
						foreach($obras_egl as $row) :

						if($row->fachada)
							$img_fachada = UPLOADS_PATH . 'obras_realizadas/' . $row->fachada;
						else
							$img_fachada = base_url() . 'assets/site/img/bg-default.png';
						?>
						<li class="box scales" style="background:url(<?=$img_fachada?>)">
							<div class="left">
								<h2><?=$row->empreendimento?></h2>
							</div>
						</li>
						<?php
						$i++;
						endforeach;

						for($j = $i; $j < 5; $j++) :                  
							echo '<li class="box scales" style="background:url(' . base_url() . 'assets/site/img/bg-default.png)"></li>';
						endfor; 
						?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation ObrasCarouselNav">
						<span id="prev12" class="prev nomobile"></span>
						<div id="pager12" class="pager"></div>
						<span id="next12" class="next nomobile"></span>
					</div>
				</div>

				<!-- obras_lomando -->
				<div class="list_carousel list-obras" id="id_empresa-4" style="opacity: 0;">

					<h3 style="color: #333; text-align: center;">Lomando, Aita Engenharia (<?=count($obras_lomando).' de '.$total_obras?> obras)</h3>

					<ul id="foo14">
						<?php
						$i=0;
						foreach($obras_lomando as $row) :

						if($row->fachada)
							$img_fachada = UPLOADS_PATH . 'obras_realizadas/' . $row->fachada;
						else
							$img_fachada = base_url() . 'assets/site/img/bg-default.png';
						?>
						<li class="box scales" style="background:url(<?=$img_fachada?>)">
							<div class="left">
								<h2><?=$row->empreendimento?></h2>
							</div>
						</li>
						<?php
						$i++;
						endforeach;

						for($j = $i; $j < 5; $j++) :
							echo '<li class="box scales" style="background:url(' . base_url() . 'assets/site/img/bg-default.png)"></li>';
						endfor;
						?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation ObrasCarouselNav">
						<span id="prev14" class="prev nomobile"></span>
						<div id="pager14" class="pager"></div>
						<span id="next14" class="next nomobile"></span>
					</div>
				</div>

			</div> <!-- #list-obras-container; -->

		</div> <!-- .row; -->
	</section>
	<!-- .obras-realizadas; -->

	<!-- RESPONSABILIDADE SOCIAL  -->
	<div id="ancora-responsabilidade" style="float: left; width: 98%; margin-top: -120px;"></div>

	<section class="responsabilidade">
		<div class="row">
			<h2>RESPONSABILIDADE SOCIAL<br><i>Também ajudamos a construir dias melhores.</i></h2>
			<div class="left">
				<p>
					A Nex Group já doou sete creches à Prefeitura de Porto Alegre, dentro do Programa Parceria Público Privada, com o objetivo de ajudar e potencializar o desenvolvimento das comunidades carentes da cidade. Essas doações fizeram parte do Projeto Engenharia Social da empresa que é desenvolvido através do comprometimento da Nex e empresas parceiras em desempenhar um papel na construção de um futuro melhor para crianças de baixa renda. <br><br>
				</p>		

				<p>
					<b>Veja as principais empresas parceiras que se engajaram no Projeto Engenharia Social:</b> <br><br>

					<div class="mini-box">
						<p>Alusistem Alumínio para Construções Ltda</p>
						<p>Belmetal Indústria e Comércio Ltda</p>
						<p>Cecrisa Revestimentos Cerâmicos Ltda</p>
						<p>Cerâmica Eliane</p>
						<p>Docol Metais Sanitários</p>
						<p>Elektra Distribuidora de Materiais Elétricos</p>
						<p>Estádio 3 Engenharia de Estruturas</p>
						<p>Hydrus Projetos de Instalações Sanitárias</p>
						<p>Iguaçu Pré Fabricados de Granito</p>
						<p>Incepa Revestimentos Cerâmicos</p>
					</div>
					<div class="mini-box" style="float:right; padding:0; border:0">
						<p>Instaladora Base Ltda</p>
						<p>Irmãos Ciocari Ltda</p>
						<p>Knauf do Brasil Ltda</p>
						<p>Metalúrgica Borba</p>
						<p>Petra Soluções em Pedras Ltda</p>
						<p>Pauluzzi Produtos Cerâmicos Ltda</p>
						<p>Plenobrás Distribuidora Ltda</p>
						<p>Salvaro Produtos de Madeira</p>
						<p>Sanidro Instalações Hidráulicas Ltda</p>
						<p>Tintas Killing</p>
						<p>Tintas Kresil</p>
						<p>Vidro Box Vidros Gerais Ltda</p>
					</div>
					

				</p>						
			</div>
			<div class="right">
				<h3>As creches entregues pela Nex Group foram:</h3>
				<?php
				$cont_creches = count($responsabilidades);

				if($cont_creches > 0) :
				$i = 0;
				foreach($responsabilidades as $creche) :

					if($i == 0)
						echo '<ul>';

					if($i % 4 == 0)
						echo '</ul><ul>';
				?>
				<li>
					<figure style="background:url(<?=UPLOADS_PATH . 'responsabilidades_social/zoom/' . $creche->imagem_destaque?>);"></figure>
					<span><?=$creche->ano?></span>
					<h4><?=$creche->nome?></h4>
					<a href="javascript:void(0)" onclick="openCrecheGalery(<?=$creche->id_responsabilidade?>);">Ver galeria de fotos</a>
					<a id="creche-fancy-<?=$creche->id_responsabilidade?>" href="<?=UPLOADS_PATH . 'responsabilidades_social/zoom/' . $creche->imagem_destaque?>" class="fancybox-thumb" rel="creche<?=$creche->id_responsabilidade?>" title="<?=$creche->nome?>" style="display: none;"></a>
					<?php
					if(array_key_exists($creche->id_responsabilidade, $responsabilidades_fotos)) 
						foreach($responsabilidades_fotos[$creche->id_responsabilidade] as $foto)
							echo '<a id="creche-fancy-'.$creche->id_responsabilidade.'" href="'.UPLOADS_PATH.'responsabilidades_social/galeria/'.$foto['foto'].'" class="fancybox-thumb" rel="creche'.$creche->id_responsabilidade.'" title="'.$foto['legenda'].'" style="display: none;"></a>';
					?>
				</li>
				<?php
				$i++;
				if($i == $cont_creches)
					echo '</ul>';
				endforeach;
				endif;
				?>
			</div>
		</div> <!-- .row -->
	</section>
	<!-- .responsabilidade -->



	<!-- RECURSOS HUMANOS  -->
	<div id="ancora-rh" style="float: left; width: 98%; margin-top: -120px;"></div>

	<section class="rh-corp">
		<div class="row">
			<h2>RECURSOS HUMANOS<br><i></i></h2>
			<div class="left-corpo">
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/01.jpg">
					<p>
						O Nossa Nex visa fortalecer a identificação dos colaboradores com a Nex Group, simbolizando o espírito de integração e empreendedorismo da empresa. É a marca para os programas de RH, que reconhecem, valorizam e desenvolvem as equipes; dentre estes: Treinamento de Lideranças Nex, Aniversariantes do Mês, Datas Comemorativas, ….
					</p>
				</div>
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/02.jpg">
					<p>
						O Gestão Solidária – GS estabelece uma gestão participativa entre direção e colaboradores visando a melhoria contínua do clima organizacional, através da implementação das suas três etapas: 1º Pesquisa de Clima, 2º Grupos Operativos e 3º Plano de Ação – Implementação.
					</p>
				</div>
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/03.jpg">
					<p>
						O objetivo do Programa de Estágio Crescer Nex é oferecer aos estudantes a oportunidade de complementarem sua formação escolar, possibilitando que na sua experiência prática possam ter um aperfeiçoamento técnico, um aprendizado de competências e a construção de relacionamentos. É também o momento da Nex identificar os talentos, de forma a desenvolvê-los para assumirem futuras posições dentro da empresa. Venha crescer na Nex também, <a href="http://vagas.nex-group.infojobs.com.br/" target="_blank">cadastre-se para ser nosso estagiário</a>.
					</p>
				</div>
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/04.jpg">
					<p>
						É o reconhecimento por tempo de empresa aos funcionários aniversariantes a cada cinco anos completos de Nex Group. Mensalmente são divulgados os homenageados e é realizada cerimônia de homenagem na festa de confraternização de final de ano. 
					</p>
				</div>
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/05.jpg">
					<p>
						Trabalhamos pela satisfação do nossos clientes com imóveis de qualidade, credibilidade  e responsabilidade socioambiental. <a href="http://vagas.nex-group.infojobs.com.br/" target="_blank">Trabalhe conosco</a> e seja bem vindo a uma das maiores construtoras do RS.
					</p>
				</div>
				<div class="casxi">
					<img src="<?=site_url()?>assets/site/img/rh/06.jpg">
					<p>
						Valorização da Vida, priorizando a saúde dos envolvidos nas diversas atividades pela Nex Group, baseado em um Sistema de Gestão de Saúde e Segurança do Trabalho, estruturado em procedimentos, análises de riscos fundamentados nos aspectos legais.
					</p>
				</div>
			</div>

			<div class="right-corpo">
				<h3>Papel do RH</h3>
				<p>
					Desenvolver políticas e ações de RH, que propiciem o fortalecimento da identidade dos colaboradores com a Nex Group, visando a  excelência operacional, otimização de processos, fortalecimento das áreas de apoio, para que estas sejam realmente estratégicas para a expansão e sustentabilidade dos negócios, propiciando o impacto positivo no clima organizacional. 
				</p>
				
				<h3>Competências Nex Group</h3>
				<p>
					A Nex Group caracteriza-se por ser voltada para o contínuo aperfeiçoamento de seus produtos/serviços e de seus colaboradores.
				</p>

				<div class="left">
					<div class="box">
						<h4>Iniciativa</h4>
						<p>Possuir predisposição para crescer, aprender e assumir novas responsabilidades.</p>
					</div>
					<div class="box fnove">
						<h4>Comprometimento</h4>
						<p>Estar vinculado aos objetivos da empresa. Planejar e realizar tarefas, visando sempre à qualidade de execução e à consequente satisfação dos clientes internos e externos.</p>
					</div>
					<div class="box">
						<h4>Colaboração</h4>
						<p>Trabalhar em grupo. Saber tanto auxiliar um colega na execução do serviço, quanto delegar tarefas, tendo sempre como objetivos um trabalho com qualidade.</p>
					</div>
					<div class="box fnove">
						<h4>Senso Crítico</h4>
						<p>Analisar rotinas e procedimentos, otimizando-as quando necessário e encontrando soluções para problemas quando houver.</p>
					</div>
				</div>
			</div>

		</div> <!-- .row -->
	</section>
	<!-- RECURSOS HUMANOS -->

</section>
<!-- EMPREENDIMENTOS HOME END -->

<script type="text/javascript">
	function openCrecheGalery(id){$('#creche-fancy-'+id).trigger('click');}
</script>

<?php $this->load->view("includes/footer"); ?>