<?php $this->load->view("includes/header");  ?>
<?php $this->load->view("includes/header-html-new");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<script>
	var status_id = '-'; //o primeiro filtro é feito com todos
	var imoveisBusca = <?=json_encode($arrImoveisBusca)?>

	$(window).load(function(){
		if($(window).width() <= 882){$("#cards").trigger("click");}
	});
</script>

<!-- EMPREENDIMENTOS HOME -->

<div id="ancora2"></div>

<section id="empreendimentos-procurar">

	<div id="ancora3"></div>

	<!-- PROCURAR FIELD -->
	<div class="row">
		<div class="procurar-box">
			<label>
				Procure os empreendimentos por palavra-chave, cidade, estado, ...
				<form name="formBusca" id="formBusca" action="<?=base_url()?>imoveis/buscar" method="post" onsubmit="submitBusca(); return false;">
					<input type="hidden" name="buscar" value="1" />
					<input type="text" name="palavra-chave" id="palavrachave" value="<?=@$palavraChave; ?>" placeholder="insira sua busca..." />
				</form>
			</label>
			<p><i>Mais procuradas:</i>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Porto Alegre</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Canoas</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Pelotas</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Chácara</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Polo</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Vergéis</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Síngolo</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Jk Parque Clube</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Life Park</span></a>
				<a href="javascript:void(0);" onclick="buscar(this)" rel="nofollow"><span>Riserva Piattelli</span></a>
			</p>
		</div>
	</div>
</section>

<!-- EMPREENDIMENTOS HOME -->

<div id="ancora2"></div>

<section id="empreendimentos-home" style="margin-top:0px;">

	<div id="ancora3"></div>

	<!-- SUB MENU -->
	<section class="submenu-home relativo">
		<div class="row">
			<div class="left clickroll">
				<ul>
					<li class="li-todos active"><a href="javascript:void(0);" id="todos" data-status="0"  onclick="selecionarAbaFiltro(this);">TODOS<span></span></a></li>
					<li class="li-lancamento"><a href="javascript:void(0);" id="lancamento" data-status="1"  onclick="selecionarAbaFiltro(this);">Lançamentos<span></span></a></li>
					<li class="li-pronto nomobile"><a href="javascript:void(0);" id="pronto" data-status="2"  onclick="selecionarAbaFiltro(this);">pronto <br class="yesmobile">para morar<span></span></a></li>
					<!-- <li class="li-perto"><a href="#ancora3" id="perto" onclick="selecionarAbaFiltro(this);" rel="nofollow">perto de vocÊ<span></span></a></li> -->
				</ul>
			</div>
			<div class="right">
				<ul>
					<li class="li-card"><a href="javascript:void(0);" id="cards" onclick="selecionarAbaFiltro(this);"><i class="icon icon-th-large "></i></a></li>
					<li class="li-lista active nomobile"><a href="javascript:void(0);" id="lista" onclick="selecionarAbaFiltro(this);"><i class="icon icon-list "></i></a></li>
					<!-- <li id="tab-location" class="li-mapa"><a href="javascript:void(0);" id="mapa" onclick="selecionarAbaFiltro(this);" rel="nofollow"><i class="icon icon-map-marker "></i></a></li> -->
				</ul>
			</div>
		</div>
	</section>

	<!-- FILTROS -->
	<section class="filtros nomobile">
		<div class="row">
			<div style="clear:both;"></div>
			<form>
				<div class="small-4">
					<select id="filtro-estado">
						<option value="-">Filtrar por Estado:</option>
						<?php //foreach($arrEstados as $id => $estado) { echo '<option value="'.$id.'">'.$estado.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-cidade" onchange="filtrarHomeCard(); filtrarHomeLista();">
						<option value="-">Filtrar por Cidade: Selecione um estado</option>	
						<?php //foreach($arrCidades as $id => $cidade) { echo '<option value="'.$id.'">'.$cidade.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-tipo" onchange="filtrarHomeCard(); filtrarHomeLista();">
						<option value="-">Filtrar por Tipo:</option>
						<?php //foreach($arrTipos as $id => $tipo) { echo '<option value="'.$id.'">'.$tipo.'</option>'; } ?>
					</select>
				</div>
			</form> 
		</div>
	</section>

	<!-- MAPA -->
	<!-- <section id="tab-mapa" class="cards" style="display:none;">
		<div class="mapa" id="map_canvas"></div>
	</section> -->

	<!-- MENSAGEM SEM RESULTADOS -->
	<section id="none_search" class="none_search">
		<div class="row">
			<p>Nenhum imóvel foi encontrado. Tente novamente.</p>
		</div>
	</section>

	<!-- CARDS -->
	<section id="tab-cards" class="cards" style="display:none;">
		<div class="tab-todos">
			<div class="row">
				<div id="div-imoveisDestaque" class="list_carousel">
				</div>
			</div>
		</div>
	</section>  
	<!-- CARDS ENDS -->

	<!-- LISTA -->
	<section id="tab-lista" class="lista" style="display:;">
		<div class="tab-todos">
			<div class="row">
				<div id="div-imoveisLista"></div>
			</div>
		</div>
	</section>  
	<!-- LISTA ENDS -->

</section><!-- EMPREENDIMENTOS HOME END -->

<!-- MAIS VISTOS HOME -->
<?php $this->load->view("includes/imoveis_visitados"); ?>
<?php $this->load->view("includes/footer"); ?>