<?php
	$link_corretor = CORETOR_DEFAULT;
	
	if(isset($imovel->link_corretor) && !empty($imovel->link_corretor))
	{
		
		$ses_url = @$_SESSION['url'];
		$link_corretor = $imovel->link_corretor;
		
	}
	
	if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
			
			if(isset($_GET['utm_campaign'])){
				$ref[] = $_GET['utm_campaign'];
			}
			
			if(isset($_GET['utm_source'])){
				$ref[] = $_GET['utm_source'];
			}
			
			if(isset($_GET['utm_medium'])){
				$ref[] = $_GET['utm_medium'];
			}
			
			$ref = implode('-', $ref);
			
		} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
			
			$ref = $_SERVER['HTTP_REFERER'];
			
			
		} else {
			$ref = 'direto';
		}

	$this->load->view("includes/header", array('link_corretor' => $link_corretor)); 
?>
	<style type="text/css">

	#backpanel {
		background: -moz-linear-gradient(top,  rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.7) 1%, rgba(0,0,0,0.7) 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.7)), color-stop(1%,rgba(0,0,0,0.7)), color-stop(100%,rgba(0,0,0,0.7)));
		background: -webkit-linear-gradient(top,  rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 1%,rgba(0,0,0,0.7) 100%);
		background: -o-linear-gradient(top,  rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 1%,rgba(0,0,0,0.7) 100%);
		background: -ms-linear-gradient(top,  rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 1%,rgba(0,0,0,0.7) 100%);
		background: linear-gradient(to bottom,  rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 1%,rgba(0,0,0,0.7) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cc000000', endColorstr='#cc000000',GradientType=0 );

		display: none;
		width: 100%;
		height: 100%;
		position: fixed;
		top: 0;
		z-index: 1000;
	}

	#backpanel .loadpanel {
		background-color: #fff;
		
		display: none;
		width: 674px;
		height: auto;
		margin: 6% auto 0 auto;
		overflow: hidden;

		-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;
	}

	#backpanel .loadpanel > iframe {
		border: 0;
		overflow: hidden;
	}

	#backpanel .loadpanel > span {
	    background-color: #a11;
	    color: #fff;
	    font-weight: bold;
	    font-size: 2.5em;
	    padding: 1px 15px;
	    border-radius: 00;
	    float: right;
	    cursor: default;
	}

	#backpanel .loadpanel > span:hover {
		background-color: #c11;
	}

	</style>

	<!--[if gte IE 9]>
		<style type="text/css">
		#backpanel {
		   filter: none;
		}
		</style>
	<![endif]-->


	<!--<div id="backpanel">
		<div class="loadpanel">
			<span title="Fechar Atendimento">x</span>
			<iframe name="interno" width="100%" height="497" src="<?=$link_corretor?>&midia=<?php echo $ref; ?>"></iframe>
		</div>
	</div>-->

	<script type="text/javascript">

	/*$(document).ready(function(){
		$("#backpanel").delay(6000).fadeIn(800, function(){
			$("#backpanel .loadpanel").fadeIn(1000);
		})
	});

	$("span", "#backpanel").click(function(){
		$("#backpanel").hide();
	})*/

	</script>

<?php
	$video = $this->model->getFotos($imovel->id_empreendimento, 13);
	$tour  = $this->model->getFotos($imovel->id_empreendimento, 12);

	$this->load->view("includes/header-html-new");  
	$this->load->view("includes/modal_telefone"); 
	$this->load->view("includes/modal_contato");  
	$this->load->view("includes/modal_interesse");  

	if(isset($fases) && count($fases) > 0)
	{ 
		$temObraParaMostrar = false;

		foreach($fases as $row)
		{
			$tem_itens = $this->model->getItensFases($fases[0]->id_fase);
			$tem_fotos = $this->model->getFotosObra(@$fases[0]->id_fase, $imovel->id_empreendimento);

			if($tem_itens || $tem_fotos)
			{
				$temObraParaMostrar = true;
				break;
			}
		}
	}

	if(count(@$video) > 0 || count(@$tour) > 0) 
		$this->load->view("includes/modal_video", true); 

	$fotos 		 = $this->model->getFotos($imovel->id_empreendimento, 10); 
	$decorado 	 = $this->model->getFotos($imovel->id_empreendimento, 15);
	$perspectiva = $this->model->getFotos($imovel->id_empreendimento, 9);

	$plantas 	 = false; 
	$implantacao = false;

	foreach($empreendimentosFotos as $row)
	{
		if($row->id_midia == 8)
			$plantas = true;
		elseif($row->id_midia == 11)
			$implantacao = true;
	}
?>

		<!-- DESTAQUE EMPREENDIMENTOS -->
		<?php if(in_array($imovel->id_empreendimento, array('71','90','83','5','61'))) { ?>
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora" style="position:absolute;top:90px;width:100px;z-index:999;left:50%;margin-left:-50px;">
		<?php } ?>
		<section id="destaque-emps" class="">
          	<div class="row">
          		<div class="empress">
          			<div class="left">
          				<img src="<?=UPLOADS_PATH?>imoveis/logo/<?=@$imovel->logotipo?>" alt="" class="nomobile" />
          			</div>
          			<div class="right">
          				<?php
          				if($imovel->status_label != "")
          				{
          					echo '<span>'.$imovel->status_label.'</span><br>'; 
          				}
          				else
          				{
          					if($imovel->id_status == 1)
          						echo '<span>Lançamento</span><br>';
          					elseif($imovel->id_status == 2)
          						echo '<span>Pronto para morar</span><br>';
          					elseif($imovel->id_status == 6)
          						echo '<span>100% Vendido</span><br>';
							elseif($imovel->id_status == 7)
          						echo '<span>Em Obras</span><br>';
          				}
          				?>
          				<h2 class="yesmobile"><?=$imovel->empreendimento?></h2>
          				<p class="sizematt"><i><?=$imovel->chamada_empreendimento; ?></i></p><br>
          				
          				<?php if(count(@$video) > 0) { ?>
          					<?php if(@$video[0]->tipo_arquivo == 'L') : ?>
          					<a href="<?=$video[0]->arquivo?>" class="btn btn-white button" target="_blank"><i class="icon icon-play"></i><p>ASSISTIR AO VÍDEO</p></a>
          					<?php else : ?>
          					<a href="javascript:void(0);" class="btn btn-white button click_video" data-id="<?=@$video[0]->arquivo?>" data-titulo="<?=@$video[0]->legenda?>"><i class="icon icon-play"></i><p>ASSISTIR AO VÍDEO</p></a>
          					<?php endif; ?>
          				<?php } ?>

          				<?php if(count(@$tour) > 0){ ?>
          					<?php if(@$tour[0]->tipo_arquivo == 'L') : ?>
          					<a href="<?=$tour[0]->arquivo?>" class="btn btn-white button" target="_blank"><i class="icon icon-refresh"></i><p>TOUR VIRTUAL</p></a>
          					<?php else : ?>
          					<a href="javascript:void(0);" class="btn btn-white button click_video" data-id="<?=@$tour[0]->arquivo?>" data-titulo="<?=@$tour[0]->legenda?>"><i class="icon icon-refresh"></i><p>TOUR VIRTUAL</p></a>
          					<?php endif; ?>
          				<?php } ?>
          			</div>
          		</div>
          	</div>

            <div class="list_carousel">
              <ul id="foo">
              	<?php if(count($destaquesSuperior) > 0){ ?>
	              	<?php foreach($destaquesSuperior as $row){ ?>
		                <li class="box scales paral bloco" style="background:url(<?=UPLOADS_PATH?>imoveis/midias/<?=$row->arquivo?>)"><!-- CARD -->
		                  <div class="pix"></div>
		                  <div class="black"></div>
		                </li>
	                <?php }?>
                <?php }else if(count($fotos) > 0){ ?>
                	<?php $i=0; ?>
                	<?php foreach($fotos as $row){ ?>
		                <li class="box scales paral bloco" style="background:url(<?=UPLOADS_PATH?>imoveis/midias/<?=@$row->arquivo?>)"><!-- CARD -->
		                  <div class="pix"></div>
		                  <div class="black"></div>
		                </li>
		                <?php if($i++ == 3) break; ?>
	                <?php } ?>
                <?php }else{ ?>
	                <li class="box scales paral bloco" style="background:url(<?=base_url()?>assets/site/img/destaque-03.jpg)"><!-- CARD -->
	                  <div class="pix"></div>
	                  <div class="black"></div>
	                </li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
            </div>

           </section>



        <!-- EMPREENDIMENTOS INTERNA -->
        <section id="empreendimentos-interna">
          <div id="ancora-topo" style="float: left; width: 98%; margin-top: -80px;"></div>
          
          
          <!-- SUB MENU -->
          <div id="ancora-menu-fixed"></div>
          <section class="submenu-home relativo">
            <div class="row">
              <div class="left clickroll">
                <ul>
                  <li class="active name">
                  	<a href="#ancora-topo" id="btn-topo" onclick="selecioneMenuInterna(this);">
                  		<i id="btn-favorito1" class="icon icon-heart" <?php echo($favoritado) ? 'style="color:#b72025;"' : ''; ?> onclick="salvarImovelFavoritos(this, <?php echo @$imovel->id_empreendimento; ?>)" style="margin-right:10px;"></i>
                  		<img src="<?=base_url()?>uploads/imoveis/fachada/visitados/<?=@$imovel->imagem_visitado?>" />
                  	</a>
                  </li>

                  <?php if(count($fotos) > 0 || count($decorado) > 0 || count($perspectiva) > 0){ ?>
                  	<li class="nomobile"><a href="#ancora-galeria" id="btn-galeria"  onclick="selecioneMenuInterna(this);">galeria<span></span></a></li>
                  <?php } ?>

                  <?php if($plantas || $implantacao){?>
                  	<li class="nomobile"><a href="#ancora-plantas" id="btn-plantas" onclick="selecioneMenuInterna(this);">Plantas<span></span></a></li>
                  <?php } ?>
                  
                  <li class="nomobile"><a href="#ancora-localizacao" id="btn-localizacao" onclick="selecioneMenuInterna(this);">localização<span></span></a></li>
                  
                  <?php if(intval($imovel->porcentagem_obras) > 0) { ?>
                  	<li class="nomobile"><a href="#ancora-obra" id="btn-obra" onclick="selecioneMenuInterna(this);">estágio da obra<span></span></a></li>
                  <?php } ?>
                </ul>
              </div>

              <div class="right">
                <a class="btn small btn-red button right click_interesse">Tenho Interesse</a>
              </div>

            </div>
          </section>

          <!-- BREADCAMPS  -->
          <section class="breadcamps">
            <div class="row ">
              <div class="bread columns">
                  <a href="" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
                  <ul class="breadcrumbs">
                    <li><a href="<?=base_url()?>">Home</a></li>
                    <li><i class="icon icon-angle-right"></i></li>
                    <?php if($imovel->id_status == 1){ ?>
                    	<li><a href="#">Lançamentos</a></li>
                    	<li><i class="icon icon-angle-right"></i></li>
                    <?php } ?>
                    <li class="unavailable"><a href="#"><?=@$imovel->empreendimento?></a></li>
                  </ul>
              </div>
            </div>
          </section>


          <!-- APRESENTAÇÃO  -->
          <div id="ancora-apresentacao" style="float: left; width: 98%; margin-top: -120px;"></div>
          <section class="apresentacao">
            <div class="row">
              <div class="leftmatt">
              	<img src="<?=UPLOADS_PATH?>imoveis/fachada/visitados/<?=@$imovel->imagem_visitado?>" class="yesmobile" />
			  </div>
              <div class="rightmatt">
	              <div class="icones"><i class="icon icon-map-marker"></i>
	              	<p>
	              		<?=@$imovel->cidade?>-<?=@$imovel->uf?>
	              		<span><?php if($imovel->bairro) echo '<br/>'.$imovel->bairro; ?></span>
	              	</p>
	            	</div>
	              <div class="icones"><div class="icones-img two"></div><p><?=@$imovel->metragem?></p></div>
	              <div class="icones"><div class="icones-img one"></div><p><?=@$imovel->dormitorio?></p></div>

	              <?php if($imovel->vagas) : ?>
	              <div class="icones"><div class="icones-img tree"></div><p><?=$imovel->vagas?></p></div>
	              <?php endif; ?>
			  </div>
              <i class="text-emp">
                <?php 
					if(@$itens)
					{
						$i = 0;
						$ultimo = sizeof($itens);

					    foreach($itens as $row)
					    { 
							echo ($i++ > 0) ? ' &bull; ' : '';
							echo $row->item;
						}
					}
					
					if($imovel->id_empreendimento == 86)
					{
						echo '
							Piscina adulto e infantil &bull;
							Playground &bull;
							Salão de festas &bull;
							Quiosque com churrasqueira &bull;
							Quadra poliesportiva &bull;
							Cancha de bocha
						';
					}
					elseif($imovel->id_empreendimento == 87)
					{
						echo '
							Wi-fi zone  &bull;
							For Kids &bull;
							Salão de Festas &bull;
							Pergolado &bull;
							2 quiosques com churrasqueiras &bull;
							Redário &bull;
							Lavanderia &bull;
							Espaço Gourmet &bull;
							Bicicletário &bull;
							Baby Place &bull;
							Fitness &bull;
							Espaço Pet &bull;
							Espaço Chimas &bull;
							2 quiosques com churrasqueira e forno de pizza
						';
					}
				?>
              </i>

              <div class="compar">
                <div class="left">

                  <!-- <p><i><?php //echo $imovel->qtd_favorito . 'pessoas favoritaram este empreendimento'; ?></i></p> -->

                  <a id="btn-favorito2" href="javascript:void(0);" <?php echo($favoritado) ? 'style="color:#ffffff; background-color:#b72025;"' : ''; ?> onclick="salvarImovelFavoritos(this, <?php echo @$imovel->id_empreendimento; ?>)" class="btn tiny btn-red button"><i class="icon icon-heart"></i> <?php echo($favoritado) ? 'favorito!' : 'favoritar'; ?></a>

                  <ul >
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_native_toolbox"></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-513de0b25d7bcf95"></script>

                  </ul>

                </div>
              </div>
            </div>
          </section>

          
		<!-- BOX CORRETORES  -->
		<div class="box full">
			<div class="row">
				<img src="<?=base_url()?>assets/site/img/contato-comprar.png" alt="" class="left">
				<div class="right-full">
					<h2>Fale com a EQUIPE NEX vendas</h2>
					<i>Uma equipe exclusiva para garantir o melhor atendimento e os melhores negócios para você.</i>
					<a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?php echo $ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" class="btn small btn-red button nomargin-left">Converse agora</a>
					<a class="btn small btn-red button click_interesse">Solicite contato</a>
				</div>
			</div>
		</div>
          

          <!-- GALERIA  -->
          <?php if(count($fotos) > 0 || count($decorado) > 0 || count($perspectiva) > 0){ ?>
	          <div id="ancora-galeria" style="float: left; width: 98%; margin-top: -200px;"></div>
	          <section id="galeria">
	
	            <div class="row">
	              <div class="galeria-title">
	                <h2>Galeria</h2>
	                 <h3><i>Veja fotos e ilustrações sobre o imóvel.</i></h3> 
	              </div>
	            </div>

	            <?php 
	            	$arr_imagens  = array();

	            	if(count($perspectiva) > 0)
					$arr_imagens  = array_merge($arr_imagens, (array) $perspectiva);

					if(count($fotos) > 0)
					$arr_imagens  = array_merge($arr_imagens, (array) $fotos);

					if(count($decorado) > 0)
					$arr_imagens  = array_merge($arr_imagens, (array) $decorado);

	            	// $arr_imagens  = $fotos + $perspectiva + $decorado;
	            	$first_imagem = '';
	            ?>
	
	            <div class="list_carousel ">
	              <ul id="foo4">
	              	<?php if($arr_imagens) : $i =0; ?>
	            	<?php foreach($arr_imagens as $row) : $i++; ?>
		                <li class="box scales paral image-content-<?=$i?>" style="background:url(<?=UPLOADS_PATH?>imoveis/midias/<?=@$row->arquivo?>)"><!-- CARD -->
		                  <div class="box2">
		                    <div class="left">
		                      <h3><i><?=@$row->legenda?></i></h3>
		                      <a href="javascript:;" class="btn btn-white button" title="Ver Galeria" onclick="showGallery();"><i class="icon icon-fullscreen "></i><p>Ver Galeria</p></a>
		                    </div>
		                  </div>
		                  <div class="pix"></div>
		                  <div class="black"></div>
		                </li>
	                <?php endforeach; ?>
	            	<?php endif; ?>
	              </ul>

	              <?php if($arr_imagens) : $i =0;?>
	              <?php foreach($arr_imagens as $row) : $i++; ?>
	              <ul id="gallery" style="display: none">
	              	<li class="image-content-<?=$i?>">
	              		<a href="<?=UPLOADS_PATH?>imoveis/midias/<?=@$row->arquivo?>" class="fancybox-thumb btn btn-white button" rel="fancybox-thumb" title="<?=@$row->legenda?>" style="display: none;">
	              			<i class="icon icon-fullscreen "></i><p>Ver Galeria</p>
	              		</a>
              		</li>
	              </ul>
	              <?php endforeach; ?>
            	  <?php endif; ?>

	              <div class="clearfix"></div>
	              <div class="navegation" style="display:none">
	                  <a id="prev4" class="prev" href="#"></a>
	                  <div id="pager4" class="pager"></div>
	                  <a id="next4" class="next" href="#"></a>
	              </div>
	            </div>
	          </section>
          <?php } ?>


	<?php if($plantas || $implantacao) { // PLANTAS: ?>
  	<div id="ancora-plantas" style="float: left; width: 98%; margin-top: -120px;"></div>
	<section id="plantas">
		<div class="row">
			<div class="plantas-title">
				<h2>PLANTAS</h2>
				<h3><i> Plantas ilustrativas com sugestão de decoração.</i></h3> 
				<ul class="right nomobile auto-margin">
					<?php if($plantas && $implantacao) { ?>
					<li class="active"><a id="midia-1" href="javascript:void(0);" onclick="selecionarAbaPlantas(this);">Plantas</a></li>
					<li class="last"><a id="midia-2" href="javascript:void(0);" onclick="selecionarAbaPlantas(this);">Implantação</a></li>
					<?php } ?>
				</ul>
			</div>
			<div style="clear:both;"></div>
			<div class="list_carousel">
				<?php
				$i = 1;
				foreach($empreendimentosFotos as $row)
				{
					if($row->id_midia == 8 || $row->id_midia == 11)
					{
						$midias = $this->model->getFotos($imovel->id_empreendimento, $row->id_midia); 
						?>
						<div id="tab-midia-<?=$i?>" <?=($i>1) ? 'style="display:none;"' : ''?>>
							<ul id="foo5<?=$i?>">
								<?php
								foreach($midias as $midia)
								{
									echo '<li class="box">';

									if($midia->id_tipo_midia == 12)
									{
										?>
										<a target="_blank" href="<?=@$midia->arquivo?>" class="fancybox-thumb nomobile" rel="fancybox-thumb2" title="<?=@$midia->legenda?>">
											<img src="<?=UPLOADS_PATH?>imoveis/midias/thumbs/<?=@$midia->thumb?>">
										</a>
										<div class="yesmobile">
											<img src="<?=UPLOADS_PATH?>imoveis/midias/thumbs/<?=@$midia->thumb?>">
										</div>
										<a target="_blank" href="<?=@$midia->arquivo?>" class="yesmobile fancybox-thumb" rel="fancybox-thumb2" title="<?=@$midia->legenda?>"><p><?=@$midia->legenda?></p></a>
										<?php
									}
									elseif($midia->id_tipo_midia == 13)
									{
										?>
										<a href="http://www.youtube.com/watch?v=<?=@$row->arquivo?>" class="fancybox-thumb nomobile" rel="fancybox-thumb2" title="<?=@$midia->legenda?>">
											<img src="http://img.youtube.com/vi/<?=@$midia->arquivo?>/1.jpg" />
										</a>
										<div class="yesmobile">
											<img src="http://img.youtube.com/vi/<?=@$midia->arquivo?>/1.jpg" />
										</div>
										<a href="http://www.youtube.com/watch?v=<?=@$row->arquivo?>" class="yesmobile fancybox-thumb" rel="fancybox-thumb2" title="<?=@$midia->legenda?>"><p><?=@$midia->legenda?></p></a>
										<?php
									}
									else
									{
										?>
										<a href="<?=UPLOADS_PATH?>imoveis/midias/<?=@$midia->arquivo?>" class="fancybox-thumb nomobile" rel="fancybox-thumb2" title="<?=@$midia->legenda?>">
											<img src="<?=UPLOADS_PATH?>imoveis/midias/<?=@$midia->arquivo?>">
										</a>
										<div class="yesmobile">
											<img src="<?=UPLOADS_PATH?>imoveis/midias/<?=@$midia->arquivo?>">
										</div>
										<a href="<?=UPLOADS_PATH?>imoveis/midias/<?=@$midia->arquivo?>" class="yesmobile fancybox-thumb" rel="fancybox-thumb2" title="<?=@$midia->legenda?>"><p><?=@$midia->legenda?></p></a>
										<?php
									}

									echo '<p class="nomobile">' . @$midia->legenda . '</p>';
									echo '</li>';
								}
								?>
							</ul>
							<div class="clearfix"></div>
							<div class="navegation">
								<a id="prev5<?=$i?>" class="prev nomobile" href="#"></a>
								<div id="pager5<?=$i?>" class="pager auto-margin-bolinhas"></div>
								<a id="next5<?=$i?>" class="next nomobile" href="#"></a>
							</div>                      
						</div>
						<?php
						$i++;
					}
				}
				?>
			</div>
		</div>
	</section>
	<?php } // PLANTAS; ?>


          <?php if(!empty($imovel->endereco_central_vendas) || !empty($imovel->telefone_central_vendas)) { ?>
	          <!-- PLANTÃO DE VENDAS -->
	          <section id="plantao">
	            <div class="row">
	
	              <div class="one">
	                  <i class="icon icon-home"></i>
	                  <h5>
						<?php if($imovel->id_empreendimento != 36 && $imovel->id_empreendimento != 68){ ?>
							Plantão de 
						<?php } ?>
						vendas</h5>
	              </div> 
	
	              <div class="two">
	               	<i>
	              	 <?php
	              	 if($imovel->endereco_central_vendas)
	              	 {
	              	 	echo @$imovel->endereco_central_vendas;
	              	 }
	              	 ?>
	                 
	                 <?php if(!empty($imovel->horario_atendimento)){ ?>
	                 	<br>Horário de Atendimento: <?=$imovel->horario_atendimento?>
	                 <?php } ?></i>
	              </div> 
	

	              <div class="tree">
	              <?php if(!empty($imovel->telefone_central_vendas)){ ?>
	                  <i class="icon icon-phone"></i>
	                  <h5><?=$imovel->telefone_central_vendas?></h5>
                  <?php } ?></i>
	              </div> 
	
	            </div>
	          </section>
          <?php } ?>


          <!-- LOCALIZAÇÃO  -->
          <div id="ancora-localizacao" style="float: left; width: 98%; margin-top: -120px;"></div>
          <section id="localizacao">
            <div class ="black"></div>
            <div id="endereco" class="row">
              <div class="localizacao-title">
                <h2>LOCALIZAÇÃO</h2>
                <?php if(@$imovel->endereco_empreendimento){?>
					<h3><i><?=$imovel->endereco_empreendimento?></i></h3>
				<?php }?>
                <ul id="tipo-mapa" class="right nomobile auto-margin">
                   <li id="mapa-rua" class="active"><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['ROADMAP']); selecionaTipoMapa('rua');">RUAS</a></li> 
	               <li id="mapa-satelite"><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['SATELLITE']); selecionaTipoMapa('satelite');">SATÉLITE</a></li> 
                </ul> 
              </div>
            </div>


            <!-- MAPA --> 
          <!--   <ul>
            	<li><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['SATELLITE']);">SATELITE</a></li>
            	<li><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['ROADMAP']);">RUAS</a></li>
            </ul>--> 
            <div class="mapa" id="map_canvas" lat="<?php echo $imovel->latitude; ?>" lon="<?php echo $imovel->longitude; ?>" marker="<?=UPLOADS_PATH?>imoveis/pin/<?php echo $imovel->pin; ?>"></div>

            <div class="tools">
              <ul class="right nomobile" id="ul-marker">
              	<h5>Selecione para ver no mapa:</h5>

              	<li><a href="javascript:void(0);" data-tipo="hospital" data-icon="<?=base_url()?>assets/site/img/pins-novos/1-hospital.png" onclick="ativarPin(this)">HOSPITAL</a></li>
              	<li><a href="javascript:void(0);" data-tipo="pharmacy" data-icon="<?=base_url()?>assets/site/img/pins-novos/2-farmacias.png" onclick="ativarPin(this)">FARMÁCIAS</a></li>
              	<li><a href="javascript:void(0);" data-tipo="bank" data-icon="<?=base_url()?>assets/site/img/pins-novos/3-bancos.png" onclick="ativarPin(this)">BANCOS</a></li>
              	<li><a href="javascript:void(0);" data-tipo="gym" data-icon="<?=base_url()?>assets/site/img/pins-novos/4-academia.png" onclick="ativarPin(this)">ACADEMIA</a></li>
              	<li><a href="javascript:void(0);" data-tipo="school,university" data-icon="<?=base_url()?>assets/site/img/pins-novos/5-ensino.png" onclick="ativarPin(this)">ENSINO</a></li>
              	<li><a href="javascript:void(0);" data-tipo="shopping_mall" data-icon="<?=base_url()?>assets/site/img/pins-novos/6-shopping.png" onclick="ativarPin(this)">SHOPPING</a></li>
              	<li><a href="javascript:void(0);" data-tipo="grocery_or_supermarket" data-icon="<?=base_url()?>assets/site/img/pins-novos/7-super-mercado.png" onclick="ativarPin(this)">SUPERMERCADO</a></li>
              	<li><a href="javascript:void(0);" data-tipo="cafe" data-icon="<?=base_url()?>assets/site/img/pins-novos/8-padaria.png" onclick="ativarPin(this)">PADARIA</a></li>
              </ul>

              <!-- https://www.google.com/maps/dir/<?php echo $imovel->latitude; ?>,<?php echo $imovel->longitude; ?>/-30.0420173,-51.1840569/@-29.9785348,-51.2884514 -->

              <div class="row">
                <div class="left">
                  <span>Como Chegar:</span>
                  <input type="hidden" name="latitude" id="latitude" value="<?php echo $imovel->latitude; ?>" />
                  <input type="hidden" name="longitude" id="longitude" value="<?php echo $imovel->longitude; ?>" />
                  <input type="text" name="localizacao-texto" id="localizacao-texto" placeholder="Digite aqui seu CEP ou endereço">
                  <a href="javascript:void(0);" onclick="irParaLocalizacao($('#localizacao-texto').val());" class="btn btn-grey button"><p>OK</p></a>
                  <!-- <a href="javascript:void(0);" onclick="pegarGeoLocalizacao();" class="btn btn-black button yesmobile"><i class="icon icon-map-marker "></i><p>TRAÇAR ROTA NO MAPA</p></a> -->
                  <a href="geo:<?=$imovel->latitude . ',' . $imovel->longitude?>" id="link-geo-route" class="btn btn-black button yesmobile"><i class="icon icon-map-marker "></i><p>usar minha localização</p></a>
                </div>

              </div>
            </div>
          </section>
          
  	<?php if(intval($imovel->porcentagem_obras) > 0) : ?>
	<!-- ESTAGIO DA OBRA: -->
	<div id="ancora-obra" style="float: left; width: 98%; margin-top: -120px;"></div>
	<section id="obras">
		<div class="row">
			<div class="obras-title"><h2>ESTÁGIO DA OBRA</h2></div>
			<div id="tab-fase">
				<div class="percent">

					<?php
					if($imovel->data_atualizacao_itens_obras)
					{
						$date_updates_obra = date('m/Y', strtotime($imovel->data_atualizacao_itens_obras));
						$ex_date_updates_obra = explode('/', $date_updates_obra);

						switch ($ex_date_updates_obra[0])
						{
							case '01': $ex_date_updates_obra[0] = 'janeiro'; break;
							case '02': $ex_date_updates_obra[0] = 'fevereiro'; break;
							case '03': $ex_date_updates_obra[0] = 'março'; break;
							case '04': $ex_date_updates_obra[0] = 'abril'; break;
							case '05': $ex_date_updates_obra[0] = 'maio'; break;
							case '06': $ex_date_updates_obra[0] = 'junho'; break;
							case '07': $ex_date_updates_obra[0] = 'julho'; break;
							case '08': $ex_date_updates_obra[0] = 'agosto'; break;
							case '09': $ex_date_updates_obra[0] = 'setembro'; break;
							case '10': $ex_date_updates_obra[0] = 'outubro'; break;
							case '11': $ex_date_updates_obra[0] = 'novembro'; break;
							case '12': $ex_date_updates_obra[0] = 'dezembro'; break;
						}

						echo '<p style="margin-bottom: 2%; font-size: 1.8em;">Atualização das Obras: '. $ex_date_updates_obra[0] .'/'. $ex_date_updates_obra[1] .'</p>';
					}
					?>

					<br style="clear: both;" />

					<!-- PORCENT BAR : -->
					<div style="margin-bottom:4%; float:left; width:100%">
						<h4>geral</h4>
						<div class="progress" style="height: 4em;">
							<span class="meter" style="width:<?=$imovel->porcentagem_obras?>%"></span>
						</div>
						<h5><?=$imovel->porcentagem_obras?>%</h5>
					</div>
					<!-- PORCENT BAR; -->

					<?php if(count($fases) > 0) : $i = 0; foreach($fases as $fase) : ?>
					<br style="clear: both;" />

					<!-- PORCENT BAR : -->
					<div style="width: 96%; float: left; padding: 2.5% 0 2.5% 4%;border-top: 1px solid #e2e2e2;">
						<h4 style="font-size: 1.6em; font-weight: 600;"><?=$fase->titulo?></h4>
						<div class="progress" style="height: 23px;">
							<span class="meter" style="width:<?=$fase->porcentagem_fase?>%"></span>
						</div>
						<h5 style="font-size: 2.8em;"><?=$fase->porcentagem_fase?>%</h5>
					</div>
					<!-- PORCENT BAR; -->
					<?php $i++; endforeach; endif; ?>
										
					<br style="clear: both;" />

				</div>
				<?php
				$fotosObra = $this->model->getFotosObrasByEmpreendimentoID($imovel->id_empreendimento);
				if(count($fotosObra) > 0) :
				?>
				<div class="list_carousel">
					<ul id="foo61">
						<?php foreach($fotosObra as $foto) : ?>
						<li class="box scales" style="background:url(<?=UPLOADS_PATH?>imoveis/fases/<?=@$foto->imagem?>)">
							<a href="<?=UPLOADS_PATH?>imoveis/fases/<?=@$foto->imagem?>" class="fancybox-thumb" rel="fancybox-thumb3" title="<?=@$foto->legenda?>"></a>
						</li>
						<?php endforeach; ?>
					</ul>
					<div class="clearfix"></div>
					<div class="navegation">
						<span id="prev61" class="prev nomobile"></span>
						<div id="pager61>" class="pager auto-margin-bolinhas"></div>
						<span id="next61" class="next nomobile"></span>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<!-- ESTAGIO DA OBRA; -->
	<?php endif; ?>

<script type="text/javascript">
	function showGallery()
    {
    	$('ul#gallery > li.image-content-1 a.fancybox-thumb').eq(0).trigger('click');
    }
 
 	$(document).ready(function(){
 		if(navigator.geolocation)
 			console.log(navigator.geolocation.getCurrentPosition(setLinkGeo, erroLinkGeo));
 	})

 	function setLinkGeo(position) {
		var lat = $('#latitude').val();
		var lon = $('#longitude').val();
		var de = position.coords.latitude+','+position.coords.longitude;
		var para = lat+','+lon;
		
		$('#link-geo-route').attr('href', 'https://www.google.com/maps/dir/'+de+'/'+para+'/');

		// console.log('LINK GEO');
		// console.log('https://www.google.com/maps/dir/'+de+'/'+para+'/');
		// window.open('https://www.google.com/maps/dir/'+de+'/'+para+'/');
	}

	function erroLinkGeo()
	{
		console.log('fail ao realizar geo!');
	} 

</script>


<?php $this->load->view("includes/imoveis_semelhantes", true); ?>
<?php // $this->load->view("includes/invite_chat",array('link_corretor'=>$link_corretor)); ?>
<?php $this->load->view("includes/footer",array('link_corretor' => $link_corretor)); ?>