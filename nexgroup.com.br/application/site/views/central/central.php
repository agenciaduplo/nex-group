<?php $this->load->view("includes/header"); ?>

					<section id="sales-center" class="Main Inner">
						<div class="Bg2">
							<div class="Wrapper">
								<div class="Box1">
									<h1>Central de Vendas</h1>
									<div class="Box2">
										<hgroup>
											<h2>Corretor <span>on-line</span></h2>
											<h3>fale com um de nossos corretores</h3>
										</hgroup>
										<p>&Eacute; f&aacute;cil esclarecer as suas d&uacute;vidas e obter todas as informa&ccedil;&otilde;es para fechar o melhor neg&oacute;cio com os im&oacute;veis que a <b>NEX GROUP</b> oferece para voc&ecirc;, basta consultar o <b>CORRETOR ON-LINE</b>.</p>
										<p class="Exception"><b>segunda &agrave; domingo das 9h &agrave;s 18h.</b></p>
										<a class="BtnRed CorretorOnline"  href="javascript:window.open('<?=CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online">Converse agora</a>
									</div>
									<!--<div class="Box2">
										<hgroup>
											<h2>Atendimento <span>por email</span></h2>
											<h3>descreva abaixo o im&oacute;vel que voc&ecirc; procura</h3>
										</hgroup>
										<form action="" method="post" id="frm-email-assistance">
											<fieldset>
												<label for="ipt-email-assistance-your-name">seu nome</label>
												<input class="Text" id="ipt-email-assistance-your-name" name="seu_nome" placeholder="Seu Nome"  type="text" value=""/>
												<label for="ipt-email-assistance-your-email">seu email</label>
												<input class="Text" id="ipt-email-assistance-your-email" name="seu_email" placeholder="Seu Email"  type="email" value=""/>
												<label for="ipt-email-assistance-description">Descreva o im&oacute;vel</label>
												<textarea  class="Text" id="ipt-email-assistance-decription" name="descricao" placeholder="Descreva o im&oacute;vel" ></textarea>
												<button class="BtnRed" type="submit">Enviar</button>
												<select id="estados"  required="required">
													<option>Estado</option>
													<?php foreach($estados as $estado):?>
													<option value="<?php echo $estado->id_estado?>"><?php echo $estado->estado?></option>
													<?php endforeach;?>
												</select>
												<select id="cidades" required="required">
													<option>Cidade</option>
												</select>
											</fieldset>
										</form>
									</div>-->
									<div class="Box3 Exception">
										<h2>Formul&aacute;rio de <span>atendimento</span></h2>
										<form action="" method="post" id="frm-assistance">
											<fieldset>
												<label for="ipt-assistance-your-name">seu nome*</label>
												<input class="Text" id="ipt-assistance-your-name" name="seu_nome" type="text" value=""/>
												<label for="ipt-assistance-your-email">seu email*</label>
												<input class="Text" id="ipt-assistance-your-email" name="seu_email" type="text" value=""/>
												<div>
													<label for="ipt-assistance-your-phone-area">seu telefone</label>
													<input class="Text" id="ipt-assistance-your-phone-area" name="seu_phone_area"   type="text" value=""/>
													<input class="Text" id="ipt-assistance-your-phone" name="seu_phone" type="text" value=""/>
												</div>
												<select name="empreendimento" id="empreendimento" >
													<option value="">Selecione o Imóvel</option>
													<?php foreach($empreendimentos as $empreendimento):?>
													<option value="<?php echo $empreendimento->id_empreendimento?>"><?php echo $empreendimento->empreendimento?></option>
													<?php endforeach;?>
												</select>
												<div class="Exception">
													<label class="">contate-me por</label>
													<input class="Radio" id="ipt-assistance-email-opt" checked="checked"  name="opt_contact"  type="radio" value=""/>
													<label class="Exception " for="ipt-assistance-email-opt">email</label>
													<input class="Radio" id="ipt-assistance-phone-opt" name="opt_contact"  type="radio" value=""/>
													<label class="Exception " for="ipt-assistance-phone-opt">telefone</label>
												</div>
												<div class="Clear"></div>
												<div>
													<label for="ipt-assistance-message">mensagem</label>
													<textarea class="Text"  id="ipt-assistance-message" name="mensagem" ></textarea>
													<span class="AlertFrm">Os campos com asterisco (*) s&atilde;o de preenchimento obrigat&oacute;rio.</span>
												</div>
												<button class="BtnRed" type="submit">Enviar</button>
											</fieldset>
										</form>
										<div id="sucess-assistance" class="Sucess">
											<hgroup>
												<h3>Mensagem enviada com sucesso!</h3>
												<h4>em breve nossa equipe entrar&aacute; em contato com voc&ecirc;!</h4>
											</hgroup>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="Wrapper">
							<?php $this->load->view("includes/imoveis_visitados"); ?>
						</div>
					</section>
					
<?php $this->load->view("includes/footer"); ?>