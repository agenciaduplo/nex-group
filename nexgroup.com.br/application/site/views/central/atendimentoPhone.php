<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput-1.3.min.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/source/validations.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.placeholder.js" type="text/javascript"></script>
	</head>
	<body>
		<hgroup>
			<h2>Ligamos para você</span></h2>
		</hgroup>
		<form action="" method="post" id="frm-phone-assistance">
			<fieldset>
				<label for="ipt-phone-assistance-your-name">seu nome*</label>
				<input class="Text" id="ipt-phone-assistance-your-name" name="seu_nome"  type="text" value=""/>
				<label for="ipt-phone-assistance-your-email">seu email</label>
				<input class="Text" id="ipt-phone-assistance-your-email" name="seu_email" type="text" value=""/>
				<div>
					<label for="ipt-phone-assistance-your-phone-area">seu telefone*</label>
					<input class="Text" id="ipt-phone-assistance-your-phone-area" name="seu_phone_area" type="text" value=""/>
					<input class="Text" id="ipt-phone-assistance-your-phone" name="seu_phone"   type="text" value=""/>
				</div>
				<select name="empreendimento" id="empreendimento" >
					<option value="">Selecione o Imóvel</option>
					<?php foreach($empreendimentos as $empreendimento):?>
						<option value="<?php echo $empreendimento->id_empreendimento?>" <?php if(@$id_empreendimento==$empreendimento->id_empreendimento) echo 'selected="selected"'?> ><?php echo $empreendimento->empreendimento?></option>
					<?php endforeach;?>
				</select>
				<div class="Exception">
					<label class="">contate-me por</label>
					<input class="Radio" id="ipt-phone-assistance-email-opt" name="opt_contact"  type="radio" value=""/>
					<label class="Exception " for="ipt-phone-assistance-email-opt">email</label>
					<input class="Radio" id="ipt-phone-assistance-phone-opt" checked="checked"  name="opt_contact"  type="radio" value=""/>
					<label class="Exception " for="ipt-phone-assistance-phone-opt">telefone</label>
				</div>
				<div class="Clear"></div>
				<div>
					<label for="ipt-phone-assistance-message">mensagem</label>
					<textarea class="Text"  id="ipt-phone-assistance-message" name="mensagem" ></textarea>
					<span class="AlertFrm">Os campos com asterisco (*) s&atilde;o de preenchimento obrigat&oacute;rio.</span>
				</div>
				<button class="BtnRed" type="submit">Enviar</button>
			</fieldset>
		</form>
		<div id="sucess-phone-assistance" class="Sucess">
			<hgroup>
				<h3>Mensagem enviada com sucesso!</h3>
				<h4>em breve nossa equipe entrar&aacute; em contato com voc&ecirc;!</h4>
			</hgroup>
			<a onclick="$.fancybox.close();" class="BtnGrey">Voltar</a>
		</div>

		<div id="conversaoAdwords">

		</div>

	</body>
</html>