<?php $this->load->view("includes/header"); ?>
					<section id="browse-by-the-map" class="Main Inner">
						<div class="Bg2">
							<div class="Wrapper">
								<div class="Box1">
									<hgroup>
										<h2>navegue <span>pelo mapa</span></h2>
										<h3>encontre os im&oacute;veis na sua regi&atilde;o</h3>
									</hgroup>
									
									<div id="mouse-alert">
                  	<img class="mouseLeft" src="<?=base_url()?>assets/site/img/layout/ico-mouse-1.png" width="26" />
                      <p class="mouseScroll">Use o scroll do mouse para dar zoom no mapa</p>
                  	
                  	<div id="line-mouse-alert"></div>
                  	
                  	<img class="mouseLeft" src="<?=base_url()?>assets/site/img/layout/ico-mouse-2.png" width="26" />
                      <p class="mousePin">Clique nos pins do mapa para ver detalhes do imóvel</p>
                	</div>
									
									<!--<img src="<?php echo base_url()?>assets/site/img/layout/logo_por-todos-os-lados.png" alt="Nex por todos os lados" style="float:right; margin-top:-93px"/>-->
									<!--
									<form  action="" method="post" id="frm-browse-by-the-map" >
										<fieldset>
											<div class="BoxSelctFilters">
												<select>
													<option>Estado</option>
												</select>
												<select>
													<option>Cidade</option>
												</select>
												<select>
													<option>Tipo de Im&oacute;vel</option>
												</select>
												<button class="BtnRed" type="submit">Buscar</button>
											</div>
										</fieldset>
									</form>
									-->
									<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
									<script src="<?php echo base_url()?>assets/site/js/source/maps.js" type="text/javascript" ></script>
									<div  onload="initialize()" id="map_canvas" class="BoxBrowseMap">
									</div>
									<div class="BoxSharing">
										<!--<a class="BtnGrey Modal" href="<?=site_url()?>indique">Indique para um amigo</a>-->
										<div class="addthis_toolbox addthis_default_style ">
												<g:plusone size="medium"></g:plusone>
												<a addthis:url="<?=site_url()?>navegue-pelo-mapa" addthis:title="<?=@$title?>" class="addthis_button_tweet"></a>
												<a addthis:url="<?=site_url()?>navegue-pelo-mapa" addthis:title="<?=@$title?>" class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
										</div>
									</div>
								</div>
								<div class="Box2">
									<h2>Encontre seu im&oacute;vel utilizando nossa ferramenta  <span>navegue pelo mapa</span></h2>
									<h3>Agora ficou muito f&aacute;cil realizar todas as suas pesquisas e buscas por im&oacute;veis.</h3>
									<p>Atrav&eacute;s da ferramenta de procura voc&ecirc; pode navegar livremente pelo mapa utilizando todos os recursos
									do Google Maps (setas direcionais, aproxima&ccedil;&atilde;o e afastamento da escala, vis&atilde;o de sat&eacute;lite e muito mais).</p>
									<p>Ap&oacute;s realizar a sua pesquisa por im&oacute;veis, neste mesmo ambiente &eacute; poss&iacute;vel entrar em contato com a nossa
									<b>CENTRAL DE VENDAS</b>, escolhendo falar com um <b>Corretor On Line</b>, utilizar o <b>Atendimento Por Email</b> ou
									cadastrar-se e n&oacute;s <b>Ligamos Pra Voc&ecirc;</b>.</p>
									<!--
									<p>Atrav&eacute;s da ferramenta de procura voc&ecirc; pode navegar livremente pelo mapa utilizando todos os recursos
									do Googlemaps (setas direcionais, aproxima&ccedil;&atilde;o e afastamento da escala, vis&atilde;o de sat&eacute;lite e muito mais).
									Al&eacute;m destas facilidades voc&ecirc; tem a sua disposi&ccedil;&atilde;o 05 categorias de filtros para auxiliar na procura:</p>
									<ul>
										<li>seletor de estado</li>
										<li>seletor de cidade</li>
										<li>seletor de n&uacute;mero de dormit&oacute;rios</li>
										<li>seletor de valor m&iacute;nimo do im&oacute;vel</li>
										<li class="Exception">seletor de valor m&aacute;ximo do im&oacute;vel</li>
									</ul>
									<p>Ap&oacute;s realizar a sua pesquisa por im&oacute;veis, neste mesmo ambiente &eacute; poss&iacute;vel entrar em contato com a nossa
									<b>CENTRAL DE VENDAS</b>, escolhendo falar com um <b>Corretor On Line</b>, utilizar o <b>Atendimento Por Email</b> ou
									cadastrar-se e n&oacute;s <b>Ligamos Pra Voc&ecirc;</b>.</p>-->
								</div>
								<aside>
									<div class="BoxCustomer">
										<h2>central de <span>vendas</span></h2>
										<div class="BoxCustomer1 BoxSalesCenter">
											<a class="CorretorOnline" href="javascript:window.open('http://web.atendimentoaovivo.com.br/chat.asp?idc=5447&amp;pre_empresa=1192&amp;pre_depto=1276&amp;vivocustom=&amp;vivocustom2=DHZ&amp;vivocustom3=Site+DHZ&amp;vivocustom4=Rodape&amp;idtemplate=0','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" title="Corretor on line">Corretor <span>on line</span></a>
										</div>
										<div class="BoxCustomer2 BoxSalesCenter">
											<a class="Atendimento" href="<?=site_url()?>atendimento-email" >Atendimento <span>por email</span></a>
										</div>
										<div class="BoxCustomer3 BoxSalesCenter">
											<a class="Atendimento" href="<?=site_url()?>ligamos-para-voce" >Ligamos <span>para voc&ecirc;</span></a>
										</div>
									</div>
								</aside>
							</div>
						</div>
						<div class="Wrapper">
							<?php $this->load->view("includes/imoveis_visitados"); ?>
						</div>
					</section>
<?php $this->load->view("includes/footer"); ?>