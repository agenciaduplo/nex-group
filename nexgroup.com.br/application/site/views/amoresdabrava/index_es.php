<?php $this->load->view("includes/header-emp"); ?>
<header class="main-header">
	<div class="container relative">
		<div class="left">
			<a href="http://www.nexgroup.com.br/" class="logo hide-tablet">
				<h1 class="title">Nex Group</h1>
			</a>
			<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/taroii-topo.png?ver=0.4" class="taroii hide-mobile">
		</div>
		<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-amoresdabrava.png?ver=0.4" class="logo-myurban hide-mobile">		
		<div class="right btns">
				<!--<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/header', 'click');">
					<img class="imgmax100 hide-mobile" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/btn-phone-es.png?ver=0.1">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-phone.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left rnormal">CLIQUE<br /><b class="rbold">LIGUE</b></span>
				</a>-->
				<a class="click_chat" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=16&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor', 'click');">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left">CHAT</span>
				</a>
		</div>
	</div>
</header>

<section class="banner"> 
	<div class="container">
		<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-amoresdabrava-mobile.png?ver=0.4" class="hide show-mobile logo-mobile">  
		<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/banner-content-es.png?ver=0.1" class="img-letter hide-mobile imgmax100">  
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
	</div>
</section>
<section class="preco yellowbg">
	<div class="container">
		<div class="spacer"></div>
		<h1 class="rlight whitecolor tcenter">Departamentos de <b>3 dormitorios.</b> con <b>1</b> a <b>3 baños en suite.</b></h1>
		<div class="spacer"></div>
	</div>
</section>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
			<div class="spacer hide-mobile"></div>
			<div class="spacer hide-mobile"></div>
			<div class="spacer hide-mobile"></div>
				<h1 class="knormal tcenter graycolor fs45">Viva en la playa y tenga<br class="hide-mobile" /> un club privado.</h1>
				<p class="rnormal pinkcolor tcenter">El Amores da Brava es para los apasionados por el mar, a quienes les encanta reunir los amigos y disfrutar excelentes momentos en familia. A una cuadra del mar, posee un Club House con más de 5.000 m² de área de entretenimiento para usted disfrutar todos los días.</p>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="imoveis">
	<div class="container">
		
		<div class="row  right no-float-mobile">
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-1.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_01', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-1.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-2.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_02', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-2.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-3.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_03', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-3.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-4.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_04', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-4.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-5.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_05', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-5.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-6.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_06', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-6.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-7.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_07', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-7.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-8.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_08', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-8.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-9.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_09', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-9.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-10.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_10', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-10.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-11.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_11', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-11.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-12.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_12', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-12.jpg" class="img-display">
				</a>
			</div>
			
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-13.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_13', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-13.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-14.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_14', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-14.jpg" class="img-display">
				</a>
			</div>
			
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-15.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_15', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-15.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-16.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_16', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-16.jpg" class="img-display">
				</a>
			</div>
		
			<div class="col-md-3"></div>
			
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-17_a.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_17', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-17.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-18_a.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_amoresdabrava_18', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/amoresdabrava-18.jpg" class="img-display">
				</a>
			</div>
			
			<div class="col-md-3"></div>
		</div>
		<div class="spacer"></div>
		<div class="spacer"></div>
	</div>
</section>

<div class="spacer hide-mobile"></div>

<section>
	<div class="container">
		<img class="img100 hide-mobile" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/beach-point-es.png" />
		<img class="hide show-mobile beach-point-icon" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/beach-point-icon1.png" />
		<div class="col-md-12 beach-point rnormal hide show-mobile">
			<p>El Amores da Brava tiene un encanto: el <span class="rsemi">Beach Point</span>. Un espacio en la playa exclusivo para residentes con toda la estructura que usted merece. Sólo tiene que elegir su lugar en la arena y aprovechar.</p>
		</div>
	</div>
</section>
<div class="spacer hide show-mobile"></div>
<div class="spacer hide show-mobile"></div>
<div class="spacer"></div>

<section class="preco yellowbg hide-mobile">
	<div class="container">
		<div class="spacer"></div>
	</div>
</section>

<section class="banner2 hide-mobile"> 
	<div class="container">
		
	</div>
</section>

<section class="preco yellowbg hide-mobile">
	<div class="container">
		<div class="spacer"></div>
	</div>
</section>

<section class=" ">
	<div class="container lightbluebg">
		<h1 class="knormal tcenter pinkcolor fs45">El Amores da Brava está ubicado en la dirección <br class="hide-mobile" /> más famosa del litoral norte de Santa Catarina:</h1>
		<p class="rnormal graycolor tcenter">En el encuentro de la encantadora Praia dos Amores con la buena onda<br class="hide-mobile" /> de la Praia Brava, entre Itajaí y Balneário Camboriú.</p>
	</div>
</section>

<section class="pinkbg">
	<div class="container">
		<div class="littlespacer"></div>
	</div>
</section>

<section class="mapa">
	<a href="#" data-toggle="modal" data-id="810" data-target="#810">
		<img class="img100" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/map.jpg?ver=1" />
	</a>
	<div class="modal fade in" id="810" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;">×</button>
					
				</div>
				<div class="modal-body">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3556.6683964870817!2d-48.632651384322735!3d-26.94572510107326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94d8c98312b759a3%3A0x7c903f261fe5c495!2sR.+Delfim+M%C3%A1rio+de+P%C3%A1dua+Peixoto%2C+600+-+Praia+Brava%2C+Itaja%C3%AD+-+SC%2C+88306-806!5e0!3m2!1spt-BR!2sbr!4v1460728289784" width="570" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pinkbg">
	<div class="container">
		<div class="littlespacer"></div>
	</div>
</section>

<section>
	<div class="container whitebg bordered">
		<h1 class="knormal tcenter pinkcolor fs45">Visite el Departamento Decorado.</h1>
		<div class="spacer"></div>
		<p class="rnormal graycolor tcenter">Abierto todos los días, incluso los fines de semana, de las<br class="hide-mobile" /> 9:00 a las 12:00 y de las 14:00 a las 18:00.</p>
		<div class="spacer"></div>
	</div>
</section>

<div class="spacer"></div>

<section class="form  show-mobile blugraybg">
	<div class="container">
		<form id="formContact" novalidate="novalidate">
			<div class="row">
				<div class="col-md-12">
					<h1 class="whitecolor tcenter bottomnegative">Conozca más</h1>
				</div>
			</div>
			<div class="spacer"></div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-4">
							<input name="nome" id="nome" type="text" class="field required themecolor" placeholder="Nombre">
						</div>
						<div class="col-md-3">
							<input name="email" id="email" type="text" class="field required themecolor" placeholder="E-mail">
						</div>
						<div class="col-md-3">
							<input name="telefone" id="telefone" type="text" class="field required themecolor telefone" placeholder="Teléfono">
						</div>
						<div class="col-md-2">		
							<input type="hidden" id="is_mobile" name="is_mobile" value="yes" class="hide show-mobile">
							<input type="hidden" name="ref" value="direto">
							<input type="submit" id="btn-enviar" class="btn pinkbg whitecolor lato400" value="ENVIAR">
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="error-messages"></div>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</form>
	</div>
</section>
<div class="spacer"></div>
<footer class="lightbluebg">
	<div class="container">
		<div class="row">
			<div class="spacer hide show-mobile"></div>
			<div class="col-md-3 col-sm-6 col-xs-8"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-amoresdabrava-footer.png?ver=1" class="imgmax100" style="margin-bottom:-30px;"></div>
			<div class="col-md-3 col-sm-6 col-xs-4">
				<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-footer.png?ver=2" class="imgmax100 hidden-xs" style="margin-bottom: -11px;">
				<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-footer-mobile.png?ver=1" class="imgmax100 visible-xs" style="margin-bottom: -11px;margin-right:20px;">
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12"></div>
			<div class="col-md-3 col-sm-12  col-xs-12 graycolor">
				<!--<div class="row">&nbsp;</div>
				<div class="row hide-mobile">
					<span class="osbold right fs20">Contacto</span>
				</div>
				<div class="row phone-number osbold right fs30 hide-mobile">
					<span class="ossemibold fs20">(47)</span> 3349.1001
				</div>-->
			</div>
		</div>
	</div>
	<section class="rights yellowbg"> 
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12"></div>
				<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/rights1.png" class="left no-float-mobile"></div>
				<div class="col-md-1 col-sm-6 col-xs-12 tcenter"><a href="http://agenciaduplo.com.br" target="_blank"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-duplo1.png" class="right no-float-mobile"></a></div>
			</div>
	</section>
</footer>

<div class="chatbox hide-mobile">
	<div class="toggle-chat yellowbg themecolor opened bouncer" onclick="ga('send', 'event','Chat', 'Aba');"><img width="29" class="left" src="http://www.nexgroup.com.br/assets/chacara/img/icon-online.png?ver=0.2"> <span class="fs15">CHAT</span></div>
	<div class="iframe" style="width: 340px;">
	 <iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=16&amp;midia=nex_imovel" style="display: block;"></iframe>
	</div>
</div>

<?php $this->load->view("includes/footer-emp"); ?>
<script src="http://vesteacamisaicd.com.br/wp-content/themes/twentysixteen/boostrap/js/bootstrap.min.js"></script>