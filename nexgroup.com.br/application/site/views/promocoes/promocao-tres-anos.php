<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Nex 3 Anos</title>

		<link href="<?=base_url()?>assets/site/css/3anos.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>

        <script src="<?=base_url()?>assets/site/js/lib/jquery-1.6.4.min.js" type="text/javascript"></script>
       
    	
    	<!-- Analytics -->
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-1622695-41']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>		
		<!-- Fim Analytics -->
    
</head>
<body>
<!-- CONTENT -->
	<section>
        <div class="container_12">
        	<div style="width:auto; padding:12px; background:red; color:white; position:fixed;top:0; left:0"><a style="color:white;margin: 0;" href="http://www.nexgroup.com.br/">< Voltar para nexgroup.com.br</a></div>
           	<h2><img src="<?=base_url()?>assets/site/img/3anos/head-3anos.png" width="469" height="246" /></h2> 
            <p>
Solidez se constrói com o tempo. Há 3 anos, nascia a Nex Group, com a expertise formada pela união da Capa Engenharia, DHZ Construções, EGL Engenharia e Lomando Aita Engenharia. 
Mais do que uma das maiores incorporadoras do sul do Brasil, surgia um novo jeito de trabalhar. Trabalhar junto, trabalhar olhando para frente, trabalhar ousando e inovando. Trabalhar com alegria, acreditando que é possível fazer diferente. <br />
<br />

Os resultados vêm aparecendo em números, em metros quadrados construídos, em sucessos de vendas como LifePark (Canoas), JK Parque Clube (Pelotas), Chácara das Nascentes, MaxHaus, 
My Urban e Vergéis (Porto Alegre). E em futuros lançamentos, em desenvolvimento e valorização de regiões inteiras e, principalmente, através dos sorrisos de clientes que também acreditam nesse novo relacionamento que gera qualidade de vida.<br />
<br />

É o começo de uma história que vem sendo construída ao lado dos gaúchos, quebrando paradigmas, sempre de maneira completa, inteligente e moderna. Afinal, quando você quer ser reconhecido pelo seu trabalho, o que você faz define quem você é.
            
            </p>
            <a href="http://www.nexgroup.com.br/" style="margin:0"><img class="nex" src="<?=base_url()?>assets/site/img/3anos/nex-3anos.png" width="216" height="59" /></a>
            <a href="http://www.nexgroup.com.br/">www.nexgroup.com.br</a>
            
        </div>
    </section>
<!-- CONTENT END -->



</body>
</html>