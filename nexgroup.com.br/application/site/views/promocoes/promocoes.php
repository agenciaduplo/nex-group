<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Desconto na sua direção - NEXGROUP</title>

		<link href="<?=base_url()?>assets/site/css/promocao-funcionarios.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/site/css/validation.engine/validationEngine.jquery.css" type="text/css"/>
        <link rel="stylesheet" href="<?=base_url()?>assets/site/css/validation.engine/template.css" type="text/css"/>
        
        <script src="<?=base_url()?>assets/site/js/lib/jquery-1.6.4.min.js" type="text/javascript"></script>
       
        <script src="<?=base_url()?>assets/site/js/plugins/jquery.validationEngine-pt.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?=base_url()?>assets/site/js/plugins/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    
    	<script>
    	
    		function enviaForm ()
    		{
    			 $('#formContatoPromo').submit();
    		}
    	
            jQuery(document).ready(function(){

                jQuery("#formContatoPromo").validationEngine('attach', {
                	onValidationComplete: function(form, status){
                		
                		//alert(form+' - '+status);
                		
                		$("#obrigado").fadeOut();
						$("#feedback-form").fadeOut();
                		
                		if(status != false)
                		{
                		
	                		//ajax form
	                		$("#EnviarFormId").attr("href","#");
					
							var nome		= $("#nome").val();
							var email		= $("#email").val();
							var setor		= $("#setor").val();
							var interesse	= $("#interesse").val();
							
							var msg 	= '';
							
							vet_dados 	= 'nome='+ nome
							 			  +'&email='+ email
							 			  +'&setor='+ setor
										  +'&interesse='+ interesse;
						
							base_url  	= "http://www.nexgroup.com.br/index.php/promocoes/EnviaDados";
						
							$.ajax({
								type: "POST",
								url: base_url,
								data: vet_dados,
								success: function(msg) {
										
										limpaCampos('#formContatoPromo');
										
										$("#EnviarFormId").attr("href","javascript: enviaForm();");
						
										$("#obrigado").fadeIn();
										$("#feedback-form").fadeIn();
								}
						
							});
						
							return false;
                		}
                		else
                		{
                			return false;
                		}
                		
                		//ajax form		
                	}  
                });
        	});
        	
        	function limpaCampos (form)
			{
			    $(form).find(':input').each(function() {
			        switch(this.type) {
			
			            case 'password':
			            case 'select-multiple':
			            case 'select-one':
			            case 'select':
			            case 'text':
			            case 'textarea':
			                $(this).val('');
			                break;
			            case 'checkbox':
			            case 'radio':
			                this.checked = false;
			        }
			    });
			}
            
    	</script>
    	
    	<!-- Analytics -->
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-1622695-41']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>		
		<!-- Fim Analytics -->
    
</head>
<body>
<div id="container">
	<div id="box">
    
    	<div id="topo">
        	<div id="logo"><h1>Nexgroup</h1></div>
        </div>
        
        <div id="conteudo">
       		<p>Se você ficou interessado em um dos nossos empreendimentos,</p>
			<p>por favor, responda este e-mail preenchendo os campos abaixo.</p>
            <h2 class="nome">Nome completo:</h2>
            <form action="" name="" id="formContatoPromo" method="post">
    			<input name="nome" id="nome" type="text" class="form validate[required]" />
  
 			    <h2 class="setor">Setor:</h2>
                <input name="setor" id="setor" type="text" class="form validate[required]" />
    			
                <h2 class="email">E-mail:</h2>
                <input name="email" id="email" type="text" class="form validate[required,custom[email]]" />
                
                <h2 class="inter">Empreendimento de interesse:</h2>
                <input name="interesse" id="interesse" type="text" class="form validate[required]" />
			               
                </form>
                <div style="display:none;" id="feedback-form" class="sucesso">Mensagem enviada com sucesso!</div>
                <div id="btn-enviar">
                	
                    <a id="EnviarFormId" href="javascript: enviaForm();">Enviar</a>
                </div>
                <div style="display:none;" id="obrigado"> 
            		<p>Muito obrigado! Em breve entraremos em contato para</p>
					<p>informar as condições especiais na avaliação.</p>
                </div>
        </div> 
             
    </div>
</div>


<div id="container2">
	<div id="footer">
	<div id="link"><a href="http://www.nexgroup.com.br" title="Acesse o site da Nexgroup">www.nexgroup.com.br</a></div>
	<div id="marcas"></div>
    </div>
</div>

</body>
</html>