<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Nexgroup</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body bgcolor="#ffffff" style="margin:0px; text-align:center;">
    <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="display: inline-table;">
    	<tr>
    		<td colspan="3"><a href="http://www.nexgroup.com.br/index.php/promocoes?utm_source=promocao-colaboradores&utm_medium=email-marketing&utm_campaign=promo-colaboradores"><img border="0" style="display:block;" name="index_r1_c1" src="http://www.nexgroup.com.br/assets/site/img/promocao-funcionarios/index_r1_c1.jpg" width="650" height="216" border="0" id="index_r1_c1" alt="" /></a></td>
        </tr>
        <tr>
        	<td colspan="3"><a href="http://www.nexgroup.com.br/index.php/promocoes?utm_source=promocao-colaboradores&utm_medium=email-marketing&utm_campaign=promo-colaboradores"><img border="0" style="display:block;" name="index_r2_c1" src="http://www.nexgroup.com.br/assets/site/img/promocao-funcionarios/index_r2_c1.jpg" width="650" height="228" border="0" id="index_r2_c1" alt="" /></a></td>
        </tr>
        <tr>
       		<td width="42"></td>
       		<td width="566" align="center" valign="top" style="font-family:'Trebuchet MS', Arial, sans-serif; font-size:17px; color:#231F20; line-height:23px; padding-top:25px">
                <p>Para mostrar o quanto valorizamos pessoas <strong style="font-size:18px; color:#000">importantes como você</strong>, preparamos <strong>condições especiais</strong> nos nossos empreendimentos. <br />
                São vantagens que só os <strong style="font-size:18px; color:#000">nossos colaboradores</strong> têm o privilégio de poder desfrutar. Uma facilidade a mais para você fechar negócio com a <br />
                <strong style="font-size:18px; color:#000">certeza de ter se realizado</strong>.</p>
                <p><strong style="font-size:18px; color:#000">Acesse nexgroup.com.br</strong>, confira os empreendimentos e<br />
                escolha o seu. Porque nós já escolhemos: construir na sua direção.<br />
                <br />
                <span style="font-size:14px; line-height:20px;">Empreendimentos onde a Nex não tem sócios: 10% de desconto no preço de<br />
                tabela + 4% de desconto da isenção da comissão de corretagem + desconto de VPLem antecipação à tabela (descontos válidos para a primeira manifestação de interesse através do depto. comercial).<br />
                Empreendimentos onde a Nex tem sócios serão estudados caso a caso.<br />
                Descontos vedados a qualquer tempo quando feito contato com corretores externos.</span><br />
                <br />
                </p>
            </td>
       		<td width="42"></td>
		</tr>
		<tr>
			<td></td>
			<td><img name="index_r4_c2" src="http://www.nexgroup.com.br/assets/site/img/promocao-funcionarios/index_r4_c2.jpg" width="566" height="216" border="0" id="index_r4_c2" alt="" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><img name="index_r5_c2" src="http://www.nexgroup.com.br/assets/site/img/promocao-funcionarios/index_r5_c2.jpg" width="566" height="295" border="0" id="index_r5_c2" alt="" /></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3"><a href="http://www.nexgroup.com.br/index.php/promocoes?utm_source=promocao-colaboradores&utm_medium=email-marketing&utm_campaign=promo-colaboradores"><img name="index_r6_c1" src="http://www.nexgroup.com.br/assets/site/img/promocao-funcionarios/index_r6_c1.jpg" width="650" height="248" border="0" id="index_r6_c1" alt="" /></a></td>
		</tr>
    </table>
</body>
</html>
