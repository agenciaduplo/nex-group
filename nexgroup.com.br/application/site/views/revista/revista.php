<?php $this->load->view("includes/header"); ?>
<section id="megazine" class="Main Inner">
    <div class="Bg2">
        <div class="Wrapper">
            <div class="Box1">
                <h2>Revista <span>Nex Day</span></h2>
                <?php if ($revistaAtual): ?>
                    <img alt="<?php echo $revistaAtual->titulo ?>" src="<?php echo base_url() . 'uploads/revista/capa/' . $revistaAtual->capa ?>" />
                    <div class="Editorial">
                            <!--  <h4><?php echo $revistaAtual->titulo . " erer " . $revistaAtual->arquivo ?></h4>
                        <?php if ($revistaAtual->descricao)  ?>
                            <p><?php echo $revistaAtual->descricao ?> </p> -->
                        <h4><strong>Expediente</strong></h4>
                        <p><?php echo $revistaAtual->expediente ?> </p> 

                    </div>
                    <div class="Clear"></div>
                    <div class="BoxSharing">
                        <div class="addthis_toolbox addthis_default_style ">
                            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                            <a class="addthis_button_tweet"></a>
                            <a class="addthis_counter addthis_pill_style"></a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e33185829cf82a8"></script>
                        <a class="BtnRed" href="<?php echo $revistaAtual->pageflip; ?>" target="_blank">Ler revista</a>
                        <a class="BtnRed botao1" href="<?php echo base_url() ?>revista/download2/<?php echo $revistaAtual->arquivo ?>">baixar arquivo pdf</a>
                    </div>
                    <a class="LnkVoltar" href="<?php echo site_url() ?>noticias">voltar</a>
                </div>
            <?php else: ?>
                <li><p>Nenhuma revista encontrada</p></li>
            <?php endif; ?>
            <aside>
                <div class="Box BoxVideo">
                    <h2>NEX<span>Tv</span></h2>
                    <figure>
                        <a class="Modal-Video" href="http://www.youtube.com/watch?v=nHA_vdPz8ms&feature=player_embedded" title="Vídeo institucional NEX GROUP"><img alt="video nex tv" src="<?= base_url() ?>assets/site/img/layout/bg_video.jpg" /></a>
                        <!--<iframe width="310" height="206" src="http://www.youtube.com/embed/nHA_vdPz8ms?rel=0&amp;hd=1" frameborder="0"></iframe>-->
                        <figcaption>
                            <!--70 caracteres-->
                            <h4>Vídeo institucional NEX GROUP</h4>
                        </figcaption>
                    </figure>
                </div>
                <div class="Box BoxNews">
                    <h2><span>Not&iacute;cias</span></h2>
                    <ul>
                        <?php foreach ($noticias as $noticia): ?>
                            <?php $url = $this->utilidades->sanitize_title_with_dashes($noticia->titulo); ?>
                            <li>
                                <time><a href="<?php echo site_url() . 'noticias/' . $noticia->id_noticia . '/' . $url ?>" ><?php echo date('d', strtotime($noticia->data_cadastro)) ?><span><?php echo date('M', strtotime($noticia->data_cadastro)) ?></span></a></time>
                                <img alt="<?= @$noticia->titulo ?>" src="<?= base_url() ?>timthumb/timthumb.php?src=<?= 'http://www.nexgroup.com.br/uploads/noticias/medium/' . $noticia->imagem_destaque ?>&w=70&h=70" />
                                <a href="<?php echo site_url() . 'noticias/' . $noticia->id_noticia . '/' . $url ?>" title="<?php echo $noticia->titulo ?>"><?php echo character_limiter($noticia->titulo, 48); ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <a class="LnkVejaMais" href="<?php echo site_url() ?>noticias" title="Noticias">veja mais &rsaquo;</a>
                </div>
            </aside>
        </div>
    </div>
    <div class="Wrapper">
        <div >
            <h2>TODAS AS <span> edi&ccedil;&otilde;es</span></h2>
            <ul class="BoxPrevEdtions">
                <?php foreach ($outrasEdicoes as $edicao): ?>
                    <li>
                        <figure>
                            <img src="<?php echo base_url() . 'uploads/revista/capa/' . $edicao->capa ?>" alt="<?php $edicao->titulo ?>" />
                            <figcaption>
                                <h4>edi&ccedil;&atilde;o <span><?php echo $edicao->edicao ?></span></h4>
                                <a class="BtnRed" href="<?php echo base_url() . 'revista/download2/' . $edicao->arquivo ?>">baixar arquivo</a>
                            </figcaption>
                        </figure>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="BoxPagination">
                <a class="LnkPrev1" id="box-prev-edtions-prev"" href="#" title="Anterior">Anterior</a>
                <div class="NavEmps" id="box-prev-edtions-nav">

                </div>
                <a class="LnkNext1" id="box-prev-edtions-next" href="#" title="Pr&oacute;ximo">Pr&oacute;ximo</a>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("includes/footer"); ?>