<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-new");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>
			
	<!-- DESTAQUE -->
	<section id="destaque" class="notimatt">
		<div class="list_carousel ">
			<ul id="foo">
				<?php
				if($noticias)
				{
					foreach($noticias as $row)
					{ 
						$url = $this->utilidades->sanitize_title_with_dashes($row->titulo);
						$url = site_url().'noticias/'.$row->id_noticia.'/'.$url;
						?>
						<li class="box scales paral" >
							<div class="box2">
								<div class="left">
									<h3 style="font-weight: 700"><i><?=character_limiter(@$row->titulo,140);?></i></h3>
									<label><?=strftime("%d de %B de %Y", strtotime(@$row->data_cadastro))?></label>
								</div>
								<div class="right">
									<a href="<?=$url?>" class="btn btn-white button">VER MAIS</a>
								</div>
							</div>
							<div class="black" style="opacity: 0.5; filter: alpha(opacity=50);"></div>
						</li>
						<?php
					}
				}
				?>
			</ul>
			<div class="clearfix"></div>
			<div class="navegation">
				<a id="prev" class="prev" href="#"></a>
				<div id="pager" class="pager"></div>
				<a id="next" class="next" href="#"></a>
			</div>
		</div>
	</section>

    <!-- NOTICIAS HOME -->
    <section class="noticias-interna">
      	<div class="greymat">
	      	<div class="row">
				<div class="top">
					<ul class="left">
						<li class="nomobile"><i class="icon icon-file-text"></i></li>
						<li>
							<h5>NOTÍCIAS</h5>
							<h6><i>Fique por dentro e ...</i></h6>
						</li>
					</ul>
					<?php
					$arrMes = array();
					$arrAno = array();

					foreach($todas as $row)
					{
						$mesInt = intval(date('m', strtotime(@$row->data_cadastro)));
						$anoInt = intval(date('Y', strtotime(@$row->data_cadastro)));

						if( ! isset($arrMes[$anoInt][$mesInt]))
							$arrMes[$anoInt][$mesInt] = utf8_encode(strftime("%B", strtotime(@$row->data_cadastro)));
						
						if( ! isset($arrAno[$anoInt]))
							$arrAno[$anoInt] = $anoInt;
					}

					ksort($arrMes);
					?>
	          		<ul class="right">
			            <li>
			                <select id="noticia-ano">
							  <?php foreach($arrAno as $i => $ano){ ?>
			                  	<option <?php echo (intval(date('Y')) == $i) ? 'selected' : '' ?> value="<?php echo $i;?>"><?php echo $ano; ?></option>
			                  <?php } ?>
			                </select>
			            </li>
						<li>
							<?php 
							$cont_ano = 0;

							foreach($arrAno as $i => $ano)
							{
								$show = ($cont_ano == 0) ? '' : 'style="display: none;"';
								?>

								<select class="noticia-mes mes-ano-<?=$i?>" <?=$show?>>
									<option value="0" selected>Todos</option>
									<?php foreach($arrMes[$i] as $m => $mes) { ?>
									<option <?php //echo (intval(date('m')) == $m) ? 'selected' : '' ?> value="<?php echo $m;?>"><?php echo $mes; ?></option>
									<?php } ?>
								</select>

								<?php
								$cont_ano++;
							}
							?>
						</li>
		          	</ul>
		        </div>

		        <div class="list_carousel">
		        	<p class="nenhum-registro none">Nenhuma notícia encontrada na data especificada.</p>
		        	<ul>
						<?php
						if($todas)
						{
							$i = 0;

			            	foreach($todas as $row)
							{
								$url = $this->utilidades->sanitize_title_with_dashes($row->titulo);
								$url = site_url().'noticias/'.$row->id_noticia.'/'.$url;

								$mostrar = false; // Se for false não mostra o card da notícia

								// Mostra todas notícias do mês/ano atual
								// if(intval(date('m')) == intval(date('m', strtotime(@$row->data_cadastro))) && intval(date('Y')) == intval(date('Y', strtotime(@$row->data_cadastro))))

								// Mostra todas notícias do ano atual
								if(intval(date('Y')) == intval(date('Y', strtotime(@$row->data_cadastro))))
									$mostrar = true;
								// elseif($i < 5)
								// 	$mostrar = true; // Mostra até 5 notícias 
								
								$imagem = base_url().'assets/site/img/bg-default.png';

								if($row->imagem_destaque)
									$imagem = UPLOADS_PATH . 'noticias/zoom/' . $row->imagem_destaque;

								// if(file_exists('uploads/noticias/zoom/'.(@$row->imagem_destaque)) && !is_dir('uploads/noticias/zoom/'.(@$row->imagem_destaque)))
								// {
								// 	$imagem = base_url().'uploads/noticias/zoom/'.(@$row->imagem_destaque);
								// }
								?>
								<li class="li-noticias box scales <?php echo 'mes-'.intval(date('m', strtotime(@$row->data_cadastro)));?> <?php echo 'ano-'.date('Y', strtotime(@$row->data_cadastro));?>" style="<?php echo ($mostrar) ? '' : 'display:none;' ?> background:url(<?php echo $imagem; ?>)">
									<a href="<?php echo $url; ?>">
										<div class="left">
											<h2><?=character_limiter(@$row->titulo,140);?></h2>
											<div href="<?php echo $url; ?>" class="btn btn-white button">Ver Mais</div>
										</div>
										<div class="data"><?= strftime("%d", strtotime(@$row->data_cadastro))?><br><b><?= utf8_encode(strftime("%b", strtotime(@$row->data_cadastro))); ?></b></div>
										<div class="pix"></div>
										<div class="black"></div>
									</a>
								</li>
		                  		<?php
		                  		$i++;
	          				}
	      				}
	          			?>
		            </ul>
		        </div>

	      	</div><!-- .row; -->
      	</div>
	</section>

        <script type="text/javascript">

        $('#noticia-ano').change(function(){
        	$('.noticia-mes').hide();
        	$('.mes-ano-'+this.value).show();
        	$('option',$('.mes-ano-'+this.value)).eq(0).prop('selected', true);

        	noticiaTrocarMesAno();
        });

        $('.noticia-mes').change(function(){
        	noticiaTrocarMesAno();
        });

        </script>
				
<?php $this->load->view("includes/footer"); ?>