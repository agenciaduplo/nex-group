<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-scroll");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<!-- DESTAQUE -->

<section id="destaque" class="notimatt noticias-visualizar">
	<div class="list_carousel ">
		<ul id="foo">
			<?php 
			if(file_exists('uploads/noticias/zoom/'.$noticia->imagem_destaque) && !is_dir('uploads/noticias/zoom/'.$noticia->imagem_destaque))
			{
				$foto_destaque = base_url().'uploads/noticias/zoom/'.@$noticia->imagem_destaque;

				// echo '<li class="box scales paral" style="background:url('.$foto_destaque.')">'; // NÃO REMOVER
			}
			else
			{
				$foto_destaque = "";

				// echo '<li class="box scales paral" style="background:url('.base_url().'assets/site/img/bg-default2.png)">'; // NÃO REMOVER
			}
			?>
			<li class="box scales paral" style="background:url(<?=base_url()?>assets/site/img/bg-default2.png)">
				<div class="box2">
					<div class="left">
						<h3 style="font-weight: 700"><i><?=$noticia->titulo?></i></h3>
						<label><?=strftime("%d de %b de %Y", strtotime(@$noticia->data_cadastro))?></label>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
		</ul>
		<div class="clearfix"></div>
		<div class="navegation">
			<a id="prev" class="prev" href="#"></a>
			<div id="pager" class="pager"></div>
			<a id="next" class="next" href="#"></a>
		</div>
	</div>
</section>

<!-- NOTICIAS HOME -->
<section class="noticias-interna noticias-visualizar-count">
	<div class="greymat">
    	<div class="row">

    		<!-- BREADCAMPS -->
		    <ul class="count-left">
	      		<section class="breadcamps">
			        <div class="row ">
			        	<div class="bread columns">
			        		<a href="<?=site_url()?>noticias" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
			        		<ul class="breadcrumbs">
			        			<li><a href="<?=site_url()?>">Home</a></li>
			        			<li><i class="icon icon-angle-right"></i></li>
			        			<li><a href="<?=site_url()?>noticias">Notícias</a></li>
			        			<li><i class="icon icon-angle-right"></i></li>
			        			<li class="unavailable"><a href="#"><?=$noticia->titulo?></a></li>
			        		</ul>
			        	</div>
			        </div>
		      	</section>

	      		<label class="dpost">Postado em <?=strftime("%d de %b de %Y", strtotime(@$noticia->data_cadastro))?></label>

		      	<article>
			      	<?=strip_tags(@$noticia->descricao, '<p><a><strong><img><br><br/><b>\n');?>
		      	</article>
		      
				<?php if(@$galeria || $foto_destaque != "") : $cont_n = count($galeria); ?>
			        <p class="mes-label">Imagens</p>
			        <div class="list_carousel">
			        	<ul id="foo3">
							<?php if($foto_destaque != "") : $cont_n++; ?>
							<li class="box scales" style="background:url(<?=$foto_destaque?>)" rel="<?=@$noticia->id_noticia?>" title="<?=$noticia->titulo?>">
								<a href="<?=$foto_destaque?>" class="fancybox-thumb" rel="<?=@$noticia->id_noticia?>" title="<?=$noticia->titulo?>">
									<div class="black"></div>
								</a>
							</li>
							<?php endif; ?>

				          	<?php if(@$galeria) : ?>
							<?php foreach ($galeria as $foto) : ?>
							<li class="box scales" style="background:url(<?=base_url()?>uploads/noticias/galeria/<?=@$foto->imagem?>)">
								<a href="<?=base_url()?>uploads/noticias/galeria/<?=@$foto->imagem?>" class="fancybox-thumb" rel="<?=@$noticia->id_noticia?>" title="<?=@$foto->legenda?>">
									<div class="black"></div>
								</a>
				            </li>
							<?php endforeach; ?>
							<?php endif; ?>
											
							<?php //if($cont_n == 1) : ?>
								<!-- <li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li> -->
								<!-- <li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li> -->
							<?php //elseif($cont_n == 2) : ?>
								<!-- <li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li> -->
							<?php //endif; ?>
					    </ul>
				        <div class="clearfix"></div>
				        <div class="navegation">
				        	<a id="prev3" class="prev nomobile" href="#"></a>
				        	<div id="pager3" class="pager"></div>
				        	<a id="next3" class="next nomobile" href="#"></a>
				        </div>
			    	</div>
				<?php endif; ?>

				<p class="mes-label" style="padding:1em 0">
					<div class="addthis_native_toolbox"></div>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-513de0b25d7bcf95"></script>
				</p>
			</ul>

			<!-- NOTICIAS RECENTES -->
		    <ul class="count-right">
				<h6>Recentes</h6>
		      	<?php 
		      	foreach($ultimas as $noticia) 
		      	{ 
					$url = $this->utilidades->sanitize_title_with_dashes($noticia->titulo); 
					$url = site_url().'noticias/'.$noticia->id_noticia.'/'.$url;

					$rec_destaque = 'uploads/noticias/medium/'.$noticia->imagem_destaque;
				
					echo '<a href="'.$url.'" title="'.@$noticia->titulo.'">';

					if(file_exists($rec_destaque) && !is_dir($rec_destaque)) 
					{ 
						echo '<img src="'.base_url().$rec_destaque.'" alt="'.@$noticia->titulo.'">';
					} 
					else
					{ 
						echo '<img src="'.base_url().'assets/site/img/bg-default.png" alt="">';
					} 

					echo '<label>'.strftime("%d de %B de %Y", strtotime(@$noticia->data_cadastro)).'</label>';

					echo '<h3><i>'.character_limiter($noticia->titulo,48).'</i></h3>';

					echo '</a>';
				}
				?>
		    </ul>

    	</div> <!-- .row /-->
  	</div> <!-- .greymat /-->
</section>					
					
<?php $this->load->view("includes/footer"); ?>