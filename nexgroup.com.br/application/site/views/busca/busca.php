<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-fixo");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<div id="ancora2"></div>

<section id="empreendimentos-procurar">

	<div id="ancora3"></div>

	<!-- PROCURAR FIELD -->
	<div class="row">
		<div class="procurar-box">
			<label>
				Procure por palavra-chave, cidade, estado, ...
        <form id="formBusca" name="formBusca" class="" action="<?=base_url().'busca'?>" method="get">
					<input type="text" name="q" id="palavrachave" value="<?=@$palavraChave; ?>" placeholder="insira sua busca..." />
				</form>
			</label>

		</div>
	</div>
</section>

<!-- EMPREENDIMENTOS HOME -->

<!-- EMPREENDIMENTOS INTERNA -->
<section id="empreendimentos-interna" class="pages-empresa">

	<!-- BREADCAMPS  -->
	<section class="breadcamps">
		<div class="row ">
			<div class="bread columns">
				<a href="<?=base_url()?>" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
				<ul class="breadcrumbs">
					<li><a href="<?=base_url()?>">Home</a></li>
					<li><i class="icon icon-angle-right"></i></li>
					<li class="unavailable"><a href="#">Busca Nex Group</a></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="apresentacao">
		<div class="row">

			<p class="dados"><?=count($dados)?> resultados encontrados.</p>
			
			<ul class="boxer">
				<?php foreach($dados as $row) : ?>

				<li class="first"><?=$row['tipo']?></li>
				<li class="second"><a href="<?=$row['link']?>"><?=$row['title']?></a></li>
				<li class="third">--</li>

				<?php endforeach; ?>
			</ul>
			
		</div>
	</section>

</section>
<!-- EMPREENDIMENTOS HOME END -->

<?php $this->load->view("includes/footer"); ?>