<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-scroll");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<!-- DESTAQUE -->
<section id="destaque" class="destaque-empresa">
	<div class="list_carousel ">
		<ul id="">
			<li class="box scales paral" style="background-color: #C31D31; height: 412px;">
				<div class="box2">
					<div class="left">
						<h2>BUSCA NEX GROUP</h2>
						<h3><i>Encontre o que você procura aqui.</i></h3>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
		</ul>
	</div>
</section>

<!-- EMPREENDIMENTOS INTERNA -->
<section id="empreendimentos-interna" class="pages-empresa">

	<!-- BREADCAMPS  -->
	<section class="breadcamps">
		<div class="row ">
			<div class="bread columns">
				<a href="<?=base_url()?>" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
				<ul class="breadcrumbs">
					<li><a href="<?=base_url()?>">Home</a></li>
					<li><i class="icon icon-angle-right"></i></li>
					<li class="unavailable"><a href="#">Busca Nex Group</a></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="apresentacao">
		<div class="row">

			<p><?=count($dados)?> resultados encontrados.</p>
			
			<ul>
				<?php foreach($dados as $row) : ?>

				<li><?=$row['tipo']?></li>
				<li><a href="<?=$row['link']?>"><?=$row['title']?></a></li>
				<li>--</li>

				<?php endforeach; ?>
			</ul>
			
		</div>
	</section>

</section>
<!-- EMPREENDIMENTOS HOME END -->

<?php $this->load->view("includes/footer"); ?>