<?php $this->load->view("includes/header"); ?>

					<section id="news-view" class="Main Inner">
						<div class="Wrapper">
                            <div class="menu">
                                <ul>
                                    <li class="menu-a desactive"><a href="<?=site_url()?>noticias"><h2>Principais <span>notícias</span></h2></a></li>
                                    <li class="menu-b desactive"><a href="<?=site_url()?>noticias-cotidiano"><h2>Notícias do <span>cotidiano</span></h2></a></li>
                                    <li class="menu-c active "><a href="<?=site_url()?>clippings"><h2><span>Clipping</span></h2></a></li>
                                </ul>
                                
                            </div>
						</div>
                    
						<div class="Bg2">
							<div class="Wrapper">
								<div class="Box1">
									<h3>
										<time><?=date('d',strtotime(@$clipping->data_cadastro))?><span><?=strftime("%b", strtotime(@$clipping->data_cadastro))?></span></time>
                                        <div class="title">
											<?=$clipping->titulo?>
                                        </div>
                                    </h3>
									
									<?php if(@$clipping->imagem_destaque): ?>
									<img class="Main" alt="<?=@$clipping->titulo;?>" src="<?=base_url()?>uploads/clippings/medium/<?=@$clipping->imagem_destaque?>" />
									<?php endif; ?>
									
									<p><?=strip_tags(@$clipping->descricao, '<p><a><strong><img><br><br/><b>\n');?></p>

									<?php 
										$encurta = new Encurtador();
										$url_encur = site_url().'clippings/'.$clipping->id_clipping.'/'.$this->utilidades->sanitize_title_with_dashes($clipping->titulo);
										$urlCurta =  @$encurta->Curt_URL($url_encur);
									?>
                                    
                                    <?php if($clipping->arquivo): ?>
                                    <div class="download">
                                    	<i></i><p><a href="<?=site_url()?>uploads/clippings/arquivos/<?=$clipping->arquivo?>" target="_blank">Clique aqui </a>e faça o download do arquivo</p>
                                    </div>
                                	<?php endif; ?>
							
									<div class="BoxSharing" style="clear:both; width:645px;">
										<div class="addthis_toolbox addthis_default_style ">
											<a addthis:url="<?=@$url_encur?>" addthis:title="<?=@$clipping->titulo?>" class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
											<a addthis:url="<?=@$url_encur?>" addthis:title="<?=@$clipping->titulo?>" class="addthis_button_tweet"></a>
											<a addthis:url="<?=@$url_encur?>" addthis:title="<?=@$clipping->titulo?>" class="addthis_counter addthis_pill_style"></a>
										</div>
										<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e33185829cf82a8"></script>
										<div class="Minified">
											<label for="ipt-share-news">link</label>
											<input class="Text" id="ipt-share-news" value="<?=$urlCurta ?>" />
										</div>
									</div>
									<a class="LnkVoltar" href="<?=site_url()?>clippings">voltar</a>
								</div>
								<aside>
									<div class="Box BoxNews">
										<ul>
											<?php foreach($ultimas as $clipping):?>
												<?php $url	= $this->utilidades->sanitize_title_with_dashes($clipping->titulo);?>
												<li>
													<time><a rel="Clipping" href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" ><?=date('d',strtotime($clipping->data_cadastro))?><span><?=strftime("%b", strtotime(@$clipping->data_cadastro))?></span></a></time>
													
													<a href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" title="<?=@$clipping->titulo?>"><?=character_limiter($clipping->titulo,48);?></a>
												</li>
											<?php endforeach; ?>	
										</ul>
										<a class="LnkVejaMais" href="<?=site_url()?>clippings" title="Clippings">veja mais &rsaquo;</a>
									</div>
									
								</aside>
								<div class="Clear"></div>
							</div>
						</div>
					</section>
					
<?php $this->load->view("includes/footer"); ?>