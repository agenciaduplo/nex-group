<?php $this->load->view("includes/header"); ?>

					<section id="news" class="Main Inner">
						
						
						<div class="Wrapper">
						<!-- 	
							<div id="banner-news" >
								<figure>
									<a href="<?=site_url()?>clippings/visualizar" title="Saiba tudo da grande festa de lançamento da NEX GROUP">
										<img alt="Saiba tudo da grande festa de lançamento da NEX GROUP" src="<?=base_url()?>uploads/bannerClipping/01.jpg" />
										<figcaption>
											
											<p>Saiba tudo da grande festa de lan&ccedil;amento da NEX GROUP</p>
										</figcaption>
									</a>
								</figure>
							</div>
						-->
                        <div class="menu">
                        	<ul>
                            	<li class="menu-a desactive"><a href="<?=site_url()?>noticias"><h2>Principais <span>notícias</span></h2></a></li>
                            	<li class="menu-b desactive"><a href="<?=site_url()?>noticias-cotidiano"><h2>Notícias do <span>cotidiano</span></h2></a></li>
                            	<li class="menu-c active"><a href="<?=site_url()?>clippings"><h2><span>Clipping</span></h2></a></li>
                            </ul>
                        	
                        </div>
                        
						</div>
						
						<div class="Bg2">
							<div class="Wrapper home-news">
								<div class="Box1">
									<div class="BoxNews">
										<ul>
											
											<?php if($clippings): ?>
											<?php foreach($clippings as $row):?>
												<?php $url	= $this->utilidades->sanitize_title_with_dashes($row->titulo);?>
												<li>
													<?php if(@$row->imagem_destaque): ?>
                                                	<img alt="<?=@$row->titulo?>" src="<?=base_url()?>timthumb/timthumb.php?src=<?='http://www.nexgroup.com.br/uploads/clippings/zoom/'.$row->imagem_destaque?>&w=628&h=300&zc=2" /></a>
													<?php endif; ?>
													<time><a href="<?=site_url()?>clippings/<?=@$row->id_clipping?>/<?=@$url?>" ><?=date('d',strtotime(@$row->data_cadastro))?><span><?=strftime("%b", strtotime(@$row->data_cadastro))?></span></a></time>
                                                    
													<div class="title">
                                                        <a href="<?=site_url()?>clippings/<?=@$row->id_clipping?>/<?=@$url?>" title="<?=$row->titulo?>">
                                                        </a>
                                                       
                                                        <a href="<?=site_url()?>clippings/<?=@$row->id_clipping?>/<?=@$url?>" title="<?=$row->titulo?>"><?=character_limiter(@$row->titulo,140);?></a> 
                                                        <p>
                                                        	<?=character_limiter(@$row->descricao, 300);?>
                                                        </p>
														<a href="<?=site_url()?>clippings/<?=@$row->id_clipping?>/<?=@$url?>" class="continue">Leia mais</a>
                                                    </div>
                                                    
												</li>
											<?php endforeach; ?>
											<?php else: ?>
												<li><p>Nenhum clipping encontrado</p></li>
											<?php endif; ?>
										
										</ul>
									</div>
									<ul id="pag-news">
									<?=@$paginacao;?>
										
									</ul>
								</div>	
                                
                                
									
                                    <ul id="pag-news">
									<?//@$paginacao;?>
										
									</ul>
								</div>
                                
								<aside>
									<div class="Box BoxNews">
										<ul class="mes mes-on">
											<h4>OUTRAS<span> NOTÍCIAS</span></h4>

											<?php foreach($ultimas as $noticia):?>
												<?php $url	= $this->utilidades->sanitize_title_with_dashes($noticia->titulo);?>
												<li>
													<time><a rel="Noticia" href="<?=site_url().'noticias/'.$noticia->id_noticia.'/'.$url?>" ><?=date('d',strtotime($noticia->data_cadastro))?><span><?=strftime("%b", strtotime(@$noticia->data_cadastro))?></span></a></time>
													
													<a href="<?=site_url().'noticias/'.$noticia->id_noticia.'/'.$url?>" title="<?=@$noticia->titulo?>"><?=character_limiter($noticia->titulo,40);?></a>
												</li>
											<?php endforeach; ?>	
										</ul>
										<!--
                                        
										<ul class="mes mes-on">
											<h4><span>MARÇO</span>2013</h4>

											<?php foreach($ultimas as $clipping):?>
												<?php $url	= $this->utilidades->sanitize_title_with_dashes($clipping->titulo);?>
                                                
                                                
												<li>
													<time><a rel="Clipping" href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" ><?=date('d',strtotime($clipping->data_cadastro))?></a></time>
													
													<a href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" title="<?=@$clipping->titulo?>"><?=character_limiter($clipping->titulo,48);?></a>
												</li>
											<?php endforeach; ?>	
										</ul>
                                        
										<ul class="mes mes-off">
											<h4><span>FEVEREIRO</span>2013</h4>

											<?php foreach($ultimas as $clipping):?>
												<?php $url	= $this->utilidades->sanitize_title_with_dashes($clipping->titulo);?>
                                                
                                                
												<li>
													<time><a rel="Clipping" href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" ><?=date('d',strtotime($clipping->data_cadastro))?></a></time>
													
													<a href="<?=site_url().'clippings/'.$clipping->id_clipping.'/'.$url?>" title="<?=@$clipping->titulo?>"><?=character_limiter($clipping->titulo,48);?></a>
												</li>
											<?php endforeach; ?>	
										</ul>
									-->


									</div>
									
								</aside>
                                
								<div class="Clear"></div>
                                
                        
                        
						<div class="Wrapper">
							<?php $this->load->view("includes/imoveis_visitados"); ?>
						</div>
					</section>
					
<?php $this->load->view("includes/footer"); ?>