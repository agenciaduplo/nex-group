<!-- MODAL CONTATO -->
<div id="area_contato" style="display:none">
	<div class="black click_contato"></div>
	<div class="row">

		<!-- DIV SUCESSO -->
		<div class="sucess">
			<div class="boxmat">
				<h2>Sua contato foi enviado com sucesso!</h2>
				<h3>Obrigado e em breve entraremos em contato com você!</h3>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="Contato.close(1);">Enviar outra mensagem</a>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="Contato.close(2);">fechar e continuar navegando</a>
			</div>
		</div>
		<!-- DIV SUCESSO END -->

		<a href="javascript:;" class="close-telefone click_contato" rel="nofollow"><i class="icon-remove"></i></a>

		<h2 class="columns" id="titulo_contato">ENTRE EM CONTATO</h2>

		<img src="<?=base_url()?>assets/site/img/contato-comprar.png" alt="Contato Nex">

		<form name="formContato" id="formContato" method="post" action="" onsubmit="return false;">
			<?php
				if($this->agent->referrer())
				{
					$url = $this->agent->referrer();
				}
				else
				{
					$url = current_url();
					if($_SERVER['QUERY_STRING'])
					{
						$url .= '?' . $_SERVER['QUERY_STRING'];
					}
				}
			?>
			<input type="hidden" name="contato_refer" value="<?=$url?>" />
			<div class="left">
				<p style="font-style: italic;">
					Nosso horário de atendimento é de segunda à sexta, das 8h às 18h. 
				</p> 
			</div>

			<div class="doble left">
				<label>Nome: * <input type="text" name="contato_nome" placeholder="Digite seu nome aqui" tabindex="1" /></label>
			</div>

			<div class="doble right">
				<label>E-mail: * <input type="text" name="contato_email" placeholder="Digite seu e-mail aqui" tabindex="2" /></label>
			</div>

			<div class="doble left">
				<label>Telefone: * <input type="text" name="contato_telefone" class="mask-telefone" placeholder="Digite seu telefone aqui" tabindex="3" /></label>
			</div>

			<div class="doble right horario">
				<label>Interessado em um imóvel especifico?</label>
				<select name="contato_id_empreendimento" id="contato_id_empreendimento" tabindex="4">
					<option value="">Selecione um imóvel:</option>
				</select>
			</div>

			<div class="doble left">
				<label>	Mensagem: <span style="font-size: 0.9em;font-weight: normal;text-transform: none;">(minimo 20 caracteres)</span> *</label>
				<textarea name="contato_massege" id="contato_massege" placeholder="Digite sua mensagem aqui" tabindex="5"></textarea>
			</div>

			<div class="doble right radios">
				<label style="padding:0 1em 0 0">Contate-me por:</label>
				<input type="radio" name="contato_tipo" value="1" id="contato_tipo1" checked="checked" tabindex="6"><label for="contato_tipo1">E-mail</label>
				<input type="radio" name="contato_tipo" value="2" id="contato_tipo2" tabindex="7"><label for="contato_tipo2">Telefone</label>
			</div>

			<div class="doble right">
				<input type="checkbox" name="contato_newsletter" value="S" id="checkbox1" tabindex="8">
				<label class="check" for="checkbox1">Gostaria de receber e-mails informativos e promocionais da Nex Group.</label>

				<a href="javascript:;" class="btn large btn-red button left" rel="nofollow" onclick="Contato.send();" tabindex="9">Enviar mensagem</a>
			</div>
		</form>

	</div>
</div>

<script type="text/javascript">

	var Contato = {
		form: $('#formContato'),
		nome: '',
		email: '',
		telefone: '',
		contato_tipo: '',
		id_empreendimento: '',
		mensagem: '',
		newsletter: '',
		refer: '',
		getFields: function()
		{
			this.nome     			= $('input[name=contato_nome]', this.form);
			this.email     			= $('input[name=contato_email]', this.form);
			this.telefone 			= $('input[name=contato_telefone]', this.form);
			this.contato_tipo 		= $('input[name=contato_tipo]:checked', this.form);
			this.id_empreendimento 	= $('#contato_id_empreendimento', this.form);
			this.mensagem 			= $('#contato_massege', this.form);
			this.newsletter 		= $('input[name=contato_newsletter]:checked', this.form);
			this.refer 				= $('input[name=contato_refer]', this.form);
		},
		validation: function()
		{
			if(this.nome.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo NOME corretamente."); 
				this.nome.focus();
				return false;
			}

			var rxEmail = /^.+@.+\..{2,}$/;
	        if(!rxEmail.test(this.email.val()))
	        {
	            this.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente."); 
				this.email.focus();
				return false;
	        }

			if(this.telefone.val() == "" || this.telefone.val() == "(__) ____-_____")
			{   
				this.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
				this.telefone.focus();
				return false;
			}

			if(this.mensagem.val().length < 20)
			{   
				this.showMassage("Por favor, sua mensagem deve ter 20 ou mais caracteres."); 
				this.mensagem.focus();
				return false;
			}

			return true;
		},
		showMassage: function(text)
		{
			alert(text);
		},
		btnText: function(text)
		{
			$('.btn', this.form).text(text);
		},
		send: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('Enviar mensagem');
				return false;
			}

			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/entreEmContatoXHR",
				data: {
					nome: 				Contato.nome.val(),
					email: 				Contato.email.val(),
					telefone: 			Contato.telefone.val(),
					contato_tipo: 		Contato.contato_tipo.val(),
					id_empreendimento: 	Contato.id_empreendimento.val(),
					mensagem: 			Contato.mensagem.val(),
					newsletter: 		Contato.newsletter.val(),
					refer: 				Contato.refer.val()
				},
				dataType: "json",
				success: function(response)
				{
					Contato.btnText('ENVIAR');

					if(response.erro == 1)
					{
						Contato.showMassage("Por favor, preencha o campo NOME corretamente."); 
						Contato.nome.focus();
						return false;
					}

					if(response.erro == 2)
					{
						Contato.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente.");  
						Contato.nome.focus();
						return false;
					}

					if(response.erro == 3)
					{
						Contato.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
						Contato.nome.focus();
						return false;
					}

					if(response.erro == 4)
					{
						Contato.showMassage("Por favor, sua mensagem deve ter 20 ou mais caracteres."); 
						Contato.nome.focus();
						return false;
					}

					if(response.erro == 0 || response.erro == 101)
					{
						$('.sucess','#area_contato').show();
						Contato.form.get(0).reset();
						return true;
					}
				}
			});

		},
		close: function(n)
		{
			if(n == 1)
			{
				$('.sucess','#area_contato').hide();
			}
			else
			{
				$('.sucess','#area_contato').hide();
				$('#area_contato').hide();
			}
		},
		getEmpreendimentosToSelect: function()
		{
			$.ajax({
				url: BASE_URL + "contato/getEmpreendimentosXHR",
				dataType: "html",
				success: function(response)
				{
					$('#contato_id_empreendimento', this.form).html(response);
					$('#contato_id_empreendimento', this.form).append('<option value="0">Outros</option>')
				}
			});
		}
	} // object


	$(document).ready(function()
	{
		Contato.getEmpreendimentosToSelect();
	});
</script>