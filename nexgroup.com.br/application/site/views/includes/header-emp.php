<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
    <!--[if IE 8]>   <html class="ie8" lang="pt-BR"> <![endif]-->  
    <!--[if IE 9]>   <html class="ie9" lang="pt-BR"> <![endif]-->  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

	
	<!-- TODO: Buscar infos-->
    <meta name="description" content="<?php echo $description; ?>" />  
    <meta name="keywords" content="<?php echo $keywords; ?>" />    
    <meta name="author" content="http://www.agenciaduplo.com.br" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />

    <meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='<?php echo $title; ?>' />
	<?php if($pagedir == 'myurban'){ ?>
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/<?php echo $ogimage; ?>'/>
	<?php } else { ?>
    <meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<?php } ?>
    <meta property='og:description' content='<?php echo $description; ?>'/>
    <meta property='og:url' content='<?php echo $ogurl; ?>'/>
    


    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />
    
    <link rel="shortcut icon" href="<?=base_url()?>favicon.png" />
    
    <link rel="canonical" href="<?php echo $ogurl; ?>" />

	<?php if($pagedir != 'myurban'){ ?>
	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	
	<?php } else { ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/css/swiper.min.css">
	<?php }  ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/css/main.css?ver=0.4" type="text/css" media="screen" />
    
    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->
	
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->  
	
	<!-- PROPRIEDADE NEX@http://www.nexgroup.com.br -->
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo $UA; ?>', 'auto');
	ga('send', 'pageview');
	</script>
	
</head>
<body class="<?php echo $empClass; ?>">