<section class="vistos-home" style="padding-bottom: 35px;">
	<div class="row">

		<div class="top">
			<ul class="left">
				<li class="nomobile"><i class="icon icon-th"></i></li>
				<li>
					<h5>IMÓVEIS QUE VOCÊ JÁ VIU</h5>
					<h6><i>Os empreendimentos já visitados por você em nosso site</i></h6>
				</li>
			</ul >
			<ul class="right nomobile">
				<?php $active = false;?>

				<?php if(count(@$visitados) > 0) { $active = true; ?><li class="active"><a href="javascript:void(0);" id="recentes" onclick="selecionarAba(this);">Mais recentes</a></li><?php } ?>
				
				<?php if(count(@$favoritos) > 0) { ?><li <?=( ! $active) ? 'class="active"' : ''?>><a href="javascript:void(0);" id="favoritos" onclick="selecionarAba(this);">Favoritos</a></li><?php $active = true; } ?>

				<li <?=( ! $active) ? 'class="active"' : '' ?>><a href="javascript:void(0);" id="mais" onclick="selecionarAba(this);" class="last">Mais visitados</a></li>
			</ul>
		</div>
		
		<div class="list_carousel">
			<?php 
			$active = false;

			// IMÓVEIS RECENTES (visitados pelo usuário na sessão atual)
			// =======================================================================================================
				
			if(count(@$visitados) > 0) : $i=0; ?>
			<div id="tab-recentes" <?=($active) ? 'style="display:none;"' : ''?>>
				<ul id="foo10">
					<?php 
					foreach (@$visitados as $row) : 
					
					$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
					
					$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
					$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;

					$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
					$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
					$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);

					if(site_url() == 'http://localhost/newnex/trunk/')
          			{
          				$iv_file_headers = array();
          				$iv_file_headers[0] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo);
          				$iv_file_headers[1] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo);
          				$iv_file_headers[2] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo);

          				if(count($fotos1) > 0 && $iv_file_headers[0][0] == 'HTTP/1.1 200 OK') 	  $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo;
          				elseif(count($fotos2) > 0 && $iv_file_headers[1][0] == 'HTTP/1.1 200 OK') $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo;
          				elseif(count($fotos3) > 0 && $iv_file_headers[2][0] == 'HTTP/1.1 200 OK') $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo;
          				else $imagem = base_url().'assets/site/img/bg-default.png';
          			}	
          			else
          			{
          				if(count($fotos1) > 0 && file_exists('uploads/imoveis/midias/'.$fotos1[0]->arquivo))      $imagem = base_url().'uploads/imoveis/midias/'.$fotos1[0]->arquivo;
          				else if(count($fotos2) > 0 && file_exists('uploads/imoveis/midias/'.$fotos2[0]->arquivo)) $imagem = base_url().'uploads/imoveis/midias/'.$fotos2[0]->arquivo;
          				else if(count($fotos3) > 0 && file_exists('uploads/imoveis/midias/'.$fotos3[0]->arquivo)) $imagem = base_url().'uploads/imoveis/midias/'.$fotos3[0]->arquivo;
          				else $imagem = base_url().'assets/site/img/bg-default.png';
          			}
					?>
					<li class="box scales" style="background:url(<?=$imagem?>)">
						<div class="left">
							<h2><?=@$row->empreendimento?></h2>
							<h3><i><?=@$row->chamada_empreendimento?></i></h3>
							<a href="<?=$url?>" class="btn btn-white button">Conheça</a>
						</div>
						<div class="pix"></div>
						<div class="black"></div>
					</li>
					<?php
					$i++;
					endforeach; 

					// for($j= $i; $j<5; $j++)
					// 	echo '<li class="box scales" style="background:url('.base_url().'assets/site/img/bg-default.png)"><li>'; 
					if( ! $this->agent->is_mobile())
					{
						for($j= $i; $j<5; $j++)
						{
							?>
	            			<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                		<?php
	                	}
                	}
                	?>
				</ul>
				<div class="clearfix"></div>
				<div class="navegation">
					<a id="prev10" class="prev nomobile" href="#"></a>
					<div id="pager10" class="pager"></div>
					<a id="next10" class="next nomobile" href="#"></a>
				</div>
			</div>
			<?php
		 	$active = true;
		 	endif; 

		 	// IMÓVEIS MAIS VISITADOS
			// =======================================================================================================
			
			if(count(@$mais_vistos) > 0) : $i=0; ?>
			<div id="tab-mais" <?=($active) ? 'style="display:none;"' : ''?>>
				<ul id="foo12">
					<?php 
					foreach (@$mais_vistos as $row) : 
					
					$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
					
					$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
					$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;

					$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
					$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
					$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);

					if(site_url() == 'http://localhost/newnex/trunk/')
          			{
          				$mv_file_headers = array();
          				$mv_file_headers[0] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo);
          				$mv_file_headers[1] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo);
          				$mv_file_headers[2] = @get_headers(UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo);

          				if(count($fotos1) > 0 && $mv_file_headers[0][0] == 'HTTP/1.1 200 OK')     $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo;
          				elseif(count($fotos2) > 0 && $mv_file_headers[1][0] == 'HTTP/1.1 200 OK') $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo;
          				elseif(count($fotos3) > 0 && $mv_file_headers[2][0] == 'HTTP/1.1 200 OK') $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo;
          				else $imagem = base_url().'assets/site/img/bg-default.png';
          			}	
          			else
          			{
          				if(count($fotos1) > 0 && file_exists('uploads/imoveis/midias/'.$fotos1[0]->arquivo))      $imagem = base_url().'uploads/imoveis/midias/'.$fotos1[0]->arquivo;
          				else if(count($fotos2) > 0 && file_exists('uploads/imoveis/midias/'.$fotos2[0]->arquivo)) $imagem = base_url().'uploads/imoveis/midias/'.$fotos2[0]->arquivo;
          				else if(count($fotos3) > 0 && file_exists('uploads/imoveis/midias/'.$fotos3[0]->arquivo)) $imagem = base_url().'uploads/imoveis/midias/'.$fotos3[0]->arquivo;
          				else $imagem = base_url().'assets/site/img/bg-default.png';
          			}
					?>
					<li class="box scales" style="background:url(<?=$imagem?>)">
						<div class="left">
							<h2><?=@$row->empreendimento?></h2>
							<h3><i><?=@$row->chamada_empreendimento?></i></h3>
							<a href="<?=$url?>" class="btn btn-white button">Conheça</a>
						</div>
						<div class="pix"></div>
						<div class="black"></div>
					</li>
					<?php
					$i++;
					endforeach; 

					// for($j = $i; $j < 5; $j++)
					// 	echo '<li class="box scales" style="background:url('.base_url().'assets/site/img/bg-default.png)"><li>'; 
					if( ! $this->agent->is_mobile())
					{
						for($j= $i; $j<5; $j++)
						{
							?>
	            			<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                		<?php
	                	}
                	}
                	?>
				</ul>
				<div class="clearfix"></div>
				<div class="navegation">
					<a id="prev12" class="prev nomobile" href="#"></a>
					<div id="pager12" class="pager"></div>
					<a id="next12" class="next nomobile" href="#"></a>
				</div>
			</div>
			<?php
			$active = true; 
			endif; 

			// IMÓVEIS FAVORITOS
			// =======================================================================================================

			if(count(@$favoritos) > 0) : $i=0; ?>
			<div id="tab-favoritos" <?=($active) ? 'style="display:none;"' : ''?>>
				<ul id="foo11">
					<?php 
					foreach (@$favoritos as $row) : 

					$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
					
					$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
					$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;

					// $favorito_file_headers = @get_headers(UPLOADS_PATH.'imoveis/fachada/visualizar/'.@$row->imagem_fachada);

					// if($favorito_file_headers[0] == 'HTTP/1.1 200 OK')
					// 	$favorito_img = UPLOADS_PATH.'imoveis/fachada/visualizar/'.@$row->imagem_fachada;
					// else
					// 	$favorito_img = base_url().'assets/site/img/bg-default.png';

					$fotos1	= $this->modelImoveis->getFotos($row->id_empreendimento, 30);
					$fotos2	= $this->modelImoveis->getFotos($row->id_empreendimento, 10);
					$fotos3	= $this->modelImoveis->getFotos($row->id_empreendimento, 11);

					$imagem = base_url().'assets/site/img/bg-default.png';

					if(count($fotos1) > 0)     $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos1[0]->arquivo;
      				elseif(count($fotos2) > 0) $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos2[0]->arquivo;
      				elseif(count($fotos3) > 0) $imagem = UPLOADS_PATH.'imoveis/midias/'.$fotos3[0]->arquivo;
					?>
					<li class="box scales" style="background:url(<?=$imagem?>)">
						<div class="left">
							<h2><?=@$row->empreendimento?></h2>
							<h3><i><?=@$row->chamada_empreendimento?></i></h3>
							<a href="<?=$url?>" class="btn btn-white button">Conheça</a>
						</div>
						<div class="pix"></div>
						<div class="black"></div>
					</li>
					<?php 
					$i++;
					endforeach; 

					// for($j = $i; $j < 5; $j++) 
					// { 
					// 	echo '<li class="box scales" style="background:url('.base_url().'assets/site/img/bg-default.png)"><li>'; 
					// } 
					if( ! $this->agent->is_mobile())
					{
						for($j= $i; $j<5; $j++)
						{
							?>
	            			<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
	                		<?php
	                	}
                	}
                	?>
				</ul>
				<div class="clearfix"></div>
				<div class="navegation">
					<a id="prev11" class="prev nomobile" href="#"></a>
					<div id="pager11" class="pager"></div>
					<a id="next11" class="next nomobile" href="#"></a>
				</div>
			</div>
			<?php endif; ?>

		</div> <!-- .list_carousel; -->
	</div>
</section>