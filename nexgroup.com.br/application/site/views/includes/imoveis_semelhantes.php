<?php if( ! empty($semelhantes) && count(@$semelhantes) > 0) { ?>
<section class="vistos-home padmatt" style="margin-bottom:10px">
	<div class="row">
		<div class="top" style="width:600px;">
			<ul class="left" style="width:600px;">
				<li class="nomobile"><i class="icon icon-th"></i></li>
				<li>
					<h5>IMÓVEIS SUGERIDOS</h5>
					<h6><i>Confira os imóveis que se assemelham ao empreendimento acima</i></h6>
				</li>
			</ul>
		</div>
		<div class="list_carousel">
			<div id="tab-recentes">
				<ul id="foo10">
					<?php
					$i = 0;

					foreach (@$semelhantes as $row)
					{
						$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;

						$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
						$url = site_url().'imoveis/'.@$row->id_empreendimento.'/'.@$url;

						?>
						<li class="box scales" style="background:url(<?=base_url()?>uploads/imoveis/fachada/visualizar/<?=@$row->imagem_fachada?>)"><!-- CARD -->
							<a href="<?=$url?>">
								<div class="left">
									<h2><?=@$row->empreendimento?></h2>
									<h3><i><?=@$row->chamada_empreendimento?></i></h3>
									<div href="<?=$url?>" class="btn btn-white button">Conheça</div>
								</div>
								<div class="pix"></div>
								<div class="black"></div>
							</a>
						</li>
						<?php
						$i++;
					}

					if( ! $this->agent->is_mobile())
					{
						for($j= $i; $j<5; $j++)
						{
							?>
							<li class="box scales" style="background:url(<?=base_url()?>assets/site/img/bg-default.png)"></li><!-- CARD -->
							<?php
						}
					}
					?>
				</ul>
				<div class="clearfix"></div>
				<div class="navegation">
					<a id="prev10" class="prev nomobile" href="#"></a>
					<div id="pager10" class="pager"></div>
					<a id="next10" class="next nomobile" href="#"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>