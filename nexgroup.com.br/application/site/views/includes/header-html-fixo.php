<?php
	@session_start();
		
	if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
		
		if(isset($_GET['utm_campaign'])){
			$ref[] = $_GET['utm_campaign'];
		}
		
		if(isset($_GET['utm_source'])){
			$ref[] = $_GET['utm_source'];
		}
		
		if(isset($_GET['utm_medium'])){
			$ref[] = $_GET['utm_medium'];
		}
		
		$ref = implode('-', $ref);
		
	} else if(isset($_SESSION['origem'])){
		$ref = $_SESSION['origem'];
		
	}else if(isset($_SERVER['HTTP_REFERER'])) {
		
		$ref = @$_SERVER['HTTP_REFERER'];
		
		
	} else {
		$ref = 'direto';
	}
?>

        <div id="ancora"></div>
        <nav class="header-fixo parado fixo">
          <div class="left">
            <a class="first">
              <span title="Clique para abrir o menu"> MENU </span>
              <i class="icon icon-reorder"></i>
              <i class="icon icon-remove"></i>
            </a>
            <form id="formBusca" name="formBusca" class="formBusca" action="<?=base_url().'busca'?>" method="get">
	            <input type="text" name="q" id="palavra-chave" value="" placeholder="Insira sua busca..." />
              <input type="submit" class="btn-busca-fixo" value="Buscar"  />
            </form>
            <div class="btn-procurar">
	            <a href="javascript:void(0);" class="search">
	              <i class="icon icon-search"></i>
	              <span style="margin-top: -5px">PROCURAR <br><i>no Site</i></span>
	            </a>
            </div>
          </div>

          <a href="<?=base_url()?>" class="logo">
            <h1 class="title">Nex Group</h1>
          </a>

          <div class="right">
            <span class="pric">
              <a rel="contact" href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);">
                <i class="icon icon-comments"></i>
                <span style="margin-top: -5px">CORRETOR <br><i>Online</i></span>
              </a>
              <a class="click_fone" title="Ligamos para você" >
                <i class="icon icon-phone"></i>
              </a>
              <a href="<?=base_url()?>contato" title="Contato Nex">
                <i class="icon icon-envelope"></i>
              </a>
            </span>
          </div>
        </nav>