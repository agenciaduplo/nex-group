<script> var base_url = '<?=base_url()?>'; </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<?php if($pagedir == 'myurban' || $pagedir == 'maxhaus' ){ ?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php } ?>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.mask.min.js"></script>
<?php if($pagedir == 'myurban' || $pagedir == 'maxhaus' || $pagedir == 'amoresdabrava'){ ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/colorbox/colorbox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/site/css/animate.min.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/js/swiper.min.js"></script>
<?php } else { ?>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?=base_url()?>assets/fancybox/jquery.fancybox.js?v=2.1.5"></script>

<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<?php } ?>

<script type="text/javascript" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/js/script.js?v=5.8"></script>

<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 972557761;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
 
</body>
</html>