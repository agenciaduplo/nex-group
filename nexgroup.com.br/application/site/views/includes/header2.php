<?php
	@session_start();
		
	if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
		
		if(isset($_GET['utm_campaign'])){
			$ref[] = $_GET['utm_campaign'];
		}
		
		if(isset($_GET['utm_source'])){
			$ref[] = $_GET['utm_source'];
		}
		
		if(isset($_GET['utm_medium'])){
			$ref[] = $_GET['utm_medium'];
		}
		
		$ref = implode('-', $ref);
		
	} else if(isset($_SESSION['origem'])){
		$ref = $_SESSION['origem'];
		
	}else if(isset($_SERVER['HTTP_REFERER'])) {
		
		$ref = @$_SERVER['HTTP_REFERER'];
		
		
	} else {
		$ref = 'direto';
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		
		<?php if (@$this->uri->segment(1) == 'nao-encontrou-seu-imovel') { ?>
		
		<title>Não encontrou seu imóvel? - <?=@$title?></title>
		
		<?php } else { ?>
		
		<title><?=@$title?></title>
		
		<?php } ?>
		
		<!-- METAS -->
		<?php if(@$pagetag=='imovelvisualizar'):

				if(@$imovel->tags_description):?>
					<meta name="description" content="<?=@$imovel->tags_description?>" />
				<?php else:?>
					<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />	
				<?php endif;
				
				if(@$imovel->tags_keywords):?>
					<meta name="keywords" content="<?=@$imovel->tags_keywords?>" />
				<?php else:?>
					<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
				<?php endif;?>

				<meta property='og:locale' content='pt_BR' />
				<meta property='og:title' content='<?=@$title?>' />
				<meta property='og:image' content='http://www.nexgroup.com.br/uploads/imoveis/logo/<?=@$imovel->logotipo?>'/>
				if(@$imovel->tags_description):?>
					<meta property='og:description' content='<?=@$imovel->tags_description?>'/>
				<?php else:?>
					<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
				<?php endif; ?>
				<meta property='og:url' content='<?=current_url()?>'/>
				
	    <?php else: ?>

	    		<?php if(@$description):?>
	    				<meta name="description" content="<?=$description?>" />
	    		<?php else: ?>
	    				<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
	    		<?php endif;?>
	    		<?php if(@$description):?>
	    				<meta name="keywords" content="<?=$keywords?>" />
	    		<?php else: ?>
	    				<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
		    	<?php endif;?>

				<meta property='og:locale' content='pt_BR' />
				<meta property='og:title' content='Nex Group' />
				<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
				<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
				<meta property='og:url' content='http://www.nexgroup.com.br'/>
	    
	    <?php endif;?>
	    
	    <meta name="author" content="http://www.divex.com.br" />
	    <meta name="language" content="pt-br" />
	    <meta name="revisit-after" content="1 days" />
	    <meta name="mssmarttagspreventparsing" content="true" />
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />
		<!-- METAS -->
		
		
		<!-- STYLE -->
		<link rel="shortcut icon" href="<?=base_url()?>favicon.png" />
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/main.css" type="text/css" media="screen" />
		<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
		<!-- STYLE -->
		
		
		<!-- LIBS -->
		<script type="text/javascript" src="<?=base_url()?>assets/site/js/lib/jquery-1.6.4.min.js"></script>
		<!-- LIBS -->

		<!-- SCRIPTS -->
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!-- Analytics -->
		<script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-1622695-41']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>		
		<!-- Fim Analytics -->
		
	</head>
	<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
	<body>
		
		<!-- Facebook -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=266241050086130";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<!-- Facebook -->
	
		<div class="Wrapper" >
			<header class="Main">
				<h1><a href="<?=site_url()?>" rel="index" title="NEX GROUP">NEX GROUP</a></h1>
				<div class="Right">
					<ul class="Foo">
						<!--<li><a href="<?=site_url()?>conexao" title="Conex&atilde;o">Conex&atilde;o</a></li>
						<li><a href="<?=site_url()?>clientes" title="Clientes">Clientes</a></li>
						<li><a href="<?=site_url()?>investidores" title="Investidores">Investidores</a></li>
						<li><a href="<?=site_url()?>contato" title="Venda seu terreno">Venda seu terreno</a></li>
						<li><a href="<?=site_url()?>contato" title="Trabalhe Conosco">Trabalho conosco</a></li>-->
						<li><a class="LnkConexao" href="<?=site_url()?>conexao" target="_blank" title="Conex&atilde;o">acesso restrito ao corretor</a></li>
					</ul>
					<div>
						<form method="get" action="<?=base_url()?>buscar" id="frm-search-simple">
							<fieldset>
								<legend>Busca</legend>
								<label for="inp-search">Busca</label>
								<input class="Text" id="inp-search" name="q" required="required" type="text" value="Buscar" onfocus="if(this.value == 'Buscar') this.value=''" onblur="if(this.value == '') this.value = 'Buscar'" />
								<button class="BtnSearch" form="frm-search-simple" type="submit" >Buscar</button>
								<!--<a href="<?=site_url()?>busca-avancada" rel="search" title="Busca Avan&ccedil;ada?">Busca Avan&ccedil;ada?</a>-->
							</fieldset>
						</form>
						<ul class="Exception">
							<li><a class="LnkAtendimentoEmail Atendimento"  href="<?=site_url()?>atendimento-email/<?=@$imovel->id_empreendimento?>" >Atendimento por e-mail</a></li>
							<li><a class="LnkAtendimentoTelefone Atendimento"  href="<?=site_url()?>ligamos-para-voce/<?=@$imovel->id_empreendimento?>" >Ligamos para voc&ecirc;</a></li>
							<li><a class="LnkCorretorOnline"  href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online">Corretor Online</a></li>
						</ul>
						
					</div>
				</div>
			
			</header>
			<!--<ul class="Nav3">
				<li><a class="LnkCapa" href="http://www.capa.com.br/site/grupo/" rel="external" title="Grupo Capa">Capa</a></li>
				<li><a class="LnkDHZ" href="http://www.dhz.com.br/" rel="external" title="DHZ Constru&ccedil;&otilde;es">DHZ Constru&ccedil;&otilde;es</a></li>
				<li><a class="LnkEGL" href="http://www.eglengenharia.com.br/novosite/index.php/home/principal" rel="external" title="EGL Engenharia">EGL Engenharia</a></li>
				<li><a class="LnkLomandoAita" href="http://www.lomandoaita.com.br/" rel="external" title="Lomando Aita Edifica&ccedil;&otilde;es">Lomando Aita Edifica&ccedil;&otilde;es</a></li>
			</ul>-->
				<nav>
					<ul id="navigations" class="default">
						<li <?php if(@$page == "home"): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>" rel="index" title="Home">Home</a></li>
						<li <?php if(@$page == "imoveis" || @$page == "imovel" ): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>imoveis" title="Im&oacute;veis">Im&oacute;veis</a></li>
						<li <?php if(@$page == "empresa"): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>empresa" title="A Empresa">A Empresa</a></li>
						<li <?php if(@$page == "central-de-vendas"): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>central-de-vendas" title="Central de Vendas">Central de Vendas</a></li>
						<li <?php if(@$page == "noticias"): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>noticias" title="Not&iacute;cias">Not&iacute;cias</a></li>
						<li <?php if(@$page == "contato"): ?> class="Active" <?php endif; ?>><a href="<?=site_url()?>contato/fale-conosco" title="Contato">Contato</a></li>
						<li class="Exception"><a class="LnkBlog" href="http://www.nexgroup.com.br/blog/" title="Blog" target="_blank">Blog</a></li>
						<li class="Exception"><a class="LnkFacebook" href="http://www.facebook.com/pages/Nex-Group/110883575657465" title="Facebook" target="_blank">Facebook</a></li>
						<li class="Exception"><a class="LnkNaveguePeloMapa" href="<?=site_url()?>navegue-pelo-mapa" title="Navegue pelo Mapa">Navegue pelo Mapa</a></li>
					</ul>
				</nav>
		</div>
		<div id="sub-nav">
			<ul class="Wrapper" style="">
				<li class="Exception"><a class="LnkSubmenu1" href="<?=site_url()?>" rel="index" title="NEX GROUP">NEX GROUP</a></li>
				<li><a class="LnkSubmenu4 CorretorOnline"  href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online">Corretor Online</a></li>
				<li><a class="LnkSubmenu3 Atendimento"  href="<?=site_url()?>ligamos-para-voce" >Ligamos para voc&ecirc;</a></li>
				<li><a class="LnkSubmenu2 Atendimento"  href="<?=site_url()?>atendimento-email" >Atendimento por e-mail</a></li>
				<li>
					<form method="get" action="<?=base_url()?>buscar"  id="frm-search-simple2">
						<fieldset>
							<legend>Busca</legend>
							<label for="inp-search">Busca</label>
							<input class="Text" id="inp-search" name="q" required="required" type="text" value="Buscar" onfocus="if(this.value == 'Buscar') this.value=''" onblur="if(this.value == '') this.value = 'Buscar'" />
							<button class="BtnSearch" form="frm-search-simple" type="submit" >Buscar</button>
							<!--<a href="<?=site_url()?>busca-avancada" rel="search" title="Busca Avan&ccedil;ada?">Busca Avan&ccedil;ada?</a>-->
						</fieldset>
					</form>
				</li>
			</ul>
		</div>