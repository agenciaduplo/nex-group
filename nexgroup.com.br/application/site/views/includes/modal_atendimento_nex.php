<!-- MODAL CONTATO -->
<div id="area_atendimento_nex" style="display:none">
	<div class="black click_atendimento_nex"></div>
	<div class="row">

		<!-- DIV SUCESSO -->
		<div class="sucess">
			<div class="boxmat">
				<h2>Sua contato foi enviado com sucesso!</h2>
				<h3>Obrigado e em breve entraremos em contato com você!</h3>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="AtendimentoNex.close(1);">Enviar outra mensagem</a>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="AtendimentoNex.close(2);">fechar e continuar navegando</a>
			</div>
		</div>
		<!-- DIV SUCESSO END -->

		<a href="javascript:;" class="close-telefone click_atendimento_nex" rel="nofollow"><i class="icon-remove"></i></a>

		<h2 class="columns" id="titulo_contato">ATENDIMENTO NEX GROUP</h2>

		<img src="<?=base_url()?>assets/site/img/contato-interesse.png" alt="Contato Nex">

		<form name="formAtendimentoNex" id="formAtendimentoNex" method="post" action="" enctype="multipart/form-data" onsubmit="return false;">
			<?php
				if($this->agent->referrer())
				{
					$url = $this->agent->referrer();
				}
				else
				{
					$url = current_url();
					if($_SERVER['QUERY_STRING'])
					{
						$url .= '?' . $_SERVER['QUERY_STRING'];
					}
				}
			?>
			<input type="hidden" name="atendinex_refer" value="<?=$url?>" />
			<div class="left">
				<p style="font-style: italic;">
					Nosso horário de atendimento é de segunda à sexta, das 8h às 18h.
				</p> 
			</div>

			<div class="doble left horario">
				<label>Setor de atendimento:</label>
				<select name="atendinex_setor" id="atendinex_setor" style="margin: 2% 0 1.2em;" tabindex="4">
					<option value="">Selecione:</option>
					<option value="1">Seja um fornecedor</option>
					<option value="2">Venda seu terreno</option>
					<!--<option value="3">Trabalhe conosco</option>-->
					<option value="4">Sou corretor e quero vender Nex</option>
					<option value="5">Outros</option>
				</select>
			</div>

			<div class="doble right">
				<label>Nome: * <input type="text" name="atendinex_nome" placeholder="Digite seu nome aqui" tabindex="1" /></label>
			</div>

			<div style="clear:both;"></div>

			<div class="doble left">
				<label>E-mail: * <input type="text" name="atendinex_email" placeholder="Digite seu e-mail aqui" tabindex="2" /></label>
			</div>

			<div class="doble right ">
				<label>Telefone: * <input type="text" name="atendinex_telefone" class="mask-telefone" placeholder="Digite seu telefone aqui" tabindex="3" /></label>
			</div>

			<div style="clear:both;"></div>

			<div class="doble left">
				<label>	Mensagem: </label>
				
				<textarea name="atendinex_massege" id="atendinex_massege" placeholder="Digite sua mensagem aqui" tabindex="5" style="/*height: 75px;*/margin: 0.5em 0 -0.5em 0;"></textarea>

				<br style="clear: both;"/>

				<label for="atendinex_file" style="display: none;">Currículo: <span style="font-size: 0.9em;font-weight: normal;text-transform: none;">(.pdf, .doc ou .docx)</span> *<input type="file" name="atendinex_file" id="atendinex_file" /></label>
			</div>

			<div class="doble right radios">
				<label style="padding:0 1em 0 0">Contate-me por:</label>
				<input type="radio" name="atendinex_tipo" value="1" id="atendinex_tipo1" checked="checked" tabindex="6"><label for="atendinex_tipo1">E-mail</label>
				<input type="radio" name="atendinex_tipo" value="2" id="atendinex_tipo2" tabindex="7"><label for="atendinex_tipo2">Telefone</label>
			</div>

			<div class="doble right">
				<input type="checkbox" name="atendinex_newsletter" value="S" id="checkbox1" tabindex="8">
				<label class="check" for="checkbox1">Gostaria de receber e-mails informativos e promocionais da Nex Group.</label>

				<a href="javascript:;" id="btn-send1" class="btn large btn-red button left" rel="nofollow" onclick="AtendimentoNex.send();" tabindex="9">Enviar</a>
				<a href="javascript:;" id="btn-send2" class="btn large btn-red button left" rel="nofollow" onclick="AtendimentoNex.sendCurriculo();" tabindex="9" style="display: none;">Enviar</a>
			</div>
		</form>

	</div>
</div>

<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery-fileupload.js"></script>

<script type="text/javascript">

	$('#atendinex_setor','#formAtendimentoNex').change(function()
	{
		AtendimentoNex.changeSetor();
	});


	var files;
	 
	$('input[name=atendinex_file]','#formAtendimentoNex').on('change', prepareUpload);
	 
	function prepareUpload(event)
	{
	  files = event.target.files;
	}

	$('#formAtendimentoNex').fileUpload();

	var AtendimentoNex = {
		form: $('#formAtendimentoNex'),
		setor: '',
		nome: '',
		email: '',
		telefone: '',
		contato_tipo: '',
		mensagem: '',
		file: '',
		newsletter: '',
		refer: '',
		getFields: function()
		{
			this.setor     			= $('#atendinex_setor', this.form);
			this.nome     			= $('input[name=atendinex_nome]', this.form);
			this.email     			= $('input[name=atendinex_email]', this.form);
			this.telefone 			= $('input[name=atendinex_telefone]', this.form);
			this.contato_tipo 		= $('input[name=atendinex_tipo]:checked', this.form);
			this.mensagem 			= $('#atendinex_massege', this.form);
			this.file 				= $('input[name=atendinex_file]', this.form);
			this.newsletter 		= $('input[name=atendinex_newsletter]:checked', this.form);
			this.refer 				= $('input[name=atendinex_refer]', this.form);
		},
		validation: function()
		{
			if(this.setor.val() === "" || this.setor.val() == 0)
			{   
				this.showMassage("Por favor, selecione o SETOR DE ATENDIMENTO."); 
				this.setor.focus();
				return false;
			}

			if(this.nome.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo NOME corretamente."); 
				this.nome.focus();
				return false;
			}

			var rxEmail = /^.+@.+\..{2,}$/;
	        if(!rxEmail.test(this.email.val()))
	        {
	            this.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente."); 
				this.email.focus();
				return false;
	        }

			if(this.telefone.val() == "" || this.telefone.val() == "(__) ____-_____")
			{   
				this.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
				this.telefone.focus();
				return false;
			}

			if(this.setor.val() == 3)
			{
				if(this.file.val() == "")
				{   
					this.showMassage("Por favor, selecione seu arquivo do currículo para enviar."); 
					return false;
				}
			}

			return true;
		},
		showMassage: function(text)
		{
			alert(text);
		},
		btnText: function(text)
		{
			$('.btn', this.form).text(text);
		},
		changeSetor: function()
		{
			this.getFields();

			this.file.val(""); 

			if(this.setor.val() == 3)
			{
				$('#btn-send1', this.form).hide();
				$('#btn-send2', this.form).show();
				this.mensagem.animate({height: 75}, 400, function(){
					AtendimentoNex.file.parent().fadeIn(600);
				})
			}
			else
			{
				$('#btn-send1', this.form).show();
				$('#btn-send2', this.form).hide();
				if(this.mensagem.outerHeight() != 153)
				{
					this.file.parent().fadeOut(600, function(){
						AtendimentoNex.mensagem.animate({height: 153});
					});					
				}
			}
		},
		send: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('ENVIAR');
				return false;
			}

			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/atendimentoNexXHR",
				data: {
					setor: 				AtendimentoNex.setor.val(),
					nome: 				AtendimentoNex.nome.val(),
					email: 				AtendimentoNex.email.val(),
					telefone: 			AtendimentoNex.telefone.val(),
					contato_tipo: 		AtendimentoNex.contato_tipo.val(),
					mensagem: 			AtendimentoNex.mensagem.val(),
					newsletter: 		AtendimentoNex.newsletter.val(),
					refer: 				AtendimentoNex.refer.val()
				},
				dataType: "json",
				success: function(response)
				{
					AtendimentoNex.btnText('ENVIAR');

					if(response.erro == 1)
					{
						AtendimentoNex.showMassage("Por favor, preencha o campo NOME corretamente."); 
						AtendimentoNex.nome.focus();
						return false;
					}

					if(response.erro == 2)
					{
						AtendimentoNex.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente.");  
						AtendimentoNex.nome.focus();
						return false;
					}

					if(response.erro == 3)
					{
						AtendimentoNex.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
						AtendimentoNex.nome.focus();
						return false;
					}

					if(response.erro == 0 || response.erro == 101)
					{
						$('.sucess','#area_atendimento_nex').show();
						AtendimentoNex.form.get(0).reset();
						return true;
					}
				}
			});
		},
		sendCurriculo: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('ENVIAR');
				return false;
			}

			var data = new FormData();

			$.each(files, function(key, value)
			{
				data.append(key, value);
			});
    
			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/trabalheConoscoXHR?files",
				data: data,
				cache: false,
				dataType: "json",
				processData: false, // Não processar os arquivos
        		contentType: false, // Definir o tipo de conteúdo para false, como jQuery dirá ao servidor de seu pedido string de consulta
				success: function(data, textStatus, jqXHR)
				{
					AtendimentoNex.btnText('ENVIAR');

					if(typeof data.error === 'undefined' || data.error === 0 || data.error === 101)
					{
		        		AtendimentoNex.submitForm(data);
		        	}
		        	else if(data.error > 0 &&  data.error <= 100)
		        	{
		        		// Handle errors here
		        		alert(data.msg);
		        		console.log('ERROR: ' + data.error);
		        	}				
				},
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		        	// Handle errors here
		        	console.log('ERRORS: ' + textStatus);

		        	// STOP LOADING SPINNER
		        }
			});
		},
		submitForm: function(data)
		{
			var formData = this.form.serialize();
	
			$.each(data.files, function(key, value)
			{
				formData = formData + '&filenames[]=' + value;
			});

			formData = formData + '&id_curriculo=' + data.id_curriculo;

			$.ajax({
				url:  BASE_URL+"contato/trabalheConoscoXHR",
				type: 'POST',
				data: formData,
				cache: false,
				dataType: 'json',
				success: function(data, textStatus, jqXHR)
				{
					if(typeof data.error === 'undefined' || data.error === 0 || data.error === 101)
					{
		        		console.log('SUCCESS: ' + data.success);
		        		$('.sucess','#area_atendimento_nex').show();
						AtendimentoNex.form.get(0).reset();
		        	}
		        	else if(data.error > 0 &&  data.error <= 100)
		        	{
		        		// Handle errors here
		        		alert(data.msg);
		        		console.log('ERROR: ' + data.msg);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		        	// Handle errors here
		        	console.log('ERRORS: ' + textStatus);
		        },
		        complete: function()
		        {
		        	// STOP LOADING SPINNER
		        }
		    });
		},
		close: function(n)
		{
			if(n == 1)
			{
				$('.sucess','#area_atendimento_nex').hide();
			}
			else
			{
				$('.sucess','#area_atendimento_nex').hide();
				$('#area_atendimento_nex').hide();
			}
		}
	}
</script>