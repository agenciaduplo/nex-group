<!-- MODAL REVISTA -->
<div id="area_revista" style="display:none">
	<div class="black click_revista"></div>
	<div class="row">
		<a class="close-telefone click_revista"><i class="icon-remove"></i></a>
		<img src="<?=base_url().'uploads/revista/capa/'.$revistaAtual->capa?>" alt="">
		<div class="box">
			<h2>REVISTA NEXDAY</h2>
			<h3>Conheça a Nex Day</h3>
			<?php if($revistaAtual->pageflip != "") { ?><a href="<?=$revistaAtual->pageflip?>" class="btn btn-red button" target="_blank">LER REVISTA</a><?php } ?>
			<a href="http://issuu.com/nexday" class="btn btn-red button" target="_blank">Ver edições anteriores</a>
			<?php if($revistaAtual->arquivo != "" && file_exists('uploads/revista/pdf/'.$revistaAtual->arquivo)) { ?>
			<a href="<?=base_url().'uploads/revista/pdf/'.$revistaAtual->arquivo?>" class="btn btn-red button" target="_blank">Baixar arquivo PDF</a>
			<?php } ?>
		</div>
	</div>
</div>