<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title><?=@$title?></title>

    <!-- METAS -->
    <?php if(isset($pagetag) && $pagetag == 'imovelvisualizar') : ?>
			<meta property='og:locale' content='pt_BR' />
			<meta property='og:title' content='<?=@$title?>' />
			<meta property='og:image' content='http://www.nexgroup.com.br/uploads/imoveis/logo/<?=@$imovel->logotipo?>'/>
			
			<?php if(@$imovel->tags_description):?>
				<meta property='og:description' content='<?=@$imovel->tags_description?>'/>
			<?php else:?>
				<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
			<?php endif; ?>
			
			<meta property='og:url' content='<?=current_url()?>'/>

			<?php if(@$imovel->tags_description):?>
				<meta name="description" content="<?=@$imovel->tags_description?>" />
			<?php else:?>
				<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />	
			<?php endif;
			
			if(@$imovel->tags_keywords):?>
				<meta name="keywords" content="<?=@$imovel->tags_keywords?>" />
			<?php else:?>
				<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
			<?php endif;?>
	<?php else: ?>
			<?php if(@$description):?>
				<meta name="description" content="<?=$description?>" />
			<?php else: ?>
				<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
			<?php endif;?>
			<?php if(@$description):?>
				<meta name="keywords" content="<?=$keywords?>" />
			<?php else: ?>
				<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
			<?php endif;?>

			<meta property='og:locale' content='pt_BR' />
			<meta property='og:title' content='Nex Group' />
			<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
			<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
			<meta property='og:url' content='http://www.nexgroup.com.br'/>
    <?php endif;?>
    
    <!-- METAS -->
    <meta name="author" content="http://www.divex.com.br/" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    
    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />

    <?php if(@$canonical):?>
    <link rel="canonical" href="<?=$canonical?>" />
    <?php endif;?>
	
	<link rel="shortcut icon" href="<?=base_url()?>favicon.png" type="image/x-icon" />


    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,900,700italic' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" href="<?=base_url()?>assets/site/css/orbit-1.2.3.css"> <!-- ORBIT SLIDER -->
	
	<!-- <link rel="stylesheet" href="<?=base_url()?>assets/site/css/animate.css"> -->
	
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/styles.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/styles_adicional.css?anticache=4">

    <link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->
    

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA6XE_1wpfA5pgvS2V5xdyMKpqhbaNzuiY&libraries=places&sensor=false"></script>
    <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
    
    
    <script src="<?=base_url()?>assets/site/js/jquery-1.7.1.js"></script>
    

    <script>
    	BASE_URL = '<?=base_url()?>';
    </script>
    
    <!-- Analytics -->
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-42888838-1']);
	_gaq.push(['_trackPageview']);
	(function()
	{ var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
	)();
	</script>

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-41']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

	<!-- Fim Analytics -->
</head>    
<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
<body>      

	      <!-- LOADING 
	      <div class="loadmore"> 
		      <img src="<?=base_url()?>assets/site/img/5.gif" class="gif" alt="">
			-->

		<!-- LOAD ANTIGO -->
        <!-- <div id="load-site" class="loading">
            <img src="<?=base_url()?>assets/site/img/2.gif" class="gif" alt="">
        </div> -->
		 

		<!-- Facebook -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=266241050086130";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<!-- Facebook -->


        <!-- MENU OFFCANVAS -->
        <div id="menu" >
          <div class="black first"></div>
          <div class="content">
            <a class="first" >
              <span title="Clique para abrir o menu"> MENU </span>
              <i class="icon icon-remove"></i>
            </a>
            <ul>
              <li><a href="<?=site_url()?>"><i class="icon icon-home"></i> Home</a></li>
              <li><a href="<?=site_url()?>empresa"><i class="icon icon-heart"></i> A Empresa</a></li>
              <li><a href="<?=site_url()?>imoveis"><i class="icon icon-th-large"></i> Empreendimentos</a></li>
              <li><a href="<?=site_url()?>ofertas" style="line-height:1.2"><i class="icon icon-tags"></i> Classificados Nex Vendas</a></li>
              <li><a href="<?=site_url()?>noticias"><i class="icon icon-file-text"></i> Notícias</a></li>
              <li><a href="<?=site_url()?>contato"><i class="icon icon-envelope"></i> Contato</a></li>

              <li><a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank"><i class="icon icon-signin "></i> PORTAL DO CLIENTE</a></li>
              <li><a href="http://www.nexgroup.com.br/conexao/" target="_blank"><i class="icon icon-briefcase"></i>	CONEXÃO</a></li>
            </ul>
          </div>   
        </div>   