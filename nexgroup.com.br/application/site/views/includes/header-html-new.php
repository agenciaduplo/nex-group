<?php
	@session_start();
		
	if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
		
		if(isset($_GET['utm_campaign'])){
			$ref[] = $_GET['utm_campaign'];
		}
		
		if(isset($_GET['utm_source'])){
			$ref[] = $_GET['utm_source'];
		}
		
		if(isset($_GET['utm_medium'])){
			$ref[] = $_GET['utm_medium'];
		}
		
		$ref = implode('-', $ref);
		
	} else if(isset($_SESSION['origem'])){
		$ref = $_SESSION['origem'];
		
	}else if(isset($_SERVER['HTTP_REFERER'])) {
		
		$ref = @$_SERVER['HTTP_REFERER'];
		
		
	} else {
		$ref = 'direto';
	}
?>


<nav class="header-relative hide show-mobile show-tablet">
	<div class="left">
		<a class="first">
			<span title="Clique para abrir o menu"> MENU </span>
			<i class="icon icon-reorder"></i>
			<i class="icon icon-remove"></i>
		</a>
		
		<form id="formBusca" name="formBusca" class="formBusca" action="<?=base_url().'busca'?>" method="get">
			<input type="text" name="q" id="palavra-chave" value="" placeholder="Insira sua busca..." />
			<input type="submit" class="btn-busca" value="Buscar" />
		</form>
		
		<div class="btn-procurar">
			<a href="javascript:void(0);" class="search">
				<i class="icon icon-search"></i>
				<span style="margin-top: -5px">PROCURAR <br><i>no Site</i></span>
			</a>
		</div>
	</div>

	<a href="<?=base_url()?>" class="logo">
		<h1 class="title">Nex Group</h1>
	</a>

	<div class="right">
		<span class="pric">
			<a rel="contact" href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);">
				<i class="icon icon-comments"></i>
				<span style="margin-top: -5px">CORRETOR <br><i>Online</i></span>
			</a>
			<a data-tooltip class="click_fone" title="Ligamos para você" >
				<i class="icon icon-phone"></i>
			</a>
			<a href="<?=base_url()?>contato" title="Contato Nex">
				<i class="icon icon-envelope"></i>
			</a>
		</span>
	</div>
</nav>

<div id="ancora"></div>
<nav class="header-fixo absoluto clickroll">
  <div class="left">
	<a class="first">
	  <span title="Clique para abrir o menu"> MENU </span>
	  <i class="icon icon-reorder"></i>
	  <i class="icon icon-remove"></i>
	</a>
	<form id="formBuscaFixed" name="formBuscaFixed" class="formBusca" action="<?=base_url().'busca'?>" method="get">
		<input type="text" name="q" id="palavra-chave-fixed" value="" placeholder="Pesquisar no site" />
	  <input type="submit" class="btn-busca" value="Buscar" style="right: -65px;"/>
	</form>
	<div id="btn-procurar-fixed" class="btn-procurar">
		<a href="javascript:void(0);" class="search_fixed">
		  <i class="icon icon-search"></i>
		  <span style="margin-top: -5px">PROCURAR <br><i>no Sites</i></span>
		</a>
	</div>
  </div>

  <a href="<?=base_url()?>" class="logo">
	<h1 class="title">Nex Group</h1>
  </a>

  <div class="right">
	<span class="pric">
	  <a href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor : CORETOR_DEFAULT; ?>&midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);">
		<i class="icon icon-comments"></i>
		<span style="margin-top: -5px">CORRETOR <br><i>Online</i></span>
	  </a>
	  <a class="click_fone" title="Ligamos para você" >
		<i class="icon icon-phone"></i>
	  </a>
	  <a href="<?=base_url()?>contato" title="Contato Nex">
		<i class="icon icon-envelope"></i>
	  </a>
	</span>
  </div>
</nav>

<div id="ancora"></div>

<nav class="header-new parado fixo hide-mobile hide-tablet">
	<div class="container">
		<div class="left">
			<a href="<?=base_url()?>" class="logo">
				<h1 class="title">Nex Group</h1>
			</a>

			<div class="v-separator"></div>

			<ul class="left new-menu">
				<li class="left "><a href="<?=site_url()?>">HOME</a></li>
				<li class="left "><a href="<?=site_url()?>empresa">A EMPRESA</a></li>
				<li class="left "><a href="<?=site_url()?>imoveis">EMPREENDIMENTOS</a></li>
				<li class="left "><a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank">PORTAL DO CLIENTE</a></li>
			</ul>

			<div class="v-separator"></div>

			<div class="left">
				<form id="newFormBuscaFixed" name="formBuscaFixed" class="" action="<?=base_url().'busca'?>" method="get">
					<input type="text" name="q" id="palavra-chave-fixed" class="left input-text" value="" placeholder="Pesquise no site" />
					<a href="#" title="Contato Nex" class="left search-icon" onclick="$('#newFormBuscaFixed').submit();">
						<i class="icon icon-search"></i>
					</a>
				</form>
			</div>

			<div class="v-separator"></div>
		</div>

		<div class="right">

		<a href="<?=base_url()?>contato" title="CONTATO" class="tt">
			<i class="icon icon-envelope"></i>
		</a>

		<a href="<?=base_url()?>conexao" title="CONEXÃO" class="tt">
			<i class="icon icon-briefcase"></i>
		</a>
		</div>
	</div>
</nav>
	