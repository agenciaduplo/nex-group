<?php
	@session_start();
		
	if(isset($_GET['utm_campaign']) || isset($_GET['utm_source']) || isset($_GET['utm_medium'])){
		
		if(isset($_GET['utm_campaign'])){
			$ref[] = $_GET['utm_campaign'];
		}
		
		if(isset($_GET['utm_source'])){
			$ref[] = $_GET['utm_source'];
		}
		
		if(isset($_GET['utm_medium'])){
			$ref[] = $_GET['utm_medium'];
		}
		
		$ref = implode('-', $ref);
		
	} else if(isset($_SESSION['origem'])){
		$ref = $_SESSION['origem'];
		
	} else if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
		
		$ref = @$_SERVER['HTTP_REFERER'];
		
		
	} else {
		$ref = 'direto';
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
	<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	<meta name="author" content="http://www.divex.com.br/" />
	<meta name="language" content="pt-br" />
	<meta name="revisit-after" content="1 days" />
	<meta name="mssmarttagspreventparsing" content="true" />

	<link rel="canonical" href="http://www.nexgroup.com.br/" />

    <meta property='og:locale' content='pt_BR' />
    <meta property="og:type" content="website" />
	<meta property='og:title' content='Imóveis, casas e apartamentos você encontra em NEX GROUP' />
	<meta property='og:url' content='<?=base_url()?>'/>
	<meta property="og:site_name" content="NEX GROUP" />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>

	<!-- <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" /> old -->

	<!-- <meta name="google-site-verification" content="GQcYmmyE9jv44cPgV-pr-Jbe6uasH6j8CHKMFz9vY64" /> old 2 -->

	<meta name="google-site-verification" content="erk0frTQJtZQRmcRl_DvhbK3EtBGy3q7T6GUpQ3n6Cs" />
	
	<title>Imóveis, casas e apartamentos você encontra em NEX GROUP</title>

	<link rel="shortcut icon" href="<?=base_url()?>favicon.png" type="image/x-icon" />

	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,700,900,700italic' type='text/css'>
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700' type='text/css'>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.tooltipster/4.0.4/css/tooltipster.bundle.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.tooltipster/4.0.4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome2.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/styles.css?ac=1">


	<!--[if IE 7]>
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome-ie7.css">
	<![endif]-->

	<script src="<?=base_url()?>assets/site/js/jquery-1.7.2.min.js"></script>
	<!-- <script src="<?=base_url()?>assets/site/js/jquery-1.7.1.js"></script> -->

	<script>
	BASE_URL = '<?=base_url()?>';

	preLoadedObjects = ["<?=base_url().'assets/site/img/nex_2015.jpg'?>", "<?=base_url().'assets/site/img/nex.jpg'?>"];
	var currentImage = new Image();
	currentImage.src = preLoadedObjects[0];
	currentImage.src = preLoadedObjects[1];
	</script>

    <!-- Analytics -->

	<!-- PROPRIEDADE NEX@http://www.nexgroup.com.br -->
	<script>
	// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	// 	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	// 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	// ga('create', 'UA-42888838-1', 'auto');
	// ga('send', 'pageview');
	</script>


	<!-- PROPRIEDADE DIVEX@http://www.nexgroup.com.br -->
	<script>
	// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	// 	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	// 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	// ga('create', 'UA-1622695-41', 'auto');
	// ga('send', 'pageview');
	</script>

	<!-- PROPRIEDADE DIVEX@NEX -->
	<script>
	// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	// 	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	// 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	// ga('create', 'UA-1622695-74', 'auto');
	// ga('send', 'pageview');
	</script>

	<!-- Fim Analytics -->

	<script>
      var _prum = [['id', '540f0029abe53dd26eea5eb7'],
                   ['mark', 'firstbyte', (new Date()).getTime()]];
      (function() {
          var s = document.getElementsByTagName('script')[0]
            , p = document.createElement('script');
          p.async = 'async';
          p.src = '//rum-static.pingdom.net/prum.min.js';
          s.parentNode.insertBefore(p, s);
      })();
    </script>
</head>    
<body>

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NQ4BPL"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NQ4BPL');</script>
	<!-- End Google Tag Manager -->

	<!-- Facebook -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=266241050086130";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!-- Facebook -->

	<!-- CHAT -->
	<div class="chatbox hide-mobile">
		<iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=<?=isset($link_corretor) ? $link_corretor : 19; ?>&midia=<?=$ref; ?>"></iframe>
		<div class="toggle-chat themebg themecolor"><img width="30" class="left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2"><span>- FECHAR</span></div> 
	</div>
	
    <!-- MENU OFFCANVAS -->
	<div id="menu" >
		<div class="black first"></div>
		<div class="content">
			<a class="first" >
				<span title="Clique para abrir o menu"> MENU </span>
				<i class="icon icon-remove"></i>
			</a>
			<ul>
				<li><a href="<?=site_url()?>"><i class="icon icon-home"></i> Home</a></li>
				<li><a href="<?=site_url()?>empresa"><i class="icon icon-heart"></i> A Empresa</a></li>
				<li><a href="<?=site_url()?>imoveis"><i class="icon icon-th-large"></i> Empreendimentos</a></li>
				<!--<li><a href="<?=site_url()?>ofertas" style="line-height:1.2"><i class="icon icon-tags"></i> Classificados Nex Vendas</a></li>
				<li><a href="<?=site_url()?>noticias"><i class="icon icon-file-text"></i> Notícias</a></li>
				<li><a href="<?=site_url()?>contato"><i class="icon icon-envelope"></i> Contato</a></li>-->
				<li><a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank"><i class="icon icon-signin "></i> PORTAL DO CLIENTE</a></li>
				<!--<li><a href="http://www.nexgroup.com.br/conexao/" target="_blank"><i class="icon icon-briefcase" ></i> CONEXÃO</a></li>-->
			</ul>
		</div>   
	</div>   