
		<footer class="Main">
			<div class="Wrapper">
				<div class="Box1">
					<ul>
						<li class="Exception"><h1><a href="<?=site_url()?>" rel="index" title="NEX GROUP">NEX GROUP</a></h1></li>
						<li><a class="LnkCapaFoo" href="http://www.capa.com.br" rel="external" title="Grupo Capa">Capa</a></li>
						<li><a class="LnkDHZFoo" href="http://www.dhz.com.br" rel="external" title="DHZ Constru&ccedil;&otilde;es">DHZ Constru&ccedil;&otilde;es</a></li>
						<li><a class="LnkEGLFoo" href="http://www.eglengenharia.com.br" rel="external" title="EGL Engenharia">EGL Engenharia</a></li>
						<li><a class="LnkLomandoAitaFoo Exception" href="http://www.lomandoaita.com.br" rel="external" title="Lomando Aita Edifica&ccedil;&otilde;es">Lomando Aita Edifica&ccedil;&otilde;es</a></li>
					</ul>
					<ul class="addthis_toolbox addthis_default_style ">
						<!-- <li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_facebook_like" fb:like:layout="button_count"></a></li> -->
						<li><div class="fb-like" data-href="https://www.facebook.com/NexGroup" data-send="false" data-width="530" data-show-faces="true" data-font="arial"></div></li>
						<li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_tweet"></a></li>
						<li><g:plusone size="medium"></g:plusone></li>
						<!--<li><a class="addthis_counter addthis_pill_style"></a></li>-->
					</ul>
				</div>
				<div class="Box2">
					<ul>
						<li><a class="LnkAtendimentoEmail2 Atendimento"  href="<?=site_url()?>atendimento-email/<?=@$imovel->id_empreendimento?>" >Atendimento por e-mail</a></li>
						<li><a class="LnkAtendimentoTelefone2 Atendimento"  href="<?=site_url()?>ligamos-para-voce/<?=@$imovel->id_empreendimento?>" >Ligamos para voc&ecirc;</a></li>
						<?php $ses_url = @$_SESSION['url']; ?>
						<li><a class="LnkCorretorOnline Exception" href="javascript:window.open('<?=isset($link_corretor) ? $link_corretor."&midia=$ses_url" : CORETOR_DEFAULT."&midia=$ses_url"; ?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online">Corretor Online</a></li>
					</ul>
					<div class="Clear"></div>
					<ul class="Right">
						<li><a href="<?=site_url()?>contato/venda-seu-terreno" title="Venda seu terreno">Venda seu terreno</a></li>
						<li><a href="<?=site_url()?>contato/trabalhe-conosco" title="Trabalhe Conosco">Trabalhe conosco</a></li>
					</ul>
					<div class="Clear"></div>
					<div class="Left">
						<p>NEX GROUP</p>
						<p><b>(51) 3378.7800</b></p>
					</div>					
					<address class="Right">Furriel Luiz Antônio Vargas, 250<br/> 9° andar - CEP: 90470.130<br/> <span>Porto Alegre - RS - Brasil</span></address>
				</div>
				<a class="LnkDivexImob" href="http://www.divex.com.br/imobi/" target="_blank">Divex Imobi</a>
			</div>
			
			<div id="conversaoAdwords">

			</div>
			
		</footer>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="<?php echo base_url()?>assets/site/js/plugins/infobox.js" type="text/javascript" ></script>
		<!--SCRIPTS-->
		<script src="<?=base_url()?>assets/site/js/plugins/validator.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput-1.3.min.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.nivo.slider.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.carouFredSel-4.3.3-packed.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
		
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.placeholder.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/source/script.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/source/validations.js" type="text/javascript"></script>
		<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e33185829cf82a8"></script>
		
		<?php if (@$this->uri->segment(1) == 'empresa') { ?>
			<script src="<?=base_url()?>assets/site/js/source/empresa.js" type="text/javascript" ></script>
		<?php } ?>
		
		<?php if (@$this->uri->segment(1) == 'nao-encontrou-seu-imovel') { ?>
		
			<script>
			$(document).ready(function() 
			{		
				$("#NaoEncontrei").click();
			});	
			</script>
		
		<?php } ?>
		
		<?php if ($page=='navegue-pelo-mapa'){ ?>

			<script src="<?php echo base_url()?>assets/site/js/source/maps.js" type="text/javascript" ></script>
			<script type="text/javascript">initialize();</script>
			
		<?php }elseif ($page=='imovel' && @$imovel->latitude){?>
					
			<script src="<?php echo base_url()?>assets/site/js/source/maps2.js" type="text/javascript" ></script>
			<script type="text/javascript">initialize2(<?=$imovel->id_empreendimento?>);</script>
			
		<?php }elseif ($page=='noticias'){?>
						
			<script src="<?php echo base_url()?>assets/site/js/source/mapsHome.js" type="text/javascript" ></script>
			<script type="text/javascript">initialize();</script>
		
		<?php }?>
			
		<?php if(!$this->agent->is_browser('Chrome')){ ?>
			<script type="text/javascript">
				
				window.___gcfg = {lang: 'pt-BR'};
				(function() {
					var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
					po.src = 'https://apis.google.com/js/plusone.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				})();
				
			 </script>
		<?php } ?>
		
	
	<!-- Código do Google para tag de remarketing -->
	<!--------------------------------------------------
	As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 972557761;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

</body>
</html>