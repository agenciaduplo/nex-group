<!DOCTYPE html>
<html lang="pt-br">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
	<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
	<meta name="author" content="http://www.divex.com.br/" />
	<meta name="language" content="pt-br" />
	<meta name="revisit-after" content="1 days" />
	<meta name="mssmarttagspreventparsing" content="true" />

	<link rel="canonical" href="http://www.nexgroup.com.br/" />

    <meta property='og:locale' content='pt_BR' />
    <meta property="og:type" content="website" />
	<meta property='og:title' content='Imóveis, casas e apartamentos você encontra em NEX GROUP' />
	<meta property='og:url' content='<?=base_url()?>'/>
	<meta property="og:site_name" content="NEX GROUP" />
	<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
	<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>

	<!-- <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" /> -->

	<title>Imóveis, casas e apartamentos você encontra em NEX GROUP</title>

	<link rel="shortcut icon" href="<?=base_url()?>favicon.png" type="image/x-icon" />

	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,700,900,700italic' type='text/css'>
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700' type='text/css'>

	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome2.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/styles.css?ac=1">


	<!--[if IE 7]>
	<link rel="stylesheet" href="<?=base_url()?>assets/site/css/font-awesome/css/font-awesome-ie7.css">
	<![endif]-->

	<script src="<?=base_url()?>assets/site/js/jquery-1.7.2.min.js"></script>
	<!-- <script src="<?=base_url()?>assets/site/js/jquery-1.7.1.js"></script> -->

	<script>
	BASE_URL = '<?=base_url()?>';

	preLoadedObjects = ["<?=base_url().'assets/site/img/nex_2015.jpg'?>", "<?=base_url().'assets/site/img/nex.jpg'?>"];
	var currentImage = new Image();
	currentImage.src = preLoadedObjects[0];
	currentImage.src = preLoadedObjects[1];
	</script>

    <!-- Analytics -->
    
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-42888838-1']);
	_gaq.push(['_trackPageview']);
	(function()
	{ var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
	)();
	</script>
	

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-41']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

	<!-- Fim Analytics -->
</head>    
<body>      
	<!-- Facebook -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=266241050086130";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!-- Facebook -->

    <!-- MENU OFFCANVAS -->
	<div id="menu" >
		<div class="black first"></div>
		<div class="content">
			<a class="first" >
				<span title="Clique para abrir o menu"> MENU </span>
				<i class="icon icon-remove"></i>
			</a>
			<ul>
				<li><a href="<?=site_url()?>"><i class="icon icon-home"></i> Home</a></li>
				<li><a href="<?=site_url()?>empresa"><i class="icon icon-heart"></i> A Empresa</a></li>
				<li><a href="<?=site_url()?>imoveis"><i class="icon icon-th-large"></i> Empreendimentos</a></li>
				<li><a href="<?=site_url()?>ofertas" style="line-height:1.2"><i class="icon icon-tags"></i> Classificados Nex Vendas</a></li>
				<li><a href="<?=site_url()?>noticias"><i class="icon icon-file-text"></i> Notícias</a></li>
				<li><a href="<?=site_url()?>contato"><i class="icon icon-envelope"></i> Contato</a></li>
				<li><a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank"><i class="icon icon-signin "></i> PORTAL DO CLIENTE</a></li>
				<li><a href="http://www.nexgroup.com.br/conexao/" target="_blank"><i class="icon icon-briefcase" ></i> CONEXÃO</a></li>
			</ul>
		</div>   
	</div>   