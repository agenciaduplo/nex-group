<div id="footer">
	<div class="whit-box">
		<div class="row">
			<div class="one">
				<img src="<?=base_url()?>assets/site/img/logo2.png" alt="">
			</div> 
			<div class="two">
				<i class="icon icon-phone "></i>
				<div style="float: right;width: 80%;">
					<h4>Telefone Nex Group:</h4>
					<h5>(51) 3378.7800</h5>
					<h4 style="margin-top: 30px;">Comercial Nex Vendas:</h4>
					<h5>(51) 3092.3124</h5>
					<p style="float: none;">Ligue agora, estamos prontos para atendê-lo, ou <a href="javascript:;" class="click_fone" rel="nofollow">ligamos para você</a></p>
				</div>
			</div> 
			<div class="tree">
				<i class="icon icon-flag "></i>
				<h4>Endereço:</h4>
				<p>Furriel Luiz Antônio Vargas, 250<br>9° Andar - CEP: 90470.130<br>Porto Alegre - RS - Brasil</p>
			</div>
			<ul class="inline-list">
				<li><i class="icon icon-tag "></i><a href="http://www.nexgroup.com.br/nexchange" target="_blank">Nex Change</a></li>
				<li><i class="icon icon-briefcase "></i><a href="http://vagas.nex-group.infojobs.com.br/" target="_blank">TRABALHE CONOSCO</a></li>
				<li><i class="icon icon-facebook "></i><a href="https://www.facebook.com/NexGroup?fref=ts" target="_blank">nex group no facebook</a></li>
				<li class="last"><i class="icon icon-youtube "></i><a href="http://www.youtube.com/nexgrouptv" target="_blank">nex group no youTube</a></li>
			</ul>
		</div>
	</div>
	<div class="box">
		<div class="row">
			<div class="box-in one">
				<ul class="addthis_toolbox addthis_default_style ">
					<li><div class="fb-like" data-href="https://www.facebook.com/NexGroup" data-send="false" data-show-faces="true" data-font="arial"></div></li>
				</ul>
			</div>
			<div class="box-in">
				<i>
					A NEX GROUP pode ajudá-lo a encontrar o imóvel ideal, aquele que se encaixa no seu perfil,
					no tamanho da sua família e no tamanho dos seus sonhos.
					<a href="javascript:void(0);" class="click_contato_cliente" rel="nofollow">Cadastre-se</a> através do formulário de atendimento e aguarde nossa equipe entrar em contato.
				</i>
			</div>
		</div>
	</div>
	<div class="box-map">
		<div class="row">
			<nav class="mapasite">
                <h4>Sobre a Nex Group</h4>
                <a href="<?=base_url()?>empresa#ancora-apresentacao">Apresentação</a>
                <a href="<?=base_url()?>empresa#ancora-realizacoes">Realizações</a>
                <a href="<?=base_url()?>empresa#ancora-obras">Obras Realizadas</a>
                <a href="<?=base_url()?>empresa#ancora-responsabilidade">Responsabilidade</a>
                <a href="<?=base_url()?>empresa#ancora-gestao">Gestão Corporativa</a>
                <a href="<?=base_url()?>empresa#ancora-rh">Recursos Humanos</a>
              </nav>

              <nav class="mapasite">
                <h4>Fale com a Nex Group</h4>
                <a href="<?=base_url()?>contato/">Entre em Contato</a>
                <a href="<?=base_url()?>contato/">Informações sobre empreendimentos</a>
                <a href="<?=base_url()?>contato/">Trocar meu imóvel usado</a>
                <a href="<?=base_url()?>contato/">Conversar com o corretor</a>
                <a href="<?=base_url()?>contato/">Venda seu terreno</a>
                <a href="http://vagas.nex-group.infojobs.com.br/" target="_blank">Trabalhe Conosco</a>
                <a href="<?=base_url()?>contato/">Seja um fornecedor</a>
                <a href="<?=base_url()?>contato/">Sou corretor e quero vender Nex</a>
              </nav>

              <nav class="mapasite">
                <h4>Já sou Cliente</h4>
                <a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank">Portal do cliente</a>
                <a href="<?=base_url()?>contato/">Entre em Contato</a>
                <a href="<?=base_url()?>contato/">Atualizar seus cadastros</a>
                <a href="<?=base_url()?>contato/">Verificar informações financeiras</a>
                <a href="<?=base_url()?>contato/">2ª via de boletos</a>
              </nav>

              <nav class="mapasite">
                <h4>Informações</h4>
                <a href="<?=base_url()?>noticias/">Notícias</a>
                <a href="https://www.google.com/maps/dir//R.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil/@-30.0276634,-51.1828499,17z/data=!4m13!1m4!3m3!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2sR.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil!3b1!4m7!1m0!1m5!1m1!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2m2!1d-51.1828499!2d-30.0276634" target="_blank">Como chegar na Nex Group</a>
                <a href="<?=base_url()?>contato/">Elogios, sugestões e reclamações</a>
              </nav>
		</div>
	</div>
	<div class="row">
		<div class="assinatura">
			<p>Todos os direitos reservados&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;Todas as imagens são meramente ilustrativas&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;</p>
			<a href="http://imobi.divex.com.br/" targe="_blank">
				<img title="Conheça a Divex Imobi" src="<?=base_url()?>assets/site/img/divexlogo.png" width="95" alt="Logo Divex Imobi">
			</a>
		</div>
	</div>
</div>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA6XE_1wpfA5pgvS2V5xdyMKpqhbaNzuiY&libraries=places&sensor=false"></script>-->
<!-- <script type="text/javascript" src="<?=base_url()?>assets/site/js/plugins/infobox.js"></script>-->

<script type="text/javascript" src="<?=base_url()?>assets/site/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.touchSwipe.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/jquery.backgroundSize.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput.min.js"></script>
<!--<script type="text/javascript" src="<?=base_url()?>assets/site/js/google-maps.js?anticache=6"></script>-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.tooltipster/4.0.4/js/tooltipster.bundle.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/init-home.js?anticache=6"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/filters.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/site/js/funcoes.js?v=27"></script>

<!-- Código do Google para tag de remarketing -->
<!--
------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
-------------------------------------------------
-->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 972557761;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline; display:none">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>