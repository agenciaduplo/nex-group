<!-- MODAL CONTATO -->
<div id="area_interesse" style="display:none">
	<div class="black click_interesse"></div>

	<div class="row">

		<!-- DIV SUCESSO -->
		<div class="sucess">
			<div class="boxmat" style="text-align: center;">
				<h2>Sua contato foi enviado com sucesso!</h2>
				<h3>Obrigado e em breve entraremos em contato com você!</h3>
				<!-- <a class="btn large btn-black button">Enviar outra mensagem</a> -->
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" style="float: none;padding: 0.8em;" onclick="Interesse.close();">fechar e continuar navegando</a>
			</div>
		</div>
		<!-- DIV SUCESSO END -->

		<a class="close-telefone click_interesse"><i class="icon-remove"></i></a>
		
		<h2 class="columns" id="titulo_interesse">TENHO INTERESSE no <?=@$imovel->empreendimento?></h2>

		<img src="<?=base_url()?>assets/site/img/contato-comprar.png" alt="Contato Nex">

		<form name="formInteresse" id="formInteresse" method="post" action="" onsubmit="return false;">
			<?php
				if($this->agent->referrer())
				{
					$url = $this->agent->referrer();
				}
				else
				{
					$url = current_url();
					if($_SERVER['QUERY_STRING'])
					{
						$url .= '?' . $_SERVER['QUERY_STRING'];
					}
				}
			?>
			<input type="hidden" name="interesse_refer" value="<?=$url?>" />
			<input type="hidden" name="interesse_id_empreedimento" value="<?=@$imovel->id_empreendimento?>" />

			<div class="left">
				<p style="font-style: italic;">
					Nosso horário de atendimento é de segunda à sexta, das 8h às 18h.
				</p> 
			</div>

			<div class="doble left">
				<label>Nome: * <input type="text" name="interesse_nome" placeholder="Digite seu nome aqui" tabindex="1" /></label>
			</div>

			<div class="doble right">
				<label>E-mail: * <input type="text" name="interesse_email" placeholder="Digite seu e-mail aqui" tabindex="2" /></label>
			</div>

			<div class="doble left">
				<label>Telefone: * <input type="text" name="interesse_telefone" class="mask-telefone" placeholder="Digite seu telefone aqui" tabindex="3" /></label>
			</div>

			<div class="doble right radios">
				<label style="padding:0 1em 0 0">Contate-me por:</label>
				<input type="radio" name="interesse_tipo" value="1" id="interesse_tipo1" checked="checked" tabindex="4"><label for="interesse_tipo1">E-mail</label>
				<input type="radio" name="interesse_tipo" value="2" id="interesse_tipo2" tabindex="5"><label for="interesse_tipo2">Telefone</label>
			</div>

			<div class="doble left">
				<label>Mensagem: <textarea name="interesse_massege" id="interesse_massege" placeholder="Digite sua mensagem aqui" tabindex="6"></textarea></label>
			</div>

			<div class="doble right">
				<input id="checkbox1" type="checkbox" name="interesse_newsletter" value="S" tabindex="7">
				<label class="check" for="checkbox1">Gostaria de receber e-mails informativos e promocionais da Nex Group.</label>
				
				<a href="javascript:;" class="btn large btn-red button left" rel="nofollow" onclick="Interesse.send();" tabindex="8">Enviar mensagem</a>
			</div>
		</form>

	</div>
</div>

<script type="text/javascript">
	var Interesse = {
		form: $('#formInteresse'),
		nome: '',
		email: '',
		telefone: '',
		contato_tipo: '',
		id_empreendimento: '',
		mensagem: '',
		newsletter: '',
		refer: '',
		getFields: function()
		{
			this.nome     			= $('input[name=interesse_nome]', this.form);
			this.email     			= $('input[name=interesse_email]', this.form);
			this.telefone 			= $('input[name=interesse_telefone]', this.form);
			this.contato_tipo 		= $('input[name=interesse_tipo]:checked', this.form);
			this.id_empreendimento 	= $('input[name=interesse_id_empreedimento]', this.form);
			this.mensagem 			= $('#interesse_massege', this.form);
			this.newsletter 		= $('input[name=interesse_newsletter]:checked', this.form);
			this.refer 				= $('input[name=interesse_refer]', this.form);
		},
		validation: function()
		{
			if(this.nome.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo NOME corretamente."); 
				this.nome.focus();
				return false;
			}

			var rxEmail = /^.+@.+\..{2,}$/;
	        if(!rxEmail.test(this.email.val()))
	        {
	            this.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente."); 
				this.email.focus();
				return false;
	        }

			if(this.telefone.val() == "" || this.telefone.val() == "(__) ____-_____")
			{   
				this.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
				this.telefone.focus();
				return false;
			}

			return true;
		},
		showMassage: function(text)
		{
			alert(text);
		},
		btnText: function(text)
		{
			$('.btn', this.form).text(text);
		},
		send: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('Enviar mensagem');
				return false;
			}

			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/interesseNoEmpreendimentoXHR",
				data: {
					nome: 				Interesse.nome.val(),
					email: 				Interesse.email.val(),
					telefone: 			Interesse.telefone.val(),
					contato_tipo: 		Interesse.contato_tipo.val(),
					id_empreendimento: 	Interesse.id_empreendimento.val(),
					mensagem: 			Interesse.mensagem.val(),
					newsletter: 		Interesse.newsletter.val(),
					refer: 				Interesse.refer.val()
				},
				dataType: "json",
				success: function(response)
				{
					Interesse.btnText('ENVIAR');

					if(response.erro == 1)
					{
						Interesse.showMassage("Por favor, preencha o campo NOME corretamente."); 
						Interesse.nome.focus();
						return false;
					}

					if(response.erro == 2)
					{
						Interesse.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente.");  
						Interesse.nome.focus();
						return false;
					}

					if(response.erro == 3)
					{
						Interesse.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
						Interesse.nome.focus();
						return false;
					}

					if(response.erro == 0 || response.erro == 101)
					{
						$('.sucess','#area_interesse').show();
						Interesse.form.get(0).reset();
						return true;
					}
				}
			});
		},
		close: function(n)
		{
			if(n == 1)
			{
				$('.sucess','#area_interesse').hide();
			}
			else
			{
				$('.sucess','#area_interesse').hide();
				$('#area_interesse').hide();
			}
		}
	} // object
</script>