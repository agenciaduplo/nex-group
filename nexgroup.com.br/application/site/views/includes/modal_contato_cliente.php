<!-- MODAL CONTATO -->
<div id="area_contato_cliente" style="display:none">
	<div class="black click_contato_cliente"></div>
	<div class="row">

		<!-- DIV SUCESSO -->
		<div class="sucess">
			<div class="boxmat">
				<h2>Sua contato foi enviado com sucesso!</h2>
				<h3>Obrigado e em breve entraremos em contato com você!</h3>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="ContatoCliente.close(1);">Enviar outra mensagem</a>
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" onclick="ContatoCliente.close(2);">fechar e continuar navegando</a>
			</div>
		</div>
		<!-- DIV SUCESSO END -->

		<a href="javascript:;" class="close-telefone click_contato_cliente" rel="nofollow"><i class="icon-remove"></i></a>

		<h2 class="columns" id="titulo_contato">ATENDIMENTO CLIENTE NEX</h2>

		<img src="<?=base_url()?>assets/site/img/contato-info.png" alt="Contato Nex">

		<form name="formContatoCliente" id="formContatoCliente" method="post" action="" onsubmit="return false;">
			<?php
				if($this->agent->referrer())
				{
					$url = $this->agent->referrer();
				}
				else
				{
					$url = current_url();
					if($_SERVER['QUERY_STRING'])
					{
						$url .= '?' . $_SERVER['QUERY_STRING'];
					}
				}
			?>
			<input type="hidden" name="contatocli_refer" value="<?=$url?>" />
			<div class="left">
				<p style="font-style: italic;">
					Nosso horário de atendimento é de segunda à sexta, das 8h às 18h.
				</p> 
			</div>

			<div class="doble left horario">
				<label>Setor de atendimento:</label>
				<select name="contatocli_setor" id="contatocli_setor" style="margin: 2% 0 1.2em;" tabindex="4">
					<option value="">Selecione:</option>
					<option value="1">Dados cadastrais</option>
					<option value="2">Informações financeiras</option>
					<option value="3">Pagamentos e 2° via de boletos</option>
					<option value="4">Elogios, sugestões e reclamações</option>
					<option value="0">Outros</option>
				</select>
			</div>

			<div class="doble right">
				<label>Nome: * <input type="text" name="contatocli_nome" placeholder="Digite seu nome aqui" tabindex="1" /></label>
			</div>

			<div style="clear:both;"></div>

			<div class="doble left">
				<label>E-mail: * <input type="text" name="contatocli_email" placeholder="Digite seu e-mail aqui" tabindex="2" /></label>
			</div>

			<div class="doble right ">
				<label>Telefone: * <input type="text" name="contatocli_telefone" class="mask-telefone" placeholder="Digite seu telefone aqui" tabindex="3" /></label>
			</div>

			<div style="clear:both;"></div>

			<div class="doble left">
				<label>	Mensagem: </label>
				
				<textarea name="contatocli_massege" id="contatocli_massege" placeholder="Digite sua mensagem aqui" tabindex="5" style="/*height: 75px;*/margin: 0.5em 0 -0.5em 0;"></textarea>

				<br style="clear: both;"/>
			</div>

			<div class="doble right radios">
				<label style="padding:0 1em 0 0">Contate-me por:</label>
				<input type="radio" name="contatocli_tipo" value="1" id="contatocli_tipo1" checked="checked" tabindex="6"><label for="contatocli_tipo1">E-mail</label>
				<input type="radio" name="contatocli_tipo" value="2" id="contatocli_tipo2" tabindex="7"><label for="contatocli_tipo2">Telefone</label>
			</div>

			<div class="doble right">
				<input type="checkbox" name="contatocli_newsletter" value="S" id="checkbox1" tabindex="8">
				<label class="check" for="checkbox1">Gostaria de receber e-mails informativos e promocionais da Nex Group.</label>

				<a href="javascript:;" id="btn-send1" class="btn large btn-red button left" rel="nofollow" onclick="ContatoCliente.send();" tabindex="9">Enviar</a>
			</div>
		</form>

	</div>
</div>

<script type="text/javascript">

	var ContatoCliente = {
		form: $('#formContatoCliente'),
		setor: '',
		nome: '',
		email: '',
		telefone: '',
		contato_tipo: '',
		mensagem: '',
		newsletter: '',
		refer: '',
		getFields: function()
		{
			this.setor     			= $('#contatocli_setor', this.form);
			this.nome     			= $('input[name=contatocli_nome]', this.form);
			this.email     			= $('input[name=contatocli_email]', this.form);
			this.telefone 			= $('input[name=contatocli_telefone]', this.form);
			this.contato_tipo 		= $('input[name=contatocli_tipo]:checked', this.form);
			this.mensagem 			= $('#contatocli_massege', this.form);
			this.newsletter 		= $('input[name=contatocli_newsletter]:checked', this.form);
			this.refer 				= $('input[name=contatocli_refer]', this.form);
		},
		validation: function()
		{
			if(this.setor.val() == "")
			{   
				this.showMassage("Por favor, selecione o SETOR DE ATENDIMENTO."); 
				this.setor.focus();
				return false;
			}

			if(this.nome.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo NOME corretamente."); 
				this.nome.focus();
				return false;
			}

			var rxEmail = /^.+@.+\..{2,}$/;
	        if(!rxEmail.test(this.email.val()))
	        {
	            this.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente."); 
				this.email.focus();
				return false;
	        }

			if(this.telefone.val() == "" || this.telefone.val() == "(__) ____-_____")
			{   
				this.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
				this.telefone.focus();
				return false;
			}

			return true;
		},
		showMassage: function(text)
		{
			alert(text);
		},
		btnText: function(text)
		{
			$('.btn', this.form).text(text);
		},
		send: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('ENVIAR');
				return false;
			}

			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/ContatoClienteXHR",
				data: {
					setor: 				ContatoCliente.setor.val(),
					nome: 				ContatoCliente.nome.val(),
					email: 				ContatoCliente.email.val(),
					telefone: 			ContatoCliente.telefone.val(),
					contato_tipo: 		ContatoCliente.contato_tipo.val(),
					mensagem: 			ContatoCliente.mensagem.val(),
					newsletter: 		ContatoCliente.newsletter.val(),
					refer: 				ContatoCliente.refer.val()
				},
				dataType: "json",
				success: function(response)
				{
					ContatoCliente.btnText('ENVIAR');

					if(response.erro == 1)
					{
						ContatoCliente.showMassage("Por favor, preencha o campo NOME corretamente."); 
						ContatoCliente.nome.focus();
						return false;
					}

					if(response.erro == 2)
					{
						ContatoCliente.showMassage("Email inválido! Por favor, digite seu EMAIL corretamente.");  
						ContatoCliente.nome.focus();
						return false;
					}

					if(response.erro == 3)
					{
						ContatoCliente.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
						ContatoCliente.nome.focus();
						return false;
					}

					if(response.erro == 0 || response.erro == 101)
					{
						$('.sucess','#area_contato_cliente').show();
						ContatoCliente.form.get(0).reset();
						return true;
					}
				}
			});
		},
		close: function(n)
		{
			if(n == 1)
			{
				$('.sucess','#area_contato_cliente').hide();
			}
			else
			{
				$('.sucess','#area_contato_cliente').hide();
				$('#area_contato_cliente').hide();
			}
		}
	}
</script>