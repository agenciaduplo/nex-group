<!-- TELEFONE -->
<div id="area_fone" style="display:none">
	<div class="black"></div>
	<div class="row">

		<!-- DIV SUCESSO -->
		<div class="sucess">
			<div class="boxmat" style="text-align: center;">
				<h2>Sua contato foi enviado com sucesso!</h2>
				<h3>Obrigado e em breve entraremos em contato com você!</h3>
				<!-- <a class="btn large btn-black button">Enviar outra mensagem</a> -->
				<a href="javascript:;" class="btn large btn-black button" rel="nofollow" style="float: none;padding: 0.8em;" onclick="LigamosParaVoce.close();">fechar e continuar navegando</a>
			</div>
		</div>
		<!-- DIV SUCESSO END -->

		<a href="javascript:;" class="close-telefone click_fone" rel="nofollow"><i class="icon-remove"></i></a>
		
		<h2 class="columns">Ligamos para você</h2>
   <!-- <h3>Nosso horário de atendimento é de segunda à sexta, das 8h às 18h.</h3> -->

		<img src="<?=base_url()?>assets/site/img/contato-comprar.png" alt="">

		<form name="formLigamos" id="formLigamos" method="post" action="" onsubmit="return false;">
			<?php
				if($this->agent->referrer())
				{
					$url = $this->agent->referrer();
				}
				else
				{
					$url = current_url();
					if($_SERVER['QUERY_STRING'])
					{
						$url .= '?' . $_SERVER['QUERY_STRING'];
					}
				}
			?>
			<input type="hidden" name="lig_refer" value="<?=$url?>" />
			<div class="doble left">
				
				<label for="lig_nome">Nome: <input type="text" name="lig_nome" id="lig_nome" placeholder="Digite seu nome aqui" tabindex="1" /></label>

				<label style="padding:0 1em 0 0">Contate-me:</label>
				<input type="radio" name="lig_contate_me" value="1" id="lig_contate_me1" checked="checked" tabindex="3"><label for="lig_horario1"><span>Agora</span></label>
				<input type="radio" name="lig_contate_me" value="2" id="lig_contate_me2" tabindex="4"><label for="lig_horario2"><span>Outro horário</span></label>
				
				<div class="hideHorario horario" id="HorarioElements">
					
					<select name="lig_horario" id="lig_horario" class="validate[required] horariosSelect" tabindex="5" disabled="disabled">
						<option value="">Selecione horário de preferência: *</option>
						<option value="das 08:00 às 09:00">das 08:00 às 09:00</option>
						<option value="das 09:00 às 10:00">das 09:00 às 10:00</option>
						<option value="das 10:00 às 11:00">das 10:00 às 11:00</option>
						<option value="das 11:00 às 12:00">das 11:00 às 12:00</option>
						<option value="das 12:00 às 13:00">das 12:00 às 13:00</option>
						<option value="das 13:00 às 14:00">das 13:00 às 14:00</option>
						<option value="das 14:00 às 15:00">das 14:00 às 15:00</option>
						<option value="das 15:00 às 16:00">das 15:00 às 16:00</option>
						<option value="das 16:00 às 17:00">das 16:00 às 17:00</option>
						<option value="das 17:00 às 18:00">das 17:00 às 18:00</option>
						<option value="das 18:00 às 19:00">das 18:00 às 19:00</option>
						<option value="das 19:00 às 20:00">das 19:00 às 20:00</option>
						<option value="das 20:00 às 21:00">das 20:00 às 21:00</option>
					</select>

				</div>
			</div>
			<div class="doble right">
				
				<label for="lig_telefone">Telefone: <input type="text" name="lig_telefone" id="lig_telefone" class="mask-telefone" placeholder="Digite seu telefone aqui" tabindex="2" /></label>

				<a href="javascript:;" class="btn large btn-red button left" rel="nofollow" onclick="LigamosParaVoce.send();" tabindex="6">Enviar</a>

			</div>
		</form>

		<div class="left" style="width:100%" >
			<div class="triple first">
				<i class="icon icon-phone"></i>
				<span class="left">
					<h2>Telefone Nex Group:</h2>
					<h3>(51) 3378.7800</h3>
					<p>Nosso horário de atendimento é de segunda à sexta, das 8h às 18h</p>
				</span>
			</div> 
			<div class="triple">
				<i class="icon icon-phone"></i>
				<span class="left">
					<h2>Comercial Nex Vendas:</h2>
					<h3>(51) 3092.3124</h3>
					<p>Nosso horário de atendimento é de domingo à sábado, das 8h às 22h</p>
				</span>
			</div> 
			<div class="triple" style="display:none">
				<i class="icon whatsapp"><img src="<?=base_url()?>assets/site/img/whatsapp.png" alt="icon whatsapp"></i>
				<span class="left">
					<h2>Whats App da Nex Group:</h2>
					<h3>(51) 0000.0000</h3>
					<p>niet illum fugit ad accusantium earum ut deserunt cumque sit sapiente sequi? Debitis, non.</p>
				</span>
			</div> 
		</div> 

    </div>  
</div>

<script type="text/javascript">

	$('input[name=lig_contate_me]','#formLigamos').change(function()
	{
		if($(this).val() == '2')
		{
			$('#lig_horario','#formLigamos').removeAttr('disabled');
		}
		else
		{
			$('#lig_horario','#formLigamos').attr('disabled','disabled');
		}
	})

	var LigamosParaVoce = {
		form: $('#formLigamos'),
		nome: '',
		contate_me: '',
		telefone: '',
		horario: '',
		refer: '',
		getFields: function()
		{
			this.nome     	= $('input[name=lig_nome]', this.form);
			this.contate_me = $('input[name=lig_contate_me]:checked', this.form);
			this.telefone 	= $('input[name=lig_telefone]', this.form);
			this.horario 	= $('#lig_horario', this.form);
			this.refer 		= $('input[name=lig_refer]', this.form);
		},
		validation: function()
		{
			if(this.nome.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo NOME corretamente."); 
				this.nome.focus();
				return false;
			}

			if(this.telefone.val() == "")
			{   
				this.showMassage("Por favor, preencha o campo TELEFONE corretamente."); 
				this.telefone.focus();
				return false;
			}

			if(this.contate_me.val() == 2 && this.horario.val() == "")
			{
				this.showMassage("Por favor, selecione o horário de sua preferência."); 
				return false;
			}

			return true;
		},
		showMassage: function(text)
		{
			alert(text);
		},
		btnText: function(text)
		{
			$('.btn', this.form).text(text);
		},
		send: function()
		{
			this.btnText('ENVIANDO...');

			this.getFields();

			if(!this.validation())
			{
				this.btnText('ENVIAR');
				return false;
			}

			$.ajax({
				type: "POST",
				url: BASE_URL+"contato/ligamosParaVoceXHR",
				data: {
					nome: 		LigamosParaVoce.nome.val(),
					contate_me: LigamosParaVoce.contate_me.val(),
					telefone: 	LigamosParaVoce.telefone.val(),
					horario: 	LigamosParaVoce.horario.val(),
					refer: 		LigamosParaVoce.refer.val()
				},
				dataType: "json",
				success: function(response)
				{
					LigamosParaVoce.btnText('ENVIAR');

					if(response.erro == 1)
					{
						this.showMassage("Por favor, preencha o campo NOME corretamente."); 
						this.nome.focus();
						return false;
					}

					if(response.erro == 0 || response.erro == 101)
					{
						$('.sucess','#area_fone').show();
						LigamosParaVoce.form.get(0).reset();
						return true;
					}
				}
			});
		}, // send()
		close: function()
		{
			$('.sucess','#area_fone').hide();
			$('#area_fone').hide();
		}
	} // object


</script>