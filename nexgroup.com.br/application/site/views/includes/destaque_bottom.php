		<!-- DESTAQUES BOTTOM -->
        <section id="desk-bottom-container">

          <!-- ANTIGO COM MODAL

          <a class="desk-bottom small-4 click_tv">
            <div class="bg-pix"></div>
            <div class="black"></div>
            <div class="bg one"></div>
            <span>
              <h2>NEX TV</h2>
              <h3><i>Confira o canal online da Nex</i></h3>
            </span>
          </a>

          -->

          <a class="desk-bottom small-4" href="https://www.youtube.com/channel/UCKmLkk5EC927QPml79HPIUg" target="_blank">
            <div class="bg-pix"></div>
            <div class="black"></div>
            <div class="bg one"></div>
            <span>
              <h2>NEX TV</h2>
              <h3><i>Confira o canal online da Nex</i></h3>
            </span>
          </a>


          <a class="desk-bottom small-4 click_revista">
            <div class="bg-pix"></div>
            <div class="black"></div>
            <div class="bg two" style="background-image:url(<?=base_url().'uploads/revista/capa/'.@$revistaAtual->capa?>); "></div>
            <span>
              <h2><?php echo @$revistaAtual->titulo; ?></h2>
              <h3><i><?php echo character_limiter(strip_tags($revistaAtual->descricao), 80); ?></i></h3>
            </span>
          </a>
          
          <a class="desk-bottom small-4" href="https://www.facebook.com/NexGroup?fref=ts" target="_blank">
            <div class="bg-pix"></div>
            <div class="black"></div>
            <div class="bg tree"></div>
            <span>
              <h2>Fan Page da NEX</h2>
              <h3><i>A vida da Nex está aqui</i></h3>
            </span>
          </a>
        </section>