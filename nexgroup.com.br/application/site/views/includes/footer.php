        <?php $this->load->view("includes/chamada_newsletter", true); ?>

<!-- FOOTER -->
        <div id="footer">
          <div class="whit-box">

            <div class="row">
              <div class="one">
                <img src="<?=base_url()?>assets/site/img/logo2.png" alt="">
              </div> 

              <div class="two">
                <i class="icon icon-phone "></i>
                <div style="float: right;width: 80%;">
                  <h4>Telefone Nex Group:</h4>
                  <h5>(51) 3378.7800</h5>
                  <h4 style="margin-top: 30px;">Comercial Nex Vendas:</h4>
                  <h5>(51) 3092.3124</h5>
                  <p style="float: none;">
                    Ligue agora, estamos prontos para atendê-lo,
                    ou <a href="javascript:;" class="click_fone" rel="nofollow">ligamos para você</a>
                  </p>
                </div>
       
              </div> 
              <div class="tree">
                <i class="icon icon-flag "></i>
                <h4>Endereço:</h4>
                <p>
                  Furriel Luiz Antônio Vargas, 250<br>
                  9° Andar - CEP: 90470.130<br>
                  Porto Alegre - RS - Brasil
                </p>
              </div>

              <ul class="inline-list">
                <li><i class="icon icon-tag "></i><a href="http://www.nexgroup.com.br/nexchange" target="_blank">Nex Change</a></li>
                <li><i class="icon icon-briefcase "></i><a href="<?php echo site_url(); ?>contato">TRABALHE CONOSCO</a></li>
                <li><i class="icon icon-facebook "></i><a href="https://www.facebook.com/NexGroup?fref=ts" target="_blank">nex group no facebook</a></li>
                <li class="last"><i class="icon icon-youtube "></i><a href="http://www.youtube.com/nexgrouptv" target="_blank">nex group no youTube</a></li>
              </ul>
            </div>
          </div>



          <div class="box">
            <div class="row">
                <div class="box-in one">
                  <ul class="addthis_toolbox addthis_default_style ">
                    <!-- <li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_facebook_like" fb:like:layout="button_count"></a></li> -->
                    <li><div class="fb-like" data-href="https://www.facebook.com/NexGroup" data-send="false" data-show-faces="true" data-font="arial"></div></li>
                    <li><a addthis:url="<?=site_url()?>" addthis:title="NEX GROUP" class="addthis_button_tweet"></a></li>
                    <li><g:plusone size="medium"></g:plusone></li>
                    <!--<li><a class="addthis_counter addthis_pill_style"></a></li>-->
                  </ul>
                </div>
                <div class="box-in">
                  <i>A NEX GROUP pode ajudá-lo a encontrar o imóvel ideal, aquele que se encaixa no seu perfil, no tamanho da sua família e no tamanho dos seus sonhos. <a class="click_contato">Cadastre-se</a> através do formulário de atendimento e aguarde nossa equipe entrar em contato.</i>




                </div>
            </div> 
          </div> 

          <div class="box-map">
            <div class="row">

              <nav class="mapasite">
                <h4>Sobre a Nex Group</h4>
                <a href="<?=base_url()?>empresa#ancora-apresentacao">Apresentação</a>
                <a href="<?=base_url()?>empresa#ancora-realizacoes">Realizações</a>
                <a href="<?=base_url()?>empresa#ancora-obras">Obras Realizadas</a>
                <a href="<?=base_url()?>empresa#ancora-responsabilidade">Responsabilidade</a>
                <a href="<?=base_url()?>empresa#ancora-gestao">Gestão Corporativa</a>
                <a href="<?=base_url()?>empresa#ancora-rh">Recursos Humanos</a>
              </nav>

              <nav class="mapasite">
                <h4>Fale com a Nex Group</h4>
                <a href="<?=base_url()?>contato/">Entre em Contato</a>
                <a href="<?=base_url()?>contato/">Informações sobre empreendimentos</a>
                <a href="<?=base_url()?>contato/">Trocar meu imóvel usado</a>
                <a href="<?=base_url()?>contato/">Conversar com o corretor</a>
                <a href="<?=base_url()?>contato/">Venda seu terreno</a>
                <a href="http://vagas.nex-group.infojobs.com.br/" target="_blank">Trabalhe Conosco</a>
                <a href="<?=base_url()?>contato/">Seja um fornecedor</a>
                <a href="<?=base_url()?>contato/">Sou corretor e quero vender Nex</a>
              </nav>

              <nav class="mapasite">
                <h4>Já sou Cliente</h4>
                <a href="http://nexgroup.portalcliente.sienge.com.br:8093/PortalCliente/" target="_blank">Portal do cliente</a>
                <a href="<?=base_url()?>contato/">Entre em Contato</a>
                <a href="<?=base_url()?>contato/">Atualizar seus cadastros</a>
                <a href="<?=base_url()?>contato/">Verificar informações financeiras</a>
                <a href="<?=base_url()?>contato/">2ª via de boletos</a>
              </nav>

              <nav class="mapasite">
                <h4>Informações</h4>
                <a href="<?=base_url()?>noticias/">Notícias</a>
                <a href="https://www.google.com/maps/dir//R.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil/@-30.0276634,-51.1828499,17z/data=!4m13!1m4!3m3!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2sR.+Furriel+Lu%C3%ADz+Ant%C3%B4nio+de+Vargas,+250+-+Bela+Vista,+Porto+Alegre+-+RS,+90470-130,+Rep%C3%BAblica+Federativa+do+Brasil!3b1!4m7!1m0!1m5!1m1!1s0x9519782ba7c269ab:0x8f8f1213cd70e67b!2m2!1d-51.1828499!2d-30.0276634" target="_blank">Como chegar na Nex Group</a>
                <a href="<?=base_url()?>contato/">Elogios, sugestões e reclamações</a>
              </nav>

          </div>
        </div>



          <div class="row">
            <div class="assinatura">
              <p>Todos os direitos reservados&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;Todas as imagens são meramente ilustrativas&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;</p>
              <a href="http://imobi.divex.com.br/" targe="_blank">
                <img title="Conheça a Divex Imobi" src="<?=base_url()?>assets/site/img/divexlogo.png" width="95" alt="Logo Divex Imobi">
              </a>
            </div>
          </div>
        </div>
  

   <!-- </div> LOADING ENDS -->

   <!-- <script src="<?=base_url()?>assets/site/js/jquery.simplr.smoothscroll.js" type="text/javascript" charset="utf-8"></script>

    <script src="<?=base_url()?>assets/site/js/demo1.js" type="text/javascript" charset="utf-8"></script>-->

	<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    
    <!-- optionally include helper plugins -->
    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.mousewheel.min.js"></script>
    <!-- <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.touchSwipe.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.touchSwipe.js"></script>
    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.transit.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?=base_url()?>assets/site/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>

	<script type="text/javascript" src="<?=base_url()?>assets/site/js/jquery.orbit-1.2.3.js"></script>	<!-- ORBIT SLIDER -->
	<script type="text/javascript" src="<?=base_url()?>assets/site/js/background/jquery.parallax-1.1.3.js"></script><!-- BACKGROUND  -->
    <script type="text/javascript" src="<?=base_url()?>assets/site/js/background/jquery.localscroll-1.2.7-min.js"></script><!-- BACKGROUND  -->
    <script type="text/javascript" src="<?=base_url()?>assets/site/js/background/jquery.scrollTo-1.4.2-min.js"></script><!-- BACKGROUND  -->
    <script type='text/javascript' src='<?=base_url()?>assets/site/js/background/animate.js'></script><!-- BACKGROUND  -->


	<script type="text/javascript" src="<?=base_url()?>assets/site/js/parallax-plugin.js"></script>

	<!-- Add Thumbnail helper (this is optional) -->

    <!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?=base_url()?>assets/site/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/site/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/site/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="<?=base_url()?>assets/site/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/site/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="<?=base_url()?>assets/site/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="<?=base_url()?>assets/site/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    
  <script src="<?=base_url()?>assets/site/js/jquery.backgroundSize.js" type="text/javascript"></script>

  <script type="text/javascript" src="<?=base_url()?>assets/site/js/google-maps.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.tooltipster/4.0.4/js/tooltipster.bundle.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/site/js/init.js?anticache=6"></script>

  <?php if(@$page == 'imoveis') : ?>
  <script type="text/javascript" src="<?=base_url()?>assets/site/js/filters.js"></script>
  <?php endif; ?>

  <script type="text/javascript" src="<?=base_url()?>assets/site/js/funcoes.js?ver=0.1"></script>

  <script type="text/javascript" src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput.min.js"></script>
    
	<script type="text/javascript"> 

    $(".mask-telefone").mask("(99) 9999-9999?9");

		$(document).ready(function() {
			$(".fancybox-thumb").fancybox({
				padding	: 0,
				margin	: 30,
        index: 0,
				prevEffect	: 'none',
				nextEffect	: 'none',
				helpers	: {
					title	: {
						type: 'outside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
			});
		});    

	</script>
	
  <!-- Código do Google para tag de remarketing -->
  <!--
  ------------------------------------------------
  As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
  -------------------------------------------------
  -->
  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 972557761;
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
  <div style="display:inline; display:none">
  <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
  </div>
  </noscript>
  
	</body>
</html>