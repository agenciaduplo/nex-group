<!DOCTYPE html>
<html lang="pt-br">

<!--[if lte IE 7]> <html class="ie7"> <![endif]-->  
<!--[if IE 8]>     <html class="ie8"> <![endif]-->  
<!--[if IE 9]>     <html class="ie9"> <![endif]-->  
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>NewLife - Nex Group</title>

  <meta name="description" content="New Life, um condomínio pensado para facilitar o seu estilo de vida e para você aproveitar cada instante." />
  <meta name="keywords" content="newlife, new life, newlife nex group, nex group, nexgroup, nex, apartamento 2 3 dormitorios, apartamento porto alegre, condominios porto alegre" />
  
  <meta property='og:locale' content='pt_BR' />
  <meta property='og:title' content='NewLife' />
  <meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
  <meta property='og:description' content='New Life, um condomínio pensado para facilitar o seu estilo de vida e para você aproveitar cada instante'/>
  <meta property='og:url' content='http://www.nexgroup.com.br/newlife'/>

  <meta name="author" content="http://www.divex.com.br" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="1 days" />
  <meta name="mssmarttagspreventparsing" content="true" />

  <link rel="shortcut icon" href="<?=base_url()?>assets/newlife/img/favicon.ico" type="image/x-icon" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="<?=base_url()?>assets/newlife/css/orbit-1.2.3.css"> <!-- ORBIT SLIDER -->
  <link rel="stylesheet" href="<?=base_url()?>assets/newlife/css/animate.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/newlife/css/styles.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/newlife/webfontkit/stylesheet.css" type="text/css" charset="utf-8" />


  <link rel="stylesheet" href="<?=base_url()?>assets/newlife/css/font-awesome/css/font-awesome.css">
  
  <!--[if IE 7]>
    <link rel="stylesheet" href="<?=base_url()?>assets/newlife/css/font-awesome/css/font-awesome-ie7.css">
  <![endif]-->

  <script src="<?=base_url()?>assets/newlife/js/jquery-1.7.1.js"></script>

  <!-- Analytics -->
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-42888838-1']);
    _gaq.push(['_trackPageview']);
    (function()
    { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
    )();
  </script>
    
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-1622695-74', 'auto');
    ga('send', 'pageview');
  </script>
</head>    
<body>  

  <?php 
    $url_camp = $this->input->get('utm_source') . "__";
    $url_camp .= $this->input->get('utm_medium') . "__";
    $url_camp .= $this->input->get('utm_campaign');
  
    if($url_camp == "____")
      $url_camp = "Direto";
  ?>

 <div id="ancora"></div>




 <!-- HEADER FIXO -->
 <nav class="header-fixo absoluto clickroll ">
  <div class="row">
    <div id="interesse" class="clickroll">
     <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=34&midia=<?=$url_camp?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" class="ste lk1"><i class="icon icon-comments"></i><p>Corretor Online</p></a>
     <a href="#contato" class="ste lk2"><i class="icon icon-bullhorn"></i><p>Tenho interesse</p></a>
   </div>
   <a href="#home" class="logo">
    <h1 class="title">NewLife</h1>
  </a>
  <div class="right">
    <a href="#home">Home</a>
    <a href="#apresentacao">Apresentação</a>
    <a href="#localizacao">Localização</a>
    <a href="#infraestrutura">Infraestrutura</a>
    <a href="#plantas">Plantas</a>
    <a href="#decorado">Apartamento</a>
    <a href="#implantacao">Implantação</a>
    <a href="#contato">Contato</a>
  </div>
</div>
</nav>

<!-- CONTENT -->
<section id="home" class="bloco paral">
  <div class="clickroll">
    <a href="#apresentacao" class="ste"><i class="icon icon-chevron-down flash animated"></i></a>
  </div>
</section>

<!-- CONTENT -->
<section id="apresentacao" class="bloco paral">
  <div class="clickroll only-mobile">
    <a href="#localizacao" class="ste"><i class="icon icon-chevron-down"><span>Localização</span></i></a>
  </div>
</section>

<!-- CONTENT -->
<section id="localizacao" class="bloco paral">
  <div class="clickroll only-mobile">
    <a href="#infraestrutura" class="ste"><i class="icon icon-chevron-down"><span>Infraestrutura</span></i></a>
  </div>
</section>


<!-- CONTENT -->
<section id="infraestrutura">
  <div class="row">
    <h2>Apartamentos de 2 e 3 dorms.</h2>
    <p>com infraestrutura de lazer completa.</p>

  </div>

  <div class="clickroll">
    <a href="#fotos" class="ste"><i class="icon icon-chevron-down"></i></a>
  </div>


  <div id="fotos" class="list_carousel clickroll">
    <ul id="foo4">

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/01.jpg) bottom center !important"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Fachada Noturna</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>
      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/02.jpg) bottom center !important"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Fachada</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/03.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Piscina Coberta Aquecida</i></h3>
            <!--<a href="img/i1.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Piscina Coberta Aquecida"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>-->
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/04.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Salão de Festas Adulto</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/05.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Salão de Festas Infantil</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/06.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Fitness</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/08.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Pergolado</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/09.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Playground</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/10.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Portaria</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/11.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Praça Boas Vindas</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/12.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Quadra Recreativa</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>

      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/13.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Quiosque com churrasqueira</i></h3>
          </div>
        </div>
        <div class="black"></div>
      </li>


    </ul>
    <div class="clearfix"></div>
    <div class="navegation">
      <a id="prev4" class="prev" href="#fotos"></a>
      <div id="pager4" class="pager"></div>
      <a id="next4" class="next" href="#fotos"></a>
    </div>

  </div>
</section>

<!-- CONTENT -->
<section id="plantas">

    <div id="plants" class="list_carousel clickroll">
      <ul id="foo6">

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/plantas/01.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>2 dorms. 54m² privativos</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/plantas/01.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="2 dorms. 54m² privativos"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/plantas/02.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>3 dorms. 75m² privativos</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/plantas/02.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="3 dorms. 75m² privativos"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/plantas/03.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>3 dorms. 75m² privativos</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/plantas/03.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="3 dorms. 75m² privativos"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

      </ul>
      <div class="clearfix"></div>
      <div class="navegation">
        <a id="prev6" class="prev" href="#plants"></a>
        <div id="pager6" class="pager"></div>
        <a id="next6" class="next" href="#plants"></a>
      </div>
    </div>

</section>



<!-- CONTENT -->
<section id="decorado">
  <div class="row">
    <h2>Living Apto. 3 dorms.</h2>
    <p></p>
  </div>
  <div class="clickroll">
    <a href="#fotos2" class="ste"><i class="icon icon-chevron-down"></i></a>
  </div>
  <div id="fotos2" class="list_carousel clickroll">
    <ul id="foo5">
      <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/fotos/03/07.jpg)"><!-- CARD -->
        <div class="box2">
          <div class="left">
            <h3><i>Living Apto. 3 dorms.</i></h3>
            <!--<a href="img/living.jpg" class="fancybox-thumb " rel="fancybox-thumb" title="Piscina Coberta Aquecida"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>-->
          </div>
        </div>
        <!--<div class="black"></div>-->
        </li>
    </ul>
    <div class="clearfix"></div>
    <div class="navegation">
      <a id="prev5" class="prev" href="#fotos2"></a>
      <div id="pager5" class="pager"></div>
      <a id="next5" class="next" href="#fotos2"></a>
    </div>
  </div>
</section>

<!-- CONTENT -->
<section id="implantacao">

    <div id="implants" class="list_carousel clickroll">
      <ul id="foo7">

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/implantacao/02.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>Implantação</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/implantacao/02.jpg" class="fancybox-thumb " rel="fancybox-thumb2" title="Implantação"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/implantacao/01.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>Garagem Terreo</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/implantacao/01.jpg" class="fancybox-thumb " rel="fancybox-thumb2" title="Garagem Terreo"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/implantacao/03.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>Garagem Primeiro Subsolo</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/implantacao/03.jpg" class="fancybox-thumb " rel="fancybox-thumb2" title="Garagem Primeiro Subsolo"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

        <li class="box scales bloco paral" style="background:url(<?=base_url()?>assets/newlife/img/implantacao/04.jpg)"><!-- CARD -->
          <div class="box2">
            <div class="left">
              <h3><i>Garagem Segundo Subsolo</i></h3>
              <a href="<?=base_url()?>assets/newlife/img/implantacao/04.jpg" class="fancybox-thumb " rel="fancybox-thumb2" title="Garagem Segundo Subsolo"><i class="icon icon-zoom-in"></i>Ampliar imagem</a>
            </div>
          </div>
          <div class="black"></div>
        </li>

      </ul>
      <div class="clearfix"></div>
      <div class="navegation">
        <a id="prev7" class="prev" href="#implants"></a>
        <div id="pager7" class="pager"></div>
        <a id="next7" class="next" href="#implants"></a>
      </div>
    </div>

</section>




<div id="modal-sucesso" class="black-modal" style="display: none;">
  <div id="sucesso" class="modal-content">
    <div><span class="close-modal">X</span></div>
    <h4>SUA CADASTRO FOI</h4>
    <h3>REALIZADO COM SUCESSO</h3>
    <p>Obrigado! Em breve entraremos em contato!</p>
  </div>
</div> 


<div id="modal-fail" class="black-modal" style="display: none;">
  <div id="fail" class="modal-content">
    <div><span class="close-modal">X</span></div>
    <h4>OPA! FALHA AO ENVIAR CADASTRO =( </h4>
    <h3>TENTE NOVAMENTE</h3>
    <p>mensagem de erro!</p>
  </div>
</div> 

<!-- CONTENT -->
<section id="contato" class="bloco paral">
  <div class="row">

    <form id="formInteresse" name="formInteresse" method="post" action="" onsubmit="return false;">
      <ul class="left">
        <label>Nome:</label>
        <input name="nome" id="nome" type="text" class="field" placeholder="Digite seu nome aqui" maxlength="200" />

        <label>E-mail:</label>
        <input name="email" id="email" type="text" class="field" placeholder="Digite seu e-mail aqui"  maxlength="200" />

        <label>Telefone:</label>
        <input name="telefone" id="telefone" type="text" class="field" placeholder="Digite seu telefone aqui" />

        <label>Cidade:</label>
        <input name="cidade" type="text" class="field" placeholder="Digite o nome de sua cidade" maxlength="200" />
      </ul>
      <ul class="left">
        <label>Mensagem:</label>
        <textarea id="msg" name="msg" placeholder="Sua Mensagem aqui"></textarea>
        
        <input type="button" class="btn" value="Enviar" onclick="newlife.enviaInteresse();" />
      </ul>
    </form> 

  </div>
  <div class="ablu">
    <a class="im1" href="http://www.nexgroup.com.br/" target="_blank"><img src="<?=base_url()?>assets/newlife/img/newlife_nex_logo.png"></a>
    <a class="im2" href="http://imobi.divex.com.br/" target="_blank"><p>site desenvolvido por </p><img src="<?=base_url()?>assets/newlife/img/divex.png"></a>
  </div>
</section>


<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/newlife/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    
<!-- optionally include helper plugins -->
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/newlife/js/helper-plugins/jquery.mousewheel.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/newlife/js/helper-plugins/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/newlife/js/helper-plugins/jquery.transit.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/newlife/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/jquery.orbit-1.2.3.js"></script>	<!-- ORBIT SLIDER -->
<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/background/jquery.parallax-1.1.3.js"></script><!-- BACKGROUND  -->
<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/background/jquery.localscroll-1.2.7-min.js"></script><!-- BACKGROUND  -->
<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/background/jquery.scrollTo-1.4.2-min.js"></script><!-- BACKGROUND  -->
<script type='text/javascript' src='<?=base_url()?>assets/newlife/js/background/animate.js'></script><!-- BACKGROUND  -->


<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/parallax-plugin.js"></script>

<!-- Add Thumbnail helper (this is optional) -->

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?=base_url()?>assets/newlife/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/newlife/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/newlife/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="<?=base_url()?>assets/newlife/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/newlife/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="<?=base_url()?>assets/newlife/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/newlife/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<script src="<?=base_url()?>assets/newlife/js/jquery.backgroundSize.js" type="text/javascript"></script>

<script type="text/javascript" src="<?=base_url()?>assets/newlife/js/init.js"></script>

<script src="<?=base_url()?>assets/newlife/js/jquery.maskedinput.js" type="text/javascript"></script>
   
<script type="text/javascript">  
 
  $(document).ready(function() {

    $("#telefone").mask("(99) 9999-9999");   

    $(".fancybox-thumb").fancybox({
      padding	: 0,
      margin	: 30,
      prevEffect	: 'none',
      nextEffect	: 'none',
      helpers	: {
        title	: {type: 'outside'},
        thumbs	: {width: 50, height	: 50}
      }
    });
  });   

  var newlife = {
    enviaInteresse: function() {

      var response = "";
      var retorno = true;

      var nome    = $("input[name=nome]");
      var email   = $("input[name=email]");
      var tel     = $("input[name=telefone]");
      var cidade  = $("input[name=cidade]");
      var msg     = $("textarea[name=msg]");

      base_url  =  "<?=base_url()?>" + "newlife/newLifeEnviaInteresse";

      if(retorno == true) {

        $.ajax({
          type: "POST",
          url: base_url,
          data: {nome: nome.val(), email: email.val(), telefone: tel.val(), cidade: cidade.val(), msg: msg.val()},
          dataType: "json",
          success: function(response) {
            if(response.erro == 0) {
              $('#modal-fail').hide();
              $('#modal-sucesso').show();
              nome.val('');
              email.val('');
              tel.val('');
              cidade.val('');
              msg.val('');
              return true;
            }
            if(response.erro == 1) {
              $('#modal-sucesso').hide();
              $('#modal-fail p').text(response.msg);
              $('#modal-fail').show();
              return false;
            }
          }
        });

      } else {
        return false;
      }
    },
    closeModal: function() {
      $('.black-modal').hide();
    }
  }

  $('.close-modal','.black-modal .modal-content').click(function() {
    newlife.closeModal();
  });

</script>
</body>
</html>