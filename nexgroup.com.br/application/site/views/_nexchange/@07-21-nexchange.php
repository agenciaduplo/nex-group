<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
    <!--[if IE 8]>   <html class="ie8" lang="pt-BR"> <![endif]-->  
    <!--[if IE 9]>   <html class="ie9" lang="pt-BR"> <![endif]-->  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link rel="canonical" href="http://www.nexgroup.com.br/nexchange" />

  <title>A melhor troca para seu imóvel - NexChange | Nex Group</title>
  
  <meta name="description" content="Seu imóvel atual, com Nex Change vale muito! A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família." />  
  <meta name="keywords" content="nexchange, nex change, nex group, troca, avaliação, imoveis, compra imovel, como adquirir imovel, troca imovel, trocar imovel por um novo, avaliação de imovel, avaliar imovel, valor do meu imovel" />

  <meta property='og:locale' content='pt_BR' />
  <meta property='og:title' content='A melhor troca para seu imóvel - NexChange | Nex Group' />
  <meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
  <meta property='og:description' content='Seu imóvel atual, com Nex Change vale muito! A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família.'/>
  <meta property='og:url' content='http://www.nexgroup.com.br/nexchange'/>
    
    <meta name="author" content="http://www.divex.com.br" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    
    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />

  <link rel="shortcut icon" href="<?=base_url()?>favicon.png" />
  <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/main.css" type="text/css" media="screen" />
  <link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
  <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/nexchange3.css" type="text/css" media="screen" />
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome.css">
    
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->

  <script type="text/javascript" src="<?=base_url()?>assets/nexchange/js/jquery-1.6.4.min.js"></script>

  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
      
  <script type="text/javascript">
    // var _gaq = _gaq || [];
    // _gaq.push(['_setAccount', 'UA-42888838-1']);
    // _gaq.push(['_trackPageview']);
    // (function()
    //   { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
    // )();
  </script>
    
    <script>
      // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      // ga('create', 'UA-1622695-74', 'auto');
      // ga('send', 'pageview');
    </script>
  
 
    <script>
    $(document).ready(function() {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
        })
      });
      });
    </script>

    <script>
    $(document).ready(function(){
        $("input[name=telefone_area]").inputmask("99[9]",{ 
                placeholder: "DDD",
                showMaskOnFocus: true, 
                clearMaskOnLostFocus: false,
                skipOptionalPartCharacter: " ",
                onincomplete: function(){
                  $(this).val('');
                },
                isComplete: function() {
                  if($(this).val() == '0DD' || $(this).val() == '00D' || $(this).val() == '000' || $(this).val() == 'DDD') {
                    $(this).val('DDD');
                    return false;
                  }
                  var a = $(this).val().replace('D', '');
                  if(a.length == 2) {
                  var r = $(this).val().replace('D', '');
                  $(this).val('0' + r);
                }
                }
            });

        if($.browser.msie && parseFloat($.browser.version) == 7){
          $("input[name=telefone]").inputmask("99999999",{ 
            placeholder: "TELEFONE", 
            showMaskOnFocus: false, 
            clearMaskOnLostFocus: false,
            onincomplete: function(){
                    $(this).val('');
                },
            isComplete: function(){
              var num = $(this).val().match(/\d/g).join('');

              if(num.length == 8){
                var er = /^[0-9]+$/;
                if( !er.test($(this).val())){
                  $(this).val('');
                }
              }
            }
          });
        }else{
          $("input[name=telefone]").inputmask("99999999",{ 
            placeholder: "TELEFONE", 
            showMaskOnFocus: false, 
            clearMaskOnLostFocus: false,
            onincomplete: function(){
              $(this).val('');
            }
          });
        }

    });
    </script>

    <style type="text/css">
      input.error {
        background: #f88;
      }
      input.valid {
        background: #8f8;
        display: block !important;
      }
      .modal .box-modal {position: fixed;}
    </style>  
</head>
<body> 

  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NQ4BPL"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-NQ4BPL');</script>
  <!-- End Google Tag Manager -->
  
  <div class="modal" <?php if($liberar_mensagem == 1): echo 'style="display:block"'; else: echo 'style="display:none"'; endif; ?>>
    <div class="black"></div>
    <div class="box-modal">
      <p>SUA mensagem <br>foi enviada com sucesso!</p>
          <a href="<?=base_url()?>nexchange" class="btn">fechar</a>
    </div>
  </div>


  <section id="nexchange" class="paral clickroll">

        <div class="container_12">
      <a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/entrar.php?id_produto=33&gclid=&referencia=<?=@$_SESSION['url']?>','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor on line" class="corretor radius3">
            <i class="icon icon-comments"></i>
            <span>Fale agora com um </span><p>corretor online</p>
        </a>

      <a href="#empreendimentos" title="more" class="more-info radius4 no-mobile" style="display:none !important">
            <i class="icon icon-arrow-down"></i>
            <p>ver Imóveis participantes</p>
        </a>


            <div class="box">
              <h2>
                <b>A SUA CHANCE DE TROCAR SEU IMÓVEL ESTÁ AQUI.</b>
          
        </h2>

        <div style="width:100%; float:left">
          <form id="formNexChange" name="formNexChange" action="<?=base_url()?>nexchange" method="post" onsubmit="return false;">
            <p style="float:left; font-size:15px; font-weight:700 !important">Para mais informações, preencha o formulário abaixo:</p>
            <p style="font-size: 11px;"><span style="font-size: 24px;vertical-align: -7px;">*</span>Campo obrigatório</p>

            <input name="nome" id="nome" type="text" class="field" placeholder="* NOME:" />
            <input name="email" id="email" type="text" class="field" placeholder="* E-MAIL:" />

            <input name="telefone_area" type="text" class="field Dig3"  style="width: 23%;" />
            <input name="telefone" type="text" class="field"  style="width: 68%; margin-right: 0;;" />

            <input name="cidade" type="text" class="field" placeholder="CIDADE:" />

                    <input type="button" id="btn-enviar" class="btn" value="Enviar" />
          </form> 
        </div>

            </div>
        </div>
    </section>

<!--
    <section id="convite">
        <div class="container_12">
          <img src="<?=base_url()?>assets/site/img/nexchange/novo/logo_auxiliadora.png" alt="Logo Auxiliadora">
      <div class="fooo" style="float:left">
        <h1>
          <span>Venha conferir as melhores oportunidades que a Nex tem pra você.</span><br>
          Neste sábado, 15/11, atendimento especial na Auxiliadora Predial da Protásio Alves

        </h1>
      </div>
    </div>
    </section>
-->

    <section id="empreendimentos" style="display:none">
        <div class="container_12">
          <h3>CONFIRA ABAIXO OS MELHORES IMÓVEIS DE PORTO ALEGRE E CANOAS.</h3>

          <ul class="um_colun no-mobile">
            <li>
              <img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-new-life.png">
              <div class="left">
                <h4>NEW LIFE - PORTO ALEGRE / RS</h4>
                <p>Lazer Completo 2 e 3 Dormitório próximo à Puc</p>
                <a href="http://www.nexgroup.com.br/imoveis/102/new-life-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
              <figure><span>Lançamento</span></figure>
            </li>
          </ul>


          <ul class="tres_colun">
            <li class="yes-mobile"> <!-- ******** 01 MOBILE ******* -->
              <figure class="chacara"><span>Lançamento</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-new-life-2.png"></div>
              <div class="left">
                <h4><span>NEW LIFE - PORTO ALEGRE / RS</span> - PORTO ALEGRE / RS</h4>
                <p>Lazer Completo 2 e 3 Dormitório próximo à Puc</p>
                <a href="http://www.nexgroup.com.br/imoveis/102/new-life-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>

            <li> <!-- ******** 01 ******* -->
              <figure class="chacara"><span>Obras avançadas</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-chacara.png"></div>
              <div class="left">
                <h4><span>CHÁCARA DAS NASCENTES</span> - PORTO ALEGRE / RS</h4>
                <p>Sobrados e casas térreas de 3 dorms em Bairro planejado</p>
                <a href="http://www.nexgroup.com.br/imoveis/86/chacara-das-nascentes-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 04 ******* -->
              <figure class="joy"><span>Obras avançadas</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-joy.png"></div>
              <div class="left">
                <h4><span>JOY</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 2 e 3 dorms. c/ suíte e churrasqueira</p>
                <a href="http://www.nexgroup.com.br/imoveis/71/joy-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 07 ******* -->
              <figure class="piattelli"><span>Obras avançadas</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-piattelli.png"></div>
              <div class="left">
                <h4><span>RISERVA PIATTELLI</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 2 e 3 dorms. C/ suíte, churrasqueira e vaga dupla</p>
                <a href="http://www.nexgroup.com.br/imoveis/5/riserva-piattelli-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
          </ul>


          <ul class="tres_colun">
            <li> <!-- ******** 02 ******* -->
              <figure class="singolo"><span>Obras avançadas</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-singolo.png"></div>
              <div class="left">
                <h4><span>SÍNGOLO</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 2 e 3 dorms. c/ suíte e churrasqueira</p>
                <a href="http://www.nexgroup.com.br/imoveis/82/singolo-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 05 ******* -->
              <figure class="polo"><span>últimas unidades</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-polo.png"></div>
              <div class="left">
                <h4><span>polo</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 2 e 3 suítes c/ churrasqueira</p>
                <a href="http://www.nexgroup.com.br/imoveis/83/polo-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 08 ******* -->
              <figure class="victoria"><span>Concluído</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-victoria.png"></div>
              <div class="left">
                <h4><span>VICTORIA TOWN HOUSES</span> - PORTO ALEGRE / RS</h4>
                <p>Casas de 3 dorms. c/ suíte, pátio e sótão</p>
                <a href="http://www.nexgroup.com.br/imoveis/9/victoria-town-houses-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
          </ul>


          <ul class="tres_colun no-margin-right">
            <li> <!-- ******** 03 ******* -->
              <figure class="max"><span>Obras avançadas </span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-max-hause.jpg"></div>
              <div class="left">
                <h4><span>MAX HAUS</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 0, 1, 2, 3 OU X dorms.</p>
                <a href="http://www.nexgroup.com.br/imoveis/90/maxhaus-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 06 ******* -->
              <figure class="vergeis"><span>Obras avançadas</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-vergeis.png"></div>
              <div class="left">
                <h4><span>vergéis</span> - PORTO ALEGRE / RS</h4>
                <p>1 e 2 Dorms. + Coberturas</p>
                <a href="http://www.nexgroup.com.br/imoveis/36/vergeis-porto-alegre-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>
            <li> <!-- ******** 09 ******* -->
              <figure class="stellato"><span>Concluído</span></figure>
              <div class="imgs"><img src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-stellato.png"></div>
              <div class="left">
                <h4><span>stellato</span> - PORTO ALEGRE / RS</h4>
                <p>Aptos de 3 dorms. com suíte e churrasqueira</p>
                <a href="http://www.nexgroup.com.br/imoveis/68/stellato-canoas-rio-grande-do-sul" target="_blank">Saiba mais</a>
              </div>
            </li>  
          </ul>
          <h3 style="margin-top: -2.5em !important">A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família e para o seu estilo de vida.  Com o Nex Change, você pode usar seu imóvel atual para adquirir um novo com agilidade e facilidade.</h3>

    </div>
    </section>


    <footer>
        <div class="container_12">
          <div class="foo">

            <br><br><br>      
                
            <div class="conter">
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=388662154605333&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>

              
              <br>  
                
              <div class="fb-like" data-href="https://www.facebook.com/NexGroup/" data-width="100%" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>

            </div>  




            <br style="clear: both" />

              <a href="http://www.nexgroup.com.br/"><img title="Com NexChange seu imóvel vale muito" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-nex.png" alt="Logo nex"></a>
              <p>
              <span>Avaliação e condições facilitadas | Agilidade no fechamento</span><br>
                Dação condicionada à avaliação e a análise da documentação do imóvel, livre e desembaraçado de quaisquer, ônus pela vendedora. Consulte empreendimentos participantes.
            </p>
          </div>
      </div>
    </footer>






    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.parallax-1.1.3.js"></script><!-- BACKGROUND  -->
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.localscroll-1.2.7-min.js"></script><!-- BACKGROUND  -->
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.scrollTo-1.4.2-min.js"></script><!-- BACKGROUND  -->
    <script type='text/javascript' src='<?=base_url()?>assets/nexchange/img/nexchange/novo/background/animate.js'></script><!-- BACKGROUND  -->
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/parallax-plugin.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/init.js"></script>



  <script src="<?=base_url()?>assets/nexchange/js/jquery.inputmask.js" type="text/javascript"></script>

  <script type="text/javascript">
    $("#btn-enviar").click(function(){
      ValidaForm();
    });

    function ValidaForm() {

      // alert('ativo');

      // $("#btn-enviar").attr("value", "Enviando...");
      // $("#btn-enviar").attr("disabled", "disabled");

      // return false;

      var response = "";
      var retorno = true;

      var nome =  $("input[name=nome]");
          var email = $("input[name=email]");
          var ddd = $("input[name=telefone_area]");
          var tel = $("input[name=telefone]");
          var cidade = $("input[name=cidade]");
          
          var emailValido=/^.+@.+\..{2,}$/;
          var nomeValido=/^[a-z\u00C0-\u00ff A-Z]+$/i;
          var telefoneValido = /^[0-9]+$/;


          if(!nomeValido.test(nome.val()))  {
              nome.removeClass('valid');
              nome.addClass('error');
              retorno = false;
          } else {
              nome.removeClass('error');
            nome.addClass('valid');
          }

          if(!emailValido.test(email.val())) {
              email.removeClass('valid');
              email.addClass('error');
              retorno = false;
          } else {
              email.removeClass('error');
            email.addClass('valid');
          }

          if((ddd.val() != "" && ddd.val() != "DDD") && ddd.val().length == 3) {

        if(!telefoneValido.test(tel.val())) {
          tel.val('TELEFONE');
          tel.removeClass('valid');
                tel.addClass('error');
                retorno = false;
        }

            ddd.removeClass('error');
            ddd.addClass('valid');
          }

          if((tel.val() != "" && tel.val() != "TELEFONE") && tel.val().length == 8) {

        if(!telefoneValido.test(tel.val())) {
          tel.val('');
          tel.removeClass('valid');
                tel.addClass('error');
                retorno = false;
        }

            if(ddd.val() == "" || ddd.val() == "000" || ddd.val() == "DDD") {
              ddd.removeClass('valid');
                ddd.addClass('error');
                retorno = false;
            }

        tel.removeClass('error');
            tel.addClass('valid');
          }

          if((tel.val() == "" || tel.val() == "TELEFONE") && (ddd.val() == "" || ddd.val() == "DDD")) {
            ddd.removeClass('valid');
            ddd.removeClass('error');
            tel.removeClass('valid');
            tel.removeClass('error');
          }

          
          if(retorno) 
          {
          base_url = "<?=base_url()?>" + "nexchange/enviaNexChange";
    
        $.ajax({
          type: "POST",
          url: base_url,
          data: {nome: nome.val(), email: email.val(), telefone_area: ddd.val(), telefone: tel.val(), cidade:cidade.val()},
          dataType: "json",
          success: function(response) 
          { 
            if(response.erro == 0)
            {
              $('.modal').show();
            }

            if(response.erro == 1)
            {
              alert('Falha ao enviar contato! Tente novamente mais tarde.');
            } 
          }
        });
      
        return true;
          } 
          else 
          {
            return false;
          } 
    }
  </script>
</body>
</html>