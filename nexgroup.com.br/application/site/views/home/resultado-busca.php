<?php $this->load->view("includes/header"); ?>
					<section id="search-results" class="Main Inner">
						<div class="Bg2">
							<div class="Wrapper">
								<div class="Box1">
									<hgroup>
										<h2>encontre <span>seu im&oacute;vel</span></h2>
										<h3>utilize os filtros para facilitar a sua busca</h3>
									</hgroup>

									<form  action="<?=base_url()?>imoveis/buscarImoveis" method="post" id="frm-browse-by-the-map" >
									<fieldset>
										<div class="BoxSelctFilters">
											<select id="imoEstados" name="estados">
												<option>Estado</option>
												<?php foreach (@$estados as $row): ?>
													<option value="<?=@$row->id_estado?>"  <?php if($row->id_estado==21) echo "selected='selected'"; ?>><?=$row->uf?></option>
												<?php endforeach; ?>
											</select>
											<select name="cidades" id="imoCidades">
												<option>Cidade</option>
												<?php foreach (@$cidades as $row): ?>
													<option value="<?=@$row->id_cidade?>"><?=$row->cidade?></option>
												<?php endforeach; ?>
											</select>
											<select name="tipos">
												<option value="">Tipo de Imóvel</option>
												<?php foreach (@$tipo_imoveis as $row): ?>
													<option value="<?=@$row->id_tipo_imovel?>"><?=$row->tipo_imovel?></option>
												<?php endforeach; ?>
											</select>
											<button class="BtnRed" type="submit">Buscar</button>
										</div>
									</fieldset>
								</form>
                                    <? //var_dump($noticias);?>
									<div class="BoxDescription">
										<span><?=$countImoveis?> Resultados em <a href="<?=site_url()?>imoveis">IM&Oacute;VEIS</a>, <?=$countNoticias?> Resultados em <a href="<?=site_url()?>noticias">NOT&Iacute;CIAS</a></span>
									</div>
								</div>
								<div class="Box1">
									<h2>resultados em im&oacute;vel</h2>
									<div class='BoxListEmps BoxList'>
										<?php 
											if(@$imoveis):
											echo '<ul>';
											foreach ($imoveis as $row):
											if(@$row->id_empresa == 1):
												$imgLogo = "logo-capa-2";
											elseif(@$row->id_empresa == 2):
												$imgLogo = "logo-egl-2";
											elseif(@$row->id_empresa == 3):
												$imgLogo = "logo-dhz-2";
											elseif(@$row->id_empresa == 4):
												$imgLogo = "logo-lomando-aita-2";
											elseif(@$row->id_empresa == 5):
												$imgLogo = "logo-capamax-2";
											elseif(@$row->id_empresa == 6):
												$imgLogo = "logo-nex";
											elseif(@$row->id_empresa == 7):
												$imgLogo = "logo-capamax-sem-nex";
											endif;
											
											$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
											$url = $this->utilidades->sanitize_title_with_dashes(@$newUrl);
										?>
											<li>
												<a href="<?=site_url()?>imoveis/<?=@$row->id_empreendimento?>/<?=@$url?>" title="<?=@$row->empreendimento?>">
													<figure>
														<?php if(@$row->imagem_fachada): ?>
															<img width="170px;" src="<?=base_url()?>uploads/imoveis/fachada/visualizar/<?=@$row->imagem_fachada?>" alt="<?=@$row->empreendimento?> - <?=@$row->empresa?>" />
															
															<?php
															if($row->status_geral == 2){
																echo '<img class="sold"  src="',base_url(),'assets/site/img/layout/pronto_p_morar.png" width="82" height="82" />';
															} elseif($row->status_geral == 6){
																echo '<img class="sold"  src="',base_url(),'assets/site/img/layout/bg_100-percent.png" width="82" height="82" />';
															}
															?>
															
														<?php else: ?>
																
														<?php endif; ?>
														<figcaption>
															<div>
																<h4><?=@$row->empreendimento?></h4>
																<p><?=@$row->chamada_empreendimento?></p>
																<address><?=$row->cidade?> / <?=@$row->uf?></address>
																<?php if(@$row->mostrar_fase == "SS"): ?>
																<p class="Exception"><?=@$row->titulo?></p>
																<?php endif; ?>
															</div>
															<?php if($row->id_empreendimento == 84): ?>
															<img src="<?=base_url()?>assets/site/img/layout/logo-nex-taroii.png" alt="Nex Group - Taroii" />
															<?php else: ?>
															<img <?php if($row->id_empresa == 6) echo "width='100px;'"; ?> src="<?=base_url()?>assets/site/img/layout/<?=@$imgLogo?>.png" alt="<?=$row->empresa?>" />
															<?php endif; ?>
														</figcaption>
													</figure>
												</a>
											</li>
										<?php endforeach; ?>
										</ul>
										<?php else:?>
											<p>Nenhum imóvel encontrado</p>
										<?php endif; ?>
									</div>
								</div>
								<div class="Box1">
									<h2>resultados em not&iacute;cias</h2>
									<ul class="BoxResultNews">
                                    <?php if($noticias): 
                                    		foreach($noticias as $row):
												 $url	= $this->utilidades->sanitize_title_with_dashes($row->titulo);?>
										<li><a href="<?=site_url()?>noticias/<?=@$row->id_noticia?>/<?=@$url?>" title="<?=$row->titulo?>"><?=$row->titulo?></a></li>
                                        <?php endforeach; ?>
                                        </ul>
										<?php else: ?>
                                            <p>Nenhuma notícia encontrada</p>
										<?php endif; ?>
										
									
								</div>
                                <!--
								<div class="Box1">
									<h2>resultados em empresa</h2>
									<ul class="BoxResultNews">
										<li><a href="#" title="">A Nex Group ir&aacute; atuar basicamente em 4 segmentos: produtos na faixa de R$ 150 mil (atrav&eacute;s do Programa Minha Casa, Minha Vida), condom&iacute;nios verticais fechados (na faixa de R$ 200 mil), condom&iacute;nios horizontais fechados (na faixa de R$ 300 mil) e em nichos, como im&oacute;veis de alto padr&atilde;o e corporativos tipo Triple A.</a></li>
										<li><a href="#" title="">A Nex Group ir&aacute; atuar basicamente em 4 segmentos: produtos na faixa de R$ 150 mil (atrav&eacute;s do Programa Minha Casa, Minha Vida), condom&iacute;nios verticais fechados (na faixa de R$ 200 mil), condom&iacute;nios horizontais fechados (na faixa de R$ 300 mil) e em nichos, como im&oacute;veis de alto padr&atilde;o e corporativos tipo Triple A.</a></li>
										<li><a href="#" title="">A Nex Group ir&aacute; atuar basicamente em 4 segmentos: produtos na faixa de R$ 150 mil (atrav&eacute;s do Programa Minha Casa, Minha Vida), condom&iacute;nios verticais fechados (na faixa de R$ 200 mil), condom&iacute;nios horizontais fechados (na faixa de R$ 300 mil) e em nichos, como im&oacute;veis de alto padr&atilde;o e corporativos tipo Triple A.</a></li>
										<li><a href="#" title="">A Nex Group ir&aacute; atuar basicamente em 4 segmentos: produtos na faixa de R$ 150 mil (atrav&eacute;s do Programa Minha Casa, Minha Vida), condom&iacute;nios verticais fechados (na faixa de R$ 200 mil), condom&iacute;nios horizontais fechados (na faixa de R$ 300 mil) e em nichos, como im&oacute;veis de alto padr&atilde;o e corporativos tipo Triple A.</a></li>
										<li><a href="#" title="">A Nex Group ir&aacute; atuar basicamente em 4 segmentos: produtos na faixa de R$ 150 mil (atrav&eacute;s do Programa Minha Casa, Minha Vida), condom&iacute;nios verticais fechados (na faixa de R$ 200 mil), condom&iacute;nios horizontais fechados (na faixa de R$ 300 mil) e em nichos, como im&oacute;veis de alto padr&atilde;o e corporativos tipo Triple A.</a></li>
									</ul>
								</div>-->
							</div>
						</div>
					</section>
<?php $this->load->view("includes/footer"); ?>				