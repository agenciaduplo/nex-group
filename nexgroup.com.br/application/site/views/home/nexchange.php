<!DOCTYPE html>
<html lang="pt-br">
	<head>
        <!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
        <!--[if IE 8]>	 <html class="ie8" lang="pt-BR"> <![endif]-->  
        <!--[if IE 9]>	 <html class="ie9" lang="pt-BR"> <![endif]-->  
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<?php if (@$this->uri->segment(1) == 'nao-encontrou-seu-imovel') { ?>
		
		<title>Não encontrou seu imóvel? - <?=@$title?></title>
		
		<?php } else { ?>
		
		<title><?=@$title?></title>
		
		<?php } ?>
		
		<!-- METAS -->
		<?php if(@$pagetag=='imovelvisualizar'): ?>

				<meta property='og:locale' content='pt_BR' />
				<meta property='og:title' content='<?=@$title?>' />
				<meta property='og:image' content='http://www.nexgroup.com.br/uploads/imoveis/logo/<?=@$imovel->logotipo?>'/>
				<?php if(@$imovel->tags_description):?>
					<meta property='og:description' content='<?=@$imovel->tags_description?>'/>
				<?php else:?>
					<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
				<?php endif; ?>
				<meta property='og:url' content='<?=current_url()?>'/>


				<?php if(@$imovel->tags_description):?>
					<meta name="description" content="<?=@$imovel->tags_description?>" />
				<?php else:?>
					<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />	
				<?php endif;
				
				if(@$imovel->tags_keywords):?>
					<meta name="keywords" content="<?=@$imovel->tags_keywords?>" />
				<?php else:?>
					<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
				<?php endif;?>

			
				
	    <?php else: ?>

	    		<?php if(@$description):?>
	    				<meta name="description" content="<?=$description?>" />
	    		<?php else: ?>
	    				<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
	    		<?php endif;?>
	    		<?php if(@$description):?>
	    				<meta name="keywords" content="<?=$keywords?>" />
	    		<?php else: ?>
	    				<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
		    	<?php endif;?>

				<meta property='og:locale' content='pt_BR' />
				<meta property='og:title' content='Nex Group' />
				<meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
				<meta property='og:description' content='A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior.'/>
				<meta property='og:url' content='http://www.nexgroup.com.br'/>
	    
	    <?php endif;?>
	    
	    <meta name="author" content="http://www.reweb.com.br" />
	    <meta name="language" content="pt-br" />
	    <meta name="revisit-after" content="1 days" />
	    <meta name="mssmarttagspreventparsing" content="true" />
	    
	    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />
		<!-- METAS -->
		
		
		<!-- STYLE -->
		<link rel="shortcut icon" href="<?=base_url()?>favicon.png" />
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/main.css" type="text/css" media="screen" />
		<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
		<!-- STYLE -->
		
		<!-- STYLE FEIJOADINHA -->
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/feijoadinha-style.css" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

		<!-- STYLE NEXCHANGE -->
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/nexchange.css" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>


		<!-- LIBS -->
		<script type="text/javascript" src="<?=base_url()?>assets/site/js/lib/jquery-1.6.4.min.js"></script>
		<!-- LIBS -->

		<!-- SCRIPTS -->
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!-- Analytics -->
		<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-42888838-1']);
		_gaq.push(['_trackPageview']);
		(function()
		{ var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
		)();
		</script>
		<!--script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-1622695-41']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script-->		
		<!-- Fim Analytics -->
		<style type="text/css">
			input.error {
				background: #f88;
			}
			input.valid {
				background: #8f8;
				display: block !important;
			}
			.modal .box-modal {position: fixed;}
		</style>	
	</head>
	<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
<body> 
	<div class="modal" style="display:none">
		<div class="black"></div>
		<div class="box-modal">
			<p>SUA mensagem <br>foi enviada com sucesso!</p>
	        <a href="<?=base_url()?>nexchange" class="btn">fechar</a>
		</div>


	</div>

<!-- CONTENT -->
	<section id="nexchange">
        <div class="container_12">
            <div class="box">
            	<img src="<?=base_url()?>assets/site/img/nexchange/logo.png" alt="">
            	<h2><b>VOCÊ SABE 
					QUANTO VALE 
					SEU IMÓVEL?</b>
					COM A NEX, 
					ELE PODE 
					VALER MUITO.
				</h2>
				<div class="foo1" style="float:left">
					<p>A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família e para o seu estilo de vida. 
					Com o Nex Change, você pode usar seu imóvel atual para adquirir um novo, com agilidade, facilidade. <br>
					<span>
						Condições facilitadas <b>|</b> Agilidade no fechamento
					</span>
					</p>
				</div>

				<div style="width:100%; float:left">
					<form id="formNexChange" name="formNexChange" action="<?=base_url()?>nexchange" method="post" onsubmit="return false;">
						<p style="float:left; font-size:16px; font-weight:700 !important">Para mais informações, preencha o formulario abaixo:</p>
						<p style="font-size: 13px;"><span style="font-size: 24px;vertical-align: -7px;">*</span>Campo obrigatório</p>
						<input name="nome" id="nome" type="text" class="field" placeholder="* NOME:" />
						<input name="email" id="email" type="text" class="field" placeholder="* E-MAIL:" />
						<input name="telefone_area" type="tel" class="field Dig3" placeholder="DDD:" style="width: 5%;" />
						<input name="telefone" type="tel" class="field" placeholder="TELEFONE:" style="width: 9%;" />
						<input name="cidade" type="text" class="field" placeholder="CIDADE:" />
		                <input type="button" class="btn" value="Enviar" onclick="ValidaForm();" />
					</form>	
				</div>
				<div class="foo" style="float:left; margin-bottom:4%">
	            	<a href="http://www.nexgroup.com.br/"><img src="<?=base_url()?>assets/site/img/nexchange/logo-nex.png" alt=""></a>
	            	<p style="width:75%; margin:5% 0 0 2%; font-size:11px; color:#CCC">Dação condicionada à avaliação e a análise da documentação do imóvel, livre e desembaraçado de quaisquer, ônus pela vendedora.</p>
	            </div>
            </div>
        </div>
    </section>
<!-- CONTENT END -->

<!--SCRIPTS-->

<script src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput-1.3.min.js" type="text/javascript" ></script>
<script src="<?=base_url()?>assets/site/js/plugins/jquery.validate.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/site/js/source/validations.js" type="text/javascript"></script>


<script type="text/javascript">

	function ValidaForm() {
		var response = "";
		var retorno = true;

		var nome =  $("input[name=nome]");
        var email = $("input[name=email]");
        var ddd = $("input[name=telefone_area]");
        var tel = $("input[name=telefone]");
        var cidade = $("input[name=cidade]");
        
        var emailValido=/^.+@.+\..{2,}$/;
        var nomeValido=/^[a-z\u00C0-\u00ff A-Z]+$/i;


        if(!nomeValido.test(nome.val()))  {
            nome.removeClass('valid');
            nome.addClass('error');
            retorno = false;
        } else {
            nome.removeClass('error');
        	nome.addClass('valid');
        }

        if(!emailValido.test(email.val())) {
            email.removeClass('valid');
            email.addClass('error');
            retorno = false;
        } else {
            email.removeClass('error');
        	email.addClass('valid');
        }

        if((ddd.val() != "" && ddd.val() != "00" && ddd.val() != "__" ) && ddd.val().length == 2) {

        	if(tel.val() == "" || tel.val() == "0000-0000" || tel.val() == "____-____") {
        		tel.removeClass('valid');
            	tel.addClass('error');
            	retorno = false;
        	}

        	ddd.removeClass('error');
        	ddd.addClass('valid');
        }

        if((tel.val() != "" && tel.val() != "0000-0000" && tel.val() != "____-____" ) && tel.val().length == 9) {

        	if(ddd.val() == "" || ddd.val() == "00" || ddd.val() == "__") {
        		ddd.removeClass('valid');
            	ddd.addClass('error');
            	retorno = false;
        	}

			tel.removeClass('error');
        	tel.addClass('valid');
        }

        if(tel.val() == "" && ddd.val() == "") {
        	ddd.removeClass('valid');
        	ddd.removeClass('error');
        	tel.removeClass('valid');
        	tel.removeClass('error');
        }

        
        if(retorno) {
    		base_url  	=  "<?=base_url()?>" + "home/enviaNexChange";
	
			$.ajax({
				type: "POST",
				url: base_url,
				data: {nome: nome.val(), email: email.val(), telefone_area: ddd.val(), telefone: tel.val(), cidade:cidade.val()},
				dataType: "json",
				success: function(response) {
					
					if(response.erro == 0)
					{
						$('.modal').show();
					}


					if(response.erro == 1)
					{
						alert('Falha ao enviar contato! Tente novamente mais tarde.');
					}
					
				}
			});
		
		return true;
        } else {
        	return false;
        } 
	}

</script>

</body>
</html>



