<?php
$this->load->view("includes/header-home");
$this->load->view("includes/header-html-scroll");
// $ses_url = @$_SESSION['url'];
$this->load->view("includes/modal_telefone");
$this->load->view("includes/modal_contato_cliente");
$this->load->view("includes/modal_revista");
// $this->load->view("includes/modal_politica");
?>

<script>
var status_id = '-'; // '-' = aba todos  1 = aba de lançamentos  2 = aba pronto pra morar
var imoveisBusca = <?=json_encode($arrImoveisBusca)?>
</script>

<!-- DESTAQUE -->
<section id="destaque" class="bloco only-home" style="background-color: #9C9B9A;">
	<div class="list_carousel">
		<ul id="foo">
		
			<!-- WHATSNEX -->
			<!--<li class="box scales paral bloco" style="display: none;background:url(<?=base_url()?>assets/site/img/whatsnex.jpg) right">
				<div class="box2">
					<div class="left">
						<h2>Nex Group</h2>
						<h3><i>Construir é a nossa vida</i></h3>
					</div>
					<div class="right">
						<a href="<?=site_url()?>empresa" class="btn btn-white button">Conheça</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>-->
			<!-- WHATSNEX; -->
			
			<li class="box scales paral bloco" style="display: none;background:url(<?=base_url()?>assets/site/img/banner-tv-bg.jpg) center center">
				<div class="box2 tv">
					<div class="left">
						<img src="<?=base_url()?>assets/site/img/banner-tv-chamada.png" alt="LifeMember" class="nomobile" width="100%" />
						<!--<h2>Life Member</h2>-->
					</div>
					<div class="right">
						<a href="http://www.nexgroup.com.br/lifemember/" class="btn btn-white button" target="_blank">Saiba mais</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			
			<!-- LIFE MEMBER -->
			<!--<li class="box scales paral bloco" style="display: none;background:url(<?=base_url()?>assets/site/img/lifemember.png) center">
				<div class="box2">
					<div class="left">
						<img src="http://www.nexgroup.com.br/lifemember/img/logo.png" alt="LifeMember" class="nomobile" width="26%" />
						<h2 style="display:none">Life Member</h2>
						<h3><i>Promoção exclusiva para proprietários do Life Park. Quem tem Life Park tem muito mais. Uma TV LED 55", por exemplo.</i></h3>
					</div>
					<div class="right">
						<a href="http://www.nexgroup.com.br/lifemember/" class="btn btn-white button" target="_blank">Saiba mais</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>-->

			<!-- NEX INTITUCIONAL -->
			<li class="box scales paral bloco" style="display: none;background:url(<?=base_url()?>assets/site/img/nex5.jpg) right">
				<div class="box2">
					<div class="left">
						<h2>Nex Group</h2>
						<h3><i>Construir é a nossa vida</i></h3>
					</div>
					<div class="right">
						<a href="<?=site_url()?>empresa" class="btn btn-white button">Conheça</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			<!-- NEX INTITUCIONAL; -->


			<!-- NEX 2015 
			<li class="box scales paral bloco" style="background:url(<?=base_url()?>assets/site/img/nex_2015-2.jpg) right">
				<div class="box2">
					<div class="left">
						<h2>Nex 2015</h2>
						<h3><i>O ano da sua conquista já começou</i></h3>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			 NEX 2015; -->



			<?php
			// DESTAQUE DOS EMPREENDIMENTOS
			foreach($destaqueHome as $row) :

			$newUrl = $row->empreendimento."-".$row->cidade."-".$row->estado;
			$url = $this->utilidades->sanitize_title_with_dashes($newUrl);
			$url = site_url().'imoveis/'.$row->id_empreendimento.'/'.$url;

			$fotos = $this->modelImoveis->getFotos($row->id_empreendimento, 20);

			if(count($fotos) > 0) :
			?>
			<li class="box scales paral bloco" style="display: none;background:url(<?=UPLOADS_PATH?>imoveis/midias/<?=@$fotos[0]->arquivo?>)">
				<div class="box2">
					<div class="left">
						<img src="<?=UPLOADS_PATH?>imoveis/logo/<?=$row->logotipo?>" alt="Logo <?=$row->empreendimento?>" class="nomobile" width="26%" />

						<?php
          				if($row->status_label != "")
          				{
          					echo '<span>'.$row->status_label.'</span>'; 
          				}
          				else
          				{
          					if($row->id_status == 1)
          						echo '<span>Lançamento</span>';
          					elseif($row->id_status == 2)
          						echo '<span>Pronto para morar</span>';
          					elseif($row->id_status == 6)
          						echo '<span>100% Vendido</span>';
          				}
          				?>

						<h2 class="yesmobile"><?=$row->empreendimento?></h2>
						<h3><i><?=$row->chamada_empreendimento?></i></h3>
					</div>
					<div class="right">
						<a href="<?=$url?>" class="btn btn-white button">Conheça</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			<?php endif; ?>
			<?php endforeach; ?>

			<!-- NEXCHANGE -->
			<li class="box scales paral bloco" style="display: none;background:url(<?=base_url()?>assets/site/img/nexchange-2.jpg) right">
				<div class="box2">
					<div class="left">
						<h2>Nex Change</h2>
						<h3><i>A melhor troca para seu imóvel usado</i></h3>
					</div>
					<div class="right">
						<a href="http://www.nexgroup.com.br/nexchange" target="_blank" class="btn btn-white button">Conheça</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			<!-- NEXCHANGE; -->



			<!-- bannerMoeda -->
			<!-- <li class="box scales paral bloco" style="background:url(<?=base_url()?>assets/site/img/bannerMoeda.jpg) right">
				<div class="box2">
					<div class="left">
						<h3><i>"É nos períodos de incerteza<br>que surgem as melhores<br>oportunidades de investimento."</i></h3>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li> -->
			<!-- bannerMoeda -->



			<!-- OFERTAS NEX
			<li class="box scales paral bloco" style="display: none;background-color: #C31D31;">
				<div class="box2">
					<div class="left">
						<h2>Ofertas Nex Vendas</h2>
						<h3><i>Veja as melhores ofertas de usados que só a Nex tem para você.</i></h3>
					</div>
					<div class="right">
						<a href="<?=site_url()?>ofertas" title="Ofertas Nex Vendas" class="btn btn-white button">Conheça</a>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
			 OFERTAS NEX -->

	
		</ul>


		<div class="clearfix"></div>
		<div class="navegation">
			<a href="javascript:;" id="prev" class="prev" rel="nofollow"></a>
			<div id="pager" class="pager"></div>
			<a href="javascript:;" id="next" class="next" rel="nofollow"></a>
		</div>
	</div>
</section>
<!-- END DESTAQUE; -->

<div id="ancora2"></div>
<section id="empreendimentos-home">
	<div id="ancora3"></div>
	<section class="submenu-home relativo">
		<div class="row">
			<div class="left clickroll">
				<ul id="aba-filtro-status">
					<li class="li-todos active"><a href="javascript:void(0);" id="todos" data-status="0" onclick="selecionarAbaFiltro(this);">TODOS<span></span></a></li>
					<li class="li-lancamento"><a href="javascript:void(0);" id="lancamento" data-status="1" onclick="selecionarAbaFiltro(this);">Lançamentos<span></span></a></li>
					<li class="li-pronto nomobile"><a href="javascript:void(0);" id="pronto" data-status="2" onclick="selecionarAbaFiltro(this);">pronto <br class="yesmobile">para morar<span></span></a></li>
					<li class="li-ofertas"><a href="javascript:void(0);" id="ofertas" data-status="3" onclick="selecionarAbaFiltro(this);">Classificados NEX Vendas<span></span></a></li>
					<!-- <li class="li-perto"><a href="#ancora3" id="perto" onclick="selecionarAbaFiltro(this);" rel="nofollow">perto de você<span></span></a></li> -->
				</ul>
			</div>
			<div class="right clickroll">
				<ul>
					<li class="li-card active"><a href="javascript:void(0);" id="cards" onclick="selecionarAbaFiltro(this);"><i class="icon icon-th-large "></i></a></li>
					<li class="li-lista nomobile"><a href="javascript:void(0);" id="lista" onclick="selecionarAbaFiltro(this);"><i class="icon icon-list "></i></a></li>
					<!-- <li id="tab-location" class="li-mapa"><a href="#ancora3" id="mapa" onclick="selecionarAbaFiltro(this);" rel="nofollow"><i class="icon icon-map-marker "></i></a></li> -->
				</ul>
			</div>
		</div>
	</section>

	<section class="filtros nomobile">
		<div class="row">
			<div style="clear:both;"></div>
			<form>
				<div class="small-4">
					<select id="filtro-estado">
						<option value="-">Filtrar por Estado:</option>
						<?php //foreach($arrEstados as $id => $estado) { echo '<option value="'.$id.'">'.$estado.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-cidade" onchange="filtrarHomeCard(); filtrarHomeLista();">
						<option value="-">Filtrar por Cidade: Selecione um estado.</option>	
						<?php //foreach($arrCidades as $id => $cidade) { echo '<option class="cidades uf-'.$cidade['id_estado'].'" value="'.$cidade['id'].'" style="display: none;">'.$cidade['cidade'].'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-tipo" onchange="filtrarHomeCard(); filtrarHomeLista();">
						<option value="-">Filtrar por Tipo:</option>
						<?php //foreach($arrTipos as $id => $tipo) { echo '<option value="'.$id.'">'.$tipo.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 filtro-ofertas" style="display:none">
					<select id="filtro-tipo" onchange="filtraOfertas(this);">
						<option value="0">Todas Ofertas</option>
						<?php foreach($tipo_ofertas as $tipo) { echo '<option value="'.$tipo->id_ofertas_tipo.'">'.$tipo->tipo.'</option>'; } ?>
					</select>
				</div>
			</form> 
		</div>
	</section>

	<!-- <section id="tab-mapa" class="cards" style="display:none;">
		<div class="mapa" id="map_canvas"></div>
	</section> -->

	<section id="tab-cards" class="cards" style="display:;">
		<div class="tab-todos">
			<div class="row"><div id="div-imoveisDestaque" class="list_carousel"></div></div>
		</div>
	</section>

	<section id="tab-lista" class="lista" style="display:none;">
		<div class="tab-todos">
			<div class="row"><div id="div-imoveisLista"></div></div>
		</div>
	</section>  

		<!-- LISTA -->
	<section id="tab-ofertas" class="ofertas lista" style="display:none;">
		<div class="tab-todos">
			<div class="row">
				<div id="div-imoveisOfertas">


					<?php

					if($ofertas) :
					foreach ($ofertas as $row) :

					$img = UPLOADS_PATH . 'ofertas/zoom/' . $row->img;

					?>


					<div class="home-emp emp oferta-tipo-<?=$row->id_ofertas_tipo?>">
						<figure class="scales" style="background: url('<?=$img?>')"></figure>
						<div class="emp-content">
							<ul class="up">
								<li class="one" style=''>
									<h2><?=$row->titulo?></h2>
									<?php if(intval($row->valor) > 0) : ?>
									<h3 style='margin-top: 10px;'>
										<?php if($row->valor_tipo == 'de_por') : ?>

										De
										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor_de, 2, ',', '.')?></span>

										Por
										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor, 2, ',', '.')?></span>

										<?php else : ?>

										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor, 2, ',', '.')?></span>

										<?php endif; ?>

										<?php if($row->valor_frase != "") { ?>
										<span style="color: #B72025;font-weight: 600;"><i><?=$row->valor_frase?></i></span>
										<?php } ?>
									</h3>
									<?php endif; ?>
								</li>
								<li class="two" style=''>
									<a href="<?=base_url().'oferta/'.$row->url?>" class="btn small expand btn-red button" style='display: block;margin-top: 15px;'>Conheça</a>
								</li>
							</ul>
							<ul class="down">
								<li><i class="icon icon-tag"></i><p>Oferta Nex</p></li>
								<?php if($row->cidade != "") { ?><li><i class="icon icon-map-marker"></i><p><?=$row->cidade?></p></li><?php } ?>
								
								<!-- <li><i class="icones-img one"></i><p>2 e 3 dormitórios com suíte</p></li>		 -->
								
								<li><i class="icones-img "></i><p><?=$row->tipo?> <?=($row->comodos) ? $row->comodos : ''?></p></li>
								<?php if($row->vagas != "") { ?><li><i class="icones-img tree"></i><p><?=$row->vagas?></p></li><?php } ?>			
							</ul>
						</div>
					</div>


					<?php
					endforeach;
					endif;
					?>


				</div>
			</div>
		</div>
	</section>  
	<!-- LISTA ENDS --> 
	
</section>
<script type="text/javascript">
function filtraOfertas(object)
{
	if( $(object).val() > 0) { $('#tab-ofertas .emp').hide(10, function() { $('#tab-ofertas .oferta-tipo-'+ $(object).val()).show(); }); return true; }

	$('#tab-ofertas .emp').show();
	return true;
}
</script>
<?php $this->load->view("includes/imoveis_visitados"); ?>

<!-- NOTICIAS -->
<section class="noticias-home">
	<div class="row">
		<div class="top">
			<ul class="left">
				<li class="nomobile"><i class="icon icon-file-text"></i></li>
				<li>
					<h5>NOTÍCIAS</h5>
					<h6><i>Fique por dentro e ...</i></h6>
				</li>
			</ul >
			<ul class="right nomobile">
				<li><a href="<?=site_url()?>noticias" title="Ver todas as notícias">ver todas as notícias ></a></li>
			</ul>
		</div>
		<div class="list_carousel">
			<ul id="foo3">
				<?php
				if(@$noticias) :
				foreach(@$noticias as $row) : 

				$url = $this->utilidades->sanitize_title_with_dashes($row->titulo);
				$url = site_url().'noticias/'.$row->id_noticia.'/'.$url;

				if(site_url() != 'http://www.nexgroup.com.br/newnex/')
				{
					$noticia_file_headers = @get_headers(UPLOADS_PATH.'noticias/medium/'.$row->imagem_destaque);

					if($noticia_file_headers[0] == 'HTTP/1.0 404 Not Found')
						$bg_noticia = base_url().'assets/site/img/bg-default.png';
					else 
						$bg_noticia = UPLOADS_PATH.'noticias/medium/'.$row->imagem_destaque;
				}
				else
				{
					if(file_exists('uploads/noticias/medium/'.$row->imagem_destaque) && !is_dir('uploads/noticias/medium/'.$row->imagem_destaque))
						$bg_noticia = base_url().'uploads/noticias/medium/'.$row->imagem_destaque;
					else
						$bg_noticia = base_url().'assets/site/img/bg-default.png';
				}
				?>
				<li class="box scales" style="background:url('<?=$bg_noticia?>'); ">
					<div class="left">
						<h2><?=character_limiter($row->titulo,48)?></h2>
						<a href="<?=$url?>" title="<?=$row->titulo?>" class="btn btn-white button">Ver Mais</a>
					</div>
					<div class="data">
						<?=strftime("%d", strtotime(@$row->data_cadastro))?><br>
						<b><?=utf8_encode(strftime("%b", strtotime(@$row->data_cadastro)))?></b>
					</div>
					<div class="pix"></div>
					<div class="black"></div>
				</li>
				<?php endforeach; ?>
				<?php else : ?>
				<li><p>Nenhuma notícia encontrada</p></li>
				<?php endif; ?>
			</ul>
			<div class="clearfix"></div>
			<div class="navegation">
				<a href="javascript:;" id="prev3" class="prev nomobile" rel="nofollow"></a>
				<div id="pager3" class="pager"></div>
				<a href="javascript:;" id="next3" class="next nomobile" rel="nofollow"></a>
			</div>
		</div>
	</div>
</section>
<!-- NOTICIAS; -->

<section id="desk-bottom-container">
	<a class="desk-bottom small-4" href="https://www.youtube.com/channel/UCKmLkk5EC927QPml79HPIUg" target="_blank">
		<div class="bg-pix"></div>
		<div class="black"></div>
		<div class="bg one"></div>
		<span>
			<h2>NEX TV</h2>
			<h3><i>Confira o canal online da Nex</i></h3>
		</span>
	</a>
	<a class="desk-bottom small-4 click_revista">
		<div class="bg-pix"></div>
		<div class="black"></div>
		<div class="bg two" style="background-image:url(<?=base_url().'uploads/revista/capa/'.$revistaAtual->capa?>);"></div>
		<span>
			<h2>REVISTA NEXDAY</h2>
			<h3><i>Conheça a Nex Day</i></h3>
		</span>
	</a>
	<a class="desk-bottom small-4" href="https://www.facebook.com/NexGroup?fref=ts" target="_blank">
		<div class="bg-pix"></div>
		<div class="black"></div>
		<div class="bg tree"></div>
		<span>
			<h2>Fan Page da NEX</h2>
			<h3><i>A vida da Nex está aqui</i></h3>
		</span>
	</a>
</section>

<div id="newsletter">
	<div class="row">
		<div class="one"><h5>FIQUE POR DENTRO DA NEX GROUP</h5></div> 
		<div class="two"><i>Cadastre-se e fique por dentro de tudo que acontece no Nex Group.</i></div> 
		<form>
			<div class="tree">
				<label><input type="text" placeholder="SEU NOME" id="ipt-newsletter-your-name" /></label>
			</div> 
			<div class="tree">
				<label><input type="text" placeholder="SEU E-MAIL" id="ipt-newsletter-your-email" /></label>
			</div> 
			<div class="four">
				<a href="javascript:void(0);" onclick="validaNewsletter();" id="btn-enviar-newsletter" class="btn small btn-white button">ENVIAR</a>
			</div> 
		</form>
	</div>
</div>

<?php $this->load->view("includes/footer-home"); ?>