<?php $this->load->view("includes/header"); ?>
	<section id="error-404" class="Main Inner">
		<div class="Bg2">
			<div class="Wrapper">
				<header>
					<hgroup>
						<h2>P&Aacute;GINA <span>N&Atilde;O ENCONTRADA</span></h2>
						<h3>utilize o campo de pesquisa no topo ou use os links abaixo</h3>
					</hgroup>
				</header>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>" title="Home">Home</a></li>
					<li><a href="<?=site_url()?>navegue-pelo-mapa" title="Navegue pelo mapa">Navegue pelo mapa</a></li>
                    <li><a href="<?=site_url()?>revista-nexday" title="Revista Nexday">Revista Nex Day</a></li>
					<!--<li><a  href="<?=site_url()?>#navigation-help" title="Fique por dentro do Nex Group">Fique por dentro do Nex Group</a></li>-->
				</ul>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Im&oacute;veis</a></li>
					<li><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Im&oacute;veis</a></li>
					<li><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Apartamentos</a></li>
					<li><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Casas em condom&iacute;nio</a></li>
					<li><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Coberturas</a></li>
					<li><a href="<?=site_url()?>imoveis/lista" title="Im&oacute;veis">Im&oacute;veis comerciais</a></li>
				</ul>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>empresa" title="A Empresa">A Empresa</a></li>
					  <li><a href="<?=site_url()?>empresa/obras-realizadas" title="Obras realizadas">Obras realizadas</a></li>
                      	<li class="Exception2"><a href="<?=site_url()?>empresa/obras-realizadas/capa-engenharia" title="Capa Engenharia">Capa Engenharia</a></li>
                      	<li class="Exception2"><a href="<?=site_url()?>empresa/obras-realizadas/dhz-construcoes" title="Dhz Construções">Dhz Construções</a></li>
                        <li class="Exception2"><a href="<?=site_url()?>empresa/obras-realizadas/egl-engenharia" title="Egl Engenharia">Egl Engenharia</a></li>
                        <li class="Exception2"><a href="<?=site_url()?>empresa/obras-realizadas/lomando-aita" title="Lomando Aita">Lomando Aita</a></li>
				</ul>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>central-de-vendas" title="Central de Vendas">Central de Vendas</a></li>
					<li><a class="Modal" href="<?=site_url()?>atendimento-email" title="">Atendimento por e-mail</a></li>
					<li><a class="Modal" href="<?=site_url()?>ligamos-para-voce" title="">Ligamos para voc&ecirc;</a></li>
					<li><a href="javascript:window.open('http://web.atendimentoaovivo.com.br/chat.asp?idc=5447&amp;pre_empresa=1192&amp;pre_depto=1276&amp;vivocustom=&amp;vivocustom2=DHZ&amp;vivocustom3=Site+DHZ&amp;vivocustom4=Rodape&amp;idtemplate=0','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" rel="contact" title="Corretor Online">Corretor online</a></li>
				</ul>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>noticias" title="Not&iacute;cias">Not&iacute;cias</a></li>
					<li><a href="<?=site_url()?>noticias" title="Not&iacute;cias Nex Group">Not&iacute;cias Nex Group</a></li>
					<li><a href="<?=site_url()?>noticias" title="Not&iacute;cias cotidiano">Not&iacute;cias cotidiano</a></li>
				</ul>
				<ul>
					<li class="Exception"><a href="<?=site_url()?>contato" title="Contato">Contato</a></li>
					<li><a href="<?=site_url()?>contato/fale-conosco" title="Fale conosco">Fale conosco</a></li>
					<li><a href="<?=site_url()?>contato/venda-seu-terreno" title="Venda o seu terreno">Venda o seu terreno</a></li>
					<li><a href="<?=site_url()?>contato/seja-um-fornecedor" title="Seja um fornecedor">Seja um fornecedor</a></li>
					<li><a href="<?=site_url()?>contato/trabalhe-conosco" title="Trabalhe conosco">Trabalhe conosco</a></li>
				</ul>
			</div>
		</div>
	</section>
<?php $this->load->view("includes/footer"); ?>