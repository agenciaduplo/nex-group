<?php $this->load->view("includes/header-emp"); ?>
<header class="main-header">
	<div class="container relative">
		
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="hide label-mude-agora">
		<div class="left">
			<a href="http://www.nexgroup.com.br/" class="logo hide-mobile hide-tablet">
				<h1 class="title">Nex Group</h1>
			</a>
		</div>
		<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-emp.png?ver=0.1" class="logo-emp">  
		<div class="right btns">
				<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/header', 'click');">
					<img class="imgmax100 hide-mobile" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/btn-phone.png?ver=0.1">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-phone.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left rnormal">CLIQUE<br /><b class="rbold">LIGUE</b></span>
				</a>
				<a class="click_chat" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=27&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor', 'click');">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left">CHAT</span>
				</a>
		</div>
		
		<div class="chatbox hide-mobile">
			<iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=27&amp;midia=<?=$ref; ?>"></iframe>
			<div class="toggle-chat whitecolor"><img style="-webkit-filter: brightness(10);filter: brightness(10);" width="30" class="left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2"><span>- FECHAR</span></div> 
		</div>
		
		<form id="fixedFormContact" class="hide hide-tablet hide-mobile">
		
	</div>
</header>

<section class="banner"> 
	<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/destaque-230.png?ver=0.3" class="hide label-destaque">
	<div class="container">
	<img src="http://www.nexgroup.com.br/assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
		<!--<img src="http://www.nexgroup.com.br/assets/site/img/banner-info.png">-->
	</div>
</section>
<div class="preco hide yellowbg">
		<div class="row whitecolor rbold fs35 img100 nomargin">
			<div class="col-md-12">A <span class="blackcolor">PARTIR</span> DE</div>
		</div>
		<div class="row whitecolor rbold fs50 img100 nomargin" style="font-size:47px;line-height:30px;">
			<div class="col-md-12"><span class="blackcolor fs40">R$</span> <span style="text-shadow: 2px 2px #1e2329;">230.000,00</span></div>
		</div>
		<div class="row blackcolor rbold img100 nomargin" style="line-height:15px;">
			<div class="col-md-12 right">&agrave; vista</div>
			<div class="spacer"></div>
		</div>
	</div>
<div class="spacer"></div>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 hide-mobile">
				<h1 class="lato300 themecolor fs45 tcenter">CONHE&Ccedil;A O CONCEITO DE ARQUITETURA<br> ABERTA, MORE NA SUA &Eacute;POCA.</h1>
				<h4 class="lato300 fs30 tcenter">0, 1, 2 OU X DORMS.</h2>
			</div>
			<div class="col-md-10 hide show-mobile">
				<h1 class="lato300 themecolor fs35 tcenter">CONHE&Ccedil;A O CONCEITO DE ARQUITETURA ABERTA, <br>MORE NA SUA &Eacute;POCA.</h1>
				<h4 class="lato300 fs35 tcenter">0, 1, 2 OU X DORMS.</h2>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="info hide show-mobile show-tablet acc">
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/autohaus', 'click');">Auto<span>Haus</span></h1>
	<div><p class="lato300">Já imaginou diminuir a luz, fechar as cortinas e ligar o som com um simples toque no seu celular?</p></div>
	
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxlight', 'click');"><span>Max</span>light</h1>
	<div><p class="lato300">Aqui são as janelas que se adequam a sua decoração.</p></div>
	
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxdoor', 'click');"><span>Max</span>door</h1>
	<div><p class="lato300">Para entrar em casa com o pé direito, nada melhor do que uma porta com design premiado internacionalmente.</p></div>
					
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxswitch', 'click');"><span>Max</span>switch</h1>
	<div><p class="lato300">Mais do que acender ou apagar as luzes, regule a intensidade delas de forma integrada.</p></div>
					
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxbagno', 'click');"><span>Max</span>Bagno</h1>
	<div><p class="lato300">Para dar um banho nos outros apartamentos: duchas com padrão alemão, cuba e tampo em cristal, com pastilha de vidro e piso de porcelanato.</p></div>
				
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxkitchen', 'click');"><span>Max</span>Kitchen</h1>
	<div><p class="lato300">Um ingrediente que não pode faltar na sua casa é uma cozinha multifuncional e adaptável.</p></div>
	
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxcuba', 'click');"><span>Max</span>cuba</h1>
	<div><p class="lato300">Como a gente não sabe quanto tempo você vai morar aqui, a cuba vem com garantia eterna.</p></div>
					
	<h1 class="block-title" onclick="ga('send', 'event', 'aba/maxschindler', 'click');"><span>Max</span>schindler</h1>
	<div><p class="lato300">Seus conhecimentos sobre arte vão para outro nível. Nossos elevadores com vidro transparente exibem uma arte diferente a cada andar.</p></div>		
</section>

<section class="info hide-mobile hide-tablet lato300">
	<div class="container">
		<div class="row">
				<div class="col-md-5 block padding40" style="min-height:343px;">
					<h1 class="block-title">Auto<span>Haus</span></h1>
					<p>Já imaginou diminuir a luz, fechar as cortinas e ligar o som com um simples toque no seu celular?</p>
				</div>
				<div class="col-md-2">
					<img style="min-height:343px;" class="img100 right" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/img-autohaus.jpg">
				</div>
				<div class="col-md-5 block padding40" style="min-height:343px;">
					<h1 class="block-title"><span>Max</span>light</h1>
					<p>Aqui são as janelas que se adequam a sua decoração.</p>
				</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-7" style="padding-left:0px;">
				<div class="col-md-12 block padding40" style="min-height:273px;">
					<h1 class="block-title"><span>Max</span>door</h1>
					<p>Para entrar em casa com o pé direito, nada melhor do que uma porta com design premiado internacionalmente.</p>
				</div>
			</div>
			<div class="col-md-5 no-padding">
				<img class="img100" style="min-height:273px;" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/img-maxdoor.jpg">
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-5 no-padding">
				<img class="img100" style="min-height:307px;" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/imgmax-switch.jpg">
			</div>
			<div class="col-md-7" style="padding-right:0px;">
				<div class="col-md-12 block padding40" style="min-height:307px;">
					<h1 class="block-title"><span>Max</span>switch</h1>
					<p>Mais do que acender ou apagar as luzes, regule a intensidade delas de forma integrada.</p>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-5 block padding40" style="min-height:343px;">
				<h1 class="block-title"><span>Max</span>Bagno</h1>
				<p>Para dar um banho nos outros apartamentos: duchas com padrão alemão, cuba e tampo em cristal, com pastilha de vidro e piso de porcelanato.</p>
			</div>
			<div class="col-md-2">
				<img class="img100 right" style="min-height:343px;" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/img-maxbagno.jpg">
			</div>
			<div class="col-md-5 block padding40" style="min-height:343px;">
				<h1 class="block-title"><span>Max</span>Kitchen</h1>
				<p>Um ingrediente que não pode faltar na sua casa é uma cozinha multifuncional e adaptável.</p>
			</div>
		</div>
		
		
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-7" style="padding-left:0px;">
				<div class="col-md-12 block padding40" style="min-height:273px;">
					<h1 class="block-title"><span>Max</span>cuba</h1>
					<p>Como a gente não sabe quanto tempo você vai morar aqui, a cuba vem com garantia eterna.</p>
				</div>
			</div>
			<div class="col-md-5 no-padding">
				<img class="img100" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/img-maxcuba.jpg">
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-5 no-padding">
				<img class="img100" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/img-maxschindler.jpg">
			</div>
			<div class="col-md-7" style="padding-right:0px;">
				<div class="col-md-12 block padding40" style="min-height:307px;">
					<h1 class="block-title"><span>Max</span>schindler</h1>
					<p>Seus conhecimentos sobre arte vão para outro nível. Nossos elevadores com vidro transparente exibem uma arte diferente a cada andar.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer hide show-mobile"></div>

<section class="mapa noise">
	<div class="container">
		<div class="row">
				<div class="col-md-12">
					<h1 class="rlight whitecolor tcenter fs45">LOCALIZA&Ccedil;&Atilde;O</h1>
				</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-12">
				<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/mapa.png?ver=2" class="img-mapa img100">
			</div>
		</div>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
</section>

<footer class="">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-emp.png?ver=1" class="imgmax100 logo-max-footer" ></div>
			<div class="col-md-3 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-footer.png?ver=1" class="imgmax100" style="margin-bottom: -11px;"></div>
			<div class="col-md-3 col-sm-12 col-xs-12"></div>
			<!--<div class="col-md-4 col-sm-12  col-xs-12 whitecolor fs12">
				<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>
				*Preço base: janeiro/2016. Valor ref. à unidade 304, <br class="hide-mobile"> torre E, 50,98m2 privativos, box 149, 2 dormitórios. <br class="hide-mobile">Imagens meramente ilustrativas.
				<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>
			</div>-->
		</div>
	</div>
	<section class="rights">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12"></div>
				<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/rights1.png" class="left no-float-mobile"></div>
				<div class="col-md-1 col-sm-6 col-xs-12 tcenter"><a href="http://agenciaduplo.com.br" target="_blank"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-duplo1.png" class="right no-float-mobile"></a></div>
			</div>
	</section>
</footer>

<?php $this->load->view("includes/footer-emp"); ?>