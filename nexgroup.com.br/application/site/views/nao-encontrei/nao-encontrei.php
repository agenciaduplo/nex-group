<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="pt-br" class="ie6 no-js"> <![endif]-->
<!--[if IE 7 ]>    <html lang="pt-br" class="ie7 no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="pt-br" class=" no-js"> <!--<![endif]-->
	<head>
		<title><?=@$title?></title>
		
		<!-- METAS -->
		<?php if(@$pagetag=='imovelvisualizar'):
				if(@$imovel->tags_description):?>
					<meta name="description" content="<?=@$imovel->tags_description?>" />
				<?php else:?>
					<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />	
				<?php endif;
				
				if(@$imovel->tags_keywords):?>
					<meta name="keywords" content="<?=@$imovel->tags_keywords?>" />
				<?php else:?>
					<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
				<?php endif;?>
				
	    <?php else:?>
	    		<?php if(@$description):?>
	    				<meta name="description" content="<?=@$description?>" />
	    		<?php else: ?>
	    				<meta name="description" content="A maior incorporadora genuinamente gaúcha nasceu grande para ser ainda maior." />
	    		<?php endif;?>
	    		<?php if(@$description):?>
	    				<meta name="keywords" content="<?=$keywords?>" />
	    		<?php else: ?>
	    				<meta name="keywords" content="nex group, nexgroup, nex, group, capa engenharia, capa, egl engenharia, egl, dhz, dhz construções, lomando aita, lomando, aita" />
		    	<?php endif;?>
		<?php endif;?>
		    	
		<!-- STYLE -->
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/main.css" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
		<!-- STYLE -->
		<!-- LIBS -->
		<script type="text/javascript" src="<?=base_url()?>assets/site/js/lib/jquery-1.6.1.min.js"></script>
		<!-- LIBS -->
		<!-- SCRIPTS -->
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput-1.3.min.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/source/validations.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.placeholder.js" type="text/javascript"></script>
		<!-- SCRIPTS -->
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="contact">
			<form method="post" id="frm-newsletter">
				<fieldset>
					<h2>N&atilde;o encontrou <span>seu im&oacute;vel?</span></h2>
					<div>
						<label class="" for="ipt-newsletter-your-name">nome*</label>
						<input class="Text" id="ipt-newsletter-your-name" name="seu_nome" type="text" value=""/>
					</div>
					<div>
						<label for="ipt-newsletter-your-email">email*</label>
						<input class="Text" id="ipt-newsletter-your-email" name="seu_email"  type="text" value=""/>
					</div>
					<div>
						<label for="ipt-newsletter-your-phone-area">telefone*</label>
						<input class="Text Dig3" id="ipt-newsletter-your-phone-area" name="telefone_area"  type="text" value=""/>
						<input class="Text Dig8" id="ipt-newsletter-your-phone" name="telefone" type="text" value=""/>
					</div>
					<div class="Exception">
						<label class="">contate-me por*</label>
						<input class="Radio" id="ipt-newsletter-email-opt" name="opt_contact" type="radio" value=""/>
						<label class="Exception" for="ipt-newsletter-email-opt">email</label>
						<input class="Radio" id="ipt-newsletter-phone-opt" name="opt_contact" type="radio" value=""/>
						<label class="Exception" for="ipt-newsletter-phone-opt">telefone</label>
					</div>
					<div>
						<label for="ipt-newsletter-message">mensagem*</label>
						<textarea class="Text"  id="ipt-newsletter-message" name="mensagem"></textarea><br/>
						<span class="AlertFrm">Os campos com asterisco (*) s&atilde;o de preenchimento obrigat&oacute;rio.</span>
					</div>
					<button class="BtnRed" type="submit">enviar</button>
					
				</fieldset>
			</form>
			<div id="sucess-newsletter" class="Sucess">
				<hgroup>
					<h3>Mensagem enviada com sucesso!</h3>
					<h4>em breve nossa equipe entrar&aacute; em contato com voc&ecirc;!</h4>
				</hgroup>
				<a id="fancybox-close" class="BtnGrey">Voltar</a>
			</div>
		</div>
		
		<div id="conversaoAdwords">

		</div>
	</body>
</html>