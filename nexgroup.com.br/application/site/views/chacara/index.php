<?php $this->load->view("includes/header-emp"); ?>
<header class="main-header">
	<div class="container" style="position: relative;">
		
		<div class="left hide-tablet hide-mobile">
			<a href="http://www.nexgroup.com.br/" class="logo">
				<h1 class="title">Nex Group</h1>
			</a>
		</div>
		<img src="<?=base_url()?>assets/site/img/logo-chacara-2.png" class="logo-chacara">  
		<div class="right hlinks">
			<span class="pric">
				<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/header', 'click');">
					<i class="icon icon-phone"></i>
					<span class="t400" style="margin-top: -5px">(51) 3092.3124</span>
				</a>
				<a style="float:right;" class="show-only-mobile hide-tablet hide" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=25&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor/header', 'click');">
					<i class="icon icon-comments"></i>
					<span class="t400" style="margin-top: -5px">CHAT</span>
				</a>
				<!--<a href="http://www.nexgroup.com.br/contato" title="Contato Nex">
					<i class="icon icon-envelope"></i>
					<span class="t400" style="margin-top: -5px">Nex Group</span>
				</a>-->
			</span>
		</div>

		<div class="chatbox hide-mobile">
			<iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=25&amp;midia=<?=$ref; ?>"></iframe>
			<div class="toggle-chat themebg whitecolor"><img width="30" class="left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2"><span>- FECHAR</span></div> 
		</div>
	</div>
</header>

<section class="banner hide-mobile hide-tablet"> 
	<div class="container">
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
		<img src="<?=base_url()?>assets/site/img/chacara/destaque.png" class="label-destaque">
		<!--<img src="http://www.nexgroup.com.br/assets/site/img/banner-info.png">-->
	</div>
</section>

<section class="banner banner-mobile hide show-mobile"> 
	<div class="container">
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
		<img src="<?=base_url()?>assets/site/img/chacara/destaque.png" class="label-destaque">
		<!--<img src="http://www.nexgroup.com.br/assets/site/img/banner-info.png">-->
	</div>
</section>

<div class="spacer"></div>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h1 class="lato700 themecolor tcenter">SOBRADOS E CASAS TÉRREAS DE 3 DORMITÓRIOS <br class="hide-mobile hide-tablet">COM SUÍTE, PÁTIO E CHURRASQUEIRA.</h1>
				<h2 class="latoi300 themecolor tcenter">Grande, aconchegante e com um pátio feito para você. Venha<br class="hide-mobile hide-tablet"> morar na casa ideal para todos os seus sonhos.</h2>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="imoveis">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="rtitle">
					<h5 class="themebg whitecolor left">Casas t&eacute;rreas</h5>
				</div>
			</div>
		</div>
		<div class="row hide-tablet hide-mobile">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/casa-terrea-1.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/casa-terrea-1', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-1.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/casa-terrea-2.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/casa-terrea-2', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-2.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/casa-terrea-3.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/casa-terrea-3', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-3.png" class="img-display">
				</a>
			</div>
		</div>
		
		<div class="row hide show-mobile show-tablet">
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-1.png" class="img-display">
			</div>
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-2.png" class="img-display">
			</div>
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/casa-terrea-3.png" class="img-display">
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer hide show-mobile"></div>

<section class="imoveis">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="rtitle">
					<h5 class="themebg whitecolor left">Sobrados</h5>
				</div>
			</div>
		</div>
		<div class="row hide-tablet hide-mobile">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/sobrado-1.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/sobrado-1', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/sobrado-1.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/sobrado-2.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/sobrado-2', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/sobrado-2.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/chacara/sobrado-3.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'foto/sobrado-3', 'click');">
					<img src="<?=base_url()?>assets/site/img/chacara/sobrado-3.png" class="img-display">
				</a> 
			</div>
		</div>
		<div class="row hide show-mobile show-tablet">
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/sobrado-1.png" class="img-display">
			</div>
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/sobrado-2.png" class="img-display">
			</div>
			<div class="col-md-4">
				<img src="<?=base_url()?>assets/site/img/chacara/sobrado-3.png" class="img-display">
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer"></div>


<section class="info-links">

</section>


<section class="mapa themebggradient">
	<div class="container">
		<div class="row">
				<div class="col-md-12">
					<h1 class="lato700 whitecolor tcenter">Mapa de localiza&ccedil;&atilde;o</h1>
				</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<img src="<?=base_url()?>assets/site/img/chacara/mapa.png" class="img-mapa img100 hide-mobile hide-tablet">
				<img src="<?=base_url()?>assets/site/img/chacara/mapa-mobile.png" class="img-mapa img100 hide show-mobile">
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="form">
	<div class="container">
		<form id="formContact">
			<div class="row">
				<div class="col-md-12">
					<h1 class="lato700 themecolor tcenter">Se interessou?</h1>
					<h2 class="lato400 themecolor tcenter">Solicite um contato de nossa equipe:</h2>
				</div>
			</div>
			<div class="spacer"></div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-12">
							<input name="nome" id="nome" type="text" class="field required themecolor" placeholder="Nome (obrigatório)">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<input name="email" id="email" type="text" class="field required themecolor" placeholder="E-mail (obrigatório)">
						</div>
						<div class="col-md-6">
							<input name="telefone" type="text" class="field themecolor telefone" placeholder="Telefone">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">		
							<textarea name="msg" class="field themecolor" placeholder="Mensagem" rows="4"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="error-messages"></div>
						</div>
						<div class="col-md-4">		
							<input type="hidden" id="is_mobile" name="is_mobile" value="yes" class="hide show-mobile">
							<input type="hidden" name="ref" value="<?=$ref; ?>">
							<input type="submit" id="btn-enviar" class="btn themebg whitecolor lato400" value="ENVIAR">
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			
		</form>
	</div>
</section>

<div class="spacer"></div>

<footer class="themebg">
	<div class="spacer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/site/img/chacara/icon-footer-chacara.png" class="img100"></div>
			<div class="col-md-3 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/site/img/chacara/icon-footer-inc.png" class="img100"></div>
		</div>
	</div>
	<div class="spacer"></div>
	<section class="rights">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12"></div>
				<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/site/img/chacara/rights1.png" class="left no-float-mobile"></div>
				<div class="col-md-1 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/site/img/chacara/logo-duplo1.png" class="right no-float-mobile"></div>
			</div>
	</section>
</footer>

<?php $this->load->view("includes/footer-emp"); ?>