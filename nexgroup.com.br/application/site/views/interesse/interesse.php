<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="pt-br" class="ie6 no-js"> <![endif]-->
<!--[if IE 7 ]>    <html lang="pt-br" class="ie7 no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="pt-br" class=" no-js"> <!--<![endif]-->
	<head>
		<title>Interesse - NEX GROUP</title>
		<!-- STYLE -->
		<link rel="stylesheet" href="<?=base_url()?>assets/site/css/main.css" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
		<!-- STYLE -->
		<!-- LIBS -->
		<script type="text/javascript" src="<?=base_url()?>assets/site/js/lib/jquery-1.6.1.min.js"></script>
		<!-- LIBS -->
		<!-- SCRIPTS -->
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.maskedinput-1.3.min.js" type="text/javascript" ></script>
		<script src="<?=base_url()?>assets/site/js/source/validations.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/site/js/plugins/jquery.placeholder.js" type="text/javascript"></script>
		<!-- SCRIPTS -->
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="contact">
			<form method="post" id="frm-have-an-interest" action="">
			<input id="id_empreendimento" name="id_empreendimento"  type="hidden" value="<?=$id_empreendimento?>"/>
				<fieldset>
					<h2>Tenho <span>Interesse</span></h2>
					<div>
						<label for="ipt-have-an-interest-your-name">nome*</label>
						<input class="Text" id="ipt-have-an-interest-your-name" name="nome" type="text" value=""/>
					</div>
					<div>
						<label for="ipt-have-an-interest-your-email">email*</label>
						<input class="Text" id="ipt-have-an-interest-your-email" name="email" type="text" value=""/>
					</div>
					<div>
						<label for="ipt-have-an-interest-your-phone-area">telefone</label>
						<input class="Text Dig3" id="ipt-have-an-interest-your-phone-area" name="telefone_area"   type="text" value=""/>
						<input class="Text Dig8" id="ipt-have-an-interest-your-phone" name="telefone"  type="text" value=""/>
					</div>
					<div class="Exception">
						<label class="">contate-me por</label>
						<input class="Radio " id="ipt-have-an-interest-email-opt" name="opt_contact" checked="checked"  type="radio" value=""/>
						<label class="Exception " for="ipt-have-an-interest-email-opt">email</label>
						<input class="Radio " id="ipt-have-an-interest-phone-opt" name="opt_contact"  type="radio" value=""/>
						<label class="Exception " for="ipt-have-an-interest-phone-opt">telefone</label>
					</div>
					<div>
						<label for="ipt-have-an-interest-message">mensagem</label>
						<textarea class="Text"  id="ipt-have-an-interest-message" name="mensagem" ></textarea><br/>
						<span class="AlertFrm">Os campos com asterisco (*) s&atilde;o de preenchimento obrigat&oacute;rio.</span>
					</div>
					<button class="BtnRed" type="submit" onClick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Botão Enviar - <?=@$empreendimento?>'])">enviar</button>
				</fieldset>
			</form>
			<div class="Sucess" id="sucess-have-an-interest">
				<hgroup>
					<h3>Mensagem enviada com sucesso!</h3>
					<h4>em breve nossa equipe entrar&aacute; em contato com voc&ecirc;!</h4>
				</hgroup>
				<a id="fancybox-close" class="BtnGrey">Voltar</a>
			</div>

			<div id="conversaoAdwords">

			</div>

		</div>
	</body>
</html>
		
