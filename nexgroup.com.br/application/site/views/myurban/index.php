<?php $this->load->view("includes/header-emp"); ?>
<header class="main-header">
	<div class="container relative">
		
		<div class="left">
			<a href="http://www.nexgroup.com.br/" class="logo hide-mobile hide-tablet">
				<h1 class="title">Nex Group</h1>
			</a>
		</div>
		<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-myurban.png?ver=0.1" class="logo-myurban">  
		<div class="right btns">
				<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/header', 'click');">
					<img class="imgmax100 hide-mobile" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/btn-phone.jpg?ver=0.1">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-phone.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left rnormal">CLIQUE<br /><b class="rbold">LIGUE</b></span>
				</a>
				<a class="click_chat" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=26&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor', 'click');">
					<img class="hide show-mobile hide-tablet left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2">
					<span class="hide show-mobile hide-tablet themecolor fs25 left">CHAT</span>
				</a>
		</div>
		
		<div class="chatbox hide-mobile">
			<iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=26&amp;midia=<?=$ref; ?>"></iframe>
			<div class="toggle-chat themebg themecolor"><img width="30" class="left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2"><span>- FECHAR</span></div> 
		</div>
		
		<form id="fixedFormContact" class="hide hide-tablet hide-mobile">
		
	</div>
</header>

<section class="banner">
	<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/destaque-220.png?ver=0.3" class="label-destaque">
	<div class="container">
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
		<!--<img src="http://www.nexgroup.com.br/assets/site/img/banner-info.png">-->
	</div>
</section>
<div class="preco hide show-mobile yellowbg">
		<div class="row whitecolor rbold fs35 img100 nomargin">
			<div class="col-md-12">A <span class="blackcolor">PARTIR</span> DE</div>
		</div>
		<div class="row whitecolor rbold fs50 img100 nomargin" style="font-size:47px;line-height:30px;">
			<div class="col-md-12"><span class="blackcolor fs40">R$</span> <span style="text-shadow: 2px 2px #1e2329;">220.000,00</span></div>
		</div>
		<div class="row blackcolor rbold img100 nomargin" style="line-height:15px;">
			<div class="col-md-12 right">&agrave; vista</div>
			<div class="spacer"></div>
		</div>
	</div>
<div class="spacer"></div>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h1 class="rextra whitecolor fs45">MAIS DO QUE PRONTO PARA MORAR,</h1>
				<h2 class="rsemi whitecolor fs50">PRONTO PARA VOC&Ecirc; REALIZAR.</h2>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="imoveis hide-mobile">
	<div class="container">
		
		<div class="row row-less-padding right no-float-mobile">
			<div class="col-md-4 first">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-1.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_myurban_01', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-1.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-2.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_myurban_02', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-2.png" class="img-display">
				</a>
			</div>
			<div class="col-md-4 last">
				<a href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-3.png" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_myurban_03', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-3.png" class="img-display">
				</a>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="spacer"></div>
	</div>
</section>

<section class="imoveis hide show-mobile" style="width:100%;">
	<style>
		.swiper-container {
			width: 100%;
			height: 100%;
		}
		.swiper-slide {
			text-align: center;
			font-size: 18px;
			background: #fff;
			/* Center slide text vertically */
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			-webkit-justify-content: center;
			justify-content: center;
			-webkit-box-align: center;
			-ms-flex-align: center;
			-webkit-align-items: center;
			align-items: center;
		}
		.swiper-pagination-bullet-active {
			background: #066377;
		}
	</style>
	<div class="container">
		<div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-1.png" class="img100"></div>
					<div class="swiper-slide"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-2.png" class="img100"></div>
					<div class="swiper-slide"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/myurban-3.png" class="img100"></div>
				</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>	
		</div>
	</div>
</section>
<div class="spacer"></div>
<div class="spacer hide show-mobile"></div>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="rlight whitecolor tcenter fs45">A VIDA MODERNA &Eacute; MAIS MODERNA <span class="themecolor rbold">AQUI</span></h2>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-custom col-xs-3">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-wifi.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-wifi.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-3">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-kids.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-kids.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-3">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-festas.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-festas.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-pergolado.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-churras.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-3">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-lavanderia.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-lavanderia.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-redario.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-pizza.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-gourmet.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-gourmet.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-baby.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-baby.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/foto-fitness.jpg" onclick="ga('send', 'event', 'cards', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-fitness.png" class="img100">
				</a>
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-pet.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-bike.png" class="img100">
				
			</div>
			<div class="col-md-custom col-xs-4 hide-mobile hide-tablet">
				
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/card-chima.png" class="img100">
				
			</div>
		</div>
		<div class="spacer"></div> 
		<div class="row">
			<div class="col-md-12 col-xs-12 tcenter">
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/planta-1.jpg" onclick="ga('send', 'event', 'plantas', 'click');">
					<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/btn-plantas.png" class="imgmax100">
				</a>
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/planta-2.jpg"></a>
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/planta-3.jpg"></a>
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/planta-4.jpg"></a>
				<a class="cards" href="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/planta-5.jpg"></a>
			</div>
		</div>
	</div>
</section>
<div class="spacer hide show-mobile"></div>
<div class="spacer"></div>

<section class="mapa noise">
	<div class="container">
		<div class="row">
				<div class="col-md-12">
					<h1 class="rlight whitecolor tcenter fs45">LOCALIZA&Ccedil;&Atilde;O</h1>
				</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/mapa-comp.png" class="img100">
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/mapa.png?ver=2" class="img-mapa img100">
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="spacer"></div>
</section>

<footer class="">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-myurban.png?ver=1" class="imgmax100" style="margin-bottom:-30px;"></div>
			<div class="col-md-3 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-footer.png?ver=1" class="imgmax100" style="margin-bottom: -11px;"></div>
			<div class="col-md-3 col-sm-12 col-xs-12"></div>
			<div class="col-md-4 col-sm-12  col-xs-12 whitecolor fs12">
				<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>
				*Preço base: Válido em Junho/2016.<br class="hide-mobile"> Valor ref. à unidade 901, torre E, box 155, 2 dormitórios.<br class="hide-mobile"> Imagens meramente ilustrativas.
				<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>
			</div>
		</div>
	</div>
	<section class="rights">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12"></div>
				<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/rights1.png" class="left no-float-mobile"></div>
				<div class="col-md-1 col-sm-6 col-xs-12 tcenter"><a href="http://agenciaduplo.com.br" target="_blank"><img src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/logo-duplo1.png" class="right no-float-mobile"></a></div>
			</div>
	</section>
</footer>

<?php $this->load->view("includes/footer-emp"); ?>