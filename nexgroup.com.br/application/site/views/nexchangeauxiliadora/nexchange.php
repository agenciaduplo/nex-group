<?php setlocale(LC_ALL, NULL);setlocale(LC_ALL, 'pt_BR');?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->  
    <!--[if IE 8]>   <html class="ie8" lang="pt-BR"> <![endif]-->  
    <!--[if IE 9]>   <html class="ie9" lang="pt-BR"> <![endif]-->  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>A melhor troca para seu imóvel - NexChange | Nex Group</title>

    <meta name="description" content="Seu imóvel atual, com Nex Change vale muito! A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família." />  
    <meta name="keywords" content="nexchange, nex change, nex group, troca, avaliação, imoveis, compra imovel, como adquirir imovel, troca imovel, trocar imovel por um novo, avaliação de imovel, avaliar imovel, valor do meu imovel" />    
    <meta name="author" content="http://www.agenciaduplo.com.br" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />

    <meta property='og:locale' content='pt_BR' />
    <meta property='og:title' content='A melhor troca para seu imóvel - NexChange | Nex Group' />
    <meta property='og:image' content='http://www.nexgroup.com.br/assets/site/img/layout/tit_nex.png'/>
    <meta property='og:description' content='Seu imóvel atual, com Nex Change vale muito! A Nex avalia e facilita para você comprar o empreendimento perfeito para a sua família.'/>
    <meta property='og:url' content='http://www.nexgroup.com.br/nexchange'/>
    


    <meta name="google-site-verification" content="CSBi0664OcaeFd7I1lfYeG2b1IDe5cvHh6bcue0Gg-4" />
    
    <link rel="shortcut icon" href="<?=base_url()?>favicon.png" />
    
    <link rel="canonical" href="http://www.nexgroup.com.br/nexchange" />

    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange-auxiliadora/css/main.css?ver=0.2" type="text/css" media="screen" />

    <link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/nexchange3.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=base_url()?>assets/nexchange/css/font-awesome/css/font-awesome-ie7.css">
    <![endif]-->

    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/js/jquery-1.6.4.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
      
    <script type="text/javascript">
    // var _gaq = _gaq || [];
    // _gaq.push(['_setAccount', 'UA-42888838-1']);
    // _gaq.push(['_trackPageview']);
    // (function()
    //   { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); }
    // )();
    </script>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69113303-2', 'auto');
  ga('send', 'pageview', 'auxiliadora');

</script>
  
    <script>
    $(document).ready(function() {
        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
              input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
              input.addClass('placeholder');
              input.val(input.attr('placeholder'));
            }
        }).blur().parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $("input[name=telefone_area]").inputmask("99[9]",{
            placeholder: "DDD",
            showMaskOnFocus: true, 
            clearMaskOnLostFocus: false,
            skipOptionalPartCharacter: " ",
            onincomplete: function(){
                $(this).val('');
            },
            isComplete: function() {
                if($(this).val() == '0DD' || $(this).val() == '00D' || $(this).val() == '000' || $(this).val() == 'DDD') {
                    $(this).val('DDD');
                    return false;
                }
                var a = $(this).val().replace('D', '');
                if(a.length == 2) {
                    var r = $(this).val().replace('D', '');
                    $(this).val('0' + r);
                }
            }
        });

        if($.browser.msie && parseFloat($.browser.version) == 7){
          $("input[name=telefone]").inputmask("99999999",{ 
                placeholder: "TELEFONE", 
                showMaskOnFocus: false, 
                clearMaskOnLostFocus: false,
                onincomplete: function(){
                    $(this).val('');
                },
                isComplete: function(){
                    var num = $(this).val().match(/\d/g).join('');
                    if(num.length == 8){
                        var er = /^[0-9]+$/;
                        if( !er.test($(this).val())){
                            $(this).val('');
                        }
                    }
                }
            });
        }else{
            $("input[name=telefone]").inputmask("99999999",{ 
                placeholder: "TELEFONE", 
                showMaskOnFocus: false, 
                clearMaskOnLostFocus: false,
                onincomplete: function(){
                    $(this).val('');
                }
            });
        }
    });
    </script>

    <style type="text/css">
    input.error {
        background: #f88;
    }
    input.valid {
        background: #8f8;
        display: block !important;
    }
    .modal .box-modal {position: fixed;}
    </style>  
</head>
<body> 

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NQ4BPL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NQ4BPL');</script>
    <!-- End Google Tag Manager -->
	
    <div class="modal" <?php if($liberar_mensagem == 1): echo 'style="display:block"'; else: echo 'style="display:none"'; endif; ?>>
        <div class="black"></div>
        <div class="box-modal">
            <p>SUA mensagem <br>foi enviada com sucesso!</p>
            <a href="<?=base_url()?>nexchange" class="btn">fechar</a>
        </div>
    </div>

	<header class="main-header">
		<div class="container">
			<div class="left">
				<a href="http://www.nexgroup.com.br/" class="logo">
					<h1 class="title">Nex Group</h1>
				</a>
			</div>
			<div class="right">
				<span class="pric">
					<a href="tel:+555130730101" class="click_fone" title="Ligamos para você">
						<i class="icon icon-phone"></i>
						<span class="t400" style="margin-top: -5px">(51) 3073.0101</span>
					</a>
					<!--<a class="hide-mobile hide-tablet" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=19','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">
						<i class="icon icon-comments"></i>
						<span class="t400 hide-mobile" style="margin-top: -5px">Corretor Online</span>
					</a>-->
					<a href="mailto:country@auxiliadorapredial.com.br" title="Contato Auxiliadora">
						<i class="icon icon-envelope"></i>
						<span class="t400 hide-mobile hide-mobile" style="margin-top: -5px">country@auxiliadorapredial.com.br</span>
					</a>
				</span>
			</div>
			
		</div>
	</header>

	<section class="banner"> 
		<div class="container" style="padding-bottom: 8%;padding-top: 2%;">
			<img style="width:200px;margin-top:15px;" src="<?=base_url()?>assets/site/img/logo-auxiliadora-white.png">
		</div>
		<div class="container">
			<img class="banner-info" src="<?=base_url()?>assets/site/img/banner-info.png">
		</div>
	</section>
	
	<section class="avalie-aqui hide-mobile hide-tablet scroller">
		<div class="container">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<a href="#form"><input type="button" id="btn-avalie-aqui" class="btn t700" value="Avalie aqui" /></a>
			</div>
			<div class="col-md-4"></div>
		</div>
	</section>
	
	<section class="form" id="form">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div>
					</div>
					<div class="form-canvas" style="">
						<img class="icon-form" src="<?=base_url()?>assets/site/img/icon-form.png">
						<div class="success-content" style="display:none">
							<h3 class="fa fa-check">&nbsp;&nbsp;&nbsp;<span class="t600">Mensagem mensagem foi enviada com successo!</span></h2>
							
							<div class="row hide-mobile hide-tablet">
								<div class="col-md-3"></div>
								<div class="col-md-6 tcenter">
									<span class="t400">
										Para tirar outras dúvidas, solicite o <br> atendimento pelo telefone. <!-- ou 
										<a class="a-black" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=19','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">acesse o chat</a>.-->
									</span>
								</div>
								<div class="col-md-3"></div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 tcenter">
									<a href="tel:+555130730101" class="click_fone" title="Ligamos para você">
										<img class="img-success" src="<?=base_url()?>assets/site/img/img-success-phone-aux.png">
									</a>
								</div>
								<!--<div class="col-md-3 tcenter hide-mobile hide-tablet">
									<a class="a-black" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=19','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">
										<img class="img-success" src="<?=base_url()?>assets/site/img/img-success-chat.png">
									</a>
								</div>-->
								<div class="col-md-3"></div>
							</div>
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4 tcenter">
									<input type="button" id="btn-voltar" class="btn" value="Voltar" />
								</div>
								<div class="col-md-4"></div>
							</div>
						</div>
						<div class="form-content">
							<div class="row">
								<div class="col-md-3">
									<img class="logo-auxiliadora" src="<?=base_url()?>assets/site/img/logo-auxiliadora.png">
								</div>
								<div class="col-md-7">
									<h3 class="t600">Para avaliar seu im&oacute;vel, &eacute; s&oacute; preencher os dados abaixo.<br> Em seguida, nosso corretor vai entrar em contato.</h2>
								</div>
								<div class="col-md-2"></div>
							</div>
							<form id="formNexChange">
								<div class="row"> 
									<div class="col-md-6"><input name="nome" id="nome" type="text" class="field required" placeholder="Nome (obrigatório)" /></div>
									<div class="col-md-6"><input name="email" id="email" type="text" class="field required" placeholder="E-mail (obrigatório)" /></div>
								</div>
								<div class="row"> 
									<div class="col-md-1"><input name="telefone_area" type="text" class="field Dig3" placeholder="DDD"/></div>
									<div class="col-md-5"><input name="telefone" type="text" class="field"  /></div>
									<div class="col-md-6"> <input name="cidade" type="text" class="field" placeholder="Cidade" /></div>
								</div>
								<div class="row">
									<div class="col-md-7">
										<select name="como_chegou">
											<option value="0">Como você chegou aqui? <i>(obrigat&oacute;rio)</i></option>
											<option value="1">Tv Com</option>
											<option value="2">Rádio</option>
											<option value="3">Indicação</option>
											<option value="4">Folheto</option>
											<option value="5">Internet</option>
											<option value="6">Site da Nex Group</option>
											<option value="7">Anúncio em jornal ou revista</option>
										</select>
									</div>
									<div class="col-md-5"><input type="button" id="btn-enviar" class="btn" value="Enviar" /></div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	
	<section class="entenda-melhor  hide-mobile hide-tablet scroller">
		<div class="container">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<a href="#video"><input type="button" id="btn-entenda-melhor" class="btn t700" value="Entenda melhor" /></a>
			</div>
			<div class="col-md-4"></div>
		</div>
	</section>
	
	
	<section class="video" id="video">
		<div class="container">
			
			<h1 class="t700">Conhe&ccedil;a mais sobre a NEX Change assistindo ao v&iacute;deo.</h1>
			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZGOXTdcpAHk?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	
    <footer class="footer">
        <div class="container_12">
            <div class="full">
                <ul>
                    <li class="one"></li>
                    <li class="two"></li>
                    <li class="tre"></li>
                    <li class="four"></li>
                    <li class="five"></li>
                    <li class="six"></li>
                    <li class="seven"></li>
                    <!--<li class="eight"></li>-->
                    <li class="nine"></li>
                    <li class="ten"></li>
                    <li class="eleven"></li>
                    <li class="twelve"></li>
                    <li class="thirteen"></li>
                </ul>
            </div>
            <div class="foo">
                <div class="conter">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=388662154605333&version=v2.0";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <br />  
                    <div class="fb-like" data-href="https://www.facebook.com/NexGroup/" data-width="100%" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
                </div>  
                <br style="clear: both" />
                <a href="http://www.nexgroup.com.br/">
                    <img title="Com NexChange seu imóvel vale muito" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/logo-nex.png" alt="Logo nex">
                </a>
				<img class="logo-auxiliadora-footer" src="<?=base_url()?>assets/site/img/logo-auxiliadora.png">
                <p>
                    <!--<span>Avaliação e condições facilitadas | Agilidade no fechamento</span><br>-->
                    Dação condicionada à avaliação e a análise da documentação do imóvel, livre e desembaraçado de quaisquer, ônus pela vendedora. Consulte empreendimentos participantes.
                </p>
            </div>
        </div>
    </footer>



     <!-- ************ YOUTUBE VIDEO ************ -->
            
        <div class="modal_yt" style="display:none">
            <a onClick="closePromo();" class="btn-fechar"><p>X</p></a>
            <!-- <iframe id="my-video" src="https://www.youtube.com/embed/ZGOXTdcpAHk?rel=0&amp;showinfo=0;autoplay=1;enablejsapi=1" frameborder="0" allowfullscreen></iframe> -->
            <!-- <iframe id="my-video" type="text/html" src="https://www.youtube.com/embed/ZGOXTdcpAHk?autoplay=1&enablejsapi=1&playerapiid=my-video" frameborder="0" allowfullscreen></iframe> -->
            <div id="ytapiplayer">You need Flash player 8+ and JavaScript enabled to view this video.</div>
        </div>
        <div class="black-modal" style="display:none"></div>
    
        <style>
        .modal_yt { position: fixed; top: 12%; left: 50%; z-index: 1000; background-color: #000; width: 900px ; height:510px ; margin: 0 0 0 -450px }
        .modal_yt .btn-fechar {position: absolute; z-index: 1006; right: -30px; top: -35px; width: 50px; height: 50px; background: black; border: 4px solid white; border-radius: 50%; color: white; text-align: center; cursor: pointer }
        .modal_yt .btn-fechar:hover { background: white; color: black }
        .modal_yt .btn-fechar:hover > p { color: black }
        .modal_yt .btn-fechar p {float: left; width: 100%; text-align: center; padding-top:18px; font-size: 1.6em; font-weight: 700; color: white }
        .black-modal { position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: black; opacity: 0.8; filter: alpha(opacity =80); z-index: 999  }

        @media (max-width:950px) {
            .modal_yt { top: 0 !important; left: 0 !important; width: 90% !important; height: 300px !important; margin: 5% !important  }
            .modal_yt .btn-fechar {margin: 0 !important; top: -8% !important; right: -5% !important }

        }
        </style>

   <!--  ************ YOUTUBE VIDEO ************ -->

    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.localscroll-1.2.7-min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/background/jquery.scrollTo-1.4.2-min.js"></script>
    <script type='text/javascript' src='<?=base_url()?>assets/nexchange/img/nexchange/novo/background/animate.js'></script>
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/parallax-plugin.js"></script>
    <!--<script type="text/javascript" src="<?=base_url()?>assets/nexchange/img/nexchange/novo/init.js"></script> -->
    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/js/jquery.inputmask.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/nexchange/js/swfobject/swfobject.js"></script>

    <script type="text/javascript">

	$('.scroller a').click(function(e){
		e.preventDefault();
		ga('send', 'event', $(this).find('input').val(), 'click');
		var scrollto = $(this).attr('href');
		$('html, body').animate({scrollTop:$(scrollto).offset().top}, 1000);
	});

	//GA Event 
	var control = true;	
	$(window).scroll(function(){
		
		//element = $('#video'); 
		
		var $elem = $('#video');
		var $window = $(window);

		var docViewTop = $window.scrollTop();
		var docViewBottom = docViewTop + $window.height();

		var elemTop = $elem.offset().top;
		var elemBottom = elemTop + $elem.height();
		//console.log((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		if(elemBottom <= docViewBottom && control){
			console.log('Aqui vai um tag GA');
			ga('send','event','nexchange/auxiliadora','footer');
			control = false;
		}
	});
	
    // YOUTUBE PLAYER
    var params = { allowScriptAccess: "always" };
    var atts = { id: "myytplayer" };
    swfobject.embedSWF("http://www.youtube.com/v/ZGOXTdcpAHk?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1",
                       "ytapiplayer", "100%", "100%", "8", null, null, params, atts);

    function onYouTubePlayerReady(playerId) {
        ytplayer = document.getElementById("myytplayer");
        ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
    }

    function onytplayerStateChange()
    {
        if(ytplayer.getPlayerState() === 0)
        {
            $('.modal_yt').fadeOut('slow');
            $('.btn-fechar').fadeOut('slow');
            $('.black-modal').fadeOut('slow');
        }
    }

    $(function(){
        $('.my-video').delay(1500).fadeIn(500);
    });

    function closePromo() {
        $('.modal_yt').fadeOut('slow');
        $('.btn-fechar').fadeOut('slow');
        $('.black-modal').fadeOut('slow');

        ytplayer.pauseVideo();
    }


    // YOUTUBE PLAYER

	$("#btn-voltar").click(function(){
		console.log('Clicou voltar');
		$('.form-content').show();
		$('.success-content').hide();
    });
	
    $("#btn-enviar").click(function(){
		console.log('Clicou enviar');
        ValidaForm();
    });

    function ValidaForm(){
        var response = "";
        var retorno  = true;

        var nome        = $("input[name=nome]");
        var email       = $("input[name=email]");
        var ddd         = $("input[name=telefone_area]");
        var tel         = $("input[name=telefone]");
        var cidade      = $("input[name=cidade]");
        var como_chegou = $("select[name=como_chegou]");

        var emailValido    = /^.+@.+\..{2,}$/;
        var nomeValido     = /^[a-z\u00C0-\u00ff A-Z]+$/i;
        var telefoneValido = /^[0-9]+$/;
		console.log('Validando...');
        //if(!nomeValido.test(nome.val())){
        if(nome.val() == ''){
            nome.removeClass('valid');
            nome.addClass('error');
            retorno = false;
        }else{
            nome.removeClass('error');
            nome.addClass('valid');
        }

        if(!emailValido.test(email.val())){
            email.removeClass('valid');
            email.addClass('error');
            retorno = false;
        }else{
            email.removeClass('error');
            email.addClass('valid');
        }

        if((ddd.val() != "" && ddd.val() != "DDD") && ddd.val().length == 3){

            if(!telefoneValido.test(tel.val())){
                tel.val('TELEFONE');
                tel.removeClass('valid');
                tel.addClass('error');
                retorno = false;
            }

            ddd.removeClass('error');
            ddd.addClass('valid');
        }

        if((tel.val() != "" && tel.val() != "TELEFONE") && tel.val().length == 8){

            if(!telefoneValido.test(tel.val())){
                tel.val('');
                tel.removeClass('valid');
                tel.addClass('error');
                retorno = false;
            }

            if(ddd.val() == "" || ddd.val() == "000" || ddd.val() == "DDD"){
                ddd.removeClass('valid');
                ddd.addClass('error');
                retorno = false;
            }

            tel.removeClass('error');
            tel.addClass('valid');
        }

        if((tel.val() == "" || tel.val() == "TELEFONE") && (ddd.val() == "" || ddd.val() == "DDD")){
            ddd.removeClass('valid');
            ddd.removeClass('error');
            tel.removeClass('valid');
            tel.removeClass('error');
        }


        if(como_chegou.val() > 0 && como_chegou.val() < 8)
        {
            como_chegou.removeClass('error');
            como_chegou.addClass('valid');
        }
        else
        {
            como_chegou.removeClass('valid');
            como_chegou.addClass('error');
            retorno = false;
        }

          
        if(retorno) 
        {
            base_url = "<?=base_url()?>" + "nexchange/enviaNexChange";

            $.ajax({
                type: "POST",
                url: base_url,
                data: {
                    nome:          nome.val(),
                    email:         email.val(),
                    telefone_area: ddd.val(),
                    telefone:      tel.val(),
                    cidade:        cidade.val(),
                    como_chegou:   como_chegou.val()
                },
                dataType: "json",
                success: function(response){ 
                    if(response.erro == 0){
                        //$('.modal').show();
						
                        $('#formNexChange').get(0).reset();
                        $('input', '#formNexChange').removeClass('error');
                        $('select', '#formNexChange').removeClass('error');
                        $('input', '#formNexChange').removeClass('valid');
                        $('select', '#formNexChange').removeClass('valid');
						
						$('.form-content').hide();
						$('.success-content').show();
						
						ga('send','event','nexchange/auxiliadora','contato');
                    }

                    if(response.erro == 1)
                        alert('Falha ao enviar contato! Tente novamente mais tarde.');
                }
            });

            return true;
        } 
        else
        {
            return false;
        } 
    }
    </script>
</body>
</html>