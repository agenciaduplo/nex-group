<?php $this->load->view("includes/header-emp"); ?>
<header class="main-header" style="position:fixed!important">
	<div class="container relative">
		
		<div class="left hide-mobile">
			<a href="http://www.nexgroup.com.br/" class="logo">
				<h1 class="title">Nex Group</h1>
			</a>
		</div>
		<img src="<?=base_url()?>assets/site/img/singolo/logo-singolo.png?ver=0.3" class="logo-singolo">  
		<div class="right hlinks">
			<span class="pric">
				<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/header', 'click');">
					<i class="icon icon-phone"></i>
					<span class="t400" style="margin-top: -5px">(51) 3092.3124</span>
				</a>
				<a class="show-mobile hide-tablet hide right" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor/header', 'click');">
					<i class="icon icon-comments"></i>
					<span class="t400" style="margin-top: -5px">CHAT</span>
				</a>
				<!--<a href="http://www.nexgroup.com.br/contato" title="Contato Nex">
					<i class="icon icon-envelope"></i>
					<span class="t400" style="margin-top: -5px">Nex Group</span>
				</a>-->
			</span>
		</div>
		<div class="chatbox hide-mobile">
			<iframe width="340" height="440" border="0" src="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&amp;midia=<?=$ref; ?>"></iframe>
			<div class="toggle-chat themebg themecolor"><img width="30" class="left" src="<?=base_url()?>assets/<?php echo $pagedir; ?>/img/icon-online.png?ver=0.2"><span>- FECHAR</span></div> 
		</div>

		<form id="fixedFormContact" class="hide-mobile hide-tablet hide">
			<div class="row inputs-title">
				<div class="col-md-12">
					<h1 class="lato700 whitecolor tcenter bottomnegative upperize">Se interessou?</h1>
					<h2 class="latoi300 whitecolor tcenter topnegative">Solicite um contato de nossa equipe:</h2>
				</div>
			</div>
			<div class="spacer"></div>
			<div class="row inputs">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-12">
							<input name="nome" id="nome" type="text" class="field required themecolor" placeholder="Nome (obrigatório)">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input name="email" id="email" type="text" class="field required themecolor" placeholder="E-mail (obrigatório)">
						</div>
						<div class="col-md-12">
							<input name="telefone" type="text" class="field themecolor telefone" placeholder="Telefone">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">		
							<textarea name="msg" class="field themecolor" placeholder="Mensagem" rows="4"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="error-messages"></div>
						</div>
						<div class="col-md-6">		
							<input type="hidden" id="is_mobile" name="is_mobile" value="yes" class="hide show-mobile">
							<input type="hidden" name="ref" value="<?php echo $ref; ?>">
							<input type="submit" id="btn-enviar" class="btn redbg whitecolor lato400" value="ENVIAR">
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="spacer"></div>
			</div>
			
		</form>
		
	</div>
</header>

<section class="banner"> 
	<div class="container">
		<img src="<?=base_url()?>assets/img/mude-agora.png?ver=0.3" class="label-mude-agora">
		<img src="<?=base_url()?>assets/site/img/singolo/destaque.png" class="label-destaque">
		<!--<img src="http://www.nexgroup.com.br/assets/site/img/banner-info.png">-->
	</div>
</section>

<div class="spacer"></div>

<section class="chamada">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h1 class="latoi300 themecolor tcenter fs45">2 e 3 dormit&oacute;rios com su&iacute;te e churrasqueira</h1>
				<h2 class="latoi700 themecolor tcenter fs50">na melhor localiza&ccedil;&atilde;o do bairro Tristeza.</h2>
				<h2 class="lato700 redcolorshadow tcenter fs40">DE 69m&sup2; A 108m&sup2; PRIVATIVOS.</h1>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="imoveis">
	<div class="container">
		
		<div class="row row-no-padding w98 right no-float-mobile">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_01.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_01', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_01.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_02.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_02', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_02.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_03.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_03', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_03.jpg" class="img-display">
				</a>
			</div>
		</div>
		<div class="row row-no-padding w98 left topnegative no-float-mobile hide-mobile hide-tablet">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_04.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_04', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_04.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_05.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_05', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_05.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_06.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_06', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_06.jpg" class="img-display">
				</a>
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer hide show-mobile"></div>

<section class="chamada padder">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="lato300 themecolor tcenter fs25"> • TRANQUILIDADE • CONVENIÊNCIA • GASTRONOMIA <br class="hide show-mobile">• DIVERSÃO • LIBERDADE</h2>
				<h2 class="lato700 redcolorshadow tcenter fs50">VIVER TUDO ISSO É MUITO ZONA SUL.</h1>
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>

<section class="imoveis">
	<div class="container">
		
		<div class="row row-no-padding w98 left no-float-mobile">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_07.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_07', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_07.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_08.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_08', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_08.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_09.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_09', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_09.jpg" class="img-display">
				</a>
			</div>
		</div>
		<div class="row row-no-padding w98 right topnegative no-float-mobile hide-mobile hide-tablet">
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_10.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_10', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_10.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_11.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_11', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_11.jpg" class="img-display">
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?=base_url()?>assets/site/img/singolo/imagens_singolo_12.jpg" rel="group1" class="fancybox" onclick="ga('send', 'event', 'imagens_singolo_12', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/imagens_singolo_12.jpg" class="img-display">
				</a>
			</div>
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer"></div>

<section class="info-chamada themelightbggradient hide">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="lato700 whitecolor tcenter bottomnegative upperize">Para mais informa&ccedil;&otilde;es</h1>
				<h2 class="latoi300 whitecolor tcenter topnegative" style="margin-bottom:20px;">fale com nosso <b>corretor online</b> ou <b>ligue</b>:</h2>
			</div>
		</div>
	</div>
</section>

<section class="info-links hide">
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-4 col-xs-6">
				<a class="" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);"  onclick="ga('send', 'event', 'corretor/meio', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/icon-big-corretor.png?ver=2" class="ico-big-corretor img100">
				</a>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4 col-xs-6">
				<a href="tel:+555130923124" class="click_fone" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/meio', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/icon-big-telefone.png?ver=2" class="ico-big-telefone img100">
					<span class="hide-tablet hide-mobile" style="color:transparent;position:absolute;bottom:0;right:0;font-size:50px;">30923124</span>
				</a>
			</div>
			<div class="col-md-1"></div>
			
		</div>
	</div>
</section>

<div class="spacer"></div>
<div class="spacer"></div>

<section class="mapa themebggradient">
	<div class="container">
		<div class="row">
				<div class="col-md-12">
					<h1 class="lato700 whitecolor tcenter">Mapa de localiza&ccedil;&atilde;o</h1>
				</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 latoi300 whitecolor tcenter fs25"> • Localização privilegiada • 4min. do Zaffari da Otto <br class="hide-mobile hide-tablet">• 3min. do Paseo Zona Sul • Próximo ao pórtico e à praça da Tristeza.</div>
			<div class="col-md-2"></div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<img src="<?=base_url()?>assets/site/img/singolo/mapa.png?ver=1" class="img-mapa img100">
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="spacer"></div>
</section>

<div class="spacer"></div>

<section class="form <?php if(isset($_GET['dev'])) { ?> hide <?php } ?> show-mobile">
	<div class="container">
		<form id="formContact">
			<div class="row">
				<div class="col-md-12">
					<h1 class="lato700 whitecolor tcenter bottomnegative upperize">Se interessou?</h1>
					<h2 class="latoi300 whitecolor tcenter topnegative">Solicite um contato de nossa equipe:</h2>
				</div>
			</div>
			<div class="spacer"></div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-12">
							<input name="nome" id="nome" type="text" class="field required themecolor" placeholder="Nome (obrigatório)">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<input name="email" id="email" type="text" class="field required themecolor" placeholder="E-mail (obrigatório)">
						</div>
						<div class="col-md-6">
							<input name="telefone" type="text" class="field themecolor telefone" placeholder="Telefone">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">		
							<textarea name="msg" class="field themecolor" placeholder="Mensagem" rows="4"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="error-messages"></div>
						</div>
						<div class="col-md-4">		
							<input type="hidden" id="is_mobile" name="is_mobile" value="yes" class="hide show-mobile">
							<input type="hidden" name="ref" value="<?php echo $ref; ?>">
							<input type="submit" id="btn-enviar" class="btn redbg whitecolor lato400" value="ENVIAR">
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			
		</form>
	</div>
</section>

<div class="spacer"></div>

<footer class="themebg">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/site/img/singolo/logo-singolo-footer.png?ver=1" class="imgmax100"></div>
			<div class="col-md-3 col-sm-6 col-xs-6"><img src="<?=base_url()?>assets/site/img/singolo/logo-footer.png?ver=1" class="imgmax100"></div>
			<div class="col-md-3 col-sm-12 col-xs-12"></div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<a class="hide" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=14&amp;midia=<?=$ref; ?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" onclick="ga('send', 'event', 'corretor/footer', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/icon-footer-corretor.png?ver=1" class="imgmax100">
				</a>
			</div>
			<div class="col-md-2 col-sm-6  col-xs-6">
				<a href="tel:+555130923124" class="click_fone hide" title="Ligamos para você" onclick="ga('send', 'event', 'telefone/footer', 'click');">
					<img src="<?=base_url()?>assets/site/img/singolo/icon-footer-telefone.png?ver=2" class="imgmax100">
				</a>
			</div>
		</div>
	</div>
	<section class="rights">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12"></div>
				<div class="col-md-6 col-sm-6 col-xs-12 tcenter"><img src="<?=base_url()?>assets/site/img/singolo/rights1.png" class="left no-float-mobile"></div>
				<div class="col-md-1 col-sm-6 col-xs-12 tcenter"><a href="http://agenciaduplo.com.br" target="_blank"><img src="<?=base_url()?>assets/site/img/singolo/logo-duplo1.png" class="right no-float-mobile"></a></div>
			</div>
	</section>
</footer>

<?php $this->load->view("includes/footer-emp"); ?>