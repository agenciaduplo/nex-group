<?php //var_dump($imoveis); ?>
<?php if(count($imoveis) > 0): $box = 1; $boxS = ''; $j = 0; ?>
	<ul class="imoveisBusca" id="">
	<?php if($card_view == 1): ?>
		<?php foreach($imoveis as $row): ?>
		<?php
			if($j%3 == 0){
				$box = 1;
			}

			if($box > 1){
				$boxS = $box;
			}else{
				$boxS = '';
			}

			$boxS = "";
		?>
		<li>
			<div class="box<?=$boxS?> scales" style="background:url('<?=$row['imagem']?>')">
				<div class="left">
					<h2><?=$row['nome']?></h2>
					<h3><i><?=$row['chamada']?></i></h3>
					<h4><?=$row['chamada2']?></h4>
					<a href="<?=$row['url']?>" class="btn btn-white button">Conheça</a>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</div>
		</li>
		<?php $box++; $j++; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<?php foreach($imoveis as $row): ?>
		<?php
			if($j == 0){
				echo '<li>';
			}else if($j % 3 == 0){
				$box = 1;
				echo '</li>';
				echo '<li>';
			}
			if($box > 1){
				$boxS = $box;
			}else{
				$boxS = '';
			}
		?>
		<div class="box<?=$boxS?> scales" style="background:url('<?=$row['imagem']?>')">
			<div class="left">
				<h2><?=$row['nome']?></h2>
				<h3><i><?=$row['chamada']?></i></h3>
				<h4><?=$row['chamada2']?></h4>
				<a href="<?=$row['url']?>" class="btn btn-white button">Conheça</a>
			</div>
			<div class="pix"></div>
			<div class="black"></div>
		</div>
       	<?php $box++; $j++; ?>
		<?php endforeach; ?>
	<?php endif; ?>

	</li></ul>
	<div class="clearfix"></div>
	<div class="navegation">
		<a id="prev-imoveisBusca" class="prev" href="#"></a>
		<div id="pager-imoveisBusca" class="pager auto-margin-bolinhas"></div>
		<a id="next-imoveisBusca" class="next" href="#"></a>
	</div>

<?php else: ?>
	<div class="row none_search" style="display: block">
		<p>Nenhum imóvel foi encontrado. Tente novamente.</p>
	</div>
<?php endif; ?>