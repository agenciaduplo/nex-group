<?php //var_dump($imoveis); ?>
<?php if(count($imoveis) > 0) : ?>
<?php foreach($imoveis as $row) : ?>
	<div class="emp">
		<a href="<?=$row['url']?>"><figure class="scales" style="background: url('<?=$row['imagem']?>')"></figure></a>
		<div class="emp-content">
			<ul class="up">
				<li class="one">
					<h2><?=$row['nome']?></h2>
					<h3><i><?=$row['chamada']?></i></h3>
				</li>
				<li class="two">
					<a href="<?=$row['url']?>" class="btn small expand btn-red button">Conheça</a>
				</li>
			</ul>
			<ul class="down">
				<?php 
				if($row['id_status'] == 1) { $status = 'Lançamento'; }
				elseif($row['id_status'] == 2) { $status = 'Pronto para morar'; }
				elseif($row['id_status'] == 3) { $status = 'Em obras'; }
				elseif($row['id_status'] == 4) { $status = 'Em obras'; }
				elseif($row['id_status'] == 5) { $status = 'Em obras'; }
				elseif($row['id_status'] == 6) { $status = '100% vendido'; }
				else { $status = ''; }

				$class_style = 'two';
				// $class_style = ($row['metragem'] == 'Apto.') ? 'two' : 'two-casa';
				?>
				
				<?php if($status != '') { ?><li><i class="icon icon-tag"></i><p><?=$status?></p></li><?php } ?>
				<?php if($row['cidade']) { ?><li><i class="icon icon-map-marker"></i><p><?=$row['cidade']?></p></li><?php } ?>
				<?php if($row['dormitorios']) { ?><li><i class="icones-img one"></i><p><?=$row['dormitorios']?></p></li><?php } ?>		
				<?php if($row['metragem']) { ?><li><i class="icones-img <?=$class_style?>"></i><p><?=$row['metragem']?></p></li><?php } ?>
				<?php if($row['vagas']) { ?><li><i class="icones-img tree"></i><p><?=$row['vagas']?></p></li><?php } ?>
			</ul>
		</div>
	</div>
<?php endforeach; ?>

<div class="pagination-centered"><?=$pagination?></div>

<?php else: ?>
	<div class="row none_search" style="display: block">
		<p>Nenhum imóvel foi encontrado. Tente novamente.</p>
	</div>
<?php endif; ?>