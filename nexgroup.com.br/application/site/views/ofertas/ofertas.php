<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("includes/header-html-scroll");  ?>

<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>

<!-- DESTAQUE -->
<section id="" class="destaque-ofertas desk-interno-ofertas">
	<div class="list_carousel ">
		<ul id="">
			<li class="box scales paral" style="background:url(<?=base_url()?>/assets/site/img/nexchange.jpg); ">
				<div class="box2">
					<div class="left">
						<h2>Classificados nex vendas</h2>
						<h3><i>Oportunidades imperdíveis!</i></h3>
					</div>
				</div>
				<div class="pix"></div>
				<div class="black"></div>
			</li>
		</ul>
	</div>
</section>


<!-- EMPREENDIMENTOS INTERNA -->
<section id="empreendimentos-interna" class="page-ofertas emp-in-ofer">
	<div id="ancora-topo" style="float: left; width: 98%; margin-top: -80px;"></div>

	<div id="ancora3"></div>
	<div id="ancora4"></div>

	<!-- SUB MENU -->
	<section class="submenu-home relativo" >
		<div class="row">
			<div class="left clickroll">
				<ul>
					<li class="active nomobile"><a href="#ancora-apresentacao" id="btn-apresentacao" onclick="selecioneMenuInterna(this);">Classificados Nex Vendas<span></span></a></li>
				</ul>
			</div>
			<div class="right">

				<div class="sub-contacts">
					<div class="ofertas-fone"><i class="icon icon-phone"></i><p>(51) 3092-3124</p></div>
					<a href="<?php echo site_url(); ?>contato" class="btn small btn-red button right">Entrar em contato</a>
				</div>
				<ul>
					<li class="li-lista active nomobile"><a href="javascript:void(0);" id="lista" onclick="selecionarAbaFiltro(this);" rel="nofollow"><i class="icon icon-list "></i></a></li>
					<!-- <li id="tab-location" class="li-mapa"><a href="javascript:void(0);" id="mapa" onclick="selecionarAbaFiltro(this);" rel="nofollow"><i class="icon icon-map-marker "></i></a></li> -->
				</ul>


			</div>
		</div>
	</section>


	<section class="filtros nomobile">
		<div class="row">
			<div style="clear:both;"></div>
			<form>
				<div class="small-4">
					<select id="filtro-tipo" onchange="filtraOfertas(this);">
						<option value="0">Todas Ofertas</option>
						<?php foreach($tipo_ofertas as $tipo) { echo '<option value="'.$tipo->id_ofertas_tipo.'">'.$tipo->tipo.'</option>'; } ?>
					</select>
				</div>

				<!-- 
				<div class="small-4">
					<select id="filtro-estado" onchange="filtrarHomeCard(); filtrarHomeLista(); filtrarHomeMapa();">
						<option value="-">Filtrar por Estado:</option>
						<?php foreach($arrEstados as $id => $estado) { echo '<option value="'.$id.'">'.$estado.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-cidade" onchange="filtrarHomeCard(); filtrarHomeLista(); filtrarHomeMapa();">
						<option value="-">Filtrar por Cidade:</option>	
						<?php foreach($arrCidades as $id => $cidade) { echo '<option value="'.$id.'">'.$cidade.'</option>'; } ?>
					</select>
				</div>
				<div class="small-4 nomargin-left">
					<select id="filtro-tipo" onchange="filtrarHomeCard(); filtrarHomeLista(); filtrarHomeMapa();">
						<option value="-">Filtrar por Tipo:</option>
						<?php foreach($arrTipos as $id => $tipo) { echo '<option value="'.$id.'">'.$tipo.'</option>'; } ?>
					</select>
				</div> -->
			</form> 
		</div>
	</section>



	<!-- MAPA -->
	<section id="tab-mapa" class="cards" style="display:none;">
		<div class="mapa" id="map_canvas"></div>
	</section>

	<!-- MENSAGEM SEM RESULTADOS -->
	<section id="none_search" class="none_search">
		<div class="row">
			<p>Nenhuma oferta foi encontrada. Tente novamente.</p>
		</div>
	</section>

	<!-- LISTA -->
	<section id="tab-lista" class="lista">
		<div class="tab-todos">
			<div class="row">
				<div id="div-imoveisLista">


					<?php

					if($ofertas) :
					foreach ($ofertas as $row) :

					$img = UPLOADS_PATH . 'ofertas/zoom/' . $row->img;

					?>


					<div class="emp oferta-tipo-<?=$row->id_ofertas_tipo?>">
						<figure class="scales" style="background: url('<?=$img?>')"></figure>
						<div class="emp-content">
							<ul class="up">
								<li class="one" style=''>
									<h2><?=$row->titulo?></h2>
									<?php if(intval($row->valor) > 0) : ?>
									<h3 style='margin-top: 10px;'>
										<?php if($row->valor_tipo == 'de_por') : ?>

										De
										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor_de, 2, ',', '.')?></span>

										Por
										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor, 2, ',', '.')?></span>

										<?php else : ?>

										<span style="color: #B72025;font-weight: 600;">R$</span>
										<span style=''><?=number_format($row->valor, 2, ',', '.')?></span>

										<?php endif; ?>

										<?php if($row->valor_frase != "") { ?>
										<span style="color: #B72025;font-weight: 600;"><i><?=$row->valor_frase?></i></span>
										<?php } ?>
									</h3>
									<?php endif; ?>
								</li>
								<li class="two" style=''>
									<a href="<?=base_url().'oferta/'.$row->url?>" class="btn small expand btn-red button" style='display: block;margin-top: 15px;'>Conheça</a>
								</li>
							</ul>
							<ul class="down">
								<li><i class="icon icon-tag"></i><p>Oferta Nex</p></li>
								<?php if($row->cidade != "") { ?><li><i class="icon icon-map-marker"></i><p><?=$row->cidade?></p></li><?php } ?>
								
								<!-- <li><i class="icones-img one"></i><p>2 e 3 dormitórios com suíte</p></li>		 -->
								
								<li><i class="icones-img "></i><p><?=$row->tipo?> <?=($row->comodos) ? $row->comodos : ''?></p></li>
								<?php if($row->vagas != "") { ?><li><i class="icones-img tree"></i><p><?=$row->vagas?></p></li><?php } ?>			
							</ul>
						</div>
					</div>


					<?php
					endforeach;
					endif;
					?>


				</div>
			</div>
		</div>
	</section>  
	<!-- LISTA ENDS -->



</section>


<script type="text/javascript">
function filtraOfertas(object)
{
	if( $(object).val() > 0) { $('.page-ofertas .emp').hide(10, function() { $('.page-ofertas .oferta-tipo-'+ $(object).val()).show(); }); return true; }

	$('.page-ofertas .emp').show();
	return true;
}
</script>

<?php $this->load->view("includes/ofertas_visitadas"); ?>
<?php $this->load->view("includes/footer"); ?>