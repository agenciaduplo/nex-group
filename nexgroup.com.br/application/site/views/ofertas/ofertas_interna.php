<?php $this->load->view("includes/header"); ?>

<script src="https://apis.google.com/js/platform.js" async defer>
{lang: 'pt-BR'}
</script>

<?php $this->load->view("includes/header-html-scroll");  ?>
<?php $this->load->view("includes/modal_telefone");  ?>
<?php $this->load->view("includes/modal_contato");  ?>					
<?php $this->load->view("includes/modal_interesse");  ?>					
					
	<!-- DESTAQUE: -->
	<section id="" class="destaque-ofertas desk-interno-ofertas tred">
		<div class="list_carousel ">
			<ul id="">
				<li class="box scales paral" style="background:url(<?=base_url()?>/assets/site/img/nexchange.jpg);">
					<div class="box2" style="">
						<h2><?=$oferta->titulo?></h2>
						<h3 style="">
							<span style="background-color: #B72025;padding: 5px 25px;">
								<small style='font-weight: 600;'>R$</small>
								<?=number_format($oferta->valor, 2, ',', '.')?>
							</span>
						</h3>
					</div>
					<div class="pix"></div>
					<div class="black"></div>
				</li>
			</ul>
		</div>
	</section>
	<!-- DESTAQUE; -->

	<!-- OFERTA INTERNA: -->
	<section id="empreendimentos-interna" class="emp-in-ofer" style="background-color: #fff;">
		
		<div id="ancora-topo" style="float: left; width: 98%; margin-top: -80px;"></div>
          
		<!-- NAVBAR: -->
		<div id="ancora-menu-fixed"></div>
		<section class="submenu-home relativo">
			<div class="row">
				<div class="left clickroll">
					<ul>
						<li class="active name">
							<a href="#ancora-topo" id="btn-topo" onclick="selecioneMenuInterna(this);">
								<!-- <i id="btn-favorito1" class="icon icon-heart" <?=(@$favoritado) ? 'style="color:#b72025;"' : ''?> onclick="salvarOfertaFavoritos(this, <?=$oferta->id_oferta?>)" style="margin-right:10px;"></i> -->
								<h2 style="display: block;float: right;margin-top: 0.5em;"><?=$oferta->tag_title?></h2>
							</a>
						</li>

						<li class="nomobile"><a href="#ancora-galeria" id="btn-galeria" onclick="selecioneMenuInterna(this);">galeria<span></span></a></li>

						<li class="nomobile"><a href="#ancora-localizacao" id="btn-localizacao" onclick="selecioneMenuInterna(this);">localização<span></span></a></li>
					</ul>
				</div>

				<div class="right">
					<div class="ofertas-fone"><i class="icon icon-phone"></i><p>(51) 3092-3124</p></div>
					<a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=38&midia=nex-vendas-oferta-<?=$oferta->url?>','pop','width=674, height=497, top=100, left=100, scrollbars=no');void(0);" class="btn small btn-red button right">Tenho Interesse</a>
				</div>
			</div>
		</section>
		<!-- NAVBAR; -->

		<!-- BREADCAMPS: -->
		<section class="breadcamps">
			<div class="row ">
				<div class="bread columns">
					<a href="" class="btn tiny btn-black button right"><i class="icon icon-caret-left"></i> voltar</a>
					<ul class="breadcrumbs">
						<li><a href="<?=base_url()?>">Home</a></li>
						<li><i class="icon icon-angle-right"></i></li>

						<li><a href="<?=base_url()?>ofertas">Ofertas Nex Vendas</a></li>
						<li><i class="icon icon-angle-right"></i></li>

						<li class="unavailable"><a href="#">Oferta</a></li>
						<li><i class="icon icon-angle-right"></i></li>

						<li class="unavailable"><a href="#"><?=$oferta->titulo?></a></li>
					</ul>
				</div>
			</div>
		</section>
		<!-- BREADCAMPS; -->

		<!-- APRESENTAÇÃO: -->
		<div id="ancora-apresentacao" style="float: left; width: 98%; margin-top: -120px;"></div>
		<section class="apresentacao">
			<div class="row">

				<div class="rightmatt">
					<div class="icones"><i class="icon icon-map-marker"></i><p><?=$oferta->cidade?></p></div>

					<div class="icones"><div class="icones-img one"></div><p><?=$oferta->comodos?></p></div>

					<div class="icones"><div class="icones-img two"></div><p><?=$oferta->metragem?></p></div>

					<div class="icones"><div class="icones-img tree"></div><p><?=$oferta->vagas?></p></div>
				</div>
				
				<i class="text-emp" class="sectre">
					<?=$oferta->descricao?>


					<div style="position: relative; margin: 0 auto; width: 100%;  height: 50px;  margin-top: 50px;">
						<div style="float:left; margin:0 10px 0 0;">
							<div class="fb-like" data-href="<?=current_url()?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
						</div>

						<div style="float:left; margin:10px 10px 0 0;">
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</div>

						<div style="float:left; margin:10px 10px 0 0;">
							<div class="g-plusone"></div>
						</div>
					</div>

				</i>

				


			</div>
		</section>
		<!-- APRESENTAÇÃO; -->

      	<!-- GALERIA: -->
      	<?php if(count($oferta_images) > 0) : // IF-G01 ?>
          
      	<div id="ancora-galeria" style="float: left; width: 98%; margin-top: -200px;"></div>
          
  		<section id="galeria" class="desk-in-galery">
			<div class="row">
				<div class="galeria-title">
					<h2>Galeria</h2>
					<h3><i>Veja fotos e ilustrações sobre o imóvel.</i></h3> 
				</div>
			</div>

            <div class="list_carousel ">
				<ul id="foo4">
					<?php $i = 0; foreach($oferta_images as $row) : $i++; ?>
				    <li class="box scales paral image-content-<?=$i?>" style="background:url(<?=base_url()?>uploads/ofertas/galeria/<?=$row->img?>)"><!-- CARD -->
				      <div class="box2">
				        <div class="left">
				          <h3><i><?=$row->legenda?></i></h3>
				          <a href="javascript:;" class="btn btn-white button" title="Ver Galeria" onclick="showGallery();"><i class="icon icon-fullscreen"></i><p>Ver Galeria</p></a>
				        </div>
				      </div>
				      <div class="pix"></div>
				      <div class="black"></div>
				    </li>
					<?php endforeach; ?>
				</ul>

				<?php $i = 0; foreach($oferta_images as $row) : $i++; ?>
				<ul id="gallery" style="display: none">
					<li class="image-content-<?=$i?>">
						<a href="<?=base_url()?>uploads/ofertas/galeria/<?=$row->img?>" class="fancybox-thumb btn btn-white button" rel="fancybox-thumb" title="<?=$row->legenda?>" style="display: none;">
							<i class="icon icon-fullscreen "></i><p>Ver Galeria</p>
						</a>
					</li>
				</ul>
				<?php endforeach; ?>

				<div class="clearfix"></div>
				<div class="navegation" style="display:none">
					<a id="prev4" class="prev" href="#"></a>
					<div id="pager4" class="pager"></div>
					<a id="next4" class="next" href="#"></a>
				</div>
            </div>
		</section>
      	<?php endif; // IF-G01 ?>
      	<!-- GALERIA; -->

		<!-- LOCALIZAÇÃO: -->
		<div id="ancora-localizacao" style="float: left; width: 98%; margin-top: -120px;"></div>
		<section id="localizacao">
			<div class="black"></div>
			<div id="endereco" class="row">
				<div class="localizacao-title">

					<h2>LOCALIZAÇÃO</h2>

					<?=($oferta->endereco) ? '<h3><i>'.$oferta->endereco.'</i></h3>' : ''?>

					<ul id="tipo-mapa" class="right nomobile auto-margin">
						<li id="mapa-rua" class="active"><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['ROADMAP']); selecionaTipoMapa('rua');">RUAS</a></li> 
						<li id="mapa-satelite"><a href="javascript:void();" onclick="map.setMapTypeId(google.maps.MapTypeId['SATELLITE']); selecionaTipoMapa('satelite');">SATÉLITE</a></li> 
					</ul> 
				</div>
			</div>

			<div class="mapa" id="map_canvas" lat="<?=$oferta->latitude?>" lon="<?=$oferta->longitude?>" marker="<?=base_url()?>uploads/ofertas/pin/<?=$oferta->pin?>"></div>

			<div class="tools">
				<ul class="right nomobile" id="ul-marker">
					<h5>Selecione para ver no mapa:</h5>

					<li><a href="javascript:void(0);" data-tipo="hospital" data-icon="<?=base_url()?>assets/site/img/pins-novos/1-hospital.png" onclick="ativarPin(this)">HOSPITAL</a></li>
					<li><a href="javascript:void(0);" data-tipo="pharmacy" data-icon="<?=base_url()?>assets/site/img/pins-novos/2-farmacias.png" onclick="ativarPin(this)">FARMÁCIAS</a></li>
					<li><a href="javascript:void(0);" data-tipo="bank" data-icon="<?=base_url()?>assets/site/img/pins-novos/3-bancos.png" onclick="ativarPin(this)">BANCOS</a></li>
					<li><a href="javascript:void(0);" data-tipo="gym" data-icon="<?=base_url()?>assets/site/img/pins-novos/4-academia.png" onclick="ativarPin(this)">ACADEMIA</a></li>
					<li><a href="javascript:void(0);" data-tipo="school,university" data-icon="<?=base_url()?>assets/site/img/pins-novos/5-ensino.png" onclick="ativarPin(this)">ENSINO</a></li>
					<li><a href="javascript:void(0);" data-tipo="shopping_mall" data-icon="<?=base_url()?>assets/site/img/pins-novos/6-shopping.png" onclick="ativarPin(this)">SHOPPING</a></li>
					<li><a href="javascript:void(0);" data-tipo="grocery_or_supermarket" data-icon="<?=base_url()?>assets/site/img/pins-novos/7-super-mercado.png" onclick="ativarPin(this)">SUPERMERCADO</a></li>
					<li><a href="javascript:void(0);" data-tipo="cafe" data-icon="<?=base_url()?>assets/site/img/pins-novos/8-padaria.png" onclick="ativarPin(this)">PADARIA</a></li>
				</ul>

				<div class="row">
					<div class="left">
						<span>Como Chegar:</span>
						<input type="hidden" name="latitude" id="latitude" value="<?=$oferta->latitude?>" />
						<input type="hidden" name="longitude" id="longitude" value="<?=$oferta->longitude?>" />
						<input type="text" name="localizacao-texto" id="localizacao-texto" placeholder="Digite aqui seu CEP ou endereço">
						<a href="javascript:void(0);" onclick="irParaLocalizacao($('#localizacao-texto').val());" class="btn btn-grey button"><p>OK</p></a>
						<!-- <a href="javascript:void(0);" onclick="pegarGeoLocalizacao();" class="btn btn-black button yesmobile"><i class="icon icon-map-marker "></i><p>TRAÇAR ROTA NO MAPA</p></a> -->
						<!-- <a href="javascript:void(0);" onclick="pegarGeoLocalizacao();" class="btn btn-black button nomobile"><i class="icon icon-map-marker "></i><p>usar minha localização</p></a> -->
						<!-- <a href="javascript:void(0);" onclick="pegarGeoLocalizacao();" class="btn btn-black button yesmobile"><i class="icon icon-map-marker "></i><p>TRAÇAR ROTA NO MAPA</p></a> -->
                  		<a href="geo:<?=$oferta->latitude . ',' . $oferta->longitude?>" id="link-geo-route" class="btn btn-black button yesmobile"><i class="icon icon-map-marker "></i><p>usar minha localização</p></a>
					</div>
				</div>
			</div>
		</section>
		<!-- LOCALIZAÇÃO; -->

	</section>
	<!-- OFERTA INTERNA; -->


	<script type="text/javascript">
	function showGallery() { $('ul#gallery > li.image-content-1 a.fancybox-thumb').eq(0).trigger('click'); }

	$(document).ready(function(){
 		if(navigator.geolocation)
 			console.log(navigator.geolocation.getCurrentPosition(setLinkGeo, erroLinkGeo));
 	})

 	function setLinkGeo(position) {
		var lat = $('#latitude').val();
		var lon = $('#longitude').val();
		var de = position.coords.latitude+','+position.coords.longitude;
		var para = lat+','+lon;
		
		$('#link-geo-route').attr('href', 'https://www.google.com/maps/dir/'+de+'/'+para+'/');

		// console.log('LINK GEO');
		// console.log('https://www.google.com/maps/dir/'+de+'/'+para+'/');
		// window.open('https://www.google.com/maps/dir/'+de+'/'+para+'/');
	}

	function erroLinkGeo()
	{
		console.log('fail ao realizar geo!');
	} 
	</script>

<?php $this->load->view("includes/ofertas_visitadas"); // MAIS VISTOS HOME ?>
<?php $this->load->view("includes/footer"); ?>