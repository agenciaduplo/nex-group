<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imoveis_visitados {
	
	var $CI;
	
	function Imoveis_visitados ()
	{	
		$this->CI =& get_instance();
	}
	
	function getImoveisVisitados ()
	{
		$this->CI->load->model('imoveis_model', 'modelImoveis');
		if(get_cookie('visitados')){
			$visitados = get_cookie('visitados');
			//var_dump($visitados);		
			$imoveis = explode(',', $visitados);
			foreach ($imoveis as $row):
				if(intval($row) > 0){
					$imo = $this->CI->modelImoveis->getEmpreendimento($row);
					if($imo){
						$imo_visitados[] = $imo;
					}
				}
			endforeach;
		}else{
			//echo 'sem cokie';
		}
		
		if(!@$imo_visitados)
			$imo_visitados = array();
		
		return $imo_visitados;
	}
	
	
	function getImoveisFavoritos ()
	{
		$this->CI->load->model('imoveis_model', 'modelImoveis');

		if(isset($_COOKIE['favorito']))
		{
			// $visitados = get_cookie('favorito');
			//var_dump($visitados);

			$imoveis = explode(',', $_COOKIE['favorito']);

			foreach ($imoveis as $row) :
				if(intval($row) > 0)
				{
					$imo = $this->CI->modelImoveis->getEmpreendimento($row);
					if($imo){
						$imo_visitados[] = $this->CI->modelImoveis->getEmpreendimento($row);
					}
				}
			endforeach;
		}else{
			//echo 'sem cokie';
		}
	
		if(!@$imo_visitados)
			$imo_visitados = array();
	
		return $imo_visitados;
	}
	
	
	function gravaCookie($id_empreendimento){
		$visitados = get_cookie('visitados');
		if($visitados){
			$imoveis = explode(',', $visitados);
			
			if(!in_array($id_empreendimento, $imoveis) && count($imoveis) < 8){
				if(count($imoveis) == 7)
					array_pop($imoveis);
				$visitados = $id_empreendimento.','.implode(',',$imoveis);
				
				$cookie = array( 
					'name' => 'visitados',
					'value' => $visitados, 
					'expire' => '86500', 
				); 
				$this->CI->input->set_cookie($cookie,true);
			}
		}else{	
			$visitados  = $id_empreendimento;
			
			$cookie = array( 
				'name' => 'visitados',
				'value' => $visitados, 
				'expire' => '86500', 
			); 
			$this->CI->input->set_cookie($cookie,true);
		}
		
		 	
		
	}
}

?>