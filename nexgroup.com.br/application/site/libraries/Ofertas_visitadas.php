<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas_visitadas {
	
	var $CI;
	
	function Ofertas_visitadas ()
	{	
		$this->CI =& get_instance();
	}
	
	function getOfertasVisitadas ()
	{
		$this->CI->load->model('ofertas_model', 'modelOfertas');

		$ofertas_visitadas = array();

		if(get_cookie('ofertas_visitadas'))
		{
			$visitados = get_cookie('ofertas_visitadas');
			//var_dump($visitados);		
			$ofertas = explode(',', $visitados);

			foreach ($ofertas as $row)
			{
				if(intval($row) > 0)
				{
					$oferta = $this->CI->modelOfertas->getOfertByID($row);

					if($oferta){
						$ofertas_visitadas[] = $oferta;
					}
				}
			}
		}
		// else{
		// 	//echo 'sem cokie';
		// }
		
		// if(!@$ofertas_visitadas)
		// 	$ofertas_visitadas = array();
		
		return $ofertas_visitadas;
	}
	
	
	function getOfertasFavoritadas()
	{
		$this->CI->load->model('ofertas_model', 'modelOfertas');

		$ofertas_visitadas = array();

		if(isset($_COOKIE['favorito_ofertas']))
		{
			// $visitados = get_cookie('favorito');
			//var_dump($visitados);

			$ofertas = explode(',', $_COOKIE['favorito_ofertas']);

			foreach ($ofertas as $row)
			{
				if(intval($row) > 0)
				{
					$oferta = $this->CI->modelOfertas->getOfertByID($row);

					if($oferta){
						$ofertas_visitadas[] = $oferta;
					}
				}
			};
		}
		// else{
		// 	//echo 'sem cokie';
		// }
	
		// if(!@$imo_visitados)
		// 	$imo_visitados = array();
	
		return $ofertas_visitadas;
	}
	
	
	function gravaCookie($id_oferta)
	{
		$this->CI->load->model('ofertas_model', 'modelOfertas');

		$visitados = get_cookie('ofertas_visitadas');

		if($visitados)
		{
			$ofertas = explode(',', $visitados);
			
			if(!in_array($id_oferta, $ofertas) && count($ofertas) < 8)
			{
				if(count($ofertas) == 7)
					array_pop($ofertas);

				$visitados = $id_oferta.','.implode(',',$ofertas);
				
				$cookie = array( 
					'name' 		=> 'ofertas_visitadas',
					'value' 	=> $visitados, 
					'expire' 	=> '86500', 
				); 

				$this->CI->input->set_cookie($cookie, true); // Adicona o id da oferta ao cookie

				return true;
			}

			return false; // Id da oferta já está no cookie
		}
		else
		{	
			$visitados = $id_oferta;
			
			$cookie = array( 
				'name'		=> 'ofertas_visitadas',
				'value' 	=> $visitados, 
				'expire' 	=> '86500', 
			); 

			$this->CI->input->set_cookie($cookie, true); // Adicona o id da oferta ao cookie

			return true;
		}
	}

}

?>