<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Encurtador {
	
	var $CI;
	
	function Encurtador ()
	{	
		$this->CI =& get_instance();
	}
	
/*
 * Classe para criar urls curtas
 * Essa classe utiliza o API do Migre.me
 */

 
    // Url Api Migre.me
    private $api        = "http://migre.me/api.json?url=";
    // URL a ser encurtada
    private $url;
 
    // Método construtor, recebe a url e a encurta
    public function Curt_URL($url_)
    {
        $this->url = $url_;
        return self::EncurtaURL();
    }
 
    // Metodo para encurtar a url
    private function EncurtaURL()
    {
        $url_long = urlencode($this->RetornaLink($this->url));
        $retorno_api = $this->api . $url_long;
        $resposta = file_get_contents($retorno_api);
        $resposta = json_decode($resposta, true);
        if ($resposta['erro'] != 0)
        {
            return $resposta['erro'];
        }
        else
        {
            return $resposta['migre'];
        }
    }
 
    // Retorna o link
    private function RetornaLink($texto)
    {
        $er = "/(http:\/\/(www\.|.*?\/)?|www\.)([a-zA-Z0-9]+|_|-)+(\.(([0-9a-zA-Z]|-|_|\/|\?|=|&)+))+/i";
        preg_match_all($er, $texto, $match);
        $link = $match[0][0];
        $link_completo = (stristr($link, "http://") === false) ? "http://" . $link : $link;
        return $link_completo;
    }
 
}
 
/* Como usar */

?>