<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('emails/lista/');
	}
	
	function lista_bkp ($offset = "")
	{
		$this->load->model('emails_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/emails/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numEmailsBusca($this->input->post('keyword')) : $this->model->numEmails(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  				=> $this->model->getPermissoes(),
			'cadastrados' 				=> $this->model->numEmails(),
			'empreendimentos' 			=> $this->model->getEmpreendimentos(),
    		'emails' 					=> ($this->input->post('keyword')) ? $this->model->buscaEmails($this->input->post('keyword')) : $this->model->getEmails($offset),
			'paginacao'	  				=> $this->pagination->create_links()
		);
		
		$this->load->view('emails/emails.php', $data);
	}
	
	function lista ($offset = "")
	{
		$this->load->model('emails_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/emails/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numEmailsBusca($this->input->post('keyword')) : $this->model->numEmails(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  				=> $this->model->getPermissoes(),
			'cadastrados' 				=> $this->model->numEmails(),
			'empreendimentos' 			=> $this->model->getEmpreendimentos(),
    		'emails' 					=> ($this->input->post('keyword')) ? $this->model->buscaEmails($this->input->post('keyword')) : $this->model->getEmailsViews($offset),
			'paginacao'	  				=> $this->pagination->create_links()
		);
		
		$this->load->view('emails/emails.view.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('emails_model', 'model');
		
		$data = array (
			'emails' 			=> $this->model->getEmails(),
			'empreendimentos' 	=> $this->model->getEmpreendimentos(),
			'tipos_forms'		=> $this->model->getTiposForms(),
			'permissoes'  		=> $this->model->getPermissoes()
		);

		$this->load->view('emails/emails.cadastro.php', $data);
	}
	
	function editar ($id_email = 0)
	{
		$this->load->model('emails_model', 'model', TRUE);
		
		try
		{ 
			
			$data = array (
				'email' 			=> $this->model->getEmail($id_email),
				'empreendimentos' 	=> $this->model->getEmpreendimentos(),
				'tipos_forms'		=> $this->model->getTiposForms(),
				'permissoes'  		=> $this->model->getPermissoes()
			);
			
			$this->load->view('emails/emails.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
	$this->load->model('emails_model', 'model');
		
		try
		{
			if($this->input->post('id_email'))
			{
				
				$data = array (
					'id_empreendimento' 	=> $this->input->post('empreendimento'),
					'email'					=> $this->input->post('email'),
					'tipo'					=> $this->input->post('tipo'),
					'id_tipo_form'			=> $this->input->post('id_tipo_form')
				);
				
				$id_email 		= $this->input->post('id_email');
				
				$this->model->setEmail($data, $id_email);
				
				redirect("emails/editar/$id_email");			
				
			}else{
				
				$data = array (
					'id_empreendimento' 	=> $this->input->post('empreendimento'),
					'email'					=> $this->input->post('email'),
					'tipo'					=> $this->input->post('tipo'),
					'id_tipo_form'			=> $this->input->post('id_tipo_form')
				);
				
				$id_email 		= $this->input->post('id_email');
				
				$this->model->setEmail($data);
				
				redirect("emails/lista");	
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_email = 0)
	{
		$this->load->model('emails_model', 'model');

		try
		{
						
			$this->model->delEmail($id_email);
			redirect('emails/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */