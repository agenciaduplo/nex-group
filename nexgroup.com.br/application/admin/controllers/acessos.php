<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acessos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: acessos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('acessos_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/acessos/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numAcessosBusca($this->input->post('keyword')) : $this->model->numAcessos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numAcessos(),
			'acessos'   	=> ($this->input->post('keyword')) ? $this->model->buscaAcessos($this->input->post('keyword')) : $this->model->getAcessos($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('acessos/acessos.php', $data);
	}
	
	
	function visualizar ($id_acesso = 0)
	{
		$this->load->model('acessos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'acesso' 		=> $this->model->getAcessoVisualizar($id_acesso)
			);
			
			$this->load->view('acessos/acessos.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_acessos = 0)
	{
		$this->load->model('acessos_model', 'model');

		try
		{
			$this->model->delAcesso($id_acessos);
			redirect('acessos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file acessos.php */
/* Location: ./system/application/controllers/falecom.php */