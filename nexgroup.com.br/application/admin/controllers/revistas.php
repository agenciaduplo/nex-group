<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revistas extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('revistas/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('revistas_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/revistas/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numRevistasBusca($this->input->post('keyword')) : $this->model->numRevistas(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numRevistas(),
    		'revistas'  		=> ($this->input->post('keyword')) ? $this->model->buscaRevistas($this->input->post('keyword')) : $this->model->getRevistas($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('revistas/revistas.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('revistas_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('revistas/revistas.cadastro.php', $data);
	}
	
	function editar ($id_revista = 0)
	{
		$this->load->model('revistas_model', 'model', TRUE);
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'revista' 			=> $this->model->getRevista($id_revista)
			);
			
			$this->load->view('revistas/revistas.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('revistas_model', 'model');
		
		try
		{	
			$date = implode("-",array_reverse(explode("/",$this->input->post('data_cadastro'))));	
			
			$data = array (
				'titulo' 			=> $this->input->post('titulo'),
				'descricao' 		=> $this->input->post('descricao'),
				'edicao' 			=> $this->input->post('edicao'),
				'expediente' 		=> $this->input->post('expediente'),
				'data_cadastro' 	=> $date,
				'pageflip'			=> $this->input->post('pageflip')
				
			);

			$insert_id = $this->model->setRevista($data, $this->input->post('id_revista'));
			
			if ($this->input->post('id_revista'))
				redirect('revistas/editar/' . $this->input->post('id_revista'));
			else
				redirect('revistas/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_revista = 0)
	{
		$this->load->model('revistas_model', 'model');

		try
		{
						
			$revista = $this->model->getRevista($id_revista);
			if($revista){
				foreach ($revista as $arquivos) {						
					if($arquivos->capa):
						if(file_exists('./uploads/revista/capa/' . $arquivos->capa)){
							unlink('./uploads/revista/capa/' . $arquivos->capa);
						}
					endif;
					if($arquivos->arquivo):
						if(file_exists('./uploads/revista/pdf/' . $arquivos->arquivo)){
							unlink('./uploads/revista/pdf/' . $arquivos->arquivo);
						}
					endif;
				}
			}
			$this->model->delRevista($id_revista);
			
			redirect('revistas/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	//FIM NOTICIAS CONTROLLER

		
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */