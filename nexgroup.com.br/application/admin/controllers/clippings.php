<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('clippings/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('clippings_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/clippings/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numClippingsBusca($this->input->post('keyword')) : $this->model->numClippings(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numClippings(),
    		'clippings'  		=> ($this->input->post('keyword')) ? $this->model->buscaClippings($this->input->post('keyword')) : $this->model->getClippings($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('clippings/clippings.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('clippings_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('clippings/clippings.cadastro.php', $data);
	}
	
	function editar ($id_clipping = 0)
	{
		$this->load->model('clippings_model', 'model', TRUE);
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'clipping' 			=> $this->model->getClipping($id_clipping)
			);
			
			$this->load->view('clippings/clippings.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('clippings_model', 'model');
		
		try
		{	
			$date = implode("-",array_reverse(explode("/",$this->input->post('data_cadastro'))));	
			
			$data = array (
				'titulo' 			=> $this->input->post('titulo'),
				'descricao' 		=> $this->input->post('descricao'),
				'ativo' 			=> $this->input->post('ativo'),
				'data_cadastro' 	=> $date
				
			);

			$insert_id = $this->model->setClipping($data, $this->input->post('id_clipping'));
			
			if ($this->input->post('id_clipping'))
				redirect('clippings/editar/' . $this->input->post('id_clipping'));
			else
				redirect('clippings/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_clipping = 0)
	{
		$this->load->model('clippings_model', 'model');

		try
		{
			$arquivoVerificar = $this->model->getImagem($id_clipping);

			if($arquivoVerificar->arquivo)
			{
				unlink('./uploads/clippings/arquivos/' . $arquivoVerificar->arquivo);
				unlink('./uploads/clippings/arquivos/' . $arquivoVerificar->imagem_destaque);	
			}

			if($arquivoVerificar->imagem_destaque)
			{
				unlink('./uploads/clippings/zoom/' . $imagem->imagem_destaque);
				unlink('./uploads/clippings/medium/' . $imagem->imagem_destaque);
				unlink('./uploads/clippings/thumb/' . $imagem->imagem_destaque);	
			}

			$this->model->delClipping($id_clipping);
			


			redirect('clippings/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */