<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->auth->check();
	}
	function index(){
		redirect('relatorios/contato');
	}
	function atendimentos($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'camp' => $this->model->getAtendimentosURL(),
			'emp' => $this->model->getAtendimentosEMP(),
			'data' => $this->model->getAtendimentosDATA()

		);
		if($this->input->post()){
			if($this->input->post('form_tipo') == 2){
				$form = $this->input->post('form_camp');
			} else {
				$form = $this->input->post('form_emp');
			}
			$data['query'] = $this->model->searchAtendimentosDATA(
				$this->input->post('form_tipo'),
				$form,
				$this->input->post('form_min'),
				$this->input->post('form_max')
			);
		}
		$this->load->view('relatorios/relatorio.atendimentos.php', $data);
	}
	function semanal($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'camp' => $this->model->getAtendimentosURL(),
			'emp' => $this->model->getAtendimentosEMP(),
			'data' => $this->model->getAtendimentosDATA()

		);
		if($this->input->post()){
			$data['query'] = $this->model->getRelatorioSemanal(
				$this->input->post('form_tipo'),
				$this->input->post('form_min'),
				$this->input->post('form_max')
			);
			
			$this->load->helper('download');
			$filename = url_title($this->input->post('form_tipo').'_'.$this->input->post('form_min').'_'.$this->input->post('form_max').'.csv');
			$output = fopen("uploads/temp/".$filename, 'w');
			foreach ($data['query'] as $linha) {
				foreach($linha as $_k=>$_v) $linha[$_k] = utf8_decode($linha[$_k]);
				fputcsv($output, $linha,';');
			}
			fclose($output);
			$_csv = file_get_contents("uploads/temp/".$filename); 
			force_download($filename, $_csv);
			
		}
		$this->load->view('relatorios/relatorio.semanal.php', $data);
	}
	function contatos($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'camp' => $this->model->getContatosURL(),
			'emp' => $this->model->getContatosEMP(),
			'data' => $this->model->getContatosDATA()

		);
		if($this->input->post()){
			if($this->input->post('form_tipo') == 2){
				$form = $this->input->post('form_camp');
			} else {
				$form = $this->input->post('form_emp');
			}
			$data['query'] = $this->model->searchContatosDATA(
				$this->input->post('form_tipo'),
				$form,
				$this->input->post('form_min'),
				$this->input->post('form_max')
			);
		}
		$this->load->view('relatorios/relatorio.contatos.php', $data);
	}
	function interesses($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'camp' => $this->model->getInteressesURL(),
			'emp' => $this->model->getInteressesEMP(),
			'data' => $this->model->getInteressesDATA()

		);
		if($this->input->post()){
			if($this->input->post('form_tipo') == 2){
				$form = $this->input->post('form_camp');
			} else {
				$form = $this->input->post('form_emp');
			}
			$data['query'] = $this->model->searchInteressesDATA(
				$this->input->post('form_tipo'),
				$form,
				$this->input->post('form_min'),
				$this->input->post('form_max')
			);
		}
		$this->load->view('relatorios/relatorio.interesses.php', $data);
	}
  function formularios($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'emp' => $this->model->getInteressesEMP()
		);
		if($this->input->post()){
			$data['query'] = $this->model->searchFormulariosDATA(
				$this->input->post('form_tipo'),
				$this->input->post('form_min'),
				$this->input->post('form_max'),
				$this->input->post('form_emp')
			);
		}
		$this->load->view('relatorios/relatorio.formularios.php', $data);
	}
	
	function corretor($offset = 0){
		$this->load->model('relatorios_model', 'model');
		$data = array(
			'camp' => $this->model->getCorretorURL(),
			'data' => $this->model->getCorretorDATA()

		);
		if($this->input->post()){
			$data['query'] = $this->model->searchCorretorDATA(
				$this->input->post('form_camp'),
				$this->input->post('form_min'),
				$this->input->post('form_max')
			);
		}
		$this->load->view('relatorios/relatorio.corretor.php', $data);
	}
}
?>