<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function clean($array){
	$return = array();
	$search = array('"',';','\n','\r');
	foreach($array as $i => $x){
		$return[$i] = trim(strip_tags(str_replace($search,'',$x)));
	}
	return $return;
}



class Exportacao extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->auth->check();
	}
	function index(){
		$this->load->model('exportacao_model', 'model');
		$this->load->helper('download');
		$dados = $this->model->getBicicletasPromo();
		$linhas = array();
		foreach($dados as $x){
			$a = clean($x);
			$a = '"'.implode('";"',$a).'"';
			array_push($linhas,$a);
		}
		force_download('dados-'.time().'.csv',implode(PHP_EOL,$linhas));
	}
	
}
?>