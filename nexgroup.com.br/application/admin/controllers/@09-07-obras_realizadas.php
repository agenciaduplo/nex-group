<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*httsp://www.nexgroup.com.br/*/
class Obras_realizadas extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('obras_realizadas/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('obras_realizadas_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/obras_realizadas/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numObrasRealizadasBusca($this->input->post('keyword'),$offset) : $this->model->numObrasRealizadas(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numObrasRealizadas(),
    		'obras_realizadas'  => ($this->input->post('keyword')) ? $this->model->buscaObrasRealizadas($this->input->post('keyword'),$offset) : $this->model->getObrasRealizadas($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('obras_realizadas/obras_realizadas.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('obras_realizadas_model', 'model');
		
		$data = array (
			'empresas'		=> $this->model->getEmpresas(),
			'estados'		=> $this->model->getEstados(),
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('obras_realizadas/obras_realizadas.cadastro.php', $data);
	}
	
	function editar ($id_obra_realizada = 0)
	{
		$this->load->model('obras_realizadas_model', 'model', TRUE);
		
		try
		{ 
			$obra = $this->model->getObraRealizada($id_obra_realizada);
			
			$data = array (
				'empresas'					=> $this->model->getEmpresas(),
				'permissoes'  				=> $this->model->getPermissoes(),
				'obra_realizada' 			=> $obra,
				'estados'					=> $this->model->getEstados(),
				'cidades' 					=> $this->model->getCidades($obra->id_estado)
			
			);
			
			$this->load->view('obras_realizadas/obras_realizadas.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('obras_realizadas_model', 'model');
		
		try
		{	
			$id_obra_realizada 	= $this->input->post('id_obra_realizada');

			$data = array (
				'empreendimento'	=> $this->input->post('empreendimento'),
				'id_empresa' 	 	=> $this->input->post('empresas'),
				'id_cidade' 	 	=> $this->input->post('cidade'),
				'bairro'			=> $this->input->post('bairro'),
				'ordem'				=> $this->input->post('ordem'),
				'ativo'				=> $this->input->post('ativo')
			);
					
			$insert_id = $this->model->setObraRealizada($data,$id_obra_realizada);			
			
			if ($this->input->post('id_obra_realizada'))
				redirect('obras_realizadas/editar/' . $this->input->post('id_obra_realizada'));
			else
				redirect('obras_realizadas/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_obra_realizada = 0)
	{
		$this->load->model('obras_realizadas_model', 'model');

		try
		{
						
			$imagem = $this->model->getImagemObra($id_obra_realizada);
			if($imagens){
				foreach ($imagens as $imagem) {						
					if($imagem->fachada):
						if(file_exists('./assets/uploads/obras_realizadas/' . $imagem->fachada)){
							unlink('./assets/uploads/obras_realizadas/' . $imagem->fachada);
						}
						if(file_exists('./assets/uploads/obras_realizadas/thumbs/' . $imagem->fachada)){
							unlink('./assets/uploads/obras_realizadas/thumbs/' . $imagem->fachada);
						}
					endif;
				}
				$this->model->delImagem('',$id_obra_realizada);
			}
			$this->model->delEmpreendimento($id_obra_realizada);
			
			redirect('obras_realizadas/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */