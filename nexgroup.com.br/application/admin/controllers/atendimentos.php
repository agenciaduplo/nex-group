<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Atendimentos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}	
	
	function index ()
	{
		header('Location: atendimentos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('atendimentos_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/atendimentos/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numAtendimentosBusca($this->input->post('keyword')) : $this->model->numAtendimentos($offset),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numAtendimentos(),
			'atendimentos'    => ($this->input->post('keyword')) ? $this->model->buscaAtendimentos($this->input->post('keyword')) : $this->model->getAtendimentos($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('atendimentos/atendimentos.php', $data);
	}
	
	function visualizar ($id_atendimento = 0)
	{
		$this->load->model('atendimentos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'atendimento' 	=> $this->model->getAtendimentoVisualizar($id_atendimento)
			);
			
			$this->load->view('atendimentos/atendimentos.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_atendimento = 0)
	{
		$this->load->model('atendimentos_model', 'model');

		try
		{
			$this->model->delAtendimento($id_atendimento);
			redirect('atendimentos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file atendimentos.php */
/* Location: ./system/application/controllers/atendimentos.php */