<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_destaques extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('noticias_destaques/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('noticias_destaques_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/noticias_destaques/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numDestaqueNoticiasBusca($this->input->post('keyword')) : $this->model->numDestaqueNoticias(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  				=> $this->model->getPermissoes(),
			'cadastrados' 				=> $this->model->numDestaqueNoticias(),
    		'noticias_destaques' => ($this->input->post('keyword')) ? $this->model->buscaDestaqueNoticias($this->input->post('keyword')) : $this->model->getDestaqueNoticias($offset),
			'paginacao'	  				=> $this->pagination->create_links()
		);
		
		$this->load->view('noticias/noticias.destaques.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('noticias_destaques_model', 'model');
		
		$data = array (
			'noticias' 	=> $this->model->getNoticias(),
			'permissoes'  		=> $this->model->getPermissoes()
		);

		$this->load->view('noticias/noticias.cadastro.destaques.php', $data);
	}
	
	function editar ($id_destaque = 0)
	{
		$this->load->model('noticias_destaques_model', 'model', TRUE);
		
		try
		{ 
			
			$data = array (
				'destaque'				=> $this->model->getDestaque($id_destaque),
				'noticias' 	=> $this->model->getNoticias(),
			);
			
			$this->load->view('noticias/noticias.cadastro.destaques.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
	$this->load->model('noticias_destaques_model', 'model');
		
		try
		{
			if($this->input->post('id_destaque_noticia'))
			{
				
				$data = array (
					'id_noticia' 	=> $this->input->post('noticia'),
					'legenda'		=> $this->input->post('legenda')
				);
				
				$id_destaque 		= $this->input->post('id_destaque_noticia');
				
				$this->model->setDestaque($data, $id_destaque);
				
				redirect("noticias_destaques/editar/$id_destaque");			
				
			}else{
				
				$data = array (
					'id_noticia' 		=> $this->input->post('noticia'),
					'legenda'			=> $this->input->post('legenda')
				);
				
				$id_destaque 		= $this->input->post('id_destaque');
				
				$this->model->setDestaque($data);
				
				redirect("noticias_destaques/lista");	
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_destaque_noticia = 0)
	{
		$this->load->model('noticias_destaques_model', 'model');

		try
		{
						
			$destaque = $this->model->getDestaque($id_destaque_noticia);
			if($destaque->imagem){
				if(file_exists('./uploads/noticias/destaques/' . $destaque->imagem)){
					unlink('./uploads/noticias/destaques/' . $destaque->imagem);
				}
			}
			$this->model->delDestaqueNoticia($id_destaque_noticia);
			
			
			redirect('noticias_destaques/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */