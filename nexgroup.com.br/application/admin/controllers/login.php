<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		//$this->auth->check();
	}
	
	function index ()
	{
		$this->load->view('login');
	}
	
	function entrar ()
	{
		$username = $this->input->post('login');
		$password = $this->input->post('senha');
		
		if ($this->auth->try_login($username, $password))
		{
			$this->load->model('logs_model', 'model');
			$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
			$this->model->inserirLogAcesso($id_usuario);
			
			redirect('usuarios/lista');
		}
		else
		{
			redirect('login/');
		}
	}
	
	function sair ()
	{
		$this->auth->logout();
		redirect('login/');
	}
}

/* End of file login.php */
/* Location: ./application/admin/controllers/login.php */