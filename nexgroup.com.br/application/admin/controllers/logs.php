<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('logs/acessos/');
	}
	
	function acessos ($offset = "")
	{
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/logs/acessos/',
			'total_rows'	=> $this->model->numAcessos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numAcessos(),
    		'logs'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcessos($this->input->post('keyword')) : $this->model->getAcessos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('logs/logs.php', $data);
	}
	
	function acoes ($offset = "")
	{
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/logs/acoes/',
			'total_rows'	=> $this->model->numAcoes(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numAcoes(),
    		'logs'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcoes($this->input->post('keyword')) : $this->model->getAcoes($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('logs/logs_acoes.php', $data);
	}	
}

/* End of file logs.php */
/* Location: ./system/application/controllers/logs.php */