<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function clean($array){
	$return = array();
	$search = array('"',';','\n','\r');
	foreach($array as $i => $x){
		$return[$i] = trim(strip_tags(str_replace($search,'',$x)));
	}
	return $return;
}



class Expinteresse extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->auth->check();
	}

	function index($data){
		$data = base64_decode($data);
		$data = explode('|', $data);

		$this->load->model('expinteresse_model', 'model');
		$this->load->helper('download');
		$dados = $this->model->getInteresse($data[0],$data[1],$data[2],$data[3]);
		$linhas = array();
		foreach($dados as $x){
			$a = clean($x);
			$a = '"'.implode('";"',$a).'"';
			array_push($linhas,$a);
		}
		force_download('interesse-'.time().'.csv',implode(PHP_EOL,$linhas));
	}
	
}
?>