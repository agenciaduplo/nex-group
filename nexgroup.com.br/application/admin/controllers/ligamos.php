<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Ligamos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}	
	
	function index ()
	{
		header('Location: ligamos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('ligamos_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/ligamos/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numLigamosBusca($this->input->post('keyword')) : $this->model->numLigamos($offset),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numLigamos(),
			'ligamos'    	=> ($this->input->post('keyword')) ? $this->model->buscaLigamos($this->input->post('keyword')) : $this->model->getLigamos($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('ligamos/ligamos.php', $data);
	}
	
	function visualizar ($id_ligamo = 0)
	{
		$this->load->model('ligamos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'ligacao' 	=> $this->model->getLigamoVisualizar($id_ligamo)
			);
			
			$this->load->view('ligamos/ligamos.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_ligamo = 0)
	{
		$this->load->model('ligamos_model', 'model');

		try
		{
			$this->model->delLigamo($id_ligamo);
			redirect('ligamos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file ligamos.php */
/* Location: ./system/application/controllers/ligamos.php */