<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interesses extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: interesses/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('interesses_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/interesses/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numInteressesBusca($this->input->post('keyword')) : $this->model->numInteresses(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numInteresses(),
			'interesses'   	=> ($this->input->post('keyword')) ? $this->model->buscaInteresses($this->input->post('keyword')) : $this->model->getInteresses($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('interesses/interesses.php', $data);
	}
	
	
	function visualizar ($id_interesse = 0)
	{
		$this->load->model('interesses_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'interesse' 		=> $this->model->getInteresseVisualizar($id_interesse)
			);
			
			$this->load->view('interesses/interesses.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_interesses = 0)
	{
		$this->load->model('interesses_model', 'model');

		try
		{
			$this->model->delInteresse($id_interesses);
			redirect('interesses/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file interesses.php */
/* Location: ./system/application/controllers/falecom.php */