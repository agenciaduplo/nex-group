<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Responsabilidades_Social extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
		$this->auth->check();
		$this->load->model('responsabilidades_social_model', 'model');
	}
    	
	function index()
	{
		redirect('responsabilidades_social/lista/');
	}
	
	function lista($offset = "")
	{
		$config = array (
			'base_url'		=> base_url().'admin.php/responsabilidades_social/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numResponsabilidadesSocialBusca($this->input->post('keyword')) : $this->model->numResponsabilidadesSocial(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  				=> $this->model->getPermissoes(),
			'cadastrados' 				=> $this->model->numResponsabilidadesSocial(),
    		'responsabilidades_social'  => ($this->input->post('keyword')) ? $this->model->buscaResponsabilidadesSocial($this->input->post('keyword')) : $this->model->getResponsabilidadesSocial($offset),
			'paginacao'	  				=> $this->pagination->create_links()
		);
		
		$this->load->view('responsabilidades_social/lista', $data);
	}
	
	function cadastro()
	{
		$data = array (
			'permissoes' => $this->model->getPermissoes()
		);

		$this->load->view('responsabilidades_social/cadastro', $data);
	}
	
	function editar ($id_responsabilidade = 0)
	{
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'responsabilidade' 	=> $this->model->getResponsabilidadeSocialId($id_responsabilidade)
			);
			
			$this->load->view('responsabilidades_social/cadastro', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar()
	{
		try
		{	
			$data_ex = explode(" ", $this->input->post('data_cadastro'));

			$date = implode("-",array_reverse(explode("/", $data_ex[0]))) . ' ' . $data_ex[1];	
			
			$data = array (
				'nome' 			=> $this->input->post('nome'),
				'descricao' 	=> $this->input->post('descricao'),
				'ano' 			=> $this->input->post('ano'),
				'ordem' 		=> $this->input->post('ordem'),
				'ativo' 		=> $this->input->post('ativo'),
				'data_cadastro' => $date
			);

			$insert_id = $this->model->setResponsabilidadeSocial($data, $this->input->post('id_responsabilidade'));
			
			if ($this->input->post('id_responsabilidade'))
				redirect('responsabilidades_social/editar/' . $this->input->post('id_responsabilidade'));
			else
				redirect('responsabilidades_social/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar($id_responsabilidade = 0)
	{
		try
		{
			$responsabilidade = $this->model->getResponsabilidadeSocialId($id_responsabilidade);

			if($responsabilidade->imagem_destaque)
			{
				@unlink('./uploads/responsabilidades_social/zoom/'  . $responsabilidade->imagem_destaque);
				@unlink('./uploads/responsabilidades_social/medium/'. $responsabilidade->imagem_destaque);
				@unlink('./uploads/responsabilidades_social/thumb/' . $responsabilidade->imagem_destaque);
			}

			$imagens = $this->model->getImagens($responsabilidade->id_responsabilidade);

			if($imagens)
			{
				foreach ($imagens as $imagem)
				{						
					if($imagem->foto)
					{
						if(@file_exists('./uploads/responsabilidades_social/galeria/' . $imagem->foto))
							@unlink('./uploads/responsabilidades_social/galeria/' . $imagem->foto);
						
						if(@file_exists('./uploads/responsabilidades_social/galeriaThumb/' . $imagem->foto))
							@unlink('./uploads/responsabilidades_social/galeriaThumb/' . $imagem->foto);
					}
				}

				$this->model->delImagem('', $responsabilidade->id_responsabilidade);
			}

			$this->model->delResponsabilidadeSocial($responsabilidade->id_responsabilidade);
			
			redirect('responsabilidades_social/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	//FIM creches CONTROLLER
	
	function imagens($id_responsabilidade)
	{	
		try
		{
			$data = array (
				'id_responsabilidade' 	=> $id_responsabilidade,
				'permissoes'  			=> $this->model->getPermissoes(),
				'imagens' 				=> $this->model->getImagens($id_responsabilidade)
				
			);
			
			$this->load->view('responsabilidades_social/imagens', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}

	function setImagem()
	{
		try
		{
			if($this->input->post('id_responsabilidade'))
			{
				$data = array (
					'id_responsabilidade' 	=> $this->input->post('id_responsabilidade'),
					'data_cadastro'			=> date('Y-m-d H:i:s'),	
					'legenda'				=> $this->input->post('legenda')
				);
			
				$this->model->setImagem($data);					
							
				$id_responsabilidade = $this->input->post('id_responsabilidade');
				
				redirect("responsabilidades_social/imagens/$id_responsabilidade");
			}
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}	
	
	function delImagem ($id_foto,$id_responsabilidade)
	{
		try
		{
			$imagem = $this->model->getImagemGaleria($id_foto);

			if($imagem->foto)
			{
				@unlink('./uploads/responsabilidades_social/galeria/' . $imagem->foto);		
				@unlink('./uploads/responsabilidades_social/galeriaThumb/' . $imagem->foto);
			}
			
			$this->model->delImagem($id_foto);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
		
		redirect("responsabilidades_social/imagens/$id_responsabilidade");
	}	
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */