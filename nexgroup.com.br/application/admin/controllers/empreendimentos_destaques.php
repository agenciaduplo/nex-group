<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empreendimentos_destaques extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('empreendimentos_destaques/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/empreendimentos_destaques/lista/',
		);
		
		$data = array (
			'permissoes'  				=> $this->model->getPermissoes(),
			'empreendimentos'			=> $this->model->getEmpreendimentosDestaque(),
		);
		
		$this->load->view('empreendimentos/empreendimentos.destaques.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('empreendimentos_destaques_model', 'model');
		
		$data = array (
			'empreendimentos' 	=> $this->model->getEmpreendimentos(),
			'permissoes'  		=> $this->model->getPermissoes()
		);

		$this->load->view('empreendimentos/empreendimentos.cadastro.destaques.php', $data);
	}
	
	function editar ($id_destaque = 0)
	{
		$this->load->model('empreendimentos_destaques_model', 'model', TRUE);
		
		try
		{ 
			
			$data = array (
				'destaque'				=> $this->model->getDestaque($id_destaque),
				'empreendimentos' 	=> $this->model->getEmpreendimentos(),
			);
			
			$this->load->view('empreendimentos/empreendimentos.cadastro.destaques.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
	$this->load->model('empreendimentos_destaques_model', 'model');
		
		try
		{
			if($this->input->post('id_destaque_empreendimento'))
			{
				
				$data = array (
					'id_empreendimento' 	=> $this->input->post('empreendimento'),
					'legenda'				=> $this->input->post('legenda')
				);
				
				$id_destaque 		= $this->input->post('id_destaque_empreendimento');
				
				$this->model->setDestaque($data, $id_destaque);
				
				redirect("empreendimentos_destaques/editar/$id_destaque");			
				
			}else{
				
				$data = array (
					'id_empreendimento' 	=> $this->input->post('empreendimento'),
					'legenda'				=> $this->input->post('legenda')
				);
				
				$id_destaque 		= $this->input->post('id_destaque');
				
				$this->model->setDestaque($data);
				
				redirect("empreendimentos_destaques/lista");	
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_destaque_empreendimento = 0)
	{
		$this->load->model('empreendimentos_destaques_model', 'model');

		try
		{
						
			$destaque = $this->model->getDestaque($id_destaque_empreendimento);
			if($destaque->imagem){
				if(file_exists('./uploads/imoveis/destaques/' . $destaque->imagem)){
					unlink('./uploads/imoveis/destaques/' . $destaque->imagem);
				}
			}
			$this->model->delDestaqueEmpreendimento($id_destaque_empreendimento);
			
			
			redirect('empreendimentos_destaques/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
	/*INICIO ORDEM DOS ITENS DO EMPREENDIMENTO*/
	function sobeDestaque()
	{
		$id_empreendimento = $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
	
		$rowAtual = $this->model->getEmpreendimento($id_empreendimento);
		$pos_atual = $rowAtual->destaque_home_ordem;
		
		$prox_pos = $pos_atual - 1;
		$rowProximo = $this->model->getEmpreendimentoByOrdemDestaque($prox_pos, 'menor');
		if(!empty($rowProximo)){
			$pos_proximo = $rowProximo->destaque_home_ordem;
	
			if($rowProximo){
				$this->model->setOrdem($rowProximo->id_empreendimento, $pos_atual);
				$this->model->setOrdem($rowAtual->id_empreendimento, $pos_proximo);
			}
		}
		$dataVisualizar = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'empreendimentos'	=> $this->model->getEmpreendimentosDestaque(),
		);
		
		$this->load->view('empreendimentos/empreendimentos.destaques.lista.php', $dataVisualizar);
	}
	
	function desceDestaque()
	{
		$id_empreendimento = $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
	
		$rowAtual = $this->model->getEmpreendimento($id_empreendimento);
		$pos_atual = $rowAtual->destaque_home_ordem;
		
		$prox_pos = $pos_atual + 1;
		$rowProximo = $this->model->getEmpreendimentoByOrdemDestaque($prox_pos, 'maior');
		if(!empty($rowProximo)){
			$pos_proximo = $rowProximo->destaque_home_ordem;
	
			if($rowProximo){
				$this->model->setOrdem($rowProximo->id_empreendimento, $pos_atual);
				$this->model->setOrdem($rowAtual->id_empreendimento, $pos_proximo);
			}
		}
		
		$dataVisualizar = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'empreendimentos'	=> $this->model->getEmpreendimentosDestaque(),
		);
		
		$this->load->view('empreendimentos/empreendimentos.destaques.lista.php', $dataVisualizar);
	
		//redirect('empreendimentos/lista/');
	}
	/*FIM ORDEM DOS ITENS DO EMPREENDIMENTO*/
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */