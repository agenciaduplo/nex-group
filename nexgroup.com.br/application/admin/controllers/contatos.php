<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contatos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: contatos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('contato_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/contatos/lista/',
			'total_rows'	=> $this->model->numContatos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numContatos(),
			'contato'     	=> ($this->input->post('keyword')) ? $this->model->buscaContatos($this->input->post('keyword')) : $this->model->getContatos($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('contato/contato.php', $data);
	}
	
	
	function buscar ($offset = "")
	{
		$this->load->model('contato_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/contato/buscar/',
			'total_rows'	=> $this->model->numContatos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config);
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numContatos(),
    		'contato'     	=> $this->model->buscaContatos($dataBusca),
   		);
		
		$this->load->view('contato/contato.php', $data);
	}
	
	function visualizar ($id_contato = 0)
	{
		$this->load->model('contato_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'contato' 		=> $this->model->getContatoVisualizar($id_contato)
			);
			
			$this->load->view('contato/contato.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_contato = 0)
	{
		$this->load->model('contato_model', 'model');

		try
		{
			$this->model->delContato($id_contato);
			redirect('contatos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contato.php */
/* Location: ./system/application/controllers/falecom.php */