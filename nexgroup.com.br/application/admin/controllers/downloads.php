<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downloads extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function download ($arquivo)
	{
		$this->load->helper('download');
		$data = file_get_contents("./uploads/trabalhe_conosco/".$arquivo);
		force_download($arquivo, $data);
	}
}

/* End of file contato.php */
/* Location: ./system/application/controllers/falecom.php */