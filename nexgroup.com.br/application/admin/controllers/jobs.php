<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: jobs/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('jobs_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/jobs/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numJobsBusca($this->input->post('keyword')) : $this->model->numJobs(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numJobs(),
			'jobs'     	=> ($this->input->post('keyword')) ? $this->model->buscaJobs($this->input->post('keyword')) : $this->model->getJobs($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('jobs/jobs.php', $data);
	}
	
	function visualizar ($id_job = 0)
	{
		$this->load->model('jobs_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'job' 		=> $this->model->getJobVisualizar($id_job)
			);
			
			$this->load->view('jobs/job.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_job = 0)
	{
		$this->load->model('jobs_model', 'model');

		try
		{
			$arquivo = $this->model->getJobVisualizar($id_job);
									
			if($arquivo->arquivo):
				unlink('./uploads/trabalhe_conosco/' . $arquivo->arquivo);
			endif;
			
			$this->model->delJob($id_job);
			redirect('jobs/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	function download ($arquivo)
	{
		$this->load->helper('download');
		$data = file_get_contents("./uploads/trabalhe_conosco/".$arquivo);
		force_download($arquivo, $data);
	}
}

/* End of file contato.php */
/* Location: ./system/application/controllers/falecom.php */