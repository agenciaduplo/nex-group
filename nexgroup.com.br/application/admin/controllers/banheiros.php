<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banheiros extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	
	function index ()
	{
		redirect('banheiros/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('banheiros_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/banheiros/lista/',
			'total_rows'	=> $this->model->numBanheiros(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numBanheiros(),
    		'banheiros'   	=> ($this->input->post('keyword')) ? $this->model->buscaBanheiros($this->input->post('keyword')) : $this->model->getBanheiros($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('banheiros/banheiros.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('banheiros_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('banheiros/banheiros.cadastro.php', $data);
	}
	
	function editar ($id_banheiro = 0)
	{
		$this->load->model('banheiros_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'banheiro' 		=> $this->model->getBanheiro($id_banheiro)
			);
			
			$this->load->view('banheiros/banheiros.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('banheiros_model', 'model');
		
		try
		{			
			$data = array (
				'banheiro' 				=> $this->input->post('banheiro'),
				'data_cadastro' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setBanheiro($data, $this->input->post('id_banheiro'));
				
			if ($this->input->post('id_banheiro'))
				redirect('banheiros/editar/' . $this->input->post('id_banheiro'));
			else
				redirect('banheiros/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_banheiro = 0)
	{
		$this->load->model('banheiros_model', 'model');

		try
		{
			$this->model->delBanheiro($id_banheiro);
			redirect('banheiros/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */