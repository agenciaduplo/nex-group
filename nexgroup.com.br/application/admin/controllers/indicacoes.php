<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Indicacoes extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}	
	
	function index ()
	{
		header('Location: indicacoes/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('indicacoes_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/indicacoes/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numIndicacoesBusca($this->input->post('keyword')) : $this->model->numIndicacoes($offset),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numIndicacoes(),
			'indicacoes'    => ($this->input->post('keyword')) ? $this->model->buscaIndicacoes($this->input->post('keyword')) : $this->model->getIndicacoes($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('indicacoes/indicacoes.php', $data);
	}
	
	function visualizar ($id_indique = 0)
	{
		$this->load->model('indicacoes_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'indicacao' 	=> $this->model->getIndicacaoVisualizar($id_indique)
			);
			
			$this->load->view('indicacoes/indicacoes.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_indique = 0)
	{
		$this->load->model('indicacoes_model', 'model');

		try
		{
			$this->model->delIndicacao($id_indique);
			redirect('indicacoes/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file indicacoes.php */
/* Location: ./system/application/controllers/indicacoes.php */