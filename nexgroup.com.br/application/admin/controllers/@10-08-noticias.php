<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('noticias/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('noticias_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/noticias/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numNoticiasBusca($this->input->post('keyword')) : $this->model->numNoticias(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numNoticias(),
    		'noticias'  		=> ($this->input->post('keyword')) ? $this->model->buscaNoticias($this->input->post('keyword')) : $this->model->getNoticias($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('noticias/noticias.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('noticias_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('noticias/noticias.cadastro.php', $data);
	}
	
	function editar ($id_noticia = 0)
	{
		$this->load->model('noticias_model', 'model', TRUE);
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'noticia' 			=> $this->model->getNoticia($id_noticia)
			);
			
			$this->load->view('noticias/noticias.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{	
			$date = implode("-",array_reverse(explode("/",$this->input->post('data_cadastro'))));	
			
			$data = array (
				'titulo' 			=> $this->input->post('titulo'),
				'descricao' 		=> $this->input->post('descricao'),
				'ativo' 			=> $this->input->post('ativo'),
				'data_cadastro' 	=> $date
				
			);

			$insert_id = $this->model->setNoticia($data, $this->input->post('id_noticia'));
			
			if ($this->input->post('id_noticia'))
				redirect('noticias/editar/' . $this->input->post('id_noticia'));
			else
				redirect('noticias/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_noticia = 0)
	{
		$this->load->model('noticias_model', 'model');

		try
		{
						
			$imagens = $this->model->getImagens($id_noticia);
			if($imagens){
				foreach ($imagens as $imagem) {						
					if($imagem->imagem):
						if(@file_exists('./uploads/noticias/galeria/' . $imagem->imagem)){
							@unlink('./uploads/noticias/galeria/' . $imagem->imagem);
						}
						if(@file_exists('./uploads/noticias/galeriaThumb/' . $imagem->imagem)){
							@unlink('./uploads/noticias/galeriaThumb/' . $imagem->imagem);
						}
					endif;
				}
				$this->model->delImagem('',$id_noticia);
			}
			$this->model->delNoticia($id_noticia);
			
			redirect('noticias/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	//FIM NOTICIAS CONTROLLER
	
	function imagens($id_noticia)
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{
			$data = array (
				'id_noticia'		=> $id_noticia,
				'permissoes'  		=> $this->model->getPermissoes(),
				'imagens' 			=> $this->model->getImagens($id_noticia)
				
			);
			
			$this->load->view('noticias/noticias.imagens.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}

	function setImagem()
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{
			if($this->input->post('id_noticia'))
			{
					$data = array (
						'id_noticia' 			=> $this->input->post('id_noticia'),
						'legenda'				=> $this->input->post('legenda')
					);
			
				$this->model->setImagem($data);					
							
				$id_noticia = $this->input->post('id_noticia');
				
				redirect("noticias/imagens/$id_noticia");
				
			}
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}	
	
	function delImagem ($id_galeria,$id_noticia)
	{
		$this->load->model('noticias_model', 'model');
		
		try
		{
			$imagem = $this->model->getImagemGaleria($id_galeria);
			if($imagem->imagem)
			{
				@unlink('./uploads/noticias/galeria/' . $imagem->imagem);		
				@unlink('./uploads/noticias/galeriaThumb/' . $imagem->imagem);
			}
			
			$this->model->delImagem($id_galeria);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
		
		redirect("noticias/imagens/$id_noticia");
	}	
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */