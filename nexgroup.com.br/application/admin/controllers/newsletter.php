<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: newsletter/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('newsletter_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/newsletter/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numNewsletterBusca($this->input->post('keyword')) : $this->model->numNewsletter(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numNewsletter(),
			'newsletter'   	=> ($this->input->post('keyword')) ? $this->model->buscaNewsletter($this->input->post('keyword')) : $this->model->getNewsletter($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('newsletter/newsletter.php', $data);
	}
	
	
	function visualizar ($id_newsletter = 0)
	{
		$this->load->model('newsletter_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'newsletter' 		=> $this->model->getNewsletterVisualizar($id_newsletter)
			);
			
			$this->load->view('newsletter/newsletter.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_newsletter = 0)
	{
		$this->load->model('newsletter_model', 'model');

		try
		{
			$this->model->delNewsletter($id_newsletter);
			redirect('newsletter/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file newsletter.php */
/* Location: ./system/application/controllers/falecom.php */