<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*httsp://www.nexgroup.com.br/*/
class Empreendimentos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');

		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('empreendimentos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$config = array (
			'base_url'		=> base_url() . 'admin.php/empreendimentos/lista/',
			// 'base_url'		=> 'http://www.nexgroup.com.br/admin.php/empreendimentos/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numEmpreendimentosBusca($this->input->post('keyword'),$offset) : $this->model->numEmpreendimentos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numEmpreendimentos(),
    		'empreendimentos'  	=> ($this->input->post('keyword')) ? $this->model->buscaEmpreendimentos($this->input->post('keyword'),$offset) : $this->model->getEmpreendimentos($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('empreendimentos/empreendimentos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$data = array (
			'empresas'		=> $this->model->getEmpresas(),
			'status'		=> $this->model->getStatus(),
			'tipos_imoveis'	=> $this->model->getTipos_imoveis(),
			'estados'		=> $this->model->getEstados(),
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('empreendimentos/empreendimentos.cadastro.php', $data);
	}
	
	function editar ($id_empreendimento = 0)
	{
		$this->load->model('empreendimentos_model', 'model', TRUE);
		
		try
		{ 
			$emp = $this->model->getEmpreendimento($id_empreendimento);
			
			$data = array (
				'itens'  					=> $this->model->getItens($id_empreendimento),
				'fases'  					=> $this->model->getFases($id_empreendimento),
				'itensFases'  				=> $this->model->getItensFases($id_empreendimento),
				'FasesFotos'  				=> $this->model->getFasesFotos($id_empreendimento),
				'empreendimentosFotos'  	=> $this->model->getTiposFotos($id_empreendimento),
				'dormitorios'  				=> $this->model->getDormitorios(),
				'dormitorios_atribuidos'  	=> $this->model->getDormitoriosAtribuidos($id_empreendimento),
				'banheiros'  				=> $this->model->getBanheiros(),
				'banheiros_atribuidos' 	 	=> $this->model->getBanheirosAtribuidos($id_empreendimento),
				'faixaPrecos'  				=> $this->model->getFaixaPrecos(),
				'faixaPrecos_atribuidos'  	=> $this->model->getFaixaPrecosAtribuidos($id_empreendimento),
				'semelhantes'  				=> $this->model->getEmpreendimentosSemelhantes(),
				'semelhantes_atribuidos'  	=> $this->model->getSemelhantesAtribuidos($id_empreendimento),
				'empresas'					=> $this->model->getEmpresas(),
				'status'					=> $this->model->getStatus(),
				'tipos_midias'				=> $this->model->getTiposMidias(),
				'tipos_imoveis'				=> $this->model->getTipos_imoveis(),
				'permissoes'  				=> $this->model->getPermissoes(),
				'empreendimento' 			=> $emp,
				'estados'					=> $this->model->getEstados(),
				'cidades' 					=> $this->model->getCidades($emp->id_estado)
			
			);
			
			$this->load->view('empreendimentos/empreendimentos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{	
			$date = implode("-",array_reverse(explode("/",$this->input->post('data_cadastro'))));	
			
			$data = array(
				'empreendimento' 			=> $this->input->post('empreendimento'),
				'id_empresa' 				=> $this->input->post('empresas'),
				'id_tipo_imovel' 			=> $this->input->post('tipos'),
				'id_cidade' 				=> $this->input->post('cidade'),
				'latitude' 					=> $this->input->post('end_latitude'),
				'longitude' 				=> $this->input->post('end_longitude'),
				'endereco_empreendimento' 	=> $this->input->post('end_empreendimento'),
				'endereco_central_vendas' 	=> $this->input->post('end_central_vendas'),
				'telefone_central_vendas' 	=> $this->input->post('tel_central_vendas'),
				'horario_atendimento' 		=> $this->input->post('horarioAtendimento'),
				'id_status' 				=> $this->input->post('status'),
				'status_label' 				=> $this->input->post('status_label'),
				'tags_title' 				=> $this->input->post('tags_title'),
				'tags_description' 			=> $this->input->post('tags_description'),
				'tags_keywords' 			=> $this->input->post('tags_keywords'),
				'descricao' 				=> $this->input->post('descricao'),
				'porcentagem_obras' 		=> $this->input->post('porcentagem_obras'),
				'ativo' 					=> $this->input->post('ativo'),
				'link_corretor' 			=> $this->input->post('link_corretor'),
				'hotsite' 					=> $this->input->post('hotsite'),
				'dormitorio' 				=> $this->input->post('dormitorio'),
				'vagas' 					=> $this->input->post('vagas'),
				'metragem' 					=> $this->input->post('metragem'),
				'chamada_empreendimento' 	=> $this->input->post('chamada_empreendimento'),
				'segunda_chamada_empreendimento' => $this->input->post('segunda_chamada_empreendimento'),
				'decorado'					=> $this->input->post('decorado'),
				'destaque_home' 			=> $this->input->post('destaque_home'),
				'data_update' 				=> date('Y-m-d H:i:s'),
				'data_cadastro' 			=> $date
			);

			$id_empreendimento 	= $this->input->post('id_empreendimento');
			$dormitorios 		= $this->input->post('dormitorios');
			$banheiros 			= $this->input->post('banheiros');
			$preco 				= $this->input->post('faixaPreco');
			$semelhantes 		= $this->input->post('semelhantes');
			
			$insert_id = $this->model->setEmpreendimento($data,$id_empreendimento);
			
			if($id_empreendimento){
				if($dormitorios){
					$this->salvarDormitorios($id_empreendimento, $dormitorios);
				}else{
					$dormitorios = '';
					$this->salvarDormitorios($id_empreendimento, $dormitorios);
				}
				if($banheiros){
					$this->salvarBanheiros($id_empreendimento, $banheiros);
				}else{
					$banheiros = '';
					$this->salvarBanheiros($id_empreendimento, $banheiros);
				}
				if($preco){
					$this->salvarFaixaPreco($id_empreendimento, $preco);
				}else{
					$preco = '';
					$this->salvarFaixaPreco($id_empreendimento, $preco);
				}
				if($semelhantes){
					$this->salvarSemelhantes($id_empreendimento, $semelhantes);
				}else{
					$semelhantes = '';
					$this->salvarSemelhantes($id_empreendimento, $semelhantes);
				}
			}
			
			if ($this->input->post('id_empreendimento'))
				redirect('empreendimentos/editar/' . $this->input->post('id_empreendimento'));
			else
				redirect('empreendimentos/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_empreendimento = 0)
	{
		$this->load->model('empreendimentos_model', 'model');

		try
		{
						
			/*$imagens = $this->model->getImagens($id_empreendimento);
			if($imagens){
				foreach ($imagens as $imagem) {						
					if($imagem->imagem):
						if(file_exists('./assets/uploads/empreendimentos/galeria/' . $imagem->imagem)){
							unlink('./assets/uploads/empreendimentos/galeria/' . $imagem->imagem);
						}
						if(file_exists('./assets/uploads/empreendimentos/galeria/thumb/' . $imagem->imagem)){
							unlink('./assets/uploads/empreendimentos/galeria/thumb/' . $imagem->imagem);
						}
					endif;
				}
				$this->model->delImagem('',$id_empreendimento);
			}*/
			$this->model->delEmpreendimento($id_empreendimento);
			
			redirect('empreendimentos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvarDataAtualizacao ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{	
			if($this->input->post('data_atualizacao_itens'))
				$date_itens = implode("-",array_reverse(explode("/",$this->input->post('data_atualizacao_itens'))));	
			if($this->input->post('data_atualizacao_fotos'))
				@$date_fotos = implode("-",array_reverse(explode("/",$this->input->post('data_atualizacao_fotos'))));	
			
			if(@$date_fotos && @$date_itens){
				$data = array (
					'data_atualizacao_itens_obras'	=>$date_itens,
					'data_atualizacao_fotos_obras' 	=> $date_fotos
					
				);
			}elseif (@$date_fotos){
				$data = array (
					'data_atualizacao_fotos_obras' 	=> $date_fotos
				);
			}else{
				$data = array (
					'data_atualizacao_itens_obras'	=>$date_itens,
				);
			}
			$id_empreendimento 	= $this->input->post('id_empreendimento');
						
			$insert_id = $this->model->setEmpreendimento($data,$id_empreendimento);
			
			redirect('empreendimentos/editar/' . $id_empreendimento);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvarMidia()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			if($this->input->post('id_empreendimento'))
			{
				$id_empreendimento =  $this->input->post('id_empreendimento');
				
				$ultimo = $this->model->getMaxPosicaoMidias($this->input->post('tipoMidia'),$id_empreendimento);	
				$ultimo++;
				
				$data = array (
					'id_empreendimento' 	=> $this->input->post('id_empreendimento'),
					'id_tipo_midia' 		=> $this->input->post('tipoMidia'),
					'tipo_arquivo'			=> $this->input->post('tipoArquivo'),
					'legenda'				=> $this->input->post('legenda'),
					'posicao'				=> $ultimo
				);
				
				
				$tipoArquivo = $this->input->post('tipoArquivo');
				$tipoMidia = $this->input->post('tipoMidia');
				
				$this->model->setEmpreendimentoMidia($data, $id_empreendimento,$tipoArquivo,$tipoMidia);
				
				redirect("empreendimentos/editar/$id_empreendimento#GalFotos");			
				
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}
	
	/*INICIO ORDEM FOTOS DO EMPREENDIMENTO*/
	function sobeMidia()
	{
		
		$id_foto 			= $this->input->post('id_midia');
		$id_tipo_midia 		= $this->input->post('id_tipo_midia');
		$id_empreendimento 	= $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		
		$pos_atual = $this->model->getPosicaoMidias($id_foto);
		
		$prox_pos = $pos_atual - 1;
					
		$primeiro = $this->model->getMinPosicaoMidias($id_tipo_midia,$id_empreendimento);		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoMidias($prox_pos,$id_tipo_midia,$id_empreendimento);
				//var_dump($retorno);exit;
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual - 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoMidias($id_foto,$pos_atual, $prox_pos,$id_tipo_midia,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  			=> $this->model->getPermissoes(),
				'empreendimento'		=> $this->model->getEmpreendimento($id_empreendimento),
				'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.midias.lista.php', $dataVisualizar);
					
		
		//redirect('empreendimentos/lista/');
	}
	
	function desceMidia()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$id_foto 			= $this->input->post('id_midia');
		$id_tipo_midia 		= $this->input->post('id_tipo_midia');
		$id_empreendimento 	= $this->input->post('id_empreendimento');
		
		$pos_atual = $this->model->getPosicaoMidias($id_foto);
		
		$prox_pos = $pos_atual + 1;
		 
		$ultimo = $this->model->getMaxPosicaoMidias($id_tipo_midia,$id_empreendimento);		
		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoMidias($prox_pos,$id_tipo_midia,$id_empreendimento);
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual + 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoMidias($id_foto,$pos_atual, $prox_pos,$id_tipo_midia,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  			=> $this->model->getPermissoes(),
				'empreendimento'		=> $this->model->getEmpreendimento($id_empreendimento),
				'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.midias.lista.php', $dataVisualizar);
				
		
		//redirect('empreendimentos/lista/');
	}	
	/*FIM ORDEM FOTOS DO EMPREENDIMENTO*/
	
	function getMidia(){
		$this->load->model('empreendimentos_model', 'model');
		
		$id_midia		   = $this->input->post('id_midia');
		
		$data = array (
			'midia'  => $this->model->getMidia($id_midia)	
		);	
					
		$this->load->view('empreendimentos/empreendimentos.midias.legenda.php', $data);
	}
	
	function salvaLegenda(){
		$this->load->model('empreendimentos_model', 'model');
		
		$id_midia		   = $this->input->post('id_midia');
		$id_empreendimento = $this->input->post('id_empreendimento'); 
		$data = array (
			'legenda'  => $this->input->post('legenda')
		);	

		$this->model->setLegenda($data,$id_midia);
		
		$dataVisualizar = array (
				'midia'  => $this->model->getMidia($id_midia)
		);	
					
		$this->load->view('empreendimentos/empreendimentos.midias.legenda.lista.php', $dataVisualizar);
	}
	
	function getFotoFase(){
		$this->load->model('empreendimentos_model', 'model');
		
		$id_foto		   = $this->input->post('id_foto_fase');
		
		$data = array (
			'fotoFase'  => $this->model->getFotoFase($id_foto)	
		);	
					
		$this->load->view('empreendimentos/empreendimentos.midias.fase.legenda.php', $data);
	}
	
	function salvaLegendaFotoFase(){
		$this->load->model('empreendimentos_model', 'model');
		
		$id_foto	   		= $this->input->post('id_foto_fase');
		$id_empreendimento 	= $this->input->post('id_empreendimento'); 
		
		$data = array (
			'legenda'  => $this->input->post('legenda')
		);	

		$this->model->setLegendaFase($data,$id_foto);
		
		$dataVisualizar = array (
				'fotoFase'  => $this->model->getFotoFase($id_foto)
		);	
					
		$this->load->view('empreendimentos/empreendimentos.midias.fase.legenda.lista.php', $dataVisualizar);
	}

	function salvarItens()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			if($this->input->post('id_empreendimento'))
			{
				
				$id_empreendimento= $this->input->post('id_empreendimento');
				
				
				if($this->input->post('id_item'))
				{
					$data = array (
						'id_empreendimento' 	=> $this->input->post('id_empreendimento'),
						'item'					=> $this->input->post('titulo')
						
					);
					//'destaque'				=> $this->input->post('destaque')
					$id_item = $this->input->post('id_item');

					$this->model->setItem($data, $id_item);	
					
					$dataVisualizar = array (
						'permissoes'  	=> $this->model->getPermissoes(),
						'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
						'itens'  				=> $this->model->getItens($id_empreendimento)
					);	
					
					$this->load->view('empreendimentos/empreendimentos.item.lista.php', $dataVisualizar);			
				}
				else
				{
					$ultimo = $this->model->getMaxPosicao($id_empreendimento);	
					$ultimo++;
					$data = array (
						'id_empreendimento' 	=> $this->input->post('id_empreendimento'),
						'item'					=> $this->input->post('titulo'),
						'posicao'				=>$ultimo
						
					);
					//'destaque'				=> $this->input->post('destaque')
					$this->model->setItem($data, $id_item = "");	
					redirect("empreendimentos/editar/$id_empreendimento");				
				}
				
				
				
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}
	
	/*INICIO ORDEM DOS ITENS DO EMPREENDIMENTO*/
	function sobeItem()
	{
		
		$id_item = $this->input->post('id_item');
		$id_empreendimento = $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		
		$pos_atual = $this->model->getPosicao($id_item);
		
		$prox_pos = $pos_atual - 1;
					
		$primeiro = $this->model->getMinPosicao($id_empreendimento);		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicao($prox_pos,$id_empreendimento);
				//var_dump($retorno);exit;
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual - 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicao($id_item,$pos_atual, $prox_pos,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itens'  			=> $this->model->getItens($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.item.lista.php', $dataVisualizar);
					
		
		//redirect('empreendimentos/lista/');
	}
	
	function desceItem()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$id_item = $this->input->post('id_item');
		$id_empreendimento = $this->input->post('id_empreendimento');
		
		$pos_atual = $this->model->getPosicao($id_item);
		
		$prox_pos = $pos_atual + 1;
		 
		$ultimo = $this->model->getMaxPosicao($id_empreendimento);		
		
		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicao($prox_pos,$id_empreendimento);
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual + 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicao($id_item,$pos_atual, $prox_pos,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itens'  			=> $this->model->getItens($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.item.lista.php', $dataVisualizar);
				
		
		//redirect('empreendimentos/lista/');
	}	
	/*FIM ORDEM DOS ITENS DO EMPREENDIMENTO*/
	
	function salvarFases()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			if($this->input->post('id_empreendimento'))
			{
				$id_empreendimento= $this->input->post('id_empreendimento');
				if($this->input->post('id_fase'))
				{
					$date = implode("-",array_reverse(explode("/",$this->input->post('data_atualizacao'))));	
					$data = array (
						'id_empreendimento' 	=> $this->input->post('id_empreendimento'),
						'titulo'				=> $this->input->post('titulo'),
						'mostrar_fase'			=> $this->input->post('mostrarFase'),
						'id_status'				=> $this->input->post('status'),
						'porcentagem_fase'		=> $this->input->post('porcentagem_fase'),
						'data_atualizacao'		=> $date
					);
					
					$id_fase = $this->input->post('id_fase');

					$this->model->setFase($data, $id_fase);	
					
					$dataVisualizar = array (
						'permissoes'  	=> $this->model->getPermissoes(),
						'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
						'status'			=> $this->model->getStatus(),
						'fases'  			=> $this->model->getFases($id_empreendimento)
					);	
					
					$this->load->view('empreendimentos/empreendimentos.fase.lista.php', $dataVisualizar);			
				}
				else
				{
					$date = implode("-",array_reverse(explode("/",$this->input->post('data_atualizacao'))));	
					$data = array (
						'id_empreendimento' 	=> $this->input->post('id_empreendimento'),
						'titulo'				=> $this->input->post('titulo'),
						'id_status'				=> $this->input->post('status'),
						'porcentagem_fase'		=> $this->input->post('porcentagem_fase'),
						'data_atualizacao'		=> $date
					);

					$this->model->setFase($data, $id_fase = "");	
					
					redirect("empreendimentos/editar/$id_empreendimento");				
				}
				
				
				
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}

	
	function salvarItensFases()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			if($this->input->post('id_empreendimento'))
			{
				$id_empreendimento= $this->input->post('id_empreendimento');
				$ultimo = $this->model->getMaxPosicaoItemFase($this->input->post('ItemFase'));	
				$ultimo++;
				
				if($this->input->post('id_item_fase'))
				{
					$data = array (
						'titulo'				=> $this->input->post('titulo'),
						'id_fase'				=> $this->input->post('ItemFase'),
						'porcentagem'			=> $this->input->post('porcentagem'),
						
					);
					
					$id_item_fase = $this->input->post('id_item_fase');

					$this->model->setItemFase($data, $id_item_fase);
					$dataVisualizar = array (
						'permissoes'  	=> $this->model->getPermissoes(),
						'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
						'itensFases'  	=> $this->model->getItensFases($id_empreendimento),
					);	
					$this->load->view('empreendimentos/empreendimentos.itemfase.lista.php', $dataVisualizar);			
				}
				else
				{
					$data = array (
						'titulo'				=> $this->input->post('titulo'),
						'id_fase'				=> $this->input->post('ItemFase'),
						'porcentagem'			=> $this->input->post('porcentagem'),
						'posicao'				=> $ultimo
					);

					$this->model->setItemFase($data, $id_item_fase = "");					
				
				redirect("empreendimentos/editar/$id_empreendimento");
				}
				
				
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
		
	}
	
	function sobeItemFase()
	{
		
		$id_item_fase = $this->input->post('id_item_fase');
		$id_fase = $this->input->post('id_fase');
		$id_empreendimento = $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		
		$pos_atual = $this->model->getPosicaoItemFase($id_item_fase);
		
		$prox_pos = $pos_atual - 1;
					
		$primeiro = $this->model->getMinPosicaoItemFase($id_fase);		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoItemFase($prox_pos,$id_fase);
				//var_dump($retorno);exit;
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual - 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoItemFase($id_item_fase,$pos_atual, $prox_pos,$id_fase);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itensFases'  		=> $this->model->getItensFases($id_empreendimento),
			);	
					
			$this->load->view('empreendimentos/empreendimentos.itemfase.lista.php', $dataVisualizar);
	}
	
	function desceItemFase()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$id_item_fase = $this->input->post('id_item_fase');
		$id_fase = $this->input->post('id_fase');
		$id_empreendimento = $this->input->post('id_empreendimento');
		
		$pos_atual = $this->model->getPosicaoItemFase($id_item_fase);
		
		$prox_pos = $pos_atual + 1;
		 
		$ultimo = $this->model->getMaxPosicaoItemFase($id_fase);		
		
		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoItemFase($prox_pos,$id_fase);
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual + 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoItemFase($id_item_fase,$pos_atual, $prox_pos,$id_fase);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itensFases'  		=> $this->model->getItensFases($id_empreendimento),
			);	
					
			$this->load->view('empreendimentos/empreendimentos.itemfase.lista.php', $dataVisualizar);
	}	
	
	
	function salvarDormitorios($id_empreendimento,$dormitorios)
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			//if($this->input->post('id_empreendimento'))
			//{
			//	$id_empreendimento = $this->input->post('id_empreendimento');
				$this->model->delDormitorios($id_empreendimento);
				
				//$dormitorios =  $this->input->post('dormitorios');
			if($dormitorios){
				foreach ($dormitorios as $row):
					$data = array (
						'id_empreendimento' => $id_empreendimento,
						'id_dormitorio'		=> $row
					);
					$this->model->setDormitorios($data,$id_empreendimento);
				endforeach;
				//redirect("empreendimentos/editar/$id_empreendimento");			
			}
			//}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}
	
	function salvarBanheiros($id_empreendimento,$banheiros)
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			//if($this->input->post('id_empreendimento'))
			//{
				//$id_empreendimento = $this->input->post('id_empreendimento');
				$this->model->delBanheiros($id_empreendimento);
			//	$banheiros =  $this->input->post('banheiros');
			if($banheiros){
				foreach ($banheiros as $row):
					$data = array (
						'id_empreendimento' => $id_empreendimento,
						'id_banheiro'		=> $row
					);
					$this->model->setBanheiros($data,$id_empreendimento);
				endforeach;
				//redirect("empreendimentos/editar/$id_empreendimento");			
			}
		//	}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}
	
	function salvarFaixaPreco($id_empreendimento,$faixaPreco)
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			//if($this->input->post('id_empreendimento'))
			//{
			//	$id_empreendimento = $this->input->post('id_empreendimento');
				$this->model->delFaixaPreco($id_empreendimento);
			if($faixaPreco){
				foreach ($faixaPreco as $row):
					$data = array (
						'id_empreendimento' => $id_empreendimento,
						'id_faixa_preco'	=> $row
					);
					$this->model->setFaixaPreco($data,$id_empreendimento);
				endforeach;
			}
				//redirect("empreendimentos/editar/$id_empreendimento");			
				
			//}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}
	
	function salvarSemelhantes($id_empreendimento,$semelhantes)
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
				$this->model->delSemelhantes($id_empreendimento);
			if($semelhantes){
				foreach ($semelhantes as $row):
					$data = array (
						'id_empreendimento' => $id_empreendimento,
						'id_semelhante'		=> $row
					);
					$this->model->setSemelhante($data,$id_empreendimento);
				endforeach;
			}
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}
	
	function salvarMidiaFases()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			if($this->input->post('id_empreendimento'))
			{
				$id_fase = $this->input->post('FaseFoto');
				$id_empreendimento = $this->input->post('id_empreendimento');
				
				$ultimo = $this->model->getMaxPosicaoFotosFases($id_fase,$id_empreendimento);	
				$ultimo++;
				
				$data = array (
					'id_fase' 		=> $id_fase,
					'legenda'		=> $this->input->post('legenda'),
					'posicao'		=> $ultimo
				);
				
				$this->model->setMidiaFases($data);
				
				redirect("empreendimentos/editar/$id_empreendimento#GalFases");			
				
			}	
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}
	
	/*INICIO ORDEM FOTOS FASES DO EMPREENDIMENTO*/
	function sobeFotoFase()
	{
		
		$id_foto = $this->input->post('id_foto_fase');
		$id_fase = $this->input->post('id_fase');
		$id_empreendimento = $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		
		$pos_atual = $this->model->getPosicaoFotosFases($id_foto);
		
		$prox_pos = $pos_atual - 1;
					
		$primeiro = $this->model->getMinPosicaoFotosFases($id_fase,$id_empreendimento);		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoFotosFases($prox_pos,$id_fase,$id_empreendimento);
				//var_dump($retorno);exit;
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual - 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoFotosFases($id_foto,$pos_atual, $prox_pos,$id_fase,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'FasesFotos'  		=> $this->model->getFasesFotos($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.fotosfases.lista.php', $dataVisualizar);
					
		
		//redirect('empreendimentos/lista/');
	}
	
	function desceFotoFase()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		$id_foto = $this->input->post('id_foto_fase');
		$id_fase = $this->input->post('id_fase');
		$id_empreendimento = $this->input->post('id_empreendimento');
		
		$pos_atual = $this->model->getPosicaoFotosFases($id_foto);
		
		$prox_pos = $pos_atual + 1;
		 
		$ultimo = $this->model->getMaxPosicaoFotosFases($id_fase,$id_empreendimento);		
		
			$cond = true;
			while ($cond)
			{
				$retorno = $this->model->getProxPosicaoFotosFases($prox_pos,$id_fase,$id_empreendimento);
				if (!$retorno)
					$retorno = $prox_pos;
				if(count($retorno)==0)
				{
					$prox_pos = $pos_atual + 1; 
				}
				else 
				{
					$cond = false;
				}
			}
			$prox_pos = $retorno;
			
			$this->model->trocaPosicaoFotosFases($id_foto,$pos_atual, $prox_pos,$id_fase,$id_empreendimento);
			
			$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'FasesFotos'  		=> $this->model->getFasesFotos($id_empreendimento)
			);	
					
			$this->load->view('empreendimentos/empreendimentos.fotosfases.lista.php', $dataVisualizar);
				
		
		//redirect('empreendimentos/lista/');
	}	
	/*FIM ORDEM FOTOS FASES DO EMPREENDIMENTO*/
	
	function apagarMidia ()
	{
		$id_midia 			= $this->input->post('id_midia');
		$id_empreendimento	= $this->input->post('id_empreendimento');
		
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$midia = $this->model->getMidia($id_midia);
			if($midia->arquivo)
			{
				if(file_exists('./uploads/imoveis/midias/' . $midia->arquivo)){
					unlink('./uploads/imoveis/midias/' . $midia->arquivo);
				}
				if(file_exists('./uploads/imoveis/midias/thumbs/' . $midia->arquivo)){		
					unlink('./uploads/imoveis/midias/thumbs/' . $midia->arquivo);
				}
			}
			
			$this->model->delMidia($id_midia);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
		
		//redirect("empreendimentos/editar/$id_empreendimento");
		$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'empreendimentosFotos'  => $this->model->getTiposFotos($id_empreendimento),
			);	
					
		$this->load->view('empreendimentos/empreendimentos.midias.lista.php', $dataVisualizar);
	}	
	
	function apagarMidiaFase ()
	{
		
		$id_foto_fase= $this->input->post('id_foto_fase');
		$id_empreendimento= $this->input->post('id_empreendimento');
		
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$fotoFase = $this->model->getFotoFase($id_foto_fase);
			if(@$fotoFase->imagem)
			{
				
				if(file_exists('./uploads/imoveis/fases/' . $fotoFase->imagem)){
					unlink('./uploads/imoveis/fases/' . $fotoFase->imagem);
				}
				if(file_exists('./uploads/imoveis/fases/thumbs/' . $fotoFase->imagem)){
					unlink('./uploads/imoveis/fases/thumbs/' . $fotoFase->imagem);
				}	
			}
			
			$this->model->delFotoFase($id_foto_fase);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
		
		//redirect("empreendimentos/editar/$id_empreendimento");
		
		$dataVisualizar = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'FasesFotos'  		=> $this->model->getFasesFotos($id_empreendimento)
			);	
					
		$this->load->view('empreendimentos/empreendimentos.fotosfases.lista.php', $dataVisualizar);
	}	
	
	
	//INICIO ITENS 
	function editaItensAjax ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_item 			= $this->input->post('id_item');
			$id_empreendimento 	= $this->input->post('id_empreendimento');
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itens'  			=> $this->model->getItem($id_item),
			);
			
			$this->load->view('empreendimentos/empreendimentos.item.edita.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagaItens ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_item 	= $this->input->post('id_item');
			$id_empreendimento 	= $this->input->post('id_empreendimento');
			
			$this->model->delItem($id_item);
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itens'  			=> $this->model->getItens($id_empreendimento),
			);
			
			$this->load->view('empreendimentos/empreendimentos.item.lista.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
	function limpaCamposItens ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_empreendimento = $this->input->post('id_empreendimento');
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itens'  			=> $this->model->getItens($id_empreendimento),
			);
			
			$this->load->view('empreendimentos/empreendimentos.item.limpa.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}	
	//FIM  ITENS
	
	//INICIO FASES
	function editaFasesAjax ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_fase = $this->input->post('id_fase');
			
			$id_empreendimento = $this->input->post('id_empreendimento');

			$data = array(
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'status'			=> $this->model->getStatus(),
				'fases'  			=> $this->model->getFase($id_fase,$id_empreendimento)
			);
			
			$this->load->view('empreendimentos/empreendimentos.fase.edita.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagaFases ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_fase 	= $this->input->post('id_fase');
			$id_empreendimento 	= $this->input->post('id_empreendimento');
			
			$this->model->delFase($id_fase);
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'status'			=> $this->model->getStatus(),
				'fases'  			=> $this->model->getFases($id_empreendimento)
			);
			
			$this->load->view('empreendimentos/empreendimentos.fase.lista.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
	function limpaCamposFases ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_empreendimento = $this->input->post('id_empreendimento');
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'status'			=> $this->model->getStatus(),
				'fases'  			=> $this->model->getFases($id_empreendimento)
			);
			
			$this->load->view('empreendimentos/empreendimentos.fase.limpa.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}	
	//FIM FASES
	
	//ITENS DAS FASES
	function editaItensFasesAjax ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_item = $this->input->post('id_item');
			$id_empreendimento = $this->input->post('id_empreendimento');
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'fases'  			=> $this->model->getFases($id_empreendimento),
				'itensFases' 		=> $this->model->getItemFaseId($id_item)
			);
			
			$this->load->view('empreendimentos/empreendimentos.itemfase.edita.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagaItensFases ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_item 	= $this->input->post('id_item_fase');
			$id_empreendimento 	= $this->input->post('id_empreendimento');
			
			$this->model->delItemFase($id_item);
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'itensFases'  		=> $this->model->getItensFases($id_empreendimento),
			);
			
			$this->load->view('empreendimentos/empreendimentos.itemfase.lista.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	
	function limpaCamposItensFases ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			$id_empreendimento = $this->input->post('id_empreendimento');
			$data21 = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'empreendimento'	=> $this->model->getEmpreendimento($id_empreendimento),
				'fases'  			=> $this->model->getFases($id_empreendimento),
			);
			
			$this->load->view('empreendimentos/empreendimentos.itemfase.limpa.php', $data21);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}	
	//FIM ITENS FASES
	function getCidades($id_estado){

		$this->load->model('empreendimentos_model', 'model');
		$cidades = $this->model->getCidades($id_estado);
		
		foreach ($cidades as $row):
			echo "<option value='".$row->id_cidade."'>".$row->cidade."</option>";
		endforeach;
	}

	function sobeFase(){
		$id_fase 		= $this->input->post('id_fase');
		$id_emp 	= $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		$atual = $this->model->getPosicaoFases($id_fase);
		$atual = $atual->ordem;
		$maior = $this->model->getPosicaoFasesMaior($id_fase);
		if(!empty($maior)){
			$swap = $maior->ordem == $atual ? $atual++ : $atual;
			$this->model->updatePosicaoFases($swap,$id_fase,$maior->ordem,$maior->id);
		} else {
			$swap = $atual + 1;
			$this->model->updatePosicaoFases($swap,$id_fase);
		}
		var_dump($maior,$swap); 
	}
	
	function desceFase(){
		$id_fase 		= $this->input->post('id_fase');
		$id_emp 	= $this->input->post('id_empreendimento');
		$this->load->model('empreendimentos_model', 'model');
		$atual = $this->model->getPosicaoFases($id_fase);
		$maior = $this->model->getPosicaoFasesMaior($id_fase);
		$atual = $atual->ordem;
		if(!empty($maior) && $atual <> 0){
			$swap = $maior->ordem == $atual ? $atual-- : $atual;
			$this->model->updatePosicaoFases($swap,$id_fase,$maior->ordem,$maior->id);
		} else {
			$swap = $atual + -1;
			$this->model->updatePosicaoFases($swap,$id_fase);
		}
		var_dump($maior,$swap);
	}
	
	//gera dados
	
	
	function gera_dados_interesses ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'emps'				=> $this->model->getAllEmpreendimentos()
			);
			
			$this->load->view('empreendimentos/gera_dados_interesses.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function gera_dados_atendimentos ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'emps'				=> $this->model->getAllEmpreendimentos()
			);
			
			$this->load->view('empreendimentos/gera_dados_atendimentos.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function gera_dados_contatos ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'emps'				=> $this->model->getAllEmpreendimentos()
			);
			
			$this->load->view('empreendimentos/gera_dados_contatos.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function gera_dados_indique ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'emps'				=> $this->model->getAllEmpreendimentos()
			);
			
			$this->load->view('empreendimentos/gera_dados_indique.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function gera_dados_corretor ()
	{
		$this->load->model('empreendimentos_model', 'model');
		
		try
		{
			
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'emps'				=> $this->model->getAllEmpreendimentos()
			);
			
			$this->load->view('empreendimentos/gera_dados_corretor.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}		
	
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */