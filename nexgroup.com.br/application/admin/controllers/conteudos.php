<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conteudos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	
	function index ()
	{
		redirect('conteudos/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('conteudos_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/conteudos/lista/',
			'total_rows'	=> $this->model->numConteudos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numConteudos(),
    		'conteudos'   	=> ($this->input->post('keyword')) ? $this->model->buscaConteudos($this->input->post('keyword')) : $this->model->getConteudos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('conteudos/conteudos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('conteudos_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'secoes'		=> $this->model->buscaSecoes()
		);

		$this->load->view('conteudos/conteudos.cadastro.php', $data);
	}
	
	function editar ($id_cms_conteudos = 0)
	{
		$this->load->model('conteudos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'secoes'		=> $this->model->buscaSecoes(),
				'conteudo' 		=> $this->model->getConteudo($id_cms_conteudos)
			);
			
			$this->load->view('conteudos/conteudos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('conteudos_model', 'model');
		
		try
		{			
			$data = array (
				'titulo' 			=> $this->input->post('titulo'),
				'id_cms_secoes' 	=> $this->input->post('id_cms_secoes'),
				'conteudo' 			=> $this->input->post('conteudo'),
				'data_criacao' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setConteudo($data, $this->input->post('id_cms_conteudos'));
				
			if ($this->input->post('id_cms_conteudos'))
				redirect('conteudos/editar/' . $this->input->post('id_cms_conteudos'));
			else
				redirect('conteudos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_cms_conteudos = 0)
	{
		$this->load->model('conteudos_model', 'model');

		try
		{
			$this->model->delConteudo($id_cms_conteudos);
			redirect('conteudos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */