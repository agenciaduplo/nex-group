<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faixas_Precos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	
	function index ()
	{
		redirect('faixas_precos/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('faixas_precos_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/faixas_precos/lista/',
			'total_rows'	=> $this->model->numFaixas_Precos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numFaixas_Precos(),
    		'faixas_precos'   	=> ($this->input->post('keyword')) ? $this->model->buscaFaixas_Precos($this->input->post('keyword')) : $this->model->getFaixas_Precos($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('faixas_precos/faixas_precos.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('faixas_precos_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('faixas_precos/faixas_precos.cadastro.php', $data);
	}
	
	function editar ($id_faixa_preco = 0)
	{
		$this->load->model('faixas_precos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'faixa_preco' 		=> $this->model->getFaixaPreco($id_faixa_preco)
			);
			
			$this->load->view('faixas_precos/faixas_precos.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('faixas_precos_model', 'model');
		
		try
		{			
			$data = array (
				'faixa_preco' 			=> $this->input->post('faixa_preco'),
				'data_cadastro' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setFaixaPreco($data, $this->input->post('id_faixa_preco'));
				
			if ($this->input->post('id_faixa_preco'))
				redirect('faixas_precos/editar/' . $this->input->post('id_faixa_preco'));
			else
				redirect('faixas_precos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_faixa_preco = 0)
	{
		$this->load->model('faixas_precos_model', 'model');

		try
		{
			$this->model->delFaixaPreco($id_faixa_preco);
			redirect('faixas_precos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */