<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function clean($array){
	$return = array();
	$search = array('"',';','\n','\r');
	foreach($array as $i => $x){
		$return[$i] = utf8_decode(trim(strip_tags(str_replace($search,'',$x))));
	}
	return $return;
}



class Expformulario extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->auth->check();
	}
	function index(){
		$this->load->model('relatorios_model', 'model');
		$this->load->helper('download');
		$dados = $this->model->searchFormulariosEXP($_GET['tipo'],$_GET['min'],$_GET['max'],$_GET['emp']);
		$linhas = array();
    $linhas[] = 'Nome; Email; Telefone; Data; Mensagem; Destinatario; Empreendimento; Tipo';
		foreach($dados as $x){
			$a = clean($x);
			$a = '"'.implode('";"',$a).'"';
			array_push($linhas,$a);
		}
		force_download('formulario-'.time().'.csv',implode(PHP_EOL,$linhas));
	}
	
}
?>