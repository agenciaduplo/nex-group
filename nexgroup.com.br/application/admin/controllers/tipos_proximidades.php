<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_Proximidades extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	
	function index ()
	{
		redirect('tipos_proximidades/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('tipos_proximidades_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/tipos_proximidades/lista/',
			'total_rows'	=> $this->model->numTipos_Proximidades(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numTipos_Proximidades(),
    		'tipos_proximidades'   	=> ($this->input->post('keyword')) ? $this->model->buscaTipos_Proximidades($this->input->post('keyword')) : $this->model->getTipos_Proximidades($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('tipos_proximidades/tipos_proximidades.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('tipos_proximidades_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('tipos_proximidades/tipos_proximidades.cadastro.php', $data);
	}
	
	function editar ($id_tipo_proximidade = 0)
	{
		$this->load->model('tipos_proximidades_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'tipo_proximidade' 		=> $this->model->getTipoProximidade($id_tipo_proximidade)
			);
			
			$this->load->view('tipos_proximidades/tipos_proximidades.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('tipos_proximidades_model', 'model');
		
		try
		{			
			$data = array (
				'tipo_proximidade' 		=> $this->input->post('tipo_proximidade'),
				'data_cadastro' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setTipoProximidade($data, $this->input->post('id_tipo_proximidade'));
				
			if ($this->input->post('id_tipo_proximidade'))
				redirect('tipos_proximidades/editar/' . $this->input->post('id_tipo_proximidade'));
			else
				redirect('tipos_proximidades/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_tipo_proximidade = 0)
	{
		$this->load->model('tipos_proximidades_model', 'model');

		try
		{
			$this->model->delTipoProximidade($id_tipo_proximidade);
			redirect('tipos_proximidades/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */