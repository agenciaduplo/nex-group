<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('status/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('status_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/status/lista/',
			'total_rows'	=> $this->model->numStatus(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numStatus(),
    		'status'   			=> ($this->input->post('keyword')) ? $this->model->buscaStatus($this->input->post('keyword')) : $this->model->getStatus($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('status/status.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('status_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('status/status.cadastro.php', $data);
	}
	
	function editar ($id_status = 0)
	{
		$this->load->model('status_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'status' 			=> $this->model->getStatusId($id_status)
			);
			
			$this->load->view('status/status.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('status_model', 'model');
		
		try
		{			
			$data = array (
				'status' 				=> $this->input->post('status'),
				'data_cadastro' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setStatus($data, $this->input->post('id_status'));
				
			if ($this->input->post('id_status'))
				redirect('status/editar/' . $this->input->post('id_status'));
			else
				redirect('status/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_status = 0)
	{
		$this->load->model('status_model', 'model');

		try
		{
			$this->model->delStatus($id_status);
			redirect('status/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */