<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas_tipo extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
		$this->load->model('ofertas_tipo_model', 'model');
	}
	
	function index()
	{
		redirect('ofertas_tipo/lista');
	}
	
	function lista($offset = "")
	{	
		$total = $this->model->numTipos();

		$config = array (
			'base_url'		=> base_url().'admin.php/ofertas_tipo/lista/',
			'total_rows'	=> $total,
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $total,
    		'ofertas_tipo'  => ($this->input->post('keyword')) ? $this->model->buscaTipos($this->input->post('keyword')) : $this->model->getTipos($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('ofertas_tipo/ofertas_tipo.php', $data);
	}
	
	function cadastro()
	{	
		$data = array (
			'permissoes'	=> $this->model->getPermissoes()
		);

		$this->load->view('ofertas_tipo/ofertas_tipo.cadastro.php', $data);
	}
	
	function editar($id_ofertas_tipo = 0)
	{
		try
		{
			$data = array (
				'permissoes'	=> $this->model->getPermissoes(),
				'ofertas_tipo'	=> $this->model->getTipo($id_ofertas_tipo)
			);
			
			$this->load->view('ofertas_tipo/ofertas_tipo.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar()
	{
		try
		{			
			if($this->input->post('tipo') == '')
			{
				throw new Exception('Insira o tipo corretamente!');
			}

			$data = array(
				'tipo'	=> $this->input->post('tipo')
			);
			
			$insert_id = $this->model->setTipo($data, $this->input->post('id_ofertas_tipo'));
				
			if ($this->input->post('id_ofertas_tipo'))
				redirect('ofertas_tipo/editar/' . $this->input->post('id_ofertas_tipo'));
			else
				redirect('ofertas_tipo/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar($id_ofertas_tipo = 0)
	{
		try
		{
			$this->model->delTipo($id_ofertas_tipo);
			redirect('ofertas_tipo/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file ofertas_tipo.php */
/* Location: ./system/application/controllers/ofertas_tipo.php */