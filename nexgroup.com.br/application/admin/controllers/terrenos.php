<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terrenos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		header('Location: terrenos/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('terrenos_model', 'model');
		$this->load->helper('text');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/terrenos/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numTerrenosBusca($this->input->post('keyword')) : $this->model->numTerrenos(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados'  	=> $this->model->numTerrenos(),
			'terrenos'     	=> ($this->input->post('keyword')) ? $this->model->buscaTerrenos($this->input->post('keyword')) : $this->model->getTerrenos($offset),
			'paginacao'	   	=> $this->pagination->create_links()
		);
		
		$this->load->view('terrenos/terrenos.php', $data);
	}
	
	function visualizar ($id_terreno = 0)
	{
		$this->load->model('terrenos_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'terreno' 		=> $this->model->getTerrenoVisualizar($id_terreno)
			);
			
			$this->load->view('terrenos/terreno.visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_terreno = 0)
	{
		$this->load->model('terrenos_model', 'model');

		try
		{
			$arquivo = $this->model->getTerrenoVisualizar($id_terreno);
									
			if($arquivo->imagem):
				unlink('./uploads/terrenos/' . $arquivo->imagem);
			endif;
			
			$this->model->delTerreno($id_terreno);
			redirect('terrenos/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	function download ($arquivo)
	{
		$this->load->helper('download');
		$data = file_get_contents("./uploads/terrenos/".$arquivo);
		force_download($arquivo, $data);
	}
}

/* End of file contato.php */
/* Location: ./system/application/controllers/falecom.php */