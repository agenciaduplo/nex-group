<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	function index ()
	{
		redirect('tags/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('tags_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/tags/lista/',
			'total_rows'	=> $this->model->numTags(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'cadastrados' 	=> $this->model->numTags(),
    		'tags'   	  	=> ($this->input->post('keyword')) ? $this->model->buscaTags($this->input->post('keyword')) : $this->model->getTags($offset),
			'paginacao'	  	=> $this->pagination->create_links()
		);
		
		$this->load->view('tags/tags.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('tags_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'secoes'		=> $this->model->getSecoes()
		);

		$this->load->view('tags/tags.cadastro.php', $data);
	}
	
	function editar ($id_tag = 0)
	{
		$this->load->model('tags_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  	=> $this->model->getPermissoes(),
				'tag' 			=> $this->model->getTag($id_tag),
				'secoes'		=> $this->model->getSecoes()
			);
			
			$this->load->view('tags/tags.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('tags_model', 'model');
		
		try
		{			
			$data = array (
				'id_cms_secoes' 	=> $this->input->post('secao'),
				'title' 			=> $this->input->post('title'),
				'keywords' 			=> $this->input->post('keywords'),
				'description' 		=> $this->input->post('description'),
				'id_empresa'		=> $this->input->post('empresa')
			);
			
			$insert_id = $this->model->setTag($data, $this->input->post('id_tag'));
				
			if ($this->input->post('id_tag'))
				redirect('tags/editar/' . $this->input->post('id_tag'));
			else
				redirect('tags/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_tag = 0)
	{
		$this->load->model('tags_model', 'model');

		try
		{
			$this->model->delTag($id_tag);
			redirect('tags/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */
