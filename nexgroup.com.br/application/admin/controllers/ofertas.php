<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
		$this->load->model('ofertas_model', 'model');
	}
    	
	function index()
	{
		redirect('ofertas/lista/');
	}
	
	function lista($offset = "")
	{
		$config = array (
			'base_url'		=> base_url().'admin.php/ofertas/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numOfertasBusca($this->input->post('keyword')) : $this->model->numOfertas(),
			'per_page'		=> '30'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numOfertas(),
    		'ofertas'  			=> ($this->input->post('keyword')) ? $this->model->buscaOfertas($this->input->post('keyword')) : $this->model->getOfertas($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('ofertas/ofertas.php', $data);
	}
	
	function cadastro()
	{
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes(),
			'ofetas_tipos'	=> $this->model->getOfertasTipos(),
			'estados'		=> $this->model->getEstados()
		);

		$this->load->view('ofertas/ofertas.cadastro.php', $data);
	}
	
	function editar($id_oferta = 0)
	{
		try
		{
			$oferta = $this->model->getOfertaId($id_oferta);

			$data = array(
				'permissoes'  		=> $this->model->getPermissoes(),
				'ofetas_tipos'		=> $this->model->getOfertasTipos(),
				'oferta' 			=> $oferta,
				'estados'			=> $this->model->getEstados(),
				'cidades' 			=> $this->model->getCidades($oferta->id_estado)
			);

			// var_dump($data['cidades']);
			// exit;
			
			$this->load->view('ofertas/ofertas.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		try
		{		
			$msg_erro = array();
			$erro = 0;

			$titulo = $this->input->post('titulo');
			$tag_title = $this->input->post('tag_title');

			if($titulo == '')
			{ 
				$msg_erro[] = 'Insira o título da oferta!';
				$erro++; 
			}

			if($tag_title == '')
			{ 
				$msg_erro[] = 'Insira o título de página da oferta!';
				$erro++;
			}

    		
    		
			$data = array (
				'id_ofertas_tipo' 	=> $this->input->post('id_tipo'),
				'id_cidade' 		=> $this->input->post('cidade'),
				'titulo' 			=> $this->input->post('titulo'),
				'descricao' 		=> $this->input->post('descricao'),
				'valor_frase' 		=> $this->input->post('valor_frase'),
				'comodos' 			=> $this->input->post('comodos'),
				'metragem' 			=> $this->input->post('metragem'),
				'vagas' 			=> $this->input->post('vagas'),
				'endereco' 			=> $this->input->post('endereco'),
				'latitude' 			=> $this->input->post('latitude'),
				'longitude' 		=> $this->input->post('longitude'),
				'tag_title' 		=> $this->input->post('tag_title'),
				'tag_description' 	=> $this->input->post('tag_description'),
				'tag_keywords' 		=> $this->input->post('tag_keywords'),
				'ativo' 			=> $this->input->post('ativo')
			);


			$tipo_valor = $this->input->post('valor_tipo');

			if($tipo_valor == 'uni')
			{
				$valor = $this->input->post('valor');
				$valor = str_replace('.', '', $valor);
				$valor = str_replace(',', '.', $valor);

				$data['valor_de'] = null;
				$data['valor'] 	  = $valor;

				$data['valor_tipo'] = 'uni';

				if( ! intval($data['valor']) > 0)
				{
					$msg_erro[] = 'Informe um valor válido!';
					$erro++;
				}
			}
			else if($tipo_valor == 'de_por')
			{
				$valor_de = $this->input->post('valor_de');
				$valor_de = str_replace('.', '', $valor_de);
				$valor_de = str_replace(',', '.', $valor_de);

				$data['valor_de'] = $valor_de;

				if( ! intval($data['valor_de']) > 0)
				{
					$msg_erro[] = 'Informe o valor inicial(De) válido!';
					$erro++;
				}

				$valor_por = $this->input->post('valor_por');
				$valor_por = str_replace('.', '', $valor_por);
				$valor_por = str_replace(',', '.', $valor_por);

				$data['valor'] = $valor_por;

				$data['valor_tipo'] = 'de_por';

				if( ! intval($data['valor']) > 0)
				{
					$msg_erro[] = 'Informe o valor atual(Por) válido!';
					$erro++;
				}

				if((float) $data['valor'] >= (float) $data['valor_de'])
				{
					$msg_erro[] = 'O valor atual(Por) não pode ser maior ou igual ao valor inicial(De)!';
					$erro++;
				}
			}
			else
			{
				$msg_erro[] = 'Tipologia do valor indefinida!';
				$erro++;
			}
			

			if($erro > 0)
    			throw new Exception(implode('<br/>', $msg_erro));


			if(intval($this->input->post('id_oferta')) == 0)
			{	
				$data['data_cadastro'] = date('Y-m-d H:i:s');	
			}


			$this->load->library('utilidades');

			$url = $this->utilidades->remove_accents($data['titulo']);

			$data['url'] = strtolower(url_title($url));


			// var_dump($data);
			// exit;
			$insert_id = $this->model->setOferta($data, $this->input->post('id_oferta'));
			

			if ($insert_id > 0)
			{
				redirect('ofertas/editar/' . $insert_id);
			}
			else
			{
				redirect('ofertas/lista/');
			}
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar($id_oferta = 0)
	{
		try
		{			
			$imagens = $this->model->getImagens($id_oferta);

			if($imagens)
			{
				foreach ($imagens as $imagem)
				{						
					if($imagem->img)
					{
						if(@file_exists('./uploads/ofertas/galeria/' . $imagem->img))
						{
							@unlink('./uploads/ofertas/galeria/' . $imagem->img);
						}

						if(@file_exists('./uploads/ofertas/galeriaThumb/' . $imagem->img))
						{
							@unlink('./uploads/ofertas/galeriaThumb/' . $imagem->img);
						}
					}
				}

				$this->model->delImagem('',$id_oferta);
			}

			$this->model->delOferta($id_oferta);
			
			redirect('ofertas/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	//FIM NOTICIAS CONTROLLER
	
	function imagens($id_oferta)
	{
		try
		{
			$data = array (
				'id_oferta'		=> $id_oferta,
				'permissoes'  	=> $this->model->getPermissoes(),
				'imagens' 		=> $this->model->getImagens($id_oferta)
			);
			
			$this->load->view('ofertas/ofertas.imagens.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}

	function setImagem()
	{
		try
		{
			if($this->input->post('id_oferta'))
			{
				$data = array (
					'id_oferta' => $this->input->post('id_oferta'),
					'legenda'	=> $this->input->post('legenda')
				);
			
				$this->model->setImagem($data);					
							
				$id_oferta = $this->input->post('id_oferta');
				
				redirect("ofertas/imagens/$id_oferta");
			}
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}		
	}	
	
	function delImagem($id_galeria, $id_oferta)
	{
		try
		{
			$imagem = $this->model->getImagemGaleria($id_galeria);

			if($imagem->img)
			{
				@unlink('./uploads/ofertas/galeria/' . $imagem->img);		
				@unlink('./uploads/ofertas/galeriaThumb/' . $imagem->img);
			}
			
			$this->model->delImagem($id_galeria);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
		
		redirect("ofertas/imagens/$id_oferta");
	}	
	
}

/* End of file ofertas.php */
/* Location: ./system/application/controllers/ofertas.php */