<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proximidades extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
    	
	function index ()
	{
		redirect('proximidades/lista/');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('proximidades_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/proximidades/lista/',
			'total_rows'	=> ($this->input->post('keyword')) ? $this->model->numProximidadesBusca($this->input->post('keyword')) : $this->model->numProximidades(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numProximidades(),
    		'proximidades'  	=> ($this->input->post('keyword')) ? $this->model->buscaProximidades($this->input->post('keyword')) : $this->model->getProximidades($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('proximidades/proximidades.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('proximidades_model', 'model');
		$this->load->model('tipos_proximidades_model', 'tipos_proximidades');
		
		$data = array (
			'tipos'			=> $this->tipos_proximidades->getTipos_Proximidades(),
			'permissoes'  	=> $this->model->getPermissoes()
		);
		
		$this->load->view('proximidades/proximidades.cadastro.php', $data);
	}
	
	function editar ($id_proximidade = 0)
	{
		$this->load->model('proximidades_model', 'model', TRUE);
		$this->load->model('tipos_proximidades_model', 'tipos_proximidades');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'tipos'				=> $this->tipos_proximidades->getTipos_Proximidades(),
				'proximidade' 		=> $this->model->getProximidade($id_proximidade)
			);
			
			$this->load->view('proximidades/proximidades.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('proximidades_model', 'model');
		
		try
		{	
			$date = implode("-",array_reverse(explode("/",$this->input->post('data_cadastro'))));	
			
			$data = array (
				'id_tipo_proximidade'	=> $this->input->post('id_tipo_proximidade'),
				'nome' 				=> $this->input->post('nome'),
				'latitude' 			=> $this->input->post('latitude'),
				'longitude' 		=> $this->input->post('longitude'),
				'data_cadastro' 	=> $date
			);

			$insert_id = $this->model->setProximidade($data, $this->input->post('id_proximidade'));
			
			if ($this->input->post('id_proximidade'))
				redirect('proximidades/editar/' . $this->input->post('id_proximidade'));
			else
				redirect('proximidades/lista/');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_proximidade = 0)
	{
		$this->load->model('proximidades_model', 'model');

		try
		{
						
			$this->model->delProximidade($id_proximidade);
			
			redirect('proximidades/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */