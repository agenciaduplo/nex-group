<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_Imoveis extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->auth->check();
	}
	
	
	function index ()
	{
		redirect('tipos_imoveis/lista');
	}
	
	function lista ($offset = "")
	{
		$this->load->model('tipos_imoveis_model', 'model');
		
		$config = array (
			'base_url'		=> base_url().'admin.php/tipos_imoveis/lista/',
			'total_rows'	=> $this->model->numTipos_Imoveis(),
			'per_page'		=> '20'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'permissoes'  		=> $this->model->getPermissoes(),
			'cadastrados' 		=> $this->model->numTipos_Imoveis(),
    		'tipos_imoveis'   	=> ($this->input->post('keyword')) ? $this->model->buscaTipos_Imoveis($this->input->post('keyword')) : $this->model->getTipos_Imoveis($offset),
			'paginacao'	  		=> $this->pagination->create_links()
		);
		
		$this->load->view('tipos_imoveis/tipos_imoveis.php', $data);
	}
	
	function cadastro()
	{
		$this->load->model('tipos_imoveis_model', 'model');
		
		$data = array (
			'permissoes'  	=> $this->model->getPermissoes()
		);

		$this->load->view('tipos_imoveis/tipos_imoveis.cadastro.php', $data);
	}
	
	function editar ($id_tipo_imovel = 0)
	{
		$this->load->model('tipos_imoveis_model', 'model');
		
		try
		{
			$data = array (
				'permissoes'  		=> $this->model->getPermissoes(),
				'tipo_imovel' 		=> $this->model->getTipoImovel($id_tipo_imovel)
			);
			
			$this->load->view('tipos_imoveis/tipos_imoveis.cadastro.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function salvar ()
	{
		$this->load->model('tipos_imoveis_model', 'model');
		
		try
		{			
			$data = array (
				'tipo_imovel' 			=> $this->input->post('tipo_imovel'),
				'data_cadastro' 		=> date('Y-m-d H:i:s')
			);
			
			$insert_id = $this->model->setTipoImovel($data, $this->input->post('id_tipo_imovel'));
				
			if ($this->input->post('id_tipo_imovel'))
				redirect('tipos_imoveis/editar/' . $this->input->post('id_tipo_imovel'));
			else
				redirect('tipos_imoveis/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_tipo_imovel = 0)
	{
		$this->load->model('tipos_imoveis_model', 'model');

		try
		{
			$this->model->delTipoImovel($id_tipo_imovel);
			redirect('tipos_imoveis/lista');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
}

/* End of file contatos.php */
/* Location: ./system/application/controllers/contatos.php */