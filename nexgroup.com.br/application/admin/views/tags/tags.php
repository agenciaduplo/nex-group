<?=$this->load->view('includes/topo');?>

	<!-- INICIO CONTEUDO -->

<?php
	$empresas = array(
		1 => 'Capa',
		2 => 'EGL',
		3 => 'DHZ',
		4 => 'Lomando Aita',
		6 => 'Nex Group'
	);
?>


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TAGS . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            
            	<div class="botoes"><a href="<?=site_url()?>/tags/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" /></a></div>
            
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <?php if ($tags->num_rows):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="33%">Seção</th>
                        <th width="42%">Título</th>
                        <th width="10%">Empresa</th>
                        <th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($tags->result() as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="33%"><a href="<?=site_url()?>/tags/editar/<?=$row->id_tag?>" title="Editar" class="bt-editar"><?=$row->nome?></a></td>
                        <td width="62%"><a href="<?=site_url()?>/tags/editar/<?=$row->id_tag?>" title="Editar" class="bt-editar"><?=$row->title?></a></td>
                        <td nowrap="nowrap">
                        	<?=$empresas[$row->id_empresa]; ?>
                        </td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/tags/editar/<?=$row->id_tag?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <!-- 	<a href="<?=site_url()?>/tags/apagar/<?=$row->id_tag?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->title?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a> -->
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            	<span>Nenhuma tag encontrada.</span>
            <?php endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>