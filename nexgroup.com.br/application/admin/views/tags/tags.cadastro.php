<?=$this->load->view('includes/topo');?>

<?php
$empresas = array(
	1 => 'Capa',
	2 => 'EGL',
	3 => 'DHZ',
	4 => 'Lomando Aita',
	6 => 'Nex Group'
);
?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
	<script type="text/javascript" charset="utf-8">
	  $(function() {
	      
			$('#FormTitle').jqEasyCounter({
				'maxChars': 1000,
				'maxCharsWarning': 90,
				'msgFontSize': '11px',
				'msgFontColor': '#000',
				'msgFontFamily': 'Arial',
				'msgTextAlign': 'right',
				'msgWarningColor': '#F00',
				'msgAppendMethod': 'insertAfter'              
			});
			
			$('#FormKeywords').jqEasyCounter({
				'maxChars': 1000,
				'maxCharsWarning': 200,
				'msgFontSize': '11px',
				'msgFontColor': '#000',
				'msgFontFamily': 'Arial',
				'msgTextAlign': 'right',
				'msgWarningColor': '#F00',
				'msgAppendMethod': 'insertAfter'              
			});
			
			$('#FormDescription').jqEasyCounter({
				'maxChars': 1000,
				'maxCharsWarning': 250,
				'msgFontSize': '11px',
				'msgFontColor': '#000',
				'msgFontFamily': 'Arial',
				'msgTextAlign': 'right',
				'msgWarningColor': '#F00',
				'msgAppendMethod': 'insertAfter'              
			});	     
	                
	  });
	</script>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TAGS . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormTags" action="<?=site_url()?>/tags/salvar" method="post">
 					<? if (@$tag->id_tag): ?>
                        <input type="hidden" name="id_tag" value="<?=@$tag->id_tag?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Seção</label>
                        <div>
                        	<select name="secao">
                        		<option>----</option>
                        		<?php 
                        			foreach($secoes as $row):
                        		?>
                        			<option <?php if(@$row->id_cms_secoes == @$tag->id_cms_secoes) echo 'selected="selected"'?> value="<?=$row->id_cms_secoes?>" ><?=$row->nome?></option>	                        			
                        		<?php 
                        			endforeach;
                        		?>
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Title - recomendado 90 caracteres</label>
                        <div><textarea name="title" class="campo-padrao" id="FormTitle"><?=@$tag->title?></textarea></div>                       
                        
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Keywords - recomendado 200 caracteres</label>
                        <div><textarea name="keywords" class="campo-padrao" id="FormKeywords"><?=@$tag->keywords?></textarea></div>   
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Description - recomendado 250 caracteres</label>
                        <div><textarea name="description" class="campo-padrao" id="FormDescription"><?=@$tag->description?></textarea></div>                          
                        
                    </div>
                    <br/><br/>
                    <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Empresa</label>
                        <div>
                        	<select name="empresa">
                        	<?php
                        	foreach($empresas as $eid => $e){
                        		if($tag->id_empresa == $eid){
                        			echo '<option selected="selected" value="',$eid,'">',$e,'</option>';
                        		} else {
                        			echo '<option selected="selected" value="',$eid,'">',$e,'</option>';
                        		}
                        	}
                        	?>
                        	</select>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado"><label>&nbsp;</label><div&nbsp;></div></div>
                    <br/>
                    <br class="clr" /><br/>
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/tags/lista" title="Voltar" class="bt-voltar">voltar para Tags</a><br/><br/>

	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>