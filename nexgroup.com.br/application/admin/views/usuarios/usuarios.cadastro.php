<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=CLIENTES . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormUsuariosCad" action="<?=site_url()?>/usuarios/salvar/" method="post">
 					<? if (@$usuario->id_usuario): ?>
                        <input type="hidden" name="id_usuario" value="<?=@$usuario->id_usuario?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Nome</label>
                        <div><input type="text" name="nome" class="campo-padrao" id="FormNome" value="<?=@$usuario->nome?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">E-mail</label>
	                    <div>
	                    	<input type="text" name="email" class="campo-padrao" id="FormEmail" value="<?=@$usuario->email?>" />
		                    <br class="clr" />
	                    </div>                        
                        
                    </div>
                    <br class="clr" />

                    <div class="formulario-colunado">
                        <label class="obrigatorio">Login</label>
                        <div>
                        	<input type="text" name="login" class="campo-padrao" id="FormLogin" value="<?=@$usuario->login?>" />
                        <br class="clr" />
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                    	<label>Senha</label>
                     	<? if (@$usuario->id_usuario): ?>
                    	<div><input type="password" name="senha" class="campo-padrao" id="FormSenha" value="" /></div>
                    	<? else: ?>
                    	<div><input type="password" name="senha" class="campo-padrao" id="FormSenha" value="<?=@$usuario->senha?>" /></div>                    	
                    	<? endif; ?>                    	
                    </div>
                    <br class="clr" />
                    
                    <br/><br/>
                    <br class="clr" />
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/usuarios/lista" title="Voltar" class="bt-voltar">voltar para Usuários</a>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>