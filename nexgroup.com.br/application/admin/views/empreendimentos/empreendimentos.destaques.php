<?=$this->load->view('includes/topo');?>

	<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . LISTA DESTAQUES" class="FlashTitulo"></a></div>
		<div class="cadastros"></div>
		<br class="clr" />

			<div class="base-mg">
			
            <div id="rsvErrorsItens"></div>
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaItens')?><br/></span>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="ItensAjax">
                	<h2>Destaques</h2>
                	<p>Somente os imóveis marcados com o checkbox "Mostra destaque na home" aparecem nesta lista de destaques.</p>
                </div>
                 <? if (@$empreendimentos){?>
			            <table id="listaDestaques" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                    	<th width="2%">Ordem</th>
			                        <th width="35%">Título</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($empreendimentos as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                    	<td  nowrap="nowrap">
                    					<a href="javascript: desceDestaque('<?=@$row->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeDestaque('<?=@$row->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    				</td>
			                        <td width="30%"><?=$row->empreendimento?></td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            </table>
			            <br/>
		            <? }else{ ?>
		            	<span style="margin-left:20px;">Nenhum Item encontrado.<br/></span>
		            <? } ?><br/>
           	 </div>
			
            </div>

            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>