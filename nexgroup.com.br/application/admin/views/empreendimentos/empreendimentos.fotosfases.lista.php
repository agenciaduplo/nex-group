<div id="ListaFotosFases" >
                <? if(@$FasesFotos) { ?>
                <? foreach ($FasesFotos as $row): ?> 
                 	<label class="obrigatorio">LISTA <?php echo $row->fase?> </label>   
                	<table class="tabela-padrao" style="width:515px; margin-left:18px;">
		                <thead>
		                    <tr>
		                    	<th width="2%">Ordem</th>
		                        <th width="20%">Foto</th>
		                        <th width="35%">Legenda</th>
		                        <th width="5%">Ações</th>
		                    </tr>
		                </thead>
                		<?php $this->load->model('empreendimentos_model', 'model', TRUE); 
               				  $fotos = $this->model->getFotosFases($row->id_fase);
               			?>
                		<tbody > 
		                	<? $count = 1; ?>
		                    <? foreach ($fotos as $row2): ?>      
		                    <? $count++; ?>
		                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
		                      <td  nowrap="nowrap">
                    				<a href="javascript: desceFotoFase('<?=$row2->id_foto_fase?>','<?=@$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    				&nbsp;
                    				<a href="javascript: sobeFotoFase('<?=$row2->id_foto_fase?>','<?=@$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    		  </td>
		                      <td width="20%"><a href="<?=base_url()?>uploads/imoveis/fases/<?=$row2->imagem?>" rel="shadowbox[teste]" class="border"><img width="50px" border="0" src="<?=base_url()?>uploads/imoveis/fases/thumbs/<?=$row2->imagem?>"></a></td>
		                      <td width="30%"><?=$row2->legenda?><br/></td>
		                      <td nowrap="nowrap">
		                      	<a href="javascript: getLegendaFotoFase('<?=$row2->id_foto_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                    <a href="javascript: delFotoFase('<?=$row2->id_foto_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                     </td>
		                    </tr>
		                    <? endforeach; ?>
		                </tbody>
	            	</table>
	            <? endforeach; ?>
	            <? }else{ ?>
	            	<span style="margin-left:20px;">Nenhuma Foto das Fases encontrada.<br/></span>
	            <? } ?>
	            </div>