<h2>Itens Fases</h2>
                	<br/><br/>
					<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarItensFases/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=@$empreendimento->id_empreendimento?>">
						<? if(@$itensFases->id_item_fase): ?>
						<input type="hidden" name="id_item_fase" value="<?=$itens->id_item_fase?>" id="FormItensFaseId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItemFase" value="<?=@$itensFases->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">
		                	<label class="obrigatorio">Fase do Item</label>
	                         <div>
		                         <select name="ItemFase" class="campo-padrao" id="FormItemFase">
		                         	<option value=""> -- Escolha a Fase -- </option>
		                         	<?php if (@$fases):?>
		                        		<?php foreach($fases as $row):?>
											<option value="<?php echo $row->id_fase?>"><?php echo $row->titulo ?></option>
										<?php endforeach;?>
									<?php endif;?>
	                        	</select>
	                         </div>                
		               </div>
		               <br class="clr" />		
		               <div class="formulario-colunado">           
		                    <label class="obrigatorio">Porcentagem</label>
                        	<div><input type="text" name="porcentagem" class="campo-padrao" id="FormPerItem" value="<?=@$itensFases->porcentagem?>" /></div>                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>	