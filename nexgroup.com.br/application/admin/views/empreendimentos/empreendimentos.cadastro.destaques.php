<?=$this->load->view('includes/topo');?>

	<script type="text/javascript" charset="utf-8">
	$(function() {
	    
	    $("input#FormImagem").filestyle({ 
	        image: "<?=base_url()?>assets/admin/img/file.gif",
	        imageheight : 16,
	        imagewidth : 47,
	        width : 150
	    }); 
	});
	</script>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . CADASTRO DESTAQUES" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>
            <span class="txt-destaque"><?=$this->session->flashdata('respostaItens')?><br/></span>
           
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<div class="formulario-mg" id="ItensAjax">
                	<h2>Nova Imagem</h2>
                	<br/>
		               
 						<form id="FormImagensDestaques" action="<?=site_url()?>/empreendimentos_destaques/salvar/" method="post" enctype="multipart/form-data">
 							<input type="hidden" name="id_destaque_empreendimento" value="<?=@$destaque->id_destaque_empreendimento?>" />
 							<div class="formulario-colunado">
	 							<label class="obrigatorio">Imagem</label>
	                        	<div>
	                        		<input type="file" name="imagem" class="campo-padrao" id="FormImagem" />
	                        	</div>
	                        	<? if(@$destaque->imagem): ?> <br />
										<img src="<?=base_url()?>uploads/imoveis/destaques/<?=$destaque->imagem?>"	width="100px;"> <br />
										<br />
								<? endif; ?>
                       	 	</div>
                       	 	 <div class="formulario-colunado-div">&nbsp;</div>
                   			<div class="formulario-colunado">
	                        	<label class="obrigatorio">Empreendimento</label>
		                        <div>
		                        	<select name="empreendimento" class="campo-padrao" id="FormEmpreedimento">
		                        		<option value="" selected="selected"> -- Escolha -- </option>
		                        		<?php foreach($empreendimentos as $row):?>
												<option value="<?php echo $row->id_empreendimento?>" <?php if($row->id_empreendimento==@$destaque->id_empreendimento){echo 'selected="selected"';}?>><?=@$row->empreendimento?> - <?=@$row->id_empreendimento?></option>
											<?php endforeach;?>
		                        	</select>
		                        </div>
	                        </div>
	                        <br class="clr" />
                        	<label class="obrigatorio">Legenda</label>
                        	<div>
                        		<input type="text" name="legenda" value="<?php echo @$destaque->legenda ?>" class="campo-padrao" id="FormLegenda" />
                        	</div>
                        	
                        	<div class="a-right" style="margin-right:10px;">	
                        		<input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
                        	</div>
                        	<br class="clr" />
                        	<br/>		               
		                </form>
		            
                </div>              
				<br class="clr" />	
                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/empreendimentos_destaques/lista" title="Voltar" class="bt-voltar">Voltar para Empreendimentos Destaques</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>