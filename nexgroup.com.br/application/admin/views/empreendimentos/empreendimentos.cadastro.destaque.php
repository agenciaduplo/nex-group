<?=$this->load->view('includes/topo');?>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/empreendimentos.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
	   	
	   	$("input#FormLogo").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
	   	
		$("input#FormFachada").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

	   	$("input#FormFotooo").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		
		$("input#FormBanner").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		}); 
		
		$("input#FormMemorial").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		
		      
		$('#FormTitle').jqEasyCounter({
			'maxChars': 90,
			'maxCharsWarning': 80,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormKeywords').jqEasyCounter({
			'maxChars': 200,
			'maxCharsWarning': 180,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormDescription').jqEasyCounter({
			'maxChars': 250,
			'maxCharsWarning': 230,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		}); 
		
		$("#FormDescricao").cleditor(); 
		$("#FormApresentacao").cleditor();   

		$("#data_atualizacao").datepicker({
	    	yearRange: '-100:+3',
	        showOn: "button",
	        buttonImage: "http://www.nexgroup.com.br/site2011/assets/admin/img/ico-calendario.gif",
	        buttonImageOnly: true
	    });  
             
  });

</script>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . CADASTRO" class="FlashTitulo"></a></div>
			
            <div id="rsvErrors"></div>
            
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaDormitorios')?><br/></span>
            <? if (@$empreendimento->id_empreendimento): ?>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="DormitoriosAjax">
                	<h2>Dormitórios</h2>
                	<br/><br/>
					<form id="FormDormitorios" action="<?=site_url()?>/empreendimentos/salvarDormitorios/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<ul>
						<?php $dorm_atrib =array();
							foreach ($dormitorios_atribuidos as $row):
								$dorm_atrib[] = $row->id_dormitorio;
							endforeach;
							?>
            			<?php foreach (@$dormitorios as $row): ?> 
							<li class="fli">
								<input type="checkbox"	id="dormitorios[<?=$row->id_dormitorio?>]" name="dormitorios[<?=$row->id_dormitorio?>]"	class="Checkbox" value="<?=$row->id_dormitorio?>"
								 <?php	if(in_array($row->id_dormitorio, $dorm_atrib)){	echo 'checked="checked"';}?>
								/>
								<label class="labelr"><?=$row->dormitorio?></label>
							</li>
						<?php endforeach; ?>
						</ul>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
			</div>
			<?php endif;?>
            
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaBanheiros')?><br/></span>
            <? if (@$empreendimento->id_empreendimento): ?>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="BanheirosAjax">
                	<h2>Banheiros</h2>
                	<br/><br/>
					<form id="FormBanheiros" action="<?=site_url()?>/empreendimentos/salvarBanheiros/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<ul>
						<?php $ban_atrib =array();
							foreach ($banheiros_atribuidos as $row):
								$ban_atrib[] = $row->id_banheiro;
							endforeach;
							?>
            			<?php foreach (@$banheiros as $row): ?> 
							<li class="fli">
								<input type="checkbox"	id="banheiros[<?=$row->id_banheiro?>]" name="banheiros[<?=$row->id_banheiro?>]"	class="Checkbox" value="<?=$row->id_banheiro?>"
								 <?php	if(in_array($row->id_banheiro, $ban_atrib)){	echo 'checked="checked"';}?>
								/>
								<label class="labelr"><?=$row->banheiro?></label>
							</li>
						<?php endforeach; ?>
						</ul>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
			</div>
			<?php endif;?>
            
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaItens')?><br/></span>
            <? if (@$empreendimento->id_empreendimento): ?>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="ItensAjax">
                	<h2>Itens</h2>
                	<br/><br/>
					<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarItens/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$itens->id_item): ?>
						<input type="hidden" name="id_item" value="<?=$itens->id_item?>" id="FormItensId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItem" value="<?=@$itens->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Destaque</label>
                        	<div><input id="destaqueSim" type="radio" name="destaque" class="formulario-radio" value="S" /><label class="obrigatorio">Sim</label><input type="radio" id="destaqueNao" name="destaque" class="formulario-radio" value="N" /><label class="obrigatorio">Não</label></div>                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
                 <? if (@$itens):?>
			            <table id="listaItens" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                        <th width="35%">Título</th>
			                        <th width="30%">Destaque</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($itens as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                        <td width="30%"><?=$row->item?></td>
			                        <td width="30%"><?php if($row->destaque == 'S'){echo 'Sim';} else{ echo 'Não';}?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                        	<a href="javascript: delItem('<?=$row->id_item?>', '<?=@$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=@$row->item;?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            </table>
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhum Item encontrado.<br/></span>
		            <? endif; ?><br/>
           	 </div>
            <? endif; ?>
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaFases')?><br/></span>
            
            <? if (@$empreendimento->id_empreendimento): ?>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="FasesAjax">
                	<h2>Fases</h2>
                	<br/><br/>
					<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarFases/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormVendaId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$fases->id_fase): ?>
							<input type="hidden" name="id_fase" value="<?=$fases->id_fase?>" id="FormFaseId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItem" value="<?=@$fases->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">
                        <label class="obrigatorio">Status</label>
                        <div>
                        	<select name="status" class="campo-padrao" id="FormStatus">
                        		<option value="" selected="selected"> -- Escolha -- </option>
                        		<?php foreach($status as $row):?>
										<option value="<?php echo $row->id_status?>" <?php if($row->id_status==@$empreendimento->id_status)echo 'selected="selected"'?>><?php echo $row->status ?></option>
									<?php endforeach;?>
                        	</select>
                        </div>
                    </div>
		                <br class="clr" />
		                <div class="formulario-colunado">
							<label class="obrigatorio">Data Atualização</label>
							<div class="formulario-colunado">
								<div>
									<input id="data_atualizacao" type="text" name="data_atualizacao" value="<?if(@$data_atualizacao->data_cadastro) echo date('d/m/Y',strtotime($data_atualizacao->data_cadastro))?>" /> 
								</div>
							</div>
						</div>		 
						<br class="clr" />               
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
                 	<? if (@$fases):?>
			            <table id="listaFases" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                        <th width="35%">Título</th>
			                        <th width="30%">Status</th>
			                        <th width="30%">Data Atualização</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($fases as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                        <td width="30%"><?=$row->titulo?></td>
			                        <td width="30%"><?=$row->status?></td>
			                        <td width="30%"><?=$row->data_atualizacao?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
			                        	<a href="javascript: delFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->titulo?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            </table>
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhuma Fase encontrado.<br/></span>
		            <? endif; ?><br/>
           	 </div>
            <? endif; ?>
            
            
            <? if (@$empreendimento->id_empreendimento): ?>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="ItensFasesAjax">
                	<h2>Itens Fases</h2>
                	<br/><br/>
					<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarItensFases/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$itensFases->id_item_fase): ?>
						<input type="hidden" name="id_item_fase" value="<?=$itens->id_item_fase?>" id="FormItensFaseId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItemFase" value="<?=@$itensFases->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">
		                	<label class="obrigatorio">Fase do Item</label>
	                         <div>
		                         <select name="ItemFase" class="campo-padrao" id="FormItemFase">
		                         	<option value=""> -- Escolha a Fase -- </option>
		                         	<?php if (@$fases):?>
		                        		<?php foreach($fases as $row):?>
											<option value="<?php echo $row->id_fase?>"><?php echo $row->titulo ?></option>
										<?php endforeach;?>
									<?php endif;?>
	                        	</select>
	                         </div>                
		               </div>
		               <br class="clr" />		
		               <div class="formulario-colunado">           
		                    <label class="obrigatorio">Porcentagem</label>
                        	<div><input type="text" name="porcentagem" class="campo-padrao" id="FormPerItem" value="<?=@$itensFases->porcentagem?>" /></div>                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
			            <table id="ListaItensFasesAjax" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <? if (@$itensFases):?>
			                <thead>
			                    <tr>
			                        <th width="35%">Título</th>
			                        <th width="30%">Fase</th>
			                        <th width="30%">Porcentagem</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($itensFases as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                      <td width="30%"><?=$row->titulo?></td>
			                      <td width="30%"><?=$row->fase?></td>
			                      <td width="30%"><?=$row->porcentagem?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
			                        	<a href="javascript: delItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->id_item_fase?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhum Item das Fases encontrado.<br/></span>
		            <? endif; ?>
		            </table>
		            <br/>
           	 </div>
            <? endif; ?>
            
            <!-- INICIA FOTOS FASES-->
            <? if (@$empreendimento->id_empreendimento): ?>
            <!-- aqui --><br/>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2>FOTOS DAS FASES</h2>
                	<br/><br/>
					<form id="FormFotoFasesEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvarMidiaFases/" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id_empreendimento" value="<?=$empreendimento->id_empreendimento?>">
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione a Fase</label>
		                    <div>
		                    	<select name="FaseFoto" class="campo-padrao" id="FormFaseFoto">
		                    		<option value=""> -- Escolha a Fase -- </option>
		                         	<?php if (@$fases):?>
		                        		<?php foreach($fases as $row):?>
											<option value="<?php echo $row->id_fase?>"><?php echo $row->titulo ?></option>
										<?php endforeach;?>
									<?php endif;?>
		                    	</select>
		                    </div>	                    
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado" id="campo-foto">
		                    <label class="obrigatorio">Selecione a foto:</label>
		                    <div><input type="file" name="foto" id="FormFotooo" class="campo-padrao" ></div>
		                </div>
		                <br class="clr" />
		                <div class="formulario-colunado">
		                    <label class="obrigatorio">Legenda</label>
                        	<div><input type="text" name="legenda" class="campo-padrao" id="FormLegenda" value="" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">&nbsp;</label>
		                    <div>&nbsp;</div>	                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                </form>
	                <br/><br/>
	                <? if(@$FasesFotos) { ?>
	                <? foreach ($FasesFotos as $row): ?>    
	                <label class="obrigatorio">LISTA <?php echo $row->fase?> </label>
	                <?php $this->load->model('empreendimentos_model', 'model', TRUE); 
	                $fotos = $this->model->getFotosFases($row->id_fase);?>
	                <br/>
		            <div id="galeria">
		            	<? foreach ($fotos as $row2): ?>                
			            <div id="fotos">
			            	<div align="center">
			        			<a href="<?=base_url()?>uploads/imoveis/fases/<?=$row2->imagem?>" rel="shadowbox[teste]" class="border"><img border="0" src="<?=base_url()?>uploads/imoveis/fases/thumbs/<?=$row2->imagem?>"></a>
			        		</div>
			        		<div align="center">
			        			<?=$row2->legenda?><br/>
			        			<a href="<?=site_url()?>/empreendimentos/apagarMidiaFase/<?=$row2->id_foto_fase?>/<?=$empreendimento->id_empreendimento?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			        		</div>
			        	</div>
			        	<? endforeach; ?>
					</div>
					<br class="clr">
					
	                <br/><br/>
						<? endforeach; ?>
					<? } else { ?>
			        	<label><br/>Nenhuma foto cadastrada</label>
			        <? } ?>						
					<br class="clr">
	          </div>
	        </div>
           
            <?php endif;?>
             <!-- FIM FOTOS FASES -->
            <!-- INICIA FOTOS -->
            <? if (@$empreendimento->id_empreendimento): ?>
            <!-- aqui --><br/>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2>GALERIAS DE FOTOS</h2>
                	<br/><br/>
					<form id="FormFotoEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvarMidia/" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id_empreendimento" value="<?=$empreendimento->id_empreendimento?>">
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione o Tipo de Mídia</label>
		                    <div>
		                    	<select name="tipoMidia" class="campo-padrao" id="FormTipoMidia">
		                    		<?php foreach($tipos_midias as $row):?>
										<option value="<?php echo $row->id_tipo_midia?>"><?php echo $row->tipo_midia?></option>
									<?php endforeach;?>
		                    	</select>
		                    </div>	                    
		                </div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione o Tipo de Arquivo</label>
		                    <div>
		                    	<select name="tipoArquivo" class="campo-padrao" id="FormTipoArquivo">
		                    		<option value="I">Imagem</option>
		                    		<option value="L">Link</option>
		                    		<option value="V">Vídeo</option>
		                    	</select>
		                    </div>	                    
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado" id="campo-foto">
		                    <label class="obrigatorio">Selecione a foto:</label>
		                    <div><input type="file" name="foto" id="FormFotooo" class="campo-padrao" ></div>
		                </div>
		                <div class="formulario-colunado" id="campo-link" style="display:none">
		                    <label class="obrigatorio">Digite o link ou id youtube para vídeo:</label>
		                    <div><input type="text" name="link" id="FormLink" class="campo-padrao" ></div>
		                </div>
		                <br class="clr" />
		                
		                <div class="formulario-colunado">
		                    <label class="obrigatorio">Legenda</label>
                        	<div><input type="text" name="legenda" class="campo-padrao" id="FormLegenda" value="<?=@$venda->legenda?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">&nbsp;</label>
		                    <div>&nbsp;</div>	                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                </form>
	                <br/><br/>
	                <? if(@$empreendimentosFotos) { ?>
	                <? foreach ($empreendimentosFotos as $row): ?>    
	                
	                <label class="obrigatorio">LISTA <?php echo $row->tipo?> </label>
	                <?php $this->load->model('empreendimentos_model', 'model', TRUE); 
	                $fotos = $this->model->getFotos($empreendimento->id_empreendimento,$row->id_midia);?>
	                <br/>
		            <div id="galeria">
		            	<? foreach ($fotos as $row2): ?>                
			            <div id="fotos">
			            	<div align="center">
			            		<?php if($row2->tipo_arquivo == 'I'){?>
			        				<a href="<?=base_url()?>uploads/imoveis/midias/<?=$row2->arquivo?>" rel="shadowbox[teste]" class="border"><img border="0" src="<?=base_url()?>uploads/imoveis/midias/thumbs/<?=$row2->arquivo?>"></a>
			        			<?php }else if ($row2->tipo_arquivo == 'L'){
			        					echo "<a target='_blank' href='".$row2->arquivo."'>".$row2->arquivo."</a>";
			        			}else{?>
			        				<img alt="<?=$row2->legenda?>" src="http://img.youtube.com/vi/<?=$row2->arquivo?>/1.jpg">
			        			<?php }?>
			        		</div>
			        		<div align="center">
			        			<?=$row2->legenda?><br/>
			        			<a href="<?=site_url()?>/empreendimentos/apagarMidia/<?=$row2->id_midia?>/<?=$empreendimento->id_empreendimento?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			        		</div>
			        	</div>
			        	<? endforeach; ?>
					</div>
					<br class="clr">
					
	                <br/><br/>
						<? endforeach; ?>
					<? } else { ?>
			        	<label><br/>Nenhuma foto cadastrada</label>
			        <? } ?>						
					<br class="clr">
	          </div>
	        </div>
	               
			<? endif; ?>
            <!-- TERMIN FOTOS -->
            
            
            <span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?><br/></span>
            <br/>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2>DADOS DO EMPREENDIMENTO</h2>
                	<br/><br/>
                    <form id="FormEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$empreendimento->id_empreendimento): ?>
                        <input type="hidden" name="id_empreendimento" value="<?=@$empreendimento->id_empreendimento?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Nome</label>
                        <div><input type="text" name="empreendimento" class="campo-padrao" id="FormEmpreendimento" value="<?=@$empreendimento->empreendimento?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
	                    <label class="obrigatorio">Empresa</label>
	                    <div class="formulario-colunado">
	                         <div>
	                         
		                         <select name="empresas" class="campo-padrao" id="FormEmpresas">
		                         	<option value=""> -- Escolha -- </option>
	                        		<?php foreach($empresas as $row):?>
										<option value="<?php echo $row->id_empresa?>" <?php if($row->id_empresa==$empreendimento->id_empresa) {echo 'selected="selected"';}?>><?php echo $row->empresa?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div>
		          <div class="formulario-colunado">
                        <label class="obrigatorio">Logo (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_logo" id="FormLogo" ></div>
                        <? if(@$empreendimento->logotipo): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/logo/<?=$empreendimento->logotipo?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Fachada (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_fachada" id="FormLogo" ></div>
                        <? if(@$empreendimento->logotipo): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/fachada/<?=$empreendimento->imagem_fachada?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    <br class="clr" />
		          
		          <div class="formulario-colunado">
	                    <label class="obrigatorio">Tipo Imóvel</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="tipos" class="campo-padrao" id="FormTipos">
		                         	<option value=""> -- Escolha -- </option>
	                        		<?php foreach($tipos_imoveis as $row):?>
										<option value="<?php echo $row->id_tipo_imovel?>" <?php if($row->id_tipo_imovel==@$empreendimento->id_tipo_imovel)echo 'selected="selected"'?>><?php echo $row->tipo_imovel ?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div>  
		          <br class="clr" />
                   <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Title]</label>
                        <div><textarea name="tags_title" class="campo-padrao" id="FormTitle"><?=@$empreendimento->tags_title?></textarea></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Keywords]</label>
                        <div><textarea name="tags_keywords" class="campo-padrao" id="FormKeywords"><?=@$empreendimento->tags_keywords?></textarea></div>
                    </div>
                    <br class="clr" />
                
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Description]</label>
                        <div><textarea name="tags_description" class="campo-padrao" id="FormDescription" ><?=@$empreendimento->tags_description?></textarea></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />
                    
                   	<div class="formulario">
                        <label class="obrigatorio">Descrição</label>
                        <div>
                        	<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$empreendimento->descricao?></textarea>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />                    
                   
                     <div class="formulario-colunado">
                        <label class="obrigatorio">Status Geral</label>
                        <div>
                        	<select name="status" class="campo-padrao" id="FormStatus">
                        		<option value="" selected="selected"> -- Escolha -- </option>
                        		<?php foreach($status as $row):?>
										<option value="<?php echo $row->id_status?>" <?php if($row->id_status==@$empreendimento->id_status)echo 'selected="selected"'?>><?php echo $row->status ?></option>
									<?php endforeach;?>
                        	</select>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
	                    <label class="obrigatorio">Ativo</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="ativo" class="campo-padrao" id="FormAtivo">
	                        		<option <? if(@$empreendimento->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
	                        		<option <? if(@$empreendimento->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
	                        	</select>
	                         </div>                
		                </div>
		               </div>
		            <br class="clr" />
                   <br/> 
		           <div class="formulario-colunado">
	                    <label class="obrigatorio">Estado</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="estado" class="campo-padrao" id="FormEstado">
		                         <option value="" selected="selected"> -- Escolha -- </option>
	                        		<?php foreach($estados as $row):?>
										<option value="<?php echo $row->id_estado?>" <?php if($row->id_estado==@$empreendimento->id_estado)echo 'selected="selected"'?>><?php echo $row->estado?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div> 
	              <div class="formulario-colunado-div">&nbsp;</div>
                      <div class="formulario-colunado">
	                    <label class="obrigatorio">Cidade</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="cidade" class="campo-padrao" id="FormCidade">
	                        		<?php if(@$cidades){?>
										<?php foreach($cidades as $row):?>
											<option value="<?php echo $row->id_cidade?>" <?php if($row->id_cidade==@$empreendimento->id_cidade)echo 'selected="selected"'?>><?php echo $row->cidade?></option>
										<?php endforeach;?>
									<?php }else{?>
											<option value="" selected="selected"> -- Escolha um Estado -- </option>
									<?php }?>										
	                        	</select>
	                         </div>                
		                </div>
		               </div> 
		           <br class="clr" />
                   <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Latitude</label>
                        <div>
                        	<input type="text" name="end_latitude" class="campo-padrao" id="FormLatitude" value="<?=@$empreendimento->latitude?>" />
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                   <div class="formulario-colunado">
                        <label class="obrigatorio">Longitude</label>
                        <div>
                        	<input type="text" name="end_longitude" class="campo-padrao" id="FormLongitude" value="<?=@$empreendimento->longitude?>" />
                        </div>                   
	                </div>
	                <br class="clr" />
                    <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/empreendimentos/lista" title="Voltar" class="bt-voltar">Voltar para Empreendimentos</a>
			<br/><br/><br/>
			
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>