<?=$this->load->view('includes/topo');?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/empreendimentos.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/cleditor/jquery.cleditor.min.js"></script>

<script type="text/javascript" charset="utf-8">
	
	$(function() {
	   	
	   	$("input#FormLogo").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
	   	$("input#FormPin").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		$("input#FormFachada").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

		$("input#FormVisitado").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		
	   	$("input#FormFotoFase").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

		$("input#FormFotoEmpreendimento").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		
		$("input#FormBanner").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		}); 
		
		$("input#FormMemorial").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		
		      
		$('#FormTitle').jqEasyCounter({
			'maxChars': 90,
			'maxCharsWarning': 80,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormKeywords').jqEasyCounter({
			'maxChars': 200,
			'maxCharsWarning': 180,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormDescription').jqEasyCounter({
			'maxChars': 250,
			'maxCharsWarning': 230,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});

		$('#FormChamada').jqEasyCounter({
			'maxChars': 150,
			'maxCharsWarning': 130,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		}); 
		
		// $("#FormDescricao").cleditor(); // Habilita o editor para o textarea

		$("#data_atualizacao_fotos").datepicker({
	    	yearRange: '-100:+3',
	        showOn: "button",
	        buttonImage: "http://www.nexgroup.com.br/assets/admin/img/ico-calendario.gif",
	        buttonImageOnly: true
	    });
		$("#data_atualizacao_itens").datepicker({
	    	yearRange: '-100:+3',
	        showOn: "button",
	        buttonImage: "http://www.nexgroup.com.br/assets/admin/img/ico-calendario.gif",
	        buttonImageOnly: true
	    });    
             
  });

</script>
	
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . CADASTRO" class="FlashTitulo"></a></div>
          
            
			
			<? if (@$empreendimento->id_empreendimento): ?>
            <br/>
            	<h3><?=@$empreendimento->empreendimento?> - <a href="<?=base_url()?>imoveis/pre_visualizar/<?=$empreendimento->id_empreendimento?>">Pré-visualizar</a></h3>
            <br/>
            <div id="rsvErrorsItens"></div>
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaItens')?><br/></span>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="ItensAjax">
                	<h2>Itens</h2>
                	<br/><br/>
					<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarItens/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$itens->id_item): ?>
						<input type="hidden" name="id_item" value="<?=$itens->id_item?>" id="FormItensId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItem" value="<?=@$itens->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <!--<div class="formulario-colunado">           
		                    <label class="obrigatorio">Destaque</label>
                        	<div><input id="destaqueSim" type="radio" name="destaque" class="formulario-radio" value="S" /><label class="obrigatorio">Sim</label><input type="radio" id="destaqueNao" name="destaque" class="formulario-radio" value="N" /><label class="obrigatorio">Não</label></div>                    
		                </div>-->
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
                 <? if (@$itens):?>
			            <table id="listaItens" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                    	<th width="2%">Ordem</th>
			                        <th width="35%">Título</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($itens as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                    	<td  nowrap="nowrap">
                    					<a href="javascript: desceItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    				</td>
			                        <td width="30%"><?=$row->item?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                        	<a href="javascript: delItem('<?=$row->id_item?>', '<?=@$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=@$row->item;?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            </table>
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhum Item encontrado.<br/></span>
		            <? endif; ?><br/>
           	 </div>
            <? endif; ?>
            
            <? if (@$empreendimento->id_empreendimento): ?>
            <div id="rsvErrorsFases"></div>
            <span class="txt-destaque" style="diplay:block;"><?=@$this->session->flashdata('respostaFases')?><br/></span>
           
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="FasesAjax">
                	<h2>Fases</h2>
                	<br/><br/>

					<form id="FormFases" action="<?=site_url()?>/empreendimentos/salvarFases/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormVendaId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$fases->id_fase): ?>
							<input type="hidden" name="id_fase" value="<?=$fases->id_fase?>" id="FormFaseId">
						<? endif; ?>

		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloFase" value="<?=@$fases->item?>" /></div>
		                </div>
		                
		                <div class="formulario-colunado-div">&nbsp;</div>
						
						<div class="formulario-colunado">
							<label class="obrigatorio">Status</label>
							<div>
								<select name="status" class="campo-padrao" id="FormStatus">
									<option value="" selected="selected"> -- Escolha -- </option>
									<?php foreach($status as $row):?>
									<option value="<?php echo $row->id_status?>"><?php echo $row->status ?></option>
								<?php endforeach;?>
							</select>
							</div>
						</div>

		                <br class="clr" />
		               
						<div class="formulario-colunado">
 							<label class="obrigatorio">Mostrar Fase</label>
                        	<div>
                        		<select name="mostrarFase" class="campo-padrao" id="FormMostrar">
	                        		<option <? if(@$fases->mostrar_fase == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
	                        		<option <? if(@$fases->mostrar_fase == 'N') echo "selected='selected'"; ?> value="N">Não</option>
	                        	</select>
							</div>
		                </div>

		                <div class="formulario-colunado-div">&nbsp;</div>	

		                <div class="formulario-colunado">
 							<label class="obrigatorio">Porcentagem</label>
                        	<div><input type="text" name="porcentagem_fase" class="campo-padrao" id="FormPorcentagemFase" value="<?=@$fases->porcentagem_fase?>" /></div>
		                </div>

						<br class="clr" />

		                <div class="a-right" style="margin-right:10px;">
		                	<input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" >
		                </div>
	                	<br/>
	                </form>							                
                </div>
             	<? if (@$fases):?>
		            <table id="listaFases" class="tabela-padrao" style="width:515px; margin-left:18px;">
		                <thead>
		                    <tr>
		                    	<th>Ordem</th>
		                        <th width="35%">Título</th>
		                        <th width="20%">Status</th>
		                        <th width="20%">Mostrar Fase</th>
		                        <th width="15%">Porcentagem</th>
		                        <th width="5%">Ações</th>
		                    </tr>
		                </thead>
		                <tbody > 
		                	<? $count = 1; ?>
		                    <? foreach ($fases as $row):?>
		                    <? $count++; ?>
		                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
								<td  nowrap="nowrap">
									<a href="javascript: desceFase('<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
									&nbsp;
									<a href="javascript: sobeFase('<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
								</td>
		                        <td width="30%"><?=$row->titulo?></td>
		                        <td width="20%"><?=$row->status?></td>
		                        <td width="20%"><? if($row->mostrar_fase == 'S') echo 'Sim'; else echo 'Não'?></td>
	                           	<td width="15%"><?=$row->porcentagem_fase?>%</td>
		                        <td nowrap="nowrap">
		                        	<a href="javascript: getFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
		                        	<a href="javascript: delFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->titulo?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
		                        </td>
		                    </tr>
		                    <? endforeach; ?>
		                </tbody>
		            </table>
		            <br/>
	            <? else: ?>
	            	<span style="margin-left:20px;">Nenhuma Fase encontrada.<br/></span>
	            <? endif; ?>
	            <br/>
       	 	</div>
            <? endif; ?>
          
          <? if (@$empreendimento->id_empreendimento): ?>
            <div id="rsvErrorsFases"></div>
            <span class="txt-destaque" style="diplay:block;">
            	<?=@$this->session->flashdata('respostaData')?><br/>
            </span>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="FasesAjax">
                	<h2>Datas de Atualizações</h2>
                	<br/><br/>
					<form id="FormDataAtualizacao" action="<?=site_url()?>/empreendimentos/salvarDataAtualizacao/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormIdEmpreendimento" value="<?=$empreendimento->id_empreendimento?>">
		                <br class="clr" />
		                <div class="formulario-colunado">
							<label class="obrigatorio">Data Atualização Itens das Obras</label>
							<div class="formulario-colunado">
								<div>
									<input id="data_atualizacao_itens" type="text" name="data_atualizacao_itens" value="<?if(@$empreendimento->data_atualizacao_itens_obras) echo date('d/m/Y',strtotime($empreendimento->data_atualizacao_itens_obras))?>" /> 
								</div>
							</div>
						</div> 
						<div class="formulario-colunado-div">&nbsp;</div>
						 <div class="formulario-colunado">
							<label class="obrigatorio">Data Atualização Fotos das Obras</label>
							<div class="formulario-colunado">
								<div>
									<input id="data_atualizacao_fotos" type="text" name="data_atualizacao_fotos" value="<?if(@$empreendimento->data_atualizacao_fotos_obras) echo date('d/m/Y',strtotime($empreendimento->data_atualizacao_fotos_obras))?>" /> 
								</div>
							</div>
						</div>		 
						<br class="clr" />               
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
           	 </div>
            <? endif; ?>
          
          
            <? if (@$empreendimento->id_empreendimento): ?>
             <div id="rsvErrorsItensFases"></div>
            
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg" id="ItensFasesAjax">
                	<h2>Itens Fases</h2>
                	<br/><br/>
					<form id="FormItemFase" action="<?=site_url()?>/empreendimentos/salvarItensFases/" method="post">
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<? if(@$itensFases->id_item_fase): ?>
						<input type="hidden" name="id_item_fase" value="<?=$itens->id_item_fase?>" id="FormItensFaseId">
						<? endif; ?>
		                <div class="formulario-colunado">
 							<label class="obrigatorio">Título</label>
                        	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloItemFase" value="<?=@$itensFases->item?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">
		                	<label class="obrigatorio">Fase do Item</label>
	                         <div>
		                         <select name="ItemFase" class="campo-padrao" id="FormItemFase">
		                         	<option value=""> -- Escolha a Fase -- </option>
		                         	<?php if (@$fases):?>
		                        		<?php foreach($fases as $row):?>
											<option value="<?php echo $row->id_fase?>"><?php echo $row->titulo ?></option>
										<?php endforeach;?>
									<?php endif;?>
	                        	</select>
	                         </div>                
		               </div>
		               <br class="clr" />		
		               <div class="formulario-colunado">           
		                    <label class="obrigatorio">Porcentagem</label>
                        	<div><input type="text" name="porcentagem" class="campo-padrao" id="FormPerItem" value="<?=@$itensFases->porcentagem?>" /></div>                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                	<br/>
	                </form>							                
                </div>
			            <table id="ListaItensFasesAjax" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <? if (@$itensFases):?>
			                <thead>
			                    <tr>
			                    	<th width="20%">Ordem</th>
			                        <th width="35%">Título</th>
			                        <th width="30%">Fase</th>
			                        <th width="30%">Porcentagem</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($itensFases as $row):?>
			                    <? $count++; ?>
			                    <?php 
			                 
			                    if(@$faseAnterior != @$row->id_fase && $count != 2 ){?>
					                   <tr class="par">
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   </tr>
					                   <tr class="par">
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   		<td>&nbsp;</td>
					                   </tr>
			                    <?php } ?>
			                    
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                      <td  nowrap="nowrap">
                    			 	<a href="javascript: desceItemFase('<?=$row->id_item_fase?>','<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    				&nbsp;
                    				<a href="javascript: sobeItemFase('<?=$row->id_item_fase?>','<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    		  	  </td>
			                      <td width="30%"><?=$row->titulo?></td>
			                      <td width="30%"><?=$row->fase?></td>
			                      <td width="30%"><?=$row->porcentagem?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
			                        	<a href="javascript: delItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->id_item_fase?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <tr><td colspan="6" class="editar"><div class="edita-<?=$row->id_item_fase?>"></div></td></tr>
			                    <?php $faseAnterior = $row->id_fase;?>
			                    <? endforeach; ?>
			                </tbody>
			            
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhum Item das Fases encontrado.<br/></span>
		            <? endif; ?>
		            </table>
		            <br/>
           	 </div>
            <? endif; ?>
            <? if (@$empreendimento->id_empreendimento): ?> 
              
            <div id="rsvErrorsFotosFases"></div>
            <!-- INICIA FOTOS FASES-->
            <br/>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2 id="GalFases">FOTOS DAS FASES</h2>
                	<br/><br/>
					<form id="FormFotoFasesEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvarMidiaFases/" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id_empreendimento" value="<?=$empreendimento->id_empreendimento?>">
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione a Fase</label>
		                    <div>
		                    	<select name="FaseFoto" class="campo-padrao" id="FormFaseFoto">
		                    		<option value=""> -- Escolha a Fase -- </option>
		                         	<?php if (@$fases):?>
		                        		<?php foreach($fases as $row):?>
											<option value="<?php echo $row->id_fase?>"><?php echo $row->titulo ?></option>
										<?php endforeach;?>
									<?php endif;?>
		                    	</select>
		                    </div>	                    
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado" id="campo-foto">
		                    <label class="obrigatorio">Selecione a foto:</label>
		                    <div><input type="file" name="foto" id="FormFotoFase" class="campo-padrao" ></div>
		                </div>
		                <br class="clr" />
		                <div class="formulario-colunado">
		                    <label class="obrigatorio">Legenda</label>
                        	<div><input type="text" name="legenda" class="campo-padrao" id="FormLegendaFase" value="" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">&nbsp;</label>
		                    <div>&nbsp;</div>	                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                </form>
	                <br/><br/>
	                <div id="ListaFotosFases" >
	                <? if(@$FasesFotos) { ?>
	                <? foreach ($FasesFotos as $row): ?> 
	                 	<label class="obrigatorio">LISTA <?php echo $row->fase?> </label>   
	                	<table class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                    	<th width="2%">Ordem</th>
			                        <th width="20%">Foto</th>
			                        <th width="35%">Legenda</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
	                <?php $this->load->model('empreendimentos_model', 'model', TRUE); 
	                $fotos = $this->model->getFotosFases($row->id_fase);?>
	                		<tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($fotos as $row2): ?>      
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                      <td  nowrap="nowrap">
                    					<a href="javascript: desceFotoFase('<?=$row2->id_foto_fase?>','<?=@$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeFotoFase('<?=$row2->id_foto_fase?>','<?=@$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    			  </td>
			                      <td width="20%"><a href="<?=base_url()?>uploads/imoveis/fases/<?=$row2->imagem?>" rel="shadowbox[teste]" class="border"><img width="50px" border="0" src="<?=base_url()?>uploads/imoveis/fases/thumbs/<?=$row2->imagem?>"></a></td>
			                      <td width="30%" class="LegendaFotoFase-<?=$row2->id_foto_fase?>"><?=$row2->legenda?><br/></td>
			                      <td nowrap="nowrap">
			                      	<a href="javascript: getLegendaFotoFase('<?=$row2->id_foto_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                      	<a href="javascript: delFotoFase('<?=$row2->id_foto_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                      </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
		            	</table>
		            <? endforeach; ?>
		            <? }else{ ?>
		            	<span style="margin-left:20px;">Nenhuma Foto das Fases encontrada.<br/></span>
		            <? } ?>
		            </div>
					<br class="clr">
	          </div>
	        </div>
            <?php endif;?>
             <!-- FIM FOTOS FASES -->
             <? if (@$empreendimento->id_empreendimento): ?>
              
            <div id="rsvErrorsFotosEmpreendimentos"></div>
            <!-- INICIA FOTOS -->
            <br/>
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2 id="GalFotos">GALERIAS DE FOTOS</h2>
                	<br/><br/>
					<form id="FormFotoEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvarMidia/" method="post" enctype="multipart/form-data">
						<input id="FormFotoid_empreendimento" type="hidden" name="id_empreendimento" value="<?=$empreendimento->id_empreendimento?>">
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione o Tipo de Mídia</label>
		                    <div>
		                    	<select name="tipoMidia" class="campo-padrao" id="FormTipoMidia">
									<option value="10">Fotos</option>
		                    		<?php foreach($tipos_midias as $row):?>
										<option value="<?php echo $row->id_tipo_midia?>"><?php echo $row->tipo_midia?></option>
									<?php endforeach;?>
		                    	</select>
		                    </div>	                    
		                </div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">Selecione o Tipo de Arquivo</label>
		                    <div>
		                    	<select name="tipoArquivo" class="campo-padrao" id="FormTipoArquivo">
		                    		<option value="I">Imagem</option>
		                    		<option value="L">Link</option>
		                    		<option value="V">Vídeo</option>
		                    		<option value="P">PDF (Implantações)</option>
		                    	</select>
		                    </div>	                    
		                </div>

		                <script type="text/javascript">

		                $('#FormTipoMidia').change(function(){

		                	console.log('seleciou o tipo de midia: ' + this.value);

		                	var options = '<option value="I">Imagem</option>' +
			                    		  '<option value="L">Link</option>' +
			                    		  '<option value="V">Vídeo</option>' +
			                    		  '<option value="P">PDF (Implantações)</option>';

		                	if(this.value == 12 || this.value == 13)
		                	{
		                		options = '<option value="L">Link Externo</option>' +
			                    		  '<option value="V">Modal (Video Youtube)</option>';
		                	}

		                	$('#FormTipoArquivo').html(options);
		                });

		                </script>

		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado" id="campo-foto">
		                    <label class="obrigatorio">Selecione a foto:</label>
		                    <div><input type="file" name="foto" id="FormFotoEmpreendimento" class="campo-padrao" ></div>
		                </div>
		                <div class="formulario-colunado" id="campo-link" style="display:none">
		                    <label class="obrigatorio">Digite o link ou id youtube para vídeo:</label>
		                    <div><input type="text" name="link" id="FormLink" class="campo-padrao" ></div>
		                </div>
		                <br class="clr" />
		                
		                <div class="formulario-colunado">
		                    <label class="obrigatorio">Legenda</label>
                        	<div><input type="text" name="legenda" class="campo-padrao" id="FormLegenda" value="<?=@$venda->legenda?>" /></div>
		                </div>
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <div class="formulario-colunado">           
		                    <label class="obrigatorio">&nbsp;</label>
		                    <div>&nbsp;</div>	                    
		                </div>
		                <br class="clr" />		                
		                <div class="a-right" style="margin-right:10px;"><input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" ></div>                
	                </form>
	                <br/><br/>
	                <div id="ListaMidias" >
	             		<? if(@$empreendimentosFotos) { ?>
	               		<? foreach ($empreendimentosFotos as $row): ?>  
	                 	<label class="obrigatorio">LISTA <?php echo $row->tipo?> </label>
	                	<table class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                    	<th width="2%">Ordem</th>
			                        <th width="20%">Foto</th>
			                        <th width="35%">Legenda</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
	                		<?php $this->load->model('empreendimentos_model', 'model', TRUE); 
	                		$fotos = $this->model->getFotos($empreendimento->id_empreendimento,$row->id_midia);?>
	                		<tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($fotos as $row2): ?>          
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                      <td  nowrap="nowrap">
                    					<a href="javascript: desceMidias('<?=$row2->id_midia?>','<?=$row2->id_tipo_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeMidias('<?=$row2->id_midia?>','<?=$row2->id_tipo_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    			  </td>
			                      <td width="20%">
										<?php if($row2->tipo_arquivo == 'I'){?>
			        							<a href="<?=base_url()?>uploads/imoveis/midias/<?=$row2->arquivo?>" rel="shadowbox[teste]" class="border"><img width="50px" border="0" src="<?=base_url()?>uploads/imoveis/midias/thumbs/<?=$row2->arquivo?>"></a>
			        					<?php }else if ($row2->tipo_arquivo == 'L'){
			        							echo "<a target='_blank' href='".$row2->arquivo."'>".$row2->arquivo."</a>";
			        						  }else{?>
			        								<img width="50px" alt="<?=$row2->legenda?>" src="http://img.youtube.com/vi/<?=$row2->arquivo?>/1.jpg">
			        					<?php }?>
			                      <td width="30%" class="LegendaMidia-<?=$row2->id_midia?>"><?=$row2->legenda?><br/></td>
			                      <td nowrap="nowrap">
			                      	<a href="javascript: getLegendaMidia('<?=$row2->id_midia?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                      	<a href="javascript: delMidia('<?=$row2->id_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                      </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
		            	</table>
		            	<br class="clr">
		            <? endforeach; ?>
		            <? }else{ ?>
		            	<span style="margin-left:20px;">Nenhuma Foto encontrada.<br/></span>
		            <? } ?>
		            </div>
		            				
					<br class="clr">
	          </div>
	        </div>
	               
			<? endif; ?>
            <!-- TERMIN FOTOS -->
            
            <div id="rsvErrorsEmpreendimentos"></div>
            <span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?><br/></span>
            <br/>
            <div class="formulario-grande">
            <form id="FormEmpreendimentos" action="<?=site_url()?>/empreendimentos/salvar/" method="post" enctype="multipart/form-data">
            <?  if (@$empreendimento->id_empreendimento): ?>
                <div class="formulario-mg" id="DormitoriosAjax">
                	<h2>Dormitórios</h2>
                	<br/><br/>
						<input type="hidden" name="id_empreendimento" id="FormEmpreendimentoId" value="<?=$empreendimento->id_empreendimento?>">
						<ul>
						<?php $dorm_atrib =array();
							foreach ($dormitorios_atribuidos as $row):
								$dorm_atrib[] = $row->id_dormitorio;
							endforeach;
							?>
            			<?php foreach (@$dormitorios as $row): ?> 
							<li style="width: 100px; display:inline; list-style-type: none;">
								<input type="checkbox"	id="dormitorios[<?=$row->id_dormitorio?>]" name="dormitorios[<?=$row->id_dormitorio?>]"	class="Checkbox" value="<?=$row->id_dormitorio?>"
								 <?php	if(in_array($row->id_dormitorio, $dorm_atrib)){	echo 'checked="checked"';}?>
								/>
								<label class="labelr"><?=$row->dormitorio?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
							</li>
						<?php endforeach; ?>
						</ul>
		                <br class="clr" />

		                <label class="obrigatorio">Dormitórios (descrição): </label>
                        <div><input type="text" name="dormitorio" class="campo-padrao" maxlength="37" value="<?=@$empreendimento->dormitorio?>" /></div>		                
                </div>
                <div class="formulario-mg" id="BanheirosAjax">
                	<h2>Banheiros</h2>
                	<br/><br/>
						<ul>
						<?php $ban_atrib =array();
							foreach ($banheiros_atribuidos as $row):
								$ban_atrib[] = $row->id_banheiro;
							endforeach;
							?>
            			<?php foreach (@$banheiros as $row): ?> 
							<li  style="width: 100px; display:inline; list-style-type: none;">
								<input type="checkbox"	id="banheiros[<?=$row->id_banheiro?>]" name="banheiros[<?=$row->id_banheiro?>]"	class="Checkbox" value="<?=$row->id_banheiro?>"
								 <?php	if(in_array($row->id_banheiro, $ban_atrib)){	echo 'checked="checked"';}?>
								/>
								<label class="labelr" style="width: 100px;"><?=$row->banheiro?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
							</li>
						<?php endforeach; ?>
						</ul>
		                <br class="clr" />		                
                </div>
                <div class="formulario-mg" id="FaixaPrecoAjax">
                	<h2>Faixa de Preços</h2>
                	<br/><br/>
						<ul>
						<?php $preco_atrib =array();
							foreach ($faixaPrecos_atribuidos as $row):
								$preco_atrib[] = $row->id_faixa_preco;
							endforeach;
							?>
            			<?php foreach (@$faixaPrecos as $row): ?> 
							<li style="list-style-type: none;">
								<input type="checkbox"	id="faixaPreco[<?=$row->id_faixa_preco?>]" name="faixaPreco[<?=$row->id_faixa_preco?>]"	class="Checkbox" value="<?=$row->id_faixa_preco?>"
								 <?php	if(in_array($row->id_faixa_preco, $preco_atrib)){	echo 'checked="checked"';}?>
								/>
								<label class="labelr"><?=$row->faixa_preco?> &nbsp;&nbsp;&nbsp;&nbsp;</label>
							</li>
						<?php endforeach; ?>
						</ul>
		                <br class="clr" />		                
                </div>
                <div class="formulario-mg" id="SemelhantesAjax">
                	<h2>Empreendimentos semelhantes</h2>
                	<br/><br/>
						<ul>
						<?php $seme_atrib =array();
							foreach ($semelhantes_atribuidos as $row):
								$seme_atrib[] = $row->id_semelhante;
							endforeach;
							?>
            			<?php foreach (@$semelhantes as $row): 
            					if($row->id_empreendimento != $empreendimento->id_empreendimento ){
            			?> 
            				
							<li class="semelhantes">
								<input type="checkbox"	id="semelhantes[<?=$row->id_empreendimento?>]" name="semelhantes[<?=$row->id_empreendimento?>]"	class="Checkbox" value="<?=$row->id_empreendimento?>"
								 <?php	if(in_array($row->id_empreendimento, $seme_atrib)){	echo 'checked="checked"';}?>
								/>
								<? if(@$row->logotipo): ?>
                        			<label for="semelhantes[<?=$row->id_empreendimento?>]" class="labelr"><img src="<?=base_url()?>uploads/imoveis/logo/<?=$row->logotipo?>" width="60px;"></label>
                        		<? endif; ?>
                        		<br />
								<label for="semelhantes[<?=$row->id_empreendimento?>]" class="labelr"><?=$row->empreendimento?><br /><?=$row->cidade?></label>
							</li>
						<?php } endforeach; ?>
						</ul>
		                <br class="clr" />		                
                </div>
			<?php endif;?>
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<h2>DADOS DO EMPREENDIMENTO</h2>
                	<br/><br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Nome</label>
                        <div><input type="text" name="empreendimento" class="campo-padrao" id="FormEmpreendimento" value="<?=@$empreendimento->empreendimento?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
	                    <label class="obrigatorio">Empresa</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="empresas" class="campo-padrao" id="FormEmpresas">
		                         	<option value=""> -- Escolha -- </option>
	                        		<?php foreach($empresas as $row):?>
										<option value="<?php echo $row->id_empresa?>" <?php if($row->id_empresa == @$empreendimento->id_empresa) {echo 'selected="selected"';}?>><?php echo $row->empresa?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div>

		          <br class="clr" />
		          <br class="clr" />
		            
		          <div class="formulario-colunado">
		            <input type="hidden" name="destaque_home" value="0" />
		          	<input type="checkbox"	id="destaque_home" name="destaque_home"	class="Checkbox" value="1" <?php if(@$empreendimento->destaque_home == 1){	echo 'checked="checked"';}?>/>
					<label for="destaque_home">Mostrar destaque na home</label>
		    	  </div>      

  				  <div class="formulario-colunado-div">&nbsp;</div>
		          <div class="formulario-colunado">
                        <label class="obrigatorio">Logo (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_logo" id="FormLogo" ></div>
                        <? if(@$empreendimento->logotipo): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/logo/<?=$empreendimento->logotipo?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Fachada (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_fachada" id="FormLogo" ></div>
                        <? if(@$empreendimento->imagem_fachada): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/fachada/visualizar/<?=$empreendimento->imagem_fachada?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    <br class="clr" />
		            <br class="clr" />
		            <div class="formulario-colunado">
                        <label class="obrigatorio">Imagem Imóvel já visitado (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_visitado" id="FormVisitado" ></div>
                        <? if(@$empreendimento->imagem_visitado): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/fachada/visitados/<?=$empreendimento->imagem_visitado?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Imagem pin google maps (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_pin" id="FormPin" ></div>
                        <? if(@$empreendimento->pin): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/imoveis/pin/<?=$empreendimento->pin?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                     <br class="clr" />
                     <br class="clr" />
                    <div class="formulario-colunado">
	                    <label class="obrigatorio">Tipo Imóvel</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="tipos" class="campo-padrao" id="FormTipos">
		                         	<option value=""> -- Escolha -- </option>
	                        		<?php foreach($tipos_imoveis as $row):?>
										<option value="<?php echo $row->id_tipo_imovel?>" <?php if($row->id_tipo_imovel==@$empreendimento->id_tipo_imovel)echo 'selected="selected"'?>><?php echo $row->tipo_imovel ?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div>
                  <div class="formulario-colunado-div">&nbsp;</div>
                   
		          <div class="formulario-colunado">
	                    <label class="obrigatorio">Link Hotsite</label>
	                    <div class="formulario-colunado">
	                         <div>
		                      <div><input type="text" name="hotsite" class="campo-padrao" id="FormEmpreendimentoHotsite" value="<?=@$empreendimento->hotsite?>" /></div>  
	                         </div>                
		               </div>
		          </div>

		           <br class="clr" />
		          <div class="formulario-colunado">
                        <label class="obrigatorio">Metragem</label>
                        <!-- <div><input type="text" name="metragem" class="campo-padrao" id="FormMetragem" value="<?=@$empreendimento->metragem?>" /></div> -->
                        <div>
                         <select name="metragem" class="campo-padrao" id="FormMetragem">
                          <option value=""> -- Escolha -- </option>
                          <option value="Casas"  <?php if($empreendimento->metragem == "Casas") echo 'selected="selected"'?>> Casas </option>
                          <option value="Apartamentos" <?php if($empreendimento->metragem == "Apartamentos") echo 'selected="selected"'?>> Apartamentos </option>
                         </select>
                        </div>
                    </div>
                    
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Vagas</label>
                        <div><input type="text" name="vagas" class="campo-padrao" id="FormVagas" value="<?=@$empreendimento->vagas?>" /></div>
                    </div>


		 		  <br class="clr" />
		          <div class="formulario-colunado">
                        <label class="obrigatorio">Chamada Imóvel</label>
                        <div><textarea name="chamada_empreendimento" class="campo-padrao" id="FormChamada"><?=@$empreendimento->chamada_empreendimento?></textarea></div>
                    </div>  

                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Segunda Chamada Imóvel</label>
                        <div><textarea name="segunda_chamada_empreendimento" class="campo-padrao" id="FormSegundaChamada"><?=@$empreendimento->segunda_chamada_empreendimento?></textarea></div>
                    </div>  
		         
		         <br class="clr" />
                   <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Title]</label>
                        <div><textarea name="tags_title" class="campo-padrao" id="FormTitle"><?=@$empreendimento->tags_title?></textarea></div>
                    </div>
                    <br class="clr" />
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Keywords]</label>
                        <div><textarea name="tags_keywords" class="campo-padrao" id="FormKeywords"><?=@$empreendimento->tags_keywords?></textarea></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                
                    <div class="formulario-colunado">
                        <label class="obrigatorio">TAGS [Description]</label>
                        <div><textarea name="tags_description" class="campo-padrao" id="FormDescription" ><?=@$empreendimento->tags_description?></textarea></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />
                    
                   	<div class="formulario">
                        <label class="obrigatorio">Link Corretor</label>
                        <div>
                        	<input type="text" name="link_corretor" class="campo-grande" id="FormLinkCorretor" value="<?=@$empreendimento->link_corretor?>" style="width: 100%;" />
                        </div>
                    </div>
                    <br class="clr" />
                    
                   	<div class="formulario">
                        <label class="obrigatorio">Descrição</label>
                        <div>
                        	<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$empreendimento->descricao?></textarea>
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
                        <div>&nbsp;</div>
                    </div>
                    <br class="clr" />                    
                   
                     <div class="formulario-colunado">
                        <label class="obrigatorio">Status Geral</label>
                        <div>
                        	<select name="status" class="campo-padrao" id="FormStatus">
                        		<option value="" selected="selected"> -- Escolha -- </option>
                        		<?php foreach($status as $row):?>
										<option value="<?php echo $row->id_status?>" <?php if($row->id_status==@$empreendimento->id_status)echo 'selected="selected"'?>><?php echo $row->status ?></option>
									<?php endforeach;?>
                        	</select>
                        </div>
                    </div>

                    <div class="formulario-colunado-div">&nbsp;</div>

                    <div class="formulario-colunado">
                        <label class="obrigatorio">Status Geral (label max 22 caracteres)</label>
                        <div>
                        	<input type="text" name="status_label" class="campo-padrao" id="FormStatusLabel" value="<?=@$empreendimento->status_label?>" maxlength="22" />
                        </div>
                    </div>

                    <br class="clr" />
                    <br/> 
                    
                    <div class="formulario-colunado">
                    	<label class="obrigatorio">Ativo</label>
                    	<div class="formulario-colunado">
                    		<div>
                    			<select name="ativo" class="campo-padrao" id="FormAtivo">
                    				<option <? if(@$empreendimento->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
                    				<option <? if(@$empreendimento->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
                    			</select>
                    		</div>                
                    	</div>
                    </div>

		           <br class="clr" />
                   <br/> 
		           <div class="formulario-colunado">
	                    <label class="obrigatorio">Estado</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="estado" class="campo-padrao" id="FormEstado">
		                         <option value="" selected="selected"> -- Escolha -- </option>
	                        		<?php foreach($estados as $row):?>
										<option value="<?php echo $row->id_estado?>" <?php if($row->id_estado==@$empreendimento->id_estado)echo 'selected="selected"'?>><?php echo $row->estado?></option>
									<?php endforeach;?>
	                        	</select>
	                         </div>                
		               </div>
		          </div> 
	              <div class="formulario-colunado-div">&nbsp;</div>
                      <div class="formulario-colunado">
	                    <label class="obrigatorio">Cidade</label>
	                    <div class="formulario-colunado">
	                         <div>
		                         <select name="cidade" class="campo-padrao" id="FormCidade">
	                        		<?php if(@$cidades){?>
										<?php foreach($cidades as $row):?>
											<option value="<?php echo $row->id_cidade?>" <?php if($row->id_cidade==@$empreendimento->id_cidade)echo 'selected="selected"'?>><?php echo $row->cidade?></option>
										<?php endforeach;?>
									<?php }else{?>
											<option value="" selected="selected"> -- Escolha um Estado -- </option>
									<?php }?>										
	                        	</select>
	                         </div>                
		                </div>
		               </div> 
		           <br class="clr" />
                   <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Latitude</label>
                        <div>
                        	<input type="text" name="end_latitude" class="campo-padrao" id="FormLatitude" value="<?=@$empreendimento->latitude?>" />
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                   <div class="formulario-colunado">
                        <label class="obrigatorio">Longitude</label>
                        <div>
                        	<input type="text" name="end_longitude" class="campo-padrao" id="FormLongitude" value="<?=@$empreendimento->longitude?>" />
                        </div>                   
	                </div>
	                <br class="clr" />
                    <br/>
                     <div class="formulario-colunado">
                        <label class="obrigatorio">Endereço empreendimento</label>
                        <div>
                        	<input type="text" name="end_empreendimento" class="campo-padrao" id="FormEnderecoEmpreendimento" value="<?=@$empreendimento->endereco_empreendimento?>" />
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                   <div class="formulario-colunado">
                        <label class="obrigatorio">Endereço Central de vendas</label>
                        <div>
                        	<input type="text" name="end_central_vendas" class="campo-padrao" id="FormEnderecoCentral" value="<?=@$empreendimento->endereco_central_vendas?>" />
                        </div>                   
	                </div>
	                <br class="clr" />
                    <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Telefone Central de vendas</label>
                        <div>
                        	<input type="text" name="tel_central_vendas" class="campo-padrao" id="FormTelCentral" value="<?=@$empreendimento->telefone_central_vendas?>" />
                        </div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Horário de atendimento do Plantão de vendas</label>
                        <div><textarea name="horarioAtendimento" class="campo-padrao" id="FormHorarioAtendimento" ><?=@$empreendimento->horario_atendimento?></textarea></div>
                    </div>
                     <br class="clr" />
                     <div class="formulario-colunado">
                     	<label>Endereço do decorado</label>
                     	<div><textarea name="decorado" id="FormDecorado" class="campo-padrao"><?=@$empreendimento->decorado?></textarea></div>
                     </div>
                     <br class="clr" />
                    <br/>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Porcentagem do Estágio das Obras</label>
                        <div>
                        	<input type="text" name="porcentagem_obras" class="campo-padrao" id="FormPorcentagemObras" value="<?=@$empreendimento->porcentagem_obras?>" />
                        </div>
                    </div>
                    <br class="clr" />
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/empreendimentos/lista" title="Voltar" class="bt-voltar">Voltar para Empreendimentos</a>
			<br/><br/><br/>
			
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>