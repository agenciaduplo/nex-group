<table id="ListaItensFasesAjax" class="tabela-padrao" style="width:515px; margin-left:18px;">
                <? if (@$itensFases):?>
                <thead>
                    <tr>
                     	<th width="20%">Ordem</th>
                        <th width="35%">Título</th>
                        <th width="30%">Fase</th>
                        <th width="30%">Porcentagem</th>
                        <th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody > 
                <? $count = 1; ?>
                    <? foreach ($itensFases as $row):?>
                    <? $count++; ?>
                     <?php 
	                    if(@$faseAnterior != @$row->id_fase && $count != 2 ){?>
			                   <tr class="par">
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   </tr>
			                   <tr class="par">
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   		<td>&nbsp;</td>
			                   </tr>
	                    <?php } ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                    	<td  nowrap="nowrap">
                    		<a href="javascript: desceItemFase('<?=$row->id_item_fase?>','<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    		&nbsp;
                    	<a href="javascript: sobeItemFase('<?=$row->id_item_fase?>','<?=$row->id_fase?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                      </td>
                      <td width="30%"><?=$row->titulo?></td>
                      <td width="30%"><?=$row->fase?></td>
                      <td width="30%"><?=$row->porcentagem?></td>
                        <td nowrap="nowrap">
                        <a href="javascript: getItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <a href="javascript: delItemFase('<?=$row->id_item_fase?>', '<?=$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->id_item_fase?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <tr><td colspan="6" class="editar"><div class="edita-<?=$row->id_item_fase?>"></div></td></tr>
                    <?php $faseAnterior = $row->id_fase;?>
                    <? endforeach; ?>
                </tbody>
            
            <br/>
            <? else: ?>
            <span style="margin-left:20px;">Nenhum Item das Fases encontrado.<br/></span>
            <? endif; ?>
            </table>
        