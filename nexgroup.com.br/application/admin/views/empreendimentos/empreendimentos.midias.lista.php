<div id="ListaMidias" >
	             		<? if(@$empreendimentosFotos) { ?>
	               		<? foreach ($empreendimentosFotos as $row): ?>  
	                 	<label class="obrigatorio">LISTA <?php echo $row->tipo?> </label>
	                	<table class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                    	<th width="2%">Ordem</th>
			                        <th width="20%">Foto</th>
			                        <th width="35%">Legenda</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
	                		<?php $this->load->model('empreendimentos_model', 'model', TRUE); 
	                		$fotos = $this->model->getFotos($empreendimento->id_empreendimento,$row->id_midia);?>
	                		<tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($fotos as $row2): ?>          
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                      <td  nowrap="nowrap">
                    					<a href="javascript: desceMidias('<?=$row2->id_midia?>','<?=$row2->id_tipo_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeMidias('<?=$row2->id_midia?>','<?=$row2->id_tipo_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    			  </td>
			                      <td width="20%">
										<?php if($row2->tipo_arquivo == 'I'){?>
			        							<a href="<?=base_url()?>uploads/imoveis/midias/<?=$row2->arquivo?>" rel="shadowbox[teste]" class="border"><img width="50px" border="0" src="<?=base_url()?>uploads/imoveis/midias/thumbs/<?=$row2->arquivo?>"></a>
			        					<?php }else if ($row2->tipo_arquivo == 'L'){
			        							echo "<a target='_blank' href='".$row2->arquivo."'>".$row2->arquivo."</a>";
			        						  }else{?>
			        								<img width="50px" alt="<?=$row2->legenda?>" src="http://img.youtube.com/vi/<?=$row2->arquivo?>/1.jpg">
			        					<?php }?>
			                      <td width="30%" class="LegendaMidia-<?=$row2->id_midia?>"><?=$row2->legenda?><br/></td>
			                      <td nowrap="nowrap">
			                      	<a href="javascript: getLegendaMidia('<?=$row2->id_midia?>', '<?=$empreendimento->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                      	<a href="javascript: delMidia('<?=$row2->id_midia?>','<?=@$empreendimento->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a foto?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                      </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
		            	</table>
		            	<br class="clr">
		            <? endforeach; ?>
		            <? }else{ ?>
		            	<span style="margin-left:20px;">Nenhuma Foto encontrada.<br/></span>
		            <? } ?>
		            </div>