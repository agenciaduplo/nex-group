<script type="text/javascript">
// $(document).ready(function() {	
// 	$("#data_atualizacao").datepicker({
//     	yearRange: '-100:+3',
//         showOn: "button",
//         buttonImage: "http://localhost/nex-site/assets/admin/img/ico-calendario.gif",
//         buttonImageOnly: true
//     });
// });
</script>

<h2>Fases</h2>
<br/><br/>
<!--<form id="FormItens" action="<?=site_url()?>/empreendimentos/salvarFases/" method="post"> -->
<input type="hidden" name="id_empreendimento" id="FormId" value="<?=$empreendimento->id_empreendimento?>">

<? if(@$fases->id_fase): ?>
<input type="hidden" name="id_fase" value="<?=$fases->id_fase?>" id="FormFaseId">
<? endif; ?>

<div class="formulario-colunado">
	<label class="obrigatorio">Título</label>
	<div><input type="text" name="titulo" class="campo-padrao" id="FormTituloFase" value="<?=@$fases->titulo?>" /></div>
</div>

<div class="formulario-colunado-div">&nbsp;</div>

<div class="formulario-colunado">
	<label class="obrigatorio">Status</label>
	<div>
		<select name="status" class="campo-padrao" id="FormStatus">
			<option value=""> -- Escolha -- </option>
			<?php foreach($status as $row):?>
			<option class="" value="<?php echo $row->id_status?>" <?php if($row->id_status==@$fases->id_status){echo 'selected="selected"';}?>><?php echo $row->status ?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>

<br class="clr" />

<div class="formulario-colunado">
	<label class="obrigatorio">Mostrar Fase</label>
	<div>
		<select name="mostrarFase" class="campo-padrao" id="FormMostrar">
			<option <? if(@$fases->mostrar_fase == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
			<option <? if(@$fases->mostrar_fase == 'N') echo "selected='selected'"; ?> value="N">Não</option>
		</select>
	</div>
</div>

<div class="formulario-colunado-div">&nbsp;</div>	

<div class="formulario-colunado">
	<label class="obrigatorio">Porcentagem</label>
	<div><input type="text" name="porcentagem_fase" class="campo-padrao" id="FormPorcentagemFase" value="<?=@$fases->porcentagem_fase?>" /></div>
</div>

<br class="clr" />

<div class="a-right" style="margin-right:10px;">
	<a href="javascript: enviaFase();"><b>Atualizar</b></a> ou 
	<a href="javascript: limpaCamposFases('<?=@$empreendimento->id_empreendimento?>');" ><b>Adicionar novo</b></a>
</div>                
<br/>
<!-- </form> -->