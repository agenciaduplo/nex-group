             <? if (@$itens):?>
			            <table id="listaItens" class="tabela-padrao" style="width:515px; margin-left:18px;">
			                <thead>
			                    <tr>
			                        <th width="2%">Ordem</th>
			                        <th width="35%">Título</th>
			                        <th width="5%">Ações</th>
			                    </tr>
			                </thead>
			                <tbody > 
			                	<? $count = 1; ?>
			                    <? foreach ($itens as $row):?>
			                    <? $count++; ?>
			                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
			                    	<td  nowrap="nowrap">
                    					<a href="javascript: desceItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Desce" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/desce.gif"></a>
                    					&nbsp;
                    					<a href="javascript: sobeItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Sobe" class="bt-excluir"><img src="<?=base_url()?>assets/admin/img/sobe.gif"></a>
                    				</td>
			                        <td width="35%"><?=$row->item?></td>
			                        <td nowrap="nowrap">
			                        	<a href="javascript: getItem('<?=$row->id_item?>','<?=@$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>
			                        	<a href="javascript: delItem('<?=$row->id_item?>', '<?=@$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=@$row->item;?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
			                        </td>
			                    </tr>
			                    <? endforeach; ?>
			                </tbody>
			            </table>
			            <br/>
		            <? else: ?>
		            	<span style="margin-left:20px;">Nenhum Item encontrado.<br/></span>
		            <? endif; ?><br/>