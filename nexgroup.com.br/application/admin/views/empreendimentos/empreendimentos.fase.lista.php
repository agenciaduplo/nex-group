<? if(@$fases) : ?>
<table id="listaFases" class="tabela-padrao" style="width:515px; margin-left:18px;">
    <thead>
        <tr>
            <th width="35%">Título</th>
            <th width="20%">Status</th>
            <th width="20%">Mostrar Fase</th>
            <th width="15%">Porcentagem</th>
            <th width="5%">Ações</th>
        </tr>
    </thead>
    <tbody > 
        <?
        $count = 1; 
        foreach($fases as $row) :
        $count++;
        ?>
        <tr class="<?=($count % 2) ? "par" : "impar"?>">
            <td width="30%"><?=$row->titulo?></td>
            <td width="20%"><?=$row->status?></td>
            <td width="20%"><? if($row->mostrar_fase == 'S') echo 'Sim'; else echo 'Não'?></td>
            <td width="15%"><?=$row->porcentagem_fase?>%</td>
            <td nowrap="nowrap">
                <a href="javascript: getFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                <a href="javascript: delFase('<?=$row->id_fase?>', '<?=$row->id_empreendimento?>');" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->titulo?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
            </td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
<br/>
<? else : ?>
<span style="margin-left:20px;">Nenhuma Fase encontrado.<br/></span>
<? endif; ?>
<br/>