<?=$this->load->view('includes/topo');?>

	<div id="nome"><a href="<?=base_url()?>assets/admin/media/nome.swf?nome=RESPONSABILIDADES SOCIAL . LISTA" class="FlashTitulo"></a></div>
		<div class="cadastros"><?=$cadastrados?></div>
		<div class="botoes">
			<a href="<?=site_url()?>/responsabilidades_social/cadastro/" title="Inserir">
				<img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" />
			</a>
		</div>
		<br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

			<div class="base-mg">

            <? if ($responsabilidades_social):?>
            <table class="tabela-padrao">
                <thead>
                    <tr> 
                        <th width="35%">Nome</th>
                        <td width="5%">Data</td>
                        <td width="5%">Ativo</td>
                        <td width="5%">Imagens</td>
                        <th width="10%">Ações</th>                       
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($responsabilidades_social as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                    	
                        <td width="85%"><a href="<?=site_url()?>/responsabilidades_social/editar/<?=@$row->id_responsabilidade?>" title="Editar" class="bt-editar"><?=$row->nome?></a></td>
                        <td width=""><a href="<?=site_url()?>/responsabilidades_social/editar/<?=@$row->id_responsabilidade?>" title="Editar" class="bt-editar"><?=date('d/m/Y',strtotime($row->data_cadastro))?></a></td>
                        <td width=""><? if(@$row->ativo == 'S') { echo "Sim"; } else { echo "Não"; } ?></td>
                        <td width="5%"><a href="<?=site_url()?>/responsabilidades_social/imagens/<?=$row->id_responsabilidade?>" alt="Editar Imagens"><img src="<?=base_url()?>assets/admin/img/plus.png"></a></td>
						<td nowrap="nowrap">
                        <a href="<?=site_url()?>/responsabilidades_social/editar/<?=$row->id_responsabilidade?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <a href="<?=site_url()?>/responsabilidades_social/apagar/<?=$row->id_responsabilidade?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->nome?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma responsabilidade social encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>