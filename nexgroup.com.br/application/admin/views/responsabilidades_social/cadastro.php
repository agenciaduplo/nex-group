<?=$this->load->view('includes/topo');?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		     				
		$("#FormDescricao").cleditor(); 
  	});

</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a
	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=RESPONSABILIDADE SOCIAL . CADASTRO"
	class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaMemorial')?></span>

<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<h2>DADOS DA RESPONSABILIDADE SOCIAL</h2>
		<br />
		<br />
		<form id="FormResponsabilidade" action="<?=site_url()?>/responsabilidades_social/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$responsabilidade->id_responsabilidade): ?>
				<input type="hidden" name="id_responsabilidade" value="<?=@$responsabilidade->id_responsabilidade?>" /> 
			<? endif; ?>
			<div class="formulario-colunado">
				<label class="obrigatorio">Nome</label>
				<div>
					<input type="text" name="nome" class="campo-padrao" id="FormNome" value="<?=@$responsabilidade->nome?>" />
				</div>
			</div>
			<div class="formulario-colunado">
				<label class="obrigatorio">Imagem Destaque (.JPG, .PNG, .GIF)</label>
				<div>
					<input type="file" name="imagem" id="FormImagem">
				</div>
				<? if(@$responsabilidade->imagem_destaque): ?> <br />
					<img src="<?=base_url()?>uploads/responsabilidades_social/zoom/<?=$responsabilidade->imagem_destaque?>"	width="100px;"> <br />
					<br />
				<? endif; ?>
			</div>
			<br class="clr" />
			<div class="formulario-colunado">
				<label class="obrigatorio">Ativo</label>
				<div class="formulario-colunado">
					<div>
						<select name="ativo" class="campo-padrao" id="FormAtivo">
							<option <? if(@$responsabilidade->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
							<option <? if(@$responsabilidade->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="formulario-colunado">
				<label class="obrigatorio">Data</label>
				<div class="formulario-colunado">
					<div>
						<input id="data_cadastro" type="text" name="data_cadastro" value="<?if(@$responsabilidade->data_cadastro) echo date('d/m/Y H:i:s', strtotime($responsabilidade->data_cadastro))?>" /> 
					</div>
				</div>
			</div>

			<br class="clr" />

			<div class="formulario-colunado">
				<label class="obrigatorio">Ano</label>
				<div>
					<input type="text" name="ano" class="campo-padrao" id="FormAno" value="<?=@$responsabilidade->ano?>" maxlength="4" />
				</div>
			</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Ordem</label>
				<div>
					<input type="text" name="ordem" class="campo-padrao" id="FormOrdem" value="<?=@$responsabilidade->ordem?>" />
				</div>
			</div>

			<br class="clr" />
			<br class="clr" />

			<div class="formulario">
				<label class="obrigatorio">Descrição</label>
				<div>
					<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$responsabilidade->descricao?></textarea>
				</div>
			</div>
			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>
			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			|<a href="<?=site_url()?>/responsabilidades_social/lista" title="Voltar" class="bt-voltar">Voltar para lista</a>
			<br />
			<br />
			<br />
		</form>
	</div>
</div>
		
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>