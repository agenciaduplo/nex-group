<?=$this->load->view('includes/topo');?>

	<script type="text/javascript" charset="utf-8">
	$(function() {
	    
	    $("input#FormImagem").filestyle({ 
	        image: "<?=base_url()?>assets/admin/img/file.gif",
	        imageheight : 16,
	        imagewidth : 47,
	        width : 150
	    }); 
	});
	</script>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=RESPONSABILIDADE SOCIAL . CADASTRO DE IMAGENS" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>
            <span class="txt-destaque"><?=$this->session->flashdata('respostaItens')?><br/></span>
           

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<div class="formulario-mg" id="ItensAjax">
                	<h2>Nova Imagem</h2>
                	<br/>
		                <div class="formulario-colunado">
 						<form id="FormImagens" action="<?=site_url()?>/responsabilidades_social/setImagem/" method="post" enctype="multipart/form-data">
 							<input type="hidden" name="id_responsabilidade" value="<?=@$id_responsabilidade?>" />
 							<label class="obrigatorio">Imagem</label>
                        	<div>
                        		<input type="file" name="imagem" class="campo-padrao" id="FormImagem" />
                        	</div>
                        	<label class="obrigatorio">Legenda</label>
                        	<div>
                        		<input type="text" name="legenda" class="campo-padrao" id="FormLegenda" />
                        	</div>
                        	
                        	<div class="a-right" style="margin-right:10px;">	
                        		<input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
                        	</div>
                        	<br class="clr" />
                        	<br/>		               
		                </form>
		                </div>
		                
		                <div class="formulario-colunado-div">&nbsp;</div>
		                <?php if($imagens):?>
		                <div class="formulario-colunado" id="ListaItensAjax">
	                		<table border="0" width="500px;" >
	                		<tr>
	                			<td colspan="4"><h3>IMAGENS</h3></td>
	                		</tr>
	                		<tr>
	                			<?php $cont = 0; ?>
			                	<? foreach ($imagens as $item) { ?>
			                		<td width="200px;" align="center" style="margin-right:15px;">
			                			<img src="<?=base_url()?>uploads/responsabilidades_social/galeria/<?=$item->foto?>" width="100px" />
				                		<br><?=$item->legenda?><br/>
				                		<a href="<?=site_url()?>/responsabilidades_social/delImagem/<?=$item->id_foto?>/<?=$item->id_responsabilidade?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a imagem?');">
				                			<img src="<?=base_url()?>assets/admin/img/delete.gif">
				                		</a>
			                		</td>
			                	<? }?>
			                </tr>
		                	</table>
		                </div>
		                <br class="clr" />	                
	                	<br/>
		                <?php endif;?>
                </div>              
				<br class="clr" />	
                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/responsabilidades_social/lista" title="Voltar" class="bt-voltar">Voltar para lista</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>