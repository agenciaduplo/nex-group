<?=$this->load->view('includes/topo');
switch($terreno->id_empresa) {
	case '1' : $link = "http://www.capa.com.br/"; break;
	case '2' : $link = "http://www.eglengenharia.com.br/"; break;
	case '3' : $link = "http://www.dhz.com.br/"; break;
	case '4' : $link = "http://www.lomandoaita.com.br/"; break;
	default: $link = "http://www.nexgroup.com.br/"; break;
}
$link = $link . 'uploads/terrenos/' . @$terreno->imagem;
?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TERRENOS  . VISUALIZAR" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormClientes" action="" method="post">
                    <?
                    	$data = date('d/m/Y H:i:s', strtotime($terreno->data_envio));
					?>
					<table width="400" border="0">
					  <tr>
					    <td colspan="2">Enviado por <label><strong><?=@$terreno->nome ?> </strong>, em <strong><?=$data?></strong></strong></label></td>
					  </tr>
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>E-mail</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->email?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Telefone</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->telefone?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Celular</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->celular?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Cidade</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->cidade_contato?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Bairro</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->bairro_contato?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Endereço</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->endereco_contato?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Cep</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->cep?></label></td>
					  </tr>
					  
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  
					  <tr>
					    <td width="30%"><label>Área</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->area?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Frente</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->frente?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Fundos</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->fundos?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Zoneamento</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->zoneamento?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Topografia</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->topografia?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Valor</label></td>
					    <td><label class="obrigatorio">R$ <?=@$terreno->valor?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Cidade</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->Cidade_terreno?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Bairro</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->bairro_terreno?></label></td>
					  </tr>
					   <tr>
					    <td width="30%"><label>Endereço</label></td>
					    <td><label class="obrigatorio"><?=@$terreno->endereco_terreno?></label></td>
					  </tr>	
					  <tr>
					    <td width="30%"><label>Foto</label></td>
					    <td><label class="obrigatorio"><a title='Baixar Arquivo' href='<?=$link; ?>'><?=@$terreno->imagem?></a></label></td>
					  </tr>							  
					  <tr>
					    <td width="30%" valign="top"><label>Observações</label></td>
					    <td valign="top"><label class="obrigatorio"><?=@$terreno->osservacoes?></label></td>
					  </tr>				  					  					  
					</table>                                   	
                    	
                    <br class="clr" />
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/terrenos/lista" title="Voltar" class="bt-voltar">voltar para Terrenos</a>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>