<?=$this->load->view('includes/topo');?>

<script
	type="text/javascript"
	src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
	   	
	   	
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		     				
		$("#FormDescricao").cleditor(); 
	 
             
  });

</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a
	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=CLIPPINGS . CADASTRO"
	class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaMemorial')?></span>
<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<h2>DADOS DO CLIPPING</h2>
		<br />
		<br />
		<form id="FormClippings" action="<?=site_url()?>/clippings/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$clipping->id_clipping): ?>
				<input type="hidden" name="id_clipping" value="<?=@$clipping->id_clipping?>" /> 
			<? endif; ?>
			<div class="formulario-colunado">
				<label class="obrigatorio">Título</label>
				<div>
					<input type="text" name="titulo" class="campo-padrao" id="FormClippinggg" value="<?=@$clipping->titulo?>" />
				</div>
			</div>
			<div class="formulario-colunado">
				<label class="obrigatorio">Imagem Destaque (.JPG, .PNG, .GIF)</label>
				<div>
					<input type="file" name="imagem" id="FormImagem">
				</div>
				<? if(@$clipping->imagem_destaque): ?> <br />
					<img src="<?=base_url()?>uploads/clippings/zoom/<?=$clipping->imagem_destaque?>" width="100px;"> <br />
					<br />
				<? endif; ?>
			</div>
			<br class="clr" />

			<div class="formulario-colunado">
				<label class="obrigatorio">Arquivo (.PDF ou .DOC ou Imagem)</label>
				<div>
					<input type="file" name="arquivo" id="FormArquivo">
				</div>
				<? if(@$clipping->arquivo): ?> <br />
					<a target="_blank" href="<?=base_url()?>uploads/clippings/arquivos/<?=$clipping->arquivo?>"><?=$clipping->arquivo?></a> <br />
					<br />
				<? endif; ?><br/><br/>
			</div>
			<div class="formulario-colunado">
				&nbsp;
			</div>
			<br class="clr" />


			<div class="formulario-colunado">
				<label class="obrigatorio">Ativo</label>
				<div class="formulario-colunado">
					<div>
						<select name="ativo" class="campo-padrao" id="FormAtivo">
							<option <? if(@$clipping->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
							<option <? if(@$clipping->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="formulario-colunado">
				<label class="obrigatorio">Data</label>
				<div class="formulario-colunado">
					<div>
						<input id="data_cadastro" type="text" name="data_cadastro" value="<?if(@$clipping->data_cadastro) echo date('d/m/Y',strtotime($clipping->data_cadastro))?>" /> 
					</div>
				</div>
			</div>
			<br class="clr" />
			<br class="clr" />
			<div class="formulario">
				<label class="obrigatorio">Descrição</label>
				<div>
					<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$clipping->descricao?></textarea>
				</div>
			</div>
			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>
			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			|<a href="<?=site_url()?>/clippings/lista" title="Voltar" class="bt-voltar">Voltar para Clippings</a>
			<br />
			<br />
			<br />
		</form>
	</div>
</div>
		
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>