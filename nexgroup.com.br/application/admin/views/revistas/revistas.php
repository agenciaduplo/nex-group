<?=$this->load->view('includes/topo');?>

	<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=REVISTAS . LISTA" class="FlashTitulo"></a></div>
		<div class="cadastros"><?=$cadastrados?></div>
		<div class="botoes">
			<a href="<?=site_url()?>/revistas/cadastro/" title="Inserir">
				<img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" />
			</a>
		</div>
		<br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

			<div class="base-mg">

            <? if ($revistas):?>
            <table class="tabela-padrao">
                <thead>
                    <tr> 
                    	<th width="35%">Título</th>
                        <th width="35%">Edição</th>
                        <td width="5%">Data</td>
                        <th width="10%">Ações</th>                       
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($revistas as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                    	
                        <td width="85%"><a href="<?=site_url()?>/revistas/editar/<?=@$row->id_revista?>" title="Editar" class="bt-editar"><?=$row->titulo?></a></td>
                        <td width=""><a href="<?=site_url()?>/revistas/editar/<?=@$row->id_revista?>" title="Editar" class="bt-editar">Edição <?=$row->edicao?></a></td>
                        <td width=""><a href="<?=site_url()?>/revistas/editar/<?=@$row->id_revista?>" title="Editar" class="bt-editar"><?=date('d/m/Y',strtotime($row->data_cadastro))?></a></td>
						<td nowrap="nowrap">
                        <a href="<?=site_url()?>/revistas/editar/<?=$row->id_revista?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <a href="<?=site_url()?>/revistas/apagar/<?=$row->id_revista?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a <?=$row->titulo?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma Revista encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>