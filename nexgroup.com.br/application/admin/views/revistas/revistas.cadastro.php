<?=$this->load->view('includes/topo');?>

<script
	type="text/javascript"
	src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
	   	
	   	
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

		$("input#FormArquivo").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		      
		$('#FormTitle').jqEasyCounter({
			'maxChars': 90,
			'maxCharsWarning': 80,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormKeywords').jqEasyCounter({
			'maxChars': 200,
			'maxCharsWarning': 180,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		});
		
		$('#FormDescription').jqEasyCounter({
			'maxChars': 250,
			'maxCharsWarning': 230,
			'msgFontSize': '11px',
			'msgFontColor': '#000',
			'msgFontFamily': 'Arial',
			'msgTextAlign': 'right',
			'msgWarningColor': '#F00',
			'msgAppendMethod': 'insertAfter'              
		}); 
		
		$("#FormDescricao").cleditor(); 
		$("#FormExpediente").cleditor();   
             
  });

</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a
	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=REVISTAS . CADASTRO"
	class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<h2>DADOS DA NOTÍCIA</h2>
		<br />
		<br />
		<form id="FormnRevistas" action="<?=site_url()?>/revistas/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$revista->id_revista): ?>
				<input type="hidden" name="id_revista" value="<?=@$revista->id_revista?>" /> 
			<? endif; ?>
			<div class="formulario-colunado">
				<label class="obrigatorio">Título</label>
				<div>
					<input type="text" name="titulo" class="campo-padrao" id="FormTitulo" value="<?=@$revista->titulo?>" />
				</div>
			</div>
			<div class="formulario-colunado">
				<label class="obrigatorio">Edição</label>
				<div>
					<input type="text" name="edicao" class="campo-padrao" id="FormEdicao" value="<?=@$revista->edicao?>" />
				</div>
			</div>
			<br class="clr" />
			<div class="formulario-colunado">
				<label class="obrigatorio">Capa revista (.JPG, .PNG, .GIF)</label>
				<div>
					<input type="file" name="imagem" id="FormImagem">
				</div>
				<? if(@$revista->capa): ?> <br />
					<img src="<?=base_url()?>uploads/revista/capa/<?=$revista->capa?>"	width="100px;"> <br />
					<br />
				<? endif; ?>
			</div>
			
			<div class="formulario-colunado">
				<label class="obrigatorio">Revista (.PDF)</label>
				<div>
					<input type="file" name="arquivo" id="FormArquivo">
				</div>
			</div>
			<div class="formulario-colunado">
				<label class="obrigatorio">Data</label>
				<div class="formulario-colunado">
					<div>
						<input id="data_cadastro" type="text" name="data_cadastro" value="<?if(@$revista->data_cadastro) echo date('d/m/Y',strtotime($revista->data_cadastro))?>" /> 
					</div>
				</div>
			</div>
			<br class="clr" />
			<br class="clr" />
			<div class="formulario">
				<label>Endereço do PageFlip:</label>
				<input id="pageflip" type="text" name="pageflip" value="<?=@$revista->pageflip?>" style="width: 515px" /> 
			</div>
			<br class="clr" />
			<div class="formulario">
				<label class="obrigatorio">Descrição</label>
				<div>
					<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$revista->descricao?></textarea>
				</div>
			</div>
			<br class="clr" />
			<br class="clr" />
			<div class="formulario">
				<label class="obrigatorio">Expediente</label>
				<div>
					<textarea name="expediente" class="campo-grande" id="FormExpediente" /><?=@$revista->expediente?></textarea>
				</div>
			</div>
			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>
			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			|<a href="<?=site_url()?>/revistas/lista" title="Voltar" class="bt-voltar">Voltar para Revistas</a>
			<br />
			<br />
			<br />
		</form>
	</div>
</div>
		
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>