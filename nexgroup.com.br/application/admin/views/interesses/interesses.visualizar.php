<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=INTERESSE . VISUALIZAR" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
					
                    <form id="FormClientes" action="" method="post">
                    <?
                    	$data = date('d/m/Y H:i:s', strtotime($interesse->data_envio));
					?>  

					<table width="400" border="0">
					  <tr>
					    <td colspan="2">Enviado por <label><strong><?=@$interesse->nome?></strong>, em <strong><?=$data?></strong></label></td>
					  </tr>
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>E-mail</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->email?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Telefone</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->telefone?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Empreendimento</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->empreendimento?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Bairro/Cidade</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->bairro_cidade?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Faixa Etária</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->faixa_etaria?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Estado civil</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->estado_civil?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Profissão</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->profissao?></label></td>
					  </tr>
					  <tr>
					   <td width="30%"><label>Forma de contato</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->forma_contato?></label></td>
					  </tr>						  
					  <tr>
					    <td width="30%" valign="top"><label>Mensagem</label></td>
					    <td valign="top"><label class="obrigatorio"><?=@$interesse->comentarios?></label></td>
					  </tr>	
					  <tr>
					    <td width="30%"><label>Origem</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->origem?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>URL</label></td>
					    <td><label class="obrigatorio"><br/><?=@$interesse->url?><br/><br/></label></td>
					  </tr>			  					  					  
					  <tr>
					    <td width="30%"><label>IP</label></td>
					    <td><label class="obrigatorio"><?=@$interesse->ip?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>User Agent</label></td>
					    <td><label class="obrigatorio"><br/><?=@$interesse->user_agent?><br/><br/></label></td>
					  </tr>			  					  					  
					</table>                                   	
                    	
                    <br class="clr" />
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/interesses/lista" title="Voltar" class="bt-voltar">voltar para Interesses</a>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>