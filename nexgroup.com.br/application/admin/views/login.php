<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  	<title>Sistema de Gerenciamento de Conteúdo (CMS) - Divex</title>

	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-1.5.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.media.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.maskedinput.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.rsv.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.ui.datepicker.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.filestyle.mini.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/functions.js"></script>
	
	<style type="text/css">
		<!--
		@import url("<?=base_url()?>assets/admin/css/default.css");
		-->
	</style>
	
	<script type="text/javascript">
		$().ready(function(){
			//$("select.nice").niceSelect();

			var nc			= $(window).height() - 284;
			var $conteudo	= $("#miolo");
			if($conteudo.height() < nc) $conteudo.css({'min-height': nc + 'px'});
			if($.browser.msie && $.browser.version < 7) $conteudo.css({'height': nc + 'px'});
		});
		
		function mostraRecuperaSenha()
		{
			$('#rsvErrors').hide('slow');
			$('.cadastroCorretor').show('slow');
 			$('.loginCorretor').hide('slow');
 			
		}
		
		function mostraLogin()
		{
			$('#rsvErrors').hide('slow');
			$('.cadastroCorretor').hide('slow');
 			$('.loginCorretor').show('slow');
 			
		}
	</script>		

</head>

<body>

	<div id="topo">
	    <div class="centralizacao">
	        <div id="logo"><a href="<?=base_url()?>admin.php"><h1>Divex</h1></a></div>
	        <br class="clr" />
	    </div>
	</div>
	
	<div id="conceitual"><div id="conceitual-mg"></div></div>
	
	<div id="miolo">
		
		<div id="logon-box">
			<div id="rsvErrors"></div>		
		</div>
	    <div id="logon-box" class="loginCorretor">
	        <div id="titulo"><a href="media/titulo.swf?titulo=LOGIN" class="FlashTitulo"></a></div>
	        	
	        <div class="formulario">
	            <div class="formulario-topo">&nbsp;</div>
	            <div class="formulario-mg">
	
	                <form id="FormLogon" action="<?=site_url()?>/login/entrar/" method="post">
	
		                <label class="obrigatorio">Usuário</label>
		                <div><input type="text" name="login" class="campo-padrao" id="FormUsuario" /></div>
		
		                <label class="obrigatorio">Senha</label>
		                <div><input type="password" name="senha" class="campo-padrao" id="FormSenha" /></div>
		
		                <div class="a-right">
		                	<input type="image" src="<?=base_url()?>assets/admin/img/bt-entrar.gif" alt="Entrar" title="Entrar" class="botao" id="FormEntrar" />
		                </div>
		                <div style="float:left; margin-top:-15px;">
		                	<!-- <a href="javascript: mostraRecuperaSenha();">Recuperar Senha</a>-->
		                </div>
	
	                </form>
	                
					<?php //echo $this->encrypt->encode('123'); ?>
	            </div>
	            <div class="formulario-base">&nbsp;</div>
	        </div>
			<span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span>
			 
	    </div> 
		<br class="clear">
	</div>
	
	<div id="rodapeHome">
		<div class="rodape-conceito">
			<p>
				<br/><strong>:: Sistema de Gerenciamento de Conteúdo</strong>
				<br/><strong>Nexgroup</strong>
				<br/>©‎ Desenvolvido por <a href="http://www.divex.com.br" target="_blank" style="text-decoration:underline">Divex</a>
			</p>
		</div>	
	</div>

</body>
</html>