<style>
.menupai {
	color: #000;
	text-decoration: underline;
	font-weight: bold;
}
</style>
<div id="menu-box" style="margin-left:20px;">
		<h2>MENU</h2>
		<br/>		 		
		<div>
		<ul>	
			<li class="menupai">A EMPRESA</li>
				<li><a href="<?=base_url()?>admin.php/responsabilidades_social">&nbsp;&nbsp;&nbsp;&nbsp;Responsabilidade Social (Creches)</a></li>
				<li><a href="<?=base_url()?>admin.php/obras_realizadas">&nbsp;&nbsp;&nbsp;&nbsp;Obras Realizadas</a></li>
			<li>-</li>

			<li class="menupai">NOTÍCIAS</li>
				<li><a href="<?=base_url()?>admin.php/noticias">&nbsp;&nbsp;&nbsp;&nbsp;Notícias</a></li>
				<li><a href="<?=base_url()?>admin.php/clippings">&nbsp;&nbsp;&nbsp;&nbsp;Clippings</a></li>
				<li><a href="<?=base_url()?>admin.php/noticias_destaques">&nbsp;&nbsp;&nbsp;&nbsp;Notícias Destaques</a></li>
			<li>-</li>

			<li class="menupai">REVISTAS</li>
				<li><a href="<?=base_url()?>admin.php/revistas">&nbsp;&nbsp;&nbsp;&nbsp;Revistas</a></li>
			<li>-</li>

			<li class="menupai">CENTRAL DE VENDAS</li>
				<li><a href="<?=base_url()?>admin.php/atendimentos">&nbsp;&nbsp;&nbsp;&nbsp;Atendimentos</a></li>
			<li>-</li>
			
			<li class="menupai">IMÓVEIS</li>
				<li><a href="<?=base_url()?>admin.php/empreendimentos">&nbsp;&nbsp;&nbsp;&nbsp;Imóveis</a></li>
				<li><a href="<?=base_url()?>admin.php/empreendimentos_destaques">&nbsp;&nbsp;&nbsp;&nbsp;Imóveis Destaques</a></li>
				<li><a href="<?=base_url()?>admin.php/tipos_imoveis">&nbsp;&nbsp;&nbsp;&nbsp;Tipos de Imóveis</a></li>
				<li><a href="<?=base_url()?>admin.php/status">&nbsp;&nbsp;&nbsp;&nbsp;Status</a></li>	
				<li><a href="<?=base_url()?>admin.php/faixas_precos">&nbsp;&nbsp;&nbsp;&nbsp;Preços</a></li>	
				<li><a href="<?=base_url()?>admin.php/dormitorios">&nbsp;&nbsp;&nbsp;&nbsp;Dormitórios</a></li>	
				<li><a href="<?=base_url()?>admin.php/banheiros">&nbsp;&nbsp;&nbsp;&nbsp;Banheiros</a></li>			
				<li><a href="<?=base_url()?>admin.php/emails">&nbsp;&nbsp;&nbsp;&nbsp;Emails</a></li>
			<li>-</li>	
			
			<li class="menupai">OFERTAS NEXVENDAS</li>
				<li><a href="<?=base_url()?>admin.php/ofertas">&nbsp;&nbsp;&nbsp;&nbsp;Ofertas</a></li>
				<li><a href="<?=base_url()?>admin.php/ofertas_tipo">&nbsp;&nbsp;&nbsp;&nbsp;Tipos de Ofertas</a></li>
			<li>-</li>	

			<li class="menupai">INTERESSES</li>
				<li><a href="<?=base_url()?>admin.php/interesses">&nbsp;&nbsp;&nbsp;&nbsp;Interesses</a></li>
				<li><a href="<?=base_url()?>admin.php/indicacoes">&nbsp;&nbsp;&nbsp;&nbsp;Indicações</a></li>		
			<li>-</li>

			<li class="menupai">CONTATOS</li>
				<li><a href="<?=base_url()?>admin.php/contatos">&nbsp;&nbsp;&nbsp;&nbsp;Contatos</a></li>
				<li><a href="<?=base_url()?>admin.php/terrenos">&nbsp;&nbsp;&nbsp;&nbsp;Terrenos</a></li>	
				<li><a href="<?=base_url()?>admin.php/jobs">&nbsp;&nbsp;&nbsp;&nbsp;Trabalhe Conosco</a></li>
				<li><a href="<?=base_url()?>admin.php/newsletter">&nbsp;&nbsp;&nbsp;&nbsp;Newsletter</a></li>	
				<li><a href="<?=base_url()?>admin.php/acessos">&nbsp;&nbsp;&nbsp;&nbsp;Acessos Corretor Online</a></li>
			<li>-</li>

			<li class="menupai">RELATÓRIOS</li>
				<li><a href="<?=base_url()?>admin.php/relatorios/atendimentos">&nbsp;&nbsp;&nbsp;&nbsp;Atendimentos</a></li>
				<li><a href="<?=base_url()?>admin.php/relatorios/contatos">&nbsp;&nbsp;&nbsp;&nbsp;Contatos</a></li>
				<li><a href="<?=base_url()?>admin.php/relatorios/interesses">&nbsp;&nbsp;&nbsp;&nbsp;Interesses</a></li>
				<li><a href="<?=base_url()?>admin.php/relatorios/corretor">&nbsp;&nbsp;&nbsp;&nbsp;Corretor</a></li>				
				<li><a href="<?=base_url()?>admin.php/relatorios/semanal">&nbsp;&nbsp;&nbsp;&nbsp;Semanal Atendimento/Interesse</a></li>
        		<li><a href="<?=base_url()?>admin.php/relatorios/formularios">&nbsp;&nbsp;&nbsp;&nbsp;Formulários</a></li>
				
				<?php if($_SERVER['REMOTE_ADDR'] == '187.36.24.109'): ?>					
					<li><a href="<?=base_url()?>admin.php/relatorios/mailling_cidade">&nbsp;&nbsp;&nbsp;&nbsp;Mailling Por Cidade</a></li>
				<?php endif; ?>
			<li>-</li>

			<li class="menupai">EXPORTAÇÕES</li>
				<li><a href="<?=base_url()?>admin.php/exportacao">&nbsp;&nbsp;&nbsp;&nbsp;Promoção Saint Louis</a></li>
			<li>-</li>

			<li class="menupai">SISTEMA</li>
				<li><a href="<?=base_url()?>admin.php/usuarios/lista">Usuários</a></li>
				<li><a href="<?=base_url()?>admin.php/logs/acoes">Log's de Ações</a></li>
				<li><a href="<?=base_url()?>admin.php/logs/acessos">Log's de Acesso</a></li>
			<li>-</li>
				<li><a href="<?=base_url()?>admin.php/secoes">Seções</a></li>
				<li><a href="<?=base_url()?>admin.php/conteudos">Conteúdos</a></li>
				<li><a href="<?=base_url()?>admin.php/tags">Tags</a></li>
			<li>-</li>
		</ul>
		</div>
</div>
<br/><br/>