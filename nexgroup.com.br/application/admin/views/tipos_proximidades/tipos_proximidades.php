<?=$this->load->view('includes/topo');?>


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TIPO PROXIMIDADE . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            
            	<div class="botoes"><a href="<?=site_url()?>/tipos_proximidades/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" /></a></div>
            
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($tipos_proximidades):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="45%">Tipo Proximidade</th>
                        <th width="45%">Data de Criação</th>
                        <th width="10%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($tipos_proximidades as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="45%"><?=$row->tipo_proximidade?></td>
                        <?php
                        	$data = date('d/m/Y H:i:s', strtotime($row->data_cadastro));
                        ?>
                        <td width="30%"><?=$data?></td>
                        <td nowrap="nowrap">
                        <a href="<?=site_url()?>/tipos_proximidades/editar/<?=$row->id_tipo_proximidade?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <a href="<?=site_url()?>/tipos_proximidades/apagar/<?=$row->id_tipo_proximidade?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->tipo_proximidade?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <?endforeach;?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhum Tipo Proximidade encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>