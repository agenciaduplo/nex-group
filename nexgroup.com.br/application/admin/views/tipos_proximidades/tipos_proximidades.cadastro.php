<?=$this->load->view('includes/topo');?>

    <script type="text/javascript">
      $(document).ready(function() {
        $("#FormConteudo").cleditor();
      });
    </script>

	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TIPO PROXIMIDADE . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormTipos_Proximidades" action="<?=site_url()?>/tipos_proximidades/salvar/" method="post" enctype="multipart/form-data">
 					<? if (@$tipo_proximidade->id_tipo_proximidade): ?>
                        <input type="hidden" name="id_tipo_proximidade" value="<?=@$tipo_proximidade->id_tipo_proximidade?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Tipo Proximidade</label>
                        <div><input type="text" name="tipo_proximidade" class="campo-padrao" id="FormTipoProximidade" value="<?=@$tipo_proximidade->tipo_proximidade?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    
                     <br class="clr" />
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Imagem pin google maps (.JPG, .PNG, .GIF)</label>
                        <div><input type="file" name="imagem_pin" id="FormPin" ></div>
                        <? if(@$tipo_proximidade->pin): ?>
                        	<br/>
                        	<img src="<?=base_url()?>uploads/tipos_proximidades/pin/<?=$tipo_proximidade->pin?>" width="100px;">
                        	<br/><br/><br/>
                        <? endif; ?>
                    </div>
                    
                    
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
	                    <div>&nbsp;</div>                        
                        
                    </div>
                    
                    <br class="clr" />
                   	<br/><br/>                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/tipos_proximidades/lista" title="Voltar" class="bt-voltar">voltar para Tipos Proximidades</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>