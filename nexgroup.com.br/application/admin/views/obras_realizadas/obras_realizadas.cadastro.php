<?=$this->load->view('includes/topo');?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/empreendimentos.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
		$("input#FormFachada").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

  });
</script>
	
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=OBRAS REALIZADAS . CADASTRO" class="FlashTitulo"></a></div>
                <div id="rsvErrors"></div>
                
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	
                	<h2>DADOS DO EMPREENDIMENTO</h2>
                	<br/><br/>

                	<form id="FormObrasRealizadas" action="<?=site_url()?>/obras_realizadas/salvar/" method="post" enctype="multipart/form-data">
	                	
	                	<input type="hidden" name="id_obra_realizada" value="<?=@$obra_realizada->id_obra_realizada?>">
	                    
	                    <div class="formulario-colunado">
	                        <label class="obrigatorio">Nome</label>
	                        <div><input type="text" name="empreendimento" class="campo-padrao" id="FormEmpreendimento" value="<?=@$obra_realizada->empreendimento?>" /></div>
	                    </div>
	                    
	                    <div class="formulario-colunado-div">&nbsp;</div>
	                    <div class="formulario-colunado">
		                    <label class="obrigatorio">Empresa</label>
		                    <div class="formulario-colunado">
		                         <div>
			                         <select name="empresas" class="campo-padrao" id="FormEmpresas">
			                         	<option value=""> -- Escolha -- </option>
		                        		<?php foreach($empresas as $row):?>
											<option value="<?php echo $row->id_empresa?>" <?php if($row->id_empresa == @$obra_realizada->id_empresa) {echo 'selected="selected"';}?>><?php echo $row->empresa?></option>
										<?php endforeach;?>
		                        	</select>
		                         </div>                
			               </div>
			          	</div>

	                    <div class="formulario-colunado">
	                        <label class="obrigatorio">Fachada (.JPG, .PNG, .GIF)</label>
	                        <div><input type="file" name="fachada" id="FormFachada" ></div>
	                        <? if(@$obra_realizada->fachada): ?>
	                        	<br/>
	                        	<img src="<?=base_url()?>uploads/obras_realizadas/<?=$obra_realizada->fachada?>" width="100px;">
	                        	<br/><br/><br/>
	                        <? endif; ?>
	                    </div>
	                    
	                    <div class="formulario-colunado-div">&nbsp;</div>
			           	<div class="formulario-colunado">
		                    <label class="obrigatorio">Estado</label>
		                    <div class="formulario-colunado">
		                         <div>
			                         <select name="estado" class="campo-padrao" id="FormEstado">
			                         <option value="" selected="selected"> -- Escolha -- </option>
		                        		<?php foreach($estados as $row):?>
											<option value="<?php echo $row->id_estado?>" <?php if($row->id_estado==@$obra_realizada->id_estado)echo 'selected="selected"'?>><?php echo $row->estado?></option>
										<?php endforeach;?>
		                        	</select>
		                         </div>                
			               </div>
			          	</div> 

		           		<br class="clr" />

						<div class="formulario-colunado">
							<label class="obrigatorio">Cidade</label>
							<div class="formulario-colunado">
								<div>
									<select name="cidade" class="campo-padrao" id="FormCidade">
										<?php if(@$cidades) { ?>
										<?php foreach($cidades as $row) : ?>
										<option value="<?php echo $row->id_cidade?>" <?php if($row->id_cidade==@$obra_realizada->id_cidade)echo 'selected="selected"'?>><?php echo $row->cidade?></option>
										<?php endforeach;?>
										<?php } else { ?>
										<option value="" selected="selected"> -- Escolha um Estado -- </option>
										<?php }?>
									</select>
								</div>
							</div>
						</div>

		               	<div class="formulario-colunado-div">&nbsp;</div>
		               	<div class="formulario-colunado">
		               		<label class="obrigatorio">Bairro</label>
		               		<div><input type="text" name="bairro" class="campo-padrao" id="FormBairro" value="<?=@$obra_realizada->bairro?>" /></div>
		               	</div>

		               	<br class="clr" />

		               	<div class="formulario-colunado">
	                        <label class="obrigatorio">Ordem</label>
	                        <div><input type="text" name="ordem" class="campo-padrao" id="FormOrdem" value="<?=@$obra_realizada->ordem?>" /></div>
	                    </div>

	                    <div class="formulario-colunado-div">&nbsp;</div>

                    	<div class="formulario-colunado">
	                        <label class="obrigatorio">Habita-se (mês/ano)</label>
	                        <div><input type="text" name="data_habite_se" class="campo-padrao" id="FormData_habite_se" value="<?=($obra_realizada->data_habite_se) ? date('m/Y', strtotime($obra_realizada->data_habite_se)) : NULL?>" /></div>
	                    </div>

		               	<br class="clr" />
		               	<br class="clr" />

		               	<div class="formulario-colunado">
		               		<label class="obrigatorio">Ativo</label>
		               		<div>
		               			<label><input type="radio" name="ativo" value="S" <?=@($obra_realizada->ativo == 'S') ? 'checked="checked"' : ''?>> SIM </label>
		               			<span>&nbsp;</span>
		               			<label><input type="radio" name="ativo" value="N" <?=@($obra_realizada->ativo == 'N') ? 'checked="checked"' : ''?>> NÃO </label>
		               		</div>
		               	</div>


	                	<br class="clr" />
	                 	<br class="clr" />
	                    <br/>

	                    <div class="formulario-colunado">
	                        <label class="obrigatorio">&nbsp;</label>
	                        <div>&nbsp;</div>
	                    </div>

	                    <br class="clr" />

						<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/obras_realizadas/lista" title="Voltar" class="bt-voltar">Voltar para Obras Realizadas</a>
			<br/><br/><br/>
			
	<!-- FIM CONTEUDO -->

<script type="text/javascript">

$('#FormData_habite_se').mask('99/9999');

</script>
<?=$this->load->view('includes/rodape');?>