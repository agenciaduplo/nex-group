<?=$this->load->view('includes/topo');?>

	<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . LISTA" class="FlashTitulo"></a></div>
		<div class="cadastros"><?=$cadastrados?></div>
		<div class="botoes">
			<a href="<?=site_url()?>/obras_realizadas/cadastro/" title="Inserir">
				<img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" />
			</a>
		</div>
		<br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

			<div class="base-mg">

            <? if ($obras_realizadas):?>
            <table class="tabela-padrao">
                <thead>
                    <tr> 
                        <th width="45%">Empreendimento</th>
                        <th width="15%">Fachada</th>
                        <td width="20%">Empresa</td>
                        <td width="20%">Cidade</td>
                        <td width="10%">Ativo</td>
                        <th width="10%">Ações</th>                       
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($obras_realizadas as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                    	
                        <td><a href="<?=site_url()?>/obras_realizadas/editar/<?=@$row->id_obra_realizada?>" title="Editar" class="bt-editar"><?=$row->empreendimento?></a></td>
                        <td>
                        	<a href="<?=site_url()?>/obras_realizadas/editar/<?=@$row->id_obra_realizada?>" title="Editar" class="bt-editar">
                        	<?php if(@$row->fachada): ?>
                        		<img src="<?=base_url()?>uploads/obras_realizadas/<?=$row->fachada?>" width="50px;">
                        	<? endif; ?>
                        	</a>
                        </td>
                        <td><a href="<?=site_url()?>/obras_realizadas/editar/<?=@$row->id_obra_realizada?>" title="Editar" class="bt-editar"><?=$row->empresa?></a></td>
                        <td><a href="<?=site_url()?>/obras_realizadas/editar/<?=@$row->id_obra_realizada?>" title="Editar" class="bt-editar"><?=$row->cidade?></a></td>
                        <td style='text-align: center;'><?=(@$row->ativo == 'S') ? 'SIM' : 'NÃO'?></td>
						<td nowrap="nowrap">
                        	<a href="<?=site_url()?>/obras_realizadas/editar/<?=$row->id_obra_realizada?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        	<!--  <a href="<?=site_url()?>/obras_realizadas/apagar/<?=$row->id_obra_realizada?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->empreendimento?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>-->
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma Obra realizada encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>