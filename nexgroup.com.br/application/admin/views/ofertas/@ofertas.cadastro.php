<?=$this->load->view('includes/topo');?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(function()
	{
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

	});
</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=OFERTAS . CADASTRO" class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaMemorial')?></span>

<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<? if (@$oferta->id_oferta): ?>
		<h2>DADOS DA OFERTA (ID <?=$oferta->id_oferta?>)</h2>
		<? else: ?>
		<h2>NOVA OFERTA</h2>
		<? endif; ?>
		
		<br /><br />

		<form id="FormOfertas" action="<?=site_url()?>/ofertas/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$oferta->id_oferta): ?>
			<input type="hidden" name="id_oferta" value="<?=@$oferta->id_oferta?>" /> 

			<div class="formulario">
				<label class="obrigatorio">Cadastrado Em</label>
				<div class="formulario-colunado">
					<input type="text" value="<?if(@$oferta->data_cadastro) echo date('d/m/Y H:i:s',strtotime($oferta->data_cadastro))?>" class="campo-padrao" readonly="readonly" />
				</div>
			</div>	

			<br class="clr" />

			<? endif; ?>

			<div class="formulario">
				<label class="obrigatorio">Tipo da oferta</label>
				<div class="formulario-colunado">
					<select name="id_tipo" class="campo-padrao" id="FormTipo">
						<?php 
						foreach ($ofetas_tipos as $row) {
							if($row->id_ofertas_tipo == @$oferta->id_ofertas_tipo)
								echo '<option value="' . $row->id_ofertas_tipo . '" selected="selected">' . $row->tipo . '</option>';
							else
								echo '<option value="' . $row->id_ofertas_tipo . '">' . $row->tipo . '</option>';
						}
						?>
					</select>
				</div>
			</div>

			<br class="clr" />	
			
			<div class="formulario">
				<label class="obrigatorio">Título</label>
				<div><input type="text" name="titulo" id="FormOferta" value="<?=@$oferta->titulo?>" style="width: 516px;" /></div>
			</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Valor</label>
				<div><input type="text" name="valor" value="<?=number_format(@$oferta->valor, 2, ',', '.')?>" class="money" /></div>
			</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Frase do valor</label>
				<div><input type="text" name="valor_frase" value="<?=@$oferta->valor_frase?>" class="campo-padrao" style="width: 266px;" /></div>
			</div>

			<br class="clr" />

			<div class="formulario-colunado">
				<label class="obrigatorio">Imagem Destaque (.JPG, .PNG, .GIF)</label>
				<div><input type="file" name="imagem" id="FormImagem"></div>
				<? if(@$oferta->img): ?><br />
				<img src="<?=base_url()?>uploads/ofertas/zoom/<?=$oferta->img?>" width="100px;"><br /><br />
				<? endif; ?>
			</div>
		
			<br class="clr" />
			<br class="clr" />

			<div class="formulario-colunado">
				<label class="obrigatorio">Ativo</label>
				<div class="formulario-colunado">
					<select name="ativo" class="campo-padrao" id="FormAtivo">
						<option <? if(@$oferta->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
						<option <? if(@$oferta->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
					</select>
				</div>
			</div>	

			<br class="clr" />
			<br class="clr" />

			<div class="formulario">
				<label class="obrigatorio">Descrição</label>
				<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$oferta->descricao?></textarea>
			</div>

			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>

			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			<a href="<?=site_url()?>/ofertas/lista" title="Voltar" class="bt-voltar">| Voltar para Ofertas</a>

			<br /><br /><br />
		</form>

	</div>
</div>

<!-- FIM CONTEUDO -->

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.money').mask('000.000.000.000.000,00', {reverse: true});
	});
</script>

<?=$this->load->view('includes/rodape');?>