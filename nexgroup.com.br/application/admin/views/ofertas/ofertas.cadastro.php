<?=$this->load->view('includes/topo');?>

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$(function()
	{
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

		$("input#FormPin").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});

	});
</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=OFERTAS . CADASTRO" class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaMemorial')?></span>

<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<? if (@$oferta->id_oferta): ?>
		<h2>DADOS DA OFERTA (ID <?=$oferta->id_oferta?>)</h2>
		<? else: ?>
		<h2>NOVA OFERTA</h2>
		<? endif; ?>
		
		<br /><br />

		<form id="FormOfertas" action="<?=site_url()?>/ofertas/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$oferta->id_oferta): ?>
			<input type="hidden" name="id_oferta" value="<?=@$oferta->id_oferta?>" /> 

			<div class="formulario">
				<label class="obrigatorio">Cadastrado Em</label>
				<div class="formulario-colunado">
					<input type="text" value="<?if(@$oferta->data_cadastro) echo date('d/m/Y H:i:s',strtotime($oferta->data_cadastro))?>" class="campo-padrao" readonly="readonly" />
				</div>
			</div>	

			<br class="clr" />

			<? endif; ?>

			<div class="formulario">
				<label class="obrigatorio">Tipo da oferta</label>
				<div class="formulario-colunado">
					<select name="id_tipo" class="campo-padrao" id="FormTipo">
						<?php 
						foreach ($ofetas_tipos as $row) {
							if($row->id_ofertas_tipo == @$oferta->id_ofertas_tipo)
								echo '<option value="' . $row->id_ofertas_tipo . '" selected="selected">' . $row->tipo . '</option>';
							else
								echo '<option value="' . $row->id_ofertas_tipo . '">' . $row->tipo . '</option>';
						}
						?>
					</select>
				</div>
			</div>

			<br class="clr" />	
			
			<div class="formulario">
				<label class="obrigatorio">Título</label>
				<div><input type="text" name="titulo" id="FormOferta" value="<?=@$oferta->titulo?>" style="width: 516px;" /></div>
			</div>

			
			<? if(@$oferta->valor_tipo == 'de_por') : ?>

			<div class="formulario">
				<label class="obrigatorio">Tipologia do valor</label>
				<label><input type="radio" name="valor_tipo" value="uni">Valor Unitário</label>
				<label><input type="radio" name="valor_tipo" value="de_por" checked="true">Valor DE / POR</label>
			</div>

			<div class="formulario-colunado uni" style="display: none;">
				<label class="obrigatorio">Valor</label>
				<div><input type="text" name="valor" value="<?=number_format(@$oferta->valor, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<div class="formulario-colunado de_por">
				<label class="obrigatorio">Valor De</label>
				<div><input type="text" name="valor_de" value="<?=number_format(@$oferta->valor_de, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<div class="formulario-colunado de_por">
				<label class="obrigatorio">Valor Por</label>
				<div><input type="text" name="valor_por" value="<?=number_format(@$oferta->valor, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<?php else : ?>

			<div class="formulario">
				<label class="obrigatorio">Tipologia do valor</label>
				<label><input type="radio" name="valor_tipo" value="uni" checked="true">Valor Unitário</label>
				<label><input type="radio" name="valor_tipo" value="de_por">Valor DE / POR</label>
			</div>

			<div class="formulario-colunado uni">
				<label class="obrigatorio">Valor</label>
				<div><input type="text" name="valor" value="<?=number_format(@$oferta->valor, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<div class="formulario-colunado de_por" style="display: none;">
				<label class="obrigatorio">Valor De</label>
				<div><input type="text" name="valor_de" value="<?=number_format(@$oferta->valor_de, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<div class="formulario-colunado de_por" style="display: none;">
				<label class="obrigatorio">Valor Por</label>
				<div><input type="text" name="valor_por" value="<?=number_format(@$oferta->valor, 2, ',', '.')?>" class="campo-padra money" /></div>
			</div>

			<?php endif; ?>
			
			<br class="clr" />	
			<br class="clr" />	

			<div class="formulario">
				<label class="obrigatorio">Frase do valor</label>
				<div><input type="text" name="valor_frase" value="<?=@$oferta->valor_frase?>" class="campo-padrao" style="width: 516px;" maxlength="70" /></div>
			</div>

			<br class="clr" />
			<br class="clr" />

			<div class="formulario-colunado">
				<label class="obrigatorio">Imagem Destaque (.JPG, .PNG, .GIF)</label>
				<div><input type="file" name="imagem" id="FormImagem"></div>
				<? if(@$oferta->img): ?><br />
				<img src="<?=base_url()?>uploads/ofertas/zoom/<?=$oferta->img?>" width="100px;"><br /><br />
				<? endif; ?>
			</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Imagem pin google maps (.JPG, .PNG, .GIF)</label>
				<div><input type="file" name="imagem_pin" id="FormPin" ></div>
				<? if(@$oferta->pin): ?>
				<br/>	
				<img src="<?=base_url()?>uploads/ofertas/pin/<?=$oferta->pin?>" width="100px;">
				<br/><br/><br/>
				<? endif; ?>
			</div>
		
			<br class="clr" />
			<br class="clr" />

			<div class="formulario">
				<label class="obrigatorio">Descrição</label>
				<textarea name="descricao" class="campo-grande" id="FormDescricao" /><?=@$oferta->descricao?></textarea>
			</div>	

			<div class="formulario">
				<label class="obrigatorio">Cômodos</label>
				<div><input type="text" name="comodos" id="FormOferta" value="<?=@$oferta->comodos?>" style="width: 240px;" maxlength="45" /></div>
			</div>

			<div class="formulario">
				<label class="obrigatorio">Metragem</label>
				<div><input type="text" name="metragem" id="FormOferta" value="<?=@$oferta->metragem?>" style="width: 240px;" maxlength="45" /></div>
			</div>

			<div class="formulario">
				<label class="obrigatorio">Vagas</label>
				<div><input type="text" name="vagas" id="FormOferta" value="<?=@$oferta->vagas?>" style="width: 240px;" maxlength="20" /></div>
			</div>


			<br class="clr" />
			<br class="clr" />


			<div class="formulario-colunado">
                <label class="obrigatorio">Estado</label>
                <div class="formulario-colunado">
                     <div>
                         <select name="estado" class="campo-padrao" id="FormEstado">
                         <option value="" selected="selected"> -- Escolha -- </option>
                    		<?php foreach($estados as $row):?>
								<option value="<?php echo $row->id_estado?>" <?php if($row->id_estado==@$oferta->id_estado)echo 'selected="selected"'?>><?php echo $row->estado?></option>
							<?php endforeach;?>
                    	</select>
                     </div>
               </div>
          	</div>

          	<div class="formulario-colunado-div">&nbsp;</div>

          	<div class="formulario-colunado">
	            <label class="obrigatorio">Cidade</label>
	            <div class="formulario-colunado">
	                 <div>
	                     <select name="cidade" class="campo-padrao" id="FormCidade">
	                		<?php if(@$cidades){?>
								<?php foreach($cidades as $row):?>
									<option value="<?php echo $row->id_cidade?>" <?php if($row->id_cidade==@$oferta->id_cidade)echo 'selected="selected"'?>><?php echo $row->cidade?></option>
								<?php endforeach;?>
							<?php }else{?>
									<option value="" selected="selected"> -- Escolha um Estado -- </option>
							<?php }?>										
	                	</select>
	                 </div>
	            </div>
           	</div> 

           	<div class="formulario">
				<label class="obrigatorio">Endereço</label>
				<div><input type="text" name="endereco" id="FormOferta" value="<?=@$oferta->endereco?>" style="width: 516px;" maxlength="250" /></div>
			</div>


			<div class="formulario-colunado">
				<label class="obrigatorio">Latitude</label>
				<div><input type="text" name="latitude" id="FormOferta" value="<?=@$oferta->latitude?>" class="campo-padrao"  maxlength="30" /></div>
			</div>

			<div class="formulario-colunado-div">&nbsp;</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Longitude</label>
				<div><input type="text" name="longitude" id="FormOferta" value="<?=@$oferta->longitude?>" class="campo-padrao" maxlength="30" /></div>
			</div>


			<br class="clr" />
			<br class="clr" />


			<div class="formulario">
				<label class="obrigatorio">Tag Title</label>
				<div><input type="text" name="tag_title" id="FormOferta" value="<?=@$oferta->tag_title?>" style="width: 240px;" maxlength="70" /></div>
			</div>

			<div class="formulario">
				<label class="obrigatorio">Tag Description</label>
				<textarea name="tag_description" class="campo-grande" id="FormDescricao" style='height: 85px;' /><?=@$oferta->tag_description?></textarea>
			</div>

			<div class="formulario">
				<label class="obrigatorio">Tag Keywords</label>
				<textarea name="tag_keywords" class="campo-grande" id="FormDescricao" style='height: 85px;' /><?=@$oferta->tag_keywords?></textarea>
			</div>


			<div class="formulario">
				<label class="obrigatorio">URL (Não pode pode haver url repetida)</label>
				<div><input type="text" id="FormOferta" value="<?=@$oferta->url?>" style="width: 516px;" readonly="readonly"  /></div>
			</div>	



			<br class="clr" />
			<br class="clr" />


			<div class="formulario-colunado">
				<label class="obrigatorio">Vezes em que foi favoritado</label>
				<div><input type="text" id="FormOferta" value="<?=@$oferta->qtd_favoritado?>"  style="width: 140px;" readonly="readonly"  /></div>
			</div>

			<div class="formulario-colunado-div">&nbsp;</div>

			<div class="formulario-colunado">
				<label class="obrigatorio">Vezes em que foi visitado</label>
				<div><input type="text" id="FormOferta" value="<?=@$oferta->qtd_visitado?>"  style="width: 140px;" readonly="readonly"  /></div>
			</div>

			<div class="formulario-colunado">&nbsp;</div>
			<div class="formulario-colunado">&nbsp;</div>


			<br class="clr" />
			<br class="clr" />


			<div class="formulario-colunado">
				<label class="obrigatorio">Ativo</label>
				<div class="formulario-colunado">
					<select name="ativo" class="campo-padrao" id="FormAtivo">
						<option <? if(@$oferta->ativo == 'S') echo "selected='selected'"; ?> value="S">Sim</option>
						<option <? if(@$oferta->ativo == 'N') echo "selected='selected'"; ?> value="N">Não</option>
					</select>
				</div>
			</div>	

			<br class="clr" />
			<br class="clr" />





			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>

			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			<a href="<?=site_url()?>/ofertas/lista" title="Voltar" class="bt-voltar">| Voltar para Ofertas</a>

			<br /><br /><br />
		</form>

	</div>
</div>

<!-- FIM CONTEUDO -->

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.money').mask('000.000.000.000.000,00', {reverse: true});


		$('input[name=valor_tipo]').change(function(){

			// $('.money').val('0,00');

			if(this.value == 'uni')
			{
				$('.de_por').hide('fast', function(){ $('.uni').show(); });
			}	

			if(this.value == 'de_por')
			{
				$('.uni').hide('fast', function(){ $('.de_por').show(); });
			}		

		});

	});
</script>

<?=$this->load->view('includes/rodape');?>