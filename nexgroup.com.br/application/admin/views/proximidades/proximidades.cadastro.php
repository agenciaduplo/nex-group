<?=$this->load->view('includes/topo');?>

<script
	type="text/javascript"
	src="<?=base_url()?>assets/admin/js/jquery.jqEasyCharCounter.min.js"></script>
<script type="text/javascript" charset="utf-8">
	
	$(function() {
	   	
	   	
		$("input#FormImagem").filestyle({ 
			image: "<?=base_url()?>assets/admin/img/file.gif",
			imageheight : 16,
			imagewidth : 47,
			width : 150
		});
		     				
		$("#FormDescricao").cleditor(); 
	 
             
  });

</script>
<!-- INICIO CONTEUDO -->

<div id="titulo"><a
	href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=PROXIMIDADES . CADASTRO"
	class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<span class="txt-destaque"><?=$this->session->flashdata('respostaTopo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaChamada')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaLogo')?></span>
<span class="txt-destaque"><?=$this->session->flashdata('respostaMemorial')?></span>
<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">
		<h2>DADOS DA NOTÍCIA</h2>
		<br />
		<br />
		<form id="FormnProximidades" action="<?=site_url()?>/proximidades/salvar/" method="post" enctype="multipart/form-data">
			<? if (@$proximidade->id_proximidade): ?>
				<input type="hidden" name="id_proximidade" value="<?=@$proximidade->id_proximidade?>" /> 
			<? endif; ?>
			<div class="formulario-colunado">
				<label class="obrigatorio">Tipo</label>
				<div class="formulario-colunado">
					<div>
						<select name="id_tipo_proximidade" class="campo-padrao" id="FormTipo">
							<?php foreach($tipos as $tipo){?>
								<option <?php echo ($tipo->id_tipo_proximidade == $proximidade->id_tipo_proximidade) ? ' selected ' : '' ; ?>value="<?php echo $tipo->id_tipo_proximidade;?>"><?php echo $tipo->tipo_proximidade; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<br class="clr" />
			<div class="formulario-colunado">
				<label class="obrigatorio">Nome</label>
				<div>
					<input type="text" name="nome" class="campo-padrao" id="FormProximidade" value="<?=@$proximidade->nome?>" />
				</div>
			</div>
			
			<br class="clr" />
			<div class="formulario-colunado">
				<label class="obrigatorio">Latitude</label>
				<div>
					<input type="text" name="latitude" class="campo-padrao" id="FormLatitude" value="<?=@$proximidade->latitude?>" />
				</div>
			</div>
			
			<br class="clr" />
			<div class="formulario-colunado">
				<label class="obrigatorio">Longitude</label>
				<div>
					<input type="text" name="longitude" class="campo-padrao" id="FormProximidade" value="<?=@$proximidade->longitude?>" />
				</div>
			</div>
			<br class="clr" />
			<br class="clr" />
			
			<div class="a-right">
				<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
					<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
				</a>
				<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
			</div>
			<span class="txt-destaque"><?=$this->session->flashdata('resposta')?></span>
			|<a href="<?=site_url()?>/proximidades/lista" title="Voltar" class="bt-voltar">Voltar para Notícias</a>
			<br />
			<br />
			<br />
		</form>
	</div>
</div>
		
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>