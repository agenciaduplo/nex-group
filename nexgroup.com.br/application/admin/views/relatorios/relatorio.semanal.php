<?php
$this->load->view('includes/topo');

$empr = array();
$campr = array();

?>
<div id="titulo">
	<a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=RELATORIOS . ATENDIMENTOS" class="FlashTitulo"></a>
</div>
<div class="form-select">
	<form action="" method="post">
		<fieldset>
			<legend>Configurações do relatório</legend>
			<table width="100%">
				<tr>
					<td width="100">Tipo:</td>
					<td style="padding-bottom: 5px;">
						<label>
							<input type="radio" name="form_tipo" value="atendimento" checked="checked" id="teg-emp" /> 
							Atendimento
						</label>
						<label>
							<input type="radio" name="form_tipo" value="interesse" id="teg-camp" /> 
							Interesse
						</label>
					</td>
				</tr>
				<tr>
					<td>Data:</td>
					<td>
						<input class="campo-padrao" id="min" readonly="readonly" name="form_min" value="<?=date("d/n/Y",$data['max']); ?>" style="display: inline; width: 80px;" />
						até
						<input class="campo-padrao" id="max" readonly="readonly" name="form_max" value="<?=date("d/n/Y",$data['max']); ?>" style="display: inline; width: 80px;" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<input type="submit" value="Gerar relatório" />
					</td>
				</tr>
			</table>
			
		</fieldset>
	</form>
</div>
<script type="text/javascript">
	var dates = $("#min,#max").datepicker({
		minDate: (new Date(<?=date("Y,n,d",$data['min']); ?>)),
		maxDate: (new Date(<?=date("Y,n,d",$data['max']); ?>)),
		numberOfMonths: 2,
		onSelect: function( selectedDate ) {
			var option = this.id == "min" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
</script>
<br /><br />
<br /><br />
<br /><br />
<?=$this->load->view('includes/rodape');?>