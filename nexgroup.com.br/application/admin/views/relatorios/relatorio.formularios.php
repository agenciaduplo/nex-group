<?php
$this->load->view('includes/topo');

$empr = array();
$campr = array();

$d1 = date('d/m/Y');
$d2 = date('d/m/Y',mktime(0, 0, 0, date("m"), date("d")-7, date("Y")));
$d1f = date('Y,m,d');
$d2f = date('Y,m,d',mktime(0, 0, 0, date("m"), date("d"), date("Y")-1));
?>
<div id="titulo">
	<a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=RELATORIOS . FORMULARIOS" class="FlashTitulo"></a>
</div>
<div class="form-select">
	<form action="" method="post">
		<fieldset>
			<legend>Configurações do relatório</legend>
			<table width="100%">
      	<tr>
					<td width="100">Tipo:</td>
					<td style="padding-bottom: 5px;">
						<label>
							<input type="radio" name="form_tipo" value="1" checked="checked" id="teg-emp" /> 
							Todos
						</label>
						<label>
							<input type="radio" name="form_tipo" value="2" id="teg-camp" /> 
							Atendimento
						</label>
						<label>
							<input type="radio" name="form_tipo" value="3" id="teg-camp" /> 
							Contatos
						</label>
						<label>
							<input type="radio" name="form_tipo" value="4" id="teg-camp" /> 
							Indique
						</label> 
						<label>
							<input type="radio" name="form_tipo" value="5" id="teg-camp" /> 
							Interesse
						</label>
						<label>
							<input type="radio" name="form_tipo" value="6" id="teg-camp" /> 
							Terrenos
						</label>
					</td>
				</tr>
				<tr>
					<td>Data:</td>
					<td>
						<input class="campo-padrao" id="min" readonly="readonly" name="form_min" value="<?=$d2; ?>" style="display: inline; width: 80px;" />
						até
						<input class="campo-padrao" id="max" readonly="readonly" name="form_max" value="<?=$d1; ?>" style="display: inline; width: 80px;" />
					</td>
				</tr>
				<tr id="tog-emp" style="">
					<td>Empreedimento:</td>
					<td>
						<select name="form_emp" class="campo-padrao">
							<?php
							if(sizeof($emp)<>0){
                  echo '<option value="0">Todos</option>';
								foreach($emp as $x){
									echo '<option value="'.$x->id.'">'.$x->nome.'</option>';
									$empr[$x->id] = $x->nome;
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<input type="submit" value="Gerar relatório" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
	var dates = $("#min,#max").datepicker({
		minDate: (new Date(<?=$d2f; ?>)),
		maxDate: (new Date(<?=$d1f; ?>)),
		numberOfMonths: 2,
		onSelect: function( selectedDate ) {
			var option = this.id == "min" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
</script>


<?php


 
if(isset($query)){
	if(sizeof($query)<>0){
		/* Inicia Calculos */

		$navegadores = array();
		$plataformas = array();
		$sites = array();
		$formas = array();
		foreach($query as $x){
			$nav = $this->utilidades->browser($x->navegador);
			$pat = $this->utilidades->platform($x->navegador);
			$host = parse_url($x->origem,PHP_URL_HOST);
			$for = strtolower($x->forma);
			if(!isset($navegadores[$nav['navegador']])){
				$navegadores[$nav['navegador']] = 1;
			} else {
				$navegadores[$nav['navegador']]++;
			} if(!isset($plataformas[$pat])){
				$plataformas[$pat] = 1;
			} else {
				$plataformas[$pat]++;
			} if(!isset($sites[$host])){
				$sites[$host] = 1;
			} else {
				$sites[$host]++;
			} if(!isset($formas[$for])){
				$formas[$for] = 1;
			} else {
				$formas[$for]++;
			}
		}
		arsort($navegadores,SORT_NUMERIC);
		arsort($plataformas,SORT_NUMERIC);
		arsort($sites,SORT_NUMERIC);
		arsort($formas,SORT_NUMERIC);
	}
}

if(isset($query)){
	if(sizeof($query)<>0){
?>
<div style="text-align: center;">
	<h1>Relatório</h1><br />
	Data: <strong><?=$this->input->post('form_min'); ?></strong> a <strong><?=$this->input->post('form_max'); ?></strong><br />
	<?php
	if($this->input->post('form_emp') == 0){
		echo 'Todos';
	} else {
		echo 'Empreedimento: <strong>'.$empr[$this->input->post('form_emp')] . '</strong>';
	}
	?>
</div>
<table style="width: 100%;">
	<tr>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Navegadores / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($navegadores as $navegador => $acessos){
						echo '
							<tr>
								<td>'.$navegador.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Plataformas / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($plataformas as $plataforma => $acessos){
						echo '
							<tr>
								<td>'.$plataforma.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Sites / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($sites as $site => $acessos){
						echo '
							<tr>
								<td>'.$site.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Forma de contato / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($formas as $forma => $acessos){
						echo '
							<tr>
								<td>'.ucwords($forma).'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<table style="width: 100%;" class="tabela-padrao">
	<thead>
		<tr>
			<th>Nome</th>
			<th>E-mail</th>
			<th>Data</th>
			<th>Empreendimento</th>
      <th>Tipo</th>
		</tr>
	</thead>
	<tbody>

<?php
		$i = 0;
		foreach($query as $x){
			echo '
		<tr class="',($i % 2) ? "par" : "impar",'">
			<td>'.ucwords(strtolower($x->nome)).'</td>
			<td>'.strtolower($x->email).'</td>
			<td nowrap="nowrap">'.date("d/m/Y h:i",strtotime($x->data)).'</td>
			<td>'.$x->empreendimento.'</td>
      <td>'.$x->tipo.'</td>
		</tr>
			';
			$i++;
		}
		
		
?>		<tr>
			<th></th>
			<th></th>
			<th></th>
      <th></th>
			<th><a target="_blank" href="http://www.nexgroup.com.br/admin.php/expformulario/index/?tipo=<?=$this->input->post('form_tipo');?>&emp=<?=$this->input->post('form_emp');?>&min=<?=$this->input->post('form_min');?>&max=<?=$this->input->post('form_max');?>">EXPORTAR RELATÓRIO</a></th>
		</tr>
	</tbody>
</table>
<?php
	}
}
?>
<br /><br />
<br /><br />
<br /><br />
<?=$this->load->view('includes/rodape');?>