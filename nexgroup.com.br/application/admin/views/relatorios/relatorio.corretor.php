<?=$this->load->view('includes/topo');?>
<div id="titulo">
	<a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=RELATORIOS . CORRETOR" class="FlashTitulo"></a>
</div>
<div class="form-select">
	<form action="" method="post">
		<fieldset>
			<legend>Configurações do relatório</legend>
			<table width="100%">
				<tr>
					<td width="100">Data:</td>
					<td>
						<input class="campo-padrao" id="min" readonly="readonly" name="form_min" value="<?=date("d/n/Y",$data['min']); ?>" style="display: inline; width: 80px;" />
						até
						<input class="campo-padrao" id="max" readonly="readonly" name="form_max" value="<?=date("d/n/Y",$data['max']); ?>" style="display: inline; width: 80px;" />
					</td>
				</tr>
				<tr id="tog-camp">
					<td>Campanha:</td>
					<td>
						<select name="form_camp" class="campo-padrao">
							<?php
							if(sizeof($camp)<>0){
								foreach($camp as $x){
									echo '<option value="'.$x->url.'">'.$x->url.'</option>';
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<input type="submit" value="Gerar relatório" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
	var dates = $("#min,#max").datepicker({
		minDate: (new Date(<?=date("Y,n,d",$data['min']); ?>)),
		maxDate: (new Date(<?=date("Y,n,d",$data['max']); ?>)),
		numberOfMonths: 2,
		onSelect: function( selectedDate ) {
			var option = this.id == "min" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
</script>


<?php


 
if(isset($query)){
	if(sizeof($query)<>0){
		/* Inicia Calculos */

		$navegadores = array();
		$plataformas = array();
		$sites = array();
		$i = 0;
		foreach($query as $x){
			$nav = $this->utilidades->browser($x->navegador);
			$pat = $this->utilidades->platform($x->navegador);
			$host = parse_url($x->origem,PHP_URL_HOST);
			if(!isset($navegadores[$nav['navegador']])){
				$navegadores[$nav['navegador']] = 1;
			} else {
				$navegadores[$nav['navegador']]++;
			} if(!isset($plataformas[$pat])){
				$plataformas[$pat] = 1;
			} else {
				$plataformas[$pat]++;
			} if(!isset($sites[$host])){
				$sites[$host] = 1;
			} else {
				$sites[$host]++;
			}
			$i++;
		}
		arsort($navegadores,SORT_NUMERIC);
		arsort($plataformas,SORT_NUMERIC);
		arsort($sites,SORT_NUMERIC);
	}
}

if(isset($query)){
	if(sizeof($query)<>0){
?>
<table style="width: 100%;">
	<tr>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Navegadores / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($navegadores as $navegador => $acessos){
						echo '
							<tr>
								<td>'.$navegador.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Plataformas / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($plataformas as $plataforma => $acessos){
						echo '
							<tr>
								<td>'.$plataforma.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Sites / Acessos</legend>
				<table style="width: 100%;">
					<?php
					foreach($sites as $site => $acessos){
						echo '
							<tr>
								<td>'.$site.'</td>
								<td>'.$acessos.'</td>
							</tr>
						';
					}
					?>
				</table>
			</fieldset>
		</td>
		<td width="50%" valign="top">
			<fieldset>
				<legend>Acessos</legend>
				<table style="width: 100%;">
					<tr>
						<td>Acessos</td>
						<td><?=$i; ?></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<?php
	}
}
?>
<br /><br />
<br /><br />
<br /><br />
<?=$this->load->view('includes/rodape');?>