<?=$this->load->view('includes/topo');?>

	<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . LISTA EMAILS" class="FlashTitulo"></a></div>
		<div class="cadastros"><?=$cadastrados?></div>
		<div class="botoes">
			<a href="<?=site_url()?>/emails/cadastro/" title="Inserir">
				<img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" />
			</a>
		</div>
		<br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

			<div class="base-mg">

            <? if ($emails):?>
            <table class="tabela-padrao">
                <thead>
                    <tr> 
                        <th width="45%">Empreendimento</th>
                        <th width="45%">Cidade</th>
                        <td width="15%">Email</td>
                        <td width="15%">Tipo</td>
                        <td width="15%">Setor</td>
                        <th width="10%">Ações</th>                       
                    </tr>
                </thead>
                <tbody>
                	<? $count = 1; ?>
                    <? foreach ($emails as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                    	
                        <td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->empreendimento?></a></td>
                        <td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->cidade?></a></td>
                        <td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->email?></a></td>
						<td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->tipo?></a></td>
						<td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->setor?></a></td>
						<td nowrap="nowrap">
                        	<a href="<?=site_url()?>/emails/editar/<?=$row->id_email?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/emails/apagar/<?=$row->id_email?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->email?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma Email encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>