<?=$this->load->view('includes/topo');?>

	<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . LISTA EMAILS VIEW" class="FlashTitulo"></a></div>
		<div class="cadastros"><?=$cadastrados?></div>
		<div class="botoes">
			<a href="<?=site_url()?>/emails/cadastro/" title="Inserir">
				<img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" />
			</a>
		</div>
		<br class="clr" />
		<script>
			function buscaEmmpId (name)
			{
				alert(name);
				$("#"+name).click();
			}
		</script>
		
			<!-- 

 
            <div id="busca">
		        <div>
	            	<select name="emps" onchange="return buscaEmmpId(this.value);">
	            		<option value="">SELECIONE O EMPREENDIMENTO</option>
	            		<?php foreach ($emails as $row): ?>
	            			<option value="<?=$row->empreendimento?>"><?=$row->empreendimento?></option>
	            		<?php endforeach; ?>
	            	</select>
	            </div>
	            <br class="clr" />
                
                
                <br class="clr" />
       
            </div>
            -->

			<div class="base-mg">

            <? if (@$emails):?>
            <table class="tabela-padrao">
                <thead>
                <?php foreach ($emails as $row): ?>
                    <tr id="<?=$row->empreendimento?>"> 
                        <th width="100%"><?=$row->empreendimento?> - <?=$row->cidade?></th>               
                    </tr>
                    <?php $emailsEmp = $this->model->getEmailsEmps($row->id_empreendimento); ?>
                    <?php if($emailsEmp): ?>
                    <tr> 
                    
                        <th width="100%">
                        	
                        	<!-- LISTA EMAILS POR EMP -->
                        	
				           	<table class="tabela-padrao" style="font-weight: normal;">
				                <thead>
				                    <tr>
				                        <td width="30%">Email</td>
				                        <td width="30%">Tipo</td>
				                        <td width="30%">Setor</td>
				                        <th width="10%">Ações</th>                       
				                    </tr>
				                </thead>
				                <tbody>
				                	<? $count = 1; ?>
				                    <? foreach ($emailsEmp as $row):?>
				                    <? $count++; ?>
				                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
				                    	
				                        <td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->email?></a></td>
										<td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->tipo?></a></td>
										<td><a href="<?=site_url()?>/emails/editar/<?=@$row->id_email?>" title="Editar" class="bt-editar"><?=@$row->setor?></a></td>
										<td nowrap="nowrap">
				                        	<a href="<?=site_url()?>/emails/editar/<?=$row->id_email?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
				                        	<a href="<?=site_url()?>/emails/apagar/<?=$row->id_email?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->email?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
				                        </td>
				                    </tr>
				                    <? endforeach; ?>
				                </tbody>
				            </table>
				            
				            <!-- LISTA EMAILS POR EMP -->
				            
                        </th>              
                    </tr>
                    <?php endif; ?>
                    <tr> 
                        <th width="100%">&nbsp;</th>              
                    </tr>
                <?php endforeach; ?>
                </thead>
            </table>
            <? else: ?>
            	<span>Nenhuma Email encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->

            <!-- paginação/end -->
            <br/><br/>

<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>