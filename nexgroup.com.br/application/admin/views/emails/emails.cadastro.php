<?=$this->load->view('includes/topo');?>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=EMPREENDIMENTOS . CADASTRO EMAILS" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>
            <span class="txt-destaque"><?=$this->session->flashdata('respostaItens')?><br/></span>
           
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<div class="formulario-mg" id="ItensAjax">
                	<h2>Emails</h2>
                	<br/>
		               
 						<form id="FormEmails" action="<?=site_url()?>/emails/salvar/" method="post" enctype="multipart/form-data">
 							<input type="hidden" name="id_email" value="<?=@$email->id_email?>" />
 							<div class="formulario-colunado">
	 							<label class="obrigatorio">Email</label>
	                        	<div>
	                        		<input type="text" value="<?=@$email->email?>" name="email" class="campo-padrao" id="FormEmail" />
	                        	</div>
	                        	
                       	 	</div>
                       	 	 <div class="formulario-colunado-div">&nbsp;</div>
                   			<div class="formulario-colunado">
	                        	<label class="obrigatorio">Empreendimento</label>
		                        <div>
		                        	<select name="empreendimento" class="campo-padrao" id="FormEmpreedimento">
		                        		<option value="" selected="selected"> -- Escolha -- </option>
		                        		<?php foreach($empreendimentos as $row):?>
												<option value="<?php echo $row->id_empreendimento?>" <?php if($row->id_empreendimento==@$email->id_empreendimento){echo 'selected="selected"';}?>><?=@$row->empreendimento?> - <?=@$row->id_empreendimento?></option>
											<?php endforeach;?>
		                        	</select>
		                        </div>
	                        </div>
	                        <br class="clr" />
                        	<label class="obrigatorio">Tipo</label>
                        	<div>
                        		<select name="tipo" class="campo-padrao" id="FormTipo">
		                        	<option value="" selected="selected"> -- Escolha -- </option>
										<option value="para" <?php if(@$email->tipo=='para'){echo 'selected="selected"';}?>>PARA</option>
										<option value="bcc" <?php if(@$email->tipo=='bcc'){echo 'selected="selected"';}?>>BCC</option>
										<option value="cc" <?php if(@$email->tipo=='cc'){echo 'selected="selected"';}?>>CC</option>
		                        	</select>
                        	</div>
                        	<div class="formulario-colunado">
	                        	<label class="obrigatorio">Tipo de Formulário</label>
		                        <div>
		                        	<select name="id_tipo_form" class="campo-padrao" id="FormTipoForm">
		                        		<option value="" selected="selected"> -- Escolha -- </option>
		                        		<?php foreach($tipos_forms as $row):?>
												<option value="<?php echo $row->id_tipo_form?>" <?php if($row->id_tipo_form == @$email->id_tipo_form){echo 'selected="selected"';}?>><?=@$row->tipo?> - <?=@$row->id_tipo_form?></option>
											<?php endforeach;?>
		                        	</select>
		                        </div>
	                        </div>
	                        <br class="clr" />
                        	
                        	<div class="a-right" style="margin-right:10px;">	
                        		<input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
                        	</div>
                        	<br class="clr" />
                        	<br/>		               
		                </form>
		            
                </div>              
				<br class="clr" />	
                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="javascript:history.go(-1)" title="Voltar" class="bt-voltar">Voltar para Emails</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>