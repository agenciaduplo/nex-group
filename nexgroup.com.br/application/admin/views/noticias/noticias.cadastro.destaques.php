<?=$this->load->view('includes/topo');?>

	<script type="text/javascript" charset="utf-8">
	$(function() {
	    
	    $("input#FormImagemNoticias").filestyle({ 
	        image: "<?=base_url()?>assets/admin/img/file.gif",
	        imageheight : 16,
	        imagewidth : 47,
	        width : 150
	    }); 
	});
	</script>
	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=NOTICIAS . CADASTRO DESTAQUES" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>
            <span class="txt-destaque"><?=$this->session->flashdata('respostaItens')?><br/></span>
           
            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">
                	<div class="formulario-mg" id="ItensAjax">
                	<h2>Nova Imagem</h2>
                	<br/>
		               
 						<form id="FormDestaquesNoticias" action="<?=site_url()?>/noticias_destaques/salvar/" method="post" enctype="multipart/form-data">
 							<input type="hidden" name="id_destaque_noticia" value="<?=@$destaque->id_destaque_noticia?>" />
 							<div class="formulario-colunado">
	 							<label class="obrigatorio">Imagem</label>
	                        	<div>
	                        		<input type="file" name="imagem" class="campo-padrao" id="FormImagemNoticias" />
	                        	</div>
	                        	<? if(@$destaque->imagem): ?> <br />
										<img src="<?=base_url()?>uploads/noticias/destaques/<?=$destaque->imagem?>"	width="100px;"> <br />
										<br />
								<? endif; ?>
                       	 	</div>
                       	 	 <div class="formulario-colunado-div">&nbsp;</div>
                   			<div class="formulario-colunado">
	                        	<label class="obrigatorio">Notícia</label>
		                        <div>
		                        	<select name="noticia" class="campo-padrao" id="FormNoticia">
		                        		<option value="" selected="selected"> -- Escolha -- </option>
		                        		<?php foreach($noticias as $row):?>
												<option value="<?php echo $row->id_noticia?>" <?php if($row->id_noticia==@$destaque->id_noticia){echo 'selected="selected"';}?>><?php echo $row->titulo ?></option>
											<?php endforeach;?>
		                        	</select>
		                        </div>
	                        </div>
	                        <br class="clr" />
                        	<label class="obrigatorio">Legenda</label>
                        	<div>
                        		<input type="text" name="legenda" value="<?php echo @$destaque->legenda ?>" class="campo-padrao" id="FormLegendaNoticia" />
                        	</div>
                        	
                        	<div class="a-right" style="margin-right:10px;">	
                        		<input type="image" src="<?=base_url()?>assets/admin/img/bt-adicionar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
                        	</div>
                        	<br class="clr" />
                        	<br/>		               
		                </form>
		            
                </div>              
				<br class="clr" />	
                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta')?>
            </span> | <a href="<?=site_url()?>/noticias_destaques/lista" title="Voltar" class="bt-voltar">Voltar para Notícias Destaques</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>