<?=$this->load->view('includes/topo');?>


<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TIPOS DE OFERTAS . LISTA" class="FlashTitulo"></a></div>

<div class="cadastros"><?=$cadastrados?></div>

<div class="botoes"><a href="<?=site_url()?>/ofertas_tipo/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" /></a></div>

<br class="clr" />

<!-- busca -->
<div id="busca">
	<form id="FormBusca" method="POST">
		<div id="busca-form">
			<div>busca</div>
			<div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
			<br class="clr" />
		</div>
		<div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
		<br class="clr" />
	</form>
</div>
<!-- busca/end -->

<div class="base-mg">
	<? if ($ofertas_tipo): $count = 1; ?>
	<table class="tabela-padrao">
		<thead>
			<tr>
				<th width="45%">Tipo</th>
				<th width="10%">Ações</th>
			</tr>
		</thead>
		<tbody>
			<? foreach ($ofertas_tipo as $row): $count++; ?>
			<tr class="<?=($count % 2) ? "par" : "impar"?>">
				<td width="45%"><?=$row->tipo?></td>
				<td nowrap="nowrap">
					<a href="<?=site_url()?>/ofertas_tipo/editar/<?=$row->id_ofertas_tipo?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
					<a href="<?=site_url()?>/ofertas_tipo/apagar/<?=$row->id_ofertas_tipo?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->tipo?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
				</td>
			</tr>
			<?endforeach;?>
		</tbody>
	</table>
	<? else: ?>
	<span>Nenhum Tipo de Oferta encontrado.</span>
	<? endif; ?>
</div>

<!-- paginação -->
<div id="paginacao">
	<?=@$paginacao?>
	<br class="clr" />
</div>
<!-- paginação/end -->
<br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>