<?=$this->load->view('includes/topo');?>

<script type="text/javascript">
	// $(document).ready(function() {
	// 	$("#FormConteudo").cleditor();
	// });
</script>

<!-- INICIO CONTEUDO -->

<div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TIPO DE OFERTAS . CADASTRO" class="FlashTitulo"></a></div>

<div id="rsvErrors"></div>

<div class="formulario-grande">
	<div class="formulario-topo">&nbsp;</div>
	<div class="formulario-mg">

		<form id="FormTipos" action="<?=site_url()?>/ofertas_tipo/salvar/" method="post">
		<? if (@$ofertas_tipo->id_ofertas_tipo): ?>
			<input type="hidden" name="id_ofertas_tipo" value="<?=@$ofertas_tipo->id_ofertas_tipo?>" />
		<? endif; ?>
		<div class="formulario-colunado">
			<label class="obrigatorio">Tipo</label>
			<div><input type="text" name="tipo" class="campo-padrao" value="<?=@$ofertas_tipo->tipo?>" /></div>
		</div>
		<div class="formulario-colunado-div">&nbsp;</div>
		<div class="formulario-colunado">
			<label class="obrigatorio">&nbsp;</label>
			<div>&nbsp;</div>                        
		</div>

		<br class="clr" />
		<br/><br/>                                        

		<div class="a-right">
			<a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao">
				<img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" />
			</a>
			<input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" />
		</div>
	</form>

</div>
<div class="formulario-base">&nbsp;</div>
</div>

<span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/ofertas_tipo/lista" title="Voltar" class="bt-voltar">voltar para Tipos de Ofertas</a>
<br/><br/><br/>
<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>