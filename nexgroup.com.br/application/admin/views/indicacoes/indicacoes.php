<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=INDICACOES . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->
            
            <div class="base-mg">
            <? if ($indicacoes):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="25%">De</th>
						<th width="25%">Para</th>
						<th width="22%">Empreendimento</th>
						<th width="18%">Data</th>
						<th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($indicacoes as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td><a href="<?=site_url()?>/indicacoes/visualizar/<?=$row->id_indique?>" title="Visualizar" class="bt-visualizar"><?=$row->nome_remetente?></a></td>
                        <td><a href="<?=site_url()?>/indicacoes/visualizar/<?=$row->id_indique?>" title="Visualizar" class="bt-visualizar"><?=$row->nome_destinatario?></a></td>
                        <td><?=$row->empreendimento?></td>
                        <?php
                        	$data = date('d/m/y H:i', strtotime($row->data_envio));
                        ?>
                        <td><strong><?=$data?></strong></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/indicacoes/visualizar/<?=$row->id_indique?>" title="Visualizar" class="bt-visualizar"><img src="<?=base_url()?>assets/admin/img/lupa.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/indicacoes/apagar/<?=$row->id_indique?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a indicação?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span><br/>Nenhuma Indicação encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>