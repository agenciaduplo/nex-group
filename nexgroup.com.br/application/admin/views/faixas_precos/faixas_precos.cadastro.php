<?=$this->load->view('includes/topo');?>

    <script type="text/javascript">
      $(document).ready(function() {
        $("#FormConteudo").cleditor();
      });
    </script>

	<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=FAIXA DE PRECO . CADASTRO" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormFaixas_Precos" action="<?=site_url()?>/faixas_precos/salvar/" method="post">
 					<? if (@$faixa_preco->id_faixa_preco): ?>
                        <input type="hidden" name="id_faixa_preco" value="<?=@$faixa_preco->id_faixa_preco?>" />
                    <? endif; ?>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">Faixa de Preço</label>
                        <div><input type="text" name="faixa_preco" class="campo-padrao" id="FormFaixaPreco" value="<?=@$faixa_preco->faixa_preco?>" /></div>
                    </div>
                    <div class="formulario-colunado-div">&nbsp;</div>
                    <div class="formulario-colunado">
                        <label class="obrigatorio">&nbsp;</label>
	                    <div>&nbsp;</div>                        
                        
                    </div>
                    
                    <br class="clr" />
                   	<br/><br/>                                        
                    
					<div class="a-right"><a href="javascript:void(0);" onclick="history.back()" title="Cancelar" class="botao"><img src="<?=base_url()?>assets/admin/img/bt-cancelar.gif" alt="Cancelar" title="Cancelar" id="FormCancelar" /></a><input type="image" img src="<?=base_url()?>assets/admin/img/bt-salvar.gif" alt="Salvar" title="Salvar" class="botao" id="FormSalvar" /></div>
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>
			
            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/faixas_precos/lista" title="Voltar" class="bt-voltar">voltar para Faixas de Preços</a>
			<br/><br/><br/>
	<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>