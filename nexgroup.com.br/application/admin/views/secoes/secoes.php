<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=SECOES . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            
            	<div class="botoes"><a href="<?=site_url()?>/secoes/cadastro/" title="Inserir"><img src="<?=base_url()?>assets/admin/img/bt-inserir.gif" width="68" height="22" alt="Inserir" title="Inserir" /></a></div>
            
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->

            <div class="base-mg">

            <? if ($secoes->num_rows):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="60%">Nome</th>
                        <th width="30%">Data de Criação</th>
                        <th width="2%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($secoes->result() as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td width="30%"><?=$row->nome?></td>
                        <?php
                        	$data = date('d/m/Y H:i:s', strtotime($row->data_criacao));
                        ?>
                        <td width="30%"><?=$data?></td>
                        <td nowrap="nowrap">
                        <a href="<?=site_url()?>/secoes/editar/<?=$row->id_cms_secoes?>" title="Editar" class="bt-editar"><img src="<?=base_url()?>assets/admin/img/lapis.gif"></a>&nbsp;
                        <!-- <a href="<?=site_url()?>/secoes/apagar/<?=$row->id_cms_secoes?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->nome?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a> -->
                        </td>
                    </tr>
                    <?endforeach;?>
                </tbody>
            </table>
            <? else: ?>
            	<span>Nenhuma seção encontrada.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>