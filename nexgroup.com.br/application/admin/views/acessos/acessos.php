<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=ACESSOS AO CORRETOR ONLINE . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->
            
            <div class="base-mg">
            <?php if ($acessos):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th>IP</th>
						<th>Origem</th>
						<th>Data</th>
						<th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                 
                	<?php $count = 1; ?>
                    <?php foreach ($acessos as $row):?>
                    <?php $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td><a href="<?=site_url()?>/acessos/visualizar/<?=$row->id_acesso_corretor?>" title="Visualizar" class="bt-visualizar"><?=$row->ip?></a></td>
                        <td><?=substr(@$row->origem, 0, 45);?>…</td>
                        <?php
                        	$data = date('d/m/Y H:i:s', strtotime($row->data_acesso));
                        ?>
                        <td><strong><?=$data?></strong></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/acessos/visualizar/<?=$row->id_acesso_corretor?>" title="Visualizar" class="bt-visualizar"><img src="<?=base_url()?>assets/admin/img/lupa.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/acessos/apagar/<?=$row->id_acesso_corretor?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            	<span><br/>Nenhum Acesso ao corretor online encontrado.</span>
            <?php endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>