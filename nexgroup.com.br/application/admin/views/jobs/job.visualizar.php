<?=$this->load->view('includes/topo');
switch($job->id_empresa) {
	case '1' : $link = "http://www.capa.com.br/"; break;
	case '2' : $link = "http://www.eglengenharia.com.br/"; break;
	case '3' : $link = "http://www.dhz.com.br/"; break;
	case '4' : $link = "http://www.lomandoaita.com.br/"; break;
	default: $link = "http://www.nexgroup.com.br/"; break;
}
$link = $link . 'uploads/trabalhe_conosco/' . @$job->curriculo;
?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TRABALHE CONOSCO . VISUALIZAR" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormClientes" action="" method="post">
                    <?
                    	$data = date('d/m/Y H:i:s', strtotime($job->data_envio));
					?>
					<table width="400" border="0">
					  <tr>
					    <td colspan="2">Enviado por <label><strong><?=@$job->nome ?> <?=@$job->sobrenome?></strong>, em <strong><?=$data?></strong></strong></label></td>
					  </tr>
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>E-mail</label></td>
					    <td><label class="obrigatorio"><?=@$job->email?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Telefone</label></td>
					    <td><label class="obrigatorio"><?=@$job->telefone?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Área de atuação</label></td>
					    <td><label class="obrigatorio"><?=@$job->area_atuacao?></label></td>
					  </tr>	
					  <tr>
					    <td width="30%"><label>Currículo</label></td>
					    <td><label class="obrigatorio"><a title='Baixar Arquivo' href='<?=$link; ?>'><?=@$job->curriculo?></a></label></td>
					  </tr>							  
					  <tr>
					    <td width="30%" valign="top"><label>Mensagem</label></td>
					    <td valign="top"><label class="obrigatorio"><?=@$job->sobre_voce?></label></td>
					  </tr>				  					  					  
					</table>                                   	
                    	
                    <br class="clr" />
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/jobs/lista" title="Voltar" class="bt-voltar">voltar para Trabalhe Conosco</a>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>