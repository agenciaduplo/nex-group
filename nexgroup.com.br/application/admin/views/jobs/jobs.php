<?=$this->load->view('includes/topo');?>
<!-- INICIO CONTEUDO -->
            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=TRABALHE CONOSCO . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->
            
            <div class="base-mg">
            <?php if ($jobs):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th>Nome</th>
						<th>E-mail</th>
						<th>Data</th>
						<th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<?php $count = 1; ?>
                    <?php foreach ($jobs as $row):?>
                    <?php $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td><a href="<?=site_url()?>/jobs/visualizar/<?=$row->id_job?>" title="Visualizar" class="bt-visualizar"><?=$row->nome?></a></td>
                        <td><?=$row->email?></td>
                        <?php
                        	$data = date('d/m/Y H:i:s', strtotime($row->data_envio));
                        ?>
                        <td><strong><?=$data?></strong></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/jobs/visualizar/<?=$row->id_job?>" title="Visualizar" class="bt-visualizar"><img src="<?=base_url()?>assets/admin/img/lupa.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/jobs/apagar/<?=$row->id_job?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar o item <?=$row->nome?>?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            	<span><br/>Nenhum Currículo encontrado.</span>
            <?php endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>