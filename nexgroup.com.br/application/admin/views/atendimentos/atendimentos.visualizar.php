<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->

            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=INDICACOES . VISUALIZAR" class="FlashTitulo"></a></div>

            <div id="rsvErrors"></div>

            <div class="formulario-grande">
                <div class="formulario-topo">&nbsp;</div>
                <div class="formulario-mg">

                    <form id="FormClientes" action="" method="post">
                    <?
                    	$data = date('d/m/Y H:i:s', strtotime($atendimento->data_envio));
					?>
					<table width="600" border="0">
					  <tr>
					    <td colspan="2">Enviado por <label><strong><?=@$atendimento->nome?></strong>, em <strong><?=$data?></strong></strong></label></td>
					  </tr>
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>IP</label></td>
					    <td><label class="obrigatorio"><?=@$atendimento->ip?></label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>User Agent</label></td>
					    <td><label class="obrigatorio"><?=@$atendimento->user_agent?></label></td>
					  </tr>
					  <tr>
					    <td><label>&nbsp;</label></td>
					    <td><label class="obrigatorio">&nbsp;</label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Email</label></td>
					    <td><label class="obrigatorio"><?=@$atendimento->email?> </label></td>
					  </tr>
					  <tr>
					    <td width="30%"><label>Telefone:</label></td>
					    <td><label class="obrigatorio"><?=@$atendimento->telefone?></label></td>
					  </tr>
					  
					  <tr>
					    <td width="30%" valign="top"><label>Descrição</label></td>
					    <td valign="top"><label class="obrigatorio"><?=@$atendimento->descricao?></label></td>
					  </tr>				  					  					  
					</table>                                   	
                    	
                    <br class="clr" />
                    </form>

                </div>
                <div class="formulario-base">&nbsp;</div>
            </div>

            <span class="txt-destaque"><?=$this->session->flashdata('resposta');?></span> | <a href="<?=site_url()?>/atendimentos/lista" title="Voltar" class="bt-voltar">voltar para Atendimentos</a>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>