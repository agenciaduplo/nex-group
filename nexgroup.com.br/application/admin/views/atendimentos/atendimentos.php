<?=$this->load->view('includes/topo');?>

<!-- INICIO CONTEUDO -->


            <div id="titulo"><a href="<?=base_url()?>assets/admin/media/titulo.swf?titulo=ATENDIMENTOS . LISTA" class="FlashTitulo"></a></div>

            <div class="cadastros"><?=$cadastrados?></div>
            <br class="clr" />

            <!-- busca -->
            <div id="busca">
            <form id="FormBusca" method="POST">
                <div id="busca-form">
                    <div>busca</div>
                    <div><input type="text" name="keyword" class="form-padrao" id="FormKeyword" value="<?=@$this->input->post('keyword')?>" /></div>
                    <br class="clr" />
                </div>
                <div id="busca-botao"><input type="image" src="<?=base_url()?>assets/admin/img/bt-buscar.jpg" width="16" height="14" /></div>
                <br class="clr" />
            </form>
            </div>
            <!-- busca/end -->
            
            <div class="base-mg">
            <? if ($atendimentos):?>
            <table class="tabela-padrao">
                <thead>
                    <tr>
                        <th width="25%">Nome</th>
						<th width="25%">Email</th>
						<th width="22%">Telefone</th>
						<th width="22%">Forma de Contato</th>
						<th width="18%">Data</th>
						<th width="5%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    
                	<? $count = 1; ?>
                    <? foreach ($atendimentos as $row):?>
                    <? $count++; ?>
                    <tr class="<?=($count % 2) ? "par" : "impar"?>">
                        <td><a href="<?=site_url()?>/atendimentos/visualizar/<?=$row->id_atendimento?>" title="Visualizar" class="bt-visualizar"><?=$row->nome?></a></td>
                        <td><a href="<?=site_url()?>/atendimentos/visualizar/<?=$row->id_atendimento?>" title="Visualizar" class="bt-visualizar"><?=$row->email?></a></td>
                        <td><?=$row->telefone?></td>
                        <td><?=$row->forma_contato?></td>
                        <?php
                        	$data = date('d/m/y H:i', strtotime($row->data_envio));
                        ?>
                        <td><strong><?=$data?></strong></td>
                        <td nowrap="nowrap">
                        	<a href="<?=site_url()?>/atendimentos/visualizar/<?=$row->id_atendimento?>" title="Visualizar" class="bt-visualizar"><img src="<?=base_url()?>assets/admin/img/lupa.gif"></a>&nbsp;
                        	<a href="<?=site_url()?>/atendimentos/apagar/<?=$row->id_atendimento?>" title="Excluir" class="bt-excluir" onclick="return confirm('Deseja apagar a atendimento?');"><img src="<?=base_url()?>assets/admin/img/delete.gif"></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <? else: ?>
            	<span><br/>Nenhuma Antendimento encontrado.</span>
            <? endif; ?>
            </div>

            <!-- paginação -->
            <div id="paginacao">
                <?=@$paginacao?>
                <br class="clr" />
            </div>
            <!-- paginação/end -->
            <br/><br/>


<!-- FIM CONTEUDO -->

<?=$this->load->view('includes/rodape');?>