<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  	<title>Sistema de Gerenciamento de Conteúdo (CMS) - Divex</title>

	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-1.5.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.media.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.maskedinput.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.rsv.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.ui.datepicker.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.filestyle.mini.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?=base_url()?>assets/admin/js/functions.js"></script>
	
	<style type="text/css">
		<!--
		@import url("<?=base_url()?>assets/admin/css/default.css");
		-->
	</style>	

</head>

<body>
	<div id="topo">
	    <div class="centralizacao">
	        <div id="logo"><a href="<?=base_url()?>admin.php"><h1>Divex</h1></a></div>
	        <br class="clr" />
	    </div>
	</div>
	<div id="conceitual"><div id="conceitual-mg"></div></div>
	<div id="miolo">
		<div id="logon-box">
			<div id="rsvErrors"></div>		
		</div>
	    <div id="logon-box" class="loginCorretor">
	
	        <div id="titulo"><a href="assets/admin/media/titulo.swf?titulo=LOGIN" class="FlashTitulo"></a></div>
	        
	       	<p>
				<h1>A PHP Error was encountered</h1>

				<p>Severity: <?php echo $severity; ?></p>
				<p>Message:  <?php echo $message; ?></p>
				<p>Filename: <?php echo $filepath; ?></p>
				<p>Line Number: <?php echo $line; ?></p>
			</p>
			<p style="margin-bottom:100px;"><a href="javascript: history.go(-1);">Voltar</a></p>
			 
	    </div> 
		<br class="clear">
	</div>
	
	<div id="rodapeHome">
		<div class="rodape-conceito">
			<p>
				<br/><strong>:: Sistema de Gerenciamento de Conteúdo</strong>
				<br/><strong>Nexgroup</strong>
				<br/>©‎ Desenvolvido por <a href="http://www.divex.com.br" target="_blank" style="text-decoration:underline">Divex</a>
			</p>
		</div>	
	</div>
	
</body>
</html>