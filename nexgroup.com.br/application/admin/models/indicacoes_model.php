<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indicacoes_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numIndicacoes ()
	{		
		$this->db->select('*')->from('indique');
		
		return $this->db->count_all_results();
	}
	#-----------------------------------------------------------------------------------#
	function numIndicacoesBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM indique i, empreendimentos e
				WHERE i.nome_remetente LIKE '%$keyword%'
				OR i.nome_destinatario LIKE '%$keyword%'
				AND i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
			   ";
			  		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}	
#-----------------------------------------------------------------------------------#
	
	function getIndicacoes ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM indique i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getIndicacaoVisualizar ($id_indique)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM indique i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento AND i.id_indique = $id_indique
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaIndicacoes ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM indique i, empreendimentos e
				WHERE i.nome_remetente LIKE '%$keyword%'
				OR i.nome_destinatario LIKE '%$keyword%'
				AND i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delIndicacao ($id_indique)
	{		
		$where = array ('id_indique' => $id_indique);
		$this->db->select('*')->from('indique')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_indique', $id_indique);
            $this->db->delete('indique');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "indique";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */