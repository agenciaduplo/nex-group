<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
#-----------------------------------------------------------------------------------#
	function getOfertasTipos()
	{
		$sql = "SELECT * FROM ofertas_tipo ORDER BY tipo ASC";

		$query = $this->db->query($sql);		
		return $query->result();
	}	
#-----------------------------------------------------------------------------------#
	function numOfertas()
	{		
		$this->db->select('*')->from('ofertas');		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#
	function numOfertasBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM ofertas
				WHERE titulo LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#		
	function getOfertas($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT 
				o.id_oferta, 
				o.titulo, 
				o.ativo, 
				ot.tipo AS TIPO,
				COUNT(oi.id) AS IMG_TOTAL
				FROM ofertas o
				LEFT JOIN ofertas_tipo ot ON o.id_ofertas_tipo = ot.id_ofertas_tipo
				LEFT JOIN ofertas_imagens oi ON o.id_oferta = oi.id_oferta
				GROUP BY o.id_oferta
				ORDER BY o.id_oferta ASC
				LIMIT $offset, 30
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function buscaOfertas($keyword)
	{	
		$sql = "SELECT 
				o.id_oferta, 
				o.titulo, 
				o.ativo, 
				ot.tipo AS TIPO,
				COUNT(oi.id) AS IMG_TOTAL
				FROM ofertas o
				LEFT JOIN ofertas_tipo ot ON o.id_ofertas_tipo = ot.id_ofertas_tipo
				LEFT JOIN ofertas_imagens oi ON o.id_oferta = oi.id_oferta
				WHERE titulo LIKE '%$keyword%'
				GROUP BY o.id_oferta
				ORDER BY o.titulo ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function getOfertaId($id_oferta)
	{	
		$sql = "SELECT *
				FROM ofertas
				WHERE id_oferta = $id_oferta
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}


#-----------------------------------------------------------------------------------#	
	function getOferta($id_oferta)
	{		
		$where = array ('id_oferta' => $id_oferta);
		
		$this->db->start_cache();
		$this->db->select('*')->from('ofertas')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
#-----------------------------------------------------------------------------------#	
	function setOferta ($data, $id_oferta = "")
	{		
		if ($id_oferta)
		{
			$where = array ('id_oferta' => $id_oferta);
			$this->db->select('id_oferta')->from('ofertas')->where($where);
			
			if (! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
				$this->load->library('image_lib');
					
				$upload = "";
				//=====================================================================
				//INICIO UPLOAD FOTO DESTAQUE
				//=====================================================================
					$config['upload_path']		= './uploads/ofertas/zoom';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	
			        if ($this->upload->do_upload('imagem'))
			        {
			        	$imagem = $this->getImagem($id_oferta);

						if($imagem->img)
						{
							unlink('./uploads/ofertas/zoom/'   . $imagem->img);
							unlink('./uploads/ofertas/medium/' . $imagem->img);
							unlink('./uploads/ofertas/thumb/'  . $imagem->img);	
						}

			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/ofertas/zoom/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 800;
						$config['height']			= 600;
						
						$config['quality']			= 100;

						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						//MEDIUM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/ofertas/medium/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 324;
						$config['height']			= 258;
						
						$config['quality']			= 100;

						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$size = getimagesize('./uploads/ofertas/medium/' . $file_name);
						
						if($size[0] >  $size[1])
						{
							//THUMB
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/ofertas/thumb/' . $file_name;
							$config['maintain_ratio']	= true;
							$config['width']			= 120;
							$config['height']			= 120;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}
						else
						{
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/ofertas/thumb/' . $file_name;
							$config['maintain_ratio']	= TRUE;
							$config['width']			= 180;
							$config['height']			= 180;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}
						
						$config['image_library'] 	= 'gd2';
						$config['source_image'] 	= './uploads/ofertas/thumb/' . $file_name;
						$config['x_axis'] 			= 10;
						$config['y_axis'] 			= 10;
						$config['height'] 			= 70;
						$config['width']  			= 70;
						$config['maintain_ratio'] 	= FALSE;
				
						$this->image_lib->initialize($config);
						$this->image_lib->crop();
						
						$this->db->set('img', $file_name);
					}
			        
		        //=====================================================================
				//FIM UPLOAD DA FOTO DESTAQUE
				//=====================================================================
	        	
	            $this->db->set($data);
	            $this->db->where('id_oferta', $id_oferta);
	            $this->db->update('ofertas');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "ofertas";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso

		        return $id_oferta; 	                	        	
			}
		}
		else
		{
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
			//INICIO UPLOAD DO FOTO DESTAQUE
			//=====================================================================
					$config['upload_path']		= './uploads/ofertas/zoom';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      
			        if ($this->upload->do_upload('imagem'))
			        {
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/ofertas/zoom/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 800;
						$config['height']			= 600;
						
						$config['quality']			= 100;

						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						//MEDIUM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/ofertas/medium/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 324;
						$config['height']			= 258;
						
						$config['quality']			= 100;

						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$size = getimagesize('./uploads/ofertas/medium/' . $file_name);
						
						if($size[0] >  $size[1]){
							
							//THUMB
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/ofertas/thumb/' . $file_name;
							$config['maintain_ratio']	= true;
							$config['width']			= 100;
							$config['height']			= 100;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}else{
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/ofertas/thumb/' . $file_name;
							$config['maintain_ratio']	= TRUE;
							$config['width']			= 180;
							$config['height']			= 180;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}
						
						$config['image_library'] 	= 'gd2';
						$config['source_image'] 	= './uploads/ofertas/thumb/' . $file_name;
						$config['x_axis'] 			= 10;
						$config['y_axis'] 			= 10;
						$config['height'] 			= 70;
						$config['width']  			= 70;
						$config['maintain_ratio'] 	= FALSE;
				
						$this->image_lib->initialize($config);
						$this->image_lib->crop();
					}
	        			
	        //=====================================================================
			//FIM UPLOAD DA FOTO DESTAQUE
			//=====================================================================
			
	        $this->db->set('img', $file_name)->set($data);
	        $this->db->insert('ofertas');
	        $idOfertas = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "ofertas";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idOfertas; 	                	        	
		}
	}
	
	function delOferta($id_oferta)
	{
	    $this->db->where('id_oferta', $id_oferta);
	    $this->db->delete('ofertas');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "ofertas";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getImagens($id_oferta)
	{
		$sql = "SELECT *
				FROM ofertas_imagens
				WHERE id_oferta = $id_oferta
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function setImagem($data)
	{
		$this->load->library('image_lib');
		
		$config['upload_path']		= './uploads/ofertas/galeria';
        $config['allowed_types']	= 'gif|jpg|png|bmp';
        $config['max_size']			= '6000';
        $config['max_width']		= '4000';
        $config['max_height']		= '4000';
        $config['encrypt_name']     = TRUE;
  
        $this->load->library('upload', $config);
      
        if ($this->upload->do_upload('imagem'))
        {
        	$file_data = $this->upload->data();
			
	        $file_name = $file_data['file_name'];
	        $file_size = $file_data['file_size'];
	        
	        //ZOOM
			$config['create_thumb']     = FALSE;
			$config['source_image']	    = $file_data['full_path'];
			$config['new_image']		= './uploads/ofertas/galeria/' . $file_name;
			$config['maintain_ratio']	= TRUE;
			$config['width']			= 800;
			$config['height']			= 600;
			
			$config['quality']			= 100;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();		        
			
			$size = getimagesize('./uploads/ofertas/galeria/' . $file_name);
			
			if($size[0] >  $size[1]){
				
				//THUMB
				$config['create_thumb']     = false;
				$config['source_image']	    = './uploads/ofertas/galeria/' . $file_name;
				$config['new_image']		= './uploads/ofertas/galeriaThumb/' . $file_name;
				$config['maintain_ratio']	= true;
				$config['width']			= 145;
				$config['height']			= 145;
				$config['quality']			= 100;
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			}else{
				$config['create_thumb']     = false;
				$config['source_image']	    = './uploads/ofertas/galeria/' . $file_name;
				$config['new_image']		= './uploads/ofertas/galeriaThumb/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['width']			= 140;
				$config['height']			= 140;
				$config['quality']			= 100;
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			}
			
			$config['image_library'] 	= 'gd2';
			$config['source_image'] 	= './uploads/ofertas/galeriaThumb/' . $file_name;
			$config['x_axis'] 			= 1;
			$config['y_axis'] 			= 1;
			$config['height'] 			= 70;
			$config['width']  			= 70;
			$config['maintain_ratio'] 	= FALSE;
	
			$this->image_lib->initialize($config);
			$this->image_lib->crop();	
		
			$this->db->set('img', $file_name)->set($data)->insert('ofertas_imagens');
			//Log Acesso
		    	$acao 		= "insert";
		    	$tabela 	= "ofertas_imagens";
		    	$sql 		= $this->db->last_query();
		    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		    //Log Acesso				
        }
        else
        {
        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
        }
	}
#-----------------------------------------------------------------------------------#
	function getImagem($id_oferta)
	{		
		$this->db->start_cache();
		$this->db->select('img')->from('ofertas')->where('id_oferta', $id_oferta);
		$this->db->stop_cache();
		
		if (!$this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}		
#-----------------------------------------------------------------------------------#	
	function getImagemGaleria ($id_galeria)
	{		
		$this->db->start_cache();
		$this->db->select('img')->from('ofertas_imagens')->where('id', $id_galeria);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}	
#-----------------------------------------------------------------------------------#	
	function delImagem ($id_galeria='', $id_oferta= '')
	{		
		if($id_oferta)
		{
			$where = array('id_oferta' => $id_oferta);
			$this->db->select('id')->from('ofertas_imagens')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
	            $this->db->where('id_oferta', $id_oferta);
	            $this->db->delete('ofertas_imagens');
	            
				//Log Acesso
			    	$acao 		= "delete";
			    	$tabela 	= "ofertas_imagens";
			    	$sql 		= $this->db->last_query();
			    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
			    //Log Acesso            
			}
		}
		else
		{
			$where = array('id' => $id_galeria);
			$this->db->select('id')->from('ofertas_imagens')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
	            $this->db->where('id', $id_galeria);
	            $this->db->delete('ofertas_imagens');
	            
				//Log Acesso
			    	$acao 		= "delete";
			    	$tabela 	= "ofertas_imagens";
			    	$sql 		= $this->db->last_query();
			    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
			    //Log Acesso            
			}
		}
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	