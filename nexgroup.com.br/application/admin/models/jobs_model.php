<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numJobs ()
	{		
		$this->db->select('*')->from('jobs');
		
		return $this->db->count_all_results();
	}
	
	function getJobs ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM jobs
				ORDER BY data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getJobVisualizar ($id_job)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM jobs
				WHERE id_job = $id_job
			   ";
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	function numJobsBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM jobs
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR sobre_voce LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	function buscaJobs ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM jobs
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR sobre_voce LIKE '%$keyword%'
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delJob ($id_job)
	{		
		$where = array ('id_job' => $id_job);
		$this->db->select('*')->from('jobs')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_job', $id_job);
            $this->db->delete('jobs');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "jobs";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */