<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_destaques_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$dataAcao = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($dataAcao)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numDestaqueNoticias ()
	{		
		$this->db->select('*')->from('destaques_noticias');		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numDestaqueNoticiasBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM noticias e, destaques_noticias d
				WHERE e.id_noticia = d.id_noticia
				AND e.titulo LIKE '%$keyword%' OR d.legenda LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#		
	
	function getDestaqueNoticias ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM noticias e, destaques_noticias d
				WHERE e.id_noticia = d.id_noticia
				ORDER BY d.id_destaque_noticia DESC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	function getNoticias ()
	{		
		
		$sql = "SELECT *
				FROM noticias
				ORDER BY data_cadastro DESC
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function buscaDestaqueNoticias ($keyword)
	{	
		$sql = "SELECT *
				FROM noticias e, destaques_noticias d
				WHERE e.id_noticia = d.id_noticia
				AND e.titulo LIKE '%$keyword%' d.legenda LIKE '%$keyword%'
				ORDER BY e.titulo ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function getDestaqueEmpreendimento($id_destaque_noticia)
	{		
		$where = array ('id_destaque_noticia' => $id_destaque_noticia);
		
		$this->db->select('*')->from('destaque_noticias');
		$this->db->where($where);
		
		
		$query = $this->db->get();
		
		return $query->row();

	}
		
	#-----------------------------------------------------------------------------------#	
		
	#-----------------------------------------------------------------------------------#
	
	function setDestaque($data,$id_destaque ='')
	{
		if($id_destaque){
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/noticias/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '5000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/noticias/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 992;
					$config['height']			= 425;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }

	       $this->db->set($data)->update('destaques_noticias');
	       	
		}else{
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/noticias/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '5000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/noticias/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 992;
					$config['height']			= 425;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }

	       $this->db->set($data)->insert('destaques_noticias');
		}
	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Destaques Noticias";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
	
#-----------------------------------------------------------------------------------#	

	function delDestaqueNoticia ($id_destaque_noticia)
	{
	    $this->db->where('id_destaque_noticia', $id_destaque_noticia);
	    $this->db->delete('destaques_noticias');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "destaques noticias";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	
	function getDestaque($id_destaque_noticia){
		
		$this->db->flush_cache();
	
		$this->db->select('*')->from('destaques_noticias');
		$this->db->join('noticias','noticias.id_noticia = destaques_noticias.id_noticia','INNER');
		$this->db->where('id_destaque_noticia',$id_destaque_noticia);
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	