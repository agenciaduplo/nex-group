<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numContatos ()
	{		
		$this->db->select('*')->from('contatos');
		
		return $this->db->count_all_results();
	}
	
	function getContatos ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM contatos
				ORDER BY data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getContatoVisualizar ($id_contato)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM contatos
				WHERE id_contato = $id_contato
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaContatos ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM contatos
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR comentarios LIKE '%$keyword%'
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delContato ($id_contato)
	{		
		$where = array ('id_contato' => $id_contato);
		$this->db->select('*')->from('contatos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_contato', $id_contato);
            $this->db->delete('contatos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "contatos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */