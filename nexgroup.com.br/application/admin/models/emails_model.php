<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$dataAcao = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($dataAcao)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numEmails ()
	{		
		$this->db->select('*')->from('emails');		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numEmailsBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM emails
				WHERE email LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#		
	
	function getEmails ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM emails
				INNER JOIN empreendimentos ON empreendimentos.id_empreendimento = emails.id_empreendimento
				INNER JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade
				INNER JOIN tipos_forms ON tipos_forms.id_tipo_form = emails.id_tipo_form
				INNER JOIN setores ON setores.id_setor = emails.id_setor
				ORDER BY empreendimentos.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	function getEmailsEmps ($id_empreendimento)
	{
		$this->db->flush_cache();			
		
		$sql = "SELECT *
				FROM emails
				INNER JOIN empreendimentos ON empreendimentos.id_empreendimento = emails.id_empreendimento
				INNER JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade
				INNER JOIN tipos_forms ON tipos_forms.id_tipo_form = emails.id_tipo_form
				INNER JOIN setores ON setores.id_setor = emails.id_setor
				WHERE emails.id_empreendimento = $id_empreendimento
				ORDER BY emails.email ASC
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		return $query->result();	
	}
	
	function getEmailsViews ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT i.empreendimento, c.cidade, i.id_empreendimento
				FROM emails e, empreendimentos i, cidades c
				WHERE e.id_empreendimento = i.id_empreendimento
				AND i.id_cidade = c.id_cidade
				GROUP BY e.id_empreendimento
				ORDER BY i.empreendimento ASC 
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		return $query->result();
		
	}
	
	function getEmail($id_email){
		
		$this->db->flush_cache();
	
		$this->db->select('*')->from('emails');
		$this->db->join('empreendimentos','empreendimentos.id_empreendimento = emails.id_empreendimento','INNER');
		
		$this->db->where('id_email',$id_email);
		
		$query = $this->db->get();
		
		return $query->row();
	}	
	
		
	#-----------------------------------------------------------------------------------#	
	
	function buscaEmails ($keyword,$offset =0)
	{	
		
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM emails
				INNER JOIN empreendimentos ON empreendimentos.id_empreendimento = emails.id_empreendimento 
				INNER JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade
				INNER JOIN tipos_forms ON tipos_forms.id_tipo_form = emails.id_tipo_form
				INNER JOIN setores ON setores.id_setor = emails.id_setor
				WHERE email LIKE '%$keyword%' 
				OR  empreendimento LIKE '%$keyword%' 
				ORDER BY emails.id_empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	
	#-----------------------------------------------------------------------------------#	
		
	#-----------------------------------------------------------------------------------#
	
	function setEmail($data, $id_email = '')
	{
		if($id_email){
			$this->db->where('id_email',$id_email);
			$this->db->set($data)->update('emails');
		}else{
	       $this->db->set($data)->insert('emails');
		}
	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Emails";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
	
#-----------------------------------------------------------------------------------#	

	function delEmail ($id_email)
	{
	    $this->db->where('id_email', $id_email);
	    $this->db->delete('emails');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "emails";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getEmpreendimentos ()
	{		
		
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND e.ativo = 'S'
				ORDER BY e.empreendimento ASC
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();			
		return $query->result();
		
	}
	
	function getTiposForms ()
	{
		$sql = "SELECT *
				FROM tipos_forms
				ORDER BY tipo ASC
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		return $query->result();	
	}
		
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	