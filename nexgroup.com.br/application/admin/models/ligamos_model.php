<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ligamos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numLigamos ()
	{		
		$this->db->select('*')->from('ligamos');
		
		return $this->db->count_all_results();
	}
	#-----------------------------------------------------------------------------------#
	function numLigamosBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM ligamos i, empreendimentos e
				WHERE i.nome LIKE '%$keyword%'
				OR i.email LIKE '%$keyword%'
				AND i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
			   ";
			  		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}	
#-----------------------------------------------------------------------------------#
	
	function getLigamos ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM ligamos i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getLigamoVisualizar ($id_ligamo)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM ligamos i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento and i.id_ligamo = $id_ligamo
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaLigamos ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM ligamos i, empreendimentos e
				WHERE i.nome LIKE '%$keyword%'
				OR i.email LIKE '%$keyword%'
				AND i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delLigamo ($id_ligamo)
	{		
		$where = array ('id_ligamo' => $id_ligamo);
		$this->db->select('*')->from('ligamos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_ligamo', $id_ligamo);
            $this->db->delete('ligamos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "ligamos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */