<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faixas_Precos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numFaixas_Precos ()
	{		
		$this->db->select('*')->from('faixas_precos');		
		
		return $this->db->count_all_results();
	}
	
	function getFaixas_Precos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM faixas_precos
				ORDER BY faixa_preco ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function buscaFaixas_Precos ($keyword)
	{		
		
		$sql = "SELECT *
				FROM faixas_precos 
				WHERE faixa_preco LIKE '%$keyword%'
				ORDER BY faixa_preco ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getFaixaPreco ($id_faixa_preco)
	{		
		$where = array ('id_faixa_preco' => $id_faixa_preco);
		
		$this->db->start_cache();
		$this->db->select('*')->from('faixas_precos')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setFaixaPreco ($data, $id_faixa_preco = "")
	{		
		if ($id_faixa_preco)
		{
			$where = array ('id_faixa_preco' => $id_faixa_preco);
			$this->db->select('*')->from('faixas_precos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Faixa de Preço salva com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_faixa_preco', $id_faixa_preco);
                $this->db->update('faixas_precos');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "faixas_precos";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('faixas_precos');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "faixas_precos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso  			
			
			return $this->db->insert_id();
		}
	}
	
	function delFaixaPreco ($id_faixa_preco)
	{		
		$where = array ('id_faixa_preco' => $id_faixa_preco);
		$this->db->select('*')->from('faixas_precos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_faixa_preco', $id_faixa_preco);
            $this->db->delete('faixas_precos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "faixas_precos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso             
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */