<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios_model extends CI_Model {	

	function __construct(){
		parent::__construct();
	}
	
	function getAtendimentosURL(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT atendimentos.url FROM atendimentos ORDER BY atendimentos.url ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getAtendimentosEMP(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT empreendimentos.empreendimento AS nome, atendimentos.id_empreendimento AS id FROM atendimentos INNER JOIN empreendimentos ON atendimentos.id_empreendimento = empreendimentos.id_empreendimento ORDER BY nome ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getAtendimentosDATA(){
		$this->db->flush_cache();
		$query = $this->db->query("SELECT Max(atendimentos.data_envio) AS max, Min(atendimentos.data_envio) AS min FROM atendimentos LIMIT 1");
		$query = $query->row();
		list($max) = explode(' ',$query->max);
		list($min) = explode(' ',$query->min);
		return array(
			'max' => strtotime($max),
			'min' => strtotime($min)
		);
	}
	function getRelatorioSemanal($tipo,$min,$max) {
		$min = implode('-',array_reverse(explode('/',$min)))." 00:00:00";
		$max = implode('-',array_reverse(explode('/',$max)))." 23:59:59";
		
		if(strtolower($tipo) == 'atendimento'):
			$sql = "SELECT 'atendimento' as tipo, empreendimentos.empreendimento, atendimentos.nome, atendimentos.email, atendimentos.telefone, atendimentos.descricao, grupos_atendimentos.emails, atendimentos.data_envio AS data FROM atendimentos LEFT JOIN empreendimentos ON empreendimentos.id_empreendimento = atendimentos.id_empreendimento LEFT JOIN grupos_atendimentos ON grupos_atendimentos.id_atendimento = atendimentos.id_atendimento WHERE atendimentos.id_empreendimento IN (84,71,33,86,90,83,1,87,5,61,82,36,91) AND atendimentos.data_envio BETWEEN '{$min}' AND '{$max}' ORDER BY atendimentos.data_envio DESC";
		else:
			$sql = "SELECT 'interesse' as tipo, empreendimentos.empreendimento, interesses.nome, interesses.email, interesses.telefone, interesses.comentarios, grupos_interesses.emails, interesses.data_envio AS data FROM interesses  LEFT JOIN empreendimentos ON empreendimentos.id_empreendimento = interesses.id_empreendimento LEFT JOIN grupos_interesses ON grupos_interesses.id_interesse = interesses.id_interesse WHERE interesses.id_empreendimento IN (84,71,33,86,90,83,1,87,5,61,82,36,91) AND interesses.data_envio  BETWEEN '{$min}' AND '{$max}' ORDER BY interesses.data_envio DESC";
		endif;
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function searchAtendimentosDATA($tipo,$id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		$where = "atendimentos.id_empreendimento = '{$id}'";
		if($tipo == 2){
			$where = "atendimentos.url = '{$id}'";
		}
		$sql = "SELECT atendimentos.id_atendimento AS id, atendimentos.user_agent AS navegador, atendimentos.origem, atendimentos.nome, atendimentos.email, atendimentos.forma_contato AS forma, atendimentos.url, atendimentos.data_envio AS data FROM atendimentos WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' AND '".$max."' ORDER BY atendimentos.data_envio DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getContatosURL(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT contatos.url FROM contatos ORDER BY contatos.url ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getContatosEMP(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT empreendimentos.empreendimento AS nome, contatos.id_empreendimento AS id FROM contatos INNER JOIN empreendimentos ON contatos.id_empreendimento = empreendimentos.id_empreendimento ORDER BY nome ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getContatosDATA(){
		$this->db->flush_cache();
		$query = $this->db->query("SELECT Max(contatos.data_envio) AS max, Min(contatos.data_envio) AS min FROM contatos LIMIT 1");
		$query = $query->row();
		list($max) = explode(' ',$query->max);
		list($min) = explode(' ',$query->min);
		return array(
			'max' => strtotime($max),
			'min' => strtotime($min)
		);
	}
	function searchContatosDATA($tipo,$id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		$where = "contatos.id_empreendimento = '{$id}'";
		if($tipo == 2){
			$where = "contatos.url = '{$id}'";
		}
		$sql = "SELECT contatos.id_contato AS id, contatos.user_agent AS navegador, contatos.origem, contatos.nome, contatos.email, contatos.forma_contato AS forma, contatos.url, contatos.data_envio AS data FROM contatos WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' AND '".$max."' ORDER BY contatos.data_envio DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getInteressesURL(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT interesses.url FROM interesses ORDER BY interesses.url ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getInteressesEMP(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT empreendimentos.empreendimento AS nome, interesses.id_empreendimento AS id FROM interesses INNER JOIN empreendimentos ON interesses.id_empreendimento = empreendimentos.id_empreendimento ORDER BY nome ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getInteressesDATA(){
		$this->db->flush_cache();
		$query = $this->db->query("SELECT Max(interesses.data_envio) AS max, Min(interesses.data_envio) AS min FROM interesses LIMIT 1");
		$query = $query->row();
		list($max) = explode(' ',$query->max);
		list($min) = explode(' ',$query->min);
		return array(
			'max' => strtotime($max),
			'min' => strtotime($min)
		);
	}
	function searchInteressesDATA($tipo,$id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		$where = "interesses.id_empreendimento = '{$id}'";
		if($tipo == 2){
			$where = "interesses.url = '{$id}'";
		}
		$sql = "SELECT interesses.id_interesse AS id, interesses.user_agent AS navegador, interesses.origem, interesses.nome, interesses.email, interesses.telefone, interesses.comentarios, interesses.forma_contato AS forma, interesses.url, interesses.data_envio AS data FROM interesses WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' ORDER BY interesses.data_envio DESC";
		$query = $this->db->query($sql);
		return $query->result();
	} 
     
	function searchFormulariosDATA($tipo,$min,$max,$id){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		if($id>0) $where = "id_empreendimento = '{$id}'";
    else $where = "1 = 1";
    switch($tipo) {
      default:
      case 1:
        $sql = " ( SELECT atendimentos.id_atendimento AS id, atendimentos.user_agent AS navegador, 
                       atendimentos.origem, atendimentos.nome, atendimentos.email, 
                       atendimentos.forma_contato AS forma, atendimentos.url, 
                       atendimentos.data_envio AS data, e.empreendimento, 'atendimento' as tipo 
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' 
                ) UNION (
                  SELECT contatos.id_contato AS id, contatos.user_agent AS navegador, 
                  contatos.origem, contatos.nome, contatos.email, 
                  contatos.forma_contato AS forma, contatos.url, 
                  contatos.data_envio AS data, e.empreendimento, 'contato' as tipo 
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT indique.id_indique AS id, indique.user_agent AS navegador, 
                  indique.origem, indique. nome_remetente as nome, 'email' as forma, 
                  indique.email_remetente as email, indique.url, 
                  indique.data_envio AS data, e.empreendimento, 'indique' as tipo 
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT interesses.id_interesse AS id, interesses.user_agent AS navegador, 
                    interesses.origem, interesses.nome, interesses.email,
                    interesses.forma_contato AS forma, interesses.url, 
                    interesses.data_envio AS data, e.empreendimento, 'interesse' as tipo  
                  FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                  WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                ) UNION (
                  SELECT terrenos.id_terreno AS id, terrenos.user_agent AS navegador, 
                    terrenos.origem, terrenos.nome, terrenos.email,
                    'email' AS forma, terrenos.url, terrenos.data_envio AS data, 
                    'terreno' as empreendimento, 'terreno' as tipo  
                  FROM terrenos 
                  WHERE terrenos.data_envio BETWEEN '".$min."' AND '".$max."'
                )
                ORDER BY empreendimento ASC, data DESC
                ";
      break; 
      case 2:
        $sql = "SELECT atendimentos.id_atendimento AS id, atendimentos.user_agent AS navegador, 
                       atendimentos.origem, atendimentos.nome, atendimentos.email, 
                       atendimentos.forma_contato AS forma, atendimentos.url, 
                       atendimentos.data_envio AS data, e.empreendimento, 'atendimento' as tipo 
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY atendimentos.data_envio DESC";  
      break; 
      case 3:
        $sql = "SELECT contatos.id_contato AS id, contatos.user_agent AS navegador, 
                  contatos.origem, contatos.nome, contatos.email, 
                  contatos.forma_contato AS forma, contatos.url, 
                  contatos.data_envio AS data, e.empreendimento, 'contato' as tipo 
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY contatos.data_envio DESC";
      break; 
      case 4:
        $sql = "SELECT indique.id_indique AS id, indique.user_agent AS navegador, 
                  indique.origem, indique. nome_remetente as nome, 'email' as forma, 
                  indique.email_remetente as email, indique.url, 
                  indique.data_envio AS data, e.empreendimento, 'indique' as tipo 
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY indique.data_envio DESC";
      break; 
      case 5:
          $sql = "SELECT interesses.id_interesse AS id, interesses.user_agent AS navegador, 
                    interesses.origem, interesses.nome, interesses.email, 
                    interesses.forma_contato AS forma, interesses.url, 
                    interesses.data_envio AS data, e.empreendimento, 'interesse' as tipo  
                    FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                    WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                    ORDER BY interesses.data_envio DESC";
      break;
      case 6:  
          $sql = "SELECT terrenos.id_terreno AS id, terrenos.user_agent AS navegador, 
                    terrenos.origem, terrenos.nome, terrenos.email,
                    'email' AS forma, terrenos.url, terrenos.data_envio AS data, 
                    'terreno' as empreendimento, 'terreno' as tipo  
                    FROM terrenos 
                    WHERE terrenos.data_envio BETWEEN '".$min."' AND '".$max."' 
                    ORDER BY terrenos.data_envio DESC";
      break;
    }
		$query = $this->db->query($sql);
		return $query->result();
	}     
     
	function searchFormulariosEXP($tipo,$min,$max,$id) {
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		if($id>0) $where = "id_empreendimento = '{$id}'";
    else $where = "1 = 1";
    switch($tipo) {
      default:
      case 1:
        $sql = " ( SELECT atendimentos.nome, atendimentos.email, atendimentos.telefone, 
                       atendimentos.data_envio AS data, atendimentos.descricao as mensagem,
                       null as destinatario, e.empreendimento, 'atendimento' as tipo 
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' 
                ) UNION (
                  SELECT contatos.nome, contatos.email, contatos.telefone,  
                  contatos.data_envio AS data, contatos.comentarios as mensagem, 
                  null as destinatario, e.empreendimento, 'contato' as tipo 
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT indique.nome_remetente as nome, indique.email_remetente as email, 
                  null as telefone, indique.data_envio AS data, comentarios as mensagem, 
                  indique.email_destinatario as destinatario, e.empreendimento, 'indique' as tipo 
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT interesses.nome, interesses.email, interesses.telefone,
                    interesses.data_envio AS data, interesses.comentarios as mensagem,
                    null as destinatario, e.empreendimento, 'interesse' as tipo  
                  FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                  WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                ) UNION (
                  SELECT terrenos.nome, terrenos.email, terrenos.telefone, 
                    terrenos.data_envio AS data, terrenos.observacoes as mensagem, 
                    null as destinatario, 'terreno' as empreendimento, 'terreno' as tipo  
                  FROM terrenos 
                  WHERE terrenos.data_envio BETWEEN '".$min."' AND '".$max."'
                )
                ORDER BY empreendimento ASC, data DESC
                ";
      break; 
      case 2:
        $sql = "SELECT atendimentos.nome, atendimentos.email, atendimentos.telefone, 
                       atendimentos.data_envio AS data, atendimentos.descricao as mensagem,
                       null as destinatario, e.empreendimento, 'atendimento' as tipo  
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY atendimentos.data_envio DESC";  
      break; 
      case 3:
        $sql = "SELECT contatos.nome, contatos.email, contatos.telefone,  
                  contatos.data_envio AS data, contatos.comentarios as mensagem, 
                  null as destinatario, e.empreendimento, 'contato' as tipo
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY contatos.data_envio DESC";
      break; 
      case 4:
        $sql = "SELECT indique. nome_remetente as nome, indique.email_remetente as email, 
                  null as telefone, indique.data_envio AS data, comentarios as mensagem, 
                  indique.email_destinatario as destinatario, e.empreendimento, 'indique' as tipo
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY indique.data_envio DESC";
      break; 
      case 5:
          $sql = "SELECT interesses.nome, interesses.email, interesses.telefone,
                    interesses.data_envio AS data, interesses.comentarios as mensagem,
                    null as destinatario, e.empreendimento, 'interesse' as tipo
                  FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                  WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                  ORDER BY interesses.data_envio DESC";
      break;
      case 6:  
          $sql = "SELECT terrenos.nome, terrenos.email, terrenos.telefone, 
                    terrenos.data_envio AS data, terrenos.observacoes as mensagem, 
                    null as destinatario, 'terreno' as empreendimento, 'terreno' as tipo
                  FROM terrenos 
                  WHERE terrenos.data_envio BETWEEN '".$min."' AND '".$max."' 
                  ORDER BY terrenos.data_envio DESC";
      break;
    }
		$query = $this->db->query($sql);
		return $query->result();
	}
  
	function getCorretorURL(){
		$this->db->flush_cache();
		$sql = "SELECT DISTINCT acessos_corretor.url FROM acessos_corretor ORDER BY acessos_corretor.url ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getCorretorDATA(){
		$this->db->flush_cache();
		$query = $this->db->query("SELECT Max(acessos_corretor.data_acesso) AS max, Min(acessos_corretor.data_acesso) AS min FROM acessos_corretor LIMIT 1");
		$query = $query->row();
		list($max) = explode(' ',$query->max);
		list($min) = explode(' ',$query->min);
		return array(
			'max' => strtotime($max),
			'min' => strtotime($min)
		);
	}
	function searchCorretorDATA($id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		$where = "acessos_corretor.url = '{$id}'";
		$sql = "SELECT acessos_corretor.user_agent AS navegador, acessos_corretor.origem, acessos_corretor.url FROM acessos_corretor WHERE {$where} AND acessos_corretor.data_acesso BETWEEN '".$min."' AND '".$max."' ORDER BY acessos_corretor.data_acesso DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}


}
?>