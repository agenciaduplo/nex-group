<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Obras_realizadas_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$dataAcao = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($dataAcao)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numObrasRealizadas ()
	{		
		$this->db->select('*')->from('obras_realizadas');		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numObrasRealizadasBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM obras_realizadas
				WHERE empreendimento LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#		
	
	function getObrasRealizadas ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM obras_realizadas e, empresas ep, cidades c, estados es
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				ORDER BY e.id_obra_realizada desc
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	function getObrasRealizadasSemelhantes ()
	{		
		$this->db->flush_cache();		

				
		$sql = "SELECT *
				FROM obras_realizadas e, empresas ep, cidades c, estados es
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				ORDER BY e.empreendimento ASC
				
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	#-----------------------------------------------------------------------------------#	
	
	function buscaObrasRealizadas ($keyword,$offset)
	{	
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM obras_realizadas e, empresas ep, cidades c, estados es
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.empreendimento LIKE '%$keyword%'
				ORDER BY e.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function getObraRealizadaId ($id_obra_realizada)
	{	
		$sql = "SELECT *
				FROM obras_realizadas
				WHERE id_obra_realizada = $id_obra_realizada
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}

	#-----------------------------------------------------------------------------------#	
	
	function getObraRealizada($id_obra_realizada)
	{		

		
		$this->db->start_cache();
		$this->db->select('*')->from('obras_realizadas');
		$this->db->join('cidades','cidades.id_cidade = obras_realizadas.id_cidade','INNER');
		$this->db->where('obras_realizadas.id_obra_realizada',$id_obra_realizada);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		$this->db->flush_cache();
		
		return $query->row();

		$this->db->flush_cache();
	}
		
	#-----------------------------------------------------------------------------------#	
	
	function setObraRealizada ($data, $id_obra_realizada = "")
	{		
		
		if ($id_obra_realizada)
		{
			$where = array ('id_obra_realizada' => $id_obra_realizada);
			$this->db->select('*')->from('obras_realizadas')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
			
				$this->load->library('image_lib');
					
				$upload = "";
				//=====================================================================
				//INICIO UPLOAD FACHADA
				//=====================================================================
				$config['upload_path']		= './uploads/obras_realizadas/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '6000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		  
		        $this->load->library('upload', $config);
		      	$this->upload->initialize($config);
		      	
		        if ($this->upload->do_upload('fachada'))
		        {
		        	$imagem = $this->getObraRealizadaId($id_obra_realizada);
					if($imagem->fachada)
					{
						unlink('./uploads/obras_realizadas/' . $imagem->fachada);
					}
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        //THUMB
					$config['create_thumb']     = FALSE;
					$config['source_image']		= $file_data['full_path'];
					$config['new_image']		= './uploads/obras_realizadas/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					// $config['width']			= 200; //122
					// $config['height']			= 200; //150
					
					$config['quality']			= 80;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
					
					$this->db->set('fachada', $file_name);
				}
		        
	        	//=====================================================================
				//FIM UPLOAD DO FACHADA
				//=====================================================================
				$this->db->set($data);
	            $this->db->where('id_obra_realizada', $id_obra_realizada);
	            $this->db->update('obras_realizadas');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "obras_realizadas";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso 	                	        	
			}
		}
		else
		{
			
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
			//INICIO UPLOAD FACHADA
			//=====================================================================
				$config['upload_path']		= './uploads/obras_realizadas/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '6000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		  
		        $this->load->library('upload', $config);
		      	$this->upload->initialize($config);
		      	
		        if ($this->upload->do_upload('fachada'))
		        {
		        	
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			       //THUMB
					$config['create_thumb']     = FALSE;
					$config['source_image']		= $file_data['full_path'];
					$config['new_image']		= './uploads/obras_realizadas/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 200; //111
					// $config['height']			= 200; //150
					
					$config['quality']			= 80;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
					
					$this->db->set('fachada', $file_name);
				}
		        
	        //=====================================================================
			//FIM UPLOAD DO FACHADA
			//=====================================================================
			
						
	        $this->db->set($data);
	       // $this->db->set($data);
	        $this->db->insert('obras_realizadas');
	        $idObrasRealizadas = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "obras_realizadas";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idObrasRealizadas; 	                	        	
		}
	}
	
#-----------------------------------------------------------------------------------#	

	function delObraRealizada ($id_obra_realizada)
	{
	    $this->db->where('id_obra_realizada', $id_obra_realizada);
	    $this->db->delete('obras_realizadas');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "obras_realizadas";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getEmpresas(){
		
		$this->db->select('*')->from('empresas');
		
		$query = $this->db->get();
		
		return $query->result();
		
	}
	
	function getEstados(){
		$this->db->select('*')->from('estados');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	function getCidades($id_estado){
		$this->db->select('*')->from('cidades')->where('id_estado',$id_estado);
		
		$query = $this->db->get();
		
		return $query->result();
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	