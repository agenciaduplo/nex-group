<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	function getSecoes ()
	{		
		$this->db->flush_cache();
		$this->db->select('*')->from('cms_secoes');
		return $this->db->get()->result();
	}
	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numTags ()
	{		
		$this->db->select('*')->from('tags');
		
		return $this->db->count_all_results();
	}
	
	function getTags ($offset = 0)
	{		
		$this->db->flush_cache();
		$this->db->select('*')->from('tags')->limit(20, $offset);
		$this->db->join('cms_secoes','cms_secoes.id_cms_secoes = tags.id_cms_secoes');
		return $this->db->get();
	}
	
	function buscaTags ($keyword)
	{		
		$this->db->select('*')->from('tags')->like('pagina', $keyword);
		
		return $this->db->get();
	}
	
	function getTag ($id_tag)
	{		
		$where = array ('id_tag' => $id_tag);
		
		$this->db->start_cache();
		$this->db->select('*')->from('tags')->where($where);
		$this->db->join('cms_secoes','cms_secoes.id_cms_secoes = tags.id_cms_secoes');
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setTag ($data, $id_tag = "")
	{		
		if ($id_tag)
		{
			$where = array ('id_tag' => $id_tag);
			$this->db->select('*')->from('tags')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Tag salvo com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_tag', $id_tag);
                $this->db->update('tags');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "tags";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('tags');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "tags";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 			
			
			return $this->db->insert_id();
		}
	}
	
	function delTag ($id_tag)
	{		
		$where = array ('id_tag' => $id_tag);
		$this->db->select('*')->from('tags')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_tag', $id_tag);
            $this->db->delete('tags');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "tags";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 	            
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */