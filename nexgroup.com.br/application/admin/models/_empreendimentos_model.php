<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empreendimentos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$dataAcao = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($dataAcao)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numEmpreendimentos ()
	{		
		$this->db->select('*')->from('empreendimentos');		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numEmpreendimentosBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM empreendimentos
				WHERE empreendimento LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#		
	
	function getEmpreendimentos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				ORDER BY e.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	function getEmpreendimentosSemelhantes ()
	{		
		$this->db->flush_cache();		

				
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND e.ativo = 'S'
				ORDER BY e.empreendimento ASC
				
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	#-----------------------------------------------------------------------------------#	
	
	function buscaEmpreendimentos ($keyword,$offset)
	{	
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				AND e.empreendimento LIKE '%$keyword%'
				ORDER BY e.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function getEmpreendimentoId ($id_empreendimento)
	{	
		$sql = "SELECT *
				FROM empreendimentos
				WHERE id_empreendimento = $id_empreendimento
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}

	#-----------------------------------------------------------------------------------#	
	
	function getEmpreendimento($id_empreendimento)
	{		
		$where = array ('id_empreendimento' => $id_empreendimento);
		
		$this->db->start_cache();
		$this->db->select('*')->from('empreendimentos');
		$this->db->join('cidades','cidades.id_cidade = empreendimentos.id_cidade','INNER');
		$this->db->where('empreendimentos.id_empreendimento',$id_empreendimento);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		$this->db->flush_cache();
		
		return $query->row();

		$this->db->flush_cache();
	}
		
	#-----------------------------------------------------------------------------------#	
	
	function setEmpreendimento ($data, $id_empreendimento = "")
	{		
		if ($id_empreendimento)
		{
			$where = array ('id_empreendimento' => $id_empreendimento);
			$this->db->select('*')->from('empreendimentos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
			
				$this->load->library('image_lib');
					
				$upload = "";
				//=====================================================================
				//INICIO UPLOAD LOGO
				//=====================================================================
					$config['upload_path']		= './uploads/imoveis/logo/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '10000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			      	
			        if ($this->upload->do_upload('imagem_logo'))
			        {
			        	$imagem = $this->getEmpreendimentoId($id_empreendimento);
						if($imagem->logotipo)
						{
							unlink('./uploads/imoveis/logo/' . $imagem->logotipo);
						}
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/logo/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 223;
						$config['height']			= 106;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						$this->db->set('logotipo', $file_name);
					}
			        
		        //=====================================================================
				//FIM UPLOAD DO LOGO
				//=====================================================================
					$config['upload_path']		= './uploads/imoveis/fachada/visitados/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  	
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			      	
					if ($this->upload->do_upload('imagem_visitado'))
			        {
			        	$imagem = $this->getEmpreendimentoId($id_empreendimento);
			        	if(file_exists('./uploads/imoveis/fachada/visitados/' . $imagem->imagem_visitado)){
								unlink('./uploads/imoveis/fachada/visitados/' . $imagem->imagem_visitado);
							}
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
						 //FACHADA VISITADOS
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/visitados/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 130;
						$config['height']			= 86;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();        
						
						$this->db->set('imagem_visitado', $file_name);
					}
			        	
					
					
	        	//=====================================================================
			    //INICIO UPLOAD DA FACHADA
			    //=====================================================================
					$config['upload_path']		= './uploads/imoveis/fachada/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '10000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  		
			        $this->load->library('upload', $config);
			     	$this->upload->initialize($config);
			      	
			        if ($this->upload->do_upload('imagem_fachada'))
			        {
			        	$file_data = $this->upload->data();
						
			       		$imagem = $this->getEmpreendimentoId($id_empreendimento);
						if($imagem->imagem_fachada)
						{
							if(file_exists('./uploads/imoveis/fachada/visualizar/' . $imagem->imagem_fachada)){
								unlink('./uploads/imoveis/fachada/visualizar/' . $imagem->imagem_fachada);
							}
							
							if(file_exists('./uploads/imoveis/fachada/listas/' . $imagem->imagem_fachada)){
								unlink('./uploads/imoveis/fachada/listas/' . $imagem->imagem_fachada);
							}
						}
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //FACHADA VISUALIZAR
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/visualizar/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 223;
						$config['height']			= 275;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						
						//FACHADA LISTAGENS
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/listas/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 170;
						$config['height']			= 189;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();	
						
						$this->db->set('imagem_fachada', $file_name);
						
						if(file_exists('./uploads/imoveis/fachada/' . $file_name)){
							unlink('./uploads/imoveis/fachada/'.$file_name);	        
						}
			        }
	        			
	        //=====================================================================
			//FIM UPLOAD DA FACHADA
			//=====================================================================
	            $this->db->set($data);
	            $this->db->where('id_empreendimento', $id_empreendimento);
	            $this->db->update('empreendimentos');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "empreendimentos";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso 	                	        	
			}
		}
		else
		{
			
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
			//INICIO UPLOAD DO LOGO
			//=====================================================================
					$config['upload_path']		= './uploads/imoveis/logo/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '10000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  	
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			        
			      	if ($this->upload->do_upload('logotipo'))
			        {
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/logo/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 223;
						$config['height']			= 106;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();	
						$this->db->set('logotipo', $file_name);	        
					}
	        			
	        //=====================================================================
			//FIM UPLOAD DO LOGO
			//=====================================================================
			
					$config['upload_path']		= './uploads/imoveis/fachada/visitados/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '10000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  	
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			      	
				if ($this->upload->do_upload('imagem_visitado'))
			        {
			        	$imagem = $this->getEmpreendimentoId($id_empreendimento);
			        	if(file_exists('./uploads/imoveis/fachada/visitados/' . $imagem->imagem_visitado)){
								unlink('./uploads/imoveis/fachada/visitados/' . $imagem->imagem_visitado);
							}
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
						 //FACHADA VISITADOS
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/visitados/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 130;
						$config['height']			= 86;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();        
						
						$this->db->set('imagem_visitado', $file_name);
					}		
					
	       	//=====================================================================
			//INICIO UPLOAD DA FACHADA
			//=====================================================================
					$config['upload_path']		= './uploads/imoveis/fachada/';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '10000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			      	
			        if ($this->upload->do_upload('imagem_fachada'))
			        {
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //FACHADA VISUALIZAR
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/visualizar/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 223;
						$config['height']			= 275;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						//FACHADA LISTAGENS
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/imoveis/fachada/listas/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 170;
						$config['height']			= 189;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$this->db->set('imagem_fachada', $file_name);

						if(file_exists('./uploads/imoveis/fachada/' . $file_name)){
							unlink('./uploads/imoveis/fachada/'.$file_name);	        
						}	        
					}
	        			
	        //=====================================================================
			//FIM UPLOAD DA FACHADA
			//=====================================================================
			
	        $this->db->set($data);
	       // $this->db->set($data);
	        $this->db->insert('empreendimentos');
	        $idEmpreendimentos = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "empreendimentos";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idEmpreendimentos; 	                	        	
		}
	}
	
	function setEmpreendimentoMidia($data, $id_empreedimento,$tipoArquivo,$tipoMidia)
	{
		$this->load->library('image_lib');
		
		if($tipoArquivo == 'I'){
			//UPLOAD
	        $config['upload_path']		= './uploads/imoveis/midias/';
	        $config['allowed_types']	= 'gif|jpg|png|bmp';
	        $config['max_size']			= '10000';
	        $config['max_width']		= '4000';
	        $config['max_height']		= '4000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('foto'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        
				$config['create_thumb']     = FALSE;
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './uploads/imoveis/midias/'. $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['width']			= 800;
				$config['height']			= 600;
				$config['quality']			= 100;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();		        
		        
		        
								
				$size = getimagesize('./uploads/imoveis/midias/' . $file_name);
						
				if($size[0] >  $size[1]){
					
					// THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/midias/thumbs/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 140;
					$config['height']			= 105;
					$config['quality']			= 100;			
							
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		
				}else{
					$config['create_thumb']     = TRUE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/midias/thumbs/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 140;
					$config['height']			= 105;
					$config['quality']			= 100;
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}
						
				/*$config['image_library'] 	= 'GD2';
				$config['source_image']		= './uploads/imoveis/midias/thumbs/' . $file_name;
				$config['x_axis'] 			= 10;
				$config['y_axis'] 			= 10;
				$config['width']  			= 140;
				$config['height'] 			= 95;
				$config['maintain_ratio'] 	= FALSE;
		
				$this->image_lib->initialize($config);
				//$this->image_lib->crop();*/
				
				$this->db->set('arquivo', $file_name);
				
			    //Log Acesso			
				
	        }
	        else
	        {
	        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
	        }
	    }else{
	    	$this->db->set('arquivo', $this->input->post('link'));
	    }
	    $this->db->set($data)->insert('midias');

	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Midias";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
#-----------------------------------------------------------------------------------#	
/*INICIO POSICIONAMENTO FOTOS MIDIAS*/
	
	#-----------------------------------------------------------------------------------#	
	function getPosicaoMidias($id_foto)
	{
		$sql =  "SELECT posicao 
				 FROM midias
				 WHERE id_midia = $id_foto
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getProxPosicaoMidias($posicao,$id_tipo_midia,$id_empreendimento)
	{
		$sql =  "SELECT posicao 
				 FROM midias
				 WHERE id_tipo_midia = $id_tipo_midia AND id_empreendimento = $id_empreendimento AND posicao = $posicao
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getMaxPosicaoMidias($id_tipo_midia,$id_empreendimento)
	{
		$this->db->select_max('posicao');
		$query = $this->db->get_where('midias',array('id_empreendimento' => $id_empreendimento,'id_tipo_midia' => $id_tipo_midia));	
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function getMinPosicaoMidias($id_tipo_midia,$id_empreendimento)
	{
		$this->db->select_min('posicao');
		$query = $this->db->get_where('midias',array('id_empreendimento' => $id_empreendimento,'id_tipo_midia' => $id_tipo_midia));		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function trocaPosicaoMidias($id_atual,$pos_atual, $prox_pos,$id_tipo_midia,$id_empreendimento)
	{
		
		#Pega ID proxima linha#
		$sql =  "SELECT id_midia
				 FROM midias
				 WHERE id_tipo_midia = $id_tipo_midia AND id_empreendimento = $id_empreendimento AND posicao = $prox_pos
				";
		$query = $this->db->query($sql);		
		$id_prox = $query->row('id_midia');
		
		#Insere posicao atual na proxima linha#
		if($id_prox){
			$this->db->where('id_midia', $id_prox);
			$this->db->set('posicao',$pos_atual)->update('midias');
		}
		
		#Insere posicao anterior na linha atual#
		$this->db->where('id_midia', $id_atual);
		$this->db->set('posicao',$prox_pos)->update('midias');
	}	
#-----------------------------------------------------------------------------------#
	
/*FIM POSICIONAMENTO FOTOS MIDIAS*/	
	
	
	
	function setItem($data, $id_item)
	{
		if($id_item)
		{
			$this->db->set($data)->where('id_item', $id_item)->update('itens');
			$acao = "update";
			$this->session->set_flashdata('respostaItens', 'Item atualizado com sucesso!');	
		}
		else
		{
			$this->db->set($data)->insert('itens');
			$acao = "insert";
			$this->session->set_flashdata('respostaItens', 'Item inserido com sucesso!');	
		}
		
		//Log Acesso
		    $tabela 	= "Itens";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
/* INICIO POSICIONAMENTO DOS ITENS*/
#-----------------------------------------------------------------------------------#	
	function getPosicao($id_item)
	{
		$sql =  "SELECT posicao 
				 FROM itens
				 WHERE id_item = $id_item
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getProxPosicao($posicao,$id_empreendimento)
	{
		$sql =  "SELECT posicao 
				 FROM itens
				 WHERE id_empreendimento = $id_empreendimento AND posicao = $posicao
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getMaxPosicao($id_empreendimento)
	{
		$this->db->select_max('posicao');
		$query = $this->db->get_where('itens',array('id_empreendimento' => $id_empreendimento));	
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function getMinPosicao($id_empreendimento)
	{
		$this->db->select_min('posicao');
		$query = $this->db->get_where('itens',array('id_empreendimento' => $id_empreendimento));		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function trocaPosicao($id_atual,$pos_atual, $prox_pos,$id_empreendimento)
	{
		
		#Pega ID proxima linha#
		$sql =  "SELECT id_item 
				 FROM itens
				 WHERE id_empreendimento = $id_empreendimento AND posicao = $prox_pos
				";
		$query = $this->db->query($sql);		
		$id_prox = $query->row('id_item');
		
		#Insere posicao atual na proxima linha#
		if($id_prox){
			$this->db->where('id_item', $id_prox);
			$this->db->set('posicao',$pos_atual)->update('itens');
		}
		
		#Insere posicao anterior na linha atual#
		$this->db->where('id_item', $id_atual);
		$this->db->set('posicao',$prox_pos)->update('itens');
	}	
#-----------------------------------------------------------------------------------#
/*FIM POSICIONAMENTO DOS ITENS*/
	
/*INICIO POSICIONAMENTO FOTOS DAS FASES*/
	
	#-----------------------------------------------------------------------------------#	
	function getPosicaoFotosFases($id_foto)
	{
		$sql =  "SELECT posicao 
				 FROM fotos_fases
				 WHERE id_foto_fase = $id_foto
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getProxPosicaoFotosFases($posicao,$id_fase,$id_empreendimento)
	{
		$sql =  "SELECT posicao 
				 FROM fotos_fases
				 WHERE id_fase = $id_fase AND posicao = $posicao
				";
		
		$query = $this->db->query($sql);		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#	
	function getMaxPosicaoFotosFases($id_fase,$id_empreendimento)
	{
		$this->db->select_max('posicao');
		$query = $this->db->get_where('fotos_fases',array('id_fase' => $id_fase));	
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function getMinPosicaoFotosFases($id_fase,$id_empreendimento)
	{
		$this->db->select_min('posicao');
		$query = $this->db->get_where('fotos_fases',array('id_fase' => $id_fase));		
		return $query->row('posicao');
	}
#-----------------------------------------------------------------------------------#
	function trocaPosicaoFotosFases($id_atual,$pos_atual, $prox_pos,$id_fase,$id_empreendimento)
	{
		
		#Pega ID proxima linha#
		$sql =  "SELECT id_foto_fase 
				 FROM fotos_fases
				 WHERE id_fase = $id_fase AND posicao = $prox_pos
				";
		$query = $this->db->query($sql);		
		$id_prox = $query->row('id_foto_fase');
		
		#Insere posicao atual na proxima linha#
		if($id_prox){
			$this->db->where('id_foto_fase', $id_prox);
			$this->db->set('posicao',$pos_atual)->update('fotos_fases');
		}
		
		#Insere posicao anterior na linha atual#
		$this->db->where('id_foto_fase', $id_atual);
		$this->db->set('posicao',$prox_pos)->update('fotos_fases');
	}	
#-----------------------------------------------------------------------------------#
	
/*FIM POSICIONAMENTO FOTOS DAS FASES*/	
	
#-----------------------------------------------------------------------------------#	
	
	function setFase($data, $id_fase)
	{
		if($id_fase)
		{
			$this->db->set($data)->where('id_fase', $id_fase)->update('fases');
			$acao = "update";
			$this->session->set_flashdata('respostaFases', 'Fase atualizada com sucesso!');	
		}
		else
		{
			$this->db->set($data)->insert('fases');
			$acao = "insert";
			$this->session->set_flashdata('respostaFases', 'Fase inserida com sucesso!');	
		}
		
		//Log Acesso
		    $tabela 	= "Fases";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
#-----------------------------------------------------------------------------------#	
	
	function setItemFase($data, $id_item_fase)
	{
		if($id_item_fase)
		{
			$this->db->set($data)->where('id_item_fase', $id_item_fase)->update('itens_fases');
			$acao = "update";
			$this->session->set_flashdata('respostaItensFases', 'Itens Fase atualizada com sucesso!');	
		}
		else
		{
			$this->db->set($data)->insert('itens_fases');
			$acao = "insert";
			$this->session->set_flashdata('respostaItensFases', 'Itens Fase inserida com sucesso!');	
		}
		
		//Log Acesso
		    $tabela 	= "Fases";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
	#-----------------------------------------------------------------------------------#	
	function delDormitorios($id_empreendimento){
		$this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->delete('emps_dormitorios');
		
	}
	function setDormitorios($data,$id_empreendimento)
	{
			$this->db->set($data)->insert('emps_dormitorios');
			$acao = "insert";
			$this->session->set_flashdata('respostaDormitorios', 'Dormitorios inseridos com sucesso!');	
		
		//Log Acesso
		    $tabela 	= "Dormitorios";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
	#-----------------------------------------------------------------------------------#	
	function delBanheiros($id_empreendimento){
		$this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->delete('emps_banheiros');
		
	}
	function setBanheiros($data,$id_empreendimento)
	{
			$this->db->set($data)->insert('emps_banheiros');
			$acao = "insert";
			$this->session->set_flashdata('respostaBanheiros', 'Banheiros inseridos com sucesso!');	
		
		//Log Acesso
		    $tabela 	= "Banheiros";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
	#-----------------------------------------------------------------------------------#
	
	function delFaixaPreco($id_empreendimento){
		$this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->delete('emps_precos');
		
	}
	function setFaixaPreco($data,$id_empreendimento)
	{
			$this->db->set($data)->insert('emps_precos');
			$acao = "insert";
			$this->session->set_flashdata('respostaFaixaPreco', 'Faixas de Preços  inseridas com sucesso!');	
		
		//Log Acesso
		    $tabela 	= "Faixas de preço";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
	#-----------------------------------------------------------------------------------#
	
	function delSemelhantes($id_empreendimento){
		$this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->delete('empreendimentos_semelhantes');
		
	}
	
	function setSemelhante($data,$id_empreendimento)
	{
			$this->db->set($data)->insert('empreendimentos_semelhantes');
			$acao = "insert";
			$this->session->set_flashdata('respostaFaixaPreco', 'Empreendimentos Semelhantes inseridos com sucesso!');	
		
		//Log Acesso
		    $tabela 	= "Empreendimentos Semelhantes";
		    $sql 		= $this->db->last_query();
		    $this->model->inserirLogAcoes($tabela, $acao, $sql);
		//Log Acesso				
	}
	
	#-----------------------------------------------------------------------------------#
	
	function setDestaque($data,$id_destaque ='')
	{
		if($id_destaque){
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/imoveis/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '10000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('foto'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 992;
					$config['height']			= 425;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }

	       $this->db->set($data)->update('destques_empreendimentos');
	       	
		}else{
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/imoveis/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '5000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('foto'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 992;
					$config['height']			= 425;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }

	       $this->db->set($data)->insert('destques_empreendimentos');
		}
	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Destaques Empreendimentos";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
	
	function setMidiaFases($data)
	{
		$this->load->library('image_lib');
		
			//UPLOAD
	        $config['upload_path']		= './uploads/imoveis/fases/';
	        $config['allowed_types']	= 'gif|jpg|png|bmp';
	        $config['max_size']			= '10000';
	        $config['max_width']		= '4000';
	        $config['max_height']		= '4000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('foto'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        
				$config['create_thumb']     = FALSE;
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './uploads/imoveis/fases/'. $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['width']			= 800;
				$config['height']			= 600;
				$config['quality']			= 100;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();		        
		        
		        $size = getimagesize('./uploads/imoveis/fases/' . $file_name);
						
				if($size[0] >  $size[1]){
					
					// THUMB
					$config['create_thumb']     = TRUE;
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/fases/thumbs/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 200;
					$config['height']			= 105;
					$config['quality']			= 100;			
							
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		
				}else{
					$config['create_thumb']     = TRUE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/fases/thumbs/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 190;
					$config['width']			= 150;
					
					$config['quality']			= 100;
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}
						
				$config['image_library'] 	= 'GD2';
				$config['source_image']		= './uploads/imoveis/fases/thumbs/' . $file_name;
				$config['x_axis'] 			= 10;
				$config['y_axis'] 			= 10;
				$config['width']  			= 130;
				$config['height'] 			= 90;
				$config['maintain_ratio'] 	= FALSE;
		
				$this->image_lib->initialize($config);
				$this->image_lib->crop();
				
				
				/*// THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './uploads/imoveis/fases/thumbs/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 95;
				$config['width']			= 140;
				$config['quality']			= 100;			
						
				$this->image_lib->initialize($config);
				$this->image_lib->resize();		*/		
				
				$this->db->set('imagem', $file_name);
				
			    //Log Acesso			
				
	        }
	        else
	        {
	        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
	        }

	       $this->db->set($data)->insert('fotos_fases');

	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Midias";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
	
#-----------------------------------------------------------------------------------#	

	function delEmpreendimento ($id_empreendimento)
	{
	    $this->db->where('id_empreendimento', $id_empreendimento);
	    $this->db->delete('empreendimentos');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "empreendimentos";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function delMidia($id_midia)
	{
		$this->db->where('id_midia', $id_midia);
        $this->db->delete('midias');

        
	}
	
	function delFotoFase($id_foto_fase)
	{
		$this->db->where('id_foto_fase', $id_foto_fase);
        $this->db->delete('fotos_fases');
	}
	
	function delItem ($id_item)
	{
	    $this->db->where('id_item', $id_item);
	    $this->db->delete('itens');

	    $this->session->set_flashdata('respostaItem', 'Item deletada!');	
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "Itens";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function delItemFase ($id_item_fase)
	{
	    $this->db->where('id_item_fase', $id_item_fase);
	    $this->db->delete('itens_fases');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "item fases";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function delFase ($id_fase)
	{
		$imagens = $this->getFotosFases($id_fase);
		foreach ($imagens as $row):
			if($row->imagem):
				if(file_exists('./uploads/imoveis/fases/' . $row->imagem)){
					unlink('./uploads/imoveis/fases/' . $row->imagem);
				}
				if(file_exists('./uploads/imoveis/fases/thumbs/' . $row->imagem)){
					unlink('./uploads/imoveis/fases/thumbs/' . $row->imagem);
				}
			endif;
		endforeach;
		$this->db->where('id_fase', $id_fase);
	    $this->db->delete('fases');

	    $this->session->set_flashdata('respostaFases', 'Fase deletada!');	
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "Fases";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	#-----------------------------------------------------------------------------------#	
		
	function getFases($id_empreendimento){
		
		$this->db->flush_cache();
		
		$this->db->select('*')->from('fases');
		$this->db->join('status','fases.id_status = status.id_status','INNER');
		$this->db->order_by('titulo', 'asc');
		$this->db->where('id_empreendimento',$id_empreendimento);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getFase($id_fase,$id_empreendimento){
		
		$this->db->flush_cache();
		
		$this->db->select('*')->from('fases');
		$this->db->where('id_empreendimento',$id_empreendimento);
		$this->db->where('id_fase',$id_fase);
		
		$query = $this->db->get();
		
		return $query->row();
	}
		
	function getItensFases($id_empreendimento){
		
		$this->db->flush_cache();
		
		$this->db->select('itens_fases.titulo, fases.titulo as fase, porcentagem,id_item_fase, itens_fases.id_fase')->from('itens_fases');
		$this->db->join('fases','fases.id_fase = itens_fases.id_fase','INNER');
		$this->db->order_by('titulo', 'asc');
		$this->db->where('id_empreendimento',$id_empreendimento);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getItens($id_empreendimento){
		
		$this->db->flush_cache();
		
		$this->db->select('*')->from('itens');
		$this->db->where('id_empreendimento',$id_empreendimento)->order_by('posicao','asc');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getItem($id_item){
		
		$this->db->select('*')->from('itens');
		$this->db->where('id_item',$id_item);
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
	function getItemFaseId($id_item){
		
		$this->db->select('*')->from('itens_fases');
		$this->db->where('id_item_fase',$id_item);
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
	
	function getFasesFotos ($id_empreendimento)
	{		
		$sql =  "SELECT DISTINCT (m.id_fase) AS id_fase,  f.titulo as fase
				FROM fotos_fases m, fases f
				WHERE m.id_fase = f.id_fase
				AND f.id_empreendimento =".$id_empreendimento."
				ORDER BY fase ASC";
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getFotosFases ($id_fase)
	{		
		$sql =  "SELECT * FROM fotos_fases
				WHERE id_fase = $id_fase
				ORDER BY posicao ASC			
				";
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getTiposFotos ($id_empreendimento)
	{		
		$sql =  "SELECT DISTINCT (m.id_tipo_midia) AS id_midia,  tp.tipo_midia as tipo
				FROM midias m, tipos_midias tp
				WHERE m.id_tipo_midia = tp.id_tipo_midia
				AND m.id_empreendimento =".$id_empreendimento."
				ORDER BY tp.tipo_midia ASC";
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getFotos ($id_empreendimento,$id_tipo)
	{		
		$sql =  "SELECT * FROM midias
				WHERE id_tipo_midia = $id_tipo AND id_empreendimento = $id_empreendimento
				ORDER BY posicao ASC
				";
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getFotoFase($id_foto_fase){
		$this->db->select('*')->from('fotos_fases')->where('id_foto_fase',$id_foto_fase);
		$query = $this->db->get();
		
		return $query->row();
	}
	
	function getMidia($id_midia){
		$this->db->select('*')->from('midias')->where('id_midia',$id_midia);
		$query = $this->db->get();
		
		return $query->row();
	}
	
	function getTiposMidias(){
		$this->db->select('*')->from('tipos_midias');
		$this->db->order_by('tipo_midia','asc');
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getDormitorios(){
		$this->db->select('*')->from('dormitorios');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getDormitoriosAtribuidos($id_empreendimento){
		$this->db->select('id_dormitorio')->from('emps_dormitorios')->where('id_empreendimento',$id_empreendimento);
		$query = $this->db->get();
		
		return $query->result();
	}

		
	function getBanheiros(){
		$this->db->select('*')->from('banheiros');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getBanheirosAtribuidos($id_empreendimento){
		$this->db->select('*')->from('emps_banheiros')->where('id_empreendimento',$id_empreendimento);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getFaixaPrecos(){
		$this->db->select('*')->from('faixas_precos');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getFaixaPrecosAtribuidos($id_empreendimento){
		$this->db->select('*')->from('emps_precos')->where('id_empreendimento',$id_empreendimento);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getSemelhantesAtribuidos($id_empreendimento){
		$this->db->select('*')->from('empreendimentos_semelhantes')->where('id_empreendimento',$id_empreendimento);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getStatus(){
		$this->db->select('*')->from('status');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getEmpresas(){
		
		$this->db->select('*')->from('empresas');
		
		$query = $this->db->get();
		
		return $query->result();
		
	}
	
	function getTipos_imoveis(){
		$this->db->select('*')->from('tipos_imoveis');
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getEstados(){
		$this->db->select('*')->from('estados');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function getCidades($id_estado){
		$this->db->select('*')->from('cidades')->where('id_estado',$id_estado);
		
		$query = $this->db->get();
		
		return $query->result();
	}	
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	