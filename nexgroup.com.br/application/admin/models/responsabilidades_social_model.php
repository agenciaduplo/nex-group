<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Responsabilidades_social_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
#-----------------------------------------------------------------------------------#
		
	function numResponsabilidadesSocial()
	{		
		$this->db->select('*')->from('responsabilidades_social');		
		
		return $this->db->count_all_results();
	}

#-----------------------------------------------------------------------------------#

	function numResponsabilidadesSocialBusca($keyword)
	{		
		$sql = "SELECT *
				FROM responsabilidades_social
				WHERE nome LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}

#-----------------------------------------------------------------------------------#		
	function getResponsabilidadesSocial($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM responsabilidades_social
				ORDER BY data_cadastro desc
				LIMIT $offset, 20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function buscaResponsabilidadesSocial($keyword)
	{	
		$sql = "SELECT *
				FROM responsabilidades_social
				WHERE nome LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function getResponsabilidadeSocialId($id_responsabilidade)
	{	
		$sql = "SELECT *
				FROM responsabilidades_social
				WHERE id_responsabilidade = $id_responsabilidade
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}


#-----------------------------------------------------------------------------------#	
	function getResponsabilidadeSocial($id_responsabilidade)
	{		
		$where = array ('id_responsabilidade' => $id_responsabilidade);
		
		$this->db->start_cache();
		$this->db->select('*')->from('responsabilidades_social')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
#-----------------------------------------------------------------------------------#	
	function setResponsabilidadeSocial($data, $id_responsabilidade = "")
	{		
		if($id_responsabilidade)
		{
			$where = array ('id_responsabilidade' => $id_responsabilidade);
			$this->db->select('*')->from('responsabilidades_social')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
			
				$this->load->library('image_lib');
					
				$upload = "";
				//=====================================================================
				//INICIO UPLOAD FOTO DESTAQUE
				//=====================================================================
					$config['upload_path']		= './uploads/responsabilidades_social/zoom';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	
			        if ($this->upload->do_upload('imagem'))
			        {
			        	$imagem = $this->getImagem($id_responsabilidade);
						if($imagem->imagem_destaque)
						{
							unlink('./uploads/responsabilidades_social/zoom/' . $imagem->imagem_destaque);
							unlink('./uploads/responsabilidades_social/medium/' . $imagem->imagem_destaque);
							unlink('./uploads/responsabilidades_social/thumb/' . $imagem->imagem_destaque);	
						}
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/responsabilidades_social/zoom/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 800;
						$config['height']			= 600;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						//MEDIUM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/responsabilidades_social/medium/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 324;
						$config['height']			= 258;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$size = getimagesize('./uploads/responsabilidades_social/medium/' . $file_name);
						
						if($size[0] >  $size[1]){
							
							//THUMB
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/responsabilidades_social/thumb/' . $file_name;
							$config['maintain_ratio']	= true;
							$config['width']			= 120;
							$config['height']			= 120;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}else{
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/responsabilidades_social/thumb/' . $file_name;
							$config['maintain_ratio']	= TRUE;
							$config['width']			= 180;
							$config['height']			= 180;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}
						
						$config['image_library'] 	= 'gd2';
						$config['source_image'] 	= './uploads/responsabilidades_social/thumb/' . $file_name;
						$config['x_axis'] 			= 10;
						$config['y_axis'] 			= 10;
						$config['height'] 			= 70;
						$config['width']  			= 70;
						$config['maintain_ratio'] 	= FALSE;
				
						$this->image_lib->initialize($config);
						$this->image_lib->crop();
						
						$this->db->set('imagem_destaque', $file_name);
					}
			        
		        //=====================================================================
				//FIM UPLOAD DA FOTO DESTAQUE
				//=====================================================================
			
	        	
	            $this->db->set($data);
	            $this->db->where('id_responsabilidade', $id_responsabilidade);
	            $this->db->update('responsabilidades_social');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "responsabilidades_social";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso 	                	        	
			}
		}
		else
		{
			
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
			//INICIO UPLOAD DO FOTO DESTAQUE
			//=====================================================================
					$config['upload_path']		= './uploads/responsabilidades_social/zoom';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      
			        if ($this->upload->do_upload('imagem'))
			        {
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/responsabilidades_social/zoom/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 800;
						$config['height']			= 600;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						//MEDIUM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/responsabilidades_social/medium/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 324;
						$config['height']			= 258;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$size = getimagesize('./uploads/responsabilidades_social/medium/' . $file_name);
						
						if($size[0] >  $size[1]){
							
							//THUMB
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/responsabilidades_social/thumb/' . $file_name;
							$config['maintain_ratio']	= true;
							$config['width']			= 100;
							$config['height']			= 100;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}else{
							$config['create_thumb']     = TRUE;
							$config['source_image']	    = $file_data['full_path'];
							$config['new_image']		= './uploads/responsabilidades_social/thumb/' . $file_name;
							$config['maintain_ratio']	= TRUE;
							$config['width']			= 180;
							$config['height']			= 180;
							$config['quality']			= 100;
							
							$this->image_lib->initialize($config);
							$this->image_lib->resize();
						}
						
						$config['image_library'] 	= 'gd2';
						$config['source_image'] 	= './uploads/responsabilidades_social/thumb/' . $file_name;
						$config['x_axis'] 			= 10;
						$config['y_axis'] 			= 10;
						$config['height'] 			= 70;
						$config['width']  			= 70;
						$config['maintain_ratio'] 	= FALSE;
				
						$this->image_lib->initialize($config);
						$this->image_lib->crop();
					}
	        			
	        //=====================================================================
			//FIM UPLOAD DA FOTO DESTAQUE
			//=====================================================================
			
	        $this->db->set('imagem_destaque', $file_name)->set($data);
	       // $this->db->set($data);
	        $this->db->insert('responsabilidades_social');
	        $idResponsabilidadesSocial = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "responsabilidades_social";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idResponsabilidadesSocial; 	                	        	
		}
	}
	
	function delResponsabilidadeSocial($id_responsabilidade)
	{
	    $this->db->where('id_responsabilidade', $id_responsabilidade);
	    $this->db->delete('responsabilidades_social');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "responsabilidades_social";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	function getImagens($id_responsabilidade)
	{
		$sql = "SELECT *
				FROM responsabilidades_social_fotos
				WHERE id_responsabilidade = $id_responsabilidade
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	function setImagem($data)
	{
		
		$this->load->library('image_lib');
		
			$config['upload_path']		= './uploads/responsabilidades_social/galeria';
	        $config['allowed_types']	= 'gif|jpg|png|bmp';
	        $config['max_size']			= '6000';
	        $config['max_width']		= '4000';
	        $config['max_height']		= '4000';
	        $config['encrypt_name']     = TRUE;
	  
	        $this->load->library('upload', $config);
	      
	        if ($this->upload->do_upload('imagem'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        //ZOOM
				$config['create_thumb']     = FALSE;
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './uploads/responsabilidades_social/galeria/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['width']			= 800;
				$config['height']			= 600;
				
				$config['quality']			= 100;
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize();		        
				
				$size = getimagesize('./uploads/responsabilidades_social/galeria/' . $file_name);
				
				if($size[0] >  $size[1]){
					
					//THUMB
					$config['create_thumb']     = false;
					$config['source_image']	    = './uploads/responsabilidades_social/galeria/' . $file_name;
					$config['new_image']		= './uploads/responsabilidades_social/galeriaThumb/' . $file_name;
					$config['maintain_ratio']	= true;
					$config['width']			= 145;
					$config['height']			= 145;
					$config['quality']			= 100;
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}else{
					$config['create_thumb']     = false;
					$config['source_image']	    = './uploads/responsabilidades_social/galeria/' . $file_name;
					$config['new_image']		= './uploads/responsabilidades_social/galeriaThumb/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 140;
					$config['height']			= 140;
					$config['quality']			= 100;
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}
				
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= './uploads/responsabilidades_social/galeriaThumb/' . $file_name;
				$config['x_axis'] 			= 1;
				$config['y_axis'] 			= 1;
				$config['height'] 			= 70;
				$config['width']  			= 70;
				$config['maintain_ratio'] 	= FALSE;
		
				$this->image_lib->initialize($config);
				$this->image_lib->crop();
			
			
			$this->db->set('foto', $file_name)->set($data)->insert('responsabilidades_social_fotos');
			//Log Acesso
		    	$acao 		= "insert";
		    	$tabela 	= "responsabilidades_social_fotos";
		    	$sql 		= $this->db->last_query();
		    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		    //Log Acesso			
			
	        }
	        else
	        {
	        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
	        }
	}
#-----------------------------------------------------------------------------------#
	function getImagem ($id_responsabilidade)
	{		
		$this->db->start_cache();
		$this->db->select('*')->from('responsabilidades_social')->where('id_responsabilidade', $id_responsabilidade);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}		
#-----------------------------------------------------------------------------------#	
	
	function getImagemGaleria($id_foto)
	{		
		$this->db->start_cache();
		$this->db->select('*')->from('responsabilidades_social_fotos')->where('id_foto', $id_foto);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}	

#-----------------------------------------------------------------------------------#	
	function delImagem($id_foto='', $id_responsabilidade= '')
	{		
		if($id_responsabilidade){
			$where = array ('id_responsabilidade' => $id_responsabilidade);
			$this->db->select('*')->from('responsabilidades_social_fotos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
	            $this->db->where('id_responsabilidade', $id_responsabilidade);
	            $this->db->delete('responsabilidades_social_fotos');
	            
				//Log Acesso
			    	$acao 		= "delete";
			    	$tabela 	= "responsabilidades_social_fotos";
			    	$sql 		= $this->db->last_query();
			    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
			    //Log Acesso            
			}
		}else{
			$where = array ('id_foto' => $id_foto);
			$this->db->select('*')->from('responsabilidades_social_fotos')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
	            $this->db->where('id_foto', $id_foto);
	            $this->db->delete('responsabilidades_social_fotos');
	            
				//Log Acesso
			    	$acao 		= "delete";
			    	$tabela 	= "responsabilidades_social_fotos";
			    	$sql 		= $this->db->last_query();
			    	$this->model->inserirLogAcoes($tabela, $acao, $sql);
			    //Log Acesso            
			}
		}
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	