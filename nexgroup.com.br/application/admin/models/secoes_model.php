<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secoes_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numSecoes ()
	{		
		$this->db->select('*')->from('cms_secoes');
		
		return $this->db->count_all_results();
	}
	
	function getSecoes ($offset = 0)
	{		
		$this->db->flush_cache();
		$this->db->select('*')->from('cms_secoes')->limit(20, $offset);
		
		return $this->db->get();
	}
	
	function buscaSecoes ($keyword)
	{		
		$this->db->select('*')->from('cms_secoes')->like('nome', $keyword);
		
		return $this->db->get();
	}
	
	function getSecao ($id_cms_secoes)
	{		
		$where = array ('id_cms_secoes' => $id_cms_secoes);
		
		$this->db->start_cache();
		$this->db->select('*')->from('cms_secoes')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setSecao ($data, $id_cms_secoes = "")
	{		
		if ($id_cms_secoes)
		{
			$where = array ('id_cms_secoes' => $id_cms_secoes);
			$this->db->select('*')->from('cms_secoes')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Secao salvo com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_cms_secoes', $id_cms_secoes);
                $this->db->update('cms_secoes');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "cms_secoes";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('cms_secoes');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "cms_secoes";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 			
			
			return $this->db->insert_id();
		}
	}
	
	function delSecao ($id_cms_secoes)
	{		
		$where = array ('id_cms_secoes' => $id_cms_secoes);
		$this->db->select('*')->from('cms_secoes')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_cms_secoes', $id_cms_secoes);
            $this->db->delete('cms_secoes');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "cms_secoes";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 	            
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */