<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_Proximidades_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numTipos_Proximidades ()
	{		
		$this->db->select('*')->from('tipos_proximidades');		
		
		return $this->db->count_all_results();
	}
	
	function getTipos_Proximidades ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM tipos_proximidades
				ORDER BY tipo_proximidade ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function buscaTipos_Proximidades ($keyword)
	{		
		
		$sql = "SELECT *
				FROM tipos_proximidades 
				WHERE tipo_proximidade LIKE '%$keyword%'
				ORDER BY tipo_proximidade ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getTipoProximidade ($id_tipo_proximidade)
	{		
		$where = array ('id_tipo_proximidade' => $id_tipo_proximidade);
		
		$this->db->start_cache();
		$this->db->select('*')->from('tipos_proximidades')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setTipoProximidade ($data, $id_tipo_proximidade = "")
	{		
		if ($id_tipo_proximidade)
		{
			$where = array ('id_tipo_proximidade' => $id_tipo_proximidade);
			$this->db->select('*')->from('tipos_proximidades')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Tipo proximidade salvo com sucesso!');
				
				
				$this->load->library('image_lib');
				$upload = "";
				//=====================================================================
				//INICIO PIN GOOGLE MAPS
				//=====================================================================
				$config['upload_path']		= './uploads/tipos_proximidades/pin/';
				$config['allowed_types']	= 'gif|jpg|png';
				$config['max_size']			= '6000';
				$config['max_width']		= '4000';
				$config['max_height']		= '4000';
				$config['encrypt_name']     = TRUE;
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				if ($this->upload->do_upload('imagem_pin'))
				{
					$imagem = $this->getTipoProximidade($id_tipo_proximidade);
					if($imagem->pin)
					{
						unlink('./uploads/tipos_proximidades/pin/' . $imagem->pin);
					}
					$file_data = $this->upload->data();
						
					$file_name = $file_data['file_name'];
					$file_size = $file_data['file_size'];
						
					//ZOOM
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/tipos_proximidades/pin/' . $file_name;
					$config['maintain_ratio']	= false;
// 					$config['width']			= 95;
// 					$config['height']			= 64;
						
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
						
					$this->db->set('pin', $file_name);
				}
				//=====================================================================
				//FIM PIN GOOGLE MAPS
				//=====================================================================
				
				
				$this->db->set($data);
                $this->db->where('id_tipo_proximidade', $id_tipo_proximidade);
                $this->db->update('tipos_proximidades');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "tipos_proximidades";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
			//INICIO PIN GOOGLE MAPS
			//=====================================================================
			$config['upload_path']		= './uploads/tipos_proximidades/pin/';
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '6000';
			$config['max_width']		= '4000';
			$config['max_height']		= '4000';
			$config['encrypt_name']     = TRUE;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if ($this->upload->do_upload('imagem_pin'))
			{
				$file_data = $this->upload->data();
			
				$file_name = $file_data['file_name'];
				$file_size = $file_data['file_size'];
			
				//ZOOM
				$config['create_thumb']     = FALSE;
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './uploads/tipos_proximidades/pin/' . $file_name;
				$config['maintain_ratio']	= false;
// 				$config['width']			= 95;
// 				$config['height']			= 64;
			
				$config['quality']			= 100;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			
				$this->db->set('pin', $file_name);
			}
			//=====================================================================
			//FIM PIN GOOGLE MAPS
			//=====================================================================
			
			$this->db->set($data)->insert('tipos_proximidades');
			
			
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "tipos_proximidades";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso  			
			
			return $this->db->insert_id();
		}
	}
	
	function delTipoProximidade ($id_tipo_proximidade)
	{		
		$where = array ('id_tipo_proximidade' => $id_tipo_proximidade);
		$this->db->select('*')->from('tipos_proximidades')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_tipo_proximidade', $id_tipo_proximidade);
            $this->db->delete('tipos_proximidades');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "tipos_proximidades";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso             
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */