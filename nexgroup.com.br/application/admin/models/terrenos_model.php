<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terrenos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numTerrenos ()
	{		
		$this->db->select('*')->from('terrenos');
		
		return $this->db->count_all_results();
	}
	
	function getTerrenos ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM terrenos
				ORDER BY data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getTerrenoVisualizar ($id_terreno)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM terrenos
				WHERE id_terreno = $id_terreno
			   ";
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	function numTerrenosBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM terrenos
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR observacoes LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	function buscaTerrenos ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM terrenos
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR observacoes LIKE '%$keyword%'
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delTerreno ($id_terreno)
	{		
		$where = array ('id_terreno' => $id_terreno);
		$this->db->select('*')->from('terrenos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_terreno', $id_terreno);
            $this->db->delete('terrenos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "terrenos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */