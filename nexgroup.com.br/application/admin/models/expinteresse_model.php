<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expinteresse_model extends CI_Model {	

	function __construct(){
		parent::__construct();
	}
	
	function getPermissoes(){
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function getInteresse($tipo,$id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		$where = "interesses.id_empreendimento = '{$id}'";
		if($tipo == 2){
			$where = "interesses.url = '{$id}'";
		}
		
		$sql = "SELECT 'interesse' as tipo, empreendimentos.empreendimento, interesses.nome, interesses.email, interesses.telefone, interesses.comentarios, grupos_interesses.emails, interesses.data_envio AS data 
				FROM interesses 
				LEFT JOIN empreendimentos ON empreendimentos.id_empreendimento = interesses.id_empreendimento
				LEFT JOIN grupos_interesses ON grupos_interesses.id_interesse = interesses.id_interesse
				WHERE {$where} AND interesses.data_envio 
				BETWEEN '".$min."' AND '".$max."' 
				ORDER BY interesses.data_envio DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */