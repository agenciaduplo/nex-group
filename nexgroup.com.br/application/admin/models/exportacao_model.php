<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exportacao_model extends CI_Model {	

	function __construct(){
		parent::__construct();
	}
	
	function getPermissoes(){
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	function getBicicletasPromo(){
		$sql = "SELECT bicicletas_saint_louis.nome, bicicletas_saint_louis.email, bicicletas_saint_louis.cidade, bicicletas_saint_louis.bairro, bicicletas_saint_louis.frase, bicicletas_saint_louis.como_soube, bicicletas_saint_louis.regulamento, bicicletas_saint_louis.data_envio FROM bicicletas_saint_louis ORDER BY bicicletas_saint_louis.data_envio DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */