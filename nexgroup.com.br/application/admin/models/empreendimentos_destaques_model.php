<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empreendimentos_destaques_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$dataAcao = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($dataAcao)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numDestaqueEmpreendimentos ()
	{		
		$this->db->select('*')->from('destaques_empreendimentos');		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#
	
	function numDestaqueEmpreendimentosBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM empreendimentos e, destaques_empreendimentos d
				WHERE e.id_empreendimento = d.id_empreendimento
				AND e.empreendimento LIKE '%$keyword%' OR d.legenda LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
	
	#-----------------------------------------------------------------------------------#		
	
	function getDestaqueEmpreendimentos ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM empreendimentos e, destaques_empreendimentos d
				WHERE e.id_empreendimento = d.id_empreendimento
				ORDER BY e.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	function getEmpreendimentos ()
	{		
		
		$sql = "SELECT *
				FROM empreendimentos e, empresas ep, cidades c, estados es, tipos_imoveis tp, status s
				WHERE e.id_empresa = ep.id_empresa
				AND e.id_cidade = c.id_cidade
				AND c.id_estado = es.id_estado
				AND e.id_tipo_imovel = tp.id_tipo_imovel
				AND e.id_status = s.id_status
				ORDER BY e.empreendimento ASC
			   ";		
		
		$query = $this->db->query($sql);
			
		$this->db->flush_cache();	
		
		return $query->result();
		
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function buscaDestaqueEmpreendimentos ($keyword)
	{	
		$sql = "SELECT *
				FROM empreendimentos e, destaques_empreendimentos d
				WHERE e.id_empreendimento = d.id_empreendimento
				AND e.empreendimento LIKE '%$keyword%' d.legenda LIKE '%$keyword%'
				ORDER BY e.empreendimento ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	#-----------------------------------------------------------------------------------#	
	
	function getDestaqueEmpreendimento($id_destaque_empreendimento)
	{		
		$where = array ('id_destaque_empreendimento' => $id_destaque_empreendimento);
		
		$this->db->select('*')->from('destaque_empreendimentos');
		$this->db->where($where);
		
		
		$query = $this->db->get();
		
		return $query->row();

	}
		
	#-----------------------------------------------------------------------------------#	
		
	#-----------------------------------------------------------------------------------#
	
	function setDestaque($data,$id_destaque ='')
	{
		if($id_destaque){
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/imoveis/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '5000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 990;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }
			$this->db->where('id_destaque_empreendimento',$id_destaque);
	        $this->db->set($data)->update('destaques_empreendimentos');
	       	
		}else{
			$this->load->library('image_lib');
			
				//UPLOAD
		        $config['upload_path']		= './uploads/imoveis/destaques/';
		        $config['allowed_types']	= 'gif|jpg|png|bmp';
		        $config['max_size']			= '5000';
		        $config['max_width']		= '4000';
		        $config['max_height']		= '4000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem'))
		        {
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
			        
			        
					$config['create_thumb']     = FALSE;
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './uploads/imoveis/destaques/'. $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['width']			= 990;
					//$config['height']			= 425;
					$config['quality']			= 100;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();		        
			        
					$this->db->set('imagem', $file_name);
					
				    //Log Acesso			
					
		        }
		        else
		        {
		        	$this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
		        }

	       $this->db->set($data)->insert('destaques_empreendimentos');
		}
	    //Log Acesso
	    $acao 		= "insert";
	    $tabela 	= "Destaques Empreendimentos";
	    $sql 		= $this->db->last_query();
	    $this->model->inserirLogAcoes($tabela, $acao, $sql);
	}
	
#-----------------------------------------------------------------------------------#	

	function delDestaqueEmpreendimento ($id_destaque_empreendimento)
	{
	    $this->db->where('id_destaque_empreendimento', $id_destaque_empreendimento);
	    $this->db->delete('destaques_empreendimentos');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "destaques empreendimentos";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
	
	function getDestaque($id_destaque_empreendimento){
		
		$this->db->flush_cache();
	
		$this->db->select('*')->from('destaques_empreendimentos');
		$this->db->join('empreendimentos','empreendimentos.id_empreendimento = destaques_empreendimentos.id_empreendimento','INNER');
		$this->db->where('id_destaque_empreendimento',$id_destaque_empreendimento);
		
		$query = $this->db->get();
		
		return $query->row();
	}
	
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	