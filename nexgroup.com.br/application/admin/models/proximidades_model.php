<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proximidades_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
#-----------------------------------------------------------------------------------#
		
	function blacklist(){
		$sql = "SELECT blacklist.`name` FROM blacklist";
		$query = $this->db->query($sql);		
		return $query->result();
	}




	function numProximidades ()
	{		
		$this->db->select('*')->from('proximidades');		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#
	function numProximidadesBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM proximidades
				WHERE titulo LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#		
	function getProximidades ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT p.*, t.tipo_proximidade
				FROM proximidades p
				LEFT JOIN tipos_proximidades t ON t.id_tipo_proximidade = p.id_tipo_proximidade  
				ORDER BY data_cadastro desc
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function buscaProximidades ($keyword)
	{	
		$sql = "SELECT *
				FROM proximidades
				WHERE titulo LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function getProximidadeId ($id_proximidade)
	{	
		$sql = "SELECT *
				FROM proximidades
				WHERE id_proximidade = $id_proximidade
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}


#-----------------------------------------------------------------------------------#	
	function getProximidade($id_proximidade)
	{		
		$where = array ('id_proximidade' => $id_proximidade);
		
		$this->db->start_cache();
		$this->db->select('*')->from('proximidades')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
#-----------------------------------------------------------------------------------#	
	function setProximidade ($data, $id_proximidade = "")
	{		
		if ($id_proximidade)
		{
			$where = array ('id_proximidade' => $id_proximidade);
			$this->db->select('*')->from('proximidades')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
	            $this->db->set($data);
	            $this->db->where('id_proximidade', $id_proximidade);
	            $this->db->update('proximidades');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "proximidades";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso 	                	        	
			}
		}
		else
		{
	        $this->db->set($data);
	        $this->db->insert('proximidades');
	        $idProximidades = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "proximidades";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idProximidades; 	                	        	
		}
	}
	
	function delProximidade ($id_proximidade)
	{
	    $this->db->where('id_proximidade', $id_proximidade);
	    $this->db->delete('proximidades');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "proximidades";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	