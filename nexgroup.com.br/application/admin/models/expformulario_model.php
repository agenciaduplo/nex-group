<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expformulario_model extends CI_Model {	

	function __construct(){
		parent::__construct();
	}
	
	function getPermissoes(){
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function getFormulario($tipo,$id,$min,$max){
		$min = implode('-',array_reverse(explode('/',$min)));
		$max = implode('-',array_reverse(explode('/',$max)));
		if($id>0) $where = "id_empreendimento = '{$id}'";
    else $where = "1 = 1";
    switch($tipo) {
      default:
      case 1:
        $sql = " ( SELECT atendimentos.id_atendimento AS id, atendimentos.user_agent AS navegador, 
                       atendimentos.origem, atendimentos.nome, atendimentos.email, 
                       atendimentos.forma_contato AS forma, atendimentos.url, 
                       atendimentos.data_envio AS data, e.empreendimento, 'atendimento' as tipo 
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' 
                ) UNION (
                  SELECT contatos.id_contato AS id, contatos.user_agent AS navegador, 
                  contatos.origem, contatos.nome, contatos.email, 
                  contatos.forma_contato AS forma, contatos.url, 
                  contatos.data_envio AS data, e.empreendimento, 'contato' as tipo 
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT indique.id_indique AS id, indique.user_agent AS navegador, 
                  indique.origem, indique. nome_remetente as nome, 'email' as forma, 
                  indique.email_remetente as email, indique.url, 
                  indique.data_envio AS data, e.empreendimento, 'indique' as tipo 
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."'
                ) UNION (
                  SELECT interesses.id_interesse AS id, interesses.user_agent AS navegador, 
                    interesses.origem, interesses.nome, interesses.email,
                    interesses.forma_contato AS forma, interesses.url, 
                    interesses.data_envio AS data, e.empreendimento, 'interesse' as tipo  
                  FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                  WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                ) UNION (
                  SELECT terrenos.id_terreno AS id, terrenos.user_agent AS navegador, 
                    terrenos.origem, terrenos.nome, terrenos.email,
                    'email' AS forma, terrenos.url, terrenos.data_envio AS data, 
                    'terreno' as empreendimento, 'terreno' as tipo  
                  FROM terrenos 
                  WHERE {$where} AND terrenos.data_envio BETWEEN '".$min."' AND '".$max."'
                )
                ORDER BY data DESC
                ";
      break; 
      case 2:
        $sql = "SELECT atendimentos.id_atendimento AS id, atendimentos.user_agent AS navegador, 
                       atendimentos.origem, atendimentos.nome, atendimentos.email, 
                       atendimentos.forma_contato AS forma, atendimentos.url, 
                       atendimentos.data_envio AS data, e.empreendimento, 'atendimento' as tipo 
                FROM atendimentos LEFT JOIN empreendimentos e USING(id_empreendimento)
                WHERE {$where} AND atendimentos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY atendimentos.data_envio DESC";  
      break; 
      case 3:
        $sql = "SELECT contatos.id_contato AS id, contatos.user_agent AS navegador, 
                  contatos.origem, contatos.nome, contatos.email, 
                  contatos.forma_contato AS forma, contatos.url, 
                  contatos.data_envio AS data, e.empreendimento, 'contato' as tipo 
                FROM contatos LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND contatos.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY contatos.data_envio DESC";
      break; 
      case 4:
        $sql = "SELECT indique.id_indique AS id, indique.user_agent AS navegador, 
                  indique.origem, indique. nome_remetente as nome, 'email' as forma, 
                  indique.email_remetente as email, indique.url, 
                  indique.data_envio AS data, e.empreendimento, 'indique' as tipo 
                FROM indique LEFT JOIN empreendimentos e USING(id_empreendimento) 
                WHERE {$where} AND indique.data_envio BETWEEN '".$min."' 
                  AND '".$max."' ORDER BY indique.data_envio DESC";
      break; 
      case 5:
          $sql = "SELECT interesses.id_interesse AS id, interesses.user_agent AS navegador, 
                    interesses.origem, interesses.nome, interesses.email, 
                    interesses.forma_contato AS forma, interesses.url, 
                    interesses.data_envio AS data, e.empreendimento, 'interesse' as tipo  
                    FROM interesses LEFT JOIN empreendimentos e USING(id_empreendimento) 
                    WHERE {$where} AND interesses.data_envio BETWEEN '".$min."' AND '".$max."' 
                    ORDER BY interesses.data_envio DESC";
      break;
      case 6:  
          $sql = "SELECT terrenos.id_terreno AS id, terrenos.user_agent AS navegador, 
                    terrenos.origem, terrenos.nome, terrenos.email,
                    'email' AS forma, terrenos.url, terrenos.data_envio AS data, 
                    'terreno' as empreendimento, 'terreno' as tipo  
                    FROM terrenos 
                    WHERE {$where} AND terrenos.data_envio BETWEEN '".$min."' AND '".$max."' 
                    ORDER BY terrenos.data_envio DESC";
      break;
    }
		$query = $this->db->query($sql);
		return $query->result();
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */