<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numNewsletter ()
	{		
		$this->db->select('*')->from('newsletter');
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#
	function numNewsletterBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM newsletter
				WHERE nome LIKE '%$keyword%' OR
				email LIKE '%$keyword%'
				ORDER BY data_cadastro DESC
			   ";
			  		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}	
#-----------------------------------------------------------------------------------#
	function getNewsletter ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM newsletter 
				ORDER BY data_cadastro DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getNewsletterVisualizar ($id_newsletter)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM newsletter
				WHERE id_newsletter = $id_newsletter
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaNewsletter ($keyword,$offset = 0)
	{
		
		$this->db->flush_cache();	
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		$sql = "SELECT *
				FROM newsletter 
				WHERE nome LIKE '%$keyword%' OR
				email LIKE '%$keyword%'
				ORDER BY data_cadastro DESC
				LIMIT $offset, 20				
			   ";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delNewsletter ($id_newsletter)
	{		
		$where = array ('id_newsletter' => $id_newsletter);
		$this->db->select('*')->from('newsletter')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_newsletter', $id_newsletter);
            $this->db->delete('newsletter');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "newsletter";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */