<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Atendimentos_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }	
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numAtendimentos ()
	{		
		$this->db->select('*')->from('atendimentos');
		
		return $this->db->count_all_results();
	}
	#-----------------------------------------------------------------------------------#
	function numAtendimentosBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM atendimentos, 
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY data_envio DESC
			   ";
			  		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}	
#-----------------------------------------------------------------------------------#
	
	function getAtendimentos ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM atendimentos 
				ORDER BY data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getAtendimentoVisualizar ($id_atendimento)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM atendimentos 
				WHERE id_atendimento = $id_atendimento
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaAtendimentos ($keyword)
	{
		
		$this->db->flush_cache();	
		
		$sql = "SELECT *
				FROM atendimentos, 
				WHERE nome LIKE '%$keyword%'
				OR email LIKE '%$keyword%'
				OR descricao LIKE '%$keyword%'
				ORDER BY data_envio DESC
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delAtendimento ($id_atendimento)
	{		
		$where = array ('id_atendimento' => $id_atendimento);
		$this->db->select('*')->from('atendimentos')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_atendimento', $id_atendimento);
            $this->db->delete('atendimentos');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "atendimentos";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */