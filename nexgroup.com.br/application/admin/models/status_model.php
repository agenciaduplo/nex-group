<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numStatus ()
	{		
		$this->db->select('*')->from('status');		
		
		return $this->db->count_all_results();
	}
	
	function getStatus ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM status
				ORDER BY status ASC
				LIMIT $offset,20
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function buscaStatus ($keyword)
	{		
		
		$sql = "SELECT *
				FROM status 
				WHERE status LIKE '%$keyword%'
				ORDER BY status ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getStatusId ($id_status)
	{		
		$where = array ('id_status' => $id_status);
		
		$this->db->start_cache();
		$this->db->select('*')->from('status')->where($where);
		$this->db->stop_cache();
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
			$query = $this->db->get();
			
			return $query->row();
		}
		
		$this->db->flush_cache();
	}
	
	function setStatus ($data, $id_status = "")
	{		
		if ($id_status)
		{
			$where = array ('id_status' => $id_status);
			$this->db->select('*')->from('status')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Status salvo com sucesso!');
				
				$this->db->set($data);
                $this->db->where('id_status', $id_status);
                $this->db->update('status');
                
	            //Log Acesso
	            	$acao 		= "update";
	            	$tabela 	= "status";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso                  
			}
		}
		else
		{
			$this->db->set($data)->insert('status');
			
            //Log Acesso
            	$acao 		= "insert";
            	$tabela 	= "status";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso  			
			
			return $this->db->insert_id();
		}
	}
	
	function delStatus ($id_status)
	{		
		$where = array ('id_status' => $id_status);
		$this->db->select('*')->from('status')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_status', $id_status);
            $this->db->delete('status');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "status";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso             
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/status_model.php */