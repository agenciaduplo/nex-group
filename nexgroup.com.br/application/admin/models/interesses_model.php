<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interesses_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================	
	
	function numInteresses ()
	{		
		$this->db->select('*')->from('interesses');
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#
	function numInteressesBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM interesses i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento
				AND i.nome LIKE '%$keyword%'
				OR i.email LIKE '%$keyword%'
				GROUP BY i.id_interesse
				ORDER BY i.data_envio DESC
			   ";
			  		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}	
#-----------------------------------------------------------------------------------#
	function getInteresses ($offset = 0)
	{		
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		
		$sql = "SELECT *
				FROM interesses i, empreendimentos e
				INNER JOIN cidades ON cidades.id_cidade = e.id_cidade
				WHERE i.id_empreendimento = e.id_empreendimento
				ORDER BY i.data_envio DESC
				LIMIT $offset, 20
			   ";
		
		
		$query = $this->db->query($sql);
		
		return $query->result();		
	}
	
	function getInteresseVisualizar ($id_interesse)
	{		
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM interesses i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento
				AND i.id_interesse = $id_interesse
			   ";
			
		$query = $this->db->query($sql);
		
		return $query->row();
	}
	
	function buscaInteresses ($keyword,$offset = 0)
	{
		
		$this->db->flush_cache();	
		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}	
		$sql = "SELECT *
				FROM interesses i, empreendimentos e
				WHERE i.id_empreendimento = e.id_empreendimento
				AND i.nome LIKE '%$keyword%'
				OR i.email LIKE '%$keyword%'
				GROUP BY i.id_interesse
				ORDER BY i.data_envio DESC
				LIMIT $offset, 20				
			   ";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	function delInteresse ($id_interesse)
	{		
		$where = array ('id_interesse' => $id_interesse);
		$this->db->select('*')->from('interesses')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_interesse', $id_interesse);
            $this->db->delete('interesses');
            
            //Log Acesso
            	$acao 		= "delete";
            	$tabela 	= "interesses";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso              
		}
	}
}

/* End of file falecom_model.php */
/* Location: ./system/application/model/falecom_model.php */