<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revistas_model extends CI_Model {	

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getPermissoes()
	{
		$this->db->flush_cache();		
		
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));		
		$sql = "SELECT *
				FROM permissoes
				WHERE id_usuario = $id_usuario
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}

	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($this->session->userdata('repont_id_usuario'));
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
#-----------------------------------------------------------------------------------#
	
	function numRevistas ()
	{		
		$this->db->select('*')->from('revistas');		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#
	function numRevistasBusca ($keyword)
	{		
		$sql = "SELECT *
				FROM revistas
				WHERE expediente LIKE '%$keyword%'
			   ";		
		
		$this->db->query($sql);		
		
		return $this->db->count_all_results();
	}
#-----------------------------------------------------------------------------------#		
	function getRevistas ($offset = 0)
	{		
		$this->db->flush_cache();		

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}		
		
		$sql = "SELECT *
				FROM revistas
				ORDER BY data_cadastro desc
				LIMIT $offset,5
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function buscaRevistas ($keyword)
	{	
		$sql = "SELECT *
				FROM revistas
				WHERE expediente LIKE '%$keyword%'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
#-----------------------------------------------------------------------------------#	
	function getRevistaId ($id_revista)
	{	
		$sql = "SELECT *
				FROM revistas
				WHERE id_revista = $id_revista
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}


#-----------------------------------------------------------------------------------#	
	function getRevista($id_revista)
	{		
		$where = array ('id_revista' => $id_revista);
		
		$this->db->start_cache();
		$this->db->select('*')->from('revistas')->where($where);
		$this->db->stop_cache();
		
		$query = $this->db->get();
		return $query->row();

		$this->db->flush_cache();
	}
#-----------------------------------------------------------------------------------#	
	function setRevista ($data, $id_revista = "")
	{		
		if ($id_revista)
		{
			$where = array ('id_revista' => $id_revista);
			$this->db->select('*')->from('revistas')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{	
				
				
				$this->load->library('image_lib');
					
				$upload = "";
				//=====================================================================
				//INICIO UPLOAD CAPA
				//=====================================================================
					$config['upload_path']		= './uploads/revista/capa';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			        if ($this->upload->do_upload('imagem'))
			        {
			        	$imagem = $this->getRevista($id_revista);
			        	if($imagem->capa)
						{
							unlink('./uploads/revista/capa/' . $imagem->capa);
						}
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/revista/capa/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 301;
						$config['height']			= 405;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						
						$this->db->set('capa', $file_name);
					}
			        
		        //=====================================================================
				//FIM UPLOAD DA CAPA
				//=====================================================================
				
				//=====================================================================
				//INICIO UPLOAD ARQUIVO PDF
				//=====================================================================
					$config['upload_path']		= './uploads/revista/pdf';
					$config['allowed_types']	= 'pdf';
					$config['max_size']			= '15000';
					$config['encrypt_name']     = TRUE;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					
			        //executa o upload (setar o nome do campo aqui)
					if (!$this->upload->do_upload('arquivo')){
						//caso não consiga fazer o upload
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('resposta', "Erro ao inserir arquivo".$this->upload->display_errors());
					}
					else{
						//caso consiga
						$file_data 	= $this->upload->data();
			        	$file_name 	= $file_data['file_name'];
			        	
						$dataArchive = array('upload_data' => $file_data);
					
						$this->session->set_flashdata('resposta_sucesso', 'Registro inserido com sucesso!');
							
						$this->db->set('arquivo', $file_name);
					
					}
					
				//=====================================================================
				//FIM UPLOAD ARQUIVO PDF
				//=====================================================================
				
	        	
	            $this->db->set($data);
	            $this->db->where('id_revista', $id_revista);
	            $this->db->update('revistas');				
	                
		        //Log Acesso
		        	$acao 		= "update";
		           	$tabela 	= "revistas";
		            $sql 		= $this->db->last_query();
		            $this->model->inserirLogAcoes($tabela, $acao, $sql);
		        //Log Acesso 	                	        	
			}
		}
		else
		{
			
			$this->load->library('image_lib');
			$upload = "";
			//=====================================================================
				//INICIO UPLOAD CAPA
				//=====================================================================
					$config['upload_path']		= './uploads/revista/capa';
			        $config['allowed_types']	= 'gif|jpg|png|bmp';
			        $config['max_size']			= '6000';
			        $config['max_width']		= '4000';
			        $config['max_height']		= '4000';
			        $config['encrypt_name']     = TRUE;
			  
			        $this->load->library('upload', $config);
			      	$this->upload->initialize($config);
			      	
			        if ($this->upload->do_upload('imagem'))
			        {
			        	
			        	$file_data = $this->upload->data();
						
				        $file_name = $file_data['file_name'];
				        $file_size = $file_data['file_size'];
				        
				        //ZOOM
						$config['create_thumb']     = FALSE;
						$config['source_image']	    = $file_data['full_path'];
						$config['new_image']		= './uploads/revista/capa/' . $file_name;
						$config['maintain_ratio']	= TRUE;
						$config['width']			= 301;
						$config['height']			= 405;
						
						$config['quality']			= 100;
						$this->image_lib->initialize($config);
						$this->image_lib->resize();		        
						$this->db->set('capa', $file_name);
						
					}
			        
		        //=====================================================================
				//FIM UPLOAD DA CAPA
				//=====================================================================
				
				//=====================================================================
				//INICIO UPLOAD ARQUIVO PDF
				//=====================================================================
					$config['upload_path']		= './uploads/revista/pdf';
					$config['allowed_types']	= 'pdf';
					$config['max_size']		= '15000';
					$config['encrypt_name']    = TRUE;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if (!$this->upload->do_upload('arquivo')){
						//caso não consiga fazer o upload
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('resposta', "Erro ao inserir arquivo".$this->upload->display_errors());
					}
					else{
						//caso consiga
						$file_data 	= $this->upload->data();
			        	$file_name 	= $file_data['file_name'];
			        	
						$dataArchive = array('upload_data' => $file_data);
					
						$this->session->set_flashdata('resposta_sucesso', 'Registro inserido com sucesso!');
							
						$this->db->set('arquivo', $file_name);
					
					}
					
				//=====================================================================
				//FIM UPLOAD ARQUIVO PDF
				//=====================================================================
	       	$this->db->set($data);
	        $this->db->insert('revistas');
	        $idRevistas = $this->db->insert_id();				
	            
	        //Log Acesso
	        	$acao 		= "insert";
	           	$tabela 	= "revistas";
	            $sql 		= $this->db->last_query();
	            $this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso
	        
	        return $idRevistas; 	                	        	
		}
	}
	
	function delRevista ($id_revista)
	{
	    $this->db->where('id_revista', $id_revista);
	    $this->db->delete('revistas');
	    
		//Log Acesso
        	$acao 		= "delete";
        	$tabela 	= "revistas";
        	$sql 		= $this->db->last_query();
        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso	    
	}
	
}


/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */
	