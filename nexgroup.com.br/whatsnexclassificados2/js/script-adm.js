$(document).ready(function() {
    var table = $('.table').DataTable();
 
    $('.table tbody').on( 'click', 'tr', function () {
		
		id = $(this).attr('id');
		
        $(this).toggleClass('selected');
		
		if($(this).hasClass('selected'))
		{
			$.ajax({
				method: "POST",
				url: "select.php",
				data: { id: id, visto: 1 }
			});
		} else {
			$.ajax({
				method: "POST",
				url: "select.php",
				data: { id: id, visto: 0 }
			});
		}
    } );
});