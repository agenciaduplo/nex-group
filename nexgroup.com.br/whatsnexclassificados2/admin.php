<?php
	error_reporting(E_ALL ^ E_DEPRECATED);
	session_start();
?>
<!doctype html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="index, nofollow">
<title>WHATS NEX.</title>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="css/site.css" rel="stylesheet" type="text/css">
<link href="css/style-adm.css" rel="stylesheet" type="text/css">
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Exo:400,600,500,700' rel='stylesheet' type='text/css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
  background-attachment:fixed;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.table-striped {
	width:90%;
	margin:auto;
}
.table-striped>tbody>tr{cursor:pointer;}
.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #f9f9f9;
}
.table-striped>tbody>tr:nth-of-type(even) {
    background-color: #fff;
}
.table>thead { background-color:#000; color: #fff}
.table>thead>tr>th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
}
.table>tbody>tr>td {
	padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
	color:#000;
}
</style>
</head>

<body>
<?php
	//header( 'Cache-Control: no-cache' );
	//header( 'Content-type: application/json; charset="utf-8"', true );

	//$con = mysql_connect( 'localhost', 'root', 'root' ) ;
	//mysql_select_db( 'ford', $con );

	if(isset($_POST['user']) && isset($_POST['pass']) && $_POST['user'] == 'admin' && $_POST['pass'] == 'Duplo@q1w2e3r4'){
		$_SESSION['auth_user'] = 'authenticated';
	}
	
	if(!isset($_SESSION['auth_user'])){
?>	
		
	<div class="container">

		<form class="form-signin" method="POST">
			<h2 class="form-signin-heading">Please sign in</h2>
			
			<label for="inputEmail" class="sr-only">User</label>
			<input name="user" type="text" id="inputEmail" class="form-control" placeholder="User" required="" autofocus="">
			
			<label for="inputPassword" class="sr-only">Password</label>
			<input name="pass" type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
			
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		</form>

	</div>
<?php		
	} else if($_SESSION['auth_user'] == 'authenticated') {
	
		$con = mysqli_connect( 'mysql04.site1380654295.hospedagemdesites.ws', 'site13806542952', 'nnbd01YgM0017', 'site13806542952') ;
		
		//$cod_estados = mysql_real_escape_string( $_REQUEST['estado'] );

		$cidades = array();

		$sql = "SELECT id, nome, email, telefone, data, visto FROM whatsnex_contatos";
		$res = mysqli_query($con,  $sql);
?>		
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Nome</th>
							<th>E-mail</th>
							<th>Telefone</th>
							<th>Data</th>
						</tr>
					</thead>
					<tbody>
			<?php		
				while ( $row = mysqli_fetch_assoc( $res ) ) {
			?>
				<tr id="<?php echo $row['id']; ?>" class="<?php echo $row['visto'] == 1 ? 'selected' : ''; ?>">
						<td><?php echo $row['nome']; ?></td>
						<td><?php echo $row['email']; ?></td>
						<td><?php echo $row['telefone']; ?></td>
						<td><?php echo date('d/m/Y', strtotime($row['data'])); ?></td>
					</tr>
			<?php	
					}
			?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php
	}
	
?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.mask.min.js"></script>
<script src="js/script-adm.js"></script>
</body>
</html>