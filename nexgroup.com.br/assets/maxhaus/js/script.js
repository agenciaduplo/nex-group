$(".fancybox").colorbox({current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});
$(".cards").colorbox({rel:'cards',current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});

$('.acc').accordion({
      heightStyle: "content",
	  collapsible: true
    });

var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });

$('.telefone').mask('(00) 000000000');

$('.toggle-chat').click(function(){
	$('.chatbox iframe').slideToggle();
	
	if($(this).find('span').text() == '- FECHAR'){
		$(this).find('span').text('+ ABRIR');
	} else {
		$(this).find('span').text('- FECHAR');
	}
});

var control = true;
var inputs = $('.inputs');
var fHeight = $('.logo-myurban').height();
var controlToggle = true;

$('.click_contato').click(function(){

	$(this).fadeToggle();
	ga('send','event','form','abertura');
	
});

$(window).scroll(function(){
	
	
	var $elem = $('.chamada');
	
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	//LINK OFFSET
	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	
	if(docViewTop < 84) {
		$('.label-mude-agora').fadeIn();
	} else {
		$('.label-mude-agora').hide();
	}
	
	if($window.scrollTop() > 0){
		//FECHA O FORMULARIO
		$('.click_contato').removeClass('hide'); 
		$('.click_contato').fadeIn(); 

		$('.logo-emp').css('height', 81);


	} else {
		//ABRE O FORMULARIO
		$('.logo-emp').css('height', 'auto');
		
		$('.click_contato').fadeOut();
		$('.click_contato').addClass('hide'); 
	

		
		
	}
	
	//VISUALIZA ELEMENTO NA ROLAGEM
	if(elemBottom <= docViewBottom && control){
		ga('send','event','pagina','rolagem');
		control = false;
	}
	
});