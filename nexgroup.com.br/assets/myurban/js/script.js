$(".fancybox").colorbox({current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});
$(".cards").colorbox({rel:'cards',current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});

var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });

$('.telefone').mask('(00) 000000000');

$('.toggle-chat').click(function(){
	$('.chatbox iframe').slideToggle();
	
	if($(this).find('span').text() == '- FECHAR'){
		$(this).find('span').text('+ ABRIR');
	} else {
		$(this).find('span').text('- FECHAR');
	}
});

$('#fixedFormContact').validate({
	errorClass: 'error',
	rules: {
		nome: {
			required: true,
			minlength: 2
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail."
		}
	},
	errorPlacement: function(error, element) {
		error.insertBefore( element );
		$('.error-messages').html(error);
	},
	submitHandler: function(form) {
		
		//Desabilita o form enquanto estiver enviando
		$('#fixedFormContact').addClass('locked');
		$('#fixedFormContact #btn-enviar').attr('value', 'Enviando...');
		$('#fixedFormContact #btn-enviar').attr('disabled', 'disabled');

		//Envia o form via ajax
		$(form).ajaxSubmit({
			type:"POST",
			data: $(form).serialize(),
			url:base_url+"my-urban/envia",
			success: function(data) {
				//Abrir iframe com msg de sucesso e tags de conversao
				$.colorbox({arrowKey:false, iframe:true, href : base_url+'my-urban/response',maxWidth:'95%', maxHeight:'95%', innerWidth:519, innerHeight:294});
				/*$.fancybox.open({
					padding : 0,
					openEffect  : 'none',
					closeEffect : 'none',
					href : base_url+'myurban/response',
					type: 'iframe',
					//aspectRatio: true,
					width: 519,
					maxHeight: 312
				});*/
				
				//Habilita o form novamente e limpa
				$('#fixedFormContact').removeClass('locked');
				$('#fixedFormContact #btn-enviar').attr('value', 'ENVIAR');
				$('#fixedFormContact #btn-enviar').removeAttr('disabled');
				$('#fixedFormContact')[0].reset();
				
				//Meta GA
				if($('#is_mobile').css('display') == 'none'){
					console.log('not mobile');
					ga('send','event','form','envio','desktop/fixo');
				} else {
					console.log('is mobile');
					ga('send','event','form','envio','mobile/fixo');
				}
			},
			error: function() {
			  
			}
		});
	}
});

var control = true;
var inputs = $('.inputs');
var fHeight = $('.logo-myurban').height();
var fixedForm = $('#fixedFormContact');
var controlToggle = true;

$('.click_contato').click(function(){

	fixedForm.fadeToggle();
	$(this).fadeToggle();
	ga('send','event','form','abertura');
	
});

$(window).scroll(function(){
	
	
	var $elem = $('.chamada');
	
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	//LINK OFFSET
	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	//FIXED FORM OFFSET
	var fixedFormTop = fixedForm.offset().top;
	var fixedFormBottom = fixedFormTop + fixedForm.height();
	
	if(docViewTop < 84) {
		$('.label-mude-agora').fadeIn();
	} else {
		$('.label-mude-agora').hide();
	}
	
	if($window.scrollTop() > 0){
		//FECHA O FORMULARIO
		$('.click_contato').removeClass('hide'); 
		$('.click_contato').fadeIn(); 
		fixedForm.fadeOut(); 
		$('.logo-myurban').css('height', 81);
		
		
		//FECHA CHAT
		/*if($('.chatbox iframe').not(':focus') && controlToggle){
			
			$('.chatbox iframe').slideToggle();	
			if($('.toggle-chat').find('span').text() == '- FECHAR'){
				$('.toggle-chat').find('span').text('+ ABRIR');
			} else {
				$('.toggle-chat').find('span').text('- FECHAR');
			}
			controlToggle = false;
		}*/ 

	} else {
		//ABRE O FORMULARIO
		$('.logo-myurban').css('height', 'auto');
		
		$('.click_contato').fadeOut();
		$('.click_contato').addClass('hide'); 
		fixedForm.fadeIn();		

		
		
	}
	
	//VISUALIZA ELEMENTO NA ROLAGEM
	if(elemBottom <= docViewBottom && control){
		ga('send','event','pagina','rolagem');
		control = false;
	}
	
});