// var URL = 'http://nexgroup.divex.com.br/'
//var URL = 'http://127.0.0.1:89/'

$(document).ready(function() {	
	
	$("#FormTelCentral").mask("(99) 9999-9999");
	$("#FormFone").mask("(99) 9999-9999");
	$("#FormCelCliente").mask("(99) 9999-9999");
	$("#FormTelefonee").mask("(99) 9999-9999");
	$("#FormTelefoneCasa").mask("(99) 9999-9999");
	$("#FormTelefoneResp").mask("(99) 9999-9999");
	$("#FormCelResp").mask("(99) 9999-9999");
	$("#FormFax").mask("(99) 9999-9999");
	$("#FormCpf").mask("999.999.999-99");
	$("#FormCEP").mask("99999-999");
	$("#FormHoraAgenda").mask("99:99");
	$("#FormRespTelefone").mask("(99) 9999-9999");
	$("#FormRespCelular").mask("(99) 9999-9999");
	$("#FormCelularUser").mask("(99) 9999-9999");
	$("#FormTelefoneUser").mask("(99) 9999-9999");
	$("#FormHora").mask("99:99");
	$("#FormData").mask("99/99/9999");
	$("#FormHorario").mask("99:99");
	
    $('.FlashConceitual').media( { width: 995, height: 183, caption: false, wmode: 'transparent'} );
    $('.FlashMenu').media( { width: 200, height: 420 } );
    $('.FlashTitulo').media( { width: 300, height: 36 } );
    
    
    $("#FormLogon").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormUsuario,Usuário",
        "required,FormSenha,Senha"
        ]
    });    

    $("#FormUsuariosCad").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormNome,Nome",
        "required,FormEmail,E-mail",
        "valid_email,FormEmail,E-mail Inválido",
        "required,FormLogin,Login"
        ]
    });
    
    $("#FormTags").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,secao,Página"
        ]
    });
    
	$("#FormTipos_Imoveis").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTipoImovel,Tipos Imóveis"
        ]
    });
    
    $("#FormStatus").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormStatusCad,Status"
        ]
    });
         
    $("#FormFaixas_Precos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormFaixaPreco,Faixa de Preço"
        ]
    });
    
    $("#FormnNoticias").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormNoticia,Título",
        "required,data_cadastro,Data",
        "required,FormDescricao,Descrição"
        ]
    });

    $("#FormClippings").RSV({
        onCompleteHandler: myOnComplete,
        displayType: "display-html",
        errorFieldClass: "FormErro",
        rules: [
        "required,FormClippinggg,Título",
        "required,data_cadastro,Data",
        "required,FormDescricao,Descrição"
        ]
    });
   
    $("#FormEmails").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormEmail,Email",
        "required,FormEmpreedimento,Empreendimento",
        "required,FormTipo,Tipo"
        ]
    });
    $("#FormImagens").RSV({
        onCompleteHandler: myOnComplete,
        displayType: "display-html",
        errorFieldClass: "FormErro",
        rules: [
        "required,FormImagem,Imagem",
        "required,FormLegenda,Legenda"
        
        ]
    });

    $("#FormImagensOfertas").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormImagem,Imagem"        
        ]
    });
    
    $("#FormnRevistas").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Título",
        "required,FormEdicao,Edição",
        "required,data_cadastro,Data",
        "required,FormDescricao,Descrição",
        "required,FormExpediente,Expediente"
        ]
    }); 
    
	$("#FormnDestaques").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTitulo,Título",
        "required,FormImagem,Banner",
        "required,FormLink,Link"
        ]
    }); 
	
    $("#FormEmpreendimentos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsEmpreendimentos",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormEmpreendimento,Nome Empreendimento",
        "required,FormEmpresas,Empresa",
        "required,FormTipos,Tipo Imóvel",
        "required,FormDescricao,Descrição Imóvel",
        "required,FormStatus,Status geral"
        ]
    }); 
    
    $("#FormItens").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsItens",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTituloItem,Título item"
        ]
    });
    
    $("#FormFases").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsFases",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTituloFase,Título Fase",
        "required,FormStatus,Status"
        ]
    });
    
    $("#FormItemFase").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsItensFases",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTituloItemFase,Título Item fase",
        "required,FormPerItem,Porcentagem do item",
        "required,FormItemFase,Fase do item"
        ]
    });
    
    $("#FormFotoFasesEmpreendimentos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsFotosFases",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormFaseFoto,Fase da foto",
        "required,FormFotoFase,Escolha a foto",
        "required,FormLegendaFase,Legenda da foto"
        ]
    });
    
    $("#FormFotoEmpreendimentos").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrorsFotosEmpreendimentos",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormTipoMidia,Tipo de mídia",
        "required,FormTipoArquivo,Tipo do arquivo"
        ]
    });
    
    $("#FormObrasRealizadas").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorTargetElementId:"rsvErrors",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormEmpreendimento,Empreendimento",
        "required,FormEmpresas,Empresa",
        "required,FormCidade,Cidade"
        ]
    });
    
    $("#FormImagensDestaques").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormImagem,Imagem",
        "required,FormEmpreedimento,Selecione o Empreendimento",
        "required,FormLegenda,Legenda"
        ]
    });
    
    $("#FormDestaquesNoticias").RSV({
	    onCompleteHandler: myOnComplete,
	    displayType: "display-html",
	    errorFieldClass: "FormErro",
        rules: [
        "required,FormImagemNoticias,Imagem",
        "required,FormNoticia,Selecione a Notícia",
        "required,FormLegendaNoticia,Legenda"
        ]
    });
    
    $("#data_cadastro").datepicker({
    	yearRange: '-100:+3',
        showOn: "button",
        buttonImage: URL+"/assets/admin/img/ico-calendario.gif",
        buttonImageOnly: true
    });
    
    $("#data_atualizacao").datepicker({
    	yearRange: '-100:+3',
        showOn: "button",
        buttonImage: URL+"/assets/admin/img/ico-calendario.gif",
        buttonImageOnly: true
    });  
    
    $('#FormEstado').change(function(){
        console.log(URL);
        $('#FormCidade').load(URL+'admin.php/empreendimentos/getCidades/'+$('#FormEstado').val() );
    });
    
    $('#FormTipoArquivo').change(function(){
       var tipo = $('#FormTipoArquivo').val();
       if(tipo == 'I' || tipo == 'P'){
    	   $('#campo-link').css('display','none');
    	   $('#campo-foto').css('display','block');
       }else{
    	   $('#campo-foto').css('display','none');
    	   $('#campo-link').css('display','block');
       }
       
    });
});

function mostraEsconde(div)
{
	d = document.getElementById(div).style;
	
	document.getElementById('mostra_conf').style.display = 'none';
	document.getElementById('mostra_fotos').style.display = 'none';
	document.getElementById('mostra_guia').style.display = 'none';
	document.getElementById('mostra_servicos').style.display = 'none';
	document.getElementById('mostra_formularios').style.display = 'none';
	//document.getElementById('MostraFotos').style.display = 'none';
	
	if (d.display == 'none') {
    	d.display = 'block';
	} else {
    	d.display = 'none';
	}
}

function mostraFotos(div)
{
	d = document.getElementById(div).style;

	if (d.display == 'none') {
    	d.display = 'block';
	} else {
    	d.display = 'none';
	}	
}


function myOnComplete() { return true; }
function abas(alvo) {
    $('.abas').hide();
    $("#abas ul li").removeClass('atual');
    $('#Alvo'+alvo).show();
    $('#Aba'+alvo).addClass("atual")
}


function sobeDestaque(id_emp)
{
	$('#listaDestaques').fadeOut('slow');
	
	var msg ='';
	var vet_dados = 'id_empreendimento=' + id_emp;
				
	base_url  = URL + "admin.php/empreendimentos_destaques/sobeDestaque/";
	$.ajax({
	    type: "POST",
	    url: base_url,
	    data: vet_dados,
	    success: function(msg) {
					//alert(msg);
	    			$("#listaDestaques").html(msg);
					$("#listaDestaques").fadeIn("slow");	
	            }
		});	
}

function desceDestaque(id_emp)
{
	$('#listaDestaques').fadeOut('slow');
		
	var msg ='';
	var vet_dados = 'id_empreendimento=' + id_emp;			
	
	base_url  = URL + "admin.php/empreendimentos_destaques/desceDestaque/";
	$.ajax({
	    type: "POST",
	    url: base_url,
	    data: vet_dados,
	    success: function(msg) {
	    			$("#listaDestaques").html(msg);
					$("#listaDestaques").fadeIn("slow");	
	            }
		});	
}