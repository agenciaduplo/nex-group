
	// var URL = 'http://www.nexgroup.com.br/newnex/'

		
// INICO ITENS

	function getItem(id_item,id_emp)
	{
		$('#ItensAjax').fadeOut('slow');
		
		var msg ='';
		vet_dados = 'id_item=' + id_item+'&id_empreendimento=' + id_emp;

		base_url  = URL + "admin.php/empreendimentos/editaItensAjax/";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
						$("#ItensAjax").html(msg);
						$("#ItensAjax").fadeIn("slow");	
					}
			});
	}
	
	function enviaItem()
	{
		$('#listaItens').fadeOut('slow');
			
		var titulo 				= document.getElementById('FormTituloItem').value;
		var id_item 			= document.getElementById('FormItensId').value;
		var id_empreendimento 	= document.getElementById('FormEmpreendimentoId').value;
		
		
		if ($('#destaqueSim').is(':checked'))
		{
			var destaque = "S";
		}
		else if ($('#destaqueSim').is(':checked'))
		{
			var destaque = "N";
		}
		
		var msg ='';
		var vet_dados = 'destaque=' + destaque
					+ '&titulo=' + titulo
					+ '&id_item=' + id_item
					+ '&id_empreendimento=' + id_empreendimento;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/salvarItens/";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
						$("#listaItens").html(msg);
						$("#listaItens").fadeIn("slow");	
		            }
			});	
	}
	
	function sobeItem(id_item,id_emp)
	{
		$('#listaItens').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_item=' + id_item+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/sobeItem/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						//alert(msg);
		    			$("#listaItens").html(msg);
						$("#listaItens").fadeIn("slow");	
		            }
			});	
	}
	
	function desceItem(id_item,id_emp)
	{
		$('#listaItens').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_item=' + id_item+'&id_empreendimento=' + id_emp;			
		
		base_url  = URL + "admin.php/empreendimentos/desceItem/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#listaItens").html(msg);
						$("#listaItens").fadeIn("slow");	
		            }
			});	
	}
	
	function limpaCamposItens(id_empreendimento)
	{
		$('#ItensAjax').fadeOut('slow');
		
		var id_empreendimento = id_empreendimento;
		
		var msg ='';
		var vet_dados = 'id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/limpaCamposItens/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ItensAjax").html(msg);
						$("#ItensAjax").fadeIn("slow");	
		            }
			});	
	}	 	  
	
	function delItem(id_item, id_empreendimento)
	{
		$('#listaItens').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_item=' + id_item
					+ '&id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/apagaItens/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#listaItens").html(msg);
						$("#listaItens").fadeIn("slow");	
		            }
			});
	}
	
// FIM ITENS

//INICIO FASES

function getFase (id_fase,id_emp)
{
	$('#FasesAjax').fadeOut('slow');
	
	var msg ='';
	var vet_dados = 'id_fase=' + id_fase+'&id_empreendimento=' + id_emp;
	
	base_url  = URL + "admin.php/empreendimentos/editaFasesAjax/";
	
	$.ajax({
	    type: "POST",
	    url: base_url,
	    data: vet_dados,
	    success: function(msg)
	    {
			$("#FasesAjax").html(msg);
			$("#FasesAjax").fadeIn("slow");
        }
	});
}
	
function enviaFase()
{
	$('#listaFases').fadeOut('slow');
		
	var titulo 				= document.getElementById('FormTituloFase').value;
	var id_fase 			= document.getElementById('FormFaseId').value;
	var status 				= document.getElementById('FormStatus').value;
	var mostrar 			= document.getElementById('FormMostrar').value;
	var porcentagem_fase 	= document.getElementById('FormPorcentagemFase').value;
	var id_empreendimento 	= document.getElementById('FormId').value;

	var msg = '';
	var vet_dados 	= '&titulo=' 			+ titulo
					+ '&id_fase=' 			+ id_fase
					+ '&status=' 			+ status
					+ '&mostrarFase=' 		+ mostrar
					+ '&porcentagem_fase=' 	+ porcentagem_fase
					+ '&id_empreendimento=' + id_empreendimento;
				
	base_url = URL + "admin.php/empreendimentos/salvarFases/";

	$.ajax({
	    type: "POST",
	    url: base_url,
	    data: vet_dados,
	    success: function(msg)
	    {
			$("#listaFases").html(msg);
			$("#listaFases").fadeIn("slow");	
        }
	});
}
	
	function sobeFotoFase(id_foto_fase,id_fase,id_emp)
	{
		$('#ListaFotosFases').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_foto_fase=' + id_foto_fase+'&id_fase=' + id_fase+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/sobeFotoFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						//alert(msg);
		    			$("#ListaFotosFases").html(msg);
						$("#ListaFotosFases").fadeIn("slow");	
		            }
			});	
	}
	
	function desceFotoFase(id_foto_fase,id_fase,id_emp)
	{
		$('#ListaFotosFases').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_foto_fase=' + id_foto_fase+'&id_fase=' + id_fase+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/desceFotoFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ListaFotosFases").html(msg);
						$("#ListaFotosFases").fadeIn("slow");	
		            }
			});	
	}
	
	function delFotoFase(id_foto_fase,id_emp)
	{
		$('#ListaFotosFases').fadeOut('slow');
		//alert(id_emp);
		var msg ='';
		var vet_dados = 'id_foto_fase=' + id_foto_fase+ '&id_empreendimento=' + id_emp;
		
		base_url  = URL + "admin.php/empreendimentos/apagarMidiaFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						$("#ListaFotosFases").fadeIn("slow");
		    			$("#ListaFotosFases").html(msg);
		            }
			});
	}
	
	
	function limpaCamposFases(id_empreendimento)
	{
		$('#FasesAjax').fadeOut('slow');
		
		var id_empreendimento = id_empreendimento;
		
		var msg ='';
		var vet_dados = 'id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/limpaCamposFases/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						$("#FasesAjax").fadeIn("slow");	
		    			$("#FasesAjax").html(msg);
						
		            }
			});	
	}	 	  
	
	function delFase(id_fase, id_empreendimento)
	{
		$('#listaFases').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_fase=' + id_fase
					+ '&id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/apagaFases/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						$("#listaFases").fadeIn("slow");
		    			$("#listaFases").html(msg);
		            }
			});
	}

//FIM FASES
	
// INICO ITENS DAS FASES

	function getItemFase (id_itemFase,id_emp)
	{
		
		var div = '.edita-'+id_itemFase;
		$(div).fadeOut('slow');
		var msg ='';
		var vet_dados = 'id_item=' + id_itemFase+'&id_empreendimento=' + id_emp;
		
		base_url  = URL + "admin.php/empreendimentos/editaItensFasesAjax/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$(div).html(msg);
						$(div).fadeIn("slow");	
		            }
			});
	}
	
	function enviaItemFase()
	{
		$('#ListaItensFasesAjax').fadeOut('slow');
		var id_item_fase 		= document.getElementById('FormItensFaseId').value;	
		var div = '.edita-'+id_item_fase;
		var porcentagem 		= $(div+ ' #FormPerItem').val();
		var titulo 				= $(div+ ' #FormTituloItemFase').val();
		var id_fase 			= $(div+ ' #FormItemFase').val();
		
		var id_empreendimento 	= document.getElementById('FormEmpreendimentoId').value;
		//alert(porcentagem);
		var msg ='';
		var vet_dados = 'porcentagem=' + porcentagem
					+ '&titulo=' + titulo
					+ '&ItemFase=' + id_fase
					+ '&id_item_fase=' + id_item_fase
					+ '&id_empreendimento=' + id_empreendimento;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/salvarItensFases/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ListaItensFasesAjax").html(msg);
						$("#ListaItensFasesAjax").fadeIn("slow");	
		            }
			});	
	}
	
	function limpaCamposItensFases(id_empreendimento)
	{
		$('#ItensFasesAjax').fadeOut('slow');
		
		var id_empreendimento = id_empreendimento;
		
		var msg ='';
		var vet_dados = 'id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/limpaCamposItensFases/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ItensFasesAjax").html(msg);
						$("#ItensFasesAjax").fadeIn("slow");	
		            }
			});	
	}	 	  
	
	function sobeItemFase(id_item_fase,id_fase,id_emp)
	{
		$('#ListaItensFasesAjax').fadeOut('slow');
				
		var msg ='';
		var vet_dados = 'id_fase=' + id_fase
					+ '&id_item_fase=' + id_item_fase
					+ '&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/sobeItemFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						//alert(msg);
		    			$("#ListaItensFasesAjax").html(msg);
						$("#ListaItensFasesAjax").fadeIn("slow");	
		            }
			});	
	}
	
	function desceItemFase(id_item_fase,id_fase,id_emp)
	{
		$('#ListaItensFasesAjax').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_fase=' + id_fase
					+ '&id_item_fase=' + id_item_fase
					+ '&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/desceItemFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ListaItensFasesAjax").html(msg);
						$("#ListaItensFasesAjax").fadeIn("slow");	
		            }
			});	
	}
	
	function delItemFase(id_item, id_empreendimento)
	{
		$('#ListaItensFasesAjax').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_item_fase=' + id_item
					+ '&id_empreendimento=' + id_empreendimento;
		
		base_url  = URL + "admin.php/empreendimentos/apagaItensFases/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ListaItensFasesAjax").html(msg);
						$("#ListaItensFasesAjax").fadeIn("slow");	
		            }
			});
	}
	
// FIM ITENS DAS FASES
	
	function getLegendaFotoFase(id_foto_fase,id_emp)
	{
		var id = '.LegendaFotoFase-'+id_foto_fase;
		//alert(id);
		$(id).fadeOut('slow');
		
		var msg ='';
		vet_dados = 'id_foto_fase=' + id_foto_fase;

		base_url  = URL + "admin.php/empreendimentos/getFotoFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$(id).html(msg);
						$(id).fadeIn("slow");	
		            }
			});
	}
	
	function enviaLegendaFotoFase()
	{
		
		
			
		var id_empreendimento 	= document.getElementById('FormFotoid_empreendimento').value;
		var id_foto_fase 			= document.getElementById('id_foto_fase').value;
		var legenda			 	= document.getElementById('FormLegendaFotoFase2').value;
		
		var id = '.LegendaFotoFase-'+id_foto_fase;
		
		$(id).fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_foto_fase=' + id_foto_fase
					+ '&legenda=' + legenda
					+ '&id_empreendimento=' + id_empreendimento;
					
					
		base_url  = URL + "admin.php/empreendimentos/salvaLegendaFotoFase/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
				$(id).html(msg);
				$(id).fadeIn("slow");	
		    }
		});	
	}	
	
	
// INICIO MIDIAS
	
	function sobeMidias(id_midia,id_tipo,id_emp)
	{
		$('#ListaMidias').fadeOut('slow');
			
		
		
		var msg ='';
		var vet_dados = 'id_midia=' + id_midia+'&id_tipo_midia=' + id_tipo+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/sobeMidia/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						//alert(msg);
		    			$("#ListaMidias").html(msg);
						$("#ListaMidias").fadeIn("slow");	
		            }
			});	
	}
	
	function desceMidias(id_midia,id_tipo,id_emp)
	{
		$('#ListaMidias').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_midia=' + id_midia+'&id_tipo_midia=' + id_tipo+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/desceMidia/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$("#ListaMidias").html(msg);
						$("#ListaMidias").fadeIn("slow");	
		            }
			});	
	}
	
	function getLegendaMidia(id_midia,id_emp)
	{
		var id = '.LegendaMidia-'+id_midia;
		//alert(id);
		$(id).fadeOut('slow');
		
		var msg ='';
		vet_dados = 'id_midia=' + id_midia;

		base_url  = URL + "admin.php/empreendimentos/getMidia/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    			$(id).html(msg);
						$(id).fadeIn("slow");	
		            }
			});
	}
	
	function enviaLegendaMidia()
	{
		
		
			
		var id_empreendimento 	= document.getElementById('FormFotoid_empreendimento').value;
		var id_midia 			= document.getElementById('id_midia').value;
		var legenda			 	= document.getElementById('FormLegenda2').value;
		
		var id = '.LegendaMidia-'+id_midia;
		
		$(id).fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_midia=' + id_midia
					+ '&legenda=' + legenda
					+ '&id_empreendimento=' + id_empreendimento;
					
					
		base_url  = URL + "admin.php/empreendimentos/salvaLegenda/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
				$(id).html(msg);
				$(id).fadeIn("slow");	
		    }
		});	
	}
	
	function delMidia(id_midia,id_emp)
	{
		$('#ListaMidias').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_midia=' + id_midia+ '&id_empreendimento=' + id_emp;
		
		base_url  = URL + "admin.php/empreendimentos/apagarMidia/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
						$("#ListaMidias").fadeIn("slow");
		    			$("#ListaMidias").html(msg);
		            }
			});
	}
	
//FIM MIDIAS

	function sobeFase(id_fase,id_emp){
		$('#listaItens').fadeOut('slow');
		
		var msg ='';
		var vet_dados = 'id_fase=' + id_fase+'&id_empreendimento=' + id_emp;
					
					
		
		base_url  = URL + "admin.php/empreendimentos/sobeFase/";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
						//alert(msg);
						//$("#listaItens").html(msg);
						//$("#listaItens").fadeIn("slow");	
					}
			});	
	}
	
	function desceFase(id_fase,id_emp){
		$('#listaItens').fadeOut('slow');
			
		var msg ='';
		var vet_dados = 'id_fase=' + id_fase+'&id_empreendimento=' + id_emp;			
		
		base_url  = URL + "admin.php/empreendimentos/desceFase/";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
						//$("#listaItens").html(msg);
						//$("#listaItens").fadeIn("slow");	
					}
			});	
	}