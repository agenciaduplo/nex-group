//=======================================================================================
//INICIO VALIDAÇÃO FORMULÁRIOS
//=======================================================================================
	//INICIO  FALE CONOSCO
	$.validator.setDefaults({
		submitHandler: function() { enviaFaleConosco(); }
	}); 
	$("#frm-talk-to-us").validate({
		errorElement: "",
		rules:{
			nome:{
				required: true
			},
			email:{
				required: true, email:true			
			}
			// telefone_area:{
			// 	required: true
			// },
			// telefone:{
			// 	required: true
			// },
			// mensagem:{
			// 	required: true
			// },
			// opt_contact:{
			// 	required: true
			// }
		},
		messages:{
			nome:{
				required: ""
			},
			email:{
				required: "",
				email: ""
			}
			// telefone_area:{
			// 	required: ""
			// },
			// telefone:{
			// 	required: ""
			// },
			// mensagem:{
			// 	required: ""
			// },
			// opt_contact:{
			// 	required: ""
			// }
		}
	});
	//FIM FALE CONOSCO

	//INICIO NAO ENCONTROU IMOVEL
	$.validator.setDefaults({
		submitHandler: function() { enviaNaoEncontrei(); }
	}); 
	$("#frm-newsletter").validate({
		errorElement: "",
		rules:{
			seu_nome:{
				required: true
			},
			seu_email:{
				required: true, email:true			
			},
			telefone:{
				required: true
			},
			telefone_area:{
				required: true
			},
			mensagem:{
				required: true
			},
			opt_contact:{
				required: true
			}
		},
		messages:{
			seu_nome:{
				required: ""
			},
			seu_email:{
				required: "",
				email: ""
			},
			telefone_area:{
				required: ""
			},
			telefone:{
				required: ""
			},
			mensagem:{
				required: ""
			},
			opt_contact:{
				required: ""
			}
		}
	});
	//FIM NÃO ENCONTROU IMOVEL
	
	
	//INICIO ATENDIMENTO POR TELEFONE
	$.validator.setDefaults({
		submitHandler: function() { enviaAtendimentoTelefone(); }
	}); 
	$("#frm-phone-assistance").validate({
		errorElement: "",
		rules:{
			seu_nome:{
				required: true
			},
			seu_email:{
				required: true, email:true			
			},
			seu_phone:{
				required: true
			},
			seu_phone_area:{
				required: true
			},
			// empreendimento:{
			// 	required: true
			// },
			// mensagem:{
			// 	required: true
			// },
			opt_contact:{
				required: true
			}
		},
		messages:{
			seu_nome:{
				required: ""
			},
			seu_email:{
				required: "",
				email: ""
			},
			seu_phone_area:{
				required: ""
			},
			seu_phone:{
				required: ""
			},
			// empreendimento:{
			// 	required: ""
			// },
			// mensagem:{
			// 	required: ""
			// },
			opt_contact:{
				required: ""
			}
		}
	});
	//FIM ATENDIMENTO POR TELEFONE
	
	//INICIO ATENDIMENTO POR EMAIL
	$.validator.setDefaults({
		submitHandler: function() { enviaAtendimentoEmail(); }
	}); 
	$("#frm-email-assistance").validate({
		errorElement: "",
		rules:{
			seu_nome:{
				required: true
			},
			seu_email:{
				required: true, email:true			
			},
			// seu_phone:{
			// 	required: true
			// },
			// seu_phone_area:{
			// 	required: true
			// },
			// empreendimento:{
			// 	required: true
			// },
			// mensagem:{
			// 	required: true
			// },
			opt_contact:{
				required: true
			}
		},
		messages:{
			seu_nome:{
				required: ""
			},
			seu_email:{
				required: "",
				email: ""
			},
			// seu_phone_area:{
			// 	required: ""
			// },
			// seu_phone:{
			// 	required: ""
			// },
			// empreendimento:{
			// 	required: ""
			// },
			// mensagem:{
			// 	required: ""
			// },
			opt_contact:{
				required: ""
			}
		}
	});
	//FIM ATENDIMENTO POR EMAIL

	//INICIO ATENDIMENTO
	$.validator.setDefaults({
		submitHandler: function() { enviaAtendimento(); }
	}); 
	$("#frm-assistance").validate({
		errorElement: "",
		rules:{
			seu_nome:{
				required: true
			},
			seu_email:{
				required: true, email:true			
			},
			empreendimento:{
				required: true		
			}
		},
		messages:{
			seu_nome:{
				required: ""
			},
			seu_email:{
				required: "",
				email: ""
			},
			empreendimento:{
				required: "",
				email: ""
			}
		}
	});
	//FIM ATENDIMENTO POR EMAIL

	//INICIO INDIQUE
	$.validator.setDefaults({
		submitHandler: function() { enviaIndique(); }
	}); 
	$("#frm-tell-a-friend").validate({
		errorElement: "",
		rules:{
			nome_remetente:{
				required: true
			},
			email_remetente:{
				required: true,
				email: true
			},
			nome_destinatario:{
				required: true
			},
			email_destinatario:{
				required: true,
				email: true
			}
		},
		messages:{
			nome_remetente:{
				required: ""
			},
			email_remetente:{
				required: "",
				email: ""
			},
			nome_destinatario:{
				required: ""
			},
			email_destinatario:{
				required: "",
				email: ""
			}
		}
	});
	//FIM INDIQUE
	
	//INICIO INTERESSE
	$.validator.setDefaults({
		submitHandler: function() { enviaInteresse(); }
	}); 
	$("#frm-have-an-interest").validate({
		errorElement: "",
		rules:{
			nome:{
				required: true
			},
			email:{
				required: true, email:true			
			}
			// telefone:{
			// 	required: true
			// },
			// telefone_area:{
			// 	required: true
			// },
			// mensagem:{
			// 	required: true
			// },
			// opt_contact:{
			// 	required: true
			// }
		},
		messages:{
			nome:{
				required: ""
			},
			email:{
				required: "",
				email: ""
			}
			// telefone_area:{
			// 	required: ""
			// },
			// telefone:{
			// 	required: ""
			// },
			// mensagem:{
			// 	required: ""
			// },
			// opt_contact:{
			// 	required: ""
			// }
		}
	});
	//FIM INTERESSE
	
	//INICIO TERRENO
	$.validator.setDefaults({
		submitHandler: function() { enviaTerreno(); }
	}); 
	$("#frm-sell-your-terrain").validate({
		errorElement: "",
		rules:{
			area_terreno:{
				required: true
			},
			largura:{
				required: true		
			},
			valor:{
				required: true		
			},
			estado_terreno:{
				required: true		
			},
			cidade_terreno:{
				required: true		
			},
			endereco_terreno:{
				required: true		
			},
			bairro_terreno:{
				required: true		
			},
			cep_terreno:{
				required: true		
			},
			cep_area_terreno:{
				required: true		
			},
			nome:{
				required: true		
			},
			telefone:{
				required: true		
			},
			telefone_area:{
				required: true		
			},
			email:{
				required: true,email:true		
			},
			profundidade:{
				required: true
			}
		},
		messages:{
			area_terreno:{
				required: ''
			},
			largura:{
				required: ''		
			},
			valor:{
				required: ''		
			},
			estado_terreno:{
				required: ''		
			},
			cidade_terreno:{
				required: ''		
			},
			endereco_terreno:{
				required: ''		
			},
			bairro_terreno:{
				required: ''		
			},
			cep_terreno:{
				required: ''		
			},
			cep_area_terreno:{
				required: ''		
			},
			nome:{
				required: ''		
			},
			telefone:{
				required: ''
			},
			telefone_area:{
				required: ''		
			},
			email:{
				required: '',email:''		
			},
			profundidade:{
				required: ''
			}
		}
	});
	//FIM TERRENO

	//INICIO  	FORNECEDOR
	$.validator.setDefaults({		
		submitHandler: function() { enviaFornecedor(); }
	}); 
	$("#frm-supplier").validate({
		errorElement: "",
		rules:{
			nome:{
				required: true
			},
			email:{
				required: true, email:true			
			},
			atuacao:{
				required: true
			},
			telefone:{
				required: true
			},
			telefone_area:{
				required: true
			},
			empresa:{
				required: true
			}
		},
		messages:{
			nome:{
				required: ""
			},
			email:{
				required: "",
				email: ""
			},
			telefone_area:{
				required: ""
			},
			telefone:{
				required: ""
			},
			empresa:{
				required: ""
			},
			atuacao:{
				required: ""
			}
		}
	});
	//FIM Fornecedor

	//INICIO  TRABALHE
	$.validator.setDefaults({
		submitHandler: function() { enviaTrabalhe(); }
	}); 
	$("#frm-work-to-us").validate({
		errorElement: "label",
		rules:{
			nome:{
				required: true
			},
			email:{
				required: true, email:true			
			},
			curriculum:{
				required: true,
				accept:'pdf|doc|docx|odt'
			},
			telefone:{
				required: true
			},
			telefone_area:{
				required: true
			}
		},
		messages:{
			nome:{
				required: ""
			},
			email:{
				required: "",
				email: ""
			},
			telefone_area:{
				required: ""
			},
			telefone:{
				required: ""
			},
			curriculum:{
				required: "Selecione seu curriculum",
				accept:'Formato Inválido'
			}
		}
	});
	//FIM TRABALHE

	
//=======================================================================================
//FIM VALIDAÇÃO FORMULÁRIOS
//=======================================================================================

//INICIO ENVIO TERRENO
function enviaTerreno()
{
	$("#frm-sell-your-terrain .BtnRed").attr("disabled","disabled");
								
	var area 				= $("#ipt-sell-your-terrain-area").val();
	var frente 				= $("#ipt-sell-your-terrain-width").val();
	var fundos 				= $("#ipt-sell-your-terrain-depth").val();
	var zoneamento 			= $("#ipt-sell-your-terrain-zoning").val();
	var topografia 			= $("#ipt-sell-your-terrain-topography").val();
	var valor 				= $("#ipt-sell-your-terrain-value").val();
	var cidade_terreno 		= $("#ipt-sell-your-terrain-city").val();
	var endereco_terreno 	= $("#ipt-sell-your-terrain-address").val();
	var bairro_terreno 		= $("#ipt-sell-your-terrain-neighborhood").val();
	var cep_terreno 		= $("#ipt-sell-your-terrain-zip-code").val()+'-'+$("#ipt-sell-your-terrain-zip-code-area").val();
	var nome 				= $("#ipt-sell-your-terrain-your-name").val();
	var email 				= $("#ipt-sell-your-terrain-your-email").val();
	var cidade_contato 		= $("#ipt-sell-your-terrain-your-city").val();
	var bairro_contato 		= $("#ipt-sell-your-terrain-your-neighborhood").val();
	var cep_contato 		= $("#ipt-sell-your-terrain-your-zip-code").val()+'-'+$("#ipt-sell-your-terrain-your-zip-code-area").val();
	var endereco_contato 	= $("#ipt-sell-your-terrain-your-address").val();
	var observacoes 		= $("#ipt-sell-your-terrain-message").val();
	var telefone			= '('+$("#ipt-sell-your-terrain-phone-area").val()+')'+$("#ipt-sell-your-terrain-phone").val();
	var celular 			= '('+$("#ipt-sell-your-terrain-celphone-area").val()+')'+$("#ipt-sell-your-terrain-celphone").val();
		
	document.forms['terreno'].submit();
	var msg 	= '';

	vet_dados 	= 'area='+area  
			+'&frente='+frente
			+'&fundos='+fundos
			+'&zoneamento='+ zoneamento
			+'&topografia='+ topografia
			+'&valor='+valor
			+'&cidade_terreno='+cidade_terreno
			+'&endereco_terreno='+endereco_terreno
			+'&bairro_terreno='+bairro_terreno
			+'&cep_terreno='+cep_terreno
			+'&nome='+nome
			+'&email='+email
			+'&cidade_contato='+cidade_contato
			+'&bairro_contato='+bairro_contato
			+'&cep_contato='+cep_contato
			+'&endereco_contato='+endereco_contato
			+'&observacoes='+observacoes
			+'&telefone='+telefone
			+'&celular='+celular

	base_url  	= "http://www.nexgroup.com.br/contato/EnviaTerreno";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
				limpaCampos('#frm-sell-your-terrain');
				$("#frm-sell-your-terrain .BtnRed").removeAttr("disabled");
				$("#frm-sell-your-terrain").hide('slow');	
				$("#sucess-sell-your-terrain").show('slow');
		}
	});
	return false;
}
//FIM ENVIA TERRENO

//INICIO ENVIO FORNECEDOR
function enviaFornecedor(){
	$("#frm-supplier .BtnRed").attr("disabled","disabled");
	var empresa			= $("#ipt-supplier-campany").val();
	var nome			= $("#ipt-supplier-your-name").val();
	var email			= $("#ipt-supplier-email").val();
	var descricao		= $("#ipt-supplier-message").val();
	var telefone		= '('+$("#ipt-supplier-phone-area").val()+')'+$("#ipt-supplier-phone").val();
	var cidade			= $("#ipt-supplier-city").val();
	var area			= $("#ipt-supplier-business-type").val();
	var tipo			= $("#ipt-supplier-service-type").val();

	var msg 	= '';
	vet_dados 	= 'nome='+ nome
	 			  +'&empresa='+ empresa
				  +'&email='+ email
				  +'&telefone='+ telefone
				  +'&area_atuacao='+ area
				  +'&tipo_servico='+ tipo
				  +'&cidade='+ cidade
				  +'&comentarios='+ descricao;
				  
	base_url  	= "http://www.nexgroup.com.br/contato/EnviaFornecedor";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
				limpaCampos('#frm-supplier');
				$("#frm-supplier .BtnRed").removeAttr("disabled");
				//$("#ConfirmaForm").html(msg);
				$("#frm-supplier").hide('slow');	
				$("#sucess-supplier").show('slow');
				//perceptions(nome,email,'Nex Group - Contato / Fornecedor');	
		}
	});
	return false;
}
//FIM ENVIA FORNECEDOR

//INICIO ENVIO TRABALHE CONOSCO
function enviaTrabalhe(){
	$("#frm-work-to-us .BtnRed").attr("disabled","disabled");

	var nome			= $("#ipt-work-to-us-your-name").val();
	var email			= $("#ipt-work-to-us-your-email").val();
	var descricao		= $("#ipt-work-to-us-your-message").val();
	var telefone		= '('+$("#ipt-work-to-us-your-phone-area").val()+')'+$("#ipt-work-to-us-your-phone").val();

	document.forms['work'].submit();
	return false;

}
//FIM ENVIA TRABALHE

//INICIO ENVIO FALE CONOSCO
function enviaFaleConosco(){
	$("#frm-talk-to-us .BtnRed").attr("disabled","disabled");

	var nome			= $("#ipt-talk-to-us-your-name").val();
	var email			= $("#ipt-talk-to-us-your-email").val();
	var descricao		= $("#ipt-talk-to-us-message").val();
	var telefone		= '('+$("#ipt-talk-to-us-your-phone-area").val()+')'+$("#ipt-talk-to-us-your-phone").val();

	if ($('#ipt-talk-to-us-phone-opt').is(':checked'))
	{
		var contato = "Telefone";
	}
	else if ($('#ipt-talk-to-us-email-opt').is(':checked'))
	{
		var contato = "E-mail";
	}

	var msg 	= '';
	vet_dados 	= 'nome='+ nome
				  +'&email='+ email
				  +'&telefone='+ telefone
				  +'&contato='+ contato
				  +'&descricao='+ descricao;

	base_url  	= "http://www.nexgroup.com.br/contato/EnviaFaleConosco";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
				limpaCampos('#frm-talk-to-us');
				$("#frm-talk-to-us .BtnRed").removeAttr("disabled");
				$("#frm-talk-to-us").hide('slow');	
				$("#sucess-talk-to-us").show('slow');	
				//perceptions(nome,email,'Nex Group - Contato / Fale conosco');
		}
	});
	return false;
}
//FIM ENVIA FALE CONOSCO

//INICIO ENVIO ATENDIMENTO TELEFONE
function enviaAtendimentoTelefone()
{
	$("#frm-phone-assistance .BtnRed").attr("disabled","disabled");
	
	var nome			= $("#ipt-phone-assistance-your-name").val();
	var email			= $("#ipt-phone-assistance-your-email").val();
	var descricao		= $("#ipt-phone-assistance-message").val();
	var empreendimento	= $("#empreendimento").val();
	var telefone		= '('+$("#ipt-phone-assistance-your-phone-area").val()+')'+$("#ipt-phone-assistance-your-phone").val();

	if (empreendimento == 81
		|| empreendimento == 34
		|| empreendimento == 75
		|| empreendimento == 78
		|| empreendimento == 80
		|| empreendimento == 0
		|| empreendimento == "")
	{
		alert("Selecione o imóvel que deseja receber informações.");
		return false;
	}
	else
	{

		if ($('#ipt-phone-assistance-phone-opt').is(':checked'))
		{
			var contato = "Telefone";
		}
		else if ($('#ipt-phone-assistance-email-opt').is(':checked'))
		{
			var contato = "E-mail";
		} 
		
		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email
					  +'&telefone='+ telefone
					  +'&empreendimento='+ empreendimento
					  +'&contato='+ contato
					  +'&descricao='+ descricao;

		base_url  	= "http://www.nexgroup.com.br/central/enviarAtendimentoUnico";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {

					limpaCampos('#frm-phone-assistance');
					$("#frm-phone-assistance .BtnRed").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);
					$("#frm-phone-assistance").hide('slow');	
					$("#sucess-phone-assistance").show('slow');
					//perceptions(nome,email,'Nex Group - Atendimento por Telefone');
					
					$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');
			}
		});
		return false;
	}
}
//FIM ATENDIMENTO CENTRAL DE VENDAS

//INICIO ENVIO ATENDIMENTO EMAIL
function enviaAtendimentoEmail()
{
	$("#frm-email-assistance .BtnRed").attr("disabled","disabled");
	var nome			= $("#ipt-email-assistance-your-name").val();
	var email			= $("#ipt-email-assistance-your-email").val();
	var descricao		= $("#ipt-email-assistance-message").val();
	var empreendimento	= $("#empreendimento").val();
	var telefone		= '('+$("#ipt-email-assistance-your-phone-area").val()+')'+$("#ipt-email-assistance-your-phone").val();


	if (empreendimento == 81
		|| empreendimento == 34
		|| empreendimento == 75
		|| empreendimento == 78
		|| empreendimento == 80
		|| empreendimento == 0
		|| empreendimento == "")
	{
		alert("Selecione o imóvel que deseja receber informações.");
		return false;
	}
	else
	{

		if ($('#ipt-email-assistance-phone-opt').is(':checked'))
		{
			var contato = "Telefone";
		}
		else if ($('#ipt-email-assistance-email-opt').is(':checked'))
		{
			var contato = "E-mail";
		} 

		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email
					  +'&telefone='+ telefone
					  +'&empreendimento='+ empreendimento
					  +'&contato='+ contato
					  +'&descricao='+ descricao;

		base_url  	= "http://www.nexgroup.com.br/central/enviarAtendimentoUnico";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					               	
					limpaCampos('#frm-email-assistance');
					$("#frm-email-assistance .BtnRed").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);
					$("#frm-email-assistance").hide('slow');	
					$("#sucess-email-assistance").show('slow');
					//perceptions(nome,email,'Nex Group - Atendimento por Email');
					
					$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');
			}
		});
		return false;
	}
}
//FIM ATENDIMENTO CENTRAL DE VENDAS

//INICIO ENVIO ATENDIMENTO CENTRAL DE VENDAS
function enviaAtendimento()
{
	$("#frm-assistance .BtnRed").attr("disabled","disabled");

	var nome			= $("#ipt-assistance-your-name").val();
	var email			= $("#ipt-assistance-your-email").val();
	var descricao		= $("#ipt-assistance-message").val();
	var empreendimento	= $("#empreendimento").val();
	var telefone		= '('+$("#ipt-assistance-your-phone-area").val()+')'+$("#ipt-assistance-your-phone").val();

	if (empreendimento == 81
		|| empreendimento == 34
		|| empreendimento == 75
		|| empreendimento == 78
		|| empreendimento == 80
		|| empreendimento == 0
		|| empreendimento == "")
	{
		alert("Selecione o imóvel que deseja receber informações.");
		return false;
	}
	else
	{
		if ($('#ipt-assistance-phone-opt').is(':checked'))
		{
			var contato = "Telefone";
		}
		else if ($('#ipt-assistance-email-opt').is(':checked'))
		{
			var contato = "E-mail";
		} 

		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email
					  +'&telefone='+ telefone
					  +'&contato='+ contato
					  +'&empreendimento=81'
					  +'&descricao='+ descricao;

		base_url  	= "http://www.nexgroup.com.br/central/enviarAtendimentoUnico";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					limpaCampos('#frm-assistance');
					$("#frm-assistance .BtnRed").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);
					$("#frm-assistance").hide('slow');	
					$("#sucess-assistance").show('slow');
					
					$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');
					//perceptions(nome,email,'Nex Group - Atendimento');	
			}
		});
		return false;
	}
}
//FIM ATENDIMENTO CENTRAL DE VENDAS

//INICIO ENVIO NAO ENCONTROU IMOVEL
function enviaNaoEncontrei()
{
	$("#frm-newsletter .BtnRed").attr("disabled","disabled");
	
	var nome			= $("#ipt-newsletter-your-name").val();
	var email			= $("#ipt-newsletter-your-email").val();
	var descricao		= $("#ipt-newsletter-message").val();
	var telefone		= '('+$("#ipt-newsletter-your-phone-area").val()+')'+$("#ipt-newsletter-your-phone").val();

	if ($('#ipt-newsletter-phone-opt').is(':checked'))
	{
		var contato = "Telefone";
	}
	else if ($('#ipt-newsletter-email-opt').is(':checked'))
	{
		var contato = "E-mail";
	} 

	var msg 	= '';
	vet_dados 	= 'nome='+ nome
				  +'&email='+ email
				  +'&telefone='+ telefone
				  +'&contato='+ contato
				  +'&descricao='+ descricao;			  

	base_url  	= "http://www.nexgroup.com.br/central/EnviarnaoEncontrou";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
				limpaCampos('#frm-newsletter');
				$("#frm-newsletter .BtnRed").removeAttr("disabled");
				$("#frm-newsletter").hide('slow');	
				$("#sucess-newsletter").show('slow');
				//perceptions(nome,email,'Nex Group - Não encontrou seu Imóvel');
				
				$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');
		}

	});
	return false;
}
//FIM ATENDIMENTO CENTRAL DE VENDAS

//INICIO ENVIO INTERESSE
function enviaInteresse()
{
	$("#frm-have-an-interest .BtnRed").attr("disabled","disabled");

	var nome			= $("#ipt-have-an-interest-your-name").val();
	var email			= $("#ipt-have-an-interest-your-email").val();
	var descricao		= $("#ipt-have-an-interest-message").val();
	var empreendimento	= $("#id_empreendimento").val();
	var telefone		= '('+$("#ipt-have-an-interest-your-phone-area").val()+')'+$("#ipt-have-an-interest-your-phone").val();

	if ($('#ipt-have-an-interest-phone-opt').is(':checked'))
	{
		var contato = "Telefone";
	}
	else if ($('#ipt-have-an-interest-email-opt').is(':checked'))
	{
		var contato = "E-mail";
	} 

	var msg 	= '';
	vet_dados 	= 'nome='+ nome
				  +'&email='+ email
				  +'&telefone='+ telefone
				  +'&contato='+ contato
				  +'&empreendimento='+ empreendimento
				  +'&descricao='+ descricao;

	base_url  	= "http://www.nexgroup.com.br/imoveis/enviarInteresse";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
				limpaCampos('#frm-have-an-interest');
				$("#frm-have-an-interest .BtnRed").removeAttr("disabled");
				//$("#ConfirmaForm").html(msg);
				$("#frm-have-an-interest").hide('slow');	
				$("#sucess-have-an-interest").show('slow');
				
				$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');
						
		}
	});
	return false;
}
//FIM INTERESSE

//INICIO ENVIO INDIQUE
function enviaIndique()
{
	$("#frm-tell-a-friend .BtnRed").attr("disabled","disabled");
	
	var nome_remetente			= $("#ipt-tell-a-friend-your-name").val();
	var email_remetente			= $("#ipt-tell-a-friend-your-email").val();
	var nome_destinatario		= $("#ipt-tell-a-friend-friend-name").val();
	var email_destinatario		= $("#ipt-tell-a-friend-friend-email").val();
	var descricao				= $("#ipt-tell-a-friend-message").val();
	var empreendimento			= $("#id_empreendimento").val();
	var msg 	= '';
	vet_dados 	= 'nome_remetente='+ nome_remetente
				  +'&email_remetente='+ email_remetente
				  +'&nome_destinatario='+ nome_destinatario
				  +'&email_destinatario='+ email_destinatario
				  +'&empreendimento='+ empreendimento
				  +'&descricao='+ descricao;

	base_url  	= "http://www.nexgroup.com.br/imoveis/enviarIndique";
	$.ajax({
		type: "POST",
		url: base_url,
		data: vet_dados,
		success: function(msg) {
	               	
				limpaCampos('#frm-tell-a-friend');
				$("#frm-tell-a-friend .BtnRed").removeAttr("disabled");
				$("#frm-tell-a-friend").hide('slow');	
				$("#sucess-tell-a-friend").show('slow');
				//perceptions(nome,email,'Nex Group - Indique');
				
				$('#conversaoAdwords').load('http://www.nexgroup.com.br/central/conversao');	
		}
	});
	return false;
}
//FIM INDIQUE

//INICIO ENVIO NEWSLETTER
function validaNewsletter(){		
	var nome			= $("#ipt-newsletter-your-name").val();
	var email			= $("#ipt-newsletter-your-email").val();
	
	$("#ipt-newsletter-your-email").removeClass('error');
	$("#ipt-newsletter-your-name").removeClass('error');
	
	if(nome=='Nome' && email=='E-mail'){
		//alert('1');
		$("#ipt-newsletter-your-name").addClass('error');
		$("#ipt-newsletter-your-email").addClass('error');
		
		return false;
	}else if(nome=='Nome'){
		//alert('2');
		$("#ipt-newsletter-your-name").addClass('error');
		$("#ipt-newsletter-your-email").removeClass('error');
		return false;
	}else if(email=='E-mail'){
		//alert('3');
		$("#ipt-newsletter-your-email").addClass('error');
		$("#ipt-newsletter-your-name").removeClass('error');
		return false;
	}else{
		if(!checkMail(email)){
			//alert('4');
			$("#ipt-newsletter-your-name").removeClass('error');
			$("#ipt-newsletter-your-email").addClass('error');
			return false;	
		}else{
			$("#frm-newsletter2 .BtnRed").attr("disabled","disabled");
			var msg 	= '';
			vet_dados 	= 'nome='+ nome
						  +'&email='+ email;
			
			base_url  	= "http://www.nexgroup.com.br/home/cadastraNewsletter";
			$.ajax({
				type: "POST",
				url: base_url,
				data: vet_dados,
				success: function(msg) {
						
						$("#ipt-newsletter-your-name").val('Nome');
						$("#ipt-newsletter-your-email").val('E-mail');
						
						$("#frm-newsletter2 .BtnRed").removeAttr("disabled");
						if(msg=='cadastrado'){
							$("#frm-newsletter2").html('<p><strong class="Error">Seu e-mail já está cadastrado.</strong></p>');
							return false;
						}else{
							$("#frm-newsletter2").html('<p><strong class="Sucess">Cadastro realizado com sucesso. </strong></p>');
							return false;
						}
						$("#mensagem-frm-newsletter2").show('slow');
						//perceptions(nome,email,'Nex Group - Newsletter');
				}
			});
			return false;
		}
	}
}

function checkMail(mail){
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string"){
		if(er.test(mail)){ return true; }
	}else if(typeof(mail) == "object"){
		if(er.test(mail.value)){ 
					return true; 
				}
	}else{
		return false;
		}
}
//FIM NEWSLETTER

function perceptions(name,mail,title){
	
	var nome =	name;
	var email = mail;
	var pagetitle = title;
	var scriptlocation = "http://www.divex.com.br/SmartPerceptionsV2NexGroup/track.asp";
	var pagedata = 'mtpt=' + escape(pagetitle) + '&mtr=' + escape(document.referrer) + '&mtt=2&mts=' + window.screen.width + 'x' + window.screen.height + '&mti=1&mtz=' + Math.random() + '&nome=' + escape(nome) + '&email=' + escape(email);
	
	$("#PerceptionsFooter").html("<img style='display:none;' height=1 width=1 src='" + scriptlocation + "?" + pagedata + "'>");
	//document.write ('');

}

function limpaCampos (form)
{
    $(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'select':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}

jQuery(function($){
	$('input[name*="telefone"]').mask("9999-9999");
	$('input[name*="telefone_area"]').mask("99");
	$('input[name*="seu_phone"]').mask("9999-9999");
	$('input[name*="seu_phone_area"]').mask("99");
	$('input[name*="celular"]').mask("9999-9999");
	$('input[name*="celular_area"]').mask("99");
	$('input[name*="cep_terreno"]').mask("99999");
	$('input[name*="cep_area_terreno"]').mask("999");
	$('input[name*="cep_vendedor"]').mask("99999");
	$('input[name*="cep_area_vendedor"]').mask("999");
});