$(document).ready(function(){

  $('.search').click(function(){
    if($(".formBusca").css("display") == "none"){
      $('.formBusca').css({width:'0px', display:'block'}).animate({width:200}, 250);
      $('.btn-procurar').animate({width:'70px'}, 250);
      $('#palavra-chave').focus();
    }else{
      $('.formBusca').css({width:'0px', display:'none'}).animate({width:200}, 250);
      $('.btn-procurar').animate({width:'180px'}, 250);
    }
  });

	$('.search_fixed').click(function(){
    if($("#formBuscaFixed").css("display") == "none"){
      $('#formBuscaFixed').css({width:'0px', display:'block'}).animate({width:200}, 250);
      $('#btn-procurar-fixed').animate({width:'70px'}, 250);
      $('#palavra-chave-fixed').focus();
    }else{
      $('#formBuscaFixed').css({width:'0px', display:'none'}).animate({width:200}, 250);
      $('#btn-procurar-fixed').animate({width:'180px'}, 250);
    }
	});

  // $("#palavra-chave").blur(function(){
  //   $('.formBusca').css({width:'0px', display:'none'}).animate({width:200}, 250);
  //   $('.btn-procurar').animate({width:'180px'}, 250);
  // });


    $(".click_video").click(function() {
        var displayMenu = $("#area_tv").css('display');

        $('#area_tv iframe').attr('src', 'http://www.youtube.com/embed/'+$(this).attr('data-id')+'?rel=1');
        $('#area_tv h3').text($(this).attr('data-titulo'))

        if (displayMenu == "none")
        {
            $("#area_tv").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#area_tv").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });
	
	alterarTituloContato = function(titulo){
		$('#titulo_contato').text(titulo);
	}


	var distanciaMaxima = 20; // distancia em km
	var pertoDeVoce = false

    selecionarAbaFiltro = function(target)
    {
    	// console.log('var pertoDeVoce =>' + pertoDeVoce);

		$(target).parent('li').parent('ul').find('li').removeClass('active');
		$(target).parent('li').addClass('active');
    	
		pertoDeVoce = false;

		if($(target).attr('id') == 'todos'){
			status_id = '-';
		}else if($(target).attr('id') == 'lancamento'){
			status_id = '1';
		}else if($(target).attr('id') == 'pronto'){
			status_id = '2';
		}else if($(target).attr('id') == 'perto'){ 

			// console.log($('#tab-location').attr('class'));

			if($('#tab-location.active').length > 0)
			{
				// não faz nada
				// console.log('mapa está ativo');
			}
			else
			{	
				// mostra o mapa
				$('#tab-location > a').trigger('click');
				// console.log('mapa não ativo');
			}

			getGeoLocalizacao();
			pertoDeVoce = true;
			status_id = '-';
		}else{
			status_id = '-';
		}
    	
    	var alterarAba = false;

    	if($(target).attr('id') == 'cards'){
    		alterarAba = true;
    	}else if($(target).attr('id') == 'lista'){
    		alterarAba = true;
    	}else if($(target).attr('id') == 'mapa'){
    		alterarAba = true;
    	}
    	
    	if(alterarAba){
	    	if($('#tab-'+$(target).attr('id')).css('display') == 'none')
	    	{
		    	$(target).parent('li').parent('ul').find('li').removeClass('active');
		    	$(target).parent('li').addClass('active');
		    	
		    	$(target).parent('li').parent('ul').find('li').each(function(){
		    		$('#tab-'+$(this).find('a').attr('id')).slideUp();
		    	});
		    
		    	$('#tab-'+$(target).attr('id')).slideDown(200, function(){
		    		if($(target).attr('id') == 'mapa' && !mapaCriado)
		    		{
		    			mapaCriado = true;
			    		iniciarGoogleMapsHome();
			    	}
		    	});
	    	}
    	}
    	
    	if($('.li-card.active').length > 0){
    		filtrarHomeCard();
    		// console.log('filtrarHomeCard()');
    	}else if($('.li-lista.active').length > 0){
    		filtrarHomeLista();
    		// console.log('filtrarHomeLista()');
    	}else if($('.li-mapa.active').length > 0){
    		filtrarHomeMapa();
    		// console.log('filtrarHomeMapa()');
    	}
    }
    
    /*
     * decide de o imóvel de arrImovel deve ser mostrado na busca
     */
    mostrarFiltro = function(arrImovel){
		var estado_id 	= $('#filtro-estado').val();
		var cidade_id 	= $('#filtro-cidade').val();
		var tipo_id 	= $('#filtro-tipo').val();
		
		if($('#palavrachave').length > 0){
  		var palavraChave = removeAcento($('#palavrachave').val().toLowerCase());
  	}else{
  		var palavraChave = '';
  	}
    	
    mostrar = true;
		if(estado_id != '-' && estado_id != arrImovel['id_estado']){
			mostrar = false;
		}
		
		if(cidade_id != '-' && cidade_id != arrImovel['id_cidade']){
			mostrar = false;
		}
		
		if(tipo_id != '-' && tipo_id != arrImovel['id_tipo']){
			mostrar = false;
		}
		
		if(status_id != '-' && status_id != arrImovel['id_status']){
			mostrar = false;
		}
		
		if(pertoDeVoce){
      if(latitude != 0 && longitude != 0){
        if( distance(latitude, longitude, arrImovel['lat'], arrImovel['lon'], 'k') > distanciaMaxima ){
          mostrar = false;
        }
      }
    }

    if(palavraChave != '' && 
				removeAcento(arrImovel['cidade'].toLowerCase()).indexOf(palavraChave) == -1 && 
				removeAcento(arrImovel['nome'].toLowerCase()).indexOf(palavraChave) == -1 &&
				removeAcento(arrImovel['chamada'].toLowerCase()).indexOf(palavraChave) == -1 &&
				removeAcento(arrImovel['chamada2'].toLowerCase()).indexOf(palavraChave) == -1){
			mostrar = false;
		}
		
		return mostrar;
    }
    
    
    filtrarHomeMapa = function(){
    	removerTodosMarkers();
    	
  		for(var i in imoveisBusca){
  			if(mostrarFiltro(imoveisBusca[i])){
  				// console.log('marker no mapa criado: ' + imoveisBusca[i]['nome']);
  				criaMarkers(i, imoveisBusca[i]['lat'], imoveisBusca[i]['lon'], imoveisBusca[i]['pin'], imoveisBusca[i]['nome'], imoveisBusca[i]['url'])    	
  			}
  		}
    }
    
    
    filtrarHomeLista = function(){
    	var html = '';

		$('#div-imoveisLista').empty();

		for(var i in imoveisBusca){
			if(imoveisBusca[i]['id_status'] == 1){
				statusS = 'Lançamento';
			}else if(imoveisBusca[i]['id_status'] == 2){
        // statusS = 'Concluído';
				statusS = 'Pronto para morar';
			}else if(imoveisBusca[i]['id_status'] == 3){
        // statusS = 'Até 6 meses';
				statusS = 'Em obras';
			}else if(imoveisBusca[i]['id_status'] == 4){
				// statusS = 'Até um ano';
        statusS = 'Em obras';
			}else if(imoveisBusca[i]['id_status'] == 5){
				// statusS = 'Mais de 12 meses';
        statusS = 'Em obras';
			}else if(imoveisBusca[i]['id_status'] == 2){
				statusS = '100% vendido';
			}


      if(imoveisBusca['metragem'] == "Apto."){
        class_style = "two";
      }else{
        class_style = "two-casa";
      }

			if(mostrarFiltro(imoveisBusca[i])){
		    html += '<div class="emp"><!-- EMPREENDIMENTO -->';
				html += ' <a href="'+imoveisBusca[i]['url']+'"><figure class="scales" style="background: url('+imoveisBusca[i]['imagem']+');"></figure></a>';
				html += '<div class="emp-content">';
				html += '<ul class="up">';
				html += '     <li class="one">';
				html += '       <h2>'+imoveisBusca[i]['nome']+'</h2>';
				html += '       <h3><i>'+imoveisBusca[i]['chamada']+'</i></h3>';
				html += '     </li>';
				html += '    <li class="two">';
				html += '      <a href="'+imoveisBusca[i]['url']+'" class="btn small expand btn-red button">Conheça</a>';
				html += '     </li>';
				html += '   </ul>';
				html += '   <ul class="down">';
        if(statusS)
				html += '    <li><i class="icon icon-tag"></i><p>'+statusS+'</p> </li>';
        if(imoveisBusca[i]['cidade'])
				html += '    <li><i class="icon icon-map-marker"></i><p>'+imoveisBusca[i]['cidade']+'</p> </li>';
        if(imoveisBusca[i]['dormitorios'])
				html += '   <li><i class="icones-img one"></i><p>'+imoveisBusca[i]['dormitorios']+'</p> </li>';
        if(imoveisBusca[i]['metragem'])
				html += '     <li><i class="icones-img '+class_style+' two"></i><p>'+imoveisBusca[i]['metragem']+'</p> </li>';
        if(imoveisBusca[i]['vagas'])
				html += '    <li><i class="icones-img tree"></i><p>'+imoveisBusca[i]['vagas']+'</p> </li>';
				html += '   </ul>';
				html += ' </div>';
				html += '</div>';
			}
		}
       
		$('#div-imoveisLista').append(html);
		criarPaginacao();
    }
    
    
    var paginaAtual = 0;
    var paginas = 0 ;
    var itensPorPagina = 0;
	var totalItens = 0;
    criarPaginacao = function(){
    	itensPorPagina = 5;
    	totalItens = $('#div-imoveisLista .emp').length;
    	paginas = Math.ceil(totalItens / 5);
    	paginaAtual = 0;
    	
    	var i=0;
    	$('.emp').each(function(){
    		if(i++ >= itensPorPagina){
    			$(this).css('display', 'none');
    		}
    	});
    	
    	var html = '';
    	html += '<div class="pagination-centered">';
		html += '<ul class="pagination clickroll">';
		html += '<li class="arrow anterior unavailable"><a href="#div-imoveisLista" onclick="trocarPagina(\'menos\')">&laquo;</a></li>';
		html += '<li class="current"><a href="#div-imoveisLista" onclick="trocarPagina(0)">1</a></li>';
		for(var i=2; i<=paginas; i++){
			html += '<li><a href="#div-imoveisLista" onclick="trocarPagina('+ i +')">'+ i +'</a></li>';
		}
		html += '<li class="arrow proxima"><a href="#div-imoveisLista" onclick="trocarPagina(\'mais\')">&raquo;</a></li>';
		html += '</ul>';
		html += '</div>';
    	
		$('#div-imoveisLista').append(html);
    }
    
    trocarPagina = function(flag){
    	var proxima = 0;
    	if(flag == 'mais'){
    		proxima = paginaAtual+1;
    	}else if(flag == 'menos'){
    		proxima = paginaAtual-1;
    	}else{
    		proxima = flag-1;
    	}
    	
    	if(proxima < 0){
    		proxima = 0;
    	}else if(proxima >= paginas-1){
    		proxima = paginas-1;
    	}
    	
    	if(proxima < paginas-1){
    		$('.arrow.proxima').removeClass('unavailable');
    	}else{
    		$('.arrow.proxima').addClass('unavailable');
    	}
    	
    	if(proxima > 0){
    		$('.arrow.anterior').removeClass('unavailable');
		}else{
			$('.arrow.anterior').addClass('unavailable');
		}
    	
    	
    	paginaAtual = proxima;
    	var inicio 	= proxima * itensPorPagina;
    	var fim 	= (proxima * itensPorPagina) + itensPorPagina;
    	
    	var i=0;
    	$('.emp').each(function(){
    		if(i >= inicio && i < fim){
//    			$(this).css('display', 'block');
    			$(this).slideDown(500);
    		}else{
    			$(this).slideUp(500);
//    			$(this).css('display', 'none');
    		}
    		
    		i++;
    	});
    	
    	$('.pagination-centered li').removeClass('current');
    	$('.pagination-centered li').eq(proxima+1).addClass('current');
    	
    }
    
    
    buscar = function(obj){
    	$('#palavrachave').val($(obj).find('span').text());
    	submitBusca();
    }
    
    submitBusca = function(){
      $("#none_search").css("display","none");
    	filtrarHomeCard(); 
    	filtrarHomeLista(); 
    	filtrarHomeMapa();
    	return false;
    }

	filtrarHomeCard = function(){
		var html = '';
		var box = 1;
		var boxS = '';
		var i = 0;
    var width = $(window).width();

		$('#div-imoveisDestaque').empty();
		$('.imoveisBusca').trigger('destroy');
		$('#pager-imoveisBusca').empty();
		html += '<ul class="imoveisBusca" id="foo21">';

    if(width <= 475){
      var j=0;
      for(var i in imoveisBusca){
        if(mostrarFiltro(imoveisBusca[i])){
          if(j%3 == 0){
            box = 1;
          }
          if(box > 1){
            boxS = box;
          }else{
            boxS = '';
          }

          boxS = "";

          html += '<li>'
          html += '<a href="'+imoveisBusca[i]['url']+'" class="box'+boxS+' scales" style="background:url('+imoveisBusca[i]['imagem']+')">';
          html += '<div class="left">';
          html += ' <h2>'+imoveisBusca[i]['nome']+'</h2>';
          html += '  <h3><i>'+imoveisBusca[i]['chamada']+'</i></h3>';
          html += ' <h4>'+imoveisBusca[i]['chamada2']+'</h4>';
          html += '  <div href="'+imoveisBusca[i]['url']+'" class="btn btn-white button">Conheça</div>';
          html += '</div>';
          html += '<div class="pix"></div>';
          html += '<div class="black"></div>';
          html += '</a>';
          html += '</li>';
          box++;
          j++;
        }
      }
    }else{
      var j=0;
      for(var i in imoveisBusca){
        if(mostrarFiltro(imoveisBusca[i])){
          if(j == 0){
            html += '<li>'
          }else if(j%3 == 0){
            box = 1;
            html += '</li>';
            html += '<li>';
          }
          if(box > 1){
            boxS = box;
          }else{
            boxS = '';
          }

          html += '<a href="'+imoveisBusca[i]['url']+'" class="box'+boxS+' scales" style="background:url('+imoveisBusca[i]['imagem']+')">';
          html += '<div class="left">';
          html += ' <h2>'+imoveisBusca[i]['nome']+'</h2>';
          html += '  <h3><i>'+imoveisBusca[i]['chamada']+'</i></h3>';
          html += ' <h4>'+imoveisBusca[i]['chamada2']+'</h4>';
          html += '  <div href="'+imoveisBusca[i]['url']+'" class="btn btn-white button">Conheça</div>';
          html += '</div>';
          html += '<div class="pix"></div>';
          html += '<div class="black"></div>';
          html += '</a>';
          box++;
          j++;
        }
      }
    }

		if(j == 0){
      $("#none_search").css("display","block");
    }else{
      html += '</li></ul>';
      html += '    <div class="clearfix"></div>';
      html += '<div class="navegation">';
      html += '<a id="prev-imoveisBusca" class="prev" href="#"></a>';
      html += '<div id="pager-imoveisBusca" class="pager auto-margin-bolinhas"></div>';
      html += '<a id="next-imoveisBusca" class="next" href="#"></a>';
      html += '</div>';
    }
		
		$('#div-imoveisDestaque').append(html);
		iniciarCarouselFiltro();
	}
	
	iniciarCarouselFiltro = function(){
	    $('.imoveisBusca').carouFredSel({
	        circular: false,
	        responsive: true,
	        width: null,
	        height: null,
	        prev: '#prev-imoveisBusca',
	        next: '#next-imoveisBusca',
	        mousewheel: false,
	        swipe: {
	           onTouch: true
	        },
	        scroll : {
	            easing          : "quadratic",
	            duration        : 2000,                         
	            pauseOnHover    : false
	        },
	        pagination: "#pager-imoveisBusca",
	        auto: false,
	        items: {
	            width: 200,
	            height: null,
	            visible: {
	                min: 1,
	                max: 1
	            }
	        }
	    });
	}
	
	selecionaTipoMapa = function(tipo){
		if(tipo == 'rua'){
			$('#mapa-rua').addClass('active');
			$('#mapa-satelite').removeClass('active');
		}else{
			$('#mapa-rua').removeClass('active');
			$('#mapa-satelite').addClass('active');
		}
		
	}
	
	if($('#empreendimentos-home').length > 0){
		filtrarHomeCard();
		filtrarHomeLista();
	}
	
	
	
	
	if($('#ancora-topo').length>0){
	    var alturaAncora = $('#ancora-topo').offset().top;
	    $(window).scroll(function() {
	        if ($(this).scrollTop() > alturaAncora + 50) {
	           $('.submenu-home').removeClass('relativo');
	            $('.submenu-home').addClass('fixo');
	       } else if ($(this).scrollTop() <= alturaAncora) {
	          $('.submenu-home').addClass('relativo');
	            $('.submenu-home').removeClass('fixo ');
	        }
	    });
	}
	

	validarFormulario = function(){
    	var erro = 0;
    	
    	$('#frm-phone-assistance .erro').removeClass('erro');
    	
    	if($('#frm-phone-assistance #ipt-phone-assistance-your-name').val().length < 3){
    		$('#ipt-phone-assistance-your-name').addClass('erro');
    		erro++;
    	}
    	
    	if($('#frm-phone-assistance #ipt-phone-assistance-your-email').val().length < 3){
    		$('#ipt-phone-assistance-your-email').addClass('erro');
    		erro++;
    	}
    	
    	if($('#frm-phone-assistance #ipt-phone-assistance-your-phone').val().length < 8){
    		$('#ipt-phone-assistance-your-phone').addClass('erro');
    		erro++;
    	}
    	
    	if($('#frm-phone-assistance #ipt-phone-assistance-message').val().length < 10){
    		$('#ipt-phone-assistance-message').addClass('erro');
    		erro++;
    	}
    	
    	if(erro == 0){
			$('#frm-phone-assistance').submit();    		
    	}
    }
    
    
    mostrarObrasPorEmpresa = function(target){
    	var id_empresa = $(target).val();
    	
    	$('.list-obras').slideUp(200, function(){
    		$('#id_empresa-'+id_empresa).show();
    		$('#id_empresa-'+id_empresa+' ul').trigger('updateSizes');
    	});
    }
    
    
    var mapaCriado = false;
    selecionarAba = function(target){
    	if($('#tab-'+$(target).attr('id')).css('display') == 'none'){
	    	$(target).parent('li').parent('ul').find('li').removeClass('active');
	    	$(target).parent('li').addClass('active');
	    	
	    	$(target).parent('li').parent('ul').find('li').each(function(){
	    		$('#tab-'+$(this).find('a').attr('id')).slideUp();
	    	});
	    
	    	$('#tab-'+$(target).attr('id')).slideDown(200, function(){
	    		if($(target).attr('id') == 'mapa' && !mapaCriado){
	    			mapaCriado = true;
		    		iniciarGoogleMapsHome();
		    	}
	    	});
    	}
    	
    	$('#foo21').trigger('updateSizes');
    	$('#foo22').trigger('updateSizes'); 
    	$('#foo23').trigger('updateSizes');
    	$('#foo24').trigger('updateSizes');
    	
    	$('#foo10').trigger('updateSizes');
    	$('#foo11').trigger('updateSizes');
    	$('#foo12').trigger('updateSizes');
    	
    }
    
//    $('#map_canvas').hover(function(){
//    	$('#localizacao #endereco').fadeOut();
//    }, function(){
//    	$('#localizacao #endereco').fadeIn();
//    });
    
    
    noticiaTrocarMesAno = function(){
      var ano = parseInt($('#noticia-ano').val());

    	var mes = parseInt($('.mes-ano-'+ano).val());
	
    	$('.nenhum-registro').slideUp();
    	$('.li-noticias').each(function(){
    		if(!$(this).hasClass('mes-'+mes) || !$(this).hasClass('ano-'+ano)){
    			$(this).slideUp();
    		}
    	});
    	
    	sleep(500, function(){
    		var total = 0;

        if(mes > 0)
        {
          $('.li-noticias').each(function(){
            if($(this).hasClass('mes-'+mes) && $(this).hasClass('ano-'+ano)){
              total++;
              $(this).slideDown();
            }
          });
        }
        else
        {
          $('.li-noticias').each(function(){
            if($(this).hasClass('ano-'+ano)){
              total++;
              $(this).slideDown();
            }
          });
        }
    		
    		
    		if(total == 0){
    			$('.nenhum-registro').slideDown();
    		}
    	});
    }
    
    
    ativarPin = function(target){
    	if($(target).parent('li').hasClass('active')){
    		$(target).parent('li').removeClass('active');
    	}else{
    		$(target).parent('li').addClass('active');
    	}
    	
    	//remove todos os markers
    	removerMarkers();
    	//cria os markers de todos os tipos selecionados
    	$('#ul-marker li.active').each(function(){
    		adicionarRemoverMarkersByTipo($(this).find('a').attr('data-tipo'), $(this).find('a').attr('data-icon'));
    	});
    };

    // remove as tabs sem marcação
    initTabs = function()
    {
      $('#ul-marker li').each(function(){
        getTotalMarkersByTipo($(this).find('a').attr('data-tipo'));
      });
    };

    initTabs();



    // //faz o mapa carregar todos os pins que já começam ativos;
    // $('#ul-marker .active').each(function(){
    // 	//primeiro remove a class active, pq que o método ativarPin sempre inverte o marcador de ativo
    // 	$(this).removeClass('active');
    // 	ativarPin($(this).find('a'));
    // })
    
    selecionarAbaCarousel = function(target){
    	if($('.tab-'+$(target).attr('id')).css('display') == 'none'){
	    	$(target).parent('li').parent('ul').find('li').removeClass('active');
	    	$(target).parent('li').addClass('active');
	    	
	    	$(target).parent('li').parent('ul').find('li').each(function(){
	    		$('.tab-'+$(this).find('a').attr('id')).hide();
	    	});
	    	
	    	$('.tab-'+$(target).attr('id')).show();
	    	criarMarkerHome(); 
	    	
        	$('#foo21').trigger('updateSizes');
        	$('#foo22').trigger('updateSizes'); 
        	$('#foo23').trigger('updateSizes');
        	$('#foo24').trigger('updateSizes');
    	}
    }
    
    selecioneMenuInterna = function(target){
    	$(target).parent('li').parent('ul').find('li').removeClass('active');
    	$(target).parent('li').addClass('active');
    }
    

    selecionarAbaPlantas = function(target){
    	if($('#tab-'+$(target).attr('id')).css('display') == 'none'){
	    	$(target).parent('li').parent('ul').find('li').removeClass('active');
	    	$(target).parent('li').addClass('active');
	    	
	    	$(target).parent('li').parent('ul').find('li').each(function(){
	    		$('#tab-'+$(this).find('a').attr('id')).hide();
	    	});
	    	
	    	$('#tab-'+$(target).attr('id')).show();

	    	$('#foo51').trigger('updateSizes');
			$('#foo52').trigger('updateSizes'); 
			$('#foo53').trigger('updateSizes');
			$('#foo54').trigger('updateSizes');
			$('#foo55').trigger('updateSizes');
			$('#foo56').trigger('updateSizes');
			
			
			$('#foo61').trigger('updateSizes');
			$('#foo62').trigger('updateSizes'); 
			$('#foo63').trigger('updateSizes');
			$('#foo64').trigger('updateSizes');
			$('#foo65').trigger('updateSizes');
    	}
    }
    

    // COOKIE DOS EMPREENDIMENTOS FAVORITOS

    function getCookie(cname)
    {
    	var name = cname + "=";
    	var ca = document.cookie.split(';');

    	for(var i=0; i<ca.length; i++)
    	{
    		var c = ca[i];
    		while (c.charAt(0)==' ') c = c.substring(1);
    		if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    	}

    	return "";
    }

    function setCookie(cname, cvalue, exdays)
    {
    	var d = new Date();

    	d.setTime(d.getTime() + (exdays*24*60*60*1000));

    	var expires = "expires="+d.toGMTString();
    	document.cookie = cname + "=" + cvalue + "; " + expires+"; path=/";
    }

    
    salvarImovelFavoritos = function(target, id_imovel)
    {
    	var favoritos = getCookie('favorito');

    	ex = favoritos.split(',');

    	console.log('Array ex: ' + ex);

		if(ex.length > 1)
		{
			console.log('array maior que 1');
			console.log('tamanho: ' + ex.length);

			for (var i = 0; i <= ex.length; i++)
	    	{
	    		console.log('__valor:' + ex[i] + ' == ' + id_imovel);

	    		if(ex[i] == id_imovel)
	    		{	
	    			console.log('já é favorito');
	    			removerImovelFavorito(id_imovel);
	    			return false;
	    		}
	    	}
		}
		else if(ex.length == 1) 
		{
			console.log('array igual 1');
			if(ex[0] == id_imovel)
			{
				console.log('já é favorito');
				removerImovelFavorito(id_imovel);
				return false;
			}
		}
		else
		{
			console.log('algo deu errado');
		}
    	

    	console.log(favoritos);

    	if(favoritos == "")
    	{
    		favoritos = id_imovel;
    	}
    	else
    	{
    		favoritos += ',' + id_imovel;
    	}

    	console.log(favoritos);

    	setCookie('favorito', favoritos, 30);

    	$('#btn-favorito1').css('color', '#b72025');
    	$('#btn-favorito2').css('color', '#ffffff').css('background', '#b72025');

    	$.ajax({
    		type: 'POST',
    		url: BASE_URL + 'imoveis/favoritar/'+id_imovel,
    		data: {},
    		dataType: 'json',
    		success: function(data)
    		{ 

    		}
    	});
    }

    function removerImovelFavorito(id_imovel)
    {
    	$.ajax({
    		type: 'POST',
    		url: BASE_URL + 'imoveis/removerFavorito',
    		data: {id_imovel: id_imovel},
    		dataType: 'json',
    		success: function(response)
    		{ 
    			if(response.erro == 0) 
    			{
    				console.log('imovel removido');

    				$('#btn-favorito1').removeAttr('style');
    				$('#btn-favorito2').removeAttr('style');

    				setCookie('favorito', response.favoritos, 30);
    			}
    		}
    	});
    }


    $(window).resize(function() { /*quando redimensionar a janela faz a mesma coisa */  
//    	$('.auto-margin2').each(function(){
//			var width = $(this).css('width');
//			console.log(width);
//    	});
    	
//    	$('.auto-margin').each(function(){
//    		$(this).css('margin-left', (parseInt($(this).css('width').split('px').join('')) / 2) * -1)
//    	});
   });
    
    
    
    autoMarginBolinhas = function(){
    	$('.auto-margin-bolinhas').each(function(){
        	var total = 0;
        	$(this).find('a').each(function(){
        		total += 10;
        	});
        	
        	$(this).css('margin-left', (total * -1));
        });	
    }
    setInterval(autoMarginBolinhas, 250);
    
    
    $('.auto-margin').each(function(){
		$(this).css('margin-left', (parseInt($(this).css('width').split('px').join('')) / 2) * -1)
	});

    
    
    
    
    
    if($('#empreendimentos-interna').length > 0){
	    $(window).scroll(function() {
//	    	console.log('this: ' + $(this).scrollTop());
//	    	console.log('galeria: ' + $('#ancora-galeria').offset().top);
//	    	console.log('plantas: ' + $('#ancora-plantas').offset().top);
	    	
	    	if($('#ancora-topo').length > 0 && $(this).scrollTop() <= $('#ancora-topo').offset().top + 100){
	    		selecioneMenuInterna($('#btn-topo'));
	    	}else if($('#ancora-galeria').length > 0 && $(this).scrollTop() <= $('#ancora-galeria').offset().top + 100){
	    		selecioneMenuInterna($('#btn-galeria'));
	    	}else if($('#ancora-plantas').length > 0 && $(this).scrollTop() <= $('#ancora-plantas').offset().top + 100){
	    		selecioneMenuInterna($('#btn-plantas'));
	    	}else if($('#ancora-localizacao').length > 0 && $(this).scrollTop() <= $('#ancora-localizacao').offset().top + 100){
	    		selecioneMenuInterna($('#btn-localizacao'));
	    	}else if($('#ancora-obra').length > 0 && $(this).scrollTop() <= $('#ancora-obra').offset().top + 100){
	    		selecioneMenuInterna($('#btn-obra'));

	    	}else if($('#ancora-apresentacao2').length > 0 && $(this).scrollTop() <= $('#ancora-apresentacao2').offset().top + 100){
	    		selecioneMenuInterna($('#btn-apresentacao'));
	    	}else if($('#ancora-realizacoes').length > 0 && $(this).scrollTop() <= $('#ancora-realizacoes').offset().top + 100){
	    		selecioneMenuInterna($('#btn-realizacoes'));
	    	}else if($('#ancora-obras').length > 0 && $(this).scrollTop() <= $('#ancora-obras').offset().top + 100){
	    		selecioneMenuInterna($('#btn-obras'));
	    	}else if($('#ancora-responsabilidade').length > 0 && $(this).scrollTop() <= $('#ancora-responsabilidade').offset().top + 100){
	    		selecioneMenuInterna($('#btn-responsabilidade'));
	    	}else if($('#ancora-gestao').length > 0 && $(this).scrollTop() <= $('#ancora-gestao').offset().top + 100){
          selecioneMenuInterna($('#btn-gestao'));
        }else if($('#ancora-rh').length > 0 && $(this).scrollTop() <= $('#ancora-rh').offset().top + 100){
          selecioneMenuInterna($('#btn-rh'));
        }
	    });
    }
    

});













//INICIO ENVIO NEWSLETTER
function validaNewsletter(){		
	var nome			= $("#ipt-newsletter-your-name").val();
	var email			= $("#ipt-newsletter-your-email").val();
	
	$("#ipt-newsletter-your-email").removeClass('error');
	$("#ipt-newsletter-your-name").removeClass('error');
	
	// console.log("OI")
	
	if(nome.length < 4){
		$("#ipt-newsletter-your-name").addClass('error');
	}else if(!checkMail(email)){
		//alert('4');
		$("#ipt-newsletter-your-email").addClass('error');
		return false;	
	}else{
		$("#btn-enviar-newsletter").attr("disabled","disabled");
		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email;
		
		base_url  	= "http://www.nexgroup.com.br/home/cadastraNewsletter";
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
				$("#ipt-newsletter-your-name").val('');
				$("#ipt-newsletter-your-email").val('');
					
				$("#btn-enviar-newsletter").removeAttr("disabled");
				if(msg=='cadastrado'){
					alert('Seu e-mail já está cadastrado.');
					return false;
				}else{
					alert('Cadastro realizado com sucesso.');
					return false;
				}
				$("#mensagem-frm-newsletter2").show('slow');
				//perceptions(nome,email,'Nex Group - Newsletter');
			}
		});
		return false;
	}
}

function checkMail(mail){
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string"){
		if(er.test(mail)){ return true; }
	}else if(typeof(mail) == "object"){
		if(er.test(mail.value)){ 
					return true; 
				}
	}else{
		return false;
		}
}
//FIM NEWSLETTER

function sleep(millis, callback) {
    setTimeout(function()
            { callback(); }
    , millis);
}




function removeAcento(palavra){
    //pega valor do campo e converte para letras minúsculas
    texto = palavra;
    //faz as substituições dos acentos
    texto = texto.replace(/[á|ã|â|à]/gi, "a");
    texto = texto.replace(/[é|ê|è]/gi, "e");
    texto = texto.replace(/[í|ì|î]/gi, "i");
    texto = texto.replace(/[õ|ò|ó|ô]/gi, "o");
    texto = texto.replace(/[ú|ù|û]/gi, "u");
    texto = texto.replace(/[ç]/gi, "c");
    texto = texto.replace(/[ñ]/gi, "n");
    texto = texto.replace(/[á|ã|â]/gi, "a");
    //faz a substituição dos espaços e outros caracteres por - (hífen)
//    texto = texto.replace(/\W/gi, "-");
    // remove - (hífen) duplicados
    texto = texto.replace(/(\-)\1+/gi, "-");
    return texto;
}

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var radlon1 = Math.PI * lon1/180
    var radlon2 = Math.PI * lon2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
}


var latitude = 0;
var longitude = 0;
function getGeoLocalizacao(){
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position){ 
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        
        var center = new google.maps.LatLng(latitude,longitude);  
        map.panTo(center); 
        
        // console.log('latitude: ' + latitude);
        // console.log('longitude: ' + longitude);

        if($('.li-card.active').length > 0){
          filtrarHomeCard();
          // console.log('geo filtrarHomeCard()');
        }else if($('.li-lista.active').length > 0){
          filtrarHomeLista();
          // console.log('geo filtrarHomeLista()');
        }else if($('.li-mapa.active').length > 0){
          filtrarHomeMapa();
          // console.log('geo filtrarHomeMapa()');
        }
      }, function(){ 
        // console.log('geo erro'); 
      });
  } else {
      error('Seu navegador não suporta <b style="color:black;background-color:#ffff66">Geolocalização</b>!');
  }
}