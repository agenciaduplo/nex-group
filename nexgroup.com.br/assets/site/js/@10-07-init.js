$(document).ready(function(){

    $(window).load(function() {
      $('.loadmore').fadeOut();
      $('#load-site').fadeOut();
      $('#featured').orbit();
      $('#featured2').orbit();
      $('#featured3').orbit();
      $('#featured4').orbit();
      changeNICarouObrasRealizadas();
    });


//  -------- ROLAGEM NO CLICK -------- //
  $('.clickroll').localScroll({duration: 1500, queue: true});


//  -------- HEADER FIXO -------- //

    var navigations = $('#ancora');
    pos = navigations.offset();

    $(window).scroll(function(){
    	if($('.header-fixo.parado').length == 0){
        if($(this).scrollTop() > pos.top + navigations.height()){
          $('.header-fixo').removeClass('absoluto');
          $('.header-fixo').addClass('fixo animated fadeInDown');
        }else if ($(this).scrollTop() <= pos.top){
          $('.header-fixo').addClass('absoluto');
          $('.header-fixo').removeClass('fixo animated fadeInDown');
        }
    	}
    });



//  -------- PARALLAX -------- //

  //  $('.paral').parallax({speed: -0.9});





      // -- SCROLL SUAVE --  NÃO FUNCIONA NO MOZILA !!!! wheel (attr) @ addEventListener, buga o carregamento das outras dependências

      // if (isIE () == 8) {
       // IE8 code
      // } else {

        // if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
        // window.onmousewheel = document.onmousewheel = wheel;

        // var time = 0;
        // var distance = 25;

        // function wheel(event) {
        //     if (event.wheelDelta) delta = event.wheelDelta / 120;
        //     else if (event.detail) delta = -event.detail / 3;

        //     handle();
        //     if (event.preventDefault) event.preventDefault();
        //     event.returnValue = false;
        // }

        // function handle() {

        //     $('html, body').stop().animate({
        //         scrollTop: $(window).scrollTop() - (distance * delta)
        //     }, time);
        // }


        // $(document).keydown(function (e) {

        //     switch (e.which) {
        //         //up
        //         case 38:
        //             $('html, body').stop().animate({
        //                 scrollTop: $(window).scrollTop() - distance
        //             }, time);
        //             break;

        //             //down
        //         case 40:
        //             $('html, body').stop().animate({
        //                 scrollTop: $(window).scrollTop() + distance
        //             }, time);
        //             break;
        //     }
        // });

      // }    // else;


//  -------- DIMENSSÕES DA TELA -------- //

       var altura_tela = $(window).height();/*cria variável com valor do altura da janela*/   

       $(".bloco").height(altura_tela); /* aplica a variável a altura da div*/  

       $(window).resize(function(){ /*quando redimensionar a janela faz a mesma coisa */
        var altura_tela = $(window).height();
         $(".bloco").height(altura_tela);
       });

       var largura_tela = $(window).width();




    $(".first").click(function(){
        var displayMenu = $("#menu").css('display');

        if(displayMenu == "none"){
            $(".first").addClass('active');

            $("#menu").animate({
                width: "show", opacity: "toggle"
            }, {duration: "fast"});
        }else{
            $(".first").removeClass('active');

            $("#menu").animate({
                width: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".click_fone").click(function() {
        var displayMenu = $("#area_fone").css('display');

        if(displayMenu == "none"){
            $("#area_fone").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }else{
            $("#area_fone").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });
    
    $(".click_contato").click(function(){

        var displayMenu = $("#area_contato").css('display');

        if(displayMenu == "none"){
          $("#area_contato").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_contato").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });
        
    $(".click_contato_cliente").click(function(){

        var displayMenu = $("#area_contato_cliente").css('display');

        if(displayMenu == "none"){
          $("#area_contato_cliente").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_contato_cliente").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    }); 
           
    $(".click_atendimento_nex").click(function(){

        var displayMenu = $("#area_atendimento_nex").css('display');

        if(displayMenu == "none"){
          $("#area_atendimento_nex").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_atendimento_nex").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });

    $(".click_interesse").click(function(){

        var displayMenu = $("#area_interesse").css('display');

        if(displayMenu == "none"){
          $("#area_interesse").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_interesse").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });

    $(".click_mapa").click(function(){

        var displayMenu = $("#area_mapa").css('display');

        if(displayMenu == "none"){
          $("#area_mapa").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_mapa").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });


    $(".click_inicio").click(function(){
        var displayMenu = $("#area_inicio").css('display');

        if(displayMenu == "none"){
          $("#area_inicio").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_inicio").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
          
          
          if($('#checkbox-nao-mostrar-mais').is(':checked')){
          	setCookie('naoAbrirMaisInicio', 'true', 30);
          }
        }
    });
    
    $(".click_tv").click(function(){
        var displayMenu = $("#area_tv").css('display');

        if(displayMenu == "none"){
          $("#area_tv").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_tv").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });

    $(".click_revista").click(function(){
        var displayMenu = $("#area_revista").css('display');

        if(displayMenu == "none"){
          $("#area_revista").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_revista").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });

    $(".click_politica").click(function(){
        var displayMenu = $("#area_politica").css('display');

        if(displayMenu == "none"){
          $("#area_politica").animate({
              height: "show", opacity: "toggle"
          }, {duration: "fast"});
        }else{
          $("#area_politica").animate({
              height: "hide", opacity: "toggle"
          }, {duration: "fast"});
        }
    });


    $("#filtro-estado").change(function(){
      $("option", $("#filtro-cidade")).eq(0).prop('selected', true);

      $(".cidades", $("#filtro-cidade")).hide();

      if(this.value != '-')
        $("option", $("#filtro-cidade")).eq(0).text('Filtrar por Cidade:');
      else
        $("option", $("#filtro-cidade")).eq(0).text('Filtrar por Cidade: Selecione um estado.');

      $(".uf-"+this.value, $("#filtro-cidade")).show();

      filtrarHomeCard();
      filtrarHomeLista();
    });



//  -------- CARROSEUL DESTAQUE -------- //

    var foo = $('#foo');
    $('#foo').carouFredSel({
        circular: true,
        infinite: true,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev',
        next: '#next',
        mousewheel: false,
        // swipe: false,
        scroll : {
            fx              : "crossfade",
            items           : 1,
            easing          : "linear",
            duration        : 1000,                         
            pauseOnHover    : false
        },
        pagination: "#pager",
        auto: true,
        items: {
            width: 200,
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });

    // Trigger elsewhere the swipe event , so this can work with links on carousel !
    $("#foo").swipe({
        excludedElements: "button, input, select, textarea, .noSwipe",
        swipeLeft: function() {
            $('#foo').trigger('next', true);
        },
        swipeRight: function() {
            $('#foo').trigger('prev', true);
        },
        tap: function(event, target) {
            // in case of an image wrapped by a link click on image will fire parent link
            $(target).parent().trigger('click');
        }
    });



//  -------- CARROSEUL VISTOS -------- //

  if($("#foo1").length ){
    var foo1 = $('#foo1');
    $('#foo1').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev1',
        next: '#next1',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager1",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // IMOVEIS QUE VOCÊ JÁ VU

  if($("#foo10").length ){
    var foo10 = $('#foo10');
    $('#foo10').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev10',
        next: '#next10',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager10",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo10").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo10').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo10').trigger('prev', true);
      },
      tap: function(event, target) {
          // in case of an image wrapped by a link click on image will fire parent link
          $(target).parent().trigger('click');
      }
  });



  // OBRAS REALIZADAS


  if($("#foo11").length){
    var foo11 = $('#foo11');
    $('#foo11').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev11',
        next: '#next11',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager11",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo11").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo11').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo11').trigger('prev', true);
      },
      tap: function(event, target) {
          $(target).parent().trigger('click');
      }
  });


  // GESTAO CORPORATIVA


  if($("#foo_gestao").length){
    var foo_gestao = $('#foo_gestao');
    $('#foo_gestao').carouFredSel({
        circular: true,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev_gestao',
        next: '#next_gestao',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager_gestao",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo_gestao").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo_gestao').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo_gestao').trigger('prev', true);
      },
      tap: function(event, target) {
          $(target).parent().trigger('click');
      }
  });





  if($("#foo12").length){
    var foo12 = $('#foo12');
    $('#foo12').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev12',
        next: '#next12',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager12",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo12").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo12').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo12').trigger('prev', true);
      },
      tap: function(event, target) {
          // in case of an image wrapped by a link click on image will fire parent link
          $(target).parent().trigger('click');
      }
  });



  if($("#foo13").length){
    var foo11 = $('#foo13');
    $('#foo13').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev13',
        next: '#next13',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager13",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo13").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo13').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo13').trigger('prev', true);
      },
      tap: function(event, target) {
          // in case of an image wrapped by a link click on image will fire parent link
          $(target).parent().trigger('click');
      }
  });

  if($("#foo14").length){
    var foo14 = $('#foo14');
    $('#foo14').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev14',
        next: '#next14',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager14",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
  $("#foo14").swipe({
      excludedElements: "button, input, select, textarea, .noSwipe",
      swipeLeft: function() {
          $('#foo14').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo14').trigger('prev', true);
      },
      tap: function(event, target) {
          // in case of an image wrapped by a link click on image will fire parent link
          $(target).parent().trigger('click');
      }
  });


  var changeNICarouObrasRealizadas = function(showThatManyItems)
  {
    var showThatManyItems = 5;

    var w = $(window).width();

    if(w < 378)
      showThatManyItems = 1;
    if(w >= 378 && w < 755)
      showThatManyItems = 2;
    if(w >= 755 && w < 1024)
      showThatManyItems = 3;
    if(w >= 1024)
      showThatManyItems = 5;

    $("#foo11").trigger('configuration', ['items', {visible : showThatManyItems}], true);
    $("#foo12").trigger('configuration', ['items', {visible : showThatManyItems}], true);
    $("#foo13").trigger('configuration', ['items', {visible : showThatManyItems}], true);
    $("#foo14").trigger('configuration', ['items', {visible : showThatManyItems}], true);
  }

  $(window).resize(function()
  { 
    changeNICarouObrasRealizadas();
  });


  var foo15 = $('#foo15');
  $('#foo15').carouFredSel({
      circular: false,
      responsive: true,
      width: null,
      height: null,
      prev: '#prev15',
      next: '#next15',
      mousewheel: false,
      // swipe: {
      //     onTouch: true
      // },
      scroll : {
          items           : 1,
          easing          : "quadratic",
          duration        : 2000,                         
          pauseOnHover    : false
      },
      pagination: "#pager15",
      auto: false,
      items: {
          height: null,
          visible: {
              min: 1,
              max: 1
          }
      }
  });

  $("#foo15").swipe({
      excludedElements: "button, input, select, textarea, a, .noSwipe",
      swipeLeft: function() {
          $('#foo15').trigger('next', true);
      },
      swipeRight: function() {
          $('#foo15').trigger('prev', true);
      },
      tap: function(event, target) {
          $(target).parent().trigger('click');
      }
  });






//  -------- CARROSEUL CARDS -------- //

  if($("#foo21").length){
    var foo21 = $('#foo21');
    $('#foo21').carouFredSel({
        circular: true,
        infinite: true,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev21',
        next: '#next21',
        mousewheel: false,
        // swipe: {
        //    onTouch: true
        // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager21",
        auto: true,
        items: {
            width: 200,
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
  }

  // Trigger elsewhere the swipe event , so this can work with links on carousel !
    $("#foo21").swipe({
        excludedElements: "button, input, select, textarea, a, .noSwipe",
        swipeLeft: function() {
            $('#foo21').trigger('next', true);
        },
        swipeRight: function() {
            $('#foo21').trigger('prev', true);
        },
        tap: function(event, target) {
            // in case of an image wrapped by a link click on image will fire parent link
            $(target).parent().trigger('click');
        }
    });

  

  if($("#foo22").length){
    var foo22 = $('#foo22');
    $('#foo22').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev22',
        next: '#next22',
        mousewheel: false,
        swipe: {
           onTouch: true
        },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager22",
        auto: true,
        items: {
            width: 200,
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
  }

  if($("#foo23").length){
    var foo23 = $('#foo23');
    $('#foo23').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev23',
        next: '#next23',
        mousewheel: false,
        swipe: {
           onTouch: true
        },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager23",
        auto: true,
        items: {
            width: 200,
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
  }

  if($("#foo24").length){
    var foo24 = $('#foo24');
    $('#foo24').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev24',
        next: '#next24',
        mousewheel: false,
        swipe: {
           onTouch: true
        },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },
        pagination: "#pager24",
        auto: true,
        items: {
            width: 200,
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
  }


//  -------- CARROSEUL NOTICIAS -------- //

    var foo3 = $('#foo3');
    $('#foo3').carouFredSel({
        circular: true,
        infinite: true,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev3',
        next: '#next3',
        mousewheel: false,
          // swipe: {
          //    onTouch: true
          // },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,                         
            pauseOnHover    : false
        },

        pagination: "#pager3",
        auto: false,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 5
            }
        }
    });

    // Trigger elsewhere the swipe event , so this can work with links on carousel !
    $("#foo3").swipe({
        excludedElements: "button, input, select, textarea, a, .noSwipe",
        swipeLeft: function() {
            $('#foo3').trigger('next', true);
        },
        swipeRight: function() {
            $('#foo3').trigger('prev', true);
        },
        tap: function(event, target) {
            // in case of an image wrapped by a link click on image will fire parent link
            $(target).parent().trigger('click');
        }
    });


//  -------- CARROSEUL GALERIA EMPREENDIMENTOS -------- //
  if($("#foo4").length){
    var foo4 = $('#foo4');
    $('#foo4').carouFredSel({
        circular: false,
        infinite: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev4',
        next: '#next4',
        mousewheel: false,
        swipe: {
            onTouch: true
        },
        scroll : {
            fx              : "crossfade",
            items           : 1,
            easing          : "linear",
            duration        : 1000,
            pauseOnHover    : false
        },
        pagination: {
            container : "#pager4"
        },
        auto: {
            play : true
        },
        items: {
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });
  }


//  -------- CARROSEUL PLANTAS -------- //
  if($("#foo51").length){
    var foo51 = $('#foo51');
    $('#foo51').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: 260,
        prev: '#prev51',
        next: '#next51',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager51",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

  if($("#foo52").length){
    var foo52 = $('#foo52');
    $('#foo52').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev52',
        next: '#next52',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager52",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

  if($("#foo53").length){
    var foo53 = $('#foo53');
    $('#foo53').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev53',
        next: '#next53',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager53",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

  if($("#foo54").length){
    var foo54 = $('#foo54');
    $('#foo54').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev54',
        next: '#next54',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
      scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager54",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

  if($("#foo55").length){
    var foo55 = $('#foo55');
    $('#foo55').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev55',
        next: '#next55',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager55",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

  if($("#foo56").length){
    var foo56 = $('#foo56');
    $('#foo56').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev56',
        next: '#next56',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager56",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 3
            }
        }
    });
  }

//  -------- CARROSEUL OBRA -------- //
  if($("#foo61").length){
    var foo61 = $('#foo61');
    $('#foo61').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev61',
        next: '#next61',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager61",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }

  if($("#foo62").length){
    var foo62 = $('#foo62');
    $('#foo62').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev62',
        next: '#next62',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager62",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }

  if($("#foo63").length){
    var foo63 = $('#foo63');
    $('#foo63').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev63',
        next: '#next63',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager63",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }

  if($("#foo64").length){
    var foo64 = $('#foo64');
    $('#foo64').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev64',
        next: '#next64',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager64",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }

  if($("#foo65").length){
    var foo65 = $('#foo65');
    $('#foo65').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev65',
        next: '#next65',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager65",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }

  if($("#foo66").length){
    var foo66 = $('#foo66');
    $('#foo66').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev66',
        next: '#next66',
        mousewheel: false,
          swipe: {
             onTouch: true
          },
        scroll : {
            easing          : "quadratic",
            duration        : 2000,
            pauseOnHover    : false
        },

        pagination: "#pager66",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 10
            }
        }
    });
  }


  //  -------- Backgorund Size IE -------- //
  $(document.body).css({backgroundSize: "cover"});
  $(".scales").css({backgroundSize: "cover"});
});


// pode deletar abaixo (teste de cookie)

var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
      console.log(decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    // return aKeys;
    console.log(':::: COOKIES ::::');
    console.log(aKeys);
    console.log('::: END COOKIES :::');
  }
};

// docCookies.keys();
// docCookies.getItem('__atuvc');

// console.log(':::: COOKIES ::::');
// console.log(decodeURIComponent(document.cookie));
// console.log(HTMLDocument.cookie);
// console.log('::: END COOKIES :::');

    // SISTEMA DE ABAS

    $("#MostraAba1").click(function() {

        $("#MostraAba2").removeClass('active');
        $("#MostraAba3").removeClass('active');
        $("#MostraAba1").addClass('active');

        $("#Aba2").hide();
        $("#Aba3").hide();
        $("#Aba1").show();

    });

    $("#MostraAba2").click(function() {

        $("#MostraAba1").removeClass('active');
        $("#MostraAba3").removeClass('active');
        $("#MostraAba2").addClass('active');

        $("#Aba1").hide();
        $("#Aba3").hide();
        $("#Aba2").show();

    });

    $("#MostraAba3").click(function() {

        $("#MostraAba1").removeClass('active');
        $("#MostraAba2").removeClass('active');
        $("#MostraAba3").addClass('active');

        $("#Aba1").hide();
        $("#Aba2").hide();
        $("#Aba3").show();

    });