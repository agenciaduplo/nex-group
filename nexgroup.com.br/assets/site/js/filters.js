$(document).ready(function(){

	$("#filtro-estado").change(function(){
		$("option", $("#filtro-cidade")).eq(0).prop('selected', true);

		$(".cidades", $("#filtro-cidade")).hide();

		if(this.value != '-')
			$("option", $("#filtro-cidade")).eq(0).text('Filtrar por Cidade:');
		else
			$("option", $("#filtro-cidade")).eq(0).text('Filtrar por Cidade: Selecione um estado.');

		$(".uf-"+this.value, $("#filtro-cidade")).show();

		filtrarHomeCard();
		filtrarHomeLista();
	});


	updateFiltersByStatus(status_id); // Var status_id  Location: views/home/home.php  Default: 1

	$("#aba-filtro-status > li > a").click(function(){
		
		updateFiltersByStatus($(this).attr('data-status'));
		
	});

});


function updateFiltersByStatus(status)
{
	$("option", $("#filtro-estado")).eq(0).prop('selected', true);
	$("option", $("#filtro-cidade")).eq(0).prop('selected', true);
	$("option", $("#filtro-tipo")).eq(0).prop('selected', true);

	$.ajax({
		type: "POST",
		url: BASE_URL+"filters/atualizaFiltrosXHR",
		data: {
			status: status
		},
		dataType: "json",
		success: function(response)
		{

			$("#filtro-estado").load(BASE_URL+"filters/LoadSelectEstadosXHR", {estados: response.estados});
			$("#filtro-cidade").load(BASE_URL+"filters/LoadSelectCidadesXHR", {cidades: response.cidades});
			$("#filtro-tipo").load(BASE_URL+"filters/LoadSelectTiposXHR",     {tipos:   response.tipos});

			filtrarHomeCard();
			filtrarHomeLista();
		}
	});
}