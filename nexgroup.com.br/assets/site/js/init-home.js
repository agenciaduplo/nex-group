$(document).ready(function(){
	
	$('a, img').on('focus', function(){ return false; });
	
	$(".mask-telefone").mask("(99) 9999-9999?9");
	
	$('.tt').tooltipster({theme:'tooltipster-borderless'});

	$("#destaque ul li.box").show();

	var navigations = $('#ancora');
	pos = navigations.offset();

	$(window).scroll(function(){
		if($('.header-fixo.parado').length == 0){
			if($(this).scrollTop() > pos.top + navigations.height()){
				$('.header-fixo').removeClass('absoluto');
				$('.header-fixo').addClass('fixo animated fadeInDown');
			}else if ($(this).scrollTop() <= pos.top){
				$('.header-fixo').addClass('absoluto');
				$('.header-fixo').removeClass('fixo animated fadeInDown');
			}
		}
	});

	$(".bloco").height($(window).height());  
	$(window).resize(function(){
		$(".bloco").height($(window).height());
		filtrarHomeCard();
	});

	toogleModal(".click_fone","#area_fone");
	toogleModal(".click_contato_cliente","#area_contato_cliente");
	toogleModal(".click_mapa","#area_mapa");
	toogleModal(".click_revista","#area_revista");

	$(".first").click(function(){
		var displayMenu = $("#menu").css('display');

		if(displayMenu == "none"){
			$(".first").addClass('active');

			$("#menu").animate({
				width: "show", opacity: "toggle"
			}, {duration: "fast"});
		}else{
			$(".first").removeClass('active');

			$("#menu").animate({
				width: "hide", opacity: "toggle"
			}, {duration: "fast"});
		}
	});


	// CARROSEUL DESTAQUE
	var foo = $('#foo');
	$('#foo').carouFredSel({
		circular: true,
		infinite: true,
		responsive: true,
		width: null,
		height: null,
		prev: '#prev',
		next: '#next',
		mousewheel: false,
		scroll : {
			fx              : "crossfade",
			items           : 1,
			easing          : "linear",
			duration        : 2000,
			pauseOnHover    : false
		},
		pagination: "#pager",
		auto: true,
		items: {
			width: 200,
			height: null,
			visible: {
				min: 1,
				max: 1
			}
		},
		onCreate: function () {
			$(window).on('resize', function () {
				$('#foo').parent().height($('#foo').children().first().height());
				console.log('turned');
			});
		}
	});
	swipeElement("#foo");

	// IMOVEIS QUE VOCÊ JÁ VU
	if($("#foo10").length){
		var foo10 = $('#foo10');
		$('#foo10').carouFredSel({
			circular: false,
			responsive: true,
			width: null,
			height: null,
			prev: '#prev10',
			next: '#next10',
			mousewheel: false,
			scroll : {
				easing          : "quadratic",
				duration        : 2000,
				pauseOnHover    : false
			},
			pagination: "#pager10",
			auto: true,
			items: {
				height: null,
				visible: {
					min: 1,
					max: 5
				}
			}
		});
	}
	swipeElement("#foo10");

	// IMOVEIS FAVORITOS
	if($("#foo11").length){
		var foo11 = $('#foo11');
		$('#foo11').carouFredSel({
			circular: false,
			responsive: true,
			width: null,
			height: null,
			prev: '#prev11',
			next: '#next11',
			mousewheel: false,
			scroll : {
				easing          : "quadratic",
				duration        : 2000,
				pauseOnHover    : false
			},
			pagination: "#pager11",
			auto: true,
			items: {
				height: null,
				visible: {
					min: 1,
					max: 5
				}
			}
		});
	}
	swipeElement("#foo11");

	// Imóveis vistos
	if($("#foo12").length){
		var foo12 = $('#foo12');
		$('#foo12').carouFredSel({
			circular: false,
			responsive: true,
			width: null,
			height: null,
			prev: '#prev12',
			next: '#next12',
			mousewheel: false,
			scroll : {
				easing          : "quadratic",
				duration        : 2000,
				pauseOnHover    : false
			},
			pagination: "#pager12",
			auto: false,
			items: {
				height: null,
				visible: {
					min: 1,
					max: 5
				}
			}
		});
	}
	swipeElement("#foo12");

	// CARROSEUL CARDS
	if($("#foo21").length){
		var foo21 = $('#foo21');
		$('#foo21').carouFredSel({
			circular: true,
			infinite: true,
			responsive: true,
			width: null,
			height: null,
			prev: '#prev21',
			next: '#next21',
			mousewheel: false,
			scroll : {
				easing          : "quadratic",
				duration        : 2000,
				pauseOnHover    : false
			},
			pagination: "#pager21",
			auto: true,
			items: {
				width: 200,
				height: null,
				visible: {
					min: 1,
					max: 1
				}
			}
		});
	}
	swipeElement("#foo21");

	// CARROSEUL NOTICIAS
	var foo3 = $('#foo3');
	$('#foo3').carouFredSel({
		circular: true,
		infinite: true,
		responsive: true,
		width: null,
		height: null,
		prev: '#prev3',
		next: '#next3',
		mousewheel: false,
		scroll : {
			easing          : "quadratic",
			duration        : 2000,
			pauseOnHover    : false
		},
		pagination: "#pager3",
		auto: false,
		items: {
			height: null,
			visible: {
				min: 1,
				max: 5
			}
		}
	});
	swipeElement("#foo3");

	$(document.body).css({backgroundSize: "cover"});
	$(".scales").css({backgroundSize: "cover"});
});

function swipeElement(element){
	$(element).swipe({
		excludedElements: "button, input, select, textarea, a, .noSwipe",
		swipeLeft: function() {$(element).trigger('next', true);},
		swipeRight: function() {$(element).trigger('prev', true);},
		tap: function(event, target) {$(target).parent().trigger('click');}
	});
}

function toogleModal(eclick, etarget){
	$(eclick).click(function(){
		var displayMenu = $(etarget).css('display');
		if(displayMenu == "none"){
			$(etarget).animate({height: "show", opacity: "toggle"},{duration: "fast"});
		}else{
			$(etarget).animate({height: "hide", opacity: "toggle"},{duration: "fast"});
		}
	});
}