var geocoder;
var map;
var markersArray = [];
var infoboxArray = [];
//var zoom = 5;
var portoAlegre = new google.maps.LatLng(-30.030015, -51.217575);
var sv = new google.maps.StreetViewService();
var panorama;
var latLng;

var service;
var infowindow;


iniciarGoogleMapsHome = function() {
    var mapOptions = {
        zoom: 12,
		maxZoom: 19,
		minZoom:12,
		scrollwheel: false, //desabilita o scroll
        center: portoAlegre,
		keyboardShortcuts:false,
        mapTypeControl: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        },
		disableDefaultUI: false,
		navigationControl: false,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN,
			position: google.maps.ControlPosition.LEFT_TOP
		},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    
    filtrarHomeMapa();
}


function mapaNex() 
{	
	var myLatlng = new google.maps.LatLng(-30.0276268,-51.18274460000001);

	var mapOptions = {
		zoom: 10,
		maxZoom: 19,
		minZoom: 12,
		// scrollwheel: false, 
		center: portoAlegre,
		keyboardShortcuts: false,
		mapTypeControl: false,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP]
		},
		disableDefaultUI: false,
		navigationControl: false,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN,
			position: google.maps.ControlPosition.LEFT_TOP
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById("map_nex"), mapOptions);

	var marker = new google.maps.Marker({
		position: myLatlng, 
		map: map, 
		title: "Nex Group"
	});
}





var countMarkerId = 0; 
criaMarkers = function (id, lat, lon, icon, title, url){

	// console.log('criaMarkers() started');
	// console.log('var id =>' + id);
	// console.log('var lat =>' + lat);
	// console.log('var lon =>' + lon);
	// console.log('var icon =>' + icon);
	// console.log('var title =>' + title);
	// console.log('var url =>' + url);

	// console.log('-----------------------------------');

	if(url == undefined)
		url = 'javascript:void(0);';

	var marker = new google.maps.Marker;
	if(icon.length > 0 && icon != 'false'){

		// console.log('Entrou no IF');

		marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lon),
			map: map,
			icon: icon,
			countMarkerId: countMarkerId,
			url: url
		});
	}else{

		// console.log('Entrou no ELSE');

		marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lon),
			map: map,
			url: url
		});		
	}
	
	google.maps.event.addListener(marker, 'click', function() {
        window.location.href = this.url;
    });
	
	markersArray[countMarkerId] = marker;
	criaInfoWindow(countMarkerId, title, marker);
	countMarkerId++;
}

removerTodosMarkers = function(){
	if (markersArray) {
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
	}
}

removerMarkers = function(){
	// console.log('removerMarkers()');
	
	if (markersArray) {
		for (i in markersArray) {
			// console.log('removendo marker ' + i);
			if(i > 0){
				markersArray[i].setMap(null);
			}
		}
	}		
} 


iniciarGoogleMaps = function(lat, lon, marker, controlador) {
    var mapOptions = {
        zoom: 15,
		maxZoom: 19,
		minZoom:12,
		scrollwheel: false, //desabilita o scroll
        center: new google.maps.LatLng(lat, lon),
		keyboardShortcuts:false,
        mapTypeControl: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        },
		disableDefaultUI:false,
		navigationControl: controlador,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN,
			position: google.maps.ControlPosition.LEFT_TOP
		},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    
    criaMarkers(0, lat, lon, marker, '');
    adicionarRemoverMarkersByTipo($('#ul-marker li').eq(0).find('a').attr('data-id'));
}



adicionarRemoverMarkersByTipo = function(tipo, icon){

	// console.log('adicionarRemoverMarkersByTipo() started');

	// console.log('var icon => '+icon);

    var request = {
      location: new google.maps.LatLng($('#map_canvas').attr('lat'), $('#map_canvas').attr('lon')),
      radius: '1000',
      types: [tipo]
    };

    service = new google.maps.places.PlacesService(map);

    service.search(request, function(results, status){

    	// console.log('service.search() callback');

    	if (status == google.maps.places.PlacesServiceStatus.OK) {

    		// console.log('service OK');

		    for (var i = 0; i < results.length; i++)
		    {
		      var place = results[i];
		      
		      // console.log(place);
		      // console.log('------------------------------------------------')
		      // console.log('New Place');
		      // console.log('id: ' + place['id']);

		      // console.log('location lat: ' + place['geometry']['location']['A']);
		      // console.log('location lon: ' + place['geometry']['location']['F']);

	      	//   console.log('location lat 2: ' + place.geometry.location.lat());
		      // console.log('location lon 2: ' + place.geometry.location.lng());

		      // console.log('location array');
		      // console.log(place['geometry']['location']);
		      // console.log('icon: ' + icon);
		      // console.log('name: ' + place['name']);

		      criaMarkers(place['id'], place.geometry.location.lat(), place.geometry.location.lng(), icon, place['name']);
		      
		    }
		    // console.log('total: ' + i);
    	}
	});
}

getTotalMarkersByTipo = function(tipo)
{
	var request = {
      location: new google.maps.LatLng($('#map_canvas').attr('lat'), $('#map_canvas').attr('lon')),
      radius: '1000',
      types: [tipo]
    };

    service = new google.maps.places.PlacesService(map);
    service.search(request, function(results, status){
    	if (status == google.maps.places.PlacesServiceStatus.OK) {
    		// console.log('_______total: ' + results.length);
		    retorno = results.length;
    	}
    	else
    	{
    		$('#ul-marker li').find('a[data-tipo="'+tipo+'"]').parent().remove();
    	}
	});  	
}

criaInfoWindow = function(id, title, marker){
	var contentString;
	
	if(title != ''){
		contentString = '<div class="info-window">'+title+'</div>';    	
	}
	  
	infoboxArray[id] = new InfoBox({
		content: contentString,
		disableAutoPan: false,
		maxWidth: 300,
		pixelOffset: new google.maps.Size(10, -40),
		zIndex: null,
		boxStyle: {
//			background: "url('../img/seta-bottom-google-maps.png') center bottom no-repeat",
			opacity: 1,
			width: "auto"
		},
//		closeBoxMargin: "8px 12px 2px 2px; z-index:5",
		closeBoxURL: "",
		infoBoxClearance: new google.maps.Size(1, 1)
	});	
	
	google.maps.event.addListener(marker, 'mouseover', function() {
		closeInfobox();
		infoboxArray[this.countMarkerId].open(map, marker);
	});
	
	google.maps.event.addListener(marker, 'mouseout', function() {
		closeInfobox();
	});
}

closeInfobox = function() {
    for (var i in infoboxArray) {
    	infoboxArray[i].close();
    }
}






$(function() {
	if($('#map_canvas').length){
		if($('#map_canvas').attr('lat') && $('#map_canvas').attr('lon')){
			var controlador = $('#map_canvas').attr('controlador');
			if(controlador != undefined && controlador == 'false'){
				controlador = false;
			}else{
				controlador = true;
			}
			iniciarGoogleMaps($('#map_canvas').attr('lat'), $('#map_canvas').attr('lon'), $('#map_canvas').attr('marker'), controlador);
		}
	}
});


function irParaLocalizacao(de){
	var lat = $('#latitude').val();
	var lon = $('#longitude').val();
	var para = lat+','+lon;
	
	window.open('https://www.google.com/maps/dir/'+de+'/'+para+'/', 'google-localizacao');
}
	
function pegarGeoLocalizacao(){
	if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(success, error);
	} else {
	    error('Seu navegador não suporta <b style="color:black;background-color:#ffff66">Geolocalização</b>!');
	}
}

function success(position) {
	var lat = $('#latitude').val();
	var lon = $('#longitude').val();
	var de = position.coords.latitude+','+position.coords.longitude;
	var para = lat+','+lon;
	
	window.open('https://www.google.com/maps/dir/'+de+'/'+para+'/');
}

function error(msg) {
    var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "falhou";
        s.className = 'fail';
}