
$(document).ready(function() {


//  -------- ROLAGEM NO CLICK -------- //
  $('.clickroll').localScroll({duration: 700, queue: true});


//  -------- HEADER FIXO -------- //

    var navigations = $('#ancora');
    position_header = navigations.offset();

    $(window).scroll(function() {
        if ($(this).scrollTop() > position_header.top + navigations.height()) {
            $('.header-fixo').removeClass('absoluto');
            $('.header-fixo').addClass('fixo fadeInDown animated_a');
        } else if ($(this).scrollTop() <= position_header.top) {
            $('.header-fixo').addClass('absoluto');
            $('.header-fixo').removeClass('fixo fadeInDown animated_a');
        }
    });







//  -------- SCROLL SUAVE -------- //

      if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
      window.onmousewheel = document.onmousewheel = wheel;

      var time = 0;
      var distance = 25;

      function wheel(event) {
          if (event.wheelDelta) delta = event.wheelDelta / 120;
          else if (event.detail) delta = -event.detail / 3;

          handle();
          if (event.preventDefault) event.preventDefault();
          event.returnValue = false;
      }

      function handle() {

          $('html, body').stop().animate({
              scrollTop: $(window).scrollTop() - (distance * delta)
          }, time);
      }


      $(document).keydown(function (e) {

          switch (e.which) {
              //up
              case 38:
                  $('html, body').stop().animate({
                      scrollTop: $(window).scrollTop() - distance
                  }, time);
                  break;

                  //down
              case 40:
                  $('html, body').stop().animate({
                      scrollTop: $(window).scrollTop() + distance
                  }, time);
                  break;
          }
      });


//  -------- ALTURA DA TELA -------- //

       var altura_tela = $(window).height();/*cria variável com valor do altura da janela*/   

       $(".bloco").height(altura_tela); /* aplica a variável a altura da div*/  
       $(window).resize(function() { /*quando redimensionar a janela faz a mesma coisa */  

        var altura_tela = $(window).height();
         $(".bloco").height(altura_tela);
       });

    $(".first").click(function() {
        var displayMenu = $("#menu").css('display');

        if (displayMenu == "none")
        {
            $(".first").addClass('active');

            $("#menu").animate({
                width: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $(".first").removeClass('active');

            $("#menu").animate({
                width: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });



//  -------- PARALLAX -------- //
  var width = $(window).width();

  if(width >= 900){
    $('.paral').parallax({speed: -0.9});
  }


//  -------- MODAL -------- //

    $(".click_fone").click(function() {

        var displayMenu = $("#area_fone").css('display');

        if (displayMenu == "none")
        {
            $("#area_fone").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#area_fone").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });

    $(".click_contato").click(function() {

        var displayMenu = $("#area_contato").css('display');

        if (displayMenu == "none")
        {
            $("#area_contato").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#area_contato").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });


    $(".click_inicio").click(function() {

        var displayMenu = $("#area_inicio").css('display');

        if (displayMenu == "none")
        {
            $("#area_inicio").animate({
                height: "show", opacity: "toggle"
            }, {duration: "fast"});
        }
        else
        {
            $("#area_inicio").animate({
                height: "hide", opacity: "toggle"
            }, {duration: "fast"});
        }
    });







//  -------- CARROSEUL GALERIA EMPREENDIMENTOS -------- //

    var foo4 = $('#foo4');
    $('#foo4').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev4',
        next: '#next4',
        mousewheel: false,
        swipe: {
            onTouch: true
        },
        scroll : {
            fx              : "cover-fade",
            items           : 1,
            easing          : "linear",
            duration        : 1000,                         
            pauseOnHover    : false
        },
        pagination: "#pager4",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });



    var foo5 = $('#foo5');
    $('#foo5').carouFredSel({
        circular: false,
        responsive: true,
        width: null,
        height: null,
        prev: '#prev5',
        next: '#next5',
        mousewheel: false,
        swipe: {
            onTouch: true
        },
        scroll : {
            fx              : "cover-fade",
            items           : 1,
            easing          : "linear",
            duration        : 1000,                         
            pauseOnHover    : false
        },
        pagination: "#pager5",
        auto: true,
        items: {
            height: null,
            visible: {
                min: 1,
                max: 1
            }
        }
    });




//  -------- Backgorund Size IE -------- //

    $(document.body).css({backgroundSize: "cover"});
    $(".scales").css({backgroundSize: "cover"});




//  -------- PLACEHOLDER -------- //

    $('[placeholder]').focus(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function() {
      var input = $(this);
      if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur().parents('form').submit(function() {
      $(this).find('[placeholder]').each(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
        }
      })
    });



});

