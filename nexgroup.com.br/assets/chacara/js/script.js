$(".fancybox").fancybox();

$('.telefone').mask('(00) 000000000');

$('.toggle-chat').click(function(){
	$('.chatbox iframe').slideToggle();
	
	if($(this).find('span').text() == '- FECHAR'){
		$(this).find('span').text('+ ABRIR');
	} else {
		$(this).find('span').text('- FECHAR');
	}
});

$('#formContact').validate({
	errorClass: 'error',
	rules: {
		nome: {
			required: true,
			minlength: 2
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail."
		}
	},
	errorPlacement: function(error, element) {
		error.insertBefore( element );
		$('.error-messages').html(error);
	},
	submitHandler: function(form) {
		
		//Desabilita o form enquanto estiver enviando
		$('#formContact').addClass('locked');
		$('#formContact #btn-enviar').attr('value', 'Enviando...');
		$('#formContact #btn-enviar').attr('disabled', 'disabled');

		//Envia o form via ajax
		$(form).ajaxSubmit({
			type:"POST",
			data: $(form).serialize(),
			url:base_url+"chacara-das-nascentes/envia",
			success: function(data) {
				console.log(data);
				//Abrir iframe com msg de sucesso e tags de conversao
				$.fancybox.open({
					padding : 0,
					openEffect  : 'none',
					closeEffect : 'none',
					href : base_url+'chacara-das-nascentes/response',
					type: 'iframe',
					//aspectRatio: true,
					width: 519,
					maxHeight: 312
				});
				
				//Habilita o form novamente e limpa
				$('#formContact').removeClass('locked');
				$('#formContact #btn-enviar').attr('value', 'ENVIAR');
				$('#formContact #btn-enviar').removeAttr('disabled');
				$('#formContact')[0].reset();
				
				//Meta GA
				if($('#is_mobile').css('display') == 'none'){
					console.log('not mobile');
					ga('send','event','form','envio','desktop');
				} else {
					console.log('is mobile');
					ga('send','event','form','envio','mobile');
				} 
				
				//fbq('track', 'Lead');
			},
			error: function() {
			  
			}
		});
	}
});

var control = true;	

$(window).scroll(function(){
	
	//element = $('#video'); 
	
	var $elem = $('.info-links');
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	
	//console.log((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	if(elemBottom <= docViewBottom && control){
		console.log('Aqui vai um tag GA');
		ga('send','event','pagina','rolagem');
		control = false;
	}
	
	if(docViewTop < 60) {
		
		$('.label-mude-agora').fadeIn();
		
	} else {
		$('.label-mude-agora').hide();
	}
});