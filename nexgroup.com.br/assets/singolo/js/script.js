$(".fancybox").fancybox();

$('.telefone').mask('(00) 000000000');

$('.toggle-chat').click(function(){
	$('.chatbox iframe').slideToggle();
	
	if($(this).find('span').text() == '- FECHAR'){
		$(this).find('span').text('+ ABRIR');
	} else {
		$(this).find('span').text('- FECHAR');
	}
});

$('#formContact').validate({
	errorClass: 'error',
	rules: {
		nome: {
			required: true,
			minlength: 2
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail."
		}
	},
	errorPlacement: function(error, element) {
		error.insertBefore( element );
		$('.error-messages').html(error);
	},
	submitHandler: function(form) {
		
		//Desabilita o form enquanto estiver enviando
		$('#formContact').addClass('locked');
		$('#formContact #btn-enviar').attr('value', 'Enviando...');
		$('#formContact #btn-enviar').attr('disabled', 'disabled');

		//Envia o form via ajax
		$(form).ajaxSubmit({
			type:"POST",
			data: $(form).serialize(),
			url:base_url+"singolo/envia",
			success: function(data) {
				console.log(data);
				//Abrir iframe com msg de sucesso e tags de conversao
				$.fancybox.open({
					padding : 0,
					openEffect  : 'none',
					closeEffect : 'none',
					href : base_url+'singolo/response',
					type: 'iframe',
					//aspectRatio: true,
					width: 519,
					maxHeight: 312
				});
				
				//Habilita o form novamente e limpa
				$('#formContact').removeClass('locked');
				$('#formContact #btn-enviar').attr('value', 'ENVIAR');
				$('#formContact #btn-enviar').removeAttr('disabled');
				$('#formContact')[0].reset();
				
				//Meta GA
				if($('#is_mobile').css('display') == 'none'){
					console.log('not mobile');
					ga('send','event','form','envio','desktop');
				} else {
					console.log('is mobile');
					ga('send','event','form','envio','mobile');
				} 
				
				//fbq('track', 'Lead');
			},
			error: function() {
			  
			}
		});
	}
});

$('#fixedFormContact').validate({
	errorClass: 'error',
	rules: {
		nome: {
			required: true,
			minlength: 2
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail."
		}
	},
	errorPlacement: function(error, element) {
		error.insertBefore( element );
		$('.error-messages').html(error);
	},
	submitHandler: function(form) {
		
		//Desabilita o form enquanto estiver enviando
		$('#fixedFormContact').addClass('locked');
		$('#fixedFormContact #btn-enviar').attr('value', 'Enviando...');
		$('#fixedFormContact #btn-enviar').attr('disabled', 'disabled');

		//Envia o form via ajax
		$(form).ajaxSubmit({
			type:"POST",
			data: $(form).serialize(),
			url:base_url+"singolo/envia",
			success: function(data) {
				console.log(data);
				//Abrir iframe com msg de sucesso e tags de conversao
				$.fancybox.open({
					padding : 0,
					openEffect  : 'none',
					closeEffect : 'none',
					href : base_url+'singolo/response',
					type: 'iframe',
					//aspectRatio: true,
					width: 519,
					maxHeight: 312
				});
				
				//Habilita o form novamente e limpa
				$('#fixedFormContact').removeClass('locked');
				$('#fixedFormContact #btn-enviar').attr('value', 'ENVIAR');
				$('#fixedFormContact #btn-enviar').removeAttr('disabled');
				$('#fixedFormContact')[0].reset();
				
				//Meta GA
				if($('#is_mobile').css('display') == 'none'){
					console.log('not mobile');
					ga('send','event','form','envio','desktop/fixo');
				} else {
					console.log('is mobile');
					ga('send','event','form','envio','mobile/fixo');
				} 
				
				//fbq('track', 'Lead');
			},
			error: function() {
			  
			}
		});
	}
});

var control = true;
var controlForm = true;	
var inputs = $('.inputs');
var ititle = $('.inputs-title');
var fHeight = inputs.height();
var fixedForm = $('#fixedFormContact');

ititle.click(function(){

	if(inputs.height() <= 0){
		if(fHeight > 0){
			inputs.animate({
				height:fHeight
			});
		} else {
			inputs.animate({
				height:262
			});
			$('.label-destaque').hide();
		}

		fixedForm.css({background:'#acc2c7'});
		ititle.css({cursor:'inherit'});
		ga('send','event','form','abertura');
	} else {
		inputs.animate({
				height:0
			});
		fixedForm.css({background:'#CA2C04'});
	}
});

$(window).scroll(function(){
	
	
	var $elem = $('.info-links');
	var $elemForm = $('.form');
	
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	//LINK OFFSET
	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	//FORM OFFSET
	var elemFormTop = $elemForm.offset().top;
	var elemFormBottom = elemFormTop + $elemForm.height();
	
	//FIXED FORM OFFSET
	var fixedFormTop = fixedForm.offset().top;
	var fixedFormBottom = fixedFormTop + fixedForm.height();
	
	
	$('.logo-singolo').hide();

	$('.label-destaque').hide();
	if(docViewTop > 60 && inputs.height() > 0){
		
		fixedForm.css({background:'#CA2C04'});
		ititle.css({cursor:'pointer'});
		
	} else if(docViewTop <= 60 && inputs.height() <= 0) {
		
		fixedForm.css({background:'#acc2c7'});
		ititle.css({cursor:'inherit'});
	}

	if(docViewTop < 60) {
		$('.logo-singolo').fadeIn();
		$('.label-mude-agora').fadeIn();
		
	} else {
		$('.logo-singolo').hide();
		$('.label-mude-agora').hide();
	}


	
	if(elemBottom <= docViewBottom && control){
		console.log('Aqui vai um tag GA');
		ga('send','event','pagina','rolagem');
		control = false;
	}
	
	if(elemFormBottom <= docViewBottom && controlForm){
		console.log('Aqui vai um tag GA FORM');
		ga('send','event','section_form','rolagem_form');
		controlForm = false;
	}
});