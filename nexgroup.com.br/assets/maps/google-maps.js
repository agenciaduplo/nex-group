/* Google Maps API V3 - Code by www.henrique.rs */

var http = 'http://www.localhost.com.br/';

/* Main Vars */

var gm_service,
	gm_map,
	gm_map_id,
	gm_zoom,
	gm_geocoder = new google.maps.Geocoder();
	gm_icons_url = http + 'uploads/imoveis/pin/',
	gm_boxstyle = [];

/* Places Vars */

var gm_icons = new Array(),
	gm_service = 'undefined',
	gm_placepos,
	gm_range = 500;
	gm_places_url = http + 'assets/maps/lugares/';

/* Directions Vars */

var gm_directions_id = 'googledirec',
	gm_directions_display,
	gm_directions_service = new google.maps.DirectionsService(),
	gm_directions_old = [],
	gm_current_directions = null,
	gm_directions_mode = 'DRIVING', // Modes: DRIVING, WALKING e BICYCLING
	gm_directions_nomarkers = true,
	gm_directions_draggable = false,
	gm_directions_icons = [];

/* Extra Vars */

var gm_myloc,
	gm_dot_url = http + 'assets/maps/dot/',
	gm_myicon = 'red',
	gm_dotcolors = ['blue-dot','green-dot','orange-dot','red-dot','yellow-dot'];

var gm = {

	/* Google Map */

	initialize: function(id,pos,zoom){
		gm_map_id = id;
		gm_zoom = zoom;
		gm_placepos = pos;
		var mapOptions = {
			zoom: zoom,
			center: pos,
			panControl: true,
			zoomControl: true,
			scaleControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			/* Custom */
			mapTypeControlOptions: {
            	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        	}
		};
		gm_map = new google.maps.Map(document.getElementById(id),mapOptions);
	},
	setbox: function(html,pos){
		var coordInfoWindow = new google.maps.InfoWindow();
		coordInfoWindow.setContent(html);
		coordInfoWindow.setPosition(pos);
		coordInfoWindow.open(gm_map);
		google.maps.event.addListener(gm_map, 'zoom_changed', function() {
			coordInfoWindow.setContent(html);
			coordInfoWindow.open(gm_map);
		});
	},
	addmarker: function(titulo,html,pos,open){
		var marker = new google.maps.Marker({
			map: gm_map,
			position: pos,
			visible: true,
			title: titulo
		});
		box = new google.maps.InfoWindow(gm_boxstyle);
		google.maps.event.addListener(marker, 'click', function() {
			box.setContent(html);
			box.open(gm_map, marker);
		});
	},
	addicon: function(titulo,html,img,pos){
		img = gm_icons_url + img;
		var marker = new google.maps.Marker({
			map: gm_map,
			position: pos,
			icon: img,
			title: titulo
		});
		if(html){
			box = new google.maps.InfoWindow(gm_boxstyle);
			google.maps.event.addListener(marker, 'click', function() {
				box.setContent(html);
				box.open(gm_map, marker);
			});
		}
	},
	position: function(x,y){
		return new google.maps.LatLng(x,y);
	},
	getposition: function(address){
		gm_geocoder.geocode({'address': address},function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				return results[0].geometry.location;
			} else {
				return false;
			}
		});
	},
	setcenter: function(pos){
		gm_map.setCenter(pos,gm_zoom);
	},


	/* Google Places */


	addplaces: function(place){
		var tipo = place.types[0];
		img = gm_places_url + tipo + '.png';
		var marker = new google.maps.Marker({
			map: gm_map,
			position: place.geometry.location,
			icon: img,
			shadow: gm_places_url + 'shadow.png',
			title: place.name
		});
		marker.setAnimation(google.maps.Animation.DROP);
		gm_icons.push(marker);
	},
	addrange: function(type){
		if(gm_service == 'undefined'){
			gm_service = new google.maps.places.PlacesService(gm_map);
		}
		if(type){
			var request = {
				location: gm_placepos,
				radius: gm_range,
				types: type
			};
			gm_service.search(request,gm.placescallback);
		}
	},
	clear: function(){
		if(gm_icons.length > 0){
			for(i in gm_icons){
				gm_icons[i].setMap(null);
			}
			gm_icons.length = 0;
		}
	},
	hide: function(){
		if(gm_icons.length > 0){
			for(i in gm_icons){
				gm_icons[i].setMap(null);
			}
		}
	},
	show: function(){
		if(gm_icons.length > 0){
			for(i in gm_icons){
				gm_icons[i].setMap(gm_map);
			}
		}
	},
	toggle: function(type){
		if(gm_icons.length > 0){
			gm.clear();
		}
		gm.addrange(type);
	},
	placescallback: function(results, status) {
		if(status == google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				gm.addplaces(results[i]);
			}
		}
	},


	/* Google Directions */

	directions: function(panel){
		gm_directions_display = new google.maps.DirectionsRenderer({
			'map': gm_map,
			'preserveViewport': false,
			'draggable': gm_directions_draggable,
			'suppressMarkers': gm_directions_nomarkers
		});
		if(panel){
			gm_directions_id = panel;
			gm_directions_display.setPanel(document.getElementById(gm_directions_id));
		}
		google.maps.event.addListener(gm_directions_display, 'directions_changed',function() {
			if(gm_current_directions){
				gm_directions_old.push(gm_current_directions);
			}
			gm_current_directions = gm_directions_display.getDirections();
		});
	},
	change: function(start,end){
		var request = {
			origin: start,
			destination: end,
			travelMode: google.maps.DirectionsTravelMode[gm_directions_mode]
		};
		gm_directions_service.route(request, function(response,status){
			if(status == google.maps.DirectionsStatus.OK){
				gm_directions_display.setDirections(response);
				/* Custom */
				gm.createsteps(response);
			}
		});
	},
	driving: function(mode){
		gm_directions_mode = mode;
	},

	/* Autocomplete */
	
	autocomplete: function(id){
		var input = document.getElementById(id);
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', gm_map);
		autocomplete.setTypes([]);
	},

	/* Extras */

	mylocation: function(color,mypos){
		gm_myicon = color;
		gm_myloc = new google.maps.Marker({
			map: gm_map,
			position: mypos,
			icon: gm_dot_url + gm_myicon + '-dot.png',
			title: 'Voc\u00ea',
			shadow: gm_dot_url + 'shadow-dot.png'
		});
	},
	mylocationcange: function(pos){
		if(gm_myloc.length > 0){
			gm_myloc.setMap(null);
		}
		gm.setcenter(pos);
		gm_myloc = new google.maps.Marker({
			map: gm_map,
			position: pos,
			icon: gm_dot_url + gm_myicon + '-dot.png',
			title: 'Voc\u00ea',
			shadow: gm_dot_url + 'shadow-dot.png'
		});
	},

	/* Custom */
	createsteps: function(vetor){
		var rota = vetor.routes[0].legs[0];
		gm.cleardot();
		for(var i = 0; i < rota.steps.length; i++){
			if(i == 0){
				gm.adddot('Eu',rota.steps[i].start_point,'blue');
			}
		}
	},
	adddot: function(html,pos,cor){
		var marker = new google.maps.Marker({
			map: gm_map,
			position: pos,
			icon: gm_dot_url + cor + '-dot.png',
			shadow: gm_dot_url + 'shadow-dot.png',
			title: html
		});
		marker.setAnimation(google.maps.Animation.DROP);
		gm_directions_icons.push(marker);
	},
	cleardot: function(){
		if(gm_directions_icons.length > 0){
			for(i in gm_directions_icons){
				gm_directions_icons[i].setMap(null);
			}
			gm_directions_icons.length = 0;
		}
	},
	adddots: function(titulo,html,pos,cor){
		var marker = new google.maps.Marker({
			map: gm_map,
			position: pos,
			icon: gm_dot_url + cor + '-dot.png',
			shadow: gm_dot_url + 'shadow-dot.png',
			title: titulo
		});
		box = new google.maps.InfoWindow(gm_boxstyle);
		google.maps.event.addListener(marker, 'click', function() {
			box.setContent(html);
			box.open(gm_map, marker);
		});
	}
}

