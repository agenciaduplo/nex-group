

$(".fancybox").colorbox({current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});
$(".cards").colorbox({rel:'cards',current:'{current} de {total}', maxWidth:'95%', maxHeight:'95%'});

var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });

$('.telefone').mask('(00) 000000000');

$('#formContact').validate({
	errorClass: 'error',
	rules: {
		nome: {
			required: true,
			minlength: 2
		},
		email: {
			required: function(element) {
				
				if($('#telefone').val() == ''){
					return true;
				} else {
					return false;
				}
			}, 
			email: true
		},
		telefone: {
			required: function(element) {
				if($('#email').val() == ''){
					return true;
				} else {
					return false;
				}
			}
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		email: {
			required: "Por favor preencha seu e-mail."
		},
		telefone: {
			required: "Por favor preencha seu telefone."
		}
	},
	errorPlacement: function(error, element) {
		error.insertBefore( element );
		$('.error-messages').html(error);
	},
	submitHandler: function(form) {
		
		//Desabilita o form enquanto estiver enviando
		$('#formContact').addClass('locked');
		$('#formContact #btn-enviar').attr('value', 'Enviando...');
		$('#formContact #btn-enviar').attr('disabled', 'disabled');

		//Envia o form via ajax
		$(form).ajaxSubmit({
			type:"POST",
			data: $(form).serialize(),
			url:base_url+"amores-da-brava/envia",
			success: function(data) {
				//Abrir iframe com msg de sucesso e tags de conversao
				$.colorbox({arrowKey:false, iframe:true, href : base_url+'amoresdabrava/response',maxWidth:'95%', maxHeight:'95%', innerWidth:519, innerHeight:294});
				/*$.fancybox.open({
					padding : 0,
					openEffect  : 'none',
					closeEffect : 'none',
					href : base_url+'myurban/response',
					type: 'iframe',
					//aspectRatio: true,
					width: 519,
					maxHeight: 312
				});*/
				
				//Habilita o form novamente e limpa
				$('#formContact').removeClass('locked');
				$('#formContact #btn-enviar').attr('value', 'ENVIAR');
				$('#formContact #btn-enviar').removeAttr('disabled');
				$('#formContact')[0].reset();
				
				//Meta GA
				if($('#is_mobile').css('display') == 'none'){
					console.log('not mobile');
					ga('send','event','form','envio','desktop/fixo');
				} else {
					console.log('is mobile');
					ga('send','event','form','envio','mobile/fixo');
				}
			},
			error: function() {
			  
			}
		});
	}
});

var control = true;
var inputs = $('.inputs');
var fHeight = $('.logo-myurban').height();
var fixedForm = $('#formContact');
var controlToggle = true;

$('.click_contato').click(function(){

	$(this).fadeToggle();
	ga('send','event','form','abertura');
	
});

$('.toggle-chat').click(function(){

	$('.toggle-chat').toggleClass('opened');
	
	$(this).removeClass('bouncer');
	
	if($('.toggle-chat').hasClass('opened')){
		$('.chatbox .iframe').animate({ width:'340px' }, 500);
	} else {
	
		$('.chatbox .iframe').animate({ width:'0px' }, 500);
	}
});

	var close = true;

$(window).scroll(function(){
	
	if(close){
		
		$('.toggle-chat').removeClass('opened');
		$('.chatbox .iframe').animate({ width:'0px' }, 500);
		
		close = false;
	}

	
	var $elem = $('.chamada');
	
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	//LINK OFFSET
	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	//FIXED FORM OFFSET
	var fixedFormTop = fixedForm.offset().top;
	var fixedFormBottom = fixedFormTop + fixedForm.height();

	
	if(docViewTop < 84) {
		
		$('.label-mude-agora').fadeIn();
		
	} else {
		
		$('.label-mude-agora').fadeOut();
		
	}
	
	if($window.scrollTop() > 0){
		//FECHA O FORMULARIO
		$('.click_contato').removeClass('hide'); 
		$('.click_contato').fadeIn(); 
		$('.logo-myurban').css('height', 81);
		
		/*if(!($('#is_mobile').css('display') == 'none')){
			$('.main-header').css('background', 'rgba(255,255,255, 1)');
		} */
		
		//FECHA CHAT
		/*if($('.chatbox iframe').not(':focus') && controlToggle){
			
			$('.chatbox iframe').slideToggle();	
			if($('.toggle-chat').find('span').text() == '- FECHAR'){
				$('.toggle-chat').find('span').text('+ ABRIR');
			} else {
				$('.toggle-chat').find('span').text('- FECHAR');
			}
			controlToggle = false;
		}*/ 

	} else {
		//ABRE O FORMULARIO
		$('.logo-myurban').css('height', 'auto');
		
		$('.click_contato').fadeOut();
		$('.click_contato').addClass('hide'); 	

		/*if(!($('#is_mobile').css('display') == 'none')){
			$('.main-header').css('background', 'rgba(255,255,255, 0.4)');
		} */
	
	}
	
	//VISUALIZA ELEMENTO NA ROLAGEM
	if(elemBottom <= docViewBottom && control){
		ga('send','event','pagina','rolagem');
		control = false;
	}
	
});