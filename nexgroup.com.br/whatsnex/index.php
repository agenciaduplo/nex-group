﻿<!doctype html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex, nofollow">
<meta name="description" content="desc">
<meta name="keywords" content="whatsnex, salão do imóvel, Nex Group, imóveis, apartamentos, ofertas salão do imóvel, imóveis porto alegre, Ofertas imóveis porto alegre.">
<title>Whats Nex - Aproveite, só até 31/10.</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,800italic' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="css/style.css?ver=1.9" />
<link rel="shortcut icon" href="http://www.nexgroup.com.br/favicon.png" type="image/x-icon" />
<!-- GA -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69113303-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 972557761;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972557761/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php
if (!function_exists('getallheaders')) 
{ 
    function getallheaders() 
    { 
           $headers = ''; 
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       return $headers; 
    } 
} 
$headers = getallheaders();

if(isset($headers['Referer'])){
	$ref = $headers['Referer'];
} else {
	$ref = 'direto';
}

$req = explode('-', $_SERVER['REQUEST_URI']);

?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '918457114901692');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https:=//www.facebook.com/tr?id=918457114901692&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<header></header>
<section class="container chamadas"> 
	<div class="row">
		<div class="col-md-12">
			<p class="text-whatsnex sucesso">Devido ao sucesso<br />o WhatsNex foi prorrogado.</p>
			<p class="aproveite">APROVEITE!</p>
		</div>
	</div>
	<div class="row m20">
		<div class="col-md-6">
			<p class="text-whatsnex">Ligue para NEX VENDAS<br /> pelo n&uacute;mero</p>
			<p class="telefone">[<span>51</span>]3092.3124</p>
		</div>
		<div class="col-md-6">
			<p class="text-whatsnex">ou fale com nosso</p> 
			<p class="corretor-online"><a href="http://nex.hypnobox.com.br/atendimento/index.php?id_produto=50" class="btn-corretor-online"> CORRETOR ON-LINE </a></p>
			<!--<p class="corretor-online"><a href="http://nex.hypnobox.com.br/?controle=Atendimento&acao=index&produto=50" class="btn-corretor-online"> CORRETOR ON-LINE </a></p>-->
			<p class="text-whatsnex">para receber<br />ofertas imperd&iacute;veis.</p> 
		</div>
	</div>
</section>
<section class="container ofertas">
	<div class="row">
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-my-urban-life.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-my-urban.jpg">
			</div>
		</div>
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-saint-louis.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-saint-louis.jpg">
			</div>
		</div>
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-max-haus.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-max-haus.jpg">
			</div>
		</div>
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-singolo.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-singolo.jpg">
			</div>
		</div>
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-vergeis.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-vergeis.jpg">
			</div>
		</div>
		<div class="col-md-6 emp my-urban-life">
			<div class="col-md-12 price-tag">
				<img src="img/pricetag-new-life.png">
			</div>
			<div class="col-md-12 img">
				<img src="img/img-new-life.jpg">
			</div>
		</div>
		
	</div>
</section>
<section class="container">
	<div class="row cont-form-chamada">
		<div class="col-md-3"></div>
		<div class="col-md-6"><p class="text-whatsnex">Cadastre-se e entraremos em contato com as melhores ofertas</p></div>
		<div class="col-md-3"></div>
	</div>
	<div class="row cont-form">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<form id="contact">
				<div class="col-md-6"><input type="text" name="nome" placeholder="Nome" class="text-input"></div>
				<div class="col-md-6"><input type="text" name="telefone" placeholder="Telefone" class="text-input telefone"></div>
				<div class="col-md-12"><input type="text" name="email" placeholder="E-mail" class="text-input email"></div>
				<div class="col-md-12"><input type="submit" name="email" value="ENVIAR" class="submit-input"></div>
				<input name="referer" id="referer" type="hidden" value="<?php echo $ref; ?>">
			</form>
		</div>
		<div class="col-md-2"></div>
	</div>
</section>
<footer class="container"><img src="img/logo-footer.png" width="240" height="81"></footer>
<img class="prorrogado" src="img/prorrogado.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.mask.min.js"></script>
<script src="js/script.js?ver=1.8"></script>
</body>
</body>
</html>
