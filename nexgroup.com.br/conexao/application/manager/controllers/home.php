<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

	public function index($uf="",$cidade="",$tipo="",$lanc=""){
		$this->auth->check();
		$this->load->model('home_model',      'model');
		$this->load->model('enquetes_model',  'enquete');
		$termos = $this->model->verificaTermos();


    $arrCidades = $this->model->getIdCidadesAtivas();
    $tot_cid    = count($arrCidades);
    $id_cidades = "";
    for($i=0;$i<$tot_cid;++$i){
      $pontuacao    = ( ($i+1) == $tot_cid ) ? "" : ",";
      $id_cidades  .= $arrCidades[$i]->id_cidade.$pontuacao;
    }

    $arrEstados = $this->model->getIdEstadosAtivos($id_cidades);
    $idcidade = $this->input->post("fil_cidade");
    $idestado = $this->input->post("fil_estado");

		if(@$termos->termos == "S"){
			$data = array(
				'page'					        => "home",
				'emps'  				        => $this->model->getEmps($idcidade,$idestado),

				'enquete' 				      => $this->enquete->getEnquete($this->encrypt->decode($_SESSION['conexao']['nex_id_usuario'])),
				'atualizacoesArquivos'	=> $this->model->getUpdateArquivos(),
				'atualizacoesEmmps'		  => $this->model->getUpdateEmmps(),
				'comunicado'			      => $this->model->getUltimoComunicado(),
				'comunicadosHeader'	  	=> $this->model->getComunicadosHeader(),
				'ultimoLogin'			      => $this->model->getUltimoLogin(),
				'is_admin' 				      => $_SESSION['conexao']['nex_login_tipo'] == 'administrador' ? true : false,

        'arrEstados'            => $arrEstados,
        'arrCidades'            => $arrCidades,
        'idcidade'              => $idcidade,
        'idestado'              => $idestado
			);
			$this->load->view('home/home', $data);
		}else if(@$termos->termos == "N"){
			@$data = array(
				'termos'					=> "N"
			);
			$this->load->view('login/login', $data);
		}
	}

	public function promocao(){
		$this->auth->check();

		$this->load->model('home_model', 'model');
		$termos = $this->model->verificaTermos();
		
		if(@$termos->termos == "S")
		{
			$data = array (
				'page'					=> "home",
				'emps'  				=> $this->model->getEmps(),
				
				'atualizacoesArquivos'	=> $this->model->getUpdateArquivos(),
				'atualizacoesEmmps'		=> $this->model->getUpdateEmmps(),
				'comunicado'			=> $this->model->getUltimoComunicado(),
				'comunicadosHeader'		=> $this->model->getComunicadosHeader(),
				'ultimoLogin'			=> $this->model->getUltimoLogin()
			);
			
			$this->load->view('home/promocao.php', $data);
		}
		elseif(@$termos->termos == "N")
		{
			@$data = array (
				'termos'					=> "N"
			);
			$this->load->view('login/login', $data);
		}
	}
	
	public function vergeis ()
	{
		$this->auth->check();
		
		$this->load->model('home_model', 'model');
		$termos = $this->model->verificaTermos();
		
		if(@$termos->termos == "S")
		{
			$data = array (
				'page'					=> "home",
				'emps'  				=> $this->model->getEmps(),
				
				'atualizacoesArquivos'	=> $this->model->getUpdateArquivos(),
				'atualizacoesEmmps'		=> $this->model->getUpdateEmmps(),
				'comunicado'			=> $this->model->getUltimoComunicado(),
				'comunicadosHeader'		=> $this->model->getComunicadosHeader(),
				'ultimoLogin'			=> $this->model->getUltimoLogin()
			);
			$this->load->view('home/home_vergeis', $data);
		}
		elseif(@$termos->termos == "N")
		{
			@$data = array (
				'termos'					=> "N"
			);
			$this->load->view('login/login', $data);
		}
	}
	
	function atualizacoes_disponibilidades ($offset = "")
	{
		$this->auth->check();
		$this->load->model('home_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.nexgroup.com.br/conexao/home/atualizacoes_disponibilidades/',
			'total_rows'	=> $this->model->numUpdateDisponibilidades(),
			'per_page'		=> '10'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'cadastrados' 		=> $this->model->numUpdateDisponibilidades(),
    		'disponibilidades'	=> ($this->input->post('keyword')) ? $this->model->buscaUpdateDisponibilidades($this->input->post('keyword')) : $this->model->getUpdateDisponibilidades($offset),
			'paginacao'	  		=> $this->pagination->create_links(),
			'buscar'			=> $this->input->post('buscar'),
			'termo'				=> @$this->input->post('keyword'),
			'comunicadosHeader'	=> $this->model->getComunicadosHeader()
		);
		
		$this->load->view('home/atualizacoes.disponibilidades.php', $data);	
	}
	
	function atualizacoes_arquivos ($offset = "")
	{
		$this->auth->check();
		$this->load->model('home_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.nexgroup.com.br/conexao/home/atualizacoes_arquivos/',
			'total_rows'	=> $this->model->numUpdateArquivos(),
			'per_page'		=> '10'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'cadastrados' 		=> $this->model->numUpdateArquivos(),
    		'arquivos'			=> ($this->input->post('keyword')) ? $this->model->buscaUpdateArquivos($this->input->post('keyword')) : $this->model->getUpdateArquivosList($offset),
			'paginacao'	  		=> $this->pagination->create_links(),
			'buscar'			=> $this->input->post('buscar'),
			'termo'				=> @$this->input->post('keyword'),
			'comunicadosHeader'	=> $this->model->getComunicadosHeader()
		);
		
		$this->load->view('home/atualizacoes.arquivos.php', $data);	
	}
	
	function getAllFiles ()
	{
		$this->auth->check();
		
		$this->load->library('zip');
		$this->load->model('home_model', 'model');
		
		$arquivos 	= $this->model->buscaArquivosZip(2);
		$date 		= date("YmdHis");
		
		if($arquivos)
		{
			$cont = 0;
			foreach($arquivos as $row):

				$this->zip->read_file("./assets/manager/uploads/arquivos/$row->arquivo"); // Read the file's contents
			
			endforeach;
		}
		
		//$this->zip->add_dir("tabelas_de_precos-$date");
		$this->zip->download("Tabelas-$date.zip");
	}
	
	function update_ordem ($id_empreendimento = "")
	{
		$this->load->model('home_model', 'model');
		
		if($id_empreendimento)
		{
			$unidades = $this->model->buscaUnidades($id_empreendimento);
			
			//echo "<pre>";
				foreach($unidades as $row):
					
					$conta = strlen($row->apartamento);
					$ordem = "";
					
					if($conta == 3)
					{
						$ordem = substr($row->apartamento, 0, 1);
					}
					else if($conta == 4)
					{	
						$ordem = substr($row->apartamento, 0, 2);
					}
					//print_r($row);
					//echo "<br/>ordem: $ordem <br/><br/>";
					
					//$this->model->update_ordem($row->id_apartamento, $ordem);
			
				endforeach;
			//echo "</pre>";
			
		}
		else
		{
			echo "Não atualizado.";
			exit;	
		}
	}
	
	//===========================================================================================
	// FUNÇOES PROGRAMAÇÃO SEMANAL
	//===========================================================================================
	
	function programacao_periodo ($id_periodo = "")
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('usuarios_model', 'model');
			$this->load->model('home_model', 'home_model');
			
			if($id_periodo)
			{
				$data = array (
	    			'periodo'    	=> $this->home_model->getPeriodo($id_periodo)
	    		);
			}
			else
			{
				$data = array (
	    			
	    		);
			}
				
			
			
			$this->load->view('home/programacao.periodo.php', $data);
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function programacao_inserir ()
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('home_model', 'model');
				
			$data = array (
				'titleProg'				=> "Adicionar nova Programação",
	    		'periodos'    			=> $this->model->getPeriodos(),
	    		'tiposProgramacoes'		=> $this->model->getTiposProgramacoes(),
	    		'emps'  				=> $this->model->getEmpsProgramacao()
	    	);
			
			$this->load->view('home/programacao.inserir.php', $data);
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function programacao_editar ($id_programacao = "")
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('home_model', 'model');
				
			if($id_programacao):	
				
				$data = array (
					'titleProg'				=> "Editar Programação",
		    		'periodos'    			=> $this->model->getPeriodos(),
		    		'tiposProgramacoes'		=> $this->model->getTiposProgramacoes(),
		    		'emps'  				=> $this->model->getEmpsProgramacao(),
		    		'programacao'			=> $this->model->getProgramacao($id_programacao)
		    	);
				
				$this->load->view('home/programacao.inserir.php', $data);
			
			else:
				$this->auth->logout();
				redirect('login/');
			endif;
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	public function programacao ($id_periodo = "")
	{
		$this->auth->check();
		$this->load->model('home_model', 'model');
		$this->load->model('enquetes_model','enquete');
		$termos = $this->model->verificaTermos();
		
		if($id_periodo)
		{
			$programacao = $this->model->getUltimaProgramacaoPeriodo($id_periodo);
		}
		else
		{
			$programacao = $this->model->getUltimaProgramacao();
		}
		
		
		if(@$termos->termos == "S")
		{
			$data = array (
				'page'					=> "home",
				'emps'  				=> $this->model->getEmpsProgramacao(),
				
				'enquete' 				=> $this->enquete->getEnquete($this->encrypt->decode($_SESSION['conexao']['nex_id_usuario'])),
				'atualizacoesArquivos'	=> $this->model->getUpdateArquivos(),
				'atualizacoesEmmps'		=> $this->model->getUpdateEmmps(),
				'comunicado'			=> $this->model->getUltimoComunicado(),
				'comunicadosHeader'		=> $this->model->getComunicadosHeader(),
				'ultimoLogin'			=> $this->model->getUltimoLogin(),
				
				'periodos'				=> $this->model->getPeriodos(),
				'tiposProgramacoes'		=> $this->model->getTiposProgramacoes(),
				'periodo'				=> $programacao,
				
				
				
				'is_admin' 				=> $_SESSION['conexao']['nex_login_tipo'] == 'administrador' ? true : false
				
			);
			
			$this->load->view('home/programacao', $data);
		}
		elseif(@$termos->termos == "N")
		{
			@$data = array (
				'termos'					=> "N"
			);
			$this->load->view('login/login', $data);
		}

	}
	
	function buscarProgramacao ()
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('home_model', 'model');		
			
			$periodo 	= $this->input->post('periodo');
			$id 		= $this->input->post('id');
			$pasta 		= $this->input->post('pasta');
			
			if($pasta == "Pasta")
			{
				$folder = "P";
			}
			else
			{
				$folder = "E";
			}
			
			$programacoes = $this->model->getProgramacoes($periodo, $id, $folder);
			
			$dataInsert = array (
				'programacoes'			=> $programacoes
			);
			
			$this->load->view('home/programacao.ajax.php', $dataInsert);
				
		else:
			$this->auth->logout();
			redirect('login/');
		endif;		
	}
	
	function salvarProgramacao ()
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('usuarios_model', 'model');
			$this->load->model('home_model', 'home_model');	
			
			$emp 			= $this->input->post('empreendimento');
			$empSeparar 	= explode("-", $emp);
			
			if($empSeparar[1] == "Pasta")
			{
				$emp_or_pasta = "P";	
			}
			else
			{
				$emp_or_pasta = "E";
			}
			
			$id_emp_or_pasta = $empSeparar[0];
			
			$dataInsert = array (
				'id_periodo'			=> $this->input->post('periodos'),
				'id_tipo_programacao'	=> $this->input->post('tipo'),
				'id_emp_or_pasta'		=> $id_emp_or_pasta,
				'emp_or_pasta'			=> $emp_or_pasta,
				'data_cadastro'			=> date("Y-m-d H:i:s"),
				'titulo'				=> $this->input->post('titulo'),
				'datas'					=> $this->input->post('datas'),
				'formato'				=> $this->input->post('formato'),
				'determinacao'			=> $this->input->post('determinacao'),
				'local'					=> $this->input->post('local'),
				'horario'				=> $this->input->post('horario'),
				'pontos'				=> $this->input->post('pontos'),
				'veiculo'				=> $this->input->post('veiculo'),
				'observacoes'			=> $this->input->post('obs'),
				'ordem'					=> $this->input->post('ordem')
			);
			
			$id_programacao = $this->input->post('id_programacao');
			
			if($id_programacao):
			
				$this->home_model->setProgramacao($dataInsert, $id_programacao);
				
			else:
			
				$this->home_model->setProgramacao($dataInsert);
			
			endif;
			
			$this->load->view('home/programacao.inserir.ok.php');	
				
		else:
			$this->auth->logout();
			redirect('login/');
		endif;			
	}
	
	function salvarPeriodo ()
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('usuarios_model', 'model');
			$this->load->model('home_model','home_model');
			
			if($this->input->post('id_periodo'))
			{
				$dataInsert = array (
					'periodo'			=> $this->input->post('periodo'),
					'data_cadastro'		=> date("Y-m-d H:i:s")
				);
				
				$this->home_model->updatePeriodo($dataInsert, $this->input->post('id_periodo'));	
			}
			else
			{
				$dataInsert = array (
					'periodo'			=> $this->input->post('periodo'),
					'data_cadastro'		=> date("Y-m-d H:i:s")
				);
				
				$this->home_model->setPeriodo($dataInsert);				
			}
			
			$this->load->view('home/programacao.periodo.ok.php');
			

			
			/*	
			$data = array (
	    		'usuario'    	=> $this->model->getUser($id_usuario)
			);
			
			$this->load->view('home/programacao.inserir.php', $data);
		
			*/
		else:
			$this->auth->logout();
			redirect('login/');
		endif;	
	}
	
	function removerProgramacao ()
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			
			$this->load->model('home_model', 'model');
			
			$id_programacao = $this->input->post('id_programacao');
			
			try
			{		
				$imagem = $this->model->getProgramacao($id_programacao);
				if($imagem->arquivo):
					unlink('./assets/manager/uploads/programacoes/' . $imagem->arquivo);
				endif;		
				
				$this->model->delProgramacao($id_programacao);
			}
			catch (Exception $e)
			{
				show_error($e->getMessage());
			}
		
		else:
			$this->auth->logout();
		endif;	
	}
	
	
	//===========================================================================================
	// FUNÇOES PROGRAMAÇÃO SEMANAL
	//===========================================================================================
	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */