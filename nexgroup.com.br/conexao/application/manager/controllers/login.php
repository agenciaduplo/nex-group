<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index ()
	{
		$this->auth->logout();
		$this->load->view('login/login');
	}
	
	public function cadastro ()
	{
		$this->load->view('login/cadastro.php');
	}
	
	public function cadastro2 ()
	{
		$this->load->view('login/cadastro.2.php');
	}
	
	public function cadastro3 ()
	{
		$this->load->view('login/cadastro.3.php');
	}
	
	public function confirmou ()
	{
		$this->load->view('login/confirmou.php');
	}
	
	public function mensagem_login ($x = "")
	{
		$data = array (
			'x'			=> "$x"
		);
	
		$this->load->view('login/mensagem_login.php', $data);
	}
	
	public function nao_confirmou ()
	{
		$this->load->view('login/nao.confirmou.php');
	}
	
	public function termos ()
	{
		$this->load->view('login/login.termos.php');
	}
	
	public function esqueceu_senha ()
	{
		$this->load->view('login/esqueceu_senha.php');
	}
	
	function entrar ()
	{
		$username = $this->input->post('login');
		$password = $this->input->post('senha');
		
		//exit;
		
		if ($this->auth->try_login($username, $password))
		{
			$this->load->model('login_model', 'model');
			
			$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
			
			$verificaConfirmacao = $this->model->verificaConfirmacao($id_usuario);
			
			if($verificaConfirmacao):
			
				$this->model->inserirLogAcesso($id_usuario);
				redirect('home');
				
			else:
			
				$confirmou = "N";
				$this->sair($confirmou);
			
			endif;
		}
		else
		{
			redirect('login');
		}
	}
	
	function aceito_termos ()
	{
		$this->load->model('login_model', 'model');
			
		$this->model->aceitaTermos($this->input->post('id_usuario'), $this->input->post('termos'));			
		
		$this->load->view('login/cadastro.3.php');		
	}
	
	function aceito_termos_login ()
	{
		$this->load->model('login_model', 'model');
			
		$this->model->aceitaTermos($this->input->post('id_usuario'), $this->input->post('termos'));			
		
		$this->load->view('login/login.termos.ok.php');		
	
	}
	
	
	function enviar_cadastro ()
	{
		$this->load->model('login_model', 'model');
		
		$login 		= $this->input->post('email');
		$verifica 	= $this->model->verificaLogin($login);
			
		$data = array (
			'nome'			=> $this->input->post('nome'),
			'sobrenome'		=> $this->input->post('sobrenome'),
			'telefone'		=> $this->input->post('telefone'),
			'tel_cel'		=> $this->input->post('celular'),
			'login'			=> $this->input->post('email'),
			'creci'			=> $this->input->post('creci'),
			'senha'			=> md5($this->input->post('senha')),
			'tipo'			=> "corretor",
			'termos'		=> 'S'
		);
		
		$nome 			= $this->input->post('nome');
		$sobrenome 		= $this->input->post('sobrenome');
		$telefone 		= $this->input->post('telefone');
		$celular 		= $this->input->post('celular');
		$login 			= $this->input->post('email');
		$creci			= $this->input->post('creci');
		$senha			= $this->input->post('senha');
		$tipo 			= $this->input->post('tipo');
		
		if($verifica):
		
			$existeUser = "S";
		
		else:	
			
			$idUsuario = $this->model->setUsuario($data);
		
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'sendmail';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			ob_start();
			
			?>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2">Novo corretor cadastrado via Site</td>
			  </tr>
			  <tr>
			    <td colspan="2"><strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="80%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Nome:</td>
			    <td><strong><?=$nome?></strong></td>
			  </tr>					  
			  <tr>
			    <td width="30%">Sobrenome:</td>
			    <td><strong><?=$sobrenome?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Telefone:</td>
			    <td><strong><?=$telefone?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Celular:</td>
			    <td><strong><?=$celular?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">E-mail (Login):</td>
			    <td><strong><?=$login?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Creci:</td>
			    <td><strong><?=$creci?></strong></td>
			  </tr>
			  <tr>
			  <tr>
			    <td width="30%">Senha:</td>
			    <td><strong><?=$senha?></strong></td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>					  					  					  
			  <tr>
			    <td colspan="2"><a href="http://www.nexgroup.com.br/conexao/home" target="_blank">Acesse aqui o Sistema de Corretores</a></td>
			  </tr>	  
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();

			$this->load->helper('tplmail');

			$conteudo = tplmail($conteudo);
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group - Conexão");
			
			$list = array(
				'andre.a@nexvendas.com.br'
			);
			
			$this->email->to($list);
			$this->email->bcc('gabriel.oribes@divex.com.br');
			$this->email->subject('Corretor cadastrado pelo site');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->enviar_link($idUsuario);	
		
		endif;
		
		
		
		if(@$existeUser == "S")
		{
			$dataLogin = array (
				'existeUser'	=> 'S',
				'nome'			=> $nome,
				'sobrenome'		=> $sobrenome,
				'telefone'		=> $telefone,
				'tel_cel'		=> $celular,
				'login'			=> $login,
				'creci'			=> $creci,
				'erro'			=> 'O E-mail (login) informado já está cadastro'
			);
			
			$this->load->view('login/cadastro.php', $dataLogin);
		}
		else
		{
			$dataLogin = array (
				'idUsuario'		=> $idUsuario,
				'existeUser'	=> 'N',
				'nome'			=> $nome,
				'sobrenome'		=> $sobrenome,
				'telefone'		=> $telefone,
				'tel_cel'		=> $celular,
				'login'			=> $login,
				'creci'			=> $creci
			);
			
			// Quando tiver  termos voltar pra linha comentada
			//$this->load->view('login/cadastro.2.php', $dataLogin);
			$this->load->view('login/cadastro.3.php', $dataLogin);
		}
	}
	
	function enviar_link ($idUsuario)
	{
		$this->load->model('login_model', 'model');
		
		$usuario 	= $this->model->verificaUsuario($idUsuario);
			
		if(@$usuario)
		{	
			$data = array (
				'id_usuario'			=> $usuario->id_usuario,
				'confirmado'			=> "N",
				'data_cadastro'			=> date("Y-m-d H:i:s")
			);
				
			$link = $this->model->setLink($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'sendmail';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			ob_start();
			
			?>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			   <tr>
			    <td colspan="2"><strong>Confirmação de e-mail - NEX GROUP / CONEXÃO</strong></td>
			  </tr>	
			   <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>				  
			  <tr>
			    <td colspan="2">Olá <?=@$usuario->nome?> <?=@$usuario->sobrenome?></td>
			  </tr>
			  <tr>
			    <td colspan="2"><strong><a href="<?=site_url()?>login/confirmar_email/<?=@$link?>/<?=@$usuario->id_usuario?>" target="_blank">CLIQUE AQUI PARA CONFIRMAR O SEU CADASTRO.</a></strong></td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>					  					  					  
			  <tr>
			    <td colspan="2"><a href="http://www.nexgroup.com.br/conexao/home" target="_blank">Acesse aqui o Sistema de Corretores</a></td>
			  </tr> 
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();

			$this->load->helper('tplmail');
			$conteudo = tplmail($conteudo);

			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group - Conexão");
			
			$emailDest = $usuario->login;
			
			$list = array(
				"$emailDest"
			);
			
			$this->email->to($list);
			$this->email->subject('Confirmação de e-mail');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
		}
	}

	function confirmar_email ($id_link, $id_usuario)
	{
		$this->load->model('login_model', 'model');
			
		$this->model->confirmarEmail($id_link, $id_usuario);
		
		$data = array (
			'confirmou'			=> "S"
		);
		
		$this->load->view('login/login', $data);	
	}
	
	function recuperar_senha()
	{
		$this->load->model('login_model', 'model');
		
		$this->session->set_flashdata('respostaCadastro', "");
		
		$login 		= $this->input->post('email');
		$verifica 	= $this->model->verificaLogin($login);
		
		if(!$verifica):

			$this->session->set_flashdata('respostaCadastro', "Email informado não está cadastrado.");
			$mensagemEsqueceu = "E-mail informado não está cadastrado.";
		
		else:
	
			$idUser		= $verifica->id_usuario;

			// Gera Nova Senha
				$CaracteresAceitos = 'abcdxywzABCDZYWZ0123456789';
				$max = strlen($CaracteresAceitos)-1;
				$password = null;
				
				for($i=0; $i < 8; $i++) 
				{
					$password .= $CaracteresAceitos{mt_rand(0, $max)};
				}
				
				$novaEmail = $password;
				$novaSenha = md5($password);			
			// Gera Nova Senha
			
			$this->model->updateSenha($idUser, $novaSenha);
			$this->session->set_flashdata('respostaCadastro', "Nova senha enviada para o e-mail: $login");
			$mensagemEsqueceu = "Nova senha enviada para o e-mail: $login";
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		
		ob_start();
		
		?>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2">CONEXÃO - Senha recuperada pelo site</td>
			  </tr>
			  <tr>
			    <td colspan="2">Em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="80%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">Email:</td>
			    <td><strong><?=$login?></strong></td>
			  </tr>					  
			  <tr>
			    <td width="30%">Sua nova senha é:</td>
			    <td><strong><?=$novaEmail?></strong></td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>					  					  					  
			  <tr>
			    <td colspan="2"><a href="http://www.nexgroup.com.br/conexao">Acesse aqui o Sistema de Corretores</a></td>
			  </tr>
			  
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		$this->load->helper('tplmail');
			$conteudo = tplmail($conteudo);
		//Fim da Mensagem
		
		$this->email->from("noreply@nexgroup.com.br", "Nex Group");
		
		$list = array( 
			"$login"
		);
		
		$this->email->bcc('testes@divex.com.br');
		$this->email->to($list);
		$this->email->subject('CONEXÃO - Nova senha de acesso');
		$this->email->message("$conteudo");
		
		$this->email->send();
		
		//===================================================================
		//Termina o envio do email		
		
		endif;
		
		$dataEsqueceu = array (
			'msgesqueceu'			=> $mensagemEsqueceu
		);
		
		$this->load->view('login/esqueceu_senha_ok', $dataEsqueceu);	
	}
	
	function sair ($confirmou = "")
	{
		$this->auth->logout();
		
		if($confirmou == "N"):
		
			$data = array (
				'confirmou'			=> "N"
			);
			
			$this->load->view('login/login', $data);
		else:
			
			redirect('login/');
		
		endif;
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */