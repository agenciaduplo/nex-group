<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function index($offset = "")
	{	
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			
			$this->load->model('usuarios_model', 'model');
			$this->load->helper('text');
			
			$config = array (
				'base_url'		=> 'http://www.nexgroup.com.br/conexao/usuarios/index/',
				'total_rows'	=> $this->model->numUsuarios(),
				'per_page'		=> '10'
			);
			
			$this->pagination->initialize($config); 
			
			$data = array (
				'cadastrados' 		=> $this->model->numUsuarios(),
	    		'usuarios'    		=> ($this->input->post('keyword')) ? $this->model->buscaUsuarios($this->input->post('keyword')) : $this->model->getUsuarios($offset),
				'paginacao'	  		=> $this->pagination->create_links(),
				'buscar'			=> $this->input->post('buscar'),
				'termo'				=> @$this->input->post('keyword'),
				'comunicadosHeader'	=> $this->model->getComunicadosHeader()
			);
			
			$this->load->view('usuarios/usuarios.php', $data);
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function editar ($id_usuario = "")
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
		
			$this->load->model('usuarios_model', 'model');
				
			$data = array (
	    		'usuario'    	=> $this->model->getUser($id_usuario)
			);
			
			$this->load->view('usuarios/usuarios.cadastro.php', $data);
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function editar_dados ()
	{
		$this->auth->check();
		
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		
		$this->load->model('usuarios_model', 'model');
			
		$data = array (
    		'usuario'    	=> $this->model->getUser($id_usuario)
		);
		
		$this->load->view('usuarios/usuarios.cadastro.php', $data);
	}
	
	function apagar ($id_usuario = 0)
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			
			$this->load->model('usuarios_model', 'model');
	
			try
			{
				$this->model->delUsuario($id_usuario);
				redirect('usuarios/');
			}
			catch (Exception $e)
			{
				show_error($e->getMessage());
			}
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function cadastro ()
	{
		$this->auth->check();
		
		$this->load->model('usuarios_model', 'model');
	
		$data = array (
			'usuarios'			=> $this->model->getUsuarios()
		);
	
		$this->load->view('usuarios/usuarios.cadastro.php');
	}
	
	function atualizar_cadastro ()
	{
		$this->auth->check();
		
		$this->load->model('usuarios_model', 'model');
		
		if($this->input->post('id_usuario')):
		
			if($this->input->post('senha') != ""):
			
				$data = array (
					'nome'			=> $this->input->post('nome'),
					'sobrenome'		=> $this->input->post('sobrenome'),
					'telefone'		=> $this->input->post('telefone'),
					'tel_cel'		=> $this->input->post('celular'),
					'creci'			=> $this->input->post('creci'),
					'senha'			=> md5($this->input->post('senha')),
					'tipo'			=> $this->input->post('tipo')
				);
			
			else:
			
				$data = array (
					'nome'			=> $this->input->post('nome'),
					'sobrenome'		=> $this->input->post('sobrenome'),
					'telefone'		=> $this->input->post('telefone'),
					'tel_cel'		=> $this->input->post('celular'),
					'creci'			=> $this->input->post('creci'),
					'tipo'			=> $this->input->post('tipo')
				);
			
			
			endif;
			
			if($_SESSION['conexao']['nex_login_tipo'] == 'administrador')
			{
				$this->model->setUsuario($data, $this->input->post('id_usuario'));
			}
			else
			{
				$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);	
				$this->model->setUsuario($data, $id_usuario);
			}
			
			$this->load->view('usuarios/usuarios.cadastro.ok.php');
		
		endif;
		
	}
	
	function cadastrar_usuario ()
	{
		$this->auth->check();
		
		$this->load->model('usuarios_model', 'model');
	
		$data = array (
			'usuarios'			=> $this->model->getUsuarios()
		);
	
		$this->load->view('usuarios/usuarios.cadastro.inserir.php');
	}
	
	function enviar_link ($idUsuario)
	{
		$this->auth->check();
		
		$this->load->model('login_model', 'model');
		
		$usuario 	= $this->model->verificaUsuario($idUsuario);
			
		if(@$usuario)
		{	
			$data = array (
				'id_usuario'			=> $usuario->id_usuario,
				'confirmado'			=> "N",
				'data_cadastro'			=> date("Y-m-d H:i:s")
			);
				
			$link = $this->model->setLink($data);
			
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'sendmail';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			ob_start();
			
			?>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>
				   <tr>
				    <td colspan="2"><strong>Confirmação de e-mail - NEX GROUP / CONEXÃO</strong></td>
				  </tr>	
				   <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>				  
				  <tr>
				    <td colspan="2">Olá <?=@$usuario->nome?> <?=@$usuario->sobrenome?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong><a href="<?=site_url()?>login/confirmar_email/<?=@$link?>/<?=@$usuario->id_usuario?>">CLIQUE AQUI PARA CONFIRMAR O SEU CADASTRO.</a></strong></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>					  					  					  
				  <tr>
				    <td colspan="2"><a href="http://www.nexgroup.com.br/conexao/atualizar.php/usuarios/lista/">Acesse aqui o Sistema de Corretores</a></td>
				  </tr>
				  
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			$this->load->helper('tplmail');
			$conteudo = tplmail($conteudo);
			//Fim da Mensagem
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group - Conexão");
			
			$emailDest = $usuario->login;
			
			$list = array(
				"$emailDest",
				"testes@divex.com.br"
			);
			
			$this->email->to($list);
			$this->email->subject('Confirmação de e-mail');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
		}
	}
	
	function enviar_cadastro ()
	{
		$this->auth->check();
		
		$this->load->model('login_model', 'model');
		
		$login 		= $this->input->post('email');
		$verifica 	= $this->model->verificaLogin($login);
			
		$data = array (
			'nome'			=> $this->input->post('nome'),
			'sobrenome'		=> $this->input->post('sobrenome'),
			'telefone'		=> $this->input->post('telefone'),
			'tel_cel'		=> $this->input->post('celular'),
			'login'			=> $this->input->post('email'),
			'creci'			=> $this->input->post('creci'),
			'senha'			=> md5($this->input->post('senha')),
			'tipo'			=> $this->input->post('tipo'),
			'termos'		=> "S"
		);
		
		$nome 			= $this->input->post('nome');
		$sobrenome 		= $this->input->post('sobrenome');
		$telefone 		= $this->input->post('telefone');
		$celular 		= $this->input->post('celular');
		$login 			= $this->input->post('email');
		$creci			= $this->input->post('creci');
		$senha			= $this->input->post('senha');
		$tipo 			= $this->input->post('tipo');
		
		if($verifica):
		
			$existeUser = "S";
		
		else:	
			
			$idUsuario = $this->model->setUsuario($data);
		
			//Inicia o envio do email
			//===================================================================
			
			$this->load->library('email');
			
			$config['protocol'] = 'sendmail';
			$config['charset'] 	= 'utf-8';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			ob_start();
			
			?>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="70%">&nbsp;</td>
				  </tr>					  
				  <tr>
				    <td colspan="2">Novo corretor cadastrado via Site</td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong><?=$nome?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">&nbsp;</td>
				    <td width="80%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="30%">IP:</td>
				    <td><strong><?=$this->input->ip_address()?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Nome:</td>
				    <td><strong><?=$nome?></strong></td>
				  </tr>					  
				  <tr>
				    <td width="30%">Sobrenome:</td>
				    <td><strong><?=$sobrenome?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Telefone:</td>
				    <td><strong><?=$telefone?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Celular:</td>
				    <td><strong><?=$celular?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">E-mail (Login):</td>
				    <td><strong><?=$login?></strong></td>
				  </tr>
				  <tr>
				    <td width="30%">Creci:</td>
				    <td><strong><?=$creci?></strong></td>
				  </tr>
				  <tr>
				  <tr>
				    <td width="30%">Senha:</td>
				    <td><strong><?=$senha?></strong></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>					  					  					  
				  <tr>
				    <td colspan="2"><a href="http://www.nexgroup.com.br/conexao/atualizar.php/usuarios/lista/">Acesse aqui o Sistema de Corretores</a></td>
				  </tr>
				  
			<?
			
			$conteudo = ob_get_contents();
			ob_end_clean();
			$this->load->helper('tplmail');
			$conteudo = tplmail($conteudo);
			//Fim da Mensagem
			
			$this->email->from("noreply@nexgroup.com.br", "Nex Group - Conexão");
			
			$list = array(
				'testes@divex.com.br'
			);
			
			$this->email->to($list);
			$this->email->subject('Corretor cadastrado pelo site');
			$this->email->message("$conteudo");
			
			$this->email->send();
			
			//===================================================================
			//Termina o envio do email	
			
			$this->enviar_link($idUsuario);	
		
		endif;
		
		if(@$existeUser == "S")
		{
			$dataLogin = array (
				'existeUser'	=> 'S',
				'nome'			=> $nome,
				'sobrenome'		=> $sobrenome,
				'telefone'		=> $telefone,
				'tel_cel'		=> $celular,
				'login'			=> $login,
				'creci'			=> $creci,
				'erro'			=> 'O E-mail (login) informado já está cadastro'
			);
			
			$this->load->view('usuarios/usuarios.cadastro.inserir.php', $dataLogin);
		}
		else
		{
			$dataLogin = array (
				'idUsuario'		=> $idUsuario,
				'existeUser'	=> 'N',
				'nome'			=> $nome,
				'sobrenome'		=> $sobrenome,
				'telefone'		=> $telefone,
				'tel_cel'		=> $celular,
				'login'			=> $login,
				'creci'			=> $creci
			);
			
			// Quando tiver  termos voltar pra linha comentada
			//$this->load->view('login/cadastro.2.php', $dataLogin);
			$this->load->view('login/cadastro.3.php', $dataLogin);
		}
		
	}


	public function exportar(){	
		$this->auth->check();
		$this->load->helper('download');
		$this->load->model('usuarios_model', 'model');
		$linhas = array();
		$dados = $this->model->getNameAndMail();
		foreach($dados as $x){
			array_push($linhas,('"'.trim(ucwords(strtolower($x->nome))).'";"'.trim(strtolower($x->login)).'"'));
		}
		force_download('email-'.time().'.csv',implode(PHP_EOL,$linhas));
	}
}

/* End of file usuarios.php */
/* Location: ./application/controllers/usuarios.php */