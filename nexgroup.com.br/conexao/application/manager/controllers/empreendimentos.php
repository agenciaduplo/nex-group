<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empreendimentos extends CI_Controller {

    public function index($id_empreendimento = "", $disponibilidades = "") {
        $this->auth->check();

        if ($id_empreendimento) {
            $this->load->model('empreendimentos_model', 'model');

            $estado = $this->model->getEstado($id_empreendimento);
            $id_estado = $estado->id_estado;

            //echo $id_estado; 

            $data = array(
                'page' => 'visualizar',
                'empresas' => $this->model->getEmpresas(),
                'emps' => $this->model->getEmps(),
                'emp' => $this->model->getEmp($id_empreendimento),
                'tipos_arquivos' => $this->model->getTiposArquivos(),
                'arquivos' => $this->model->getArquivos($id_empreendimento),
                'vendidos' => $this->model->getAptosVendidos($id_empreendimento),
                'reservados' => $this->model->getAptosReservados($id_empreendimento),
                'livres' => $this->model->getAptosLivres($id_empreendimento),
                'reservadosLista' => $this->model->getAptosReservadosLista($id_empreendimento),
                'aptos' => $this->model->getAptos($id_empreendimento),
                'imagens' => $this->model->getImagens($id_empreendimento),
                'estados' => $this->model->getEstados(),
                'cidades' => $this->model->getCidades($id_estado),
                'grupos' => $this->model->getgrupos(),
                'id_estado' => $id_estado,
                'comunicadosHeader' => $this->model->getComunicadosHeader(),
                'comunicadoEmp' => $this->model->getComunicadoEmp($id_empreendimento),
                'ultimoLogin' => $this->model->getUltimoLogin()
            );

            $this->load->view('empreendimentos/visualizar', $data);
        } else {
            redirect('home/');
        }
    }

    public function cadastro($id_empreendimento = "", $disponibilidades = "") {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        //echo $id_estado; 

        $data = array(
            'page' => 'visualizar',
            'empresas' => $this->model->getEmpresas(),
            'emps' => $this->model->getEmps(),
            'estados' => $this->model->getEstados(),
            'grupos' => $this->model->getgrupos(),
            'comunicadosHeader' => $this->model->getComunicadosHeader()
        );

        $this->load->view('empreendimentos/cadastro', $data);
    }

    function editar_empreendimento() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $dataOrdem = implode("-", array_reverse(explode("/", $this->input->post('data_ordem'))));

            $data = array(
                'id_empresa' => $this->input->post('emp_empresa'),
                'id_cidade' => $this->input->post('cidade'),
                'nome' => $this->input->post('emp_nome'),
                'caracterizacao' => $this->input->post('caracterizacao'),
                'descricao' => $this->input->post('descricao'),
                'entrega' => $this->input->post('entrega'),
                'endereco' => $this->input->post('emp_end'),
                'mostra_tabela' => $this->input->post('mostra_tabela'),
                'ativo' => $this->input->post('ativo'),
                'data_ordem' => $dataOrdem
            );

            $insert_id = $this->model->setEmpreendimento($data, $this->input->post('id_empreendimento'));

            $id = !empty($insert_id) ? $insert_id : $this->input->post('id_empreendimento');

            $pasta = $this->input->post('pastas');

            if (!empty($pasta)) {
                $this->model->insert_pasta($id, $pasta);
            } else {
                if (!empty($id)) {
                    $this->model->remove_pasta($id);
                }
            }
            if ($this->input->post('id_empreendimento'))
                redirect('empreendimentos/' . $this->input->post('id_empreendimento'));
            else
                redirect('home');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function excluir_empreendimento() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        if ($this->model->delete_empreendimento($this->input->post('id_empreendimento')))
            echo 1; // retorna true, deletou o empreendimento
        else
            echo 2; // retorna false, falha ao deletar
        exit();
    }

    function salvar_arquivo() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $data = array(
                'id_empreendimento' => $this->input->post('id_empreendimento'),
                'id_tipo_arquivo' => $this->input->post('id_tipo_arquivo'),
                'descricao' => $this->input->post('descricao'),
                'data_hora' => date('Y-m-d H:i:s')
            );

            $emp = $this->model->getEmp($this->input->post('id_empreendimento'));
            $type = $this->model->getTipoArquivo($this->input->post('id_tipo_arquivo'));
            $nomeFile2 = $this->utilidades->sanitize_title_with_dashes($emp->nome . "-" . $emp->id_empreendimento . "-" . $type->tipo);

            $dateHoraNow = date("ymdhis");
            $file_name2 = $nomeFile2 . "-" . $dateHoraNow;
            $nomeFile = $file_name2 . $file_data['file_ext'];


            $insert_id = $this->model->setArquivo($data, $this->input->post('id_arquivo'), $nomeFile);

            if ($this->input->post('id_arquivo'))
                redirect('empreendimentos/' . $this->input->post('id_empreendimento') . '/arquivos');
            else
                redirect('empreendimentos/' . $this->input->post('id_empreendimento') . '/arquivos');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function download($id_arquivo) {
        $this->auth->check();

        $this->load->helper('download');
        $this->load->model('empreendimentos_model', 'model');

        try {
            $arquivo = $this->model->getArquivo($id_arquivo);

            $data = file_get_contents("./assets/manager/uploads/arquivos/$arquivo->arquivo"); // Read the file's contents
            $hoje = date("Ymd");
            $name = "$hoje-$arquivo->arquivo";

            force_download($name, $data);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function apagar_arquivo($id_arquivo = 0, $id_empreendimento = 0) {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $this->model->delArquivo($id_arquivo);
            redirect("empreendimentos/$id_empreendimento" . '/arquivos');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function salvar_apto() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $dateCad = implode("-", array_reverse(explode("/", $this->input->post('data_cadastro'))));

            $data = array(
                'status' => $this->input->post('status'),
                'apartamento' => $this->input->post('apartamento'),
                'id_empreendimento' => $this->input->post('id_empreendimento'),
                'coluna' => $this->input->post('coluna'),
                'quem' => $this->input->post('quem'),
                'imobiliaria' => $this->input->post('imobiliaria'),
                'ordem' => $this->input->post('ordem'),
                'data_cadastro' => @$dateCad,
                'data_hora' => date('Y-m-d H:i:s')
            );

            $insert_id = $this->model->setApto($data, $this->input->post('id_apartamento'));

            redirect('empreendimentos/' . $this->input->post('id_empreendimento') . '/disponibilidades');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function atualizar_apto() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $dateCad = implode("-", array_reverse(explode("/", $this->input->post('data_cadastro'))));

            $data = array(
                'status' => $this->input->post('status'),
                'apartamento' => $this->input->post('apartamento'),
                'id_empreendimento' => $this->input->post('id_empreendimento'),
                'coluna' => $this->input->post('coluna'),
                'quem' => $this->input->post('quem'),
                'imobiliaria' => $this->input->post('imobiliaria'),
                'ordem' => $this->input->post('ordem'),
                'data_cadastro' => @$dateCad,
                'data_hora' => date('Y-m-d H:i:s')
            );

            $insert_id = $this->model->setApto($data, $this->input->post('id_apartamento'));

            $dataApto = array(
                'apartamento' => $this->input->post('apartamento'),
                'id_empreendimento' => $this->input->post('id_empreendimento')
            );

            $this->load->view('empreendimentos/modal.disponibilidades.ok.php', $data);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function editar_apto($id_apartamento) {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $data = array(
                'apto' => $this->model->getApto($id_apartamento)
            );

            $this->load->view('empreendimentos/modal.disponibilidades.php', $data);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function apagar_apto($id_apartamento, $id_empreendimento) {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $this->model->delApto($id_apartamento);
            redirect('empreendimentos/' . $id_empreendimento . '/disponibilidades');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function download_imagem($id_imagem) {
        $this->auth->check();

        $this->load->helper('download');
        $this->load->model('empreendimentos_model', 'model');

        try {
            $imagem = $this->model->getImagem($id_imagem);

            $data = file_get_contents("./assets/manager/uploads/imagens/$imagem->imagem");
            $hoje = date("Ymd");
            $name = "$hoje-$imagem->imagem";

            force_download($name, $data);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function excluir_imagem($id_imagem, $id_empreendimento) {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            $this->model->delImagem($id_imagem);
            redirect("empreendimentos/$id_empreendimento" . '/imagens');
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function atualizar_legenda($id_imagem, $id_empreendimento) {
        $this->auth->check();
        $return = false;
        $this->load->model('empreendimentos_model', 'model');
        if ($this->input->post()) {
            $this->model->updateImagem($id_imagem, $this->input->post('legenda'));
            $return = site_url() . ("empreendimentos/$id_empreendimento/imagens");
        }
        $this->load->view('empreendimentos/modal.editar.legenda.php', array(
            'img' => $this->model->getImagem($id_imagem),
            'return' => $return
        ));
    }

    function enviar_imagem() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        try {
            if ($this->input->post('id_empreendimento')) {
                $id_empreendimento = $this->input->post('id_empreendimento');

                $data = array(
                    'id_empreendimento' => $this->input->post('id_empreendimento'),
                    'legenda' => $this->input->post('legenda'),
                    'data_cadastro' => date("Y-m-d H:i:s")
                );

                $this->model->setImagem($data, $id_empreendimento);

                redirect("empreendimentos/$id_empreendimento/imagens");
            }
        } catch (Exception $e) {
            show_error($e->getMessage());
        }
    }

    function buscar_cidades() {
        $this->auth->check();

        $this->load->model('empreendimentos_model', 'model');

        $id_estado = $this->input->post('id_estado');

        $data = array(
            'cidades' => $this->model->getCidades($id_estado)
        );

        $this->load->view('empreendimentos/ajax.cidades.php', $data);
    }

    function impressao($id_empreendimento) {
        $this->load->model('empreendimentos_model', 'model');

        $data = array(
            'empreendimento' => $this->model->getEmpreendimento($id_empreendimento),
            'aptos' => $this->model->getAptos($id_empreendimento),
            'vendidos' => $this->model->getAptosVendidos($id_empreendimento),
            'reservas' => $this->model->getReservas($id_empreendimento),
            'reservados' => $this->model->getAptosReservados($id_empreendimento),
            'livres' => $this->model->getAptosLivres($id_empreendimento)
        );
        $this->load->view('impressao/disponibilidades.php', $data);
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */