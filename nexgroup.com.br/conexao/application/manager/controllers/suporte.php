<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suporte extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("America/Sao_Paulo");
		setlocale(LC_ALL, 'pt_BR');
	}

	public function sendMensagemXHR()
	{
		$this->auth->check();
		$this->load->model('usuarios_model', 'model');

		$user_id = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$user = (array) $this->model->getUser($user_id);

		if( ! $user['id_usuario'] > 0)
		{
			$this->auth->logout();
			redirect('login/');
		}

		$mensagem = nl2br(trim($this->input->post('msg')));

		if(strlen($mensagem) < 50)
		{
			$response = array('erro' => 1);
			echo json_encode($response);
			exit;
		}

		$this->load->library('user_agent');

		//===================================================================
		//	Efetua o registro na base

		$dados = array(
			'id_usuario' => $user['id_usuario'],
			'data_envio' => date('Y-m-d H:i:s'),
			'msg' 		 => $mensagem,
			'ip'		 => $this->input->ip_address(),
			'user_agent' => $this->input->user_agent()
		);

		$this->db->insert('usuarios_suporte', $dados);

		//	Fim do registro na base, prepara para enviar email de retorno
		//===================================================================
		//	EMAIL DE RETORNO
		//
		//	Configuração do email

		$this->load->library('email');
		$Email = new CI_Email();

		$config['protocol'] = 'mail';

		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['newline']  = "\r\n";
		$config['crlf'] 	= "\r\n";

		$Email->initialize($config);

		//	Fim da Configuração do email
		//===================================================================
		//	Corpo do Email

		ob_start();
		?>
		<table style="font-family:Verdana;font-size:12px;color:#333;line-height: 18px;" cellpadding="0" cellspacing="0" width="600" border="0">
			<tr>
				<td style="padding: 3px 0;">
					<img src="http://www.nexgroup.com.br/conexao/assets/email/index_r1_c1.jpg" width="540" height="80" border="0" alt="">
				</td>
			</tr>
			<tr>
				<td style="padding-top:15px">
					<h3 style="font-size:16px;margin:0;color: #333;">Solicitação de Suporte ao Corretor. Enviado via conexão.</h3><br/>
					Realizado em <?=date("d/m/Y H:i:sa")?>
				</td>
			</tr>
			<tr>
				<td style="padding-top:15px">
					<strong>Nome:</strong>
					<span style="font-size: 16px;"><?=$user['nome'] .' '. $user['sobrenome']?></span>
				</td>
			</tr>
			<tr>
				<td style="padding-top:15px">
					<strong>CRECI:</strong>
					<span style="font-size: 16px; font-weight: bold;"><?=$user['creci']?></span>
				</td>
			</tr>
			<tr>
				<td style="padding-top:5px">
					<strong>Email:</strong>
					<span style="font-size: 16px;text-decoration: underline;"><?=$user['login']?></span>
				</td>
			</tr>
			<tr>
				<td style="padding-top:5px">
					<strong>Telefone:</strong>
					<span style="font-size: 16px;text-decoration: underline;"><?=$user['telefone']?></span>
				</td>
			</tr>
			<tr>
				<td style="padding-top:5px">
					<strong>Celular:</strong>
					<span style="font-size: 16px;text-decoration: underline;"><?=$user['tel_cel']?></span>
				</td>
			</tr>
			<tr>
				<td style="padding-top:10px">
					<strong>Mensagem:</strong><br/>
					<?=$mensagem?>
				</td>
			</tr>
			<tr>
				<td style="color:#666;font-size:11px;font-family:Verdana;padding-top:25px;line-height: 14px;">
					Suporte ao corretor.<br/>
					Para visualizar este contato, acesse ao painel de gerenciamento de conteúdo.<br/>
					http://www.nexgroup.com.br/conexao<br/>
					&copy; CONEXÃO - NEX GROUP <?=date('Y')?><br/>
				</td>
			</tr>
		</table>
		<?
		$conteudo = ob_get_contents();
		ob_end_clean();

		//	Fim Corpo do Email
		//===================================================================
		//	Recepientes

		$Email->from("noreply@nexgroup.com.br", "Nex Group");
		$Email->to('andre.a@nexvendas.com.br, contato@nexvendas.com.br');
		$Email->subject('Conexão - Solicitação de Suporte ao Corretor');
		$Email->message("$conteudo");

		//	Fim Recepientes
		//===================================================================
		//	Envia Email

		if(@$Email->send())
			$response = array('erro' => 0);   // Email send success
		else
			$response = array('erro' => 101); // Email send fail


		//  Termina o envio do email
		//===================================================================

		echo json_encode($response);
		exit;
	}
}

/* End of file suporte.php */
/* Location: ./application/controllers/suporte.php */
