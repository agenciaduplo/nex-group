<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Grupos extends CI_Controller {

    public function index($id = false) {
        $this->auth->check();
        $this->load->model('home_model', 'model');

        if ($id) {
            $grupo = $this->model->getGrupo($id);
            if (isset($grupo->id)) {
                $this->load->view('grupos/lista', array(
                    'page' => 'home',
                    'grupo' => $grupo,
                    'emps' => $this->model->getGrupoEmp($grupo->id),
                    'comunicado' => $this->model->getUltimoComunicado(),
                    'comunicadosHeader' => $this->model->getComunicadosHeader(),
                    'ultimoLogin' => $this->model->getUltimoLogin(),
                    'is_admin' => $this->default_model->is_admin()
                ));
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function cadastro() {
        $this->auth->check();
        if ($this->default_model->is_admin()) {
            $this->load->model('grupos_model', 'model');
            if ($this->input->post()) {
                $id = $this->model->insert(array(
                    'nome' => $this->input->post('form_nome'),
                    'status' => $this->input->post('form_ativo')
                ));
                if ($id > 0) {
                    $this->session->set_flashdata('resposta', 'Pasta salva com sucesso!');

                    $this->load->library('upload', array(
                        'upload_path' => './assets/manager/uploads/logos/',
                        'allowed_types' => 'gif|jpg|png',
                        'max_size' => 15000,
                        'max_width' => 12000,
                        'max_height' => 12000,
                        'encrypt_name' => true
                    ));
                    if ($this->upload->do_upload('logomarca')) {
                        $file_data = $this->upload->data();
                        //unlink('./assets/manager/uploads/logos/' . $grupo->logomarca);
                        $this->load->library('image_lib', array(
                            'image_library' => 'GD2',
                            'source_image' => $file_data['full_path'],
                            'new_image' => './assets/manager/uploads/logos/' . $file_data['file_name'],
                            'maintain_ratio' => TRUE,
                            'width' => 150,
                            'height' => 84,
                            'quality' => 100
                        ));
                        $this->image_lib->resize();

                        $acao = "insert";
                        $tabela = "grupos";
                        $sql = $this->db->last_query();

                        $this->model->update(array('logomarca' => $file_data['file_name']), $id);

                        $this->model->inserirLogAcoes($tabela, $acao, $sql);
                        redirect('home');
                    }
                }
            }
            $this->load->view('grupos/cadastro');
        } else {
            show_404();
        }
    }

    public function editar($id = false) {
        $this->auth->check();
        if ($this->default_model->is_admin()) {
            $this->load->model('grupos_model', 'model');
            if ($id) {
                $grupo = $this->model->getGrupo($id);
                if (isset($grupo->id)) {
                    if ($this->input->post()) {
                        $this->session->set_flashdata('resposta', 'Pasta editada com sucesso!');
                        $this->model->update(array(
                            'nome' => $this->input->post('form_nome'),
                            'status' => $this->input->post('form_ativo')
                                ), $id);
                        $this->load->library('upload', array(
                            'upload_path' => './assets/manager/uploads/logos/',
                            'allowed_types' => 'gif|jpg|png',
                            'max_size' => 15000,
                            'max_width' => 12000,
                            'max_height' => 12000,
                            'encrypt_name' => true
                        ));
                        if ($this->upload->do_upload('logomarca')) {
                            $file_data = $this->upload->data();
                            $this->load->library('image_lib', array(
                                'image_library' => 'GD2',
                                'source_image' => $file_data['full_path'],
                                'new_image' => './assets/manager/uploads/logos/' . $file_data['file_name'],
                                'maintain_ratio' => TRUE,
                                'width' => 150,
                                'height' => 84,
                                'quality' => 100
                            ));
                            $this->image_lib->resize();
                            $this->model->update(array('logomarca' => $file_data['file_name']), $id);
                        }
                        $acao = "update";
                        $tabela = "grupos";
                        $sql = $this->db->last_query();
                        $this->model->inserirLogAcoes($tabela, $acao, $sql);
                        redirect('home');
                    }
                    $this->load->view('grupos/editar', array(
                        'grupo' => $grupo
                    ));
                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function remover($id = false) {
        $this->auth->check();
        if ($this->default_model->is_admin()) {
            $this->load->model('grupos_model', 'model');
            if ($id) {
                $grupo = $this->model->getGrupo($id);
                if (isset($grupo->id)) {
                    unlink('./assets/manager/uploads/logos/' . $grupo->logomarca);
                    $this->model->delete($id);
                    $acao = "delete";
                    $tabela = "grupos";
                    $sql = $this->db->last_query();
                    $this->model->inserirLogAcoes($tabela, $acao, $sql);
                    $this->session->set_flashdata('resposta', 'Pasta editada com sucesso!');
                    redirect('home');
                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function excluir_grupo() {
        $this->auth->check();
        
        $this->load->model('grupos_model', 'model');
        
        if ($this->model->deleteGrupo($this->input->post('id_grupo')))
            echo 1; // retorna true, deletou o grupo
        else
            echo 2; // retorna false, falha ao deletar
        exit();
    }

}
