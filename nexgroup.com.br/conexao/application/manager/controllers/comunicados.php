<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comunicados extends CI_Controller {

	public function index($offset = "")
	{
		$this->auth->check();
			
		if(@$_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			
			$this->load->model('comunicados_model', 'model');
			$this->load->helper('text');
			
			$config = array (
				'base_url'		=> 'http://www.nexgroup.com.br/conexao/comunicados/index/',
				'total_rows'	=> $this->model->numComunicados(),
				'per_page'		=> '50'
			);
			
			$this->pagination->initialize($config); 
			
			$data = array (
				'cadastrados' 		=> $this->model->numComunicados(),
	    		'comunicados'   	=> $this->model->getComunicados($offset),
	    		'comunicadosHeader'	=> $this->model->getComunicadosHeader(),
	    		'emps'  			=> $this->model->getEmps()
			);
			
			$this->load->view('comunicados/comunicados.php', $data);
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
	
	function salvar ()
	{
		$this->auth->check();
		
		$this->load->model('comunicados_model', 'model');
		
		try
		{			
			$data = array (
				'ativo'				=> $this->input->post('ativo'),
				'tipo'				=> $this->input->post('tipo'),
				'titulo' 			=> $this->input->post('titulo'),
				'descricao' 		=> $this->input->post('descricao'),
				'id_empreendimento'	=> $this->input->post('id_empreendimento'),
				'data_hora' 		=> date("Y-m-d H:i:s")
			);
			
			$insert_id = $this->model->setComunicado($data, $this->input->post('id_comunicado'));
				
			if ($this->input->post('id_comunicado'))
				redirect('comunicados/editar/' . $this->input->post('id_comunicado'));
			else
				redirect('comunicados');
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}	
	}
	
	function editar ($id_comunicado)
	{
		$this->auth->check();
		
		$this->load->model('comunicados_model', 'model');
		
		try
		{
		
			$data = array (
	    		'comunicados'   	=> $this->model->getComunicados(@$offset),
				'comunicado'		=> $this->model->getComunicado($id_comunicado),
				'comunicadosHeader'	=> $this->model->getComunicadosHeader(),
				'emps'  			=> $this->model->getEmps()
			);				
				
			$this->load->view('comunicados/comunicados.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function visualizar ($id_comunicado)
	{
		$this->auth->check();
		
		$this->load->model('comunicados_model', 'model');
		
		try
		{
		
			$data = array (
				'comunicado'		=> $this->model->getComunicado($id_comunicado),
				'comunicadosHeader'	=> $this->model->getComunicadosHeader()
			);				
				
			$this->load->view('comunicados/visualizar.php', $data);
		}
		catch (Exception $e)
		{
			show_error($e->getMessage());
		}
	}
	
	function apagar ($id_comunicado = 0)
	{
		$this->auth->check();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			
			$this->load->model('comunicados_model', 'model');
	
			try
			{		
				$imagem = $this->model->getComunicado($id_comunicado);
				if($imagem->imagem_destaque):
					unlink('./assets/manager/uploads/comunicados/' . $imagem->imagem_destaque);
				endif;		
				
				$this->model->delComunicado($id_comunicado);
				
				redirect('comunicados/');
			}
			catch (Exception $e)
			{
				show_error($e->getMessage());
			}
		
		else:
			$this->auth->logout();
			redirect('login/');
		endif;
	}
}

/* End of file comunicados.php */
/* Location: ./application/controllers/comunicados.php */