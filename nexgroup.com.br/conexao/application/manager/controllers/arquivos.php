<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arquivos extends CI_Controller {

	private $dir = './assets/manager/uploads/arquivos_gerais';
	private $mime = "jpg|jpeg|png|gif|zip|rar|pdf|doc|docx|csv|txt|xlsx|xlsb";

	public function index(){
		$this->auth->check();
		$this->load->model('arquivos_model','model');
		$this->load->view('arquivos/lista',array(
			'arquivos' => $this->model->getArquivos(),
			'is_admin' => $_SESSION['conexao']['nex_login_tipo'] == 'administrador' ? true : false
		));
	}
	public function download($id = false,$name = false){
		$this->auth->check();
		$this->load->model('arquivos_model','model');
		if($id){
			$arquivo = $this->model->getArquivo($id);
			if(isset($arquivo->id)){
				$this->load->helper('download');
				force_download($arquivo->filename,file_get_contents($this->dir.'/'.$arquivo->arquivo));
			} else { show_404(); }
		} else { show_404(); }
	}
	public function deletar($id = false){
		$this->auth->check();
		$this->load->model('arquivos_model','model');
		if($_SESSION['conexao']['nex_login_tipo']){
			if($id){
				$arquivo = $this->model->getArquivo($id);
				if(isset($arquivo->id)){
					@unlink($this->dir.'/'.$arquivo->arquivo);
					$this->model->delete($id);
					redirect('arquivos');
				} else { show_404(); }
			} else { show_404(); }
		} else { show_404(); }
	}
	public function upload(){
		$this->auth->check();
		$this->load->model('arquivos_model','model');
		if($_SESSION['conexao']['nex_login_tipo']){
			$this->load->library('upload',array(
				'upload_path' => $this->dir,
				'allowed_types' => $this->mime,
				'max_size' => '8000',
				'encrypt_name' => true
			));
			if($this->upload->do_upload()){
				$file = $this->upload->data();
				$nome = $this->input->post('nome');
				$insert = array(
					'nome' => $nome,
					'filename' => $this->utilidades->sanitize_title_with_dashes($nome).$file['file_ext'],
					'arquivo' => $file['file_name'],
					'ctime' => time()
				);
				$this->model->insert($insert);
				redirect('arquivos');
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			show_404();
		}
	}
}