<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {

	public function index()
	{
		//$this->load->view('logs/logs.acessos.php');
	}
	
	function acessos ($offset = "")
	{
		$this->auth->check();
		
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.nexgroup.com.br/conexao/logs/acessos/',
			'total_rows'	=> $this->model->numAcessos(),
			'per_page'		=> '10'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'cadastrados' 		=> $this->model->numAcessos(),
    		'acessos'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcessos($this->input->post('keyword')) : $this->model->getAcessos($offset),
			'paginacao'	  		=> $this->pagination->create_links(),
			'buscar'			=> $this->input->post('buscar'),
			'termo'				=> @$this->input->post('keyword'),
			'comunicadosHeader'	=> $this->model->getComunicadosHeader()
		);
		
		$this->load->view('logs/logs.acessos.php', $data);
	}
	
	function acoes ($offset = "")
	{
		$this->auth->check();
		
		$this->load->model('logs_model', 'model');
		
		$config = array (
			'base_url'		=> 'http://www.nexgroup.com.br/conexao/logs/acoes/',
			'total_rows'	=> $this->model->numAcoes(),
			'per_page'		=> '10'
		);
		
		$this->pagination->initialize($config); 
		
		$data = array (
			'cadastrados' 		=> $this->model->numAcoes(),
    		'acoes'     		=> ($this->input->post('keyword')) ? $this->model->buscaAcoes($this->input->post('keyword')) : $this->model->getAcoes($offset),
			'paginacao'	  		=> $this->pagination->create_links(),
			'buscar'			=> $this->input->post('buscar'),
			'termo'				=> @$this->input->post('keyword'),
			'comunicadosHeader'	=> $this->model->getComunicadosHeader()
		);
		
		$this->load->view('logs/logs.acoes.php', $data);
	}	
}

/* End of file logs.php */
/* Location: ./application/controllers/logs.php */