<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendas extends CI_Controller{

	public function index(){
		$this->auth->check();
		$this->load->model('vendas_model', 		'model');
		$this->load->model('usuarios_model', 	'usuarios_model');

		$data_i = $this->input->post("data_i");
		$data_f = $this->input->post("data_f");
		$data_i = ($data_i == "") ? "1/".date("m/Y") : $data_i;
		$data_f = ($data_f == "") ? date("d/m/Y")    : $data_f;

		$del_atend  = "";
		$del_inter  = "";

		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
			$atendimento 	= $this->model->getContatos("atendimentos",$data_i,$data_f);
			$del_atend 		= $this->model->getDelegacoes("a",$atendimento);
			$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f);
			$del_inter 		= $this->model->getDelegacoes("i",$interesse);
		}else{
			$atendimento 	= $this->model->getContatos("atendimentos",$data_i,$data_f,$this->input->get("search"));
			$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$this->input->get("search"));
			$atendimento  = $this->model->verifyContactsForUser("a",$atendimento);
			$interesse  	= $this->model->verifyContactsForUser("i",$interesse);
		}

		$tot_a 				= is_array($atendimento) ? count($atendimento) : 0;
		$tot_i 				= is_array($interesse)   ? count($interesse)   : 0;

		$data = array(
			'tot_a'							=> $tot_a,
			'tot_i'							=> $tot_i,
    	'atendimento'  			=> $atendimento,
    	'interesse' 				=> $interesse,
			'data_i'						=> $this->input->post("data_i"),
			"data_f" 						=> $this->input->post('data_f'),
			"search" 						=> $this->input->post('search'),
			'table' 						=> "ambos",
			"del_atend"					=> $del_atend,
			"del_inter"  				=> $del_inter,
			'comunicadosHeader'	=> $this->usuarios_model->getComunicadosHeader()
		);

		$this->load->view('vendas/vendas.php', $data);
	}

	function delegar($idcontato,$table,$idcontato_delegado=""){
		$this->auth->check();
		$this->load->model('vendas_model', 		'vendas_model');
		$this->load->model('usuarios_model', 	'usuarios_model');

		$contato 		= $this->vendas_model->getContato($idcontato,$table);
		if($idcontato_delegado != ""){
			$del_atend 	= $this->vendas_model->getByIdAtendimentoDelegado($idcontato_delegado);
			$del_atend  = $del_atend[0];
		}else{
			$del_atend  = "";
		}

		$data = array(
			'del_atend' 							=> $del_atend,
			'idcontato' 							=> $idcontato,
			'tipo'										=> $table,
			'contato' 								=> $contato,
			'usuarios'								=> $this->usuarios_model->getAll("@nexvendas.com.br")
		);

		$this->load->view('vendas/vendas.delegar.php', $data);
	}

	function search(){
		$this->auth->check();
		$this->load->model('vendas_model', 		'model');
		$this->load->model('usuarios_model', 	'usuarios_model');

		$data_i = $this->input->get("data_i");
		$data_f = $this->input->get("data_f");
		$data_i = ($data_i == "") ? "1/".date("m/Y") : $data_i;
		$data_f = ($data_f == "") ? date("d/m/Y")    : $data_f;

		$del_atend  = "";
		$del_inter  = "";
		$atendimento 	= "";
		$interesse   	= "";


		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
			if($this->input->get("table") != "ambos"){
				if($this->input->get("table") == "interesses"){
					$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$this->input->get("search"));
					$del_inter 		= $this->model->getDelegacoes("i",$interesse);
				}else{
					$atendimento = $this->model->getContatos("atendimentos",$data_i,$data_f,$this->input->get("search"));
					$del_atend 	 = $this->model->getDelegacoes("a",$atendimento);
				}
			}else{
				$atendimento = $this->model->getContatos("atendimentos",$data_i,$data_f,$this->input->get("search"));
				$del_atend 	 = $this->model->getDelegacoes("a",$atendimento);
				$interesse 	 = $this->model->getContatos("interesses",$data_i,$data_f,$this->input->get("search"));
				$del_inter 	 = $this->model->getDelegacoes("i",$interesse);
			}
		}else{
			if($this->input->get("table") != "ambos"){
				if($this->input->get("table") == "interesses"){
					$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$this->input->get("search"));
					$interesse  	= $this->model->verifyContactsForUser("i",$interesse);
				}else{
					$atendimento  = $this->model->getContatos("atendimentos",$data_i,$data_f,$this->input->get("search"));
					$atendimento  = $this->model->verifyContactsForUser("a",$atendimento);
				}
			}else{
				$atendimento 	= $this->model->getContatos("atendimentos",$data_i,$data_f,$this->input->get("search"));
				$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$this->input->get("search"));
				$atendimento  = $this->model->verifyContactsForUser("a",$atendimento);
				$interesse  	= $this->model->verifyContactsForUser("i",$interesse);
			}
		}

		$tot_a 				= is_array($atendimento) ? count($atendimento) : 0;
		$tot_i 				= is_array($interesse)   ? count($interesse)   : 0;

		$data = array(
			'tot_a'							=> $tot_a,
			'tot_i'							=> $tot_i,
    	'atendimento'  			=> $atendimento,
    	'interesse' 				=> $interesse,
			'data_i'						=> $this->input->get("data_i"),
			"data_f" 						=> $this->input->get('data_f'),
			"search" 						=> $this->input->get('search'),
			"table" 						=> $this->input->get("table"),
			"del_atend"					=> $del_atend,
			"del_inter"  				=> $del_inter,
			'comunicadosHeader'	=> $this->usuarios_model->getComunicadosHeader()
		);

		$this->load->view('vendas/vendas.php', $data);
	}

	function DelegarSave(){
		$this->auth->check();
		$this->load->model('vendas_model','model');
		$this->load->library('encrypt');

		$idusuario_logado = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$data = array(
			'idcontato_delegado' 			=> $this->input->post("idcontato_delegado"),
			'idcontato' 							=> $this->input->post("idcontato"),
			'tabela'									=> $this->input->post("table"),
			'idusuario' 							=> $this->input->post("f_idusuario"),
			'idusuario_delegou'  			=> $idusuario_logado,
			'data_hr'									=> date("Y-m-d H:i:s")
		);

		$this->model->cadastraDelegacao($data);

		$this->load->view('vendas/vendas.delegar.ok.php');
	}


	function StatusContato($idcontato, $table){
		$this->auth->check();
		$this->load->library('encrypt');
    $id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$this->load->model('vendas_model','vendas_model');

		$contato 		= $this->vendas_model->getContato($idcontato,$table);
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador' || $id_usuario == $this->vendas_model->getIdUsuarioDelegate($idcontato,$table)){
			$contato_st = $this->vendas_model->getContatoStatus("",$idcontato,$table);
		}else{
			$contato_st = "";
			$contato    = "";
		}

		if(is_array($contato)){
			$data = array(
				'idcontato' 							=> $idcontato,
				'tipo'										=> $table,
				'contato' 								=> $contato,
				'contato_st' 							=> $contato_st
			);

			$this->load->view('vendas/vendas.detalhes.php', $data);
		}else{
			$this->load->view('vendas/vendas.detalhes.not.php');
		}
	}

	function StatusContatoCad($idcontato, $table){
		$this->auth->check();
		$this->load->model('vendas_model','vendas_model');

		$idcontato_delegado = $this->vendas_model->getIdContatoDelegado($idcontato,$table);
		$contato_st 				= $this->vendas_model->getContatoStatus($idcontato_delegado);
		$status 					  = $this->vendas_model->getStatus();


		$data = array(
			'idcontato_delegado' 			=> $idcontato_delegado,
			'status'									=> $status,
			'idcontato' 							=> $idcontato,
			'tipo'										=> $table,
			'contato_st' 							=> $contato_st
		);

		$this->load->view('vendas/vendas.status.cad.php', $data);
	}

	function ContatoStatusSave(){
		$this->auth->check();
		$this->load->model('vendas_model','model');

		$data = array(
			'idcontato_status'				=> "",
			'idcontato_delegado' 			=> $this->input->post("idcontato_delegado"),
			'idstatus' 								=> $this->input->post("idstatus"),
			'descricao'								=> $this->input->post("descricao"),
			'data_hr'									=> date("Y-m-d H:i:s")
		);

		$this->model->cadastraContatoStatus($data);

		$this->load->view('vendas/vendas.status.cad.ok.php');
	}

	function ContatoStatusExc($idcontato, $table, $idcontato_status){
		$this->auth->check();
		$this->load->model('vendas_model','vendas_model');

		$this->vendas_model->ContatoStatusExc($idcontato_status);

		$contato 		= $this->vendas_model->getContato($idcontato,$table);
		$contato_st = $this->vendas_model->getContatoStatus("",$idcontato,$table);

		$data = array(
			'idcontato' 							=> $idcontato,
			'tipo'										=> $table,
			'contato' 								=> $contato,
			'contato_st' 							=> $contato_st
		);

		$this->load->view('vendas/vendas.detalhes.php', $data);
	}

	public function exportar($data_i,$data_f,$table,$search=""){
		$this->auth->check();
		$this->load->helper('download');
		$this->load->model('vendas_model', 		'model');
		$this->load->model('usuarios_model', 	'usuarios_model');

		$data_i = str_replace("-", "/", $data_i);
		$data_f = str_replace("-", "/", $data_f);

		$del_atend  	= "";
		$del_inter  	= "";
		$atendimento 	= "";
		$interesse   	= "";


		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
			if($table != "ambos"){
				if($table == "interesses"){
					$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$search);
					$del_inter 		= $this->model->getDelegacoes("i",$interesse);
				}else{
					$atendimento = $this->model->getContatos("atendimentos",$data_i,$data_f,$search);
					$del_atend 	 = $this->model->getDelegacoes("a",$atendimento);
				}
			}else{
				$atendimento = $this->model->getContatos("atendimentos",$data_i,$data_f,$search);
				$del_atend 	 = $this->model->getDelegacoes("a",$atendimento);
				$interesse 	 = $this->model->getContatos("interesses",$data_i,$data_f,$search);
				$del_inter 	 = $this->model->getDelegacoes("i",$interesse);
			}
		}else{
			if($table != "ambos"){
				if($table == "interesses"){
					$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$search);
					$interesse  	= $this->model->verifyContactsForUser("i",$interesse);
				}else{
					$atendimento  = $this->model->getContatos("atendimentos",$data_i,$data_f,$search);
					$atendimento  = $this->model->verifyContactsForUser("a",$atendimento);
				}
			}else{
				$atendimento 	= $this->model->getContatos("atendimentos",$data_i,$data_f,$search);
				$interesse 		= $this->model->getContatos("interesses",$data_i,$data_f,$search);
				$atendimento  = $this->model->verifyContactsForUser("a",$atendimento);
				$interesse  	= $this->model->verifyContactsForUser("i",$interesse);
			}
		}

		$tot_a 				= is_array($atendimento) ? count($atendimento) : 0;
		$tot_i 				= is_array($interesse)   ? count($interesse)   : 0;

		$linhas = array();

		$tipo 	 = ($_SESSION['conexao']['nex_login_tipo'] == 'administrador') ? 1 : 0;
		$l_del   = ($tipo == 1) ? '"Delegado Para";' : "";
		$l_email = ($tipo == 1) ? '"Email Enviado Para";' : "";

		array_push($linhas,(
			'"Data";'.
			'"Tipo";'.
			'"Empreendimento";'.
			$l_del.
			'"Nome";'.
			'"Email";'.
			'"Telefone";'.
			'"Comentarios";'.
			'"Campanha";'.
			'"Origem";'.
			$l_email
		));

		if($tot_i > 0){
			foreach($interesse as $x){
				$data 				= date("d/m/Y H:i", strtotime($x->data_envio));
				$nome 				= str_replace(";"," ",strip_tags($x->nome));
				$email 				= str_replace(";"," ",strip_tags($x->email));
				$telefone 		= str_replace(";"," ",strip_tags($x->telefone));
				$comentarios 	= str_replace(";"," ",strip_tags($x->comentarios));
				$comentarios 	= str_replace("'", "",$comentarios);
				$comentarios  = str_replace('"', "",$comentarios);
				$camp 				= str_replace(";"," ",strip_tags($x->camp));
				$origem 			= str_replace(";"," ",strip_tags($x->origem));

				if($tipo == 1 && $del_inter[$x->id_interesse]){
					$del_for    = '"'.$del_inter[$x->id_interesse][0]->nome." - ".$del_inter[$x->id_interesse][0]->email.'";';
				}else{
					if($tipo == 1){
						$del_for    = '"";';
					}else{
						$del_for    = "";
					}
				}

				if($tipo == 1){
					$email_for = trim(strtolower($x->email_enviado_for)).'';
          if($email_for == ""){
            $email_for = "vendas@nexvendas.com.br";
          }
				}else{
					$email_for = '"";';
				}

				array_push($linhas,(
					'"'.$data.'";'.
					'"Interesse";'.
					'"'.trim(ucwords(strtolower($x->empreendimento))).'";'.
					$del_for.
					'"'.trim(ucwords(strtolower($nome))).'";'.
					'"'.trim(ucwords(strtolower($email))).'";'.
					'"'.trim(ucwords(strtolower($telefone))).'";'.
					'"'.trim(ucwords(strtolower($comentarios))).'";'.
					'"'.trim(ucwords(strtolower($camp))).'";'.
					'"'.trim(ucwords(strtolower($origem))).'";'.
					$email_for
					)
				);
			}
		}

		if($tot_a > 0){
			foreach($atendimento as $x){
				$data 				= date("d/m/Y H:i", strtotime($x->data_envio));
				$nome 				= str_replace(";"," ",strip_tags($x->nome));
				$email 				= str_replace(";"," ",strip_tags($x->email));
				$telefone 		= str_replace(";"," ",strip_tags($x->telefone));
				$descricao 		= str_replace(";"," ",strip_tags($x->descricao));
				$descricao 		= str_replace("'", "",$descricao);
				$descricao    = str_replace('"', "",$descricao);
				$camp 				= str_replace(";"," ",strip_tags($x->camp));
				$origem 			= str_replace(";"," ",strip_tags($x->origem));

				if($tipo == 1 && $del_atend[$x->id_atendimento]){
					$del_for  	= '"'.$del_atend[$x->id_atendimento][0]->nome." - ".$del_atend[$x->id_atendimento][0]->email.'";';
				}else{
					if($tipo == 1){
						$del_for    = '"";';
					}else{
						$del_for    = "";
					}
				}

				if($tipo == 1){
					$email_for = trim(strtolower($x->email_enviado_for)).'';
          if($email_for == ""){
            $email_for = "vendas@nexvendas.com.br";
          }
				}else{
					$email_for = '"";';
				}

				array_push($linhas,(
					'"'.date("d/m/Y H:i", strtotime($x->data_envio)).'";'.
					'"Atendimento";'.
					'"'.trim(ucwords(strtolower($x->empreendimento))).'";'.
					$del_for.
					'"'.trim(ucwords(strtolower($nome))).'";'.
					'"'.trim(ucwords(strtolower($email))).'";'.
					'"'.trim(ucwords(strtolower($telefone))).'";'.
					'"'.trim(ucwords(strtolower($descricao))).'";"'.
					'"'.$camp.'";'.
					'"'.$origem.'";'.
					$email_for
					)
				);
			}
		}

		force_download('rel_atendimento-interesse'.time().'.csv',implode(PHP_EOL,$linhas));
	}
}

/* End of file vendas.php */
/* Location: ./application/controllers/vendas.php */