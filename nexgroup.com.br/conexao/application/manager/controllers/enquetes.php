<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enquetes extends CI_Controller {

	public function index(){
		$this->auth->check();
		$this->load->model('enquetes_model','model');
		
		$enquete = $this->model->getEnquete($this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']));
		$nome = explode(" ",$_SESSION['conexao']['nex_login_nome']);
		
		if(!isset($enquete->usuario)){
			$this->load->view('enquetes/celulares',array(
				'nome' => $nome[0],
				'is_admin' => $_SESSION['conexao']['nex_login_tipo'] == 'administrador' ? true : false
			));
		} else {
			$this->load->view('enquetes/celulares-sucesso',array(
				'nome' => $nome[0]
			));
		}
	}
	public function salvar(){
		$this->auth->check();
		$this->load->model('enquetes_model','model');

		$celular = $this->input->post('celular');
		$outro = $this->input->post('outro');

		$dados = !empty($outro) ? $outro : $celular ;

		if(!empty($dados)){
			$this->model->insert(array(
				'usuario' => $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']),
				'desc' => $dados
			));
		}
		redirect('enquetes');
	}
}



