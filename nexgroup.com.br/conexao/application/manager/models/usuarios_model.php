<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model{
	function __construct(){
    // Call the Model constructor
    parent::__construct();
  }

	function inserirLogAcoes($tabela, $acao, $sql){
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$data = array(
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'  => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    			=> $sql,
			'ip'					=> $this->input->ip_address()
		);

		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================

	function numUsuarios(){
		$this->db->select('*')->from('usuarios');
		return $this->db->count_all_results();
	}

	function getComunicadosHeader(){
		$this->db->flush_cache();

		$sql = "SELECT * FROM comunicados WHERE ativo = 'S' ORDER BY data_hora DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getUsuarios($offset = 0){
		$this->db->flush_cache();
		$this->db->select('*')->from('usuarios')->order_by('nome', 'asc')->limit(10, $offset);

		return $this->db->get();
	}

	function buscaUsuarios($keyword){
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		$this->db->select('*')->from('usuarios')->like('nome', $keyword)->or_like('login', $keyword);

		return $this->db->get();
	}

	function getUsuario($id_usuario){
		$where = array ('id_usuario' => $id_usuario);
		$this->db->select('*')->from('usuarios')->where($where);

		$query = $this->db->get();
		return $query->row();
	}

	function getUser($id_usuario){
		$this->db->flush_cache();

		$sql = "SELECT * FROM usuarios WHERE id_usuario = '$id_usuario'";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function setUsuario($data, $id_usuario = ""){
		if($id_usuario){
			$where = array ('id_usuario' => $id_usuario);
			$this->db->select('*')->from('usuarios')->where($where);

			if(!$this->db->count_all_results()){
				throw new Exception('Acesso negado.');
			}else{
				$this->db->set($data);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->update('usuarios');

       //Log Acesso
      	$acao 		= "update";
      	$tabela 	= "usuarios";
      	$sql 		= $this->db->last_query();
      	$this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso
			}
		}else{
			$this->db->set($data)->insert('usuarios');

      //Log Acesso
      	$acao 		= "insert";
      	$tabela 	= "usuarios";
      	$sql 		= $this->db->last_query();
      	$this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso

			return $this->db->insert_id();
		}
	}

	function delUsuario($id_usuario){
		$where = array ('id_usuario' => $id_usuario);
		$this->db->select('*')->from('usuarios')->where($where);

		if(!$this->db->count_all_results()){
			throw new Exception('Acesso negado.');
		}else{
      $this->db->where('id_usuario', $id_usuario);
      $this->db->delete('usuarios');

      //Log Acesso
      	$acao 		= "delete";
      	$tabela 	= "usuarios";
      	$sql 		= $this->db->last_query();
      	$this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso
		}
	}


	function getNameAndMail(){
		$this->db->flush_cache();
		$sql = "SELECT nome, login FROM usuarios ORDER BY nome ASC";
		$query = $this->db->query($sql);
		return $query->result();
	}


	function getAll($like=""){
    $and = ($like != "") ? " AND login LIKE '%".$like."%' OR login = 'atendimento@divex.com.br' " : "";
		$sql = "SELECT * FROM usuarios WHERE tipo = 'corretor' {$and} ORDER BY nome ASC";
		return $this->db->query($sql)->result();
	}


}
/* End of file usuarios_model.php */
/* Location: ./system/application/model/usuarios_model.php */