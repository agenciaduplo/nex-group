<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Default_model extends CI_Model {

	function __construct() { parent::__construct(); }

	function is_admin(){
		if(isset($_SESSION['conexao']['nex_login_tipo']) && $_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
			return true;
		} else { 
			return false;
		}
	}

	function getEmps(){
		$sql = "
			(	
				SELECT
				emp.id_empreendimento AS id,
				emp.nome,
				emp.caracterizacao,
				CONCAT(emp.nome,' - ',emp.caracterizacao,' - ',emp.descricao,' - ',emp.entrega,' - ',emp.endereco,' - ',cidades.cidade,' - ',estados.uf) AS titulo,
				emp.logomarca,
				emp.ativo,
				CONCAT('0') AS is_grupo
				FROM
				empreendimentos AS emp
				LEFT OUTER JOIN cidades ON emp.id_cidade = cidades.id_cidade
				LEFT OUTER JOIN estados ON cidades.id_estado = estados.id_estado
				LEFT OUTER JOIN grupos_empreendimentos ON emp.id_empreendimento = grupos_empreendimentos.id_empreendimento
				WHERE
				grupos_empreendimentos.id_grupo IS NULL
				GROUP BY
				emp.id_empreendimento
				ORDER BY
				emp.data_ordem DESC
			) UNION (
				SELECT
				grupos.id_grupo AS id,
				grupos.nome,
				CONCAT('Pasta') AS caracterizacao,
				CONCAT('') AS titulo,
				grupos.logomarca,
				grupos.`status` AS ativo,
				CONCAT('1') AS is_grupo
				FROM
				grupos
				ORDER BY
				grupos.nome ASC
			) ORDER BY is_grupo DESC
		";
		return $this->db->query($sql)->result();
	}
}