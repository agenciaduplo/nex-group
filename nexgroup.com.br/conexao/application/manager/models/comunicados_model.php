<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comunicados_model extends CI_Model {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================
	
	function numComunicados ()
	{
		$this->db->select('*')->from('comunicados');
		
		return $this->db->count_all_results();
	}
	
	function getComunicadosHeader ()
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM comunicados
				WHERE ativo = 'S'
				ORDER BY data_hora DESC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function getEmps ()
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT * 
				FROM empreendimentos e, cidades c, estados s
				WHERE e.id_cidade = c.id_cidade
				AND c.id_estado = s.id_estado
				AND e.ativo = 'S'
				ORDER BY e.id_empreendimento DESC 
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function getComunicados ($offset = 0)
	{		
		$this->db->flush_cache();
		$this->db->select('*')->from('comunicados')->order_by('data_hora', 'DESC')->limit(50, $offset);
		
		return $this->db->get();
	}
	
	function buscaComunicados ($keyword)
	{
		$id_comunicado = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		
		$this->db->select('*')->from('comunicados')->like('titulo', $keyword)->or_like('descricao', $keyword);
		
		return $this->db->get();
	}
	
	function getComunicado ($id_comunicado)
	{		
		$where = array ('id_comunicado' => $id_comunicado);		
		$this->db->select('*')->from('comunicados')->where($where);

		$query = $this->db->get();			
		return $query->row();
	}
	
	function getUser($id_comunicado)
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM comunicados
				WHERE id_comunicado = '$id_comunicado'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();		
	}
	
	function setComunicado ($data, $id_comunicado = "")
	{		
		if ($id_comunicado)
		{
			$where = array ('id_comunicado' => $id_comunicado);
			$this->db->select('*')->from('comunicados')->where($where);
			
			if ( ! $this->db->count_all_results())
			{
				throw new Exception('Acesso negado.');
			}
			else
			{
				$this->session->set_flashdata('resposta', 'Comunicado salvo com sucesso!');
				
				$this->load->library('image_lib');
				
				//UPLOAD
		        $config['upload_path']		= './assets/manager/uploads/comunicados/';
		        $config['allowed_types']	= 'gif|jpg|png';
		        $config['max_size']			= '15000';
		        $config['max_width']		= '12000';
		        $config['max_height']		= '12000';
		        $config['encrypt_name']     = TRUE;
		        
		        $this->load->library('upload', $config);
		        
		        if ($this->upload->do_upload('imagem_destaque'))
		        {
					$imagem = $this->model->getComunicado($id_comunicado);
					if($imagem->imagem_destaque):
						unlink('./assets/manager/uploads/comunicados/' . $imagem->imagem_destaque);
					endif;
		        	
		        	$file_data = $this->upload->data();
					
			        $file_name = $file_data['file_name'];
			        $file_size = $file_data['file_size'];
					
			        // THUMB
					$config['image_library']	= 'GD2';
					$config['source_image']	    = $file_data['full_path'];
					$config['new_image']		= './assets/manager/uploads/comunicados/' . $file_name;
					$config['maintain_ratio']	= TRUE;
					$config['height']			= 600;
					$config['width']			= 800;
					$config['quality']			= 100;					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
																	
							
					$this->db->set($data);
					$this->db->set('imagem_destaque', $file_name);
	                $this->db->where('id_comunicado', $id_comunicado);
	                $this->db->update('comunicados');
	                
	                $this->session->set_flashdata('resposta', 'Comunicado salvo com sucesso!');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "comunicados";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                
		        }
		        else
		        {
					$this->db->set($data);
	                $this->db->where('id_comunicado', $id_comunicado);
	                $this->db->update('comunicados');
	                
		            //Log Acesso
		            	$acao 		= "update";
		            	$tabela 	= "comunicados";
		            	$sql 		= $this->db->last_query();
		            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
		            //Log Acesso 	                	        	
		        	
					$this->session->set_flashdata('resposta', 'Comunicado inserido sem imagem!');
		        }
			}
		}
		else
		{
			$this->load->library('image_lib');
			
			//UPLOAD
	        $config['upload_path']		= './assets/manager/uploads/comunicados/';
	        $config['allowed_types']	= 'gif|jpg|png';
	        $config['max_size']			= '15000';
	        $config['max_width']		= '12000';
	        $config['max_height']		= '12000';
	        $config['encrypt_name']     = TRUE;
	        
	        $this->load->library('upload', $config);
	        
	        if ($this->upload->do_upload('imagem_destaque'))
	        {
	        	$file_data = $this->upload->data();
				
		        $file_name = $file_data['file_name'];
		        $file_size = $file_data['file_size'];
		        
		        // THUMB
				$config['image_library']	= 'GD2';
				$config['source_image']	    = $file_data['full_path'];
				$config['new_image']		= './assets/manager/uploads/comunicados/' . $file_name;
				$config['maintain_ratio']	= TRUE;
				$config['height']			= 600;
				$config['width']			= 800;
				$config['quality']			= 100;					
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				
						
				$this->db->set($data)->set('imagem_destaque', $file_name)->insert('comunicados');
				
	            //Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "comunicados";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 
		            				
				return $this->db->insert_id();
	        }
	        else
	        {
				$this->db->set($data)->insert('comunicados');
	            
				//Log Acesso
	            	$acao 		= "insert";
	            	$tabela 	= "comunicados";
	            	$sql 		= $this->db->last_query();
	            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	            //Log Acesso 				
				
				return $this->db->insert_id();	        	
	        	
				$this->session->set_flashdata('resposta', 'Comunicado inserido sem imagem!');
	        }
		}
	}
	
	function delComunicado ($id_comunicado)
	{		
		$where = array ('id_comunicado' => $id_comunicado);
		$this->db->select('*')->from('comunicados')->where($where);
		
		if ( ! $this->db->count_all_results())
		{
			throw new Exception('Acesso negado.');
		}
		else
		{
            $this->db->where('id_comunicado', $id_comunicado);
            $this->db->delete('comunicados');
            
	        //Log Acesso
	        	$acao 		= "delete";
	        	$tabela 	= "comunicados";
	        	$sql 		= $this->db->last_query();
	        	$this->model->inserirLogAcoes($tabela, $acao, $sql);
	        //Log Acesso             
		}
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/contatos_model.php */