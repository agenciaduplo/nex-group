<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Grupos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert($data) {
        $this->db->insert('grupos', $data);
        return $this->db->insert_id();
    }

    function update($data, $id) {
        $this->db->update('grupos', $data, "id_grupo = '{$id}'");
    }

    function delete($id) {
        $this->db->delete('grupos', "id_grupo = '{$id}'");
    }

    function getGrupo($id) {
        $sql = "SELECT grupos.id_grupo AS id, grupos.nome, grupos.logomarca, grupos.`status` FROM grupos WHERE grupos.id_grupo = '{$id}' LIMIT 1";
        return $this->db->query($sql)->row();
    }

    function deleteGrupo($id) {

        $sql = "SELECT id_empreendimento FROM grupos_empreendimentos WHERE id_grupo = " . $id;

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
            foreach ($query->result() as $row) { // deleta todos empreendimentos do grupo
                $this->db->where('id_empreendimento', $row->id_empreendimento);
                if (!$this->db->delete('empreendimentos'))
                    return false;
            }

        $this->delete($id); // deleta o próprio grupo

        /* log */
        $acao = "delete";
        $tabela = "grupos_empreendimentos";
        $sql = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
        // endlog  

        return true;
    }

    function inserirLogAcoes($tabela, $acao, $sql) {
        $id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

        $data = array(
            'data_hora' => date('Y-m-d H:i:s'),
            'id_usuario' => $id_usuario,
            'tabela' => $tabela,
            'acao' => $acao,
            'sql' => $sql,
            'ip' => $this->input->ip_address()
        );

        $this->db->set($data)->insert('logs_acoes');
        return $this->db->insert_id();
    }

}
