<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arquivos_model extends CI_Model {
	
	function __construct() { parent::__construct(); }

	function getArquivos(){
		$sql = "SELECT arquivos_padrao.id, arquivos_padrao.nome, arquivos_padrao.filename, arquivos_padrao.ctime FROM arquivos_padrao ORDER BY arquivos_padrao.ctime DESC";
		return $this->db->query($sql)->result();
	}
	function getArquivo($id){
		$sql = "SELECT arquivos_padrao.id, arquivos_padrao.filename, arquivos_padrao.arquivo FROM arquivos_padrao WHERE arquivos_padrao.id = '{$id}' LIMIT 1";
		return $this->db->query($sql)->row();
	}
	function insert($dados){
		$this->db->insert('arquivos_padrao',$dados);
	}
	function delete($id){
		$sql = "DELETE FROM `arquivos_padrao` WHERE (`id`='{$id}') LIMIT 1";
		$this->db->query($sql);
	}
}