<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model{

	function __construct(){
    // Call the Model constructor
    parent::__construct();
  }

  function inserirLogAcoes ($tabela, $acao, $sql){
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$data = array(
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'  => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		  => $sql,
			'ip'			    => $this->input->ip_address()
		);

		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================

	function getUltimoComunicado(){
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM comunicados
				WHERE ativo = 'S'
				AND id_empreendimento = 0
				ORDER BY data_hora DESC
				LIMIT 1
			   ";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getUltimoLogin(){
		$this->db->flush_cache();

		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		$sql = "SELECT *
				FROM logs_acessos
				WHERE id_usuario = $id_usuario
				ORDER BY data_hora DESC
				LIMIT 1, 1
			   ";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getComunicadosHeader(){
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM comunicados
				WHERE ativo = 'S'
				ORDER BY data_hora DESC
			   ";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function verificaTermos(){
		$this->db->flush_cache();

		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$sql = "SELECT *
				FROM usuarios
				WHERE id_usuario = $id_usuario
			   ";

		$query = $this->db->query($sql);
		return $query->row();
	}


	function verificaLogin($login){
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM usuarios
				WHERE login = '$login'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();		
	}
	
	function getEmps_bk ()
	{
		$this->db->flush_cache();
		
		if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
			$ativo = '';
		else:
			$ativo = "AND e.ativo = 'S'";
		endif;	
	
		$sql = "SELECT * 
				FROM empreendimentos e, cidades c, estados s
				WHERE e.id_cidade = c.id_cidade
				AND c.id_estado = s.id_estado
				$ativo
				ORDER BY e.data_ordem DESC 
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function buscaArquivosZip ($id_tipo_arquivo)
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM arquivos
				WHERE id_tipo_arquivo = $id_tipo_arquivo
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function setUsuario($data)
	{
		$this->db->set($data)->insert('usuarios');
	}

	function updateSenha($idUser, $novaSenha)
	{
		$this->db->set('senha', $novaSenha);
        $this->db->where('id_usuario', $idUser);
		$this->db->update('usuarios');
	}
	
	function getUpdateArquivos ()
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM arquivos a, empreendimentos e, empresas p, tipos_arquivos tp
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND a.id_tipo_arquivo = tp.id_tipo_arquivo
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC 
				LIMIT 5
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getUpdateEmmps ()
	{
		$this->db->flush_cache();

		$sql = "SELECT *
				FROM apartamentos a, empreendimentos e, empresas p
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC
				LIMIT 5
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function numUpdateDisponibilidades ()
	{		
		$this->db->select('*')->from('apartamentos');
		
		return $this->db->count_all_results();
	}
	
	function getUpdateDisponibilidades ($offset = 0)
	{
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}

		$sql = "SELECT *
				FROM apartamentos a, empreendimentos e, empresas p
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC
				LIMIT $offset, 10
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function buscaUpdateDisponibilidades ($keyword)
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT *
				FROM apartamentos a, empreendimentos e, empresas p
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND e.nome LIKE '%$keyword%'
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC
				LIMIT 50
				";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	//arquivos
	function numUpdateArquivos ()
	{		
		$this->db->select('*')->from('arquivos');
		
		return $this->db->count_all_results();
	}
	
	function getUpdateArquivosList ($offset = 0)
	{
		$this->db->flush_cache();

		if($offset)
		{
			
		}
		else {
			$offset = 0;
		}

		$sql = "SELECT * 
				FROM arquivos a, empreendimentos e, empresas p, tipos_arquivos tp
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND a.id_tipo_arquivo = tp.id_tipo_arquivo
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC 
				LIMIT $offset, 10
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();		
	}
	
	function buscaUpdateArquivos ($keyword)
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM arquivos a, empreendimentos e, empresas p, tipos_arquivos tp
				WHERE a.id_empreendimento = e.id_empreendimento
				AND e.id_empresa = p.id_empresa
				AND a.id_tipo_arquivo = tp.id_tipo_arquivo
				AND tp.tipo LIKE '%$keyword%'
				AND e.ativo = 'S'
				ORDER BY a.data_hora DESC 
				LIMIT 50
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function buscaUnidades ($id_empreendimento)
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM apartamentos
				WHERE id_empreendimento = $id_empreendimento
				ORDER BY apartamento ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function update_ordem ($id_apartamento, $ordem)
	{
		$this->db->set('ordem', $ordem);
        $this->db->where('id_apartamento', $id_apartamento);
        $this->db->update('apartamentos');
	}
	
	/* Grupos */
	
	function getGrupo($id)
	{
		$sql = "SELECT grupos.id_grupo AS id, grupos.nome, grupos.logomarca, grupos.`status` AS ativo FROM grupos WHERE grupos.id_grupo = '{$id}' LIMIT 1";
		return $this->db->query($sql)->row();
	}
	
	function getGrupoEmp($id)
	{
		$sql = "SELECT empreendimentos.id_empreendimento AS id, empreendimentos.nome, empreendimentos.caracterizacao, empreendimentos.descricao, empreendimentos.entrega, empreendimentos.endereco, empreendimentos.logomarca, empreendimentos.ativo, cidades.cidade, estados.uf FROM empreendimentos INNER JOIN grupos_empreendimentos ON empreendimentos.id_empreendimento = grupos_empreendimentos.id_empreendimento LEFT OUTER JOIN cidades ON empreendimentos.id_cidade = cidades.id_cidade LEFT OUTER JOIN estados ON cidades.id_estado = estados.id_estado WHERE grupos_empreendimentos.id_grupo = '{$id}' ORDER BY empreendimentos.data_ordem DESC";
		return $this->db->query($sql)->result();
	}
	
	function getEmps($idcidade="",$idestado=""){
    $cidade = ($idcidade != "") ? " AND emp.id_cidade = {$idcidade} " : "";
    $estado = ($idestado != "") ? " AND estados.id_estado = {$idestado} " : "";
		$sql = "
			(	
				SELECT
				emp.id_empreendimento AS id,
				emp.nome,
				emp.caracterizacao,
				CONCAT(emp.nome,' - ',emp.caracterizacao,' - ',emp.descricao,' - ',emp.entrega,' - ',emp.endereco,' - ',cidades.cidade,' - ',estados.uf) AS titulo,
				emp.logomarca,
				emp.ativo,
				CONCAT('0') AS is_grupo
				FROM
				empreendimentos AS emp
				LEFT OUTER JOIN cidades ON emp.id_cidade = cidades.id_cidade
				LEFT OUTER JOIN estados ON cidades.id_estado = estados.id_estado
				LEFT OUTER JOIN grupos_empreendimentos ON emp.id_empreendimento = grupos_empreendimentos.id_empreendimento
				WHERE
				grupos_empreendimentos.id_grupo IS NULL
        {$cidade}
        {$estado}
				GROUP BY
				emp.id_empreendimento
				ORDER BY
				emp.nome ASC
			) UNION (
				SELECT
				grupos.id_grupo AS id,
				grupos.nome,
				CONCAT('Pasta') AS caracterizacao,
				CONCAT('') AS titulo,
				grupos.logomarca,
				grupos.`status` AS ativo,
				CONCAT('1') AS is_grupo
				FROM
				grupos
				ORDER BY
				grupos.nome ASC
			) ORDER BY titulo ASC
		";
		
		return $this->db->query($sql)->result();
	}
	
	//===========================================================================================
	// FUNÇOES PROGRAMAÇÃO SEMANAL
	//===========================================================================================
	
	function getEmpsProgramacao ()
	{
		$sql = "
			(	
				SELECT
				emp.id_empreendimento AS id,
				emp.nome,
				emp.caracterizacao,
				CONCAT(emp.nome,' - ',emp.caracterizacao,' - ',emp.descricao,' - ',emp.entrega,' - ',emp.endereco,' - ',cidades.cidade,' - ',estados.uf) AS titulo,
				emp.logomarca,
				emp.ativo,
				CONCAT('0') AS is_grupo
				FROM
				empreendimentos AS emp
				LEFT OUTER JOIN cidades ON emp.id_cidade = cidades.id_cidade
				LEFT OUTER JOIN estados ON cidades.id_estado = estados.id_estado
				LEFT OUTER JOIN grupos_empreendimentos ON emp.id_empreendimento = grupos_empreendimentos.id_empreendimento
				WHERE
				grupos_empreendimentos.id_grupo IS NULL
				AND emp.ativo = 'S'
				GROUP BY
				emp.id_empreendimento
				ORDER BY
				emp.data_ordem DESC
			) UNION (
				SELECT
				grupos.id_grupo AS id,
				grupos.nome,
				CONCAT('Pasta') AS caracterizacao,
				CONCAT('') AS titulo,
				grupos.logomarca,
				grupos.`status` AS ativo,
				CONCAT('1') AS is_grupo
				FROM
				grupos
				WHERE
				grupos.status = 1
				ORDER BY
				grupos.nome ASC
			) ORDER BY nome ASC
		";
		
		return $this->db->query($sql)->result();
	}
	
	function getPeriodos ()
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM periodos
				ORDER BY data_cadastro DESC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getUltimaProgramacao ()
	{
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM periodos
				ORDER BY data_cadastro DESC
				LIMIT 1
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getUltimaProgramacaoPeriodo ($id_periodo)
	{
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM periodos
				WHERE id_periodo = $id_periodo
				ORDER BY data_cadastro DESC
				LIMIT 1
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getPeriodo ($id_periodo)
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM periodos
				WHERE id_periodo = $id_periodo
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getTiposProgramacoes ()
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM tipos_programacoes
				ORDER BY tipo_programacao ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function getProgramacao ($id_programacao)
	{		
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM programacoes
				WHERE id_programacao = $id_programacao
				ORDER BY ordem ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function getProgramacoes ($id_periodo, $id_empreendimento, $folder)
	{
		$this->db->flush_cache();		
		
		$sql = "SELECT * 
				FROM programacoes p, tipos_programacoes tp
				WHERE p.id_periodo = $id_periodo
				AND p.id_emp_or_pasta = $id_empreendimento
				AND p.id_tipo_programacao = tp.id_tipo_programacao
				AND p.emp_or_pasta = '$folder'
				ORDER BY p.ordem ASC
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	
	function setPeriodo ($data)
	{
		$this->db->set($data)->insert('periodos');
		return $this->db->insert_id();
	}
	
	function setProgramacao ($data, $id_programacao = "")
	{
		$this->load->library('image_lib');
				
		//UPLOAD
        $config['upload_path']		= './assets/manager/uploads/programacoes/';
        $config['allowed_types']	= 'gif|jpg|png';
        $config['max_size']			= '15000';
        $config['max_width']		= '12000';
        $config['max_height']		= '12000';
        $config['encrypt_name']     = TRUE;
        
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('arquivo'))
        {	
        	$file_data = $this->upload->data();
			
	        $file_name = $file_data['file_name'];
	        $file_size = $file_data['file_size'];
			
	        // THUMB
			$config['image_library']	= 'GD2';
			$config['source_image']	    = $file_data['full_path'];
			$config['new_image']		= './assets/manager/uploads/programacoes/' . $file_name;
			$config['maintain_ratio']	= TRUE;
			$config['height']			= 600;
			$config['width']			= 800;
			$config['quality']			= 100;					
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
															
					

			$this->db->set('arquivo', $file_name);
			
			if($id_programacao)
	        {
		    	$prog = $this->getProgramacao($id_programacao);    
		    	if($prog->arquivo):
		    		unlink("./assets/manager/uploads/programacoes/$prog->arquivo");
		    	endif;
	        }
            
            //Log Acesso
            /*
            	$acao 		= "insert";
            	$tabela 	= "programacoes";
            	$sql 		= $this->db->last_query();
            	$this->model->inserirLogAcoes($tabela, $acao, $sql);
            */
            //Log Acesso 	                
        }
        
        if($id_programacao)
        {
	    	$this->db->set($data)->where('id_programacao', $id_programacao)->update('programacoes');
	      	return $this->db->insert_id();	  
        }
        else
        {
	    	$this->db->set($data)->insert('programacoes');
	      	return $this->db->insert_id();	  
        }
        

	}
	
	function updatePeriodo ($data, $id_periodo)
	{
		$this->db->set($data);
        $this->db->where('id_periodo', $id_periodo);
        $this->db->update('periodos');
	}
	
	function delProgramacao ($id_programacao)
	{
		$this->db->where('id_programacao', $id_programacao);
        $this->db->delete('programacoes');
	}
	
	//===========================================================================================
	// FUNÇOES PROGRAMAÇÃO SEMANAL
	//===========================================================================================


  function buscaTodosImoveis(){
    $db_con = $this->load->database('nexgroup', TRUE);

    $sql = "SELECT
              *,
              e.id_status AS status_geral,
              (SELECT group_concat(dormitorios.dormitorio) FROM emps_dormitorios em LEFT JOIN dormitorios ON dormitorios.id_dormitorio = em.id_dormitorio WHERE em.id_empreendimento = e.id_empreendimento) as dormitorios
            FROM
              empreendimentos e,
              empresas ep,
              cidades c,
              estados es,
              tipos_imoveis tp,
              status s,
              fases f
            WHERE
              e.id_empresa = ep.id_empresa
            AND
              e.id_cidade = c.id_cidade
            AND
              c.id_estado = es.id_estado
            AND
              e.id_tipo_imovel = tp.id_tipo_imovel
            AND
              e.id_status = s.id_status
            AND
              f.id_empreendimento = e.id_empreendimento
            AND
              e.ativo = 'S'
            GROUP BY
              e.id_empreendimento";

    $query = $db_con->query($sql);
    $this->db->close();

    return $query->result();
  }


  function getIdCidadesAtivas(){
    $sql = "SELECT
             e.id_cidade,
             c.cidade AS cidade
            FROM
              empreendimentos AS e
            INNER JOIN
              cidades AS c
            ON
              e.id_cidade = c.id_cidade
            WHERE
              e.ativo = 'S'
            GROUP BY
              e.id_cidade";
    return $this->db->query($sql)->result();
  }

  function getIdEstadosAtivos($list_idcidade){
    $sql = "SELECT
             c.id_estado AS id_estado,
             e.estado AS estado
            FROM
              cidades AS c
            INNER JOIN
              estados AS e
            ON
              e.id_estado = c.id_estado
            WHERE
              c.id_cidade IN ($list_idcidade)

            GROUP BY
              e.id_estado";
    return $this->db->query($sql)->result();
  }
}