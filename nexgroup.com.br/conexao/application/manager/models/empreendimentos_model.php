<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empreendimentos_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function inserirLogAcoes($tabela, $acao, $sql) {
        $id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

        $data = array(
            'data_hora' => date('Y-m-d H:i:s'),
            'id_usuario' => $id_usuario,
            'tabela' => $tabela,
            'acao' => $acao,
            'sql' => $sql,
            'ip' => $this->input->ip_address()
        );

        $this->db->set($data)->insert('logs_acoes');
        return $this->db->insert_id();
    }

    //=======================================================================
    //Outras Funções=========================================================
    //=======================================================================

    function verificaLogin($login) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM usuarios
				WHERE login = '$login'
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function getUltimoLogin() {
        $this->db->flush_cache();

        $id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
        $sql = "SELECT *
				FROM logs_acessos
				WHERE id_usuario = $id_usuario
				ORDER BY data_hora DESC
				LIMIT 1, 1
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function getComunicadosHeader() {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM comunicados
				WHERE ativo = 'S'
				ORDER BY data_hora DESC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getComunicadoEmp($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM comunicados
				WHERE ativo = 'S'
				AND id_empreendimento = $id_empreendimento
				ORDER BY data_hora DESC
				LIMIT 1
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function getEmps() {
        $this->db->flush_cache();

        if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'):
            $ativo = '';
        else:
            $ativo = "AND e.ativo = 'S'";
        endif;

        $sql = "SELECT * 
				FROM empreendimentos e, cidades c, estados s
				WHERE e.id_cidade = c.id_cidade
				AND c.id_estado = s.id_estado
				$ativo
				ORDER BY e.data_ordem DESC 
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getCategorias() {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM categorias
				WHERE id_categoria <> 4
				ORDER BY categoria ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getEstados() {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM estados
				WHERE 1=1
				ORDER BY estado ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getEstado($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM empreendimentos e, cidades c, estados s
				WHERE e.id_empreendimento = $id_empreendimento
				AND c.id_estado = s.id_estado
				AND c.id_cidade = e.id_cidade
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function getGrupos() {
        $sql = "SELECT grupos.id_grupo AS id, grupos.nome FROM grupos ORDER BY nome";
        return $this->db->query($sql)->result();
    }

    function getCidades($id_estado) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM cidades
				WHERE id_estado = $id_estado
				ORDER BY cidade ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getEmpresas() {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM empresas
				WHERE 1=1
				ORDER BY empresa ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getEmp($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT
				emp.*,
				grupo_emp.id_grupo AS grupo
				FROM
				empreendimentos AS emp
				LEFT OUTER JOIN grupos_empreendimentos AS grupo_emp ON emp.id_empreendimento = grupo_emp.id_empreendimento
				WHERE
				emp.id_empreendimento = $id_empreendimento
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function getArquivos($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM arquivos a, tipos_arquivos tp
				WHERE a.id_empreendimento = $id_empreendimento
				AND a.id_tipo_arquivo = tp.id_tipo_arquivo
				ORDER BY a.data_hora DESC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getTiposArquivos() {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM tipos_arquivos
				WHERE 1=1
				ORDER BY tipo ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getTipoArquivo($id_tipo_arquivo) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM tipos_arquivos
				WHERE id_tipo_arquivo = $id_tipo_arquivo
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function setEmpreendimento($data, $id_empreendimento = "") {
        if ($id_empreendimento) {
            $where = array('id_empreendimento' => $id_empreendimento);
            $this->db->select('*')->from('empreendimentos')->where($where);

            if (!$this->db->count_all_results()) {
                throw new Exception('Acesso negado.');
            } else {
                $this->session->set_flashdata('resposta', 'Empreendimento salvo com sucesso!');

                $this->load->library('image_lib');

                //UPLOAD
                $config['upload_path'] = './assets/manager/uploads/logos/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '15000';
                $config['max_width'] = '12000';
                $config['max_height'] = '12000';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('logomarca')) {
                    $imagem = $this->model->getEmp($id_empreendimento);
                    if ($imagem->logomarca):
                        unlink('./assets/manager/uploads/logos/' . $imagem->logomarca);
                    endif;

                    $file_data = $this->upload->data();

                    $file_name = $file_data['file_name'];
                    $file_size = $file_data['file_size'];

                    // THUMB
                    $config['image_library'] = 'GD2';
                    $config['source_image'] = $file_data['full_path'];
                    $config['new_image'] = './assets/manager/uploads/logos/' . $file_name;
                    $config['maintain_ratio'] = TRUE;
                    $config['height'] = 84;
                    $config['width'] = 150;
                    $config['quality'] = 100;
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();


                    $this->db->set($data);
                    $this->db->set('logomarca', $file_name);
                    $this->db->where('id_empreendimento', $id_empreendimento);
                    $this->db->update('empreendimentos');

                    $this->session->set_flashdata('resposta', 'Empreendimento salvo com sucesso!');

                    //Log Acesso
                    $acao = "update";
                    $tabela = "empreendimentos";
                    $sql = $this->db->last_query();
                    $this->model->inserirLogAcoes($tabela, $acao, $sql);
                    //Log Acesso 					
                }
                else {
                    $this->db->set($data);
                    $this->db->where('id_empreendimento', $id_empreendimento);
                    $this->db->update('empreendimentos');

                    //Log Acesso
                    $acao = "update";
                    $tabela = "empreendimentos";
                    $sql = $this->db->last_query();
                    $this->model->inserirLogAcoes($tabela, $acao, $sql);
                    //Log Acesso 									

                    $this->session->set_flashdata('resposta', 'Empreendimento inserido sem imagem!');
                }
            }
        } else {
            $this->load->library('image_lib');

            //UPLOAD
            $config['upload_path'] = './assets/manager/uploads/logos/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '15000';
            $config['max_width'] = '12000';
            $config['max_height'] = '12000';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('logomarca')) {
                $file_data = $this->upload->data();

                $file_name = $file_data['file_name'];
                $file_size = $file_data['file_size'];

                // THUMB
                $config['image_library'] = 'GD2';
                $config['source_image'] = $file_data['full_path'];
                $config['new_image'] = './assets/manager/uploads/logos/' . $file_name;
                $config['maintain_ratio'] = TRUE;
                $config['height'] = 84;
                $config['width'] = 150;
                $config['quality'] = 100;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();


                $this->db->set($data)->set('logomarca', $file_name)->insert('empreendimentos');
                $id = $this->db->insert_id();
                //Log Acesso
                $acao = "insert";
                $tabela = "empreendimentos";
                $sql = $this->db->last_query();
                $this->model->inserirLogAcoes($tabela, $acao, $sql);
                //Log Acesso 

                return $id;
            } else {
                $this->db->set($data)->insert('empreendimentos');
                $id = $this->db->insert_id();
                //Log Acesso
                $acao = "insert";
                $tabela = "empreendimentos";
                $sql = $this->db->last_query();
                $this->model->inserirLogAcoes($tabela, $acao, $sql);
                //Log Acesso 				

                return $id;

                $this->session->set_flashdata('resposta', 'Empreendimento inserido sem imagem!');
            }
        }
    }

    function setArquivo($data, $id_arquivo = "", $nomeFile) {
        if ($id_arquivo) {
            $where = array('id_arquivo' => $id_arquivo);
            $this->db->select('*')->from('arquivos')->where($where);

            if (!$this->db->count_all_results()) {
                throw new Exception('Acesso negado.');
            } else {
                $config['upload_path'] = './assets/manager/uploads/arquivos';
                $config['allowed_types'] = 'pdf|PDF||xls|XLS|doc|docx|jpg|png|gif|bmp|zip|rar|cdr|jpeg|txt|avi|mpg|mpeg|wmv|mov';
                $config['max_size'] = '100000';
                $config['file_name'] = $nomeFile;

                $this->load->library('upload', $config);

                $file_data = $this->upload->data();
                //$file_name = $file_data['file_name'];	
                $file_name = $nomeFile;
                $file_name = $file_name . $file_data['file_ext'];

                if (!$this->upload->do_upload('arquivo')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('resposta', $error);

                    $this->db->set($data);
                    $this->db->where('id_arquivo', $id_arquivo);
                    $this->db->update('arquivos');
                } else {
                    $arquivoDel = $this->model->getArquivo($id_arquivo);
                    if ($arquivoDel->arquivo):
                        unlink('./assets/manager/uploads/arquivos/' . $arquivoDel->arquivo);
                    endif;

                    $file_data = $this->upload->data();
                    //$file_name 	= $file_data['file_name'];				
                    $file_name = $nomeFile;
                    $file_name = $file_name . $file_data['file_ext'];

                    $dataResp = array('upload_data' => $this->upload->data());
                    $this->session->set_flashdata('resposta', $dataResp);

                    $this->db->set($data);
                    $this->db->set('arquivo', $file_name);
                    $this->db->where('id_arquivo', $id_arquivo);
                    $this->db->update('arquivos');
                }

                //Log Acesso
                $acao = "update";
                $tabela = "arquivos";
                $sql = $this->db->last_query();
                $this->model->inserirLogAcoes($tabela, $acao, $sql);
                //Log Acesso				  
            }
        }
        else {
            $config['upload_path'] = './assets/manager/uploads/arquivos';
            $config['allowed_types'] = 'pdf|PDF|xls|XLS|doc|docx|jpg|png|gif|bmp|zip|rar|cdr|jpeg|txt|avi|mpg|mpeg|wmv|mov';
            $config['max_size'] = '100000';
            $config['file_name'] = $nomeFile;

            $this->load->library('upload', $config);

            $file_data = $this->upload->data();
            //$file_name = $file_data['file_name'];				
            $file_name = $nomeFile;
            $file_name = $file_name . $file_data['file_ext'];

            if (!$this->upload->do_upload('arquivo')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('resposta', $error);
            } else {
                $file_data = $this->upload->data();
                //$file_name 	= $file_data['file_name'];
                $file_name = $nomeFile;
                $file_name = $file_name . $file_data['file_ext'];

                $dataArchive = array('upload_data' => $this->upload->data());
                $this->session->set_flashdata('resposta', $dataArchive);

                $this->db->set('arquivo', $file_name);
            }
            $this->db->set($data)->insert('arquivos');
            $id = $this->db->insert_id();
            //Log Acesso
            $acao = "insert";
            $tabela = "arquivos";
            $sql = $this->db->last_query();
            $this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso  			

            return $id;
        }
    }

    function delArquivo($id_arquivo) {
        $arquivo = $this->model->getArquivo($id_arquivo);
        if ($arquivo->arquivo):
            unlink('./assets/manager/uploads/arquivos/' . $arquivo->arquivo);
        endif;

        $this->db->where('id_arquivo', $id_arquivo);
        $this->db->delete('arquivos');

        //Log Acesso
        $acao = "delete";
        $tabela = "arquivos";
        $sql = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso		
    }

    function getArquivo($id_arquivo) {
        $sql = "SELECT *
				FROM arquivos
				WHERE id_arquivo = $id_arquivo
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function setApto($data, $id_apartamento = "") {
        if ($id_apartamento) {
            $where = array('id_apartamento' => $id_apartamento);
            $this->db->select('*')->from('apartamentos')->where($where);

            if (!$this->db->count_all_results()) {
                throw new Exception('Acesso negado.');
            } else {
                $this->session->set_flashdata('resposta', 'Apartamento salvo com sucesso!');

                $this->db->set($data);
                $this->db->where('id_apartamento', $id_apartamento);
                $this->db->update('apartamentos');

                //Log Acesso
                $acao = "update";
                $tabela = "apartamentos";
                $sql = $this->db->last_query();
                $this->model->inserirLogAcoes($tabela, $acao, $sql);
                //Log Acesso				  
            }
        } else {
            $this->db->set($data)->insert('apartamentos');

            //Log Acesso
            $acao = "insert";
            $tabela = "apartamentos";
            $sql = $this->db->last_query();
            $this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 			

            return $this->db->insert_id();
        }
    }

    function getApto($id_apartamento) {
        $sql = "SELECT *
				FROM apartamentos
				WHERE id_apartamento = $id_apartamento
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function delApto($id_apartamento) {
        $where = array('id_apartamento' => $id_apartamento);
        $this->db->select('*')->from('apartamentos')->where($where);

        if (!$this->db->count_all_results()) {
            throw new Exception('Acesso negado.');
        } else {
            $this->db->where('id_apartamento', $id_apartamento);
            $this->db->delete('apartamentos');

            //Log Acesso
            $acao = "delete";
            $tabela = "apartamentos";
            $sql = $this->db->last_query();
            $this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 				
        }
    }

    function getAptosVendidos($id_empreendimento) {
        $this->db->flush_cache();
        $this->db->select('*')->from('apartamentos');
        $this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->where('status', 'vendido');
        return $this->db->count_all_results();
    }

    function getAptosReservados($id_empreendimento) {
        $this->db->flush_cache();
        $this->db->select('*')->from('apartamentos');
        $this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->where('status', 'reservado');
        return $this->db->count_all_results();
    }

    function getAptosLivres($id_empreendimento) {
        $this->db->flush_cache();
        $this->db->select('*')->from('apartamentos');
        $this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->where('status', 'disponivel');
        return $this->db->count_all_results();
    }

    function getAptos($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM apartamentos
				WHERE id_empreendimento = $id_empreendimento
				ORDER BY cast( apartamento AS SIGNED )
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getAptosReservadosLista($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM apartamentos
				WHERE id_empreendimento = $id_empreendimento
				AND status = 'reservado'
				ORDER BY cast( apartamento AS SIGNED )
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getImagens($id_empreendimento) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM imagens
				WHERE id_empreendimento = $id_empreendimento
				ORDER BY data_cadastro ASC
			   ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getImagem($id_imagem) {
        $this->db->flush_cache();

        $sql = "SELECT *
				FROM imagens
				WHERE id_imagem = $id_imagem
			   ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function delImagem($id_imagem) {
        $imagem = $this->model->getImagem($id_imagem);
        if ($imagem->imagem):
            unlink('./assets/manager/uploads/imagens/' . $imagem->imagem);
            unlink('./assets/manager/uploads/imagens/thumbs/' . $imagem->imagem);
        endif;

        $this->db->where('id_imagem', $id_imagem);
        $this->db->delete('imagens');

        //Log Acesso
        $acao = "delete";
        $tabela = "imagens";
        $sql = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso		
    }

    function updateImagem($id_imagem, $legenda) {
        $sql = "UPDATE `imagens` SET `legenda`='{$legenda}' WHERE (`id_imagem`='{$id_imagem}')";
        $this->db->query($sql);
        //Log Acesso
        $acao = "update";
        $tabela = "imagens";
        $sql = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso		
    }

    function setImagem($data, $id_empreedimento) {
        $this->load->library('image_lib');

        $config['upload_path'] = './assets/manager/uploads/imagens/';
        $config['allowed_types'] = 'gif|jpg|png|bmp';
        $config['max_size'] = '5000';
        $config['max_width'] = '4000';
        $config['max_height'] = '4000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('imagem')) {
            $file_data = $this->upload->data();

            $file_name = $file_data['file_name'];
            $file_size = $file_data['file_size'];


            $config['create_thumb'] = FALSE;
            $config['source_image'] = $file_data['full_path'];
            $config['new_image'] = './assets/manager/uploads/imagens/' . $file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1000;
            $config['height'] = 700;
            $config['quality'] = 100;
            $this->image_lib->initialize($config);
            $this->image_lib->resize();


            $config['create_thumb'] = TRUE;
            $config['source_image'] = $file_data['full_path'];
            $config['new_image'] = './assets/manager/uploads/imagens/thumbs/' . $file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 250;
            $config['height'] = 200;
            $config['quality'] = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $this->db->set('imagem', $file_name);
        } else {
            $this->session->set_flashdata('resposta', 'Não foi possivel enviar imagem!');
        }

        $this->db->set($data)->insert('imagens');

        //Log Acesso
        $acao = "insert";
        $tabela = "imagens";
        $sql = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
        //Log Acesso
    }

    //impressao
    function getEmpreendimento($id_empreendimento) {
        $where = array('id_empreendimento' => $id_empreendimento);

        $this->db->start_cache();
        $this->db->select('*')->from('empreendimentos');
        $this->db->join('empresas', 'empresas.id_empresa = empreendimentos.id_empresa');
        $this->db->where($where);
        $this->db->stop_cache();

        if (!$this->db->count_all_results()) {
            throw new Exception('Acesso negado.');
        } else {
            $query = $this->db->get();

            return $query->row();
        }

        $this->db->flush_cache();
    }

    function getReservas($id_empreendimento) {
        $this->db->flush_cache();
        $this->db->select('*')->from('apartamentos');
        $this->db->where('id_empreendimento', $id_empreendimento);
        $this->db->where('status', 'reservado');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_pasta($emp, $pasta) {
        $this->db->delete('grupos_empreendimentos', "id_empreendimento = '{$emp}'");
        $this->db->insert('grupos_empreendimentos', array(
            'id_empreendimento' => $emp,
            'id_grupo' => $pasta
        ));
    }

    function remove_pasta($id) {
        $this->db->delete('grupos_empreendimentos', "id_empreendimento = '{$id}'");
    }

    function delete_empreendimento($id_empreendimento) {
        $where = array('id_empreendimento' => $id_empreendimento);
        $this->db->select('*')->from('empreendimentos')->where($where);

        if (!$this->db->count_all_results()) {
            return false;
        } else {
            $this->db->where('id_empreendimento', $id_empreendimento);
            if (!$this->db->delete('empreendimentos'))
                return false;

            //Log Acesso
            $acao = "delete";
            $tabela = "empreendimentos";
            $sql = $this->db->last_query();
            $this->model->inserirLogAcoes($tabela, $acao, $sql);
            //Log Acesso 
            return true;
        }
    }

}

/* End of file contatos_model.php */
/* Location: ./system/application/model/home_model.php */