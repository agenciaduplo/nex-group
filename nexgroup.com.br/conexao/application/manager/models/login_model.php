<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function inserirLogAcoes ($tabela, $acao, $sql)
	{
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
		
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		=> $sql,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}
	
	function inserirLogAcesso ($id_usuario)
	{
		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'    => $id_usuario,
			'ip'			=> $this->input->ip_address()
		);
		
		$this->db->set($data)->insert('logs_acessos');
		return $this->db->insert_id();
	}	

	//=======================================================================
	//Outras Funções=========================================================
	//=======================================================================
	
	function verificaLogin($login)
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM usuarios
				WHERE login = '$login'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();		
	}
	
	function verificaUsuario ($idUsuario)
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM usuarios
				WHERE id_usuario = '$idUsuario'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();
	}
	
	function verificaConfirmacao ($id_usuario)
	{
		$this->db->flush_cache();		
	
		$sql = "SELECT *
				FROM usuarios u, links l
				WHERE u.id_usuario = $id_usuario
				AND u.id_usuario = l.id_usuario
				AND l.confirmado = 'S'
			   ";		
		
		$query = $this->db->query($sql);		
		return $query->row();	
	}
	
	function setUsuario($data)
	{
		$this->db->set($data)->insert('usuarios');
		return $this->db->insert_id();
	}
	
	function setLink($data)
	{
		$this->db->set($data)->insert('links');
		return $this->db->insert_id();
	}

	function updateSenha($idUser, $novaSenha)
	{
		$this->db->set('senha', $novaSenha);
        $this->db->where('id_usuario', $idUser);
		$this->db->update('usuarios');
	}
	
	function confirmarEmail ($id_link, $id_usuario)
	{
		$this->db->set('confirmado', "S");
		$this->db->set('data_confirmacao', date('Y-m-d H:i:s'));
		$this->db->where('id_link', $id_link);
        $this->db->where('id_usuario', $id_usuario);
		$this->db->update('links');
	}
	
	function aceitaTermos ($id_usuario, $termos)
	{
		$this->db->set('termos', $termos);
        $this->db->where('id_usuario', $id_usuario);
		$this->db->update('usuarios');
	}
}

/* End of file contatos_model.php */
/* Location: ./system/application/model/login_model.php */