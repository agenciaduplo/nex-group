<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendas_model extends CI_Model{

	function __construct(){
    parent::__construct();
  }

  function dataToDB($data){
    $data = explode("/",$data);
    return $data[2]."-".$data[1]."-".$data[0];
  }

  function getContatos($table="interesses", $data_i, $data_f, $string=""){
  	$db_con = $this->load->database('nexgroup', TRUE);
    $data_i = $this->dataToDB($data_i);
    $data_f = $this->dataToDB($data_f);
    $id     = ($table == "atendimentos") ? "id_atendimento" : "id_interesse";
    $comm   = ($table == "atendimentos") ? "descricao"      : "comentarios";
    $where_s= ($string == "") ? "" : " AND (t.email LIKE '%".$string."%' OR t.nome LIKE '%".$string."%') ";

    $sql    = "SELECT
                t.{$id},
                t.data_envio,
                t.nome,
                t.email,
                t.telefone,
                t.url AS camp,
                t.origem,
                e.empreendimento,
                t.{$comm},
                t.email_enviado_for
               FROM
                {$table} AS t
               INNER JOIN
                 empreendimentos AS e
               ON
                 t.id_empreendimento = e.id_empreendimento
               WHERE
                 t.data_envio >= '".$data_i." 00:00:00' AND t.data_envio <= '".$data_f." 23:59:59'
               {$where_s}
               ORDER BY
                t.{$id}
               DESC";

  	return $db_con->query($sql)->result();
  }

  function getContato($idcontato,$table){
    $db_con = $this->load->database('nexgroup', TRUE);
    $id     = ($table == "a") ? "id_atendimento" : "id_interesse";
    $comm   = ($table == "a") ? "descricao"      : "comentarios";
    $table  = ($table == "a") ? "atendimentos"   : "interesses";

    $sql = "SELECT
              t.{$id},
              t.data_envio,
              t.nome,
              t.email,
              t.telefone,
              t.url AS camp,
              t.origem,
              e.empreendimento,
              t.{$comm},
              t.email_enviado_for
            FROM
              {$table} AS t
            INNER JOIN
              empreendimentos AS e
            ON
              t.id_empreendimento = e.id_empreendimento
            WHERE
              {$id} = {$idcontato}
            LIMIT 1";
    return $db_con->query($sql)->result();
  }

  function cadastraDelegacao($data){
    if($data['idcontato_delegado'] != ""){
      $where = array('idcontato_delegado' => $data['idcontato_delegado']);
      $this->db->select('*')->from('contato_delegado')->where($where);

      if(!$this->db->count_all_results()){
        throw new Exception('Acesso negado.');
      }else{
        $idcontato_delegado = $this->getIdContatoDelegado($data['idcontato'],$data['tabela']);
        if($idcontato_delegado){
          $this->ContatoStatusExcByIdDelegate($idcontato_delegado);
        }

        $this->db->set($data);
        $this->db->where('idcontato_delegado', $data['idcontato_delegado']);
        $this->db->update('contato_delegado');

       //Log Acesso
        $acao     = "update";
        $tabela   = "contato_delegado";
        $sql    = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso
      }
    }else{
      $this->db->set($data)->insert('contato_delegado');

      //Log Acesso
        $acao     = "insert";
        $tabela   = "contato_delegado";
        $sql    = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso

      return $this->db->insert_id();
    }
  }

  function inserirLogAcoes($tabela, $acao, $sql){
    $this->load->library('encrypt');
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

		$data = array (
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'  => $id_usuario,
			'tabela'    	=> $tabela,
			'acao'    		=> $acao,
			'sql'    		  => $sql,
			'ip'			    => $this->input->ip_address()
		);

		$this->db->set($data)->insert('logs_acoes');
		return $this->db->insert_id();
	}


	function inserirLogAcesso($id_usuario){
    $this->load->library('encrypt');
		$data = array(
			'data_hora'  	=> date('Y-m-d H:i:s'),
			'id_usuario'  => $id_usuario,
			'ip'			    => $this->input->ip_address()
		);

		$this->db->set($data)->insert('logs_acessos');
		return $this->db->insert_id();
	}

  function getDelegacoes($table,$contato){
    $total = count($contato);
    $ret   = "";
    $id    = ($table == "a") ? "id_atendimento" : "id_interesse";

    for($i=0;$i<$total;++$i){
      $sql = "SELECT
              ad.idcontato_delegado,
              ad.idusuario,
              u.login AS email,
              u.nome AS nome
            FROM
              contato_delegado AS ad
            INNER JOIN
              usuarios AS u
            ON
              ad.idusuario = u.id_usuario
            WHERE
              ad.idcontato = {$contato[$i]->{$id}}
            AND
              ad.tabela = '{$table}'
            LIMIT 1";
      $ret[$contato[$i]->{$id}] = $this->db->query($sql)->result();
    }

    return $ret;
  }

  function getByIdAtendimentoDelegado($idcontato_delegado){
    $sql = "SELECT * FROM contato_delegado WHERE idcontato_delegado = {$idcontato_delegado} LIMIT 1";
    return $this->db->query($sql)->result();
  }

  function getContatosCorretor($table="a"){
    $this->load->library('encrypt');

    $idusuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

    $sql = "SELECT * FROM contato_delegado WHERE idusuario = {$idusuario} AND tabela = '{$table}' ORDER BY idcontato_delegado DESC";
    $ret = $this->db->query($sql)->result();
    $tot = count($ret);

    if($tot > 0){
      $db_con       = $this->load->database('nexgroup', TRUE);
      $id_tb        = ($table == "a") ? "id_atendimento"  : "id_interesse";
      $comm         = ($table == "a") ? "descricao"       : "comentarios";
      $table        = ($table == "a") ? "atendimentos"    : "interesses";
      $ids_contato  = "";

      for($i=0;$i<$tot;++$i){
        $pontuacao   = ($tot == ($i+1)) ? "" : ",";
        $ids_contato.= $ret[$i]->idcontato.$pontuacao;
      }

      $sql = "SELECT
                t.{$id_tb},
                t.data_envio,
                t.nome,
                t.email,
                t.telefone,
                t.{$comm},
                e.empreendimento
              FROM
                {$table} AS t
              INNER JOIN
                empreendimentos AS e
              ON
                t.id_empreendimento = e.id_empreendimento
              WHERE
                {$id_tb} IN (".$ids_contato.")";

      $ret = $db_con->query($sql)->result();

      return $ret;
    }else{
      return 0;
    }
  }


  function getIdContatoDelegado($idcontato,$table,$idusuario=""){
    $and = ($idusuario != "") ? " idusuario = {$idusuario} " : "";
    $sql = "SELECT idcontato_delegado FROM contato_delegado WHERE idcontato = {$idcontato} AND tabela = '{$table}' {$and} LIMIT 1";
    $ret = $this->db->query($sql)->result();

    return $ret[0]->idcontato_delegado;
  }

  function getContatoStatus($idcontato_delegado="",$idcontato="",$table=""){
    $idcontato_delegado = ($idcontato_delegado != "") ? $idcontato_delegado : $this->getIdContatoDelegado($idcontato,$table);

    $sql = "SELECT
              cs.*,
              s.nome AS status
            FROM
              contato_status AS cs
            INNER JOIN
              contato_deleg_status AS s
            ON
              cs.idstatus = s.idcontato_deleg_status
            WHERE
              cs.idcontato_delegado = {$idcontato_delegado}
            ORDER BY
              cs.data_hr ASC";
    return $this->db->query($sql)->result();
  }

  function getStatus(){
    $sql = "SELECT * FROM contato_deleg_status ORDER BY idcontato_deleg_status ASC";
    return $this->db->query($sql)->result();
  }

  function cadastraContatoStatus($data){
    if($data['idcontato_status'] != ""){
      $where = array('idcontato_status' => $data['idcontato_status']);
      $this->db->select('*')->from('contato_status')->where($where);

      if(!$this->db->count_all_results()){
        throw new Exception('Acesso negado.');
      }else{
        $this->db->set($data);
        $this->db->where('idcontato_status', $data['idcontato_status']);
        $this->db->update('contato_status');

       //Log Acesso
        $acao     = "update";
        $tabela   = "contato_status";
        $sql    = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso
      }
    }else{
      $this->db->set($data)->insert('contato_status');

      //Log Acesso
        $acao     = "insert";
        $tabela   = "contato_status";
        $sql    = $this->db->last_query();
        $this->model->inserirLogAcoes($tabela, $acao, $sql);
      //Log Acesso

      return $this->db->insert_id();
    }
  }

  function ContatoStatusExc($idcontato_status){
    $sql = "DELETE FROM contato_status WHERE idcontato_status = {$idcontato_status} LIMIT 1";
    $this->db->query($sql);
  }

  function ContatoStatusExcByIdDelegate($idcontato_delegado){
    $sql = "DELETE FROM contato_status WHERE idcontato_delegado = {$idcontato_delegado}";
    $this->db->query($sql);
  }

  function verifyContactsForUser($table,$array_cont){
    $this->load->library('encrypt');
    $id         = ($table == "a") ? "id_atendimento" : "id_interesse";
    $id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);

    $total  = count($array_cont);
    $find_a = "";

    for($i=0;$i<$total;++$i){
      $sql = "SELECT * FROM contato_delegado WHERE idcontato = {$array_cont[$i]->{$id}} AND tabela = '{$table}' AND idusuario = {$id_usuario}";
      $ret = $this->db->query($sql)->result();
      if(is_array($ret) && count($ret) > 0){
        $find_a[] = $array_cont[$i];
      }
    }

    return $find_a;
  }


  function getIdUsuarioDelegate($idcontato,$table){
    $sql = "SELECT idusuario FROM contato_delegado WHERE idcontato = {$idcontato} AND tabela = '{$table}' LIMIT 1";
    $ret = $this->db->query($sql)->result();

    return $ret[0]->idusuario;
  }
}

/* End of file vendas_model.php */
/* Location: ./system/application/model/vendas_model.php */