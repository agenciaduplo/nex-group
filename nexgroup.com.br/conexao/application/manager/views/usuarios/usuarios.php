<?=$this->load->view('includes/header');?>

<div id="usuario-container">

	<div id="usuario-box">
 		<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Usuários</p>
    	<div id="usuario-top">
        	<h2>Usuários</h2>
        	<?php if(@$buscar == "S"): ?>
        	
        		<p class="usuario-total">Resultados para '<?=@$termo?>'</p>
        	
        	<?php else: ?>
        	
            	<p class="usuario-total">Usuários Cadastrados: <?=@$cadastrados?></p>
        
        	<?php endif; ?>
        </div>
        
        <form id="FormBusca" method="POST">
        	<input type="hidden" name="buscar" value="S">
	        <div id="usuario-in">
	            <input id="keyword" name="keyword" type="text" class="validate[required] usuario-categoria">
	            <a href="javascript: EnviarBusca();">Procurar</a>
	        </div>
	    </form>

		<div id="usuario-table">
        
            <div id="usuario-add-link">
            	<a id="modal-cadastro-abrir-update" class="admin-add" href="<?=site_url()?>usuarios/cadastrar_usuario">Adicionar um Usuário</a>
            </div>
        	<?php if ($usuarios->num_rows): ?>
            <table cellspacing="10" class="tables">
                <tr>
                    <th class="usuario-table-name-1">Nome</th>
                    <th class="usuario-table-name-2">Login (E-mail)</th>
                    <th class="usuario-table-name-3">Tipo</th>
                    <th class="usuario-table-name-4"></th>
                </tr>
                <?php foreach ($usuarios->result() as $row):?>
                <tr bgcolor="#9e9e9e">
                    <th class="usuario-table-1"><?=$row->nome?></th>
                    <th class="usuario-table-2"><a style="color:#fff; text-decoration:none"><?=$row->login?></a></th>
                    <th class="usuario-table-3"><?=$row->tipo?></th>
                    <th class="usuario-table-4"><a id="modal-cadastro-abrir-update" href="<?=site_url()?>usuarios/editar/<?=$row->id_usuario?>" class="usuario-admin-edit">Editar</a><a href="<?=site_url()?>usuarios/apagar/<?=$row->id_usuario?>" class="usuario-admin-excluir">Excluir</a></th>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <?php else: ?>
            	<h3>Nenhum usuário encontrado.</h3>
            <?php endif; ?>
        
        </div>
        <!--
        <div id="usuario-numera"><p>1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">6</a> <a href="">7</a></p></div>
		-->
		<div class="">
			<a href="<?=site_url(); ?>usuarios/exportar">Exportar em CSV</a>
		</div>
		<?php if(@$buscar != "S"): ?>
		<div id="usuario-numera"><?=@$paginacao?></div>
        <?php endif; ?>
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>