<?=$this->load->view('includes/header');?>


<!-- Selecione empreendimento --> 
<?=$this->load->view('tpl/outros-empreendimentos'); ?>

<!-- Empreendimento INFO --> 
<div id="inter-container">
	<div id="inter-box">
		<div class="visualizarEmpreendimento">
			<p><a href="<?=site_url(); ?>home">Inicio</a> > Cadastrar grupo</p>
		</div>
		<form id="FormEmpreendimento" name="FormEmpreendimento" method="post" enctype="multipart/form-data" action="<?=current_url(); ?>">
			<div id="editarEmp" class="ediarEmpreendimento" style="display:block;">

				<div id="inter-img-edit">
					<span class="imgLogo">&nbsp;</span>
					<p>Escolher arquivo da logomarca:</p>
					
				  	<div style="background: none;">
				  		<input id="FileLogoEmp" style="cursor:pointer" name="logomarca" type="file" value="Procurar" />
						<p class="inter-img-edit-p">(Arquivo com fundo branco ou transparente)</p>
				  	</div>
				
				</div>
		   
				<div id="inter-info-edit" style="display:block;">
					<ul>
						<li>
							<label for="emp_nome">Nome</label>
							<input id="emp_nome" name="form_nome" type="text" value="" class="validate[required] categoria-emp-edit">
						</li>
						<li>
						   <label for="emp_ativo">Ativo</label>
							<select name="form_ativo" id="ativo">
								<option value="0">Não</option>
								<option value="1">Sim</option>
							</select>
						</li>
					</ul>
					<div id="inter-info-edit-a" style="margin-bottom: 30px; width: 400px;">
						<a href="javascript: AtualizarEmpreendimento();" style="text-decoration:none"><div class="admin-btn-salvar"><p>Salvar</p></div></a>
						<a href="<?=site_url()?>" style="text-decoration:none"><div class="admin-btn-cancelar"><p>Cancelar</p></div></a>
					</div>
				</div>
 
			</div>
		</form>
  </div>
</div>


<?=$this->load->view('includes/footer');?>