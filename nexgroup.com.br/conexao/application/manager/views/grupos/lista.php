<?php
if (!$is_admin && $grupo->ativo == 0) {
    show_404();
}

$this->load->view('includes/header');
?>

<!-- Selecione empreendimento --> 
<?= $this->load->view('tpl/outros-empreendimentos'); ?>

<div id="emp-container">
    <div id="emp-box">
        <div class="visualizarEmpreendimento">
            <br /><br />
            <p><a href="<?= site_url(); ?>home">Inicio</a> > Pasta <?= $grupo->nome; ?></p>

            <div id="inter-img">
                <span><img src="<?= base_url(); ?>assets/manager/uploads/logos/<?= $grupo->logomarca; ?>" width="120" alt="<?= $grupo->nome; ?>" /></span>
            </div>

            <div id="inter-info">
                <p><?= $grupo->nome; ?></p>
                <span>Pasta</span>
                <p>
                    <?php if ($is_admin) { ?>
                        <a href="<?= site_url(); ?>grupos/editar/<?= $grupo->id; ?>" class="admin-edit">Editar Pasta</a>
                        <!--<a href="',site_url(),'grupos/remover/',$grupo->id,'" onclick="javascript:confirm(\'Você realmente quer remover este grupo?\');" class="admin-excluir">Excluir Pasta</a>-->
                        <a href="javascript:;;" class="admin-edit" onclick="ExcluirGrupoEmpreendimentos(<?= $grupo->id; ?>);">Excluir Pasta</a>
                    <?php } ?> 
                </p>
            </div> 
        </div>
        <div id="emp-in">
            <h1></h1>
            <ul id="tooltipNex">
                <?php
                if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador') {
                    echo '
						<a href="', site_url(), 'empreendimentos/cadastro?gid=', $grupo->id, '">
							<li class="ad-emp">
								<img class="img-ad" src="', base_url(), 'assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" />
								<p>Adicionar Empreendimento</p>
							</li>
						</a>
					';
                }

                if (sizeof($emps) > 0) {
                    foreach ($emps as $rows) {
                        if ($is_admin or $rows->ativo == 'S') {
                            $title = $rows->nome;
                            if (!empty($rows->caracterizacao)) {
                                $title .= ' - ' . $rows->caracterizacao;
                            }
                            if (!empty($rows->entrega)) {
                                $title .= ' - ' . $rows->entrega;
                            }
                            if (!empty($rows->descricao)) {
                                $title .= ' - ' . $rows->descricao;
                            }
                            if (!empty($rows->endereco)) {
                                $title .= ' - ' . $rows->endereco;
                            }
                            if (!empty($rows->cidade)) {
                                $title .= ' - ' . $rows->cidade;
                            }
                            if (!empty($rows->uf)) {
                                $title .= ' - ' . $rows->uf;
                            }

                            echo '
								<a href="', site_url(), 'empreendimentos/', $rows->id, '" title="', $title, '" style="cursor:pointer">
									<li>
										<img src="', base_url(), 'assets/manager/uploads/logos/', $rows->logomarca, '" width="120" alt="', $rows->nome, '" />
										<div class="bug">
											', $rows->ativo == "N" ? '<strong>OFF</strong>' : $rows->caracterizacao, '
										</div>
									</li>
								</a>
							';
                        }
                    }
                }
                ?>
                <div id="fechar-emp-div" style="display:none"></div>

            </ul>
        </div>
    </div>
</div>


<?= $this->load->view('includes/footer'); ?>