<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="pt-br" class=" no-js">
	
	<head>
		
		<title><?=$empreendimento->nome?></title>
				
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link type="text/css" href="<?=base_url()?>assets/manager/css/print/print.css" media="screen" rel="stylesheet" />
		<link type="text/css" href="<?=base_url()?>assets/manager/css/print/print.css" media="print" rel="stylesheet" />
		<!--[if IE 7 ]>    <link type="text/css" href="<?=base_url()?>assets/manager/css/print/ie7.css" media="print" rel="stylesheet" /> <![endif]-->
		<!--[if IE 7 ]>    <link type="text/css" href="<?=base_url()?>assets/manager/css/print/ie7.css" media="screen" rel="stylesheet" /> <![endif]-->
		<!--[if IE 8 ]>    <link type="text/css" href="<?=base_url()?>assets/manager/css/print/ie8.css" media="print" rel="stylesheet" /> <![endif]-->
		<!--[if IE 9 ]>    <link type="text/css" href="<?=base_url()?>assets/manager/css/print/ie9.css" media="print" rel="stylesheet" /> <![endif]-->

	</head>

	<body>
    	
    	<div id="logo">
        	<?php if(@$empreendimento->logomarca): ?>
				<img width="150" class="emp" src="<?=base_url()?>assets/manager/uploads/logos/<?=$empreendimento->logomarca?>" >
			<?php endif;?>
        	
        	<img width="150" class="nex" align="right" src="<?=base_url()?>assets/manager/img/logo-nex-group.jpg" />
        
        </div>
        
		<table cellspacing="0" style="border-bottom:1px solid #E4E4E4;">
			<tr id="identificacao">
				<td colspan="4"  width="750" class="borda"><div class="imagem"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><span>Impressão realizada em <strong><?=date('d/m/Y')?></strong> às <strong><?=date('H:i:s')?></strong> por <?=$_SESSION['conexao']['nex_login_nome'];?></span></td>
			</tr>
			<tr width="750" id="endereco">
				<td colspan="4"> <div class="endereco"><strong><?=$empreendimento->nome?> </strong><?=$empreendimento->endereco?></div></td>
			</tr>
			<tr width="750" class="disponibilidade">
				<td class="borda"><div class="imagem num_vendidos"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><span>Vendidos <strong><?=$vendidos?></strong></span></td>
				<td class="borda"><div class="imagem num_reservados"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><span>Reservados <strong><?=$reservados?></strong></span></td>
				<td class="borda"><div class="imagem num_livres"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><span>Livres <strong><?=$livres?></strong></span></td>
				<td class="borda exception"><div class="imagem num_total"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><span>Total <strong><?=count($aptos)?></strong></span></td>
			</tr>
			<tr width="750" class="legenda">
				<td colspan="4" width="700px"class="borda"><div class="imagem"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><img src="<?=base_url()?>assets/manager/img/print/legenda.png" /></td>
			</tr>
		
			<?php if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
				<tr class="reservados">
					<td colspan="4"><div class="imagem"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><strong>Reservados</strong><hr /></td>
				</tr>
				<tr class="reservas">
					<td width="75px">Apartamento</td>
					<td width="415px">Quem</td>
					<td width="150px">Imobiliária</td>
					<td width="75px">Data</td>
				</tr>
				<?php foreach ($reservas as $row):
					if(mb_strlen($row->quem, 'utf8') > 150){
				?>
				<tr class="reserva" >
					<td width="75px"><div class="imagem apto"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span class=""><?=$row->apartamento?></span></td>
					<td width="415px"><div class="imagem quem"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span class="exception"><?=$row->quem?></span></td>
					<td width="150px"><div class="imagem imob"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span class="">&nbsp;<?=$row->imobiliaria?></span></td>
					<td width="75px"><div class="imagem data"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span class=""><?=date('d/m/Y',strtotime($row->data_hora))?></span></td>
				</tr>
				<?php } else { ?>
				<tr bgcolor="#FFFFF;" class="reserva" valign="top">
					<td width="75px"><div class="imagem apto"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span><?=$row->apartamento?></span></td>
					<td width="415px"><div class="imagem quem"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span><?=$row->quem?></span></td>
					<td width="150px"><div class="imagem imob"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span>&nbsp;<?=$row->imobiliaria?></span></td>
					<td width="75px"><div class="imagem data"><img src="<?=base_url()?>assets/manager/img/print/bg_vendido.png" /></div><span><?=date('d/m/Y',strtotime($row->data_hora))?></span></td>
				</tr>
				<?php } endforeach; ?>
			
				<tr class="branca">
					<td colspan="4">&nbsp;</td>
				</tr>
			<? endif;?>
		</table>
		
		<div class="" id="dispo-inter-list" style="color: #00000;">
			<ul>
			<?php
			$ordem = 'not';
			foreach ($aptos as $row){
    				if($row->status == 'reservado')
    					$cor = "yell";
    				elseif($row->status == 'vendido')
    					$cor = "red";
    				elseif($row->status == 'disponivel')
    					$cor = "green";
    				else {
    					$cor = "yell";
    				}

    			if($ordem != @$row->ordem){
    				if($ordem == 'not'){
    					echo '</ul><span class="dispo-clear"></span><ul>';
    					$ordem = @$row->ordem;
    				} else {
    					echo '</ul><span class="dispo-break"></span><ul>';
    					$ordem = @$row->ordem;
    				}
    			}
    			echo '<li class="list-',@$cor,'" title="',@$row->apartamento,'"><img src="',base_url(),'assets/manager/img/print/bg_',str_replace(array('disponivel','revenda'),array('livre','reservado'),$row->status),'.jpg" width="5" height="10" />&nbsp;',@$row->apartamento,'</li>';
			}
			?>
			</ul>
		</div>
		
		
		<table class=" footer" cellspacing="0">
		<tbody>
		<tr class="legenda">
		<td colspan="4" width="750px"class="borda"><div class="imagem"><img src="<?=base_url()?>assets/manager/img/print/bg.png" /></div><img src="<?=base_url()?>assets/manager/img/print/legenda.png" /></td>
		</tr>
		</tbody>
		</table>
</body>
</html>
