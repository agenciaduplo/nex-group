<?= $this->load->view('includes/header'); ?>

<!-- Selecione empreendimento --> 
<?= $this->load->view('tpl/outros-empreendimentos'); ?>

<!-- Empreendimento INFO --> 
<div id="inter-container">
    <div id="inter-box">

        <div class="visualizarEmpreendimento">
            <p><a href="<?= site_url() ?>home">Inicio</a>   >  <?= @$emp->nome ?></p>

            <div id="inter-img">
                <span><img src="<?= base_url() ?>assets/manager/uploads/logos/<?= @$emp->logomarca ?>" width="120" alt="<?= @$row->nome ?>" /></span>
            </div>

            <div id="inter-info">
                <p><?= @$emp->nome ?></p>
                <span><?= @$emp->endereco ?></span>
                <p>
                    <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>	   	
                        <a href="javascript: EditarEmpreendimento();" class="admin-edit">Editar Empreendimento</a>
                        <a href="javascript: ExcluirEmpreendimento(<?= @$emp->id_empreendimento ?>);" class="admin-excluir">Excluir Empreendimento</a>
                        <!-- <a href="" class="admin-excluir">Excluir Empreendimento</a> -->
                    <?php endif; ?>   
                </p>
            </div> 
        </div>

        <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
            <!-- editar emp -->
            <form id="FormEmpreendimento" name="FormEmpreendimento" method="post" enctype="multipart/form-data" action="<?= site_url() ?>empreendimentos/editar_empreendimento">
                <input type="hidden" name="id_empreendimento" value="<?= @$emp->id_empreendimento ?>" />
                <div id="editarEmp" class="ediarEmpreendimento" style="display:none;">
                    <p><a href="<?= site_url() ?>home">Inicio</a>   >  <a href="javascript: VisualizarEmpreendimento();"><?= @$emp->nome ?></a>   >  Editar Empreendimento</p>

                    <div id="inter-img-edit">
                        <span class="imgLogo"><img src="<?= base_url() ?>assets/manager/uploads/logos/<?= @$emp->logomarca ?>" width="120" alt="<?= @$row->nome ?>" /></span>
                        <p>Escolher arquivo da logomarca:</p>

                        <div style="background: none;">
                            <input id="FileLogoEmp" style="cursor:pointer" name="logomarca" type="file" value="Procurar" />
                            <p class="inter-img-edit-p">(Arquivo com fundo branco ou transparente)</p>
                        </div>

                    </div>

                    <div id="inter-info-edit" style="display:block;">
                        <ul>
                            <li>
                                <label for="emp_nome">Nome</label>
                                <input id="emp_nome" name="emp_nome" type="text" value="<?= @$emp->nome ?>" class="categoria-emp-edit">
                            </li>
                            <li>
                                <label for="descricao">Descrição</label>
                                <input id="descricao" name="descricao" type="text" value="<?= @$emp->descricao ?>" class="categoria-emp-edit">
                            </li>
                            <li>
                                <label for="caracterizacao">Caract.</label>
                                <input id="caracterizacao" name="caracterizacao" type="text" value="<?= @$emp->caracterizacao ?>" class="categoria-emp-edit">
                            </li>
                            <li>
                                <label for="entrega">Entrega</label>
                                <input id="entrega" name="entrega" type="text" value="<?= @$emp->entrega ?>" class="categoria-emp-edit">
                            </li>

                            <li>
                                <label for="emp_end">Estado</label>
                                <select name="estado" id="estado" onchange="return trocaCidades(this.value);">
                                    <?php foreach (@$estados as $row): ?>
                                        <option <?php if (@$row->id_estado == @$id_estado) echo "selected='selected'"; ?> value="<?= $row->id_estado ?>"><?= @$row->uf ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li>
                                <label for="emp_end">Cidade</label>
                                <select name="cidade" id="cidades">
                                    <?php if (@$cidades): ?>
                                        <?php foreach (@$cidades as $row): ?>
                                            <option <?php if (@$emp->id_cidade == @$row->id_cidade) echo "selected='selected'"; ?> value="<?= $row->id_cidade ?>"><?= $row->cidade ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </li>
                            <li>
                                <label for="emp_end">Endereço</label>
                                <input id="emp_end" name="emp_end" type="text" value="<?= @$emp->endereco ?>" class="categoria-emp-edit">
                            </li>
                            <li>
                                <label for="emp_empresa">Empresa</label>
                                <select name="emp_empresa" id="emp_empresa">
                                    <?php foreach (@$empresas as $row): ?>
                                        <option <?php if ($row->id_empresa == $emp->id_empresa) echo "selected='selected'" ?> value="<?= $row->id_empresa ?>"><?= @$row->empresa ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li>
                                <label for="emp_end">Mostrar tabela?</label>
                                <select name="mostra_tabela" id="mostra_tabela">
                                    <option <?php if (@$emp->mostra_tabela == "S") echo "selected='selected'"; ?> value="S">Sim</option>
                                    <option <?php if (@$emp->mostra_tabela == "N") echo "selected='selected'"; ?> value="N">Não</option>
                                </select>
                            </li>
                            <li>
                                <label for="emp_ativo">Ativo</label>
                                <select name="ativo" id="ativo">
                                    <option <?php if (@$emp->ativo == "N") echo "selected='selected'"; ?> value="N">Não</option>
                                    <option <?php if (@$emp->ativo == "S") echo "selected='selected'"; ?> value="S">Sim</option>
                                </select>
                            </li>
                            <li>
                                <label for="pastas">Pasta</label>
                                <select name="pastas" id="pastas">
                                    <option value="0">Sem pasta</option>
                                    <?php
                                    if (isset($grupos) && $grupos > 0) {
                                        foreach ($grupos as $rows) {
                                            if ($rows->id == $emp->grupo) {
                                                echo '<option value="', $rows->id, '" selected="selected">', $rows->nome, '</option>';
                                            } else {
                                                echo '<option value="', $rows->id, '">', $rows->nome, '</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </li>
                            <li>
                                <label for="datapicker">Data Ordem</label>
                                <input id="datapicker" name="data_ordem" type="text" value="<?= date('d/m/Y', strtotime(@$emp->data_ordem)) ?>" class="categoria-emp-edit" style="width: 120px;">
                            </li>
                        </ul>
                        <div id="inter-info-edit-a" style="width:400px;">
                            <a href="javascript: AtualizarEmpreendimento();" style="text-decoration:none"><div class="admin-btn-salvar"><p>Salvar</p></div></a>
                            <a href="javascript: VisualizarEmpreendimento();" style="text-decoration:none"><div class="admin-btn-cancelar"><p>Cancelar</p></div></a>
                        </div>
                    </div>


                </div>
            </form>
            <!-- ADIMIN --> 
        <?php endif; ?> 

        <div id="btn-categorias">
            <div style="cursor:pointer" id="btn-arquivos" class="btn-cat-on"><p>Arquivos</p></div>
            <?php if (@$emp->mostra_tabela == "S"): ?>
                <div style="cursor:pointer" id="btn-dispo" class="btn-cat-off"><p>Vejas as disponibilidades</p></div>
            <?php endif; ?>
            <div style="cursor:pointer" id="btn-imagens" class="btn-cat-off"><p>Imagens</p></div>
        </div>

        <a class="empClick" href="#btn-arquivos"></a>

    </div>
</div>

<div class="mantemHeight">

    <div id="arquivos-container" class="Some" style="display:table">
        <div id="arquivos-box">

            <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?> 
                <form id="FormArquivos" name="FormArquivos" method="post" enctype="multipart/form-data" action="<?= site_url() ?>empreendimentos/salvar_arquivo" >
                    <input type="hidden" name="id_empreendimento" value="<?= @$emp->id_empreendimento ?>" />

                    <div id="admin-ad-arquivos" style="height: 260px;">
                        <div id="admin-ad-arquivos-nome">
                            <p class="admin-add">Adicionar Arquivo</p>
                        </div>
                        <div style="margin-top: 20px; margin-left: 30px;">
                            <p>Descrição do arquivo (max 100 caracteres)</p>
                            <input type="text" name="descricao" style="padding: 10px; width: 296px; margin-top: 10px;" maxlength="100" />
                        </div>
                        <div id="admin-ad-arquivos-1">
                            <p>Tipo de arquivo</p>
                            <select id="select-admin-1" name="id_tipo_arquivo">
                                <?php foreach (@$tipos_arquivos as $row): ?>
                                    <option value="<?= $row->id_tipo_arquivo ?>"><?= $row->tipo ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div id="admin-ad-arquivos-2">
                            <p>Arquivo</p>
                            <input type="file" style="cursor:pointer" id="arquivo-procurar" name="arquivo" />
                        </div>
                        <a href="javascript: EnviarArquivo();" style="text-decoration:none">
                            <div class="admin-btn-salvar"><p>Salvar</p></div>
                        </a>
                    </div>
                </form>
            <?php endif; ?>

            <div id="arquivos-catego">
                <?php if (@$arquivos): ?>
                    <ul>
                        <?php foreach ($arquivos as $row): ?>
                            <li style="min-height: 100px; padding-bottom: 10px; height: auto; ">
                                <div style="float: left; width: 65%; ">
                                    <img src="<?=base_url()?>assets/manager/uploads/tipos_arquivos/<?=@$row->icone?>" />
                                    <p>
                                        <?=@$row->tipo?>
                                        <?php if ($row->id_tipo_arquivo == 7) { ?> > <a target="_blank" href="<?= $row->url ?>" style="font-weight:normal; color:black;">Assista o vídeo</a> <?php } ?>
                                    </p>

                                    <span><?=@$data = date('d/m/Y H:i:s', strtotime($row->data_hora)); ?></span>

                                    <?php if($row->descricao) : ?>
                                    
                                    <div style="float: left; clear: both; margin-left: 75px;"><i><?=$row->descricao?></i></div>
                                    
                                    <?php endif; ?>

                                </div>

                                <div style="float: right; width: 35%;">
                                    <div id="arquivos-catego-in">
                                        <a href="<?=site_url()?>empreendimentos/download/<?=@$row->id_arquivo?>">Baixar</a>
                                    </div>

                                    <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?> 
                                        <a onclick="return confirm('Tem certeza que deseja excluir o arquivo?');" href="<?= site_url() ?>empreendimentos/apagar_arquivo/<?= @$row->id_arquivo ?>/<?= @$emp->id_empreendimento ?>" class="admin-excluir" style="float: right; margin-top: 15px;">Excluir</a>
                                    <?php endif; ?>
                                </div>
                                

                            </li>
                        <?php endforeach; ?>				   

                    </ul>
                <?php else: ?>
                    <h3>Nenhum arquivo encontrado</h3>
                <?php endif; ?>
            </div>

        </div>
    </div>	


    <?php if (@$emp->mostra_tabela == "S"): ?>  
        <div id="dispo-inter-container" class="Some" style="display:none">
            <div id="dispo-inter-box">

                <!-- ADIMIN --> 
                <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
                    <form id="FormDisponibilidades" name="FormDisponibilidades" method="post" action="<?= site_url() ?>empreendimentos/salvar_apto">
                        <input type="hidden" name="id_empreendimento" value="<?= @$emp->id_empreendimento ?>" />	
                        <div id="admin-ad-dispo">
                            <div id="admin-ad-dispo-nome">
                                <p class="admin-add">Adicionar Unidades</p>
                            </div>
                            <ul class="ad-dispo-left">
                                <li>
                                    <label for="apartamento">Unidade</label>
                                    <input id="apartamento" name="apartamento" type="text" class="validate[required] categoria">
                                </li>
                                <li>
                                    <label for="select-admin-2">Status</label>
                                    <select id="select-admin-3" name="status">
                                        <option value="vendido">Vendido</option>
                                        <option value="disponivel">Disponível</option>
                                        <option value="reservado">Reservado</option>
                                        <option value="revenda">Revenda</option>
                                    </select>
                                </li>
                                <li>
                                    <label for="quem" class="categoria1">Para quem?</label>
                                    <input id="quem" name="quem" type="text" class="categoria">
                                </li>
                            </ul>
                            <ul class="ad-dispo-right">
                                <li>
                                    <label for="imobiliaria">Imobiliária</label>
                                    <input id="imobiliaria" name="imobiliaria" type="text" class="categoria">
                                </li>
                                <li>
                                    <label for="datapicker">Data</label>
                                    <input id="datapicker" name="data_cadastro" type="text" class="categoria">
                                </li>

                                <a href="javascript: EnviarDisponibilidade();" style="text-decoration:none">
                                    <div class="admin-btn-salvar"><p>Salvar</p></div>
                                </a>

                            </ul>
                        </div>
                    </form>
                <?php endif; ?>

                <?php
                if (@$aptos):
                    $url = base_url() . 'empreendimentos/impressao/' . $emp->id_empreendimento
                    ?>

                    <div id="btn-imprimir"><a href="javascript: imprimir('<?= @$url ?>')">Imprimir Disponibilidades</a></div>

                <?php endif; ?>
                <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
                    <?php if (@$reservadosLista): ?>
                        <div id="dispo-in">
                            <p>Apartamento<span style="margin-left:24px">Quem</span><span style="margin-left:290px">Imobiliária</span><span style="margin-left:230px">Data e hora</span></p>
                            <?php foreach (@$reservadosLista as $row): ?>
                                <ul>
                                    <li style="width:100px"><?= $row->apartamento ?></li>
                                    <li style="width:320px"><?= $row->quem ?></li>
                                    <li style="width:290px"><?= @$row->imobiliaria ?></li>
                                    <li style="width:209px; margin-right:0"><?= date('d/m/y H:i', strtotime($row->data_hora)) ?></li>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

                <p>Total de <?= count(@$aptos) ?> unidades</p>	
                <div id="dispo-inter-info">
                    <ul>
                        <li class="right30"><img src="<?= base_url() ?>assets/manager/img/conexao-ico-apto-red.png" width="40" height="40" /><p>APARTAMENTOS VENDIDOS <span>(<?= @$vendidos ?>)</span></p></li>
                        <li class="right30"><img src="<?= base_url() ?>assets/manager/img/conexao-ico-apto-green.png" width="40" height="40" /><p>APARTAMENTOS LIVRES <span>(<?= @$livres ?>)</span></p></li>
                        <li><img src="<?= base_url() ?>assets/manager/img/conexao-ico-apto-yell.png" width="40" height="40" /><p>APARTAMENTOS RESERVADOS <span>(<?= @$reservados ?>)</span></p></li>
                    </ul> 
                </div>	
                <div id="dispo-inter-list">
                    <?php $ordem = 'not';
                    if (@$aptos): ?>
                        <ul>
                            <?php foreach (@$aptos as $row): ?>
                                <?php
                                if ($row->status == 'reservado')
                                    $cor = "yell";
                                elseif ($row->status == 'vendido')
                                    $cor = "red";
                                elseif ($row->status == 'disponivel')
                                    $cor = "green";

                                if ($ordem != @$row->ordem) {
                                    if ($ordem == 'not') {
                                        $ordem = @$row->ordem;
                                    } else {
                                        echo '</ul><span class="dispo-break"></span><ul>';
                                        $ordem = @$row->ordem;
                                    }
                                }
                                ?>
                                <li class="list-<?= @$cor ?>" title="<?= @$row->apartamento ?>"><p><?= @$row->apartamento ?></p><?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?><p class="admin-edit-aptos"><a href="<?= site_url() ?>empreendimentos/editar_apto/<?= $row->id_apartamento ?>" title="Editar Unidade" id="modal_edit_disponibilidade" class="admin-edit-aptos-a">Editar</a> | <a onclick="return confirm('Tem certeza que deseja excluir a unidade?');" href="<?= site_url() ?>empreendimentos/apagar_apto/<?= @$row->id_apartamento ?>/<?= @$emp->id_empreendimento ?>" title="Excluir Unidade" class="admin-edit-aptos-a">Excluir</a></p><?php endif; ?></li>
                        <?php endforeach; ?>	
                        </ul>
                    <?php else: ?>
                        <h3>Nenhuma unidade cadastrada.</h3>
    <?php endif; ?>

                </div>  
                <div id="mar-bt"></div>

            </div>
        </div>
<?php endif; ?>

    <div id="imagens-container" class="Some" style="display:none">
        <div id="imagens-box">

            <!-- ADIMIN --> 
<?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
                <form id="FormImagem" name="FormImagem" method="post" enctype="multipart/form-data" action="<?= site_url() ?>empreendimentos/enviar_imagem">
                    <input type="hidden" name="id_empreendimento" value="<?= @$emp->id_empreendimento ?>" />
                    <div id="admin-ad-arquivosNew">
                        <div id="admin-ad-arquivos-nome">
                            <p class="admin-add">Adicionar Imagens</p>
                        </div>
                        <div id="admin-ad-arquivos-1">
                            <p>Legenda:</p>
                            <input id="apartamento" name="legenda" type="text" class="validate[required] categoria">
                        </div>
                        <div id="admin-ad-arquivos-2">
                            <p>Arquivo:</p>
                            <input id="arquivo-procurar-img" style="cursor:pointer" name="imagem" type="file" class="categoria-img">
                        </div>
                        <a href="javascript: EnviarImagem();" style="text-decoration:none">
                            <div class="admin-btn-salvar"><p>Salvar</p></div>
                        </a>

                    </div>

                    <br /><br /><br />
                </form>
            <?php endif; ?>	

<?php if (@$imagens): ?>
                <div id="imagens-thumbs">

                    <ul>
    <?php foreach ($imagens as $row): ?>
                            <li>
                                <div style="width: 152px; height: 102px; overflow: hidden; margin: 20px auto 5px auto; display:block;">
                                    <img style="margin: 0px; left: 0; top: 0;" src="<?= base_url() ?>assets/manager/uploads/imagens/thumbs/<?= @$row->imagem ?>" width="150" />
                                </div>
                                <div style="text-align: center;">&nbsp;<?= @$row->legenda; ?>&nbsp;</div><br />
                                <div id="imagens-thumbs-baixar"><a href="<?= site_url() ?>empreendimentos/download_imagem/<?= $row->id_imagem ?>">Baixar</a></div>
        <?php if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
                                    <div id="admin-imagens-edit">
                                        <a href="<?= site_url() ?>empreendimentos/atualizar_legenda/<?= $row->id_imagem ?>/<?= $row->id_empreendimento ?>" class="admin-edit modal-open">Editar</a>
                                        <a href="<?= site_url() ?>empreendimentos/excluir_imagem/<?= $row->id_imagem ?>/<?= $row->id_empreendimento ?>" onclick="return confirm('Tem certeza que deseja excluir a imagem?');" class="admin-excluir">Excluir</a>
                                    </div>
                            <?php endif; ?>
                            </li>
    <?php endforeach; ?>
                    </ul>
                </div>
<?php endif; ?>

        </div>
    </div>

</div>

<?php if (@$comunicadoEmp->id_comunicado): ?>
    <?php if (@$ultimoLogin->data_hora < $comunicadoEmp->data_hora): ?>

        <?php if (@$comunicadoEmp->imagem_destaque != "" && $comunicadoEmp->tipo == "I"): ?>

            <a title="<?= @$emp->nome ?> - Comunicado" href="<?= base_url() ?>assets/manager/uploads/comunicados/<?= $comunicadoEmp->imagem_destaque ?>" rel="fancybox" id="AbreComunicadoEmp"></a>

        <?php elseif (@$comunicadoEmp->tipo == "T"): ?>

            <a title="<?= @$emp->nome ?> - Comunicado" href="<?= site_url() ?>comunicados/visualizar/<?= @$comunicadoEmp->id_comunicado ?>" rel="fancybox" id="AbreComunicadoEmp"></a>

        <?php endif; ?>

    <?php endif; ?>
<?php endif; ?>


<?= $this->load->view('includes/footer'); ?>