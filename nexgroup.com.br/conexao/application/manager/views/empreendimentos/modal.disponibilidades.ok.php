<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		
		<script>
			jQuery(document).ready(function()
			{
				/*
				parent.$('#fancybox-content').css({ width: "200px"});
				parent.$('#fancybox-content').css({ height: "200px"});
				parent.$.fancybox.resize();
				
				
				parent.$.fancybox.resize({width: "200px", height: "200px"});
				parent.$.fancybox.resize();
			
				*/
			});
			
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
				parent.window.location.replace("<?=site_url()?>empreendimentos/<?=@$id_empreendimento?>/disponibilidades");
			}
		</script>
		

	
	</head>

	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">
		<!-------------------------------- modal_edit_sucesso ------------------------------------->    
		<div id="modal_edit_sucesso">
		
			<div id="modal_edit-nome">
	        	<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-edit.png" width="43" height="57" />
	        	<p>Editar 'Disponibilidade'</p>
			</div>
	
			<div id="modal_edit-text">
				<p><span>'<?=@$apartamento?>'</span> editado com sucesso.</p>            
			</div>
	        
			<div id="modal-fechar-emp-div"><a href="javascript: FecharFancybox();">Fechar</a></div>
		  
		</div>
	</body>
</html>