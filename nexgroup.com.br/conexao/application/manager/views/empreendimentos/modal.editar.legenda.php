<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		
		<script>
			jQuery(document).ready(function()
			{
				$("select").selectBox();
				$("#datapicker").datepicker({ dateFormat: 'dd/mm/yy' });
			});
			
			function AtualizaEditarDisponibilidades ()
			{
				$("#FormDisponibilidades").submit();
			}
			
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
			}
		</script>
		

	
	</head>

	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">
		<div id="modal_edit_disponibilidade">
			
			<form id="FormDisponibilidades" name="FormDisponibilidades" method="post" action="<?=current_url(); ?>">
				<input type="hidden" name="id_legenda" value="<?=@$img->id_imagem?>" />
	
				<div id="modal_edit-nome">
					<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-edit.png" width="43" height="57" />
					<p>Editar Legenda</p>
				</div>
				
				<div id="modal-admin-ad-dispo">
					<ul class="modal-ad-dispo-left">
						<li>
							<label for="Unidade">Legenda</label>
							<input id="legenda" name="legenda" value="<?=@$img->legenda?>" type="text" class="categoria">
						</li>
					</ul>
				</div>
				
				<div id="modal-inter-info-edit-a">
					<a href="javascript: AtualizaEditarDisponibilidades();" style="text-decoration:none"><div class="modal-admin-btn-salvar"><p>Salvar</p></div></a>
					<a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
				</div>
			
			</form>
  
		</div>
		<script type="text/javascript">
			<?php
			if(@$return){
				echo "parent.window.location.href='{$return}';";
			}
			?>
		</script>
	</body>
</html>