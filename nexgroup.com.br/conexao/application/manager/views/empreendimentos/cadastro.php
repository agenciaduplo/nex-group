<?=$this->load->view('includes/header');?>


<!-- Selecione empreendimento --> 
<div id="emp-container-inter">
	<div id="emp-box" >
    
    	<div id="emp-top-inter" style="cursor:pointer;" onclick="javascript: return mostraImoveis();">
        	<h2 id="expandirAba" style="font-size:24px;">Clique aqui para selecionar outro empreendimento</h2>
       	 	<img id="menu" src="<?=base_url()?>assets/manager/img/conexao-ico-setadown.png" width="24" height="14" />
        </div>
        
        <a class="menuClick" href="#menu"></a>
  
		<!-- OPEN  --> 
		<div id="emp-recolher" style="display:none;">
	        <div id="emp-in">
	        	<ul id="tooltipNex">
	        		<?php if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
	            		<a href="<?=site_url()?>empreendimentos/cadastro"><li class="ad-emp"><img src="<?=base_url()?>assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" /><p>Adicionar Empreendimento</p></li></a>
	            	<?php endif; ?>
	            	
	            	<?php foreach (@$emps as $row): ?>
            	
            			<a title="<?=@$row->nome?> :: <?=@$row->cidade?> / <?=@$row->uf?> - <?=@$row->descricao?> - <?=@$row->endereco?>" style="cursor:pointer" href="<?=site_url()?>empreendimentos/<?=@$row->id_empreendimento?>"><li><img src="<?=base_url()?>assets/manager/uploads/logos/<?=@$row->logomarca?>" width="120" alt="<?=@$row->nome?>" /><span><?=@$row->caracterizacao?></span></li></a>
            	
            		<?php endforeach; ?>
	            	
	            </ul>
	        </div> 
	        
			<div id="fechar-emp-div"><a href="javascript: mostraImoveis();" class="fechar-emp">Fechar</a></div>
			
		</div>
		<!-- OPEN --> 
  
    </div>
</div>

<!-- Empreendimento INFO --> 
<div id="inter-container">
	<div id="inter-box">
	
		<div class="visualizarEmpreendimento">
	    	<p><a href="<?=site_url()?>home">Inicio</a>   >  Cadastrar empreendimento</p>
		</div>
        
        <?php if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'): ?>
        <!-- editar emp -->
        <form id="FormEmpreendimento" name="FormEmpreendimento" method="post" enctype="multipart/form-data" action="<?=site_url()?>empreendimentos/editar_empreendimento">
	        <input type="hidden" name="id_empreendimento" value="<?=@$emp->id_empreendimento?>" />
	        <div id="editarEmp" class="ediarEmpreendimento" style="display:block;">

		        <div id="inter-img-edit">
					<span class="imgLogo">&nbsp;</span>
		            <p>Escolher arquivo da logomarca:</p>
		            
		          	<div style="background: none;">
		          		<input id="FileLogoEmp" style="cursor:pointer" name="logomarca" type="file" value="Procurar" />
		            	<p class="inter-img-edit-p">(Arquivo com fundo branco ou transparente)</p>
		          	</div>
		        
		        </div>
		   
		        <div id="inter-info-edit" style="display:block;">
		            <ul>
		                <li>
		                    <label for="emp_nome">Nome</label>
		                    <input id="emp_nome" name="emp_nome" type="text" value="<?=@$emp->nome?>" class="validate[required] categoria-emp-edit">
		                </li>
		                <li>
		                    <label for="caracterizacao">Caract.</label>
		                    <input id="caracterizacao" name="caracterizacao" type="text" value="<?=@$emp->caracterizacao?>" class="categoria-emp-edit">
		                </li>
		                <li>
		                    <label for="descricao">Descrição</label>
		                    <input id="descricao" name="descricao" type="text" value="<?=@$emp->descricao?>" class="categoria-emp-edit">
		                </li>
		                <li>
		                    <label for="entrega">Entrega</label>
		                    <input id="entrega" name="entrega" type="text" value="<?=@$emp->entrega?>" class="categoria-emp-edit">
		                </li>
		                
		                <li>
		                    <label for="emp_end">Estado</label>
		                    <select name="estado" id="estado" onchange="return trocaCidades(this.value);">
		                    	<?php foreach (@$estados as $row): ?>
		                    		<option value="<?=@$row->id_estado?>"><?=@$row->uf?></option>
		                    	<?php endforeach; ?>
		                    </select>
		                </li>
		                <li>
		                    <label for="emp_end">Cidade</label>
		                    <select name="cidade" id="cidades">
		                    	<?php if(@$cidades): ?>
                            		<?php foreach (@$cidades as $row): ?>
                                		<option value="<?=$row->id_cidade?>"><?=$row->cidade?></option>
                            		<?php endforeach; ?>
                            	<?php endif; ?>
		                    </select>
		                </li>
		                <li>
		                    <label for="emp_end">Endereço</label>
		                    <input id="emp_end" name="emp_end" type="text" value="<?=@$emp->endereco?>" class="categoria-emp-edit">
		                </li>
		                <li>
		                    <label for="emp_empresa">Empresa</label>
		                    <select name="emp_empresa" id="emp_empresa">
		                    	<?php foreach (@$empresas as $row): ?>
		                    		<option value="<?=$row->id_empresa?>"><?=@$row->empresa?></option>
		                    	<?php endforeach; ?>
		                    </select>
		                </li>
		                <li>
		                    <label for="emp_end">Mostrar tabela?</label>
		                    <select name="mostra_tabela" id="mostra_tabela">
		                    	<option <?php if(@$emp->mostra_tabela == "S") echo "selected='selected'"; ?> value="S">Sim</option>
		                    	<option <?php if(@$emp->mostra_tabela == "N") echo "selected='selected'"; ?> value="N">Não</option>
		                    </select>
		                </li>
		                <li>
		                   <label for="emp_ativo">Ativo</label>
		                    <select name="ativo" id="ativo">
		                    	<option <?php if(@$emp->ativo == "N") echo "selected='selected'"; ?> value="N">Não</option>
								<option <?php if(@$emp->ativo == "S") echo "selected='selected'"; ?> value="S">Sim</option>
		                    </select>
		                </li>
		                <li>
							<label for="pastas">Pasta</label>
							<select name="pastas" id="pastas">
								<option value="0">Sem pasta</option>
								<?php
								if(isset($grupos) && $grupos > 0){
									foreach($grupos as $rows){
										if(isset($_GET['gid']) && ($_GET['gid'] == $rows->id)){
											echo '<option value="',$rows->id,'" selected="selected">',$rows->nome,'</option>';
										} else {
											echo '<option value="',$rows->id,'">',$rows->nome,'</option>';
										}
									}
								}
								?>
							</select>
						</li>
		                <li>
		                    <label for="datapicker">Data Ordem</label>
		                    <input id="datapicker" name="data_ordem" type="text" value="" class="categoria-emp-edit" style="width: 120px;">
		                </li>
					</ul>
		            <div id="inter-info-edit-a" style="margin-bottom: 30px; width: 400px;">
		                <a href="javascript: AtualizarEmpreendimento();" style="text-decoration:none"><div class="admin-btn-salvar"><p>Salvar</p></div></a>
		                <a href="<?=site_url()?>" style="text-decoration:none"><div class="admin-btn-cancelar"><p>Cancelar</p></div></a>
		            </div>
		        </div>
 
	        </div>
        </form>
       	<!-- ADIMIN --> 
        <?php endif; ?>
  </div>
</div>


<?=$this->load->view('includes/footer');?>