<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		
		<script>
			jQuery(document).ready(function()
			{
				$("select").selectBox();
				$("#datapicker").datepicker({ dateFormat: 'dd/mm/yy' });
			});
			
			function AtualizaEditarDisponibilidades ()
			{
				$("#FormDisponibilidades").submit();
			}
			
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
			}
		</script>
		

	
	</head>

	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">

		<!-------------------------------- modal_edit_disponibilidade ------------------------------------->
		<div id="modal_edit_disponibilidade">
			
			<form id="FormDisponibilidades" name="FormDisponibilidades" method="post" action="<?=site_url()?>empreendimentos/atualizar_apto">
				<input type="hidden" name="id_empreendimento" value="<?=@$apto->id_empreendimento?>" />
				<input type="hidden" name="id_apartamento" value="<?=@$apto->id_apartamento?>" />
	
				<div id="modal_edit-nome">
					<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-edit.png" width="43" height="57" />
					<p>Editar '<?=@$apto->apartamento?>'</p>
				</div>
				
				<div id="modal-admin-ad-dispo">
				    <ul class="modal-ad-dispo-left">
				        <li>
				            <label for="Unidade">Unidade</label>
				            <input id="apartamento" name="apartamento" value="<?=@$apto->apartamento?>" type="text" class="categoria">
				        </li>
				        <li>
				            <label for="Status">Status</label>
				                <select id="select-admin-2" name="status">
				                <option <?php if(@$apto->status == "vendido") echo "selected='selected'"; ?> value="vendido">Vendido</option>
				                <option <?php if(@$apto->status == "disponivel") echo "selected='selected'"; ?> value="disponivel">Disponível</option>
				                <option <?php if(@$apto->status == "reservado") echo "selected='selected'"; ?> value="reservado">Reservado</option>
				                <option <?php if(@$apto->status == "revenda") echo "selected='selected'"; ?> value="revenda">Revenda</option>
				            </select>
				        </li>
				        <li>
				            <label for="Para quem?">Para quem?</label>
				            <input id="quem" name="quem" value="<?=@$apto->quem?>" type="text" class="categoria">
				        </li>
				    </ul>
				    <ul class="modal-ad-dispo-right">
				        <li>
				            <label for="Imobiliária">Imobiliária</label>
				            <input id="imobiliaria" name="imobiliaria" value="<?=@$apto->imobiliaria?>" type="text" class="categoria">
				        </li>
				        <li>
				            <label for="Data">Data</label>
				            <input id="datapicker" name="data_cadastro" value="<?=date('d/m/Y', strtotime($apto->data_cadastro))?>" type="text" class="categoria">
				        </li>
				        <li>
				            <label for="Andar">Andar (Só números)</label>
				            <input id="Andar" name="ordem" value="<?=@$apto->ordem?>" type="text" class="categoria" maxlength="4">
				        </li>
				    </ul>
				</div>
				
				<div id="modal-inter-info-edit-a">
				    <a href="javascript: AtualizaEditarDisponibilidades();" style="text-decoration:none"><div class="modal-admin-btn-salvar"><p>Salvar</p></div></a>
				    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
				</div>
			
			</form>
  
		</div>
	</body>
</html>