<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
	
		<script>
		
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
			}
		
		</script>
	
	</head>
	
	<?php
	
		$id_usuario = $this->encrypt->decode($_SESSION['conexao']['nex_id_usuario']);
	
	?>
	
	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">
  
		<div id="modal_cadastro">
			<form id="FormTermos" name="FormTermos" method="post" action="<?=site_url()?>login/aceito_termos_login">
				<?php if(@$id_usuario): ?>
					<input type="hidden" name="id_usuario" id="id_usuario" value="<?=@$id_usuario?>">
					<input type="hidden" name="termos" id="termos" value="S">
				<?php endif; ?>

				<div id="modal_edit-nome">
	            	<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-cadastro-2.png" width="37" height="57" />
	            	<p>1. Termo de Compromisso</p>
				</div>
	
	            <div id="termo">  			
	           		<p>
	           			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>   
	           		<p>
	           			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>       
				</div>
				
				<div id="modal-cadastro-a">
				    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Não aceito</p></div></a>
				    <a href="javascript: AceitoTermos();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Aceito</p></div></a>
				</div>
				
			</form>
			
		</div>
	
	</body>
</html>