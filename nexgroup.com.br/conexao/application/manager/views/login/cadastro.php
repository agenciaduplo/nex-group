<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.mask.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
		
		<script type="text/javascript">
			function FecharFancybox () {
				parent.$.fancybox.close();
			}
			
			jQuery(function($) {
			   $("#telefone").mask("(99) 9999-9999");
			   $("#celular").mask("(99) 9999-9999");
			});
		</script>

	</head>
	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">   
		<div id="modal_edit_disponibilidade">
			
			<form id="cadUsuarios" name="cadUsuarios" method="post" action="<?=site_url()?>login/enviar_cadastro">
				<div id="modal_edit-nome">
					<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-cadastro.png" width="37" height="57" />
					<p>1. Informações para Cadastro</p>
				</div>
				<div id="modal-admin-ad-cadastro">
				    <ul class="modal-ad-dispo-left">
				        <li>
				            <label for="Nome">Nome</label>
				            <input tabindex="1" id="Nome" value="<?=@$nome?>" name="nome" type="text" class="validate[required] categoria-cad">
				        </li>
				        <li>
				            <label for="telefone">Telefone</label>
				            <input tabindex="3" id="telefone" value="<?=@$telefone?>" name="telefone" type="text" class="validate[required] categoria-cad">
				        </li>
				        <li>
				            <label for="email">E-mail (Login)</label>
				            <input tabindex="5" id="email" value="<?=@$login?>" name="email" type="text" class="validate[required,custom[email]] categoria-cad">
				        </li>
				        <li>
				            <label for="Senha">Senha</label>
				            <input tabindex="7" id="Senha" name="senha" type="password" class="validate[required] categoria-cad">
				        </li>
				    </ul>
				    <ul class="modal-ad-dispo-right">
				        <li>
				            <label for="sobrenome">Sobrenome</label>
				            <input tabindex="2" id="sobrenome" value="<?=@$sobrenome?>" name="sobrenome" type="text" class="validate[required] categoria-cad">
				        </li>
				        <li>
				            <label for="celular">Celular</label>
				            <input tabindex="4" id="celular" value="<?=@$tel_cel?>" name="celular" type="text" class="validate[required] categoria-cad">
				        </li>
				        <li>
				            <label for="creci">CRECI</label>
				            <input tabindex="6" id="creci" value="<?=@$creci?>" name="creci" type="text" class="validate[required] categoria-cad">
				        </li>
				    </ul>
				</div>
				
				<?php if(@$existeUser == "S"): ?>
				<div id="mensagemErro">O E-mail (Login) informado já está cadastrado</div>
				<?php endif; ?>
				
				<div id="modal-cadastro-a">
				    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
				    <a href="javascript: Cadastro();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Prosseguir</p></div></a>
				</div>
			</form>
		  
		</div>
	</body>
</html>