<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.mask.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>

	</head>
	
	<script>
	
		function FecharFancybox ()
		{
			parent.$.fancybox.close();
		}
	
	</script>
	
	
	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">

		<div id="modal_edit_esqueceu_senha">
			<form id="cadRecuperarSenha" name="cadRecuperarSenha" method="post" action="<?=site_url()?>login/recuperar_senha">
				<div id="modal_edit-nome">
			    	<img src="<?=base_url()?>assets/manager/img/conexao-ico-key.png" width="41" height="55" />
			    	<p>Recuperar Senha</p>
				</div>
				<div id="modal_edit-text">
			        
			    </div>
			    <div id="modal-recup-email">
		            <p>Informe o e-mail de login:</p>
		
		            <label for="email">E-mail</label>
		            <input id="email" name="email" type="text" class="validate[required,custom[email]] categoria">
			    </div>
			    <div id="modal-inter-info-edit-a">
			        <a href="javascript: RecuperarSenha();" style="text-decoration:none"><div class="modal-admin-btn-salvar"><p>Enviar</p></div></a>
			        <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
			    </div>
	  		</form>
		</div>
	</body>
</html>