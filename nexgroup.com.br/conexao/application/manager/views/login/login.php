<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Conexão - Nex Group</title>

	<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/manager/css/jquery.fancybox-1.3.4.css" media="screen" />	
	
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>

	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
	
	<?php if(@$this->session->flashdata('respostaLogin')) { ?>
		
		<?php
			
			if($this->session->flashdata('respostaLogin') == "A Senha não está correta!")
			{
				$x = "pass";
			}
			elseif($this->session->flashdata('respostaLogin') == "O Usuário não está correto!")
			{
				$x = "user";
			}
		
		?>
		
		<a title="Atenção" href="<?=site_url()?>login/mensagem_login/<?=@$x?>" rel="fancybox" id="AbreLogin"></a>
		<script>
			jQuery(document).ready(function() {
				$("a#AbreLogin").fancybox();
				$("#AbreLogin").click();
			});
		</script>
		
		
	<?php } ?>
	
	<?php if(@$confirmou == "S") { ?>
		<a title="Atenção" href="<?=site_url()?>login/confirmou" rel="fancybox" id="AbreComunicado"></a>
		<script>
			jQuery(document).ready(function() {
				$("a#AbreComunicado").fancybox();
				$("#AbreComunicado").click();
			});
		</script>
		
			
	<? } ?>
	<?php if(@$confirmou == "N") { ?>
		<a title="Atenção" href="<?=site_url()?>login/nao_confirmou" rel="fancybox" id="AbreComunicado"></a>
		<script>
			jQuery(document).ready(function() {
				$("a#AbreComunicado").fancybox();
				$("#AbreComunicado").click();
			});
		</script>
	<? } ?>
	<?php if(@$termos == "N") { ?>
		
		<a title="Termos de Uso" href="<?=site_url()?>login/termos" rel="fancybox" id="AbreTermos"></a>
		<script>
			jQuery(document).ready(function() {
				$("a#AbreTermos").fancybox({
					'showCloseButton'	: false,
					'padding'			: 0,
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'autoScale'			: false,
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.6,
					'autoDimensions'	: false,
					'width'         	: 770,
					'height'        	: 490,
					'scrolling'			:'no',
					'titleShow'			: false,
					'type'          	: 'iframe'
				});
				
				$("#AbreTermos").click();
			});
			<?php //$this->auth->logout(); ?>
		</script>
	
	<?php } ?>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-35']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	
</head>
<body>
    
<div id="topo-container">
	<div id="topo">
    	<div id="logo"><h1>Conexão</h1></div>
    	<div id="topo-cadastro"><p>Não sou Cadastrado!</p><a id="modal-cadastro-abrir" href="<?=site_url()?>login/cadastro">Cadastre-se aqui!</a></div>
	</div>   
</div>

<div id="login-container">
	<div id="login-box">
    	<h2>Login</h2>
    	
    	<form name="FormLogin" id="FormLogin" action="<?=site_url()?>login/entrar" method="post">
        
	        <label for="usuario">E-mail</label>
	        <input tabindex="1" id="usuario" name="login" type="text" class="validate[required,custom[email]] form1">
	        
	        <label for="senha">Senha</label>
	        <input tabindex="2" id="senha" name="senha" type="password" class="validate[required] form1">
	        
	        <!-- 
	        <input name="lembrarsenha" type="checkbox" value="lembrarsenha" class="check-lembrar" />
	        <label  for="lembrarsenha">Lembrar senha</label>
	        -->
	        
	        <a id="EsqueceuSenha" href="<?=site_url()?>login/esqueceu_senha">Esqueceu a senha?</a>
	        
	         <!--ANTIGO BTN ENVIAR <div id="btn-logar"><a tabindex="3" href="javascript: Login();" class="btn-entrar">Entrar</a></div> -->
			<input name="Entrar" type="submit" class="btn-logar" value="Entrar" />
		</form>
		
	</div>
</div>

<div id="footer-container-login">
	<div id="footer-box">
    	<a href="http://www.nexgroup.com.br"><div id="footer-logo"></div></a>
    	<a href="http://imobi.divex.com.br"><div id="footer-divex"></div></a>
	</div>
</div>
</body>

</html>