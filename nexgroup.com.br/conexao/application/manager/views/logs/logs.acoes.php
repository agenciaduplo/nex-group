<?=$this->load->view('includes/header');?>

<div id="usuario-container">
	<div id="usuario-box">
		
		<?php if(@$termo): ?>
			
			 <p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  <a href="<?=site_url()?>logs/acessos">Log's de Ações</a>   >  <?=@$termo?></p>

		
		<?php else: ?>
		
			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Log's de Ações</p>
		
		<?php endif; ?>
		
    	<div id="acoes-top">
        	<h2>Log's de Ações</h2>
        	<?php if(@$buscar == "S"): ?>
            	
            	<p class="acoes-total">Resultados para '<?=@$termo?>'</p>
        	
        	<?php else: ?>
        	
        		<p class="acoes-total">Itens Cadastrados: <?=@$cadastrados?></p>
        	
        	<?php endif; ?>
        
        </div>
        
        <form id="FormBusca" method="POST">
        	<input type="hidden" name="buscar" value="S">        
	        <div id="usuario-in">
	            <input  id="keyword" name="keyword" type="text" class="validate[required] usuario-categoria">
	            <a href="javascript: EnviarBusca();">Procurar</a>
	        </div>
	    </form>

		<div id="usuario-table">
              
            <?php if (@$acoes):?>  
            <table cellspacing="10" class="tables">
                <tr>
                    <th class="acoes-table-name-1">Tabela</th>
                    <th class="acoes-table-name-2">Ação</th>
                    <th class="acoes-table-name-3">IP</th>
                    <th class="acoes-table-name-4">Usuário</th>
                    <th class="acoes-table-name-5">Data</th>
                    <th class="acoes-table-name-6">Hora</th>
                </tr>
                <?php foreach ($acoes as $row):?>
                <?php
                	$data = date('d/m/Y', strtotime($row->data_hora));
                	$hora = date('H:i:s', strtotime($row->data_hora));
                ?>
                <tr bgcolor="#9e9e9e">
                    <th class="acoes-table-1"><?=$row->tabela?></th>
                    <th class="acoes-table-2"><?=$row->acao?></th>
                    <th class="acoes-table-3"><?=$row->ip?></th>
                    <th class="acoes-table-4"><a style="color:#fff; text-decoration:none"><?=$row->login?></a></th>
                    <th class="acoes-table-5"><?=$data?></th>
                    <th class="acoes-table-6"><?=$hora?></th>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <?php else: ?>
            
            	<h3>Nenhum log de ações encontrado.</h3>
            
            <?php endif; ?>
        </div>
        
        <?php if(@$buscar != "S"): ?>
        	<div id="acoes-numera"<?=@$paginacao?></div>
		<?php endif; ?>
        
        <!-- <div id="acoes-numera"><p>1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">6</a> <a href="">7</a></p></div> -->

        
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>