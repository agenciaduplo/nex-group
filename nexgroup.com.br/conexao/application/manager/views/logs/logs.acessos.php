<?=$this->load->view('includes/header');?>

<div id="usuario-container">

	<div id="usuario-box">
		<?php if(@$termo): ?>
		
			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  <a href="<?=site_url()?>logs/acessos">Log's de Acessos</a>   >  <?=@$termo?></p>
		
		<?php else: ?>
		
   			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Log's de Acessos	</p>

		<?php endif; ?>
		
    	<div id="acesso-top">
        	<h2>Log's de Acessos</h2>
        	<?php if(@$buscar == "S"): ?>
        	
        		<p class="acesso-total">Resultados para '<?=@$termo?>'</p>
        	
        	<?php else: ?>
            	
            	<p class="acesso-total">Itens Cadastrados: <?=@$cadastrados?></p>
            
            <?php endif; ?>
        </div>
		
		<form id="FormBusca" method="POST">
        	<input type="hidden" name="buscar" value="S">        
	        <div id="usuario-in">
	            <input  id="keyword" name="keyword" type="text" class="validate[required] usuario-categoria">
	            <a href="javascript: EnviarBusca();">Procurar</a>
	        </div>
	    </form>

		<div id="usuario-table">
            <?php if (@$acessos):?>
            <table cellspacing="10" class="tables">
                <tr>
                    <th class="acesso-table-name-1">IP</th>
                    <th class="acesso-table-name-2">Usuário</th>
                    <th class="acesso-table-name-3">Data</th>
                    <th class="acesso-table-name-4">Hora</th>
                </tr>
                <?php foreach ($acessos as $row):?>
       			<?php
                	$data = date('d/m/Y', strtotime($row->data_hora));
                	$hora = date('H:i:s', strtotime($row->data_hora));
                ?>  
                <tr bgcolor="#9e9e9e">
                    <th class="acesso-table-1"><?=@$row->ip?></th>
                    <th class="acesso-table-2"><a style="color:#fff; text-decoration:none"><?=@$row->login?></a></th>
                    <th class="acesso-table-3"><?=@$data?></th>
                    <th class="acesso-table-4"><?=@$hora?></th>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <?php else: ?>
            
            	<h3>Nenhum log de acesso encontrado.</h3>
            
            <?php endif; ?>
        </div>

        <?php if(@$buscar != "S"): ?>
        	<div id="acoes-numera"<?=@$paginacao?></div>
		<?php endif; ?>
		<!-- <div id="acoes-numera"><p>1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">6</a> <a href="">7</a></p></div>-->
        
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>