<?=$this->load->view('includes/header');?>

<div id="usuario-container">
	<div id="usuario-box">
		<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Arquivos Padrão</p>
		<div id="acoes-top">
			<h2>Arquivos Padrão</h2>
		</div>
		<? if($is_admin){ ?>
		<div id="admin-ad-arquivosNew">
			<form action="<?=site_url(); ?>arquivos/upload" name="FormArquivos" id="FormArquivos"  method="post" enctype="multipart/form-data">
				<div id="admin-ad-arquivos-nome">
					<p class="admin-add">Adicionar Arquivo</p>
				</div>
				<div id="admin-ad-arquivos-1">
					<p>Nome do arquivo:</p>
					<input id="apartamento" name="nome" type="text" class="validate[required] categoria">
				</div>
				<div id="admin-ad-arquivos-2">
					<p>Arquivo:</p>
					<input type="file" style="cursor:pointer" id="arquivo-procurar" name="userfile" />
				</div>
				<a href="javascript: EnviarArquivo();" style="text-decoration:none">
					<div class="admin-btn-salvar"><p>Salvar</p></div>
				</a>
			</form>
		</div>
		<? } ?>
		<div id="arquivos-catego">
			<?php
			if(sizeof($arquivos) > 0){
				echo '<ul id="aqr_padr">';
				foreach($arquivos as $rows){
					echo '
						<li>
							<img src="',base_url(),'assets/manager/uploads/tipos_arquivos/convencao_condominio.png" alt="" />
							<p>',$rows->nome,'</p>
							<div id="arquivos-catego-in">
								<a href="',site_url(),'arquivos/download/',$rows->id,'/',$rows->filename,'">Baixar</a>
							</div>
							<span>',date('d/m/Y H:i',$rows->ctime),'</span>
					';
					if($is_admin){
						echo '
							<div id="admin-arquivo-preco">
								<a onclick="return confirm(\'Tem certeza que deseja excluir o arquivo?\');" href="',site_url(),'arquivos/deletar/',$rows->id,'" class="admin-excluir">Excluir</a>
							</div>
						';
					}
					echo '
						</li>
					';
				}
				echo '</ul>';
			} else {
				echo '<h3>Nenhum arquivo encontrado</h3>';
			}
			?>
		</div>
	</div>
</div>
<?=$this->load->view('includes/footer');?>