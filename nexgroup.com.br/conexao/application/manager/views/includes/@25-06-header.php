<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Conexão - Nex Group</title>

	<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/manager/css/jquery.fancybox-1.3.4.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/manager/css/jquery.tooltip.css" media="screen" />
	<link href="<?=base_url()?>assets/manager/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lte IE 7]> <html class="ie7" lang="pt-BR"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8" lang="pt-BR"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9" lang="pt-BR"> <![endif]-->

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-35']); // UA-1622695-35
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

    BASE_URL = "<?= base_url(); ?>";

	</script>

</head>

<body>

<div id="topo-container">
	<div id="topo">
    	<div id="logo" style="cursor:pointer"><h1>Conexão</h1></div>
    	<div id="user"><p>Olá <?=@$_SESSION['conexao']['nex_login_nome'];?> <a style="font-size:11px; margin-left:5px;" id="modal-cadastro-abrir-update" href="<?=site_url()?>usuarios/editar_dados">Editar cadastro</a> <span style="font-size:11px;">|</span> <a style="font-size:11px;" href="<?=site_url()?>login/sair">Sair</a></p></div>

        <div id="notifica">
        	<div id="notifica-number"><p><?=count(@$comunicadosHeader);?></p></div>
        	<?php if(@$comunicadosHeader): ?>
            <span style="cursor:pointer;" onclick="mostraCom();" id="controla-notifica" class="notifica-off"></span>
			<?php else: ?>
			<span style="cursor:default;" href="#" id="controla-notifica" class="notifica-off"></span>
			<?php endif; ?>


			<div id="notifica-open" style="display:none;">
			<?php if(@$comunicadosHeader): ?>
				<?php $cont = 0; foreach (@$comunicadosHeader as $row): $cont++; ?>
					<?php if(@$row->imagem_destaque != "" && $row->tipo == "I"): ?>
	            	<a title="Comunicado" id="comu<?=@$cont?>" href="<?=base_url()?>assets/manager/uploads/comunicados/<?=$row->imagem_destaque?>" rel="fancybox">
	                	<p class="notifica-open-note-p1"><?=@$row->titulo?></p>
	                    <p class="notifica-open-note-p2">Postada em <?=date('d/m/Y', strtotime($row->data_hora))?></p>
	                </a>
	                <?php else: ?>
	                <a title="Comunicado" id="comu<?=@$cont?>" href="<?=site_url()?>comunicados/visualizar/<?=@$row->id_comunicado?>" rel="fancybox">
	                	<p class="notifica-open-note-p1"><?=@$row->titulo?></p>
	                    <p class="notifica-open-note-p2">Postada em <?=date('d/m/Y', strtotime($row->data_hora))?></p>
	                </a>
	                <?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
            </div>
        </div>

	</div>
</div>

<?php if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>

<div id="admin-container">
    <div id="btn-admin">
        <ul>
            <a href="<?=site_url()?>usuarios"><li class="btn-admin-margin">Usuarios</li></a>
            <a href="<?=site_url()?>logs/acoes"><li class="btn-admin-margin">Log's de ações</li></a>
            <a href="<?=site_url()?>logs/acessos"><li class="btn-admin-margin">Log's de acessos</li></a>
            <a href="<?=site_url()?>comunicados"><li>Comunicados</li></a>
            <a href="<?=site_url()?>vendas"><li class="btn-admin-margin btn-admin-mt">Minhas Vendas</li></a>
        </ul>
    </div>
</div>
<?php }else{ ?>
<div id="admin-container">
  <div id="btn-admin" class="corretor">
    <ul>
      <a href="<?=site_url()?>vendas"><li class="btn-admin-margin">Minhas Vendas</li></a>
    </ul>
  </div>
</div>
<?php } ?>
<div class="gosth"></div>
<div id="menu"></div>
<a class="goMenu" href="#menu"></a>