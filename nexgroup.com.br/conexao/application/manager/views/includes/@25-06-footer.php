
	<div id="footer-container">
		<div id="footer-box">
	    	<a href=""><div id="footer-logo"></div></a>
	    	<a href=""><div id="footer-divex"></div></a>
		</div>
	</div>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jQuery.fileinput.js"></script>
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.tooltip.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/plugins.js"></script>
    	
	<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/functions.js"></script>
	
	<script>
		jQuery(document).ready(function() {
			<?php $cont = 0; foreach (@$comunicadosHeader as $row): $cont++; ?>
			$("a#comu<?=@$cont?>").fancybox();
			<?php endforeach; ?>
		});
	</script>

	<?php if(@$this->uri->segment(3) == "disponibilidades"): ?>
	<script>
		$(document).ready(function() {
			$("#btn-dispo").click();
		});
	
	</script>
	<?php elseif(@$this->uri->segment(3) == "arquivos"): ?>
	<script>
		$(document).ready(function() {
			$("#btn-arquivos").click();
		});
	
	</script>
	<?php elseif(@$this->uri->segment(3) == "imagens"): ?>
	<script>
		$(document).ready(function() {
			$("#btn-imagens").click();
		});
	
	</script>
	<?php endif; ?>
	
	<?php if(@$page == "home"): ?>

<script type="text/javascript">
$(function(){
	<?php if(isset($enquete) && !isset($enquete->usuario)){ ?>
	/*
	$.fancybox({
		'showCloseButton'	: true,
		'padding'			: 5,
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'autoScale'			: false,
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6,
		'autoDimensions'	: false,
		'width'         	: 400,
		'height'        	: 500,
		'scrolling'			:'no',
		'titleShow'			: false,
		'type'          	: 'iframe',
		'href'				: '<?=site_url(); ?>enquetes'
	});
	*/
	
	
	/*
	$.fancybox({
			//'orig'			: $(this),
			'padding'		: 0,
			'href'			: 'http://farm3.static.flickr.com/2687/4220681515_cc4f42d6b9.jpg',
			'title'   		: 'Lorem ipsum dolor sit amet',
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic'
	});
	*/
	<? } ?>
});
</script>

	
	<?php if(@$comunicado->id_comunicado): ?>
	<?php if(@$ultimoLogin->data_hora < $comunicado->data_hora): ?>
	<script>
		jQuery(document).ready(function() {
			$("a#AbreComunicado").fancybox();
			$("#AbreComunicado").click();
		});
	</script>
	<?php endif; ?>
	<?php endif; ?>
	
	<?php endif; ?>
	
	<?php if(@$page == "visualizar"): ?>
	<script>
	
		<?php if(@$comunicadoEmp->id_comunicado): ?>
		<?php if(@$ultimoLogin->data_hora < $comunicadoEmp->data_hora): ?>
	
		jQuery(document).ready(function() {
			$("a#AbreComunicadoEmp").fancybox();
			$("#AbreComunicadoEmp").click();
		});
	
		<?php endif; ?>
		<?php endif; ?>
	
		$("#btn-arquivos").click(function ()
		{	
			$('body').find('.Some').fadeOut(function ()
			{
				$(".empClick").click();
				$("#arquivos-container").show();
				$('#btn-categorias').find('div').removeClass('btn-cat-on').addClass('btn-cat-off');
				$("#btn-arquivos").removeClass('btn-cat-off').addClass('btn-cat-on');
			});
		});
		
		$("#btn-imagens").click(function ()
		{	
			$('body').find('.Some').fadeOut(function ()
			{
				$(".empClick").click();
				$("#imagens-container").show();
				$('#btn-categorias').find('div').removeClass('btn-cat-on').addClass('btn-cat-off');
				$("#btn-imagens").removeClass('btn-cat-off').addClass('btn-cat-on');
			});
		});
		
		<?php if(@$emp->mostra_tabela == "S"): ?>
		$("#btn-dispo").click(function ()
		{
			$('body').find('.Some').fadeOut(function ()
			{
				$(".empClick").click();
				$("#dispo-inter-container").show();
				$('#btn-categorias').find('div').removeClass('btn-cat-on').addClass('btn-cat-off');
				$("#btn-dispo").removeClass('btn-cat-off').addClass('btn-cat-on');
			});
		});
		<?php endif; ?>	
	
	</script>
	<?php endif; ?>

</body>

</html>