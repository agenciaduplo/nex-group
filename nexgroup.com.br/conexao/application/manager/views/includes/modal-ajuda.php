<div class="modal">
    <a href="javascript:void(0);" onclick="closeModalByClass('modal');" class="btn-fechar"><i class="fa fa-close"></i></a>
    <div class="modalin">
        <div class="left">
            <i class="fa fa-question"></i>
        </div>
        <div class="left">
            <h2>Encontrou tudo o que procurava?<br>
            <span>Temos uma equipe à disposição para ajudá-lo.</span></h2>
        </div>
        <div class="left">
            <a href="javascript:void(0);" onclick="openModalFormAjuda();">O que você precisa?</a>
        </div>
    </div>
</div>

<div class="modalquestion">
    <div class="black-modal"></div>
    <div class="box">
        <div class="top">
            <div class="left">
                <i class="fa fa-question"></i>
            </div>
            <div class="left">
                <h2>Encontrou tudo o que procurava?<br>
                <span>Temos uma equipe à disposição para ajudá-lo.</span></h2>
            </div>
        </div>
        <div class="midle">
            <h3>Descreva em que mais podemos ajudá-lo, dúvidas, solicitações ou deixe suas sugestões (min 50 caracteres):</h3>
            <form>
                <textarea name="ajuda-mensagem" id="ajuda-mensagem" ></textarea>
                <a href="javascript:void(0);" onclick="sendMensageToSuporte();" class="btn">Enviar Mensagem</a>
                <a href="javascript:void(0);" onclick="closeModalByClass('modalquestion');" class="btn cancel">Cancelar</a>
            </form>
        </div>
    </div>
</div>

<div class="sucess">
    <div class="black-modal"></div>
    <div class="box">
        <div class="left">
            <i class="fa fa-check"></i>
        </div>
        <div class="left">
            <h2>Mensagem Enviada!!<br>
            <span>Muito obrigado! Retornaremos seu contato o mais breve possível.</span></h2>
            <a href="javascript:void(0);" onclick="closeModalByClass('sucess');" class="btn">fechar</a>
        </div>
    </div>
</div>

<div class="error">
    <div class="black-modal"></div>
    <div class="box">
        <div class="left">
            <i class="fa fa-times"></i>
        </div>
        <div class="left">
            <h2>
                Opps =( &nbsp;&nbsp;&nbsp;Falha ao Enviar!!<br>
                <span>Preencha o campo da mensagem corretamente (minimo 50 caracteres).</span>
            </h2>
            <a href="javascript:void(0);" onclick="closeModalByClass('error');" class="btn">fechar</a>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
        $('.modalin').delay(1500).fadeIn(500);
    });

    function closeModalByClass(class_name) {
        $('.' + class_name).fadeOut('fast');
    }

    function openModalFormAjuda() {
        $('.modalquestion').fadeIn('fast');
    }

    function openSucess() {
        $('.sucess').fadeIn('fast');
        $('.modalquestion').fadeOut('fast');
    }

    function sendMensageToSuporte()
    {
        var msg = $('#ajuda-mensagem').val();

        // if(msg.length < 50)
        // {
        //     $('.error').fadeIn('fast');
        //     return false;
        // }

        $.post('<?=base_url()?>suporte/sendMensagemXHR', {msg: msg}, function(response) {

            if(response.erro == 1)
            {
                $('.error').fadeIn('fast');
                return false;
            }

            if(response.erro == 0)
            {
                $('#ajuda-mensagem').val('');
                openSucess();
                return true;
            }

        }, 'json');

    }
</script>

<style>
.modal { position: fixed; width: 100%; height: 80px; background: #222; bottom: 0; left: 0; z-index: 1000; opacity: 0.95; }
.modal .modalin .left { float: left; }
.modal .modalin i {font-size: 2em; margin: 0.9em; color: #444; float: left; }
.modal .modalin h2 {font-size: 1.3em; font-weight: bold; margin: 0.95em 0 0; color: #FFF; float: left; }
.modal .modalin h2 span {font-style: italic; font-size: 0.8em; font-weight: normal; }

.modal .modalin a { font-size: 1.1em; font-weight: bold; margin: 1.4em 0 0 2em; padding: 0.5em 1.5em 0.6em; background: #FFF; color: #222; border-bottom: 3px solid #ccc; border-radius: 5px; float: left; cursor: pointer; }
.modal .modalin a:hover { background: #ccc; border-bottom: 0 solid #ccc; margin: 1.55em 0 0 2em; }

.modal .btn-fechar {font-size: 2em; height: 80px; float: right; padding: 1.7% 1.7% 0 1.7%; color: #444; display: block; cursor: pointer; }
.modal .btn-fechar:hover { background: #111 }

.modalquestion { display: none; float: left; width: 100%; height: 100% }
.modalquestion .box { z-index: 2000; width: 800px; height: auto; background: #FFF; position: fixed; top: 50%; left: 50%; padding: 0 0 50px 0; margin: -300px 0 0 -400px; border-radius: 5px; display: block; overflow: hidden;  }
.modalquestion .box .top { float: left; background: #222; width: 100%; padding: 1em 0}
.modalquestion .box .top .left { float: left; }
.modalquestion .box .top i {font-size: 2em; margin: 0.9em; color: #444; float: left; }
.modalquestion .box .top h2 {font-size: 1.3em; font-weight: bold; margin: 0.95em 0 0; color: #FFF; float: left; }
.modalquestion .box .top h2 span {font-style: italic; font-size: 0.8em; font-weight: normal; }

.modalquestion .box .midle {float: left; background:; width: 83%; margin: 0 0 0 8.5% }
.modalquestion .box .midle h3 {font-size: 1.1em; width: 60%; margin: 1.5em 0 0.5em; color: #222; float: left; }
.modalquestion .box .midle textarea {font-size: 1.0em; width: 96%; height: 150px; margin: 0.5em 0 0; padding: 2%; color: #222; float: left; resize: none; }
.modalquestion .box .midle textarea:focus { background: #fffeea; border-color: #cac25a   }
.modalquestion .box .midle .btn {font-size: 1.1em; font-weight: bold; margin: 1.4em 0 0 1.5em; padding: 0.5em 1.5em 0.6em; background: #222; color: #FFF; border: 0; border-bottom: 3px solid #444; border-radius: 5px; float: right; cursor: pointer;}
.modalquestion .box .midle .btn:hover { background: #333; border-bottom: 0 solid #444; margin: 1.55em 0 0 1.5em; }

.modalquestion .box .midle .cancel {background: #c1c1c1 !important; border-bottom: 3px solid #b2b2b2 !important; float: right !important }
.modalquestion .box .midle .cancel:hover { background: #ccc !important; border-bottom: 0 !important; margin: 1.55em 0 0 0 !important; }

.sucess, .error { display:none; float: left; width: 100%; height: 100%;}
.sucess .box, .error .box { z-index: 2002; width: 550px; height: auto; position: fixed; top: 50%; left: 50%; padding: 1em 2em 2em; margin: -150px 0 0 -275px; border-radius: 5px; display: block; overflow: hidden;  }
.sucess .box { background: #7bde11; }
.error .box { background: #de2d11; }
.sucess .box .left, .error .box .left { float: left; }
.sucess .box i, .error .box i {font-size: 2em; margin: 0.9em; color: #FFF; float: left; }
.sucess .box h2, .error .box h2  {font-size: 1.3em; font-weight: bold; margin: 0.95em 0 0; color: #FFF; float: left; clear: both;}
.sucess .box h2 span, .error .box h2 span {font-style: italic; font-size: 0.8em; font-weight: normal; }
.sucess .box .btn, .error .box .btn {font-size: 1.1em; font-weight: bold; margin: 1.0em 0 0 0; padding: 0.5em 1.5em 0.6em; background: #FFF; border: 0; border-bottom: 3px solid #e1e1e1; border-radius: 5px; float: left; cursor: pointer; clear: both;}
.sucess .box .btn {color: #7bde11; }
.error .box .btn {color: #de2d11; }
.sucess .box .btn:hover, .error .box .btn:hover { background: #f0f0f0; border-bottom: 0 solid #c1c1c1; margin: 1.15em 0 0 0; }

.sucess .black-modal, .error .black-modal {z-index: 2001;}

.black-modal { z-index: 1000; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: black; opacity: 0.7; filter: alpha(opacity =70); }
</style>
