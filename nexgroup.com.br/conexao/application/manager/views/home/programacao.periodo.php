<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.mask.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
		
		<script>
		
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
			}
						
			jQuery(function($)
			{
			   $("#telefone").mask("(99) 9999-9999");
			   $("#celular").mask("(99) 9999-9999");
			   $("select").selectBox();
			   
			   jQuery("#cadPeriodos").validationEngine(
					'attach', {
						promptPosition : "topLeft"		
				});
			});
			
			function EnviarPeriodo ()
			{
				$("#cadPeriodos").submit();
			}
		
		</script>

	
	</head>
	
	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">
	   
		<div id="modal_edit_disponibilidade">
			
			<form id="cadPeriodos" name="cadPeriodos" method="post" action="<?=site_url()?>home/salvarPeriodo">
				<input type="hidden" name="id_usuario" value="<?=@$usuario->id_usuario?>" >
				<input type="hidden" name="id_periodo" value="<?=@$periodo->id_periodo?>" >
				
				<div id="modal_edit-nome">
					<p>Adicionar / Editar período</p>
				</div>
				
				<div id="modal-admin-ad-cadastro" style="height: 68px;">
				
				    <ul class="modal-ad-dispo-left">
				        <li>
				            <label for="Periodo">Descrição do Período</label>
				            <input tabindex="1" id="Periodo" value="<?=@$periodo->periodo?>" name="periodo" type="text" class="validate[required] categoria-cad">
				        </li>
				    </ul>
				</div>
				
				<?php if(@$existeUser == "S"): ?>
				<div id="mensagemErro">O E-mail (Login) informado já está cadastrado</div>
				<?php endif; ?>
				
				<div id="modal-cadastro-a">
				    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
				    <a href="javascript: EnviarPeriodo();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Salvar</p></div></a>
				</div>
			
			</form>
		  
		</div>
		
	</body>
</html>