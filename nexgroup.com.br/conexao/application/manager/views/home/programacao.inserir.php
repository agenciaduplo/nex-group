<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine-pt_BR.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.validationEngine.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.selectBox.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery.mask.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
		
		<script>
		
			function FecharFancybox ()
			{
				parent.$.fancybox.close();
			}
			
			function Cadastro ()
			{
				$("#CadProgramacao").submit();
			}
			
			jQuery("#cadPeriodos").validationEngine(
					'attach', {
						promptPosition : "topLeft"		
			});
			
			jQuery(function($)
			{
			   $("#telefone").mask("(99) 9999-9999");
			   $("#celular").mask("(99) 9999-9999");
			   $("select").selectBox();
			});
		
		</script>

	
	</head>
	
	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">
	   
		<div id="modal_edit_disponibilidade">
			
			<form id="CadProgramacao" name="CadProgramacao" enctype="multipart/form-data" method="post" action="<?=site_url()?>home/salvarProgramacao">
				<?php if(@$programacao->id_programacao): ?>
				<input type="hidden" name="id_programacao" id="id_programacao" value="<?=@$programacao->id_programacao?>" />
				<input type="hidden" name="id_emp_or_pasta" id="id_emp_or_pasta" value="<?=@$programacao->id_emp_or_pasta?>" />
				<?php endif; ?>
				
				
				<div id="modal_edit-nome">
					<p><?=@$titleProg?></p>
				</div>
				
				<div id="modal-admin-ad-cadastro">
				
				    <ul class="modal-ad-dispo-left">
				        <li>
				        	<label for="empreendimento">Empreendimento</label>
				        	<select id="empreendimento" name="empreendimento" class="validate[required] categoria-cad">
				        		<?php foreach ($emps as $row): ?>
				        		<option <?php if(@$programacao->id_emp_or_pasta == $row->id) { echo "selected='selected'"; } ?> value="<?=$row->id;?>-<?=$row->caracterizacao?>"><?=$row->nome;?></option>
				        		<?php endforeach; ?>
				        	</select>
				        </li>

				        <li>
				        	<label for="tipo">Tipo</label>
				        	<select id="tipo" name="tipo" class="validate[required] categoria-cad">
				        		<?php foreach ($tiposProgramacoes as $row): ?>
				        		<option <?php if(@$programacao->id_tipo_programacao == $row->id_tipo_programacao) { echo "selected='selected'"; } ?> value="<?=$row->id_tipo_programacao?>"><?=$row->tipo_programacao;?></option>
				        		<?php endforeach; ?>
				        	</select>
				        </li>

				        <li>
				            <label for="Datas">Datas</label>
				            <input tabindex="1" id="Datas" name="datas" value="<?=@$programacao->datas?>" type="text" class="categoria-cad">
				        </li>


				        <li>
				            <label for="Formato">Formato</label>
				            <input tabindex="1" id="Formato" name="formato" value="<?=@$programacao->formato?>" type="text" class="categoria-cad">
				        </li>
				      
				        <li>
				            <label for="Determinacao">Determinação</label>
				            <input tabindex="1" id="Determinacao" name="determinacao" value="<?=@$programacao->determinacao?>" type="text" class="categoria-cad">
				        </li>
				        
				         <li>
				            <label for="local">Local</label>
				            <input tabindex="1" id="local" name="local" type="text" value="<?=@$programacao->local?>" class="categoria-cad">
				        </li>
				        
				         <li>
				            <label for="pontos">Pontos</label>
				            <textarea name="pontos" id="pontos" cols="" rows="" style="width:320px;height:100px"><?=@$programacao->pontos?></textarea>

				        </li>
				        
				        <li>
				            <label for="ordem">Ordem</label>
				            <input tabindex="1" id="ordem" name="ordem" type="text" value="<?=@$programacao->ordem?>" class="categoria-cad">
				        </li>
                        
				    </ul>
				    <ul class="modal-ad-dispo-right">
				        <li>
				        	<label for="periodos">Período</label>
				        	<select id="periodos" name="periodos" class="validate[required] categoria-cad">
				        		<?php foreach ($periodos as $row): ?>
				        		<option <?php if(@$programacao->id_periodo == $row->id_periodo) { echo "selected='selected'"; } ?> value="<?=$row->id_periodo?>"><?=$row->periodo;?></option>
				        		<?php endforeach; ?>
				        	</select>
				        </li>

				        <li style="margin-top: 13px; float: left;">
				            <label for="obs">Observações</label>
							<textarea name="obs" id="obs" cols="" rows=""><?=@$programacao->observacoes?></textarea>
				        </li>

				        <li style="margin-top: 8px; float: left;">
				            <label for="Nome">Arquivo (somente imagem)</label>
                            <input type="file" style="cursor:pointer" id="arquivo-procurar" name="arquivo" />
				        </li>
				        
				        <li>
				            <label for="horario">Horário</label>
				            <input tabindex="1" id="horario" name="horario" type="text" value="<?=@$programacao->horario?>" class="categoria-cad">
				        </li>
				        
				        <li>
				            <label for="veiculo">Veículo</label>
				            <input tabindex="1" id="veiculo" name="veiculo" type="text" value="<?=@$programacao->veiculo?>" class="categoria-cad">
				        </li>

				        </ul>
				</div>
				
				<?php if(@$existeUser == "S"): ?>
				<div id="mensagemErro">O E-mail (Login) informado já está cadastrado</div>
				<?php endif; ?>
				
				<div id="modal-cadastro-a">
				    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
				    <a href="javascript: Cadastro();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Criar</p></div></a>
				</div>
			
			</form>
		  
		</div>
		
	</body>
</html>