<?= $this->load->view('includes/header'); ?>

<div id="emp-container">
    <div id="emp-box">
        <section class="filtros nomobile">
          <div class="row">

          <div style="clear:both;"></div>
          <form id="FormFiltro" method="post" action="">
              <div class="small-4" style="float:left;margin-right:20px;">
                <select id="fil_estado" name="fil_estado">
                  <option value="">Filtrar por Estado:</option>
                  <?php foreach($arrEstados as $estado){?>
                    <option value="<?=$estado->id_estado?>" <?=($idestado == $estado->id_estado) ? "selected='selected'" : ''?> ><?=$estado->estado?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="small-4 nomargin-left">
                <select id="fil_cidade" name="fil_cidade">
                  <option value="">Filtrar por Cidade:</option>
                  <?php foreach($arrCidades as $cidade){?>
                    <option value="<?=$cidade->id_cidade?>" <?=($idcidade == $cidade->id_cidade) ? "selected='selected'" : ''?> ><?=$cidade->cidade?></option>
                  <?php } ?>
                </select>
              </div>
          </form>

        <div id="emp-top">
            <h2>Selecione um Empreendimento</h2>
        </div>
        <div id="emp-in">
            <ul id="tooltipNex">
                <?php
                if ($_SESSION['conexao']['nex_login_tipo'] == 'administrador') {
                    echo '
                        <a href="', site_url(), 'empreendimentos/cadastro">
                         <li class="ad-emp">
                          <div class="emp-color-add">
                           <img class="img-ad" src="', base_url(), 'assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" />
                           <p>Adicionar Empreendimento</p>
                          </div>
                         </li>
                        </a>
                        <a href="', site_url(), 'grupos/cadastro">
                         <li class="ad-emp">
                          <div class="emp-color-add">
                           <img class="img-ad" src="', base_url(), 'assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" />
                           <p>Adicionar<br />Pasta</p>
                          </div>
                         </li>
                        </a>
                       ';
                }
                if (sizeof($emps) > 0) {
                    foreach ($emps as $rows) {
                        if ($is_admin or ( $rows->ativo == 'S' or $rows->ativo == 1)) {
//                            if ($rows->id == 1 && $rows->is_grupo == '0') {
//                                $url = site_url() . 'home/promocao';
//                            } else {
                                $url = $rows->is_grupo == '1' ? site_url() . 'grupos/' . $rows->id : site_url() . 'empreendimentos/' . $rows->id;
//                            }
                            if ($rows->is_grupo == '1' && $rows->ativo == 0) {
                                $ativo = '<strong>OFF</strong>';
                            } elseif ($rows->is_grupo == '0' && $rows->ativo == 'N') {
                                $ativo = '<strong>OFF</strong>';
                            } else {
                                $ativo = '';
                            }
                            echo '
                                <a href="', $url, '" title="', $rows->titulo, '" style="cursor:pointer">
                                 <li style="background-image: url('.base_url().'assets/manager/uploads/logos/'.$rows->logomarca.');background-repeat: no-repeat;background-position: 50%;background-size: 118px;">

                                  <div class="bug">
                                   ', $rows->nome, '
                                   ', $ativo, '
                                  </div>
                                 </li>
                                </a>
                               ';
                               // <img src="', base_url(), 'assets/manager/uploads/logos/', $rows->logomarca, '" width="120" alt="', $rows->nome, '" />
                        }
                    }
                }
                ?>
                <div id="fechar-emp-div" style="display:none"></div>

            </ul>
        </div>

        <p class="outras-opcoes">Outras opções:</p>

        <div class="emp-all">
            <div class="emp-all-box">
                <div class="boxin">
                    <h2>Programação Semanal de Marketing:</h2><br />
                    <p>Visualise a programação semanal planejada para cada empreendimento.</p>
                </div>
                <a class="emp-link3" href="<?= site_url() ?>home/programacao">Ver Programação Semanal de Marketing</a>
            </div>
        </div>

        <div class="emp-all" style="border:solid 1px #c1e6c4; background:#f3fff4;">
            <div class="emp-all-box">
                <div class="boxin" style="width:300px; margin-top: 17px;">
                    <h2 style="color:#32b33e">Arquivos Padrão:</h2><br />
                    <p>Visualise e faça o download dos arquivos padrão e de todas as tabelas de todos os empreendimentos.</p>
                </div>
                <a class="emp-link" href="<?= site_url() ?>home/getAllFiles">Baixar todas as tabelas</a>
                <a class="emp-link2" href="<?= site_url() ?>arquivos">Visualizar os arquivos padrão</a>
            </div>
        </div>


    </div>
</div>

<!-- Procure todos empreendimentos -->
<div id="procure-container" style="display:none;">
    <div id="procure-box">
        <div id="procure-top">
            <h2>Procure em todos os Empreendimentos</h2>
        </div>
        <div id="procure-in">
            <p>Por Categoria</p>
            <select id="select-categoria" name="categoria" class="form">
                <option value=""></option>
            </select>
            <a href="">Procurar</a>
        </div>
    </div>
</div>

<!-- ULTIMAS DISPONIBILIDADES -->

<a href="#dispo-box" class="goDispoBox"></a>
<div id="dispo-container">
    <div id="dispo-box">

        <?php if (@$atualizacoesArquivos): ?>
            <a href="javascript: mostraUpDisponibilidades();">
                <div id="dispo-top">
                    <h2>Últimas Atualizações de Disponibilidades</h2>
                    <img src="<?= base_url() ?>assets/manager/img/conexao-ico-setadown.png" width="24" height="14" />
                </div>
            </a>

            <div id="dispo-in" style="display:none;">
                <p>Empreendimentos<span style="margin-left:241px">Unidades</span><span style="margin-left:54px">Status</span><span style="margin-left:95px">Data e hora</span></p>
                <?php foreach (@$atualizacoesEmmps as $row): ?>
                    <ul>
                        <li style="width:350px"><?= $row->nome ?></li>
                        <li style="width:110px"><?= $row->apartamento ?></li>
                        <li style="width:129px"><?= $row->status ?></li>
                        <li style="width:209px; margin-right:0"><?= date('d/m/y H:i', strtotime($row->data_hora)) ?></li>
                    </ul>
                <?php endforeach; ?>
                <a href="<?= site_url() ?>home/atualizacoes_disponibilidades">Ver mais</a>
            </div>

        <?php endif; ?>

    </div>
</div>

<a href="#atual-box" class="goAtualBox"></a>
<!-- ULTIMAS ATUALIZAÇÕES -->

<div id="atual-container">
    <div id="atual-box">

        <?php if (@$atualizacoesArquivos): ?>
            <a href="javascript: mostraUpArquivos();">
                <div id="atual-top">
                    <h2>Últimas Atualizações de Arquivos</h2>
                    <img src="<?= base_url() ?>assets/manager/img/conexao-ico-setadown.png" width="24" height="14" />
                </div>
            </a>

            <div id="atual-in" style="display:none;">
                <p>Empreendimentos<span style="margin-left:240px">Arquivo</span><span style="margin-left:305px">Data</span></p>

                <?php foreach (@$atualizacoesArquivos as $row): ?>

                    <ul>
                        <li style="width:350px"><?= @$row->nome ?></li>
                        <li style="width:349px"><a href="<?= site_url() ?>empreendimentos/download/<?= $row->id_arquivo ?>"><?= $row->tipo ?></a></li>
                        <li style="width:209px;  margin-right:0"><?= date('d/m/y H:i', strtotime($row->data_hora)) ?></li>
                    </ul>
                <?php endforeach; ?>
                <div id="atual-btn-mais"><a href="<?= site_url() ?>home/atualizacoes_arquivos">Ver mais</a></div>
            </div>
        <?php endif; ?>
        <!-- OPEN -->

    </div>
</div>


<?php if (@$comunicado->id_comunicado): ?>
    <?php if (@$ultimoLogin->data_hora < $comunicado->data_hora): ?>

        <?php if (@$comunicado->imagem_destaque != "" && $comunicado->tipo == "I"): ?>

            <a title="Comunicado" href="<?= base_url() ?>assets/manager/uploads/comunicados/<?= $comunicado->imagem_destaque ?>" rel="fancybox" id="AbreComunicado"></a>

        <?php elseif (@$comunicado->tipo == "T"): ?>

            <a title="Comunicado" href="<?= site_url() ?>comunicados/visualizar/<?= @$comunicado->id_comunicado ?>" rel="fancybox" id="AbreComunicado"></a>

        <?php endif; ?>

    <?php endif; ?>
<?php endif; ?>


<?=$this->load->view('includes/modal-ajuda.php')?>

<?=$this->load->view('includes/footer', array('enquete' => $enquete))?>
