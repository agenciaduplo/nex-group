<?=$this->load->view('includes/header');?>

<div id="usuario-container">

	<div id="usuario-box">
		<?php if(@$termo): ?>
		
			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  <a href="<?=site_url()?>home/atualizacoes_disponibilidades">Atualizações de Disponibilidades</a>   >  <?=@$termo?></p>
		
		<?php else: ?>
		
   			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Atualizações de Disponibilidades	</p>

		<?php endif; ?>
		
    	<div id="acesso-top-dispo">
        	<h2>Atualizações de Disponibilidades</h2>
        	<?php if(@$buscar == "S"): ?>
        	
        		<p class="acesso-total">Resultados para '<?=@$termo?>'</p>
        	
        	<?php else: ?>
            	
            	<p class="acesso-total">Itens Cadastrados: <?=@$cadastrados?></p>
            
            <?php endif; ?>
        </div>
		
		<form id="FormBusca" method="POST">
        	<input type="hidden" name="buscar" value="S">        
	        <div id="usuario-in">
	            <input  id="keyword" name="keyword" type="text" class="validate[required] usuario-categoria">
	            <a href="javascript: EnviarBusca();">Procurar</a>
	        </div>
	    </form>
	    
	    <div id="dispo-in">
	    	<?php if(@$disponibilidades): ?>
        	<p>Empreendimento<span style="margin-left:360px">Unidade</span><span style="margin-left:66px">Status</span><span style="margin-left:95px">Data e hora</span></p>
        	<?php foreach (@$disponibilidades as $row): ?>
        	<ul>
            	<li style="width:464px"><?=$row->nome?></li>
            	<li style="width:110px"><?=$row->apartamento?></li>
            	<li style="width:129px"><?=$row->status?></li>
            	<li style="width:209px; margin-right:0"><?=date('d/m/y H:i', strtotime($row->data_hora))?></li>
            </ul>
            <?php endforeach; ?>
            
            <?php else: ?>
            	<p>Nenhuma atualização de disponibilidade encontrada</p>
            <?php endif; ?>
        </div>

        <?php if(@$buscar != "S"): ?>
        	<div id="acoes-numera"<?=@$paginacao?></div>
		<?php endif; ?>        
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>