<?=$this->load->view('includes/header');?>

<script>

	
	function AtualizaPeriodo (id_periodo)
	{
		
		jQuery(function($)
		{	
			$(".usuario-admin-edit").attr('href','<?=site_url()?>home/programacao_periodo/'+id_periodo);
			window.location.href = '<?=site_url()?>home/programacao/'+id_periodo;
		});	
	}
	
	function ExcluirProgramacao (id_programacao, cont)
	{
		var msg ='';
		vet_dados = 'id_programacao=' + id_programacao;
		
		base_url  = "http://www.nexgroup.com.br/conexao/home/removerProgramacao/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    		$("#programacao-"+cont).fadeOut();
		            },
		    error: function (request, status, error) {
        		alert(request.responseText);
    		}
		});		
	}
	
	function BuscaProgramacao (periodo, idEmp, pasta, cont)
	{
		var msg ='';
		vet_dados = 'periodo=' + periodo
					+ '&id=' + idEmp
					+ '&pasta=' + pasta;
					
					
		$("#imgss").find('li').removeClass('active-li');			
		$("#img-"+cont).addClass('active-li');
		$("#imgss").find('div').removeClass('setali');
		$("#seta-"+cont).addClass('setali');
		
		base_url  = "http://www.nexgroup.com.br/conexao/home/buscarProgramacao/";
		$.ajax({
		    type: "POST",
		    url: base_url,
		    data: vet_dados,
		    success: function(msg) {
		    		$("#programacoes").html(msg);
		            },
		    error: function (request, status, error) {
        		alert(request.responseText);
    		}
		});
		return false;		
	}
</script>

<div id="semanal-container">

	<div id="semanal-box">
 		<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Programação Semanal de Marketing</p>
        <div class="semanal-periodo">
        	<form>
                <select id="tipo" name="tipo" class="categoria-cad selectBox" style="display: none;" onchange="return AtualizaPeriodo(this.value)">
                    <?php $cont = 0; foreach (@$periodos as $row): $cont++; if($cont == 1) { $idPeriodo = $row->id_periodo; } ?>
                    <option <?php if($row->id_periodo == $periodo->id_periodo) echo "selected='selected'"; ?> value="<?=@$row->id_periodo?>"><?=@$row->periodo?></option>
                    <?php endforeach; ?>
                </select>
            </form>
            <?php if($is_admin){ ?>
            <div class="addperiodo">
            	<!--a id="modal-cadastro-prog-periodo" class="admin-add" href="<?=site_url()?>home/programacao_periodo">Adicionar novo período</a-->
                <a id="modal-cadastro-prog-periodo" href="<?=site_url()?>home/programacao_periodo/<?=@$idPeriodo?>" class="usuario-admin-edit">Adicionar / Editar período</a>
                <!-- <a href="<?=site_url()?>" class="usuario-admin-excluir">Excluir período</a> -->
            </div>
            <?php } ?>
        </div>
    	<div id="semanal-top">
        	<h2>Programação Semanal de Marketing</h2>
        	<?php if($is_admin){ ?><a id="modal-cadastro-prog-inserir" class="admin-add abarsf" href="<?=site_url()?>home/programacao_inserir">Adicionar nova Programação</a><?php } ?>

        </div>
        <p style="margin-bottom:12px; float: left; width: 100%;">Escolha o empreendimento:</p>
        <div class="semanal-left">
        	<?php if(@$emps): ?>
        	<ul id="imgss">
        		<?php $cont = 0; foreach ($emps as $row): $cont++; if($cont == 1) { $idImovel = $row->id; $pastaImovel = $row->caracterizacao;  } ?>
        		
        		<!--li id="img-<?=$cont?>" onclick="BuscaProgramacao('<?=@$periodo->id_periodo?>','<?=@$row->id?>','<?=@$row->caracterizacao?>', '<?=$cont?>');" <?php if($cont == 1) { echo "class='firstborder active-li'"; } ?>><div id="seta-<?=$cont?>" <?php if($cont == 1) { echo "class='setali'"; } ?>></div><figure><img src="http://www.nexgroup.com.br/timthumb/timthumb.php?src=http://www.nexgroup.com.br/conexao/assets/manager/uploads/logos/<?=$row->logomarca?>&w=40&h=40&zc=2" alt="<?=$row->nome?>" /></figure><p><?=character_limiter($row->nome, 25);?></p></li-->
            <li id="img-<?=$cont?>" rel="empreendimento" data-periodo="<?=@$periodo->id_periodo?>" data-empreendimento="<?=@$row->id?>" data-pasta="<?=@$row->caracterizacao?>" data-cont="<?=$cont?>" <?php if($cont == 1) { echo "class='firstborder active-li'"; } ?>><div id="seta-<?=$cont?>" <?php if($cont == 1) { echo "class='setali'"; } ?>></div><figure><img src="http://www.nexgroup.com.br/timthumb/timthumb.php?src=http://www.nexgroup.com.br/conexao/assets/manager/uploads/logos/<?=$row->logomarca?>&w=40&h=40&zc=2" alt="<?=$row->nome?>" /></figure><p><?=character_limiter($row->nome, 25);?></p></li>
        		
        		<?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
        
        <?php
        	if($pastaImovel != "P")
        	{
	        	$pastaImovel = "E";
        	}
        
        	$programacoes = $this->model->getProgramacoes($periodo->id_periodo, $idImovel, $pastaImovel);
        
        ?>
        <div class="semanal-right" id="programacoes">
        	<?php if($programacoes): ?>
        	
        	<?php $cont = 0; foreach ($programacoes as $row): $cont++; ?>
        	<div id="programacao-<?=@$cont?>"> <!-- BOX -->
                <div class="boxetopright">
                	<figure class="icos-semanal-jornal"></figure><h3><?=$row->tipo_programacao?></h3>
                	<a class="usuario-admin-excluir" style=" float: right; margin-right: 26px; margin-top: 26px" href="javascript: ExcluirProgramacao('<?=@$row->id_programacao?>', '<?=$cont?>');">Excluir</a>
                	<a class="usuario-admin-edit" id="modal-cadastro-prog-editar" style=" float: right; margin-right: 26px; margin-top: 26px" href="<?=site_url()?>home/programacao_editar/<?=@$row->id_programacao?>">Editar</a>
                </div>
                <div class="boxebottomright">
               	  <p>
           	  	   <?php if(@$row->datas): ?>
           	  	   <b>Datas:</b> <?=nl2br(@$row->datas);?><br />
           	  	   <?php endif; ?>
                   
                   <?php if(@$row->formato): ?>
                   <b>Formato:</b> <?=nl2br(@$row->formato);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->determinacao): ?>
                   <b>Determinação:</b> <?=nl2br(@$row->determinacao);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->local): ?>
                   <b>Local:</b> <?=nl2br(@$row->local);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->pontos): ?> 
                   <b>Pontos:</b> <?=nl2br(@$row->pontos);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->veiculo): ?>
                   <b>Veículo:</b> <?=nl2br(@$row->veiculo);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->observacoes): ?>
                   <b>Observações:</b> <?=nl2br(@$row->observacoes);?><br />
                   <?php endif; ?>
                   
				   <?php if(@$row->horario): ?>
                   <b>Horário:</b> <?=nl2br(@$row->horario);?><br />
                   <?php endif; ?>
				   
                     </p>
                    <?php if(@$row->arquivo): ?>
	                <div class="imagem">
				    	<a class="ampliarImagem" href="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>"><img src="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>" width="130" /></a>
	                	<i></i><p><a class="ampliarImagem" href="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>">clique para visualizar</a></p>
	                </div>
	                <?php endif; ?>
					
                </div>
            </div>
            
            <?php endforeach; ?>
            
            <?php else: ?>
            	
            	<p>Nenhuma programação cadastrada.</p>
            
            <?php endif; ?>
            
        </div>
        
    </div>
</div>

<?=$this->load->view('includes/footer.programacao.php');?>	