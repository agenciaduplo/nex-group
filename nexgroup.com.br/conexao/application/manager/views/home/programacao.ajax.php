   	<?php if($programacoes): ?>
    	
    	<?php $cont = 0; foreach ($programacoes as $row): $cont++; ?>
    	<div id="programacao-<?=@$cont?>"> <!-- BOX -->
            <div class="boxetopright">
            	<figure class="<?=$row->imagem?>"></figure><h3><?=$row->tipo_programacao?></h3>
            	<a class="usuario-admin-excluir" style=" float: right; margin-right: 26px; margin-top: 26px" href="javascript: ExcluirProgramacao('<?=@$row->id_programacao?>', '<?=$cont?>');">Excluir</a>
            	<a class="usuario-admin-edit" id="modal-cadastro-prog-editar" style=" float: right; margin-right: 26px; margin-top: 26px" href="<?=site_url()?>home/programacao_editar/<?=@$row->id_programacao?>">Editar</a>
            </div>
            <div class="boxebottomright">
           	  <p>
           	  	   <?php if(@$row->datas): ?>
           	  	   <b>Datas:</b> <?=nl2br(@$row->datas);?><br />
           	  	   <?php endif; ?>
                   
                   <?php if(@$row->formato): ?>
                   <b>Formato:</b> <?=nl2br(@$row->formato);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->determinacao): ?>
                   <b>Determinação:</b> <?=nl2br(@$row->determinacao);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->local): ?>
                   <b>Local:</b> <?=@nl2br($row->local);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->pontos): ?> 
                   <b>Pontos:</b> <?=@nl2br($row->pontos);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->veiculo): ?>
                   <b>Veículo:</b> <?=nl2br(@$row->veiculo);?><br />
                   <?php endif; ?>
                   
                   <?php if(@$row->observacoes): ?>
                   <b>Observações:</b> <?=nl2br(@$row->observacoes);?><br />
                   <?php endif; ?>
				   
				   <?php if(@$row->horario): ?>
                   <b>Horário:</b> <?=nl2br(@$row->horario);?><br />
                   <?php endif; ?>
                   
				</p>
				<?php if(@$row->arquivo): ?>
                <div class="imagem">
			    	<a class="ampliarImagem" href="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>"><img src="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>" width="130" /></a>
                	<i></i><p><a class="ampliarImagem" href="<?=site_url()?>assets/manager/uploads/programacoes/<?=$row->arquivo?>">clique para visualizar</a></p>
                </div>
                <?php endif; ?>
				
            </div>
        </div>
        
        <?php endforeach; ?>
        
	<?php else: ?>
		
		<p>Nenhuma programação cadastrada.</p>
	
	<?php endif; ?>
	
	

	<script>
	jQuery(document).ready(function() {
		$("a.ampliarImagem").fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'overlayShow'	:	false
		});
		$("a#modal-cadastro-prog-editar").fancybox({
			'showCloseButton'	: false,
			'padding'			: 0,
			'transitionIn'		: 'fade',
			'transitionOut'		: 'fade',
			'autoScale'			: false,
			'overlayColor'		: '#000',
			'overlayOpacity'	: 0.6,
			'autoDimensions'	: false,
			'width'         	: 770,
			'height'        	: 880,
			'scrolling'			:'no',
			'titleShow'			: false,
			'type'          	: 'iframe'
		});	
	});
	</script>