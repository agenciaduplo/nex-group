<?=$this->load->view('includes/header');?>

<div id="usuario-container">

	<div id="usuario-box">
		<?php if(@$termo): ?>
		
			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  <a href="<?=site_url()?>home/atualizacoes_arquivos">Atualizações de Arquivos</a>   >  <?=@$termo?></p>
		
		<?php else: ?>
		
   			<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Atualizações de Arquivos	</p>

		<?php endif; ?>
		
    	<div id="acesso-top-arquivo">
        	<h2>Atualizações de Arquivos</h2>
        	<?php if(@$buscar == "S"): ?>
        	
        		<p class="acesso-total">Resultados para '<?=@$termo?>'</p>
        	
        	<?php else: ?>
            	
            	<p class="acesso-total">Itens Cadastrados: <?=@$cadastrados?></p>
            
            <?php endif; ?>
        </div>
		
		<form id="FormBusca" method="POST">
        	<input type="hidden" name="buscar" value="S">        
	        <div id="usuario-in">
	            <input  id="keyword" name="keyword" type="text" class="validate[required] usuario-categoria">
	            <a href="javascript: EnviarBusca();">Procurar</a>
	        </div>
	    </form>
	    
	    
	     <div id="atual-in">
	     	<?php if(@$arquivos): ?>
        	<p>Empreendimentos<span style="margin-left:240px">Arquivo</span><span style="margin-left:305px">Data</span></p>
        	
        	<?php foreach (@$arquivos as $row): ?>
        	
        	<ul>
            	<li style="width:350px"><?=@$row->nome?></li>
            	<li style="width:349px"><a href="<?=site_url()?>empreendimentos/download/<?=$row->id_arquivo?>"><?=$row->tipo?></a></li>
            	<li style="width:209px;  margin-right:0"><?=date('d/m/y H:i', strtotime($row->data_hora))?></li>
            </ul>
            <?php endforeach; ?>
        	<?php else: ?>
            	<p>Nenhuma atualização de arquivo encontrada</p>
            <?php endif; ?>
        </div>

        <?php if(@$buscar != "S"): ?>
        	<div id="acoes-numera"<?=@$paginacao?></div>
		<?php endif; ?>        
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>