<?=$this->load->view('includes/header');?>

<div id="usuario-container">

	<div id="usuario-box">
  		<p><a href="<?=site_url()?>home">Inicio</a>   >  Sempre uma oportunidade que combina perfeitamente com o seu estilo de vida.</p>
		<div style="margin-top:30px; margin-bottom: 30px; font-size:10px; text-align:justify">
        	<img src="<?=base_url()?>assets/manager/img/promocao-2013-03-11/01.png" style="width: 750px; margin-left:90px" />
            <p style="width: 700px; margin-left:115px; margin-bottom:30px;">*Preço-base: Março/2013. Valor à vista ref. apto. 502, 3 dorms., 79,8m², box 73L. Em caso de parcelamento, os valores serão corrigidos pelo INCC durante as obras e IGP-M mais 1% a.m.após habite-se. Valor sujeito à alteração sem aviso prévio e válido conforme disponibilidade. Imagens meramente ilustrativas. Áreas comuns decoradas e entregues conforme memorial descritivo. Incorporação registrada no R.3 da Matrícula 163.364, 4ª Zona Poa. Proj. Arq.: Nedeff Arquitetura. Proj. Paisag.: Landscape Jardins. Incorporação e construção: DHZ, uma empresa Nex Group. 
			</p>
        	<img src="<?=base_url()?>assets/manager/img/promocao-2013-03-11/02.png" style="width: 750px; margin-left:90px" />
            <p style="width: 700px; margin-left:115px; margin-bottom:30px;">Incorporação reg. no 4ª Registro de Imóveis de Porto Alegre sob Matrícula 64.925. Projeto Arquitetônico: Arquiteto Antônio R. Xavier da Costa - CREA 13.321 e Franklin Moreira - CREA 51.031. Projeto Paisagístico: Léa Japur. Imagens meramente ilustrativas. Incorporação e construção : Capa Engenharia S.A., uma empresa Nex Group. 
			</p>
        	<img src="<?=base_url()?>assets/manager/img/promocao-2013-03-11/03.png" style="width: 750px; margin-left:90px" />
            <p style="width: 700px; margin-left:115px; margin-bottom:30px;">*Preço-base: Março/2013. Valor à vista ref. apto. 302, 3 suítes, 124,49 m², box 180 e 181. Em caso de parcelamento, os valores serão corrigidos pelo INCC durante as obras e IGP-M mais 1% a.m.após habite-se. Valor sujeito à alteração sem aviso prévio e válido conforme disponibilidade. Imagens meramente ilustrativas. Áreas comuns decoradas e entregues conforme memorial descritivo. Projeto Arquitetônico: Núcleo Arquitetura. Projeto Paisagístico: Takeda Arquitetos Paisagistas. Incorporação e construção: EGL Engenharia, uma empresa Nex Group. 
			</p>
        </div>
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>