<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/style_select.css"></script>

		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/chosen.jquery.js"></script>

		<script>
			function FecharFancybox(){
				parent.$.fancybox.close();
			}
		</script>
	</head>

	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">

		<div id="modal_edit-nome">
			<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-cadastro.png" width="37" height="57" />
			<p>Selecione um Usuário para delegar o contato.</p>
		</div>
<?php
			if($del_atend != ""){
				$idusuario_del 					= $del_atend->idusuario;
				$idcontato_delegado 		= $del_atend->idcontato_delegado;
			}else{
				$idusuario_del 					= "";
				$idcontato_delegado 		= "";
			}
?>
<div class="wrapper_select">
		<form id="delegar_contato" action="<?=base_url()?>vendas/DelegarSave/" method="post">
			<input type="hidden" name="idcontato_delegado" id="idcontato_delegado" value="<?=$idcontato_delegado?>" />
			<input type="hidden" name="idcontato" id="idcontato" value="<?=$idcontato?>" />
			<input type="hidden" name="table" id="table" value="<?=$tipo?>" />
			<select name="f_idusuario" id="f_idusuario" data-placeholder='Delegar Para' class="chzn-select">
				<option value=""></option>
<?php 	foreach($usuarios as $users){ ?>
					<option value="<?=$users->id_usuario?>" <?=($idusuario_del == $users->id_usuario) ? "selected='selected'" : ""?> ><?=strtolower($users->login)?> - <?=ucwords((mb_strtolower($users->nome)))?></option>
<?php	}	 ?>
			</select>
		</div> <!-- wrapper_select ::end::-->
<?php
			$contato 	= $contato[0];
			if($contato->origem != ""){
				$origem 	= explode("/",$contato->origem);
				$origem 	= $origem[2];
			}
?>
		<div class="wrapper_select">
			<table>
				<tr>
					<td class="header">Data:</td>
					<td><?=date("d/m/Y H:i", strtotime($contato->data_envio))?></td>
				</tr>
				<tr>
					<td class="header">Nome:</td>
					<td><?=$contato->nome?></td>
				</tr>
				<tr>
					<td class="header">Email:</td>
					<td><?=$contato->email?></td>
				</tr>
<?php if($contato->telefone != "" && $contato->telefone != 0){ ?>
				<tr>
					<td class="header">Telefone:</td>
					<td><?=$contato->telefone?></td>
				</tr>
<?php } ?>
				<tr>
					<td class="header">Empreendimento:</td>
					<td><?=$contato->empreendimento?></td>
				</tr>
				<tr>
					<td class="header">Campanha:</td>
					<td><?=$contato->camp?></td>
				</tr>
<?php if($contato->origem != ""){ ?>
				<tr>
					<td class="header">Origem:</td>
					<td><?=$origem?></td>
				</tr>
<?php } ?>
				<tr>
					<td class="header">Enviado Para:</td>
					<td><?=($contato->email_enviado_for == "lucia.keiko@vendasbbsul.com.br") ? "Lucia Keiko" : "Nexgroup Vendas"?></td>
				</tr>
			</table>
			<div class="comentario">
				<span>Comentário: </span> <br />
				<?=($tipo == "a") ? $contato->descricao : $contato->comentarios?>
			</div>
		</div> <!-- wrapper_select ::end::-->

		<div id="modal-cadastro-a" class="wrapper_buttons">
	    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
	    <a href="javascript: DelegarSave();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Delegar</p></div></a>
		</div>
	</form>

<script type="text/javascript">
  $(".chzn-select").chosen({no_results_text: "Oops, nenhum resultado encontrado:"});
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});

  function DelegarSave(){
	  $("#delegar_contato").submit();
	}
</script>
	</body>
</html>