<?=$this->load->view('includes/header');?>

<div id="usuario-container">
<div class="wrapper">

	<div id="comunica-top" class="detalhes">
    <h2>Minhas Vendas - Status</h2>
    <div class="clear"></div>
  </div>
  <div class="acoes-total">| Status Registrados: <?=count($contato_st)?> |</div>

  <br /><br /><br />

  <div style="clear:both"></div>

  <?php
      $contato    = $contato[0];
      $idcontato  = ($tipo == "a") ? "id_atendimento" : "id_interesse";

      if($contato->origem != ""){
        $origem   = explode("/",$contato->origem);
        $origem   = $origem[2];
      }
?>
  <div class="wrapper_select info_contato">
    <div class="col_esq">
      <div class="label_det">Data:</div><div class="conteudo_det"><?=date("d/m/Y H:i", strtotime($contato->data_envio))?></div>
      <div class="label_det">Empreendimento:</div><div class="conteudo_det"><?=$contato->empreendimento?></div>
      <div class="clear"></div> <br />
      <div class="label_det">Nome:</div><div class="conteudo_det"><?=$contato->nome?></div>
      <div class="label_det">Email:</div><div class="conteudo_det"><?=$contato->email?></div>
      <div class="clear"></div>
      <div class="label_det">Telefone:</div><div class="conteudo_det"><?=$contato->telefone?></div>
      <!-- <div class="clear"></div>
      <div class="label_det">Campanha:</div><div class="conteudo_det"><?=$contato->camp?></div>
      <div class="label_det">Origem:</div><div class="conteudo_det"><?=$contato->origem?></div> -->
    </div> <!-- col_esq ::end::-->

    <div class="col_dir">
      <div class="comentario det">
        <div class="coments">
          <span class="label_det">Comentário: </span>
          <?=($tipo == "a") ? $contato->descricao : $contato->comentarios?>
        </div>
      </div> <!-- comentario det ::end::-->
    </div> <!-- col_dir ::end::-->

    <div style="clear:both"></div>
    <br /><br />

    <a id="contato_status_cad" class="admin-add" href="<?=site_url()?>vendas/StatusContatoCad/<?=$contato->{$idcontato}?>/<?=$tipo?>">Adicionar Status</a>

  <div id="vendas-table">
<?php if($contato_st){ ?>
        <table cellspacing="5" class="table_vendas">
          <tr>
            <th class="table_header">Data Status</th>
            <th class="table_header">Status</th>
            <th class="table_header">Descrição</th>
            <th class="table_header" style="background:none;">&nbsp;</th>
          </tr>

<?php    foreach($contato_st as $row){ ?>
            <tr class="conteudo" bgcolor="#9e9e9e">
            <th class="w_small"><?=date("d/m/Y H:i", strtotime($row->data_hr))?></th>
            <th class="w_medium_minus"><?=$row->status?></th>
            <th class="w_medium"><?=$row->descricao?></th>
            <th class="usuario-table-4">
              <a href="<?=site_url()?>vendas/ContatoStatusExc/<?=$contato->{$idcontato}?>/<?=$tipo?>/<?=$row->idcontato_status?>" class="usuario-admin-excluir">Excluir</a>
            </th>
<?php     } ?>
        </table>

        <br /><br />
        <a id="contato_status_cad" class="admin-add" href="<?=site_url()?>vendas/StatusContatoCad/<?=$contato->{$idcontato}?>/<?=$tipo?>">Adicionar Status</a>
        <br /><br /><br /><br />

<?php }else{ ?>
        <br /><br />
        <div class="acoes-total">Você ainda não registrou nenhum status para este contato.</div>
        <br /><br />
<?php } ?>
  </div> <!-- vendas-table ::end::-->

  </div> <!-- wrapper_select ::end::-->
</div> <!-- wrapper ::end::-->
</div> <!-- #usuario-container ::end::-->

<?=$this->load->view('includes/footer');?>