<?=$this->load->view('includes/header');?>

<div id="usuario-container">
<div class="wrapper">

    	<div id="comunica-top">
        <h2>Minhas Vendas</h2>
        <div class="clear"></div>
      </div>
      <div class="acoes-total">| Atendimentos: <?=$tot_a?> | Interesses: <?=$tot_i?> | Total: <?=($tot_i+$tot_a)?> |</div>

<?php
        $data_i = ($data_i != "") ? $data_i : "01/".date("m/Y");
        $data_f = ($data_f != "") ? $data_f : date("d/m/Y");
        $table  = ($table  != "") ? $table  : "ambos";
?>
        <form id="FormBusca" method="get" action="<?=site_url()?>vendas/search">
	        <div id="usuario-in" class="vendas-in">
	            <input  id="search"  name="search"  type="text" class="email_contato" value="<?=$search?>">
              <input  id="data_i"  name="data_i"  type="text" class="validate[required] data" value="<?=$data_i?>" />
              <input  id="data_f"  name="data_f"  type="text" class="validate[required] data" value="<?=$data_f?>" />
              <select name="table" id="table" class="select_table">
                <option value="ambos" <?=($table == "ambos") ? "selected='selected'" : ''?> >Ambos</option>
                <option value="atendimentos" <?=($table == "atendimentos") ? "selected='selected'" : ''?>>Atendimentos</option>
                <option value="interesses" <?=($table == "interesses") ? "selected='selected'" : ''?>>Interesses</option>
              </select>
              <a href="javascript: EnviarBusca();">Procurar</a>
	            <a href="<?=site_url()?>vendas">Limpar</a>
	        </div>
	    </form>

        <div style="clear:both"></div>

<?php

if($tot_a > 0 || $tot_i > 0){ ?>
		<div id="vendas-table">
      <table cellspacing="5" class="table_vendas">
        <tr>
          <th class="table_header">Data Envio / Tipo</th>
<?php     if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
            <th class="table_header">Campanha (Origem)</th>
<?php     } ?>
          <th class="table_header">Nome</th>
          <th class="table_header">Email</th>
          <th class="table_header">Empreendimento</th>
          <th class="table_header">Comentários</th>
<?php     if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
            <th class="table_header">Enviado Para</th>
            <th class="table_header">Delegado</th>
<?php     }else{ ?>
            <th class="table_header">Telefone</th>
            <th class="table_header">Detalhes</th>
<?php     } ?>
        </tr>

<?php if($atendimento){
        foreach($atendimento as $row){
          if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
            if($row->origem != ""){
              $origem   = explode("/",$row->origem);
              $origem   = substr($origem[2],4);
            }else{
              $origem   = "";
            }
          } ?>
            <tr class="conteudo" bgcolor="#9e9e9e">
              <th class="w_small"><?=date("d/m/Y H:i", strtotime($row->data_envio))?> <br /> Atendimento</th>
<?php           if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
                  <th class="w_medium_minus"><?=str_replace("_", " ",$row->camp)?> (<?=$origem?>)</th>
<?php           } ?>
                <th class="w_medium_minus"><?=$row->nome?></th>
                <th class=""><?=$row->email?></th>
                <th class="w_medium_minus"><?=$row->empreendimento?></th>
                <th class="w_medium"><?=character_limiter(mb_strtolower($row->descricao), 100)?></th>
<?php           if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
                  <th class=""><?=($row->email_enviado_for == "lucia.keiko@vendasbbsul.com.br") ? "Lucia Keiko" : "Nexgroup Vendas"?></th>
                  <th class="">
<?php               if($del_atend[$row->id_atendimento]){ ?>
                      <?=$del_atend[$row->id_atendimento][0]->nome?> <br />
                      <?=$del_atend[$row->id_atendimento][0]->email?> <br /><br />
                      <div id="usuario-add-link">
                        <a id="delegar_contato" class="admin-add" href="<?=site_url()?>vendas/delegar/<?=$row->id_atendimento?>/a/<?=$del_atend[$row->id_atendimento][0]->idcontato_delegado?>">Alterar</a>
                        <a class="admin-add" href="<?=site_url()?>vendas/StatusContato/<?=$row->id_atendimento?>/a">Status</a>
                      </div>
<?php               }else{ ?>
                      <div id="usuario-add-link">
                        <a id="delegar_contato" class="admin-add" href="<?=site_url()?>vendas/delegar/<?=$row->id_atendimento?>/a">Delegar</a>
                      </div>
<?php               } ?>
                  </th>
<?php           }else{ ?>
                  <th class="w_medium_minus"><?=$row->telefone?></th>
                  <th class="">
                    <a id="contato_detalhes" class="admin-add" href="<?=site_url()?>vendas/StatusContato/<?=$row->id_atendimento?>/a">Detalhes</a>
                  </th>
<?php           } ?>
            </tr>
<?php     }
        }

        if($interesse){
          foreach($interesse as $row){
            if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){
              if($row->origem != ""){
                $origem   = explode("/",$row->origem);
                $origem   = substr($origem[2],4);
              }else{
                $origem   = "";
              }
            } ?>
            <tr class="conteudo" bgcolor="#9e9e9e">
              <th class="w_small"><?=date("d/m/Y H:i", strtotime($row->data_envio))?> <br /> Interesse</th>
<?php         if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
                <th class="w_medium_minus"><?=str_replace("_", " ",$row->camp)?> (<?=$origem?>)</th>
<?php         } ?>
              <th class="w_medium"><?=$row->nome?></th>
              <th class=""><?=$row->email?></th>
              <th class=""><?=$row->empreendimento?></th>
              <th class=""><?=character_limiter(mb_strtolower($row->comentarios), 100)?></th>
<?php         if($_SESSION['conexao']['nex_login_tipo'] == 'administrador'){ ?>
                <th class=""><?=($row->email_enviado_for == "lucia.keiko@vendasbbsul.com.br") ? "Lucia Keiko" : "Nexgroup Vendas"?></th>
                <th class="">
<?php             if($del_inter[$row->id_interesse]){ ?>
                    <?=$del_inter[$row->id_interesse][0]->nome?> <br />
                    <?=$del_inter[$row->id_interesse][0]->email?> <br /><br />
                    <div id="usuario-add-link">
                      <a id="delegar_contato" class="admin-add" href="<?=site_url()?>vendas/delegar/<?=$row->id_interesse?>/i/<?=$del_inter[$row->id_interesse][0]->idcontato_delegado?>">Alterar</a>
                      <a class="admin-add" href="<?=site_url()?>vendas/StatusContato/<?=$row->id_interesse?>/i">Status</a>
                    </div>
<?php             }else{ ?>
                    <div id="usuario-add-link">
                      <a id="delegar_contato" class="admin-add" href="<?=site_url()?>vendas/delegar/<?=$row->id_interesse?>/i">Delegar</a>
                    </div>
<?php             } ?>
                </th>
<?php         }else{ ?>
                <th class=""><?=$row->telefone?></th>
                <th class="">
                  <a id="contato_detalhes" class="admin-add" href="<?=site_url()?>vendas/StatusContato/<?=$row->id_interesse?>/i">Detalhes</a>
                </th>
<?php         } ?>
            </tr>
<?php     }
        } ?>
      </table>
    </div> <!-- vendas-table ::end::-->
<?php
}else{ ?>
  <br /><br />
  <div class="acoes-total">Oops!! <br /><br /> Nenhum contato encontrado, tente refazer a busca acima.</div>
  <br /><br />
<?php } ?>

<div style="float:left;margin-left:20px;margin-bottom:20px;">
  <a href="<?=site_url();?>vendas/exportar/<?=str_replace("/","-",$data_i)?>/<?=str_replace("/","-",$data_f)?>/<?=$table?>/<?=$search?>/">Exportar em CSV</a>
</div>

</div> <!-- wrapper ::end::-->
</div> <!-- #usuario-container ::end::-->

<?=$this->load->view('includes/footer');?>