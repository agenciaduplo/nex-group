<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link href="<?=base_url()?>assets/manager/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/modals.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/template.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/manager/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/jquery.selectBox.css"></script>
		<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/manager/css/style_select.css"></script>

		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-1.7.1.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/login.js"></script>
		<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/manager/js/chosen.jquery.js"></script>

		<script>
			function FecharFancybox(){
				parent.$.fancybox.close();
			}
		</script>
	</head>

	<body style="background:url(<?=base_url()?>assets/manager/img/conexao-bg.jpg);">

		<div id="modal_edit-nome">
			<img src="<?=base_url()?>assets/manager/img/conexao-ico-modal-cadastro.png" width="37" height="57" />
			<p>Adicione um novo status para este contato.</p>
		</div>

<div class="wrapper_select">
		<form id="contato_status" action="<?=base_url()?>vendas/ContatoStatusSave/" method="post">
			<input type="hidden" name="idcontato_delegado" id="idcontato_delegado" value="<?=$idcontato_delegado?>" />
			<input type="hidden" name="idcontato" 				 id="idcontato" 				 value="<?=$idcontato?>" 					/>

			<label for="Nome">Descrição</label>
			<input type="text" id="descricao" name="descricao"  class="validate[no-required] categoria-cad" />

			<div class="clear"></div>

			<label for="idstatus" style="margin-bottom:2px;">Status</label>
    	<select id="idstatus" name="idstatus" class="categoria-cad">
<?php foreach($status as $stats){ ?>
				<option value="<?=$stats->idcontato_deleg_status?>"><?=$stats->nome?></option>
<?php } ?>
    	</select>
</div> <!-- wrapper_select ::end::-->

		<div id="modal-cadastro-a" class="wrapper_buttons">
	    <a href="javascript: FecharFancybox();" style="text-decoration:none"><div class="modal-admin-btn-cancelar"><p>Cancelar</p></div></a>
	    <a href="javascript: StatusContatoSave();" style="text-decoration:none"><div class="modal-admin-btn-prosseguir"><p>Salvar</p></div></a>
		</div>
	</form>

<script type="text/javascript">
  function StatusContatoSave(){
	  $("#contato_status").submit();
	}
</script>
	</body>
</html>