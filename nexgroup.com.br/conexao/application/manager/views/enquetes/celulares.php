<?php
$smartphones = array('Experia (Sony)','Galaxy (Samsung)','iPhone (Apple)','Lumia (Nokia)','Optimus (LG)','BlackBerry');
$normal = array('LG','Motorola','Nokia','Samsung','Outro');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Enquete - Conexão - Nex Group</title>
<link href="<?=base_url(); ?>assets/manager/css/enquete.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form action="<?=site_url(); ?>enquetes/salvar" method="post">
	<table>
		<tr>
			<td>Olá, <?=$nome; ?><br></td>
		</tr>
		<tr>
			<td>Estamos estudando soluções mobile e gostaríamos de saber que tipo de telefone celular você usa:</td>
		</tr>
		<tr>
			<th>Smartphones:</th>
		</tr>
		<tr>
			<td>
				<?php
				foreach($smartphones as $rows){
					echo '
						<label>
							<input type="radio" name="celular" value="',$rows,'" />
							',$rows,'
						</label>
						<br />
					';
				}
				?>
				<label>
					Outro Smartphone? Qual? <input type="text" name="outro" value="" />
				</label>
			</td>
		</tr>
		<tr>
			<th>Celular normal:</th>
		</tr>
		<tr>
			<td>
				<?php
				foreach($normal as $rows){
					echo '
						<label>
							<input type="radio" name="celular" value="',$rows,'" />
							',$rows,'
						</label>
						<br />
					';
				}
				?>
			</td>
		</tr>
		<tr>
			<td align="center"><input type="submit" value="Salvar" /></td>
		</tr>
	</table>
</form>
</body>
</html>