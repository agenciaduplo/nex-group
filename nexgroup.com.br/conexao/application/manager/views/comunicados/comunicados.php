<?=$this->load->view('includes/header');?>

<script>

	function MostraTipo (tipo)
	{
		if(tipo == "T")
		{
			$("#ComuImg").fadeOut(function () {
				$("#comunica-descricao").fadeIn();
			});
		}
		else if(tipo == "I")
		{
			$("#comunica-descricao").fadeOut(function () {
				$("#ComuImg").fadeIn();
			});
		}
	}


</script>


<div id="usuario-container">

	<div id="usuario-box">
  		<p><a href="<?=site_url()?>home">Inicio</a><a href="javascript: VisualizarEmpreendimento();"><?=@$emp->nome?></a>   >  Comunicados	</p>

    	<div id="comunica-top">
        	<h2>Comunicados</h2>
        </div>
        
        <div id="comunica-criar">
			<div id="admin-ad-dispo-nome">
		        <p class="admin-add">Adicionar Comunicado</p>
		    </div>
		    <form id="FormComunicados" name="FormComunicados" method="post" enctype="multipart/form-data" action="<?=site_url()?>comunicados/salvar">
				<input type="hidden" name="id_comunicado" value="<?=@$comunicado->id_comunicado?>" >
				<ul>
					<li style="float:left; margin-left:-15px;">
	                    <label for="ativo">Ativo</label>
	                    <select name="ativo" id="ativo">
	                        <option <?php if(@$comunicado->ativo == "S") echo "selected='selected'"; ?> value="S">Sim</option>
	                        <option <?php if(@$comunicado->ativo == "N") echo "selected='selected'"; ?> value="N">Não</option>
	                    </select>
					</li>
					<li style="float:right">
	                    <label for="tipo">Tipo</label>
	                    <select id="tipo" name="tipo" onchange="MostraTipo(this.value);">
	                        <option <?php if(@$comunicado->tipo == "T") echo "selected='selected'"; ?> value="T">Texto</option>
	                        <option <?php if(@$comunicado->tipo == "I") echo "selected='selected'"; ?> value="I">Imagem</option>
	                    </select>
					</li>
	                
					<li style="float:left; margin-left:-15px; margin-top:20px;">
			            <label for="titulo">Título</label>
			            <input id="titulo" name="titulo" value="<?=@$comunicado->titulo?>" type="text" class="validate[required] categoria">
					
					</li>
					
					<li style="float:right; margin-top:20px;">
	                    <label for="id_empreendimento">Empreend.</label>
	                    <select id="id_empreendimento" name="id_empreendimento">
	                    	<option value="0">Todos</option>
	                    	<?php foreach (@$emps as $row): ?>
	                        <option <?php if(@$comunicado->id_empreendimento == @$row->id_empreendimento) echo "selected='selected'"; ?> value="<?=@$row->id_empreendimento?>"><?=@$row->nome?></option>
	                    	<?php endforeach; ?>
	                    </select>
					</li>
					
					<?php if(@$comunicado->tipo == "I") @$displayImg = "block"; else @$displayImg = "none"; ?>
					<div id="ComuImg" style="display:<?=@$displayImg?>">
						<div id="comunica-ad-arquivos-2">
				            <p style="margin-right: 12px;">Escolha um Arquivo do Computador</p>
				            <div style="margin-top:12px; margin-left:12px;"><input type="file" id="FileLogoEmp" name="imagem_destaque" value=""  /></div>
				        </div>
				        <?php if(@$comunicado->imagem_destaque): ?>
				        	<br style="clear:both;"/><img style="margin-left:30px; margin-top:5px;" src="<?=base_url()?>assets/manager/uploads/comunicados/<?=@$comunicado->imagem_destaque?>" width="100px;" >
				       	<?php endif; ?>
			       	</div>
					
					<?php if(@$comunicado->tipo == "T") @$displayTxt = "block"; else @$displayTxt = "none"; ?>
					<?php if(!@$comunicado->tipo) { @$displayTxt = "block"; } ?>
					<li id="comunica-descricao" style="clear:both; margin-left:30px; display:<?=@$displayTxt?>;">
			            <label for="descricao">Comunicado</label>
	                    <textarea name="descricao" id="descricao" class="comunica-categoria"><?=@$comunicado->descricao?></textarea>				
	                </li>
	   				
	   				<li style="float:right; margin-bottom:50px;">
	                    <a href="javascript: EnviarComunicado();" style="text-decoration:none">
	                        <div class="admin-btn-salvar"><p>Salvar</p></div>
	                    </a>
	                </li>
				</ul>
			
			</form>
			
        </div>

		<div id="usuario-table">
        	<?php if(@$comunicados->num_rows > 0): ?>
            <table cellspacing="10" class="tables">
                <tr>
                    <th class="comunica-table-name-1">Comunicado</th>
                    <th class="comunica-table-name-2">Ativo</th>
                    <th class="comunica-table-name-3">Data</th>
                    <th class="comunica-table-name-4"></th>
                </tr>
                <?php foreach ($comunicados->result() as $row): ?>
                <tr bgcolor="#9e9e9e">
                    <th class="comunica-table-1"><?=@$row->titulo?></th>
                    <th class="comunica-table-2"><?php if($row->ativo == "S") echo "Sim"; else echo "Não"; ?></th>
                    <th class="comunica-table-3"><?=date('d/m/Y', strtotime($row->data_hora))?></th>
                    <th class="comunica-table-4"><a href="<?=site_url()?>comunicados/editar/<?=$row->id_comunicado?>" class="usuario-admin-edit">Editar</a><a href="<?=site_url()?>comunicados/apagar/<?=$row->id_comunicado?>" onclick="return confirm('Tem certeza que deseja excluir o comunicado?');" class="usuario-admin-excluir">Excluir</a></th>
                </tr>
                <?php endforeach; ?>
             </table>
             <?php else: ?>
             
             	<h3>Nenhum comunicado encontrado.</h3>
             
             <?php endif; ?>
        </div>
        
    </div>
</div>

<?=$this->load->view('includes/footer');?>