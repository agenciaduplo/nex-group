<div id="emp-container-inter">
	<div id="emp-box">
		<div id="emp-top-inter" style="cursor:pointer;" onclick="javascript: return mostraImoveis();">
			<h2 id="expandirAba" style="font-size:24px;">Clique aqui para selecionar outro empreendimento</h2>
			<img id="menu" src="<?=base_url(); ?>assets/manager/img/conexao-ico-setadown.png" width="24" height="14" />
		</div>
		<a class="menuClick" href="#menu"></a>
		<div id="emp-recolher" style="display:none;">
			<div id="emp-in">
				<ul id="tooltipNex">
				<?php
				if($this->default_model->is_admin()){
					echo '
						<a href="',site_url(),'empreendimentos/cadastro">
							<li class="ad-emp">
								<img class="img-ad" src="',base_url(),'assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" />
								<p>Adicionar Empreendimento</p>
							</li>
						</a>
						<a href="',site_url(),'grupos/cadastro">
							<li class="ad-emp">
								<img class="img-ad" src="',base_url(),'assets/manager/img/conexao-ico-emp-ad.png" width="30" height="40" />
								<p>Adicionar<br />Pasta</p>
							</li>
						</a>
					';
				}
				$emps_outros = $this->default_model->getEmps();
				if(sizeof($emps_outros) > 0){
					foreach($emps_outros as $rows){
						if($this->default_model->is_admin() or ($rows->ativo == 'S' or $rows->ativo == 1)){
							if($rows->id == 1 && $rows->is_grupo == '0'){
								$url = site_url() . 'home/promocao';
							} else {
								$url = $rows->is_grupo == '1' ? site_url() . 'grupos/' . $rows->id : site_url() . 'empreendimentos/' . $rows->id ;
							}
							if($rows->is_grupo == '1' && $rows->ativo == 0){ $ativo = '<strong>OFF</strong>'; } elseif($rows->is_grupo == '0' && $rows->ativo == 'N'){ $ativo = '<strong>OFF</strong>'; } else { $ativo = ''; }
							echo '
								<a href="',$url,'" title="',$rows->titulo,'" style="cursor:pointer">
									<li>
										<img src="',base_url(),'assets/manager/uploads/logos/',$rows->logomarca,'" width="120" alt="',$rows->nome,'" />
										<div class="bug">
											',$rows->caracterizacao,'
											',$ativo,'
										</div>
									</li>
								</a>
							';
						}
					}
				}
				?>
				</ul>
			</div>
			<div id="fechar-emp-div"><a href="javascript: mostraImoveis();" class="fechar-emp">Fechar</a></div>
		</div>
	</div>
</div>