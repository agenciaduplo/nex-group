function imprimir(links)
{
    var URL = links;
    var W = window.open(URL);

    W.window.print(); // Is this the right syntax ? This prints a blank page and not the above URL
    return false;
}

function mostraCom()
{
    var cssDisplay = $("#notifica-open").css("display");

    if (cssDisplay == "none")
    {
        $("#controla-notifica").removeClass('notifica-off');
        $("#controla-notifica").addClass('notifica-on');
        $("#notifica-open").fadeIn();
    }
    else
    {
        $("#controla-notifica").removeClass('notifica-on');
        $("#controla-notifica").addClass('notifica-off');
        $("#notifica-open").fadeOut();
    }
}

function Login()
{
    $("#FormLogin").submit();
}

function EnviarImagem()
{
    $("#FormImagem").submit();
}

function EnviarBusca(){
  $("#FormBusca").submit();
}


function EnviarComunicado()
{
    $("#FormComunicados").submit();
}

function mostraImoveis()
{
    var cssDisplay = $("#emp-recolher").css("display");

    if (cssDisplay == "none")
    {
        //$("#emp-recolher").fadeIn();
        //$(".menuClick").click();

        $("#emp-recolher").fadeIn();
        $(".menuClick").click();
        //iniciarTooltip();

    }
    else
    {
        $("#emp-recolher").fadeOut();
        $(".menuClick").click();
    }
}

function trocaCidades(id)
{
    var id_estado = id;

    if (id_estado == '')
    {
        alert('Selecione um Estado!')
        return false;
    }
    else
    {
        var msg = '';
        vet_dados = 'id_estado=' + id_estado;

        base_url = "http://www.nexgroup.com.br/conexao/empreendimentos/buscar_cidades/";
        $.ajax({
            type: "POST",
            url: base_url,
            data: vet_dados,
            success: function(msg) {
                $("#cidades").html(msg);
                $('#cidades').selectBox('destroy');
                $("#cidades").selectBox();
            },
            error: function(request, status, error) {
                alert(request.responseText);
            }
        });
        return false;
    }
}

function EnviarArquivo()
{
    $("#FormArquivos").submit();
}

function EnviarDisponibilidade()
{
    $("#FormDisponibilidades").submit();
}

function recolherEmps()
{
    $("#emp-recolher").hide("blind", {direction: "vertical"}, 500);
}

function AtualizarEmpreendimento()
{
    $("#FormEmpreendimento").submit();
}

function EditarEmpreendimento()
{
    $(".menuClick").click();
    $(".visualizarEmpreendimento").fadeOut(function()
    {
        $(".ediarEmpreendimento").fadeIn();
    });
}

function ExcluirEmpreendimento(id) {
    if (confirm("Tem certeza que deseja excluir este empreendimento?")) {

        $.ajax({
            url: BASE_URL + "empreendimentos/excluir_empreendimento",
            type: 'POST',
            data: {id_empreendimento: id},
            dataType: 'json',
            success: function(data) {
                if (data == 1) {
                    alert("Empreendimento excluído com sucesso!");
                    window.location = BASE_URL + "home";
                }

                if (data == 2) {
                    alert("Falha ao excluir empreendimento!");
                }
            }
        });

    }
}

function ExcluirGrupoEmpreendimentos(id) {
    if (confirm("Tem certeza que deseja excluir este grupo de empreendimentos?")) {

        $.ajax({
            url: BASE_URL + "grupos/excluir_grupo",
            type: 'POST',
            data: {id_grupo: id},
            dataType: 'json',
            success: function(data) {
                if (data == 1) {
                    alert("Pasta de empreendimentos excluída com sucesso!");
                    window.location = BASE_URL + "home";
                }

                if (data == 2) {
                    alert("Falha ao excluir pasta de empreendimentos!");
                }
            }
        });
    }
}

function VisualizarEmpreendimento()
{
    $(".menuClick").click();
    $(".ediarEmpreendimento").fadeOut(function()
    {
        $(".visualizarEmpreendimento").fadeIn();
    });
}

function mostraUpDisponibilidades()
{
    var cssDisplay = $("#dispo-in").css("display");
    if (cssDisplay == "none")
    {
        $("#dispo-in").show("blind", {direction: "vertical"}, 500);
        $(".goDispoBox").click();
    }
    else
    {
        $("#dispo-in").hide("blind", {direction: "vertical"}, 500);
        $(".goDispoBox").click();
    }
}

function mostraUpArquivos()
{
    var cssDisplay = $("#atual-in").css("display");
    if (cssDisplay == "none")
    {
        $("#atual-in").show("blind", {direction: "vertical"}, 500);
        $(".goAtualBox").click();
    }
    else
    {
        $("#atual-in").hide("blind", {direction: "vertical"}, 500);
        $(".goAtualBox").click();
    }
}

jQuery(document).ready(function(){

  $("#fil_estado").change(function(){
    $("#FormFiltro").submit();
  });
  $("#fil_cidade").change(function(){
    $("#FormFiltro").submit();
  });
  $("#fil_tipo").change(function(){
    $("#FormFiltro").submit();
  });

  
    $(".modal-open").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 560,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });
    $('#tooltipNex a').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

    /*$(".video-fancybox").fancybox({
     'transitionIn'	: 'none',
     'transitionOut'	: 'none'
     });
     */

    $("a#modal-cadastro-abrir").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 560,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });

    $("a#modal-cadastro-abrir-update").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 560,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });

    $("a#delegar_contato").fancybox({
        'showCloseButton'   : false,
        'padding'           : 0,
        'transitionIn'      : 'fade',
        'transitionOut'     : 'fade',
        'autoScale'         : false,
        'overlayColor'      : '#000',
        'overlayOpacity'    : 0.6,
        'autoDimensions'    : false,
        'width'             : 770,
        'height'            : 580,
        'titleShow'         : false,
        'type'              : 'iframe',
        'onClosed': (function(){
                      location.reload();
                    })
    });

    $("a#contato_status_cad").fancybox({
        'showCloseButton'   : false,
        'padding'           : 0,
        'transitionIn'      : 'fade',
        'transitionOut'     : 'fade',
        'autoScale'         : false,
        'overlayColor'      : '#000',
        'overlayOpacity'    : 0.6,
        'autoDimensions'    : false,
        'width'             : 770,
        'height'            : 380,
        'titleShow'         : false,
        'type'              : 'iframe',
        'onClosed': (function(){
                      location.reload();
                    })
    });


    $("a#modal_edit_disponibilidade").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 490,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });

    $("a#modal-cadastro-prog-periodo").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 300,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });

    $("a#modal-cadastro-prog-inserir").fancybox({
        'showCloseButton': false,
        'padding': 0,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'autoScale': false,
        'overlayColor': '#000',
        'overlayOpacity': 0.6,
        'autoDimensions': false,
        'width': 770,
        'height': 950,
        'scrolling': 'no',
        'titleShow': false,
        'type': 'iframe'
    });



    jQuery("#cadPeriodos").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormArquivos").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormEmpreendimento").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormBusca").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormImagem").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormDisponibilidades").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormLogin").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    jQuery("#FormComunicados").validationEngine(
            'attach', {
                promptPosition: "topLeft"
            });

    $("#datapicker").datepicker({dateFormat: 'dd/mm/yy'});

    $("select").selectBox();

    $("#logo").click(function()
    {
        window.location.replace("http://www.nexgroup.com.br/conexao/home");
    });
});


// #Scroll to #anchor 3
/*
 $(document).ready(function() {
 
 $("a").click(function() {
 $("html, body").animate({
 scrollTop: $($(this).attr("href")).offset().top + "px"
 }, {
 duration: 400,
 easing: "swing"
 });
 return false;
 });
 
 });
 */

// #Scroll to #anchor 1

$(document).ready(function() {
    function filterPath(string) {
        return string
                .replace(/^\//, '')
                .replace(/(index|default).[a-zA-Z]{3,4}$/, '')
                .replace(/\/$/, '');
    }
    var locationPath = filterPath(location.pathname);
    var scrollElem = scrollableElement('html', 'body');

    $('a[href*=#]').each(function() {
        var thisPath = filterPath(this.pathname) || locationPath;
        if (locationPath == thisPath
                && (location.hostname == this.hostname || !this.hostname)
                && this.hash.replace(/#/, '')) {
            var $target = $(this.hash), target = this.hash;
            if (target) {
                var targetOffset = $target.offset().top;
                $(this).click(function(event) {
                    event.preventDefault();
                    $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
                        location.hash = target;
                    });
                });
            }
        }
    });

    // use the first element that is "scrollable"
    function scrollableElement(els) {
        for (var i = 0, argLength = arguments.length; i < argLength; i++) {
            var el = arguments[i],
                    $scrollElement = $(el);
            if ($scrollElement.scrollTop() > 0) {
                return el;
            } else {
                $scrollElement.scrollTop(1);
                var isScrollable = $scrollElement.scrollTop() > 0;
                $scrollElement.scrollTop(0);
                if (isScrollable) {
                    return el;
                }
            }
        }
        return [];
    }
});

// function DelegarSave(){
//   if($("#idusuario").val() == ""){
//     alert("Selecione um usuário para delegar este contato");
//     return false;
//   }else{
//     var msg = '';

//     base_url = BASE_URL+"vendas/delegar_venda/";
//     $.ajax({
//       type: "POST",
//       url: base_url,
//       data: vet_dados,
//       success: function(msg){
//         // $("#cidades").html(msg);
//         // $('#cidades').selectBox('destroy');
//         // $("#cidades").selectBox();
//       },
//       error: function(request, status, error){
//         alert(request.responseText);
//       }
//     });
//     return false;
//   }
// }

$(function(){
  $("#data_i").datepicker({dateFormat: "dd/mm/yy"});
  $("#data_f").datepicker({dateFormat: "dd/mm/yy"});
});


// Scroll to #anchor 2
/*
 $(function(){
 $('a[href*=#]:not([href=#])').click(function() {
 if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
 || location.hostname == this.hostname) {
 
 var target = $(this.hash);
 target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
 if (target.length) {
 $('html,body').animate({
 scrollTop: target.offset().top
 }, 1000);
 return false;
 }
 }
 });
 });
 */