function Login ()
{
	$("#FormLogin").submit();
}

function Cadastro ()
{
	$("#cadUsuarios").submit();
}

function RecuperarSenha ()
{
	$("#cadRecuperarSenha").submit();
}

function AceitoTermos ()
{
	$("#FormTermos").submit();
}

jQuery(document).ready(function()
{
	
	jQuery("#FormLogin").validationEngine(
		'attach', {
			promptPosition : "topLeft"		
	});
	
	jQuery("#cadUsuarios").validationEngine(
		'attach', {
			promptPosition : "topLeft"		
	});
	
	jQuery("#cadRecuperarSenha").validationEngine(
		'attach', {
			promptPosition : "topLeft"		
	});
	
	$("a#modal-cadastro-abrir").fancybox({
		'showCloseButton'	: false,
		'padding'			: 0,
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'autoScale'			: false,
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6,
		'autoDimensions'	: false,
		'width'         	: 770,
		'height'        	: 560,
		'scrolling'			:'no',
		'titleShow'			: false,
		'type'          	: 'iframe'
	});
	
	$("a#EsqueceuSenha").fancybox({
		'showCloseButton'	: false,
		'padding'			: 0,
		'transitionIn'		: 'fade',
		'transitionOut'		: 'fade',
		'autoScale'			: false,
		'overlayColor'		: '#000',
		'overlayOpacity'	: 0.6,
		'autoDimensions'	: false,
		'width'         	: 500,
		'height'        	: 330,
		'scrolling'			:'no',
		'titleShow'			: false,
		'type'          	: 'iframe'
	});
	
});