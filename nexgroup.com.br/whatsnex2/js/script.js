$('.btn-corretor-online').click(function(e){
	e.preventDefault();
	ga('send','event','chat','open', 'corretor');
	window.open($(this).attr('href'),'pop','width=674, height=497, top=100, left=100, scrollbars=no');
});

$('.telefone').mask('(00) 0000-00000', {placeholder: "Telefone"});

//VAlidação e envio do formulário de contato
$('#contact').validate({
	rules: {
		nome: {
			required: true,
			minlength: 5
		},
		telefone: {
			required: true
		}
	},
	messages: {
		nome: {
			required: "Por favor preencha seu nome.",
			minlength: "Seu nome deve ter mais caracteres."
		},
		telefone: {
			required: "Por favor, preencha seu telefone.",
			//minlength: "Sua mensagem deve ter mais caracteres."
		}
	},
	submitHandler: function(form) {
		$('.submit-input').attr('disabled', 'disabled');
		$('.submit-input').val('Enviando...');
		$('#contact').fadeTo( "slow", 0.15, function() {
			$(form).ajaxSubmit({
				type:"POST",
				data: $(form).serialize(),
				url:"envia.php",
				success: function(data) {
					//$('#conversion-wrap').load(urlPath+'conversion');
					$('.cont-form').html('<iframe src="conversion.html"></iframe>');
					
					//ga('send', 'event', 'meta', 'envio', 'form ' + $('#escolha').val());
					//console.log(data);
					//$('#contact :input').attr('disabled', 'disabled');
					
						//$(this).find(':input').attr('disabled', 'disabled');
						$('#contact').find('label').css('cursor','default');
						$('#success').fadeIn().delay(1800).fadeOut();
						$('#contact').delay(2400).fadeTo("slow", 1);
						//$('#contact')[0].reset();
						/*setTimeout(function () {
							$(".close").trigger('click');
						}, 3200);*/
					ga('send','event','lead','envia', 'form');
					fbq('track', 'Lead');
				},
				error: function(err) {
					$('#contact').fadeTo( "slow", 0.15, function() {
						$('#error').fadeIn().delay(1800).fadeOut();
						$(this).delay(2400).fadeTo("slow", 1);
					});
					console.log('Erro no envio do formulário: ');
					console.log(err);
				}
			});
		});
	}
});