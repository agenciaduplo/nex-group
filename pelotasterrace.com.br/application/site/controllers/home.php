<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

@session_start();

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index ($modal = "")
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace",
			'bodyClass'				=> "Home",
			'page'					=> "home",
			'modal'					=> $modal
		);
		
		$this->load->view('home/home', $data);
	}

	function decorados()
	{
		$this->load->model('home_model', 'model');

		$data = array(
			'title' 				=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Decorado",
			'bodyClass'				=> "Decorado",
			'page'					=> "decorados"
		);

		$this->load->view('home/decorados', $data);
	}

	function modal()
	{
		$this->load->view('home/modal');
	}
	
	function sucesso_vendas ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Sucesso de Vendas",
			'bodyClass'				=> "Home",
			'page'					=> "home"
		);
		
		$this->load->view('home/sucesso_vendas', $data);		
	}
	
	
	
	function empreendimento ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Empreendimento",
			'bodyClass'				=> "Empreendimento",
			'page'					=> "empreendimento"
		);
		
		$this->load->view('home/empreendimento', $data);
	}
	
	function plantas_baixas ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Plantas Baixas",
			'bodyClass'				=> "PlantasBaixas",
			'page'					=> "plantas_baixas"
		);
		
		$this->load->view('home/plantas_baixas', $data);
	}
	
	function localizacao ()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Localização",
			'bodyClass'				=> "Localizacao",
			'page'					=> "localizacao"
		);
		
		$this->load->view('home/localizacao', $data);
	}
	
	function contato()
	{
		$this->load->model('home_model', 'model');
		
		$data = array (
			'title'					=> "Apartamentos de 2 e 3 dormitórios em Pelotas - Pelotas Terrace - Contato",
			'bodyClass'				=> "Contato",
			'page'					=> "contato"
		);
		
		$this->load->view('home/contato', $data);
	}
	
	function enviarInteresse ()
	{
		$this->load->model('home_model', 'model');
		
		$id_empreendimento		= 33;
		$id_estado				= 21;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome 					= $this->input->post('nome');
		$email 					= $this->input->post('email');
		$telefone 				= $this->input->post('telefone');
		$faixa_etaria 			= $this->input->post('faixa_etaria');
		$estado_civil 			= $this->input->post('estado_civil');
		$profissao 				= $this->input->post('profissao');
		$forma_contato 			= $this->input->post('contato');
		$comentarios 			= $this->input->post('comentarios');
		$bairro_cidade 			= $this->input->post('bairro_cidade');
		$hotsite				= "S";
		
		$p = array (
			'id_empreendimento'		=> $id_empreendimento,
			'id_estado'				=> $id_estado,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome'					=> $nome,
			'email'					=> $email,
			'telefone'				=> $telefone,
			'faixa_etaria'			=> $faixa_etaria,
			'estado_civil'			=> $estado_civil,
			'profissao'				=> $profissao,
			'forma_contato'			=> $forma_contato,
			'comentarios'			=> $comentarios,
			'bairro_cidade'			=> $bairro_cidade,
			'hotsite'				=> $hotsite
		);
		
		$interesse 			= $this->model->setInteresse($p);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);

		$dados = array(
			'Enviado em ' => $p['data_envio'] . ' via HotSite',
			'Enviado por ' => $p['nome'],
			'Empreendimento' => $empreendimento->empreendimento . ' / ' . $empreendimento->cidade,
			'IP' => $p['ip'],
			'E-mail' => $p['email'],
			'Telefone' => $p['telefone'],
			('Comentários') => $p['comentarios']
		);
		
		//inicia o envio do e-mail
		//====================================================================
		
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] 	= 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);
		
		/* envia email via roleta */
		$id_empreendimento = 33;
		$grupos = $this->model->getGrupos($id_empreendimento);

		$total = 0;
		$total = count($grupos);
		
		foreach ($grupos as $row):

			if($row->rand == 1):
				$atual = $row->ordem;

				if($atual == $total):

					$this->model->updateRand($id_empreendimento, '001');

					$emails = $this->model->getGruposEmails($id_empreendimento, '001');
					//echo "<pre>";print_r($emails);echo "</pre>";

					$emails_txt = "";
					foreach ($emails as $email)
					{
						$grupo = $email->grupo;
						$list[] = $email->email;

						$emails_txt = $emails_txt.$email->email.", ";
					}
					$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

				else:

					$atualizar = "00".$atual+1;
					$this->model->updateRand($id_empreendimento, $atualizar);

					$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);
					//echo "<pre>";print_r($emails);echo "</pre>";

					$emails_txt = "";
					foreach ($emails as $email)
					{
						$grupo = $email->grupo;
						$list[] = $email->email;

						$emails_txt = $emails_txt.$email->email.", ";
					}
					$this->model->grupo_interesse($interesse, $grupo, $emails_txt);

				endif;	
			endif;
		endforeach;
		/* envia email via roleta */

		if($p['nome'] == "Teste123"){
			$this->email->to('testes@divex.com.br');
		} else {
			//$this->email->to($list);
                        $this->email->to('contatonex@hfm.imb.br, marketing@reweb.com.br');
		}
		//$this->email->bcc('testes@divex.com.br');
		$this->email->from("noreply@pelotasterrace.com.br", "PELOTAS TERRACE - Nex Group");
		$this->email->subject('Contato enviado via HotSite');
		$this->email->message($this->load->view('tpl/email',array('dados' => $dados),true));
		
		$this->email->send();

	}
	
	function enviarIndique ()
	{
		$this->load->model('home_model', 'model');
	
		$id_empreendimento 		= 33;
		$ip 					= $this->input->ip_address();
		$user_agent				= $this->input->user_agent();
		$url					= $_SESSION['url'];
		$origem					= $_SESSION['origem'];
		$data_envio				= date("Y-m-d H:i:s");
		$nome_remetente 		= $this->input->post('nome_remetente');
		$email_remetente 		= $this->input->post('email_remetente');
		$nome_destinatario 		= $this->input->post('nome_amigo');
		$email_destinatario		= $this->input->post('email_amigo');
		$comentarios 			= $this->input->post('comentarios');

		$data = array (
			'id_empreendimento'		=> $id_empreendimento,
			'ip'					=> $ip,
			'user_agent'			=> $user_agent,
			'url'					=> $url,
			'origem'				=> $origem,
			'data_envio'			=> $data_envio,
			'nome_remetente'		=> $nome_remetente,
			'email_remetente'		=> $email_remetente,
			'nome_destinatario'		=> $nome_destinatario,
			'email_destinatario'	=> $email_destinatario,
			'comentarios'			=> $comentarios
		);
		
		$indique 			= $this->model->setIndique($data);
		$empreendimento		= $this->model->getEmpreendimento($id_empreendimento);
		
		//Inicia o envio do email
		//===================================================================
		
		$this->load->library('email');
		
		$config['protocol'] 	= 'sendmail';
		$config['charset'] 		= 'utf-8';
		$config['wordwrap'] 	= TRUE;
		
		$this->email->initialize($config);
		
		//Inicio da Mensagem
		
		ob_start();
		
		?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>PELOTAS TERRACE</title>
			</head>
			
			<body>
			<table width="500px" border="0" style="font-family:Tahoma; font-size:11px;">
			  <tr align="center">
			  	<td colspan="2" align="center"><a href="http://www.nexgroup.com.br" target="_blank"><img width="90px" border="0" src="http://www.nexgroup.com.br/assets/admin/img/logo_nex-02.jpg" alt="NEX GROUP" /></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>					  
			  <tr>
			    <td colspan="2"><b>PELOTAS TERRACE</b> :: Indicação via HotSite</td>
			  </tr>
			  <tr>
			    <td>Indicação enviada por:</td>
			  	<td> <strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong> em <strong><?=date('d/m/Y')?></strong> as <strong><?=date('H:i:s')?></strong></td>
			  </tr>
			  <tr>
			    <td>Para:</td>
			  	<td> <strong><?=@$nome_destinatario?> / <?=@$email_destinatario?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">IP:</td>
			    <td><strong><?=$this->input->ip_address()?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>			  
			  <tr>
			    <td colspan="2">Olá <?=@$nome_destinatario?>, acesse o HotSite do PELOTAS TERRACE<br/><strong><a href="http://www.pelotasterrace.com.br">Clique aqui</a></strong></td>
			  </tr>
			  <tr>
			    <td width="30%">&nbsp;</td>
			    <td width="70%">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="30%">Nome / E-mail do remetente:</td>
			    <td><strong><?=@$nome_remetente?> / <?=@$email_remetente?></strong></td>
			  </tr>
			  <tr>
			    <td width="30%" valign="top">Comentários:</td>
			    <td valign="top"><strong><?=$comentarios?></strong></td>
			  </tr>					  					  					  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr align="center">
			    <td colspan="2" align="center">
			    	NEX GROUP
			    	<a href="http://www.capa.com.br">www.nexgroup.com.br</a>	
			    </td>
			  </tr>		  
			</table>
			</body>
			</html>
		<?
		
		$conteudo = ob_get_contents();
		ob_end_clean();
		//Fim da Mensagem
		
		//roleta
		$grupos = $this->model->getGrupos($id_empreendimento);
		$total = 0;
		$total = count($grupos);
		
		if($total == 1) // caso só tenho um grupo cadastrado
		{
			$emails = $this->model->getGruposEmails($id_empreendimento, '001');
			
			$emails_txt = "";
			foreach ($emails as $email)
			{
				$grupo = $email->grupo;
				$list[] = $email->email;

				$emails_txt = $emails_txt.$email->email.", ";
			}
			$this->model->grupo_indique($indique, $grupo, $emails_txt);

		}
		else if($total > 1) // caso tenha mais de um grupo cadastrado
		{
			foreach ($grupos as $row):

				if($row->rand == 1):
					$atual = $row->ordem;
					if($atual == $total):

						$this->model->updateRand($id_empreendimento, '001');
						$emails = $this->model->getGruposEmails($id_empreendimento, '001');
						
						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);
					
					else:

						$atualizar = "00".$atual+1;
						$this->model->updateRand($id_empreendimento, $atualizar);
						$emails = $this->model->getGruposEmails($id_empreendimento, $atualizar);

						$emails_txt = "";
						foreach ($emails as $email)
						{
							$grupo = $email->grupo;
							$list[] = $email->email;

							$emails_txt = $emails_txt.$email->email.", ";
						}
						$this->model->grupo_indique($indique, $grupo, $emails_txt);

					endif;	
				endif;
			endforeach;
		}
		/* envia email via roleta */
		
		
		$this->email->from("noreply@pelotasterrace.com.br", "$nome_remetente");
		if($nome_remetente=='teste123' || $nome_remetente=='TESTE123'){
			$this->email->to('bruno.freiberger@divex.com.br');	
		}else{
			$this->email->to($email_destinatario);
		}
		$this->email->bcc("testes@divex.com.br");		
		$this->email->subject("Pelotas Terrace - Indicação enviada para você");
		$this->email->message("$conteudo");
		$this->email->send();

		//envia para corretores
		$this->email->from("noreply@pelotasterrace.com.br", "$nome_remetente");
		//$this->email->to($list);
                $this->email->to('contatonex@hfm.imb.br, marketing@reweb.com.br');
		//$this->email->cc("testes@divex.com.br");
		$this->email->subject("Pelotas Terrace - Indique enviado via site");
		$this->email->message("$conteudo");
		$this->email->send();
		//envia para corretores
		
		//===================================================================
		//Termina o envio do email	
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */