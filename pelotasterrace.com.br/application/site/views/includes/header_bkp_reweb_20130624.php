<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	
	<title><?=@$title?></title>
	
	<meta name="description" content="Terrace Pelotas - Apartamentos com 3 dormitórios (1 suíte) no prédio mais alto de Pelotas. Área de lazer com vista de 360º na cobertura." />
    <meta name="keywords" content="apartamento 3 dormitórios, apartamento 3 quatros, apartamento com suíte, apto 3 dorms, apto 3 quartos, apto com suíte, pelotas, pre-lançamento" />
    <meta name="author" content="Divex Imobi - www.imobi.divex.com.br" />
	<meta name="copyright" content="&copy; 2011 NEX GROUP" />
	<meta name="robots" content="index, follow" />
	<meta name="language" content="pt-br" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta name="google-site-verification" content="j0rnFAxs9O9Q1L89XH7oo-d96D4zyMzFqJJP0WPQH8k" />
	
	<!-- Like Facebook -->
	<meta property="og:title" content="Pelotas Terrace - apartamentos de 3 dormitórios com suíte" />
	<meta property="og:type" content="company" />
	<meta property="og:url" content="http://www.pelotasterrace.com.br/" />
	<meta property="og:image" content="http://www.pelotasterrace.com.br/assets/img/site/logo-footer.png" />
	<meta property="og:site_name" content="Pelotas Terrace" />
	<meta property="fb:admins" content="1528287035" />
	
	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/fancybox.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/jcarousel.css" type="text/css" media="screen" />
	
	<!--[if IE 6]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie6.css" type="text/css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?=base_url()?>assets/css/site/ie7.css" type="text/css" media="screen" /><![endif]-->

	
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.jcarousel.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery.maskedinput-1.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/common.js"></script>
	
	<?php if(@$page == "contato") : ?>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/custom.contato.js"></script>
	<?php elseif(@$page == "empreendimento"): ?>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/custom.empreendimento.js"></script>
	<?php elseif(@$page == "localizacao"): ?>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/custom.localizacao.js"></script>
	<?php elseif(@$page == "decorados"): ?>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/custom.decorado.js"></script>
	
	<?php elseif(@$page == "plantas_baixas"): ?>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/custom.plantas.js"></script>
	<?php endif; ?>


	
	<!-- Social -->
	<script src="http://connect.facebook.net/pt_BR/all.js#xfbml=1"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"> {lang: 'pt-BR'} </script>
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e347eb7496ef1d5"></script>
	<!-- Social -->
	

	<!--[if IE 6]>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/dd-belated-png.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			DD_belatedPNG.fix('.pngfix, .CampoPadrao');
		});
	</script>
	<![endif]-->
    
    <?php if(@$modal != "n" && $this->uri->segment(1) == "home" || $this->uri->segment(1) == "") : ?>
    
    <script>
		
		jQuery(document).ready(function() {
			
			$("#abreModal").fancybox({
				'autoDimensions'	: true,
				'scrolling'		: 'no'
			});
		});
	</script>
    <?php endif; ?>

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-47']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	
</head>
<?php 

		@$utm_source 	= @$_GET['utm_source'];
		@$utm_medium 	= @$_GET['utm_medium'];
		@$utm_content 	= @$_GET['utm_content'];
		@$utm_campaign 	= @$_GET['utm_campaign'];
		
		@$url = $utm_source."__".$utm_medium."__".$utm_content."__".$utm_campaign;
		
		
		@session_cache_expire(1440);
		@session_start();
		
		
		if(!@$_SESSION['origem'])
		{
			@$_SESSION['origem']	= @$_SERVER['HTTP_REFERER'];
		}
		if(!@$_SESSION['url'])
		{
			@$_SESSION['url']		= @$url;
		}

?>
<body class="<?=@$bodyClass?>">

	<div id="Panels">
		<div id="PainelTenhoInteresse" class="SlidingPanel">
			<div class="Container">
				<h5 class="pngfix">Preencha nosso formulário de interesse para que possamos auxilia-lo a fechar o melhor negócio da sua vida.</h5>
				<form id="FormInteresse" class="Form" method="post" action="">
					<ul class="First">
						<li>
							<label for="Nome">nome</label>
							<input type="text" id="Nome" class="CampoPadrao" name="nome" />
						</li>
						<li>
							<label for="FaixaEtaria" class="hfixie">faixa etária</label>
							<select id="FaixaEtaria" name="faixa_etaria">
								<option value=""></option>
								<option value="Até 30 anos">At&eacute; 30 anos</option>
								<option value="De 31 a 40 anos">De 31 a 40 anos</option>
                                <option value="De 41 a 50 anos">De 41 a 50 anos</option>
                                <option value="Mais de 50 anos">Mais de 50 anos</option>
							</select>
						</li>
						<li>
							<label for="EstadoCivil" class="hfixie">estado civil</label>
							<select id="EstadoCivil" name="estado_civil">
								<option value=""></option>
								<option value="Casado">casado</option>
                                <option value="Solteiro">solteiro</option>
							</select>
						</li>
						<li>
							<label for="Cidade">cidade</label>
							<input type="text" id="Cidade" class="CampoPadrao" name="cidade" />
						</li>
					</ul>
					<ul class="Second">
						<li>
							<label for="Profissao">profissão</label>
							<input type="text" id="Profissao" class="CampoPadrao" name="profissao" />
						</li>
						<li>
							<label for="Telefone">telefone</label>
							<input type="text" id="Telefone" class="CampoPadrao" name="telefone" />
						</li>
						<li>
							<label for="Email">e-mail</label>
							<input type="text" id="Email" class="CampoPadrao" name="email" />
						</li>
						<li>
							<div class="CheckGroup">
								<span class="LabelOptions">contato por</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkTelefone" name="ChkTelefone" class="Check" value="Telefone" />
									<label for="ChkTelefone" class="Choice">telefone</label>
								</span>
								<span class="CheckOption">
									<input type="checkbox" id="ChkEmail" name="ChkEmail" class="Check" value="E-mail" />
									<label for="ChkEmail" class="Choice">e-mail</label>
								</span>
							</div> <!-- .CheckGroup -->
						</li>
					</ul>
					<ul class="Left Last">
						<li>
							<label for="Comentario">comentário</label>
							<textarea id="Comentario" class="ComentarioInteresse CampoPadrao" name="comentario" cols="40" rows="8"></textarea>
						</li>
						<li><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
					</ul>
				</form> <!-- #FormInteresse -->
			</div> <!-- .Container -->
		</div> <!-- #PainelTenhoInteresse -->
		<div id="PainelIndique" class="SlidingPanel">
			<div class="Container">
				<h5 class="pngfix">Envie uma mensagem diretamente para os seus amigos e convide-os a conhecer este empreendimento.</h5>
				<form id="FormIndique" class="Form" method="post" action="">
					<ul class="First">
						<li>
							<label for="NomeRemetente">seu nome</label>
							<input type="text" id="NomeRemetente" class="CampoPadrao" name="nomeRemetente" />
						</li>
						<li>
							<label for="EmailRemetente">seu e-mail</label>
							<input type="text" id="EmailRemetente" class="CampoPadrao" name="emailRemetente" />
						</li>
						<li>
							<label for="NomeAmigo">nome do amigo</label>
							<input type="text" id="NomeAmigo" class="CampoPadrao" name="nomeAmigo" />
						</li>
						<li>
							<label for="EmailAmigo">e-mail do amigo</label>
							<input type="text" id="EmailAmigo" class="CampoPadrao" name="emailAmigo" />
						</li>
					</ul>
					<ul class="Second">
						<li>
							<label for="Mensagem">mensagem</label>
							<textarea id="Mensagem" class="MensagemIndique CampoPadrao" name="mensagem" cols="40" rows="8"></textarea>
						</li>
						<li><input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" /></li>
					</ul>
				</form> <!-- #FormIndique -->
			</div> <!-- .Container -->
		</div> <!-- #PainelIndique -->
	</div> <!-- #Panels -->

	<div class="Main">
		<div class="Header">
			<div class="Topbar pngfix">
				<div class="Container">
					<ul class="Features">
						<li class="First"><span class="Tel pngfix">(53) 3028.4545</span></li>
						<li class="Second"><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
						<li class="Third"><a href="#" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
						<li><a href="#" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
					</ul> <!-- .Features -->
				</div> <!-- .Container -->
			</div> <!-- .Topbar -->
			<div class="Container">
				<h1 class="Logo pngfix">Pelotas Terrace - apartamentos de 3 dormitórios com suíte</h1>
				<ul class="Navigation pngfix">
					<li class="ItemHome"><a href="<?=site_url()?>" class="pngfix" title="Voltar para home">Home</a></li>
					<li class="ItemEmpreendimento"><a href="<?=site_url()?>empreendimento" class="pngfix" title="Empreendimento">Empreendimento</a></li>
					<li class="ItemPlantas"><a href="<?=site_url()?>plantas-baixas" class="pngfix" title="Plantas baixas">Plantas baixas</a></li>
					<li class="ItemDecorado"><a href="<?=site_url()?>decorado" class="pngfix" title="Decorado">Decorado</a></li>
					<li class="ItemLocalizacao"><a href="<?=site_url()?>localizacao" class="pngfix" title="Localização">Localização</a></li>
					<li class="ItemContato Last"><a href="<?=site_url()?>contato" class="pngfix" title="Contato">Contato</a></li>
				</ul> <!-- .Navigation -->
				<h2 class="Emphasis pngfix">Um novo horizonte está à sua espera</h2>
			</div> <!-- .Container -->
		</div> <!-- .Header -->
        
        <div id="dias" style="display:none;">
        	<?php
        		$datas = array(
        					"2012-05-17" => "11",
        					"2012-05-18" => "10",
        					"2012-05-19" => "09",
        					"2012-05-20" => "08",
        					"2012-05-21" => "07",
        					"2012-05-22" => "06",
        					"2012-05-23" => "05",
        					"2012-05-24" => "04",
        					"2012-05-25" => "03",
        					"2012-05-26" => "02",
        					"2012-05-27" => "01"
        				);
        		$hoje = date("Y-m-d");
        		//echo $datas["$hoje"];
        	?>
        
        </div>
        
        <!--<a rel="fancybox" id="abreModal" href="#modalimage"></a>-->
        <div style="display:none"><a id="modalimage" style="width:500px; height:490px;" href='<?=base_url()?>sucesso-de-vendas'><p style="font-size:70px; font-weight:bold; color:#15524b; position:absolute; top:185px; left:275px;"><?=@$datas["$hoje"]?></p><img width="500" height="490" src='<?=base_url()?>assets/img/site/selos-af.jpg' />
        </a>
		</div>
		