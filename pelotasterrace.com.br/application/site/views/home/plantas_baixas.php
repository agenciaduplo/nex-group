<?=$this->load->view('includes/header');?>
		<div class="Content pngfix">
			<div class="MainContent pngfix">
				<div class="Container">
					<h3 class="SectionTitle pngfix">Plantas baixas</h3>
					<ul class="ListaPlantas">
						<!--li>
							<div class="BoxPlanta Planta1 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Americana</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P1_3dormitorios.jpg" title="*cozinha americana posi&ccedil;&atilde;o ponta finais 01, 05 e 09 "><img src="<?=base_url()?>assets/img/site/planta-01.png" alt="" class="ThumbPlantaImpar" width="106" height="118" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P2_3dormitorios.jpg" title="*cozinha americana posi&ccedil;&atilde;o ponta finais 02, 06 e 10"><img src="<?=base_url()?>assets/img/site/planta-02.png" alt="" width="110" height="122" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), com cozinha americana. Posi&ccedil;&atilde;o ponta. Apartamentos com finais: 01, 05, 09 - 02, 06 e 10. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta2 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Americana</h4>
								<p>Posição Ponta (finais 03, 07, 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P9_3dormitorios.jpg" title="*cozinha americana posi&ccedil;&atilde;o meio finais 03, 07 e 11"><img src="<?=base_url()?>assets/img/site/planta-03.png" alt="" class="ThumbPlantaPar" width="151" height="75" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P10_3dormitorios.jpg" title="*cozinha americana posi&ccedil;&atilde;o meio finais 04, 08 e 12"><img src="<?=base_url()?>assets/img/site/planta-04.png" alt="" width="157" height="78" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), com cozinha americana.  Posi&ccedil;&atilde;o meio. Apartamentos com finais: 03, 07, 11 - 04, 08 e 12.  Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>

							</div>
						</li>
						<li>
							<div class="BoxPlanta Planta3 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Fechada</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P3_3dormitorios.jpg" title="*cozinha fechada posi&ccedil;&atilde;o ponta finais 01, 05 e 09 "><img src="<?=base_url()?>assets/img/site/planta-05.png" alt="" class="ThumbPlantaImpar" width="108" height="120" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P4_3dormitorios.jpg" title="*cozinha fechada posi&ccedil;&atilde;o ponta finais 02, 06 e 10"><img src="<?=base_url()?>assets/img/site/planta-06.png" alt="" width="110" height="122" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), com cozinha fechada. Posi&ccedil;&atilde;o ponta. Apartamentos com finais: 01, 05, 09 - 02, 06 e 10. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta4 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Fechada</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P11_3dormitorios.jpg" title="*cozinha fechada posi&ccedil;&atilde;o meio finais 03, 07 e 11"><img src="<?=base_url()?>assets/img/site/planta-07.png" alt="" class="ThumbPlantaPar" width="153" height="76" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P12_3dormitorios.jpg" title="*cozinha fechada posi&ccedil;&atilde;o meio finais 04, 08 e 12"><img src="<?=base_url()?>assets/img/site/planta-08.png" alt="" width="156" height="77" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), com cozinha fechada.  Posi&ccedil;&atilde;o meio. Apartamentos com finais: 03, 07, 11 - 04, 08 e 12. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li-->
						<!--li>
							<div class="BoxPlanta Planta5 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Americana</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P1_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha americana posi&ccedil;&atilde;o ponta finais 01, 05 e 09"><img src="<?=base_url()?>assets/img/site/planta-09.png" alt="" class="ThumbPlantaImpar" width="107" height="119" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P6_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha americana posi&ccedil;&atilde;o ponta finais 02, 06 e 10"><img src="<?=base_url()?>assets/img/site/planta-10.png" alt="" width="109" height="122" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), op&ccedil;&atilde;o living estendido - cozinha americana.  Posi&ccedil;&atilde;o ponta. Apartamentos com finais: 01, 05, 09 - 02, 06 e 10. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li-->
						<li>
							<div class="BoxPlanta Planta6 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Americana</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P13_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha americana posi&ccedil;&atilde;o meio finais 03, 07 e 11"><img src="<?=base_url()?>assets/img/site/planta-11.png" alt="" class="ThumbPlantaPar" width="153" height="76" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P14_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha americana posi&ccedil;&atilde;o meio finais 04, 08 e 12"><img src="<?=base_url()?>assets/img/site/planta-12.png" alt="" width="156" height="77" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), op&ccedil;&atilde;o living estendido - cozinha americana.  Posi&ccedil;&atilde;o meio. Apartamentos com finais: 03, 07, 11 - 04, 08 e 12. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li>
						<!--li>
							<div class="BoxPlanta Planta7 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Fechada</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P7_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha fechada posi&ccedil;&atilde;o ponta finais 01, 05 e 09 "><img src="<?=base_url()?>assets/img/site/planta-13.png" alt="" class="ThumbPlantaImpar" width="107" height="119" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P8_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha fechada posi&ccedil;&atilde;o ponta finais 02, 06 e 10"><img src="<?=base_url()?>assets/img/site/planta-14.png" alt="" width="109" height="122" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), op&ccedil;&atilde;o living estendido - cozinha fechada.  Posi&ccedil;&atilde;o ponta. Apartamentos com finais: 01, 05, 09 - 02, 06 e 10. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li-->
						<li class="Last">
							<div class="BoxPlanta Planta8 pngfix">
								<div>
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Fechada</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P15_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha fechada posi&ccedil;&atilde;o meio finais 03, 07 e 11"><img src="<?=base_url()?>assets/img/site/planta-15.png" alt="" class="ThumbPlantaPar" width="154" height="77" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P16_living estendido.jpg" title="*op&ccedil;&atilde;o living estendido - cozinha fechada posi&ccedil;&atilde;o meio finais 04, 08 e 12"><img src="<?=base_url()?>assets/img/site/planta-16.png" alt="" width="156" height="78" /></a>
								</div>
								<p class="Exception"><strong>Ilustra&ccedil;&otilde;es art&iacute;sticas dos apartamentos de 3 dormit&oacute;rios (1 su&iacute;te), op&ccedil;&atilde;o living estendido - cozinha fechada.  Posi&ccedil;&atilde;o meio. Apartamentos com finais: 03, 07, 11 - 04, 08 e 12. Os m&oacute;veis e objetos de decora&ccedil;&atilde;o aqui apresentados s&atilde;o de car&aacute;ter ilustrativo. N&atilde;o fazendo parte do contrato de aquisi&ccedil;&atilde;o do im&oacute;vel.</strong></p>
							</div>
						</li>
					</ul> <!-- .ListaPlantas -->
					<!--span style="width:100%;float:left;text-align:center">teste</span-->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground" class="SecondItem pngfix">Foto 2</a></li>
							<li><span class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</span></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>