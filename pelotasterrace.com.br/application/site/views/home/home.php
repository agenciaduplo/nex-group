<?=$this->load->view('includes/header');?>

		<div id="popupCorretorOverlay"></div>
		<a id="popupCorretor" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);closePopUp();">Corretor Online</a>

		<div class="Content pngfix">
			<div class="MainContent">
				<div class="Container">
					<div class="Featured">
						<div class="Predio pngfix">
							<h3 class="pngfix">O prédio mais alto de Pelotas</h3>
						</div>
						<span class="Fitness pngfix">Fitness</span>
						<span class="SalaoFestas pngfix">Salão de festas</span>
						<span class="Piscina pngfix">Piscina</span>
						<span class="SalaoInfantil pngfix">Salão de festas infantil</span>
					</div> <!-- .Featured -->
					<div class="Qualities">
						<h4 class="Terreno pngfix">Terreno com 3 frentes</h4>
						<ul class="Streets pngfix">
							<li>Rua Golçalves Chaves</li>
							<li>Rua Rafael Pinto Bandeira</li>
							<li>Rua Juscelino K. de Oliveira</li>
						</ul>
						<h4 class="AreaLazer pngfix">Área de lazer com vista de 360&deg;</h4>
						<div class="OpcoesPlantas pngfix" style="text-indent: 0;">
							<h4 class="pngfix" style="background: none"><a href="<?php echo site_url().'plantas-baixas'?>" class="pngfix" style="background: none; font-size: 25px; color: #fff; text-decoration: none;">Consulte as plantas do empreendimento</a></h4>
							
						</div> <!-- .OpcoesPlantas -->
						<ul class="ContactOptions pngfix">
							<li class="PlantaoVendas pngfix">
								<p>Visite plantão de vendas e apartamento decorado:</p>
								<p>Rua Gonçalves Chaves, esquina com Rua Rafael Pinto Bandeira</p>
							</li>
							<li class="Telefone pngfix">53 3028.4545</li>
							<li class="Endereco pngfix">
								<p>Av. Presidente Juscelino K. de Oliveira n° 3.161 esquina com Rua Rafael Pinto Bandeira - Pelotas - RS <a href="<?php echo site_url().'localizacao'?>" class="pngfix">Ver no mapa</a></p>
							</li>
						</ul> <!-- .ContactOptions -->
					</div> <!-- .Qualities -->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground" class="SecondItem pngfix">Foto 2</a></li>
							<li><span class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</span></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>



     <!-- ************ INTERTITIAL ************ -->
            <div onClick="closePromo();" class="btn-fechar"></div>
            <a href="http://www.nexgroup.com.br/tourvirtual/terrace-pelotas/" class="bloco" target="_blank"></a>
            
            <script>
                $(function(){
                    $('.bloco').delay(1500).fadeIn(500);
                });
                function closePromo() {
                    $('.bloco').fadeOut('slow');
                    $('.btn-fechar').fadeOut('slow');
                }
            </script>
    
            <style>
                .bloco {
                    position:fixed;
                        bottom:0;
                        left:0;
                    width:291px;
                    height:291px;
                    background:url("http://www.pelotasterrace.com.br/assets/img/site/tour/bloco.png") no-repeat;
                    z-index:9001;
                    display:none;
                    cursor:pointer;
                }
                .btn-fechar { 
                    position:fixed;
						bottom: 198px;
						left: 199px;
                    z-index:9002;
					width: 35px;
					height: 35px;
	                background:url("http://www.pelotasterrace.com.br/assets/img/site/tour/fechar.png") no-repeat;
                    cursor:pointer;
                 }
            </style>
     <!-- ************ INTERTITIAL ************ -->
