<?=$this->load->view('includes/header');?>

		<div class="Content pngfix">
			<div class="MainContent pngfix">
				<div class="Container">
					<h3 class="SectionTitle pngfix">Plantas baixas</h3>
					<ul class="ListaPlantas">
						<li>
							<div class="BoxPlanta Planta1 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Americana</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P1_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-01.png" alt="" class="ThumbPlantaImpar" width="106" height="118" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P2_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-02.png" alt="" width="110" height="122" /></a>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta2 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Americana</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P9_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-03.png" alt="" class="ThumbPlantaPar" width="151" height="75" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P10_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-04.png" alt="" width="157" height="78" /></a>
							</div>
						</li>
						<li>
							<div class="BoxPlanta Planta3 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Fechada</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P3_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-05.png" alt="" class="ThumbPlantaImpar" width="108" height="120" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P4_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-06.png" alt="" width="110" height="122" /></a>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta4 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Cozinha Fechada</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P11_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-07.png" alt="" class="ThumbPlantaPar" width="153" height="76" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P12_3dormitorios.jpg" title="3 Dorms. (1 suíte) Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-08.png" alt="" width="156" height="77" /></a>
							</div>
						</li>
						<li>
							<div class="BoxPlanta Planta5 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Americana</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P1_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-09.png" alt="" class="ThumbPlantaImpar" width="107" height="119" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P6_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-10.png" alt="" width="109" height="122" /></a>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta6 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Americana</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P13_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-11.png" alt="" class="ThumbPlantaPar" width="153" height="76" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P14_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Americana"><img src="<?=base_url()?>assets/img/site/planta-12.png" alt="" width="156" height="77" /></a>
							</div>
						</li>
						<li>
							<div class="BoxPlanta Planta7 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Fechada</h4>
								<p>Posição Ponta (finais 01, 05 e 09 - 02, 06 e 10)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P5_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-13.png" alt="" class="ThumbPlantaImpar" width="107" height="119" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P6_living estendido.jpg" title="3 Dorms. (1 suíte) Opção Living Estendido - Cozinha Fechada"><img src="<?=base_url()?>assets/img/site/planta-14.png" alt="" width="109" height="122" /></a>
							</div>
						</li>
						<li class="Last">
							<div class="BoxPlanta Planta8 pngfix">
								<h3 class="pngfix">3 dormitórios (1 suíte)</h3>
								<h4>Opção Living Estendido - Cozinha Fechada</h4>
								<p>Posição Meio (finais 03, 07 e 11 - 04, 08 e 12)</p>
								<a href="<?=base_url()?>assets/uploads/plantas/P15_living estendido.jpg" title="3 Dorms. (1 suíte) "><img src="<?=base_url()?>assets/img/site/planta-15.png" alt="" class="ThumbPlantaPar" width="154" height="77" /></a>
								<a href="<?=base_url()?>assets/uploads/plantas/P16_living estendido.jpg" title="3 Dorms. (1 suíte) "><img src="<?=base_url()?>assets/img/site/planta-16.png" alt="" width="156" height="78" /></a>
							</div>
						</li>
					</ul> <!-- .ListaPlantas -->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="" class="SecondItem pngfix">Foto 2</a></li>
							<li><a href="" class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</a></li>
							<li><a href="" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="#" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>