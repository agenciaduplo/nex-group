<?=$this->load->view('includes/header');?>

		<div class="Content pngfix">
			<div class="MainContent pngfix">
				<div class="Container">
					<h3 class="SectionTitle pngfix">Empreendimento</h3>
					<ul class="Gallery">
						<li><a href="<?=base_url()?>assets/uploads/imagens/Fachada.JPG" title="Fachada"><img src="<?=base_url()?>assets/img/site/thumb-fachada.jpg" alt="Fachada" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Portaria.jpg" title="Portaria"><img src="<?=base_url()?>assets/img/site/thumb-portaria.jpg" alt="Portaria" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_adulto.jpg" title="Salão de festas adulto"><img src="<?=base_url()?>assets/img/site/thumb-salao-festas-adulto.jpg" alt="Salão de festas adulto" width="226" height="226" /></a></li>
						<li class="Last"><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil"><img src="<?=base_url()?>assets/img/site/thumb-salao-festas-infantil.jpg" alt="Salão de festas infantil" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Piscina.jpg" title="Piscina" ><img  src="<?=base_url()?>assets/img/site/thumb-piscina.jpg" alt="Piscina" width="225" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Fitness.jpg" title="Fitness"><img src="<?=base_url()?>assets/img/site/thumb-fitness.jpg" alt="Fitness" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground"><img src="<?=base_url()?>assets/img/site/thumb-playground.jpg" alt="Playground" width="226" height="226" /></a></li>				
						<li class="Last"><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva"><img src="<?=base_url()?>assets/img/site/thumb-quadra-gazebo.jpg" alt="Gazebo e Quadra Poliesportiva" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras"><img src="<?=base_url()?>assets/img/site/thumb-churrasqueira.jpg" alt="Parrilleras e Churrasqueiras" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/implantacao.jpg" title="Implantação Geral"><img src="<?=base_url()?>assets/img/site/thumb-implantacao.jpg" alt="Implantação Geral" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Terraco.jpg"  title="Implantação Terraço"><img src="<?=base_url()?>assets/img/site/thumb-terraco.jpg" alt="Implantação Terraço" width="226" height="226" /></a></li>
					</ul> <!-- .Gallery -->
					<div class="Presentation DefaultBox pngfix">
						<p class="First pngfix">O <strong>Pelotas Terrace</strong> é o prédio mais alto da cidade e além disso, possui uma área de lazer na cobertura com uma vista de 360º de Pelotas.<p>
						<p>O prédio é constituído de dois blocos arquitetônicos independentes: um residencial e outro de estacionamentos.</p>
						<p>No bloco residencial são 17 andares, num total de 204 apartamentos de 3 dormitórios, cada um com estar/jantar, dormitório com suíte e banho privativo, dois dormitórios, banho social, cozinha e área de serviço, com consumo individualizado de água fria e gás, além de área de uso comum no térreo e na cobertura.</p>
						<p>O bloco de estacionamento possui no térreo 106 boxes simples cobertos, e no 2° pavimento 98 boxes simples cobertos.</p>
					</div> <!-- .Presentation -->
					<div class="DefaultBox pngfix">
						<div class="EstruturaLazer">
							<h4 class="pngfix">Estrutura de lazer</h4>
							<p class="First pngfix"><strong>Térreo:</strong> Churrasqueiras, pergolados Parrilleras, Gazebo, Quadra Poliesportiva, Playground e Salão de Festas Infantil.</p>
							<p><strong>Cobertura:</strong> Terraço com deck e Piscina adulto, Raia, Piscina Infantil e Prainha, Terraços abertos, Vestiários (feminino e masculino), Salão de Festas Adulto e Fitness.</p>
							<h5><strong>Estrutura condominial</strong></h5>
							<ul>
								<li>Pórtico com guarita e sala de espera</li>
								<li>Fontes com espelhos d’água</li>
								<li>Hall decorado</li>
								<li>Doca para carga e descarga</li>
								<li>Circuito fechado de TV</li>
								<li>Serviços: vestiários, copa e cozinha para funcionários e diaristas</li>
								<li>Estacionamento com entrada independente</li>
							</ul>
						</div> <!-- .EstruturaLazer -->
					</div> <!-- .DefaultBox -->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground" class="SecondItem pngfix">Foto 2</a></li>
							<li><span class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</span></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>