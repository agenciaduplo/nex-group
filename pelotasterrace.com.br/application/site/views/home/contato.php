<?=$this->load->view('includes/header');?>

		<div class="Content pngfix">
			<div class="MainContent pngfix">
				<div class="Container">
					<h3 class="SectionTitle pngfix">Contato</h3>
					<ul class="ContactOptions pngfix">
						<!--li class="PlantaoVendas pngfix">
							<p>Visite plantão de vendas e apartamento decorado:</p>
							<p>Rua Gonçalves Chaves, esquina com Rua Rafael Pinto Bandeira</p>
						</li-->
						<li class="Endereco pngfix">
							<p>Rua Rafael Pinto Bandeira, esquina com Av. Juscelino K. de Oliveira, 3161 - Pelotas - RS <a target="_blank" href="http://maps.google.com/maps?q=-31.755257,-52.331219&sll=-31.753797,-52.33331&sspn=0.007298,0.016512&num=1&vpsrc=0&z=17" class="pngfix">Veja a localização no Google Maps</a></p>
						</li>
						<li class="Telefone Last pngfix">53 3028.4545</li>
					</ul> <!-- .ContactOptions -->
					<form id="FormContato" class="Form pngfix" method="post" action="">
						<ul class="First">
							<li>
								<label for="NomeContato">nome</label>
								<input type="text" id="NomeContato" class="CampoPadrao" name="nomeContato" />
							</li>
							<li>
								<label for="EmailContato">e-mail</label>
								<input type="text" id="EmailContato" class="CampoPadrao" name="emailContato" />
							</li>
							<li>
								<label for="Endereco">endereço</label>
								<input type="text" id="Endereco" class="CampoPadrao" name="endereco" />
							</li>
							<li>
								<label for="Uf" class="LabelUf">UF</label>
								<select id="Uf" class="" name="uf">
									<option value="AC">Acre</option>
								    <option value="AL">Alagoas</option>
								    <option value="AM">Amazonas</option>
								    <option value="AP">Amapá</option>
								    <option value="BA">Bahia</option>
								    <option value="CE">Ceará</option>
								    <option value="DF">Distrito Federal</option>
								    <option value="ES">Espirito Santo</option>
								    <option value="GO">Goiás</option>
								    <option value="MA">Maranhão</option>
								    <option value="MG">Minas Gerais</option>
								    <option value="MS">Mato Grosso do Sul</option>
								    <option value="MT">Mato Grosso</option>
								    <option value="PA">Pará</option>
								    <option value="PB">Paraíba</option>
								    <option value="PE">Pernambuco</option>
								    <option value="PI">Piauí</option>
								    <option value="PR">Paraná</option>
								    <option value="RJ">Rio de Janeiro</option>
								    <option value="RN">Rio Grande do Norte</option>
								    <option value="RO">Rondônia</option>
								    <option value="RR">Roraima</option>
								    <option selected="selected" value="RS">Rio Grande do Sul</option>
								    <option value="SC">Santa Catarina</option>
								    <option value="SE">Sergipe</option>
								    <option value="SP">São Paulo</option>
								    <option value="TO">Tocantins</option>
								</select>
							</li>
							<li>
								<label for="Cidade" class="LabelCidade">cidade</label>
								<input type="text" id="Cidade" class="CampoPadrao" name="cidade" />
							</li>
							<li>
								<label for="Cep">CEP</label>
								<input type="text" id="Cep" class="CampoPadrao" name="cep" />
							</li>
							<li>
								<label for="TelefoneContato">telefone</label>
								<input type="text" id="TelefoneContato" class="CampoPadrao" name="telefoneContato" />
							</li>
						</ul>
						<div class="CommentCol">
							<label for="Comentario3">comentário</label>
							<textarea id="Comentario3" class="ComentarioContato pngfix" name="comentario" cols="40" rows="8"></textarea>
							<input type="submit" id="Enviar" class="BotaoEnviar pngfix" name="Enviar" value="Enviar" title="Enviar" />
						</div> <!-- .CommentCol -->
					</form> <!-- #FormContato -->
					<h4 class="ImagesTitle pngfix">Veja algumas imagens do Pelotas Terrace</h4>
					<ul class="jcarousel-skin">
						<li><a href="<?=base_url()?>assets/uploads/imagens/Fachada.JPG" title="Fachada"><img src="<?=base_url()?>assets/img/site/thumb-fachada.jpg" alt="Fachada" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Portaria.jpg" title="Portaria"><img src="<?=base_url()?>assets/img/site/thumb-portaria.jpg" alt="Portaria" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_adulto.jpg" title="Salão de festas adulto"><img src="<?=base_url()?>assets/img/site/thumb-salao-festas-adulto.jpg" alt="Salão de festas adulto" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil"><img src="<?=base_url()?>assets/img/site/thumb-salao-festas-infantil.jpg" alt="Salão de festas infantil" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Piscina.jpg" title="Piscina" ><img  src="<?=base_url()?>assets/img/site/thumb-piscina.jpg" alt="Piscina" width="225" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Fitness.jpg" title="Fitness"><img src="<?=base_url()?>assets/img/site/thumb-fitness.jpg" alt="Fitness" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground"><img src="<?=base_url()?>assets/img/site/thumb-playground.jpg" alt="Playground" width="226" height="226" /></a></li>				
						<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva"><img src="<?=base_url()?>assets/img/site/thumb-quadra-gazebo.jpg" alt="Gazebo e Quadra Poliesportiva" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras"><img src="<?=base_url()?>assets/img/site/thumb-churrasqueira.jpg" alt="Parrilleras e Churrasqueiras" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/implantacao.jpg" title="Implantação Geral"><img src="<?=base_url()?>assets/img/site/thumb-implantacao.jpg" alt="Implantação Geral" width="226" height="226" /></a></li>
						<li><a href="<?=base_url()?>assets/uploads/imagens/Terraco.jpg"  title="Implantação Terraço"><img src="<?=base_url()?>assets/img/site/thumb-terraco.jpg" alt="Implantação Terraço" width="226" height="226" /></a></li>
					</ul> <!-- .jcarousel-skin -->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground" class="SecondItem pngfix">Foto 2</a></li>
							<li><span class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</span></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>