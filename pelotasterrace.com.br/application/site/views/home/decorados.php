<?=$this->load->view('includes/header');?>

		<div class="Content pngfix">
			<div class="MainContent pngfix">
				<div class="Container">
					<h3 class="SectionTitle pngfix">Decorado</h3>
					<ul class="Gallery">
						<li><a href="<?=base_url()?>assets/img/site/decorados/foto_1.jpg" title=""><img src="<?=base_url()?>assets/img/site/decorados/foto_1.png" alt="Foto" width="419" height="250" /></a></li>
						<li class="Right"><a href="<?=base_url()?>assets/img/site/decorados/foto_2.jpg" title=""><img src="<?=base_url()?>assets/img/site/decorados/foto_2.png" alt="Foto" width="419" height="250" /></a></li>
						<li><a href="<?=base_url()?>assets/img/site/decorados/foto_3.jpg" title=""><img src="<?=base_url()?>assets/img/site/decorados/foto_3.png" alt="Foto" width="419" height="250" /></a></li>
						<li class="Right"><a href="<?=base_url()?>assets/img/site/decorados/foto_4.jpg" title=""><img src="<?=base_url()?>assets/img/site/decorados/foto_4.png" alt="Foto" width="419" height="250" /></a></li>
						<li class="Last"><a href="<?=base_url()?>assets/img/site/decorados/foto_5.jpg" title=""><img src="<?=base_url()?>assets/img/site/decorados/foto_5.png" alt="Foto" width="419" height="250" /></a></li>
					</ul> <!-- .Gallery -->
					<div class="FooterNav pngfix">
						<ul class="AuxNavigation">
							<li><a href="<?=base_url()?>assets/uploads/imagens/churrasqueira.jpg" title="Parrilleras e Churrasqueiras" class="FirstItem pngfix">Foto 1</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Playgroud.jpg" title="Playground" class="SecondItem pngfix">Foto 2</a></li>
							<li><span class="LogoFooter pngfix">Pelotas Terrace - O ponto mais alto da sua vida</span></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Salao_infantil.jpg" title="Salão de festas infantil" class="ThirdItem pngfix">Foto 3</a></li>
							<li><a href="<?=base_url()?>assets/uploads/imagens/Quadra_e_Gazebo.jpg" title="Gazebo e Quadra Poliesportiva" class="FourthItem pngfix">Foto 4</a></li>
						</ul> <!-- .AuxNavigation -->
						<ul class="Features">
							<li><span class="Tel pngfix">(53) 3028.4545</span></li>
							<li><a href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/index.php?id_produto=5','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);" class="BotaoCorretor pngfix" title="Corretor online">Corretor online</a></li>
							<li><a href="#Panels" class="BotaoInteresse pngfix" title="Tenho interesse">Tenho interesse</a></li>
							<li><a href="#Panels" class="BotaoIndique pngfix" title="Indique para um amigo">Indique para um amigo</a></li>
						</ul> <!-- .Features -->
					</div> <!-- .FooterNav -->
				</div> <!-- .Container -->
			</div> <!-- .MainContent -->
		</div> <!-- .Content -->
	</div> <!-- .Main -->
	
<?=$this->load->view('includes/footer');?>