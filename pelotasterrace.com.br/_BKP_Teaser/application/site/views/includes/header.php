<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
    
    <title>Pelotas Terrace - Apartamentos 3 dormitórios (1 suíte)</title>
    
    <meta name="description" content="Pré-lançamento Terrace Pelotas - Apartamentos com 3 dormitórios (1 suíte) no prédio mais alto de Pelotas. Área de lazer com vista de 360º na cobertura." />
    <meta name="keywords" content="apartamento 3 dormitórios, apartamento 3 quatros, apartamento com suíte, apto 3 dorms, apto 3 quartos, apto com suíte, pelotas, pre-lançamento" />
    <meta name="author" content="Divex Imobi - www.imobi.divex.com.br" />
    <meta name="robots" content="index, follow" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="1 days" />
    <meta name="mssmarttagspreventparsing" content="true" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>assets/css/site/estilo.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/site/validation/validationEngine.jquery.css" type="text/css" media="screen" />

	<script type="text/javascript" src="<?=base_url()?>assets/js/site/jquery-1.5.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine-pt.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/validation/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/site/functions.js"></script>

	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1622695-47']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
</head>

<body>
	<div class="Full">

        <div class="Wrapper">
        	<div class="Container">