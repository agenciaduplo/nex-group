

	// Aqui estão todas as chamadas para comportamentos da seção "Plantas baixas"

	$(function() {
		// Lightbox para exibição das fotos da seção
		$("ul.ListaPlantas a").attr('rel', 'galeria-plantas').fancybox({
			'titleShow'			: true,
			'titlePosition'	: 'inside',
			'overlayColor'	: '#000',
			'overlayOpacity': 0.8
		});
	});
