// Aqui estão todas as chamadas para comportamentos que são utilizados em todo site
	$(function() {

		$('a#popupcorretor').attr('rel', 'popupCorretor').fancybox();

		// $('#fancybox-outer').css({ 'background':'none' });
		// $('#fancybox-content').css({ 'border':'none' });
		// $('.fancybox-bg').hide();

		// setTimeout(function(){ $('a#popupcorretor').trigger('click') },1000);

		$('#popupCorretorOverlay').hide();
		$('#popupCorretor').hide();

		setTimeout(function(){ $('#popupCorretorOverlay').fadeIn(); $('#popupCorretor').append('<a class="close" href="javascript:closePopUp();void(0);">Fechar</a>').fadeIn(); },1000);

		// Setar o primeiro campo de formulário com o foco

		//$("input[type=text]:first").focus();

		// Uma div vazia deve ser criada depois do .Logo para evitar que o mesmo desapareça no IE6

		$(".Logo").after("<div></div>");
		
		$("ul.AuxNavigation a").attr('rel', 'footer').fancybox({

			'titleShow'			: true,

			'titlePosition'	: 'inside',

			'overlayColor'	: '#000',

			'overlayOpacity': 0.8

		});
		// Exibe e esconde os painéis dos formulários suspensos acionados pelos botões do topo

		$(".Topbar .BotaoInteresse").click(function() {

			if ($("#PainelIndique").is(":visible")) {

				$("#PainelIndique").slideToggle("slow");

				$(".Topbar .BotaoIndique").toggleClass("BotaoIndiqueAtivo");

			}

			$("#PainelTenhoInteresse").slideToggle("slow");

			$(this).toggleClass("BotaoInteresseAtivo");

			return false;

		});

		$(".Topbar .BotaoIndique").click(function() {

			if ($("#PainelTenhoInteresse").is(":visible")) {

				$("#PainelTenhoInteresse").slideToggle("slow");

				$(".Topbar .BotaoInteresse").toggleClass("BotaoInteresseAtivo");

			}

			$("#PainelIndique").slideToggle("slow");

			$(this).toggleClass("BotaoIndiqueAtivo");

			return false;

		});

		// Exibe e esconde os painéis dos formulários suspensos acionados pelos botões do rodapé

		$(".Content .BotaoInteresse").click(function(e) {

			e.preventDefault();

			if ($("#PainelIndique").is(":visible")) {

				$("#PainelIndique").slideToggle("slow");

				$(".Topbar .BotaoIndique").toggleClass("BotaoIndiqueAtivo");

			}

			$("#PainelTenhoInteresse").slideToggle("slow");

			$(".Topbar .BotaoInteresse").toggleClass("BotaoInteresseAtivo");

			var elementClicked = $(this).attr("href");

			var destination = $(elementClicked).offset().top;

			$("html:not(:animated), body:not(:animated)").animate({ scrollTop: destination - 20}, 500);

			return false;

		});

		$(".Content .BotaoIndique").click(function(e) {

			e.preventDefault();

			if ($("#PainelTenhoInteresse").is(":visible")) {

				$("#PainelTenhoInteresse").slideToggle("slow");

				$(".Topbar .BotaoInteresse").toggleClass("BotaoInteresseAtivo");

			}

			$("#PainelIndique").slideToggle("slow");

			$(".Topbar .BotaoIndique").toggleClass("BotaoIndiqueAtivo");

			var elementClicked = $(this).attr("href");

			var destination = $(elementClicked).offset().top;

			$("html:not(:animated), body:not(:animated)").animate({ scrollTop: destination - 20}, 500);

			return false;

		});

	});

	$(window).load(function() {

		// Força os links com rel="external" a abrirem em uma nova janela/aba

		$("a[rel*=external]").attr('target', '_blank');

		// Evita que o frame criado pelo widget do AddThis crie uma rolagem horizontal

		$("iframe[name*=twttrHubFrame]").css('left', '0');

	});
	
	$(document).ready(function() {
		$("input[name='telefone']").mask("(99) 9999-9999");
		$("input[name='telefoneContato']").mask("(99) 9999-9999");
		
		$.validator.setDefaults({
			submitHandler: function() { enviaInteresse(); }
		}); 
		$("#FormInteresse").validate({
			errorElement: "strong",
			rules:{
				nome:{
					required: true
				},
				email:{
					required: true, email:true			
				},
				telefone:{
					required: true
				}
			},
			messages:{
				nome:{
					required: "*"
				},
				email:{
					required: "*",
					email: "*"
				},
				telefone:{
					required: "*"
				}
			}
		});
		
		$.validator.setDefaults({
			submitHandler: function() { enviaIndique(); }
		}); 
		$("#FormIndique").validate({
			errorElement: "strong",
			rules:{
				nomeRemetente:{
					required: true
				},
				emailRemetente:{
					required: true, 
					email:true			
				},
				nomeAmigo:{
					required: true
				},
				emailAmigo:{
					required: true, 
					email:true			
				}
			},
			messages:{
				nomeRemetente:{
				required: "*"
			},
			emailRemetente:{
				required: "*", 
				email:"*"			
			},
			nomeAmigo:{
				required: "*"
			},
			emailAmigo:{
				required: "*", 
				email:"*"			
			}
			}
		});
		$.validator.setDefaults({
			submitHandler: function() { enviaContato(); }
		}); 
		$("#FormContato").validate({
			errorElement: "strong",
			rules:{
				nomeContato:{
					required: true
				},
				emailContato:{
					required: true, email:true			
				},
				telefoneContato:{
					required: true
				}
			},
			messages:{
				nomeContato:{
					required: "*"
				},
				emailContato:{
					required: "*",
					email: "*"
				},
				telefoneContato:{
					required: "*"
				}
			}
		});
	});
	function enviaInteresse(){
			
		$("#FormInteresse #Enviar").attr("disabled","disabled");
		
		var nome			= $("#Nome").val();
		var faixa_etaria	= $("#FaixaEtaria").val();
		var estado_civil	= $("#EstadoCivil").val();
		var bairro_cidade	= $("#Cidade").val();
		var profissao		= $("#Profissao").val();
		var telefone		= $("#Telefone").val();
		var email			= $("#Email").val();
		var comentarios		= $("#Comentario").val();
		
		if($('#ChkEmail').is(':checked') && $('#ChkTelefone').is(':checked'))
		{
			var contato = "E-mail e Telefone";
		}
		else if ($('#ChkEmail').is(':checked'))
		{
			var contato = "E-mail";
		}
		else if ($('#ChkTelefone').is(':checked'))
		{
			var contato = "Telefone";
		}
			
		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&faixa_etaria='+ faixa_etaria
					  +'&estado_civil='+ estado_civil
					  +'&bairro_cidade='+ bairro_cidade
					  +'&profissao='+ profissao
					  +'&telefone='+ telefone
					  +'&email='+ email
					  +'&contato='+ contato
					  +'&comentarios='+ comentarios;
					  
		base_url  	= "http://www.pelotasterrace.com.br/home/enviarInteresse";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					limpaCampos('#FormInteresse');
					$("#FormInteresse #Enviar").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);
					
					/* <!-- Google Code for Contato Conversion Page --> */
					window.google_conversion_id = 994871530;
					window.google_conversion_language = "pt"
					window.google_conversion_format = "2"
					window.google_conversion_color = "ffffff"
					window.google_conversion_label = "-JUCCJ7r0wYQ6pGy2gM";
					window.google_conversion_value = 0;

					document.write = function(node){ $("body").append(node); }
					$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });
					
					
					alert("Interesse enviado com sucesso!\nEm breve entraremos em contato.");
					}
		});
		return false;
	}

	function enviaIndique(){
		
		$("#FormIndique #Enviar").attr("disabled","disabled");
		
		var nome_remetente		= $("#NomeRemetente").val();
		var email_remetente		= $("#EmailRemetente").val();
		var nome_amigo			= $("#NomeAmigo").val();
		var email_amigo			= $("#EmailAmigo").val();
		var comentarios			= $("#Mensagem").val();
		
		
		var msg 	= '';
		vet_dados 	= 'nome_remetente='+ nome_remetente
					  +'&email_remetente='+ email_remetente
					  +'&nome_amigo='+ nome_amigo
					  +'&email_amigo='+ email_amigo
					  +'&comentarios='+ comentarios;
					  
		base_url  	= "http://www.pelotasterrace.com.br/home/enviarIndique";
																  
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					limpaCampos('#FormIndique');
					$("#FormIndique #Enviar").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);             
					
					/* <!-- Google Code for SITE Entre em Contato Agora Conversion Page --> */
        	window.google_conversion_id = 994871530;
         	window.google_conversion_language = "pt"
         	window.google_conversion_format = "2"
         	window.google_conversion_color = "ffffff"
         	window.google_conversion_label = "-JUCCJ7r0wYQ6pGy2gM";
         	window.google_conversion_value = 0;

         	document.write = function(node){ $("body").append(node); }

         	$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });
					alert("Indicação enviada com sucesso!");
			}
		});
		return false;
	}

	function enviaContato(){
		
		$("#FormContato #Enviar").attr("disabled","disabled");
		
		var nome			= $("#NomeContato").val();
		var email			= $("#EmailContato").val();
		var endereco		= $("#Endereco").val();
		var uf				= $("#Uf").val();
		var cidade			= $("#FormContato #Cidade").val();
		var cep				= $("#Cep").val();
		var telefone		= $("#TelefoneContato").val();
		var comentarios		= $("#Comentario3").val();
		
		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email
					  +'&endereco='+ endereco
					  +'&uf='+ uf
					  +'&cidade='+ cidade
					  +'&cep='+ cep
					  +'&telefone='+ telefone
					  +'&comentarios='+ comentarios;
					  
		base_url  	= "http://www.pelotasterrace.com.br/home/enviarInteresse";
		
		$.ajax({
			type: "POST",
			url: base_url,
			data: vet_dados,
			success: function(msg) {
					limpaCampos('#FormContato');
					$("#FormContato #Enviar").removeAttr("disabled");
					//$("#ConfirmaForm").html(msg);
					
					/* <!-- Google Code for SITE Entre em Contato Agora Conversion Page --> */
                  	window.google_conversion_id = 994871530;
                   	window.google_conversion_language = "pt"
                   	window.google_conversion_format = "2"
                   	window.google_conversion_color = "ffffff"
                   	window.google_conversion_label = "-JUCCJ7r0wYQ6pGy2gM";
                   	window.google_conversion_value = 0;
 
                   	document.write = function(node){ $("body").append(node); }

                   	$.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });

					alert("Mensagem enviada com sucesso!\nEm breve entraremos em contato.");
			}
		});
		return false;
	}
	function limpaCampos (form)
	{
	    $(form).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'select':
	            case 'text':
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}
	function closePopUp() {
		$('#popupCorretorOverlay').hide();
		$('#popupCorretor').hide();
	}