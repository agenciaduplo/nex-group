

	// Aqui estão todas as chamadas para comportamentos da seção "Contato"

	$(function() {
		// Lightbox para exibição das fotos da seção
		$("ul.jcarousel-skin a").attr('rel', 'galeria-fotos').fancybox({
			'titleShow'			: true,
			'titlePosition'	: 'inside',
			'overlayColor'	: '#000',
			'overlayOpacity': 0.8
		});

		// Inicializa o carrossel
		$(".jcarousel-skin").jcarousel();
	});
