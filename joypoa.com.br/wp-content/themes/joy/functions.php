<?php

/***********************
* Boot
***********************/
require_once TEMPLATEPATH . '/includes/init.php';


/***********************
 * Configurações
 ***********************/
define( "IMGPATH", get_bloginfo('template_url') . "/images/" );
add_theme_support( "post-thumbnails" );
wp_enqueue_script( "jquery" );
zt_add_support( "carrossel" );
zt_add_support( "fancybox" );
zt_add_support( "email" );
zt_add_support( "seo" );
zt_add_support( "open_graph" );
if (!is_admin()) {
	wp_enqueue_script( "zt", get_bloginfo('template_url') . "/js/jquery.zt.js", array('jquery'), '1.0', TRUE );
	wp_enqueue_script( "site", get_bloginfo('template_url') . "/js/site.js", array('jquery', 'zt'), '1.0', TRUE );
	wp_enqueue_script( "jquery.scrollTo-min", get_bloginfo('template_url') . "/js/jquery.scrollTo-min.js", array('jquery', 'site'), '1.0', TRUE );
}
add_image_size('apartamentos', 503, 325, TRUE);
add_image_size('thumb-1', 203, 132, TRUE);

/***********************
 * Menus
 ***********************/
register_nav_menus( array(
	'navegacao' => 'Menu de Navegação',
) );
register_nav_menus( array(
	'empreendimento' => 'Menu do Empreendimento',
) );