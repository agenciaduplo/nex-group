
// Página de contato
function contato($) {
	var form = $('#contato-form');
	form.submit(function(e){
		e.preventDefault();
		jQuery('#contato-form .msg-enviada').hide();
		jQuery('#contato-form .msg-erro').hide();
		jQuery('#contato-form .irequerido').removeClass('irequerido');
		if ( !contato_validar() ) return;
		jQuery('#contato-form .msg-enviando').show();
		jQuery.post(form.attr('action'), form.serialize()).done(function(data)
		{
			$('#contato-form .msg-enviando').hide();
			$('#contato-form .msg-enviada').show();
			limpar_form( '#contato-form' );
		}).fail(function(data)
		{
			$('#contato-form .msg-enviando').hide();
			$('#contato-form .msg-enviada').show();
			limpar_form( '#contato-form' );
		});
		
		/* <!-- Google Code for Contato Conversion Page --> */
		window.google_conversion_id = 978900397;
		window.google_conversion_language = "pt"
		window.google_conversion_format = "2"
		window.google_conversion_color = "ffffff"
		window.google_conversion_label = "5rocCOzXmAYQjLiK1QM";
		window.google_conversion_value = 0;

		document.write = function(node){ jQuery("body").append(node); }
		jQuery.getScript("http://www.googleadservices.com/pagead/conversion.js").done(function() {  });

		console.log('teste');
		
	});
	
}


function contato_validar() {
	campos  = "";
	campos += validar( 'nome', 'nome completo', 5 );
	campos += validar( 'email', 'e-mail', 5, 'email' );
	campos += validar( 'mensagem', 'mensagem', 7, null );
	//campos += validar( 'spambot', 'verificador de spammer', 0, 'checkbox' );
	msg = campos.substr( 0, campos.length-2 ).replace( /,([a-zA-Z ]+)$/, ' e $1');
	jQuery('#contato-form div.msg-erro').find( 'span.campos' ).html(msg);
	if (campos.length == 0)
		return true;
	else
		jQuery('#contato-form  div.msg-erro').show();
}


function validar(id, label, length, tipo, padrao) {
	campo = jQuery('#'+id);
	valor = campo.val();
	campo.val(jQuery.trim(valor));
	ret = "";
	
	if ( valor.length < length || valor==padrao ) {
		campo.addClass('irequerido');
		ret = "" + label + ', ';
	}
	else if ( tipo=='nome' ) {
		// Se não tiver espaço
		if (valor.search(' ') == -1) {
			campo.addClass('irequerido');
			ret = "" + label + ', ';
		} else {
			sobrenome = valor.substr(jQuery.trim(valor.search(' ')));
			if ( sobrenome.length < 2 ) {
				campo.addClass('irequerido');
				ret = "" + label + ', ';
			}
		}
	} else if ( tipo == 'email' ) {
		if ( !valida_email( campo.val() )) {
			campo.addClass('irequerido');
			ret = "" + label + ', ';
		}
	} else if ( tipo == 'checkbox' ) {
		if ( jQuery( '#'+id+':checked' ).length == 0 ) {
			campo.addClass('irequerido');
			ret = "" + label + ', ';
		}
	} 
	return ret;
}


function valida_email(email) {
	er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
	if(er.exec(email))
		return true;
	else
		return false;
}


function limpar_form(context) {
	jQuery( ':input', context )
	 .not(':button, :submit, :reset, :hidden')
	 .val('')
	 .removeAttr('checked')
	 .removeAttr('selected')
}


function galerias( ) {
	jQuery('.gallery').each(function(i,e){
		gal = jQuery(e);
		jQuery('a', gal).each(function(i2,e){
			jQuery(e).attr('rel', 'gal[num'+i+']');
		});
		jQuery('[rel^="gal[num'+i+']"]').prettyPhoto({show_title:false});
	});
}

function fancybox_galerias( ) {
	jQuery('.gallery').each(function(i,e){
		gal = jQuery(e);
		jQuery('a', gal).each(function(i2,e){
			jQuery(e).attr('rel', 'gal-'+i+'');
		});
	});
	jQuery('div.gallery a').fancybox({titlePosition:'over', overlayColor: '#000'});
	
	jQuery('a.gal').fancybox({titlePosition:'over', overlayColor: '#000'});
	
	jQuery('#carrossel-apartamentos a').fancybox({titlePosition:'inside', overlayColor: '#000', titleFormat: function() {
		 return '<b>'+this.title+'</b>';
	 } });
	
	jQuery('a.video').click(function(e) {
		e.preventDefault();
		jQuery.fancybox({
            'titleShow'     : false,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic',
            'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'          : 'swf',
            'swf'           : {'wmode':'transparent','allowfullscreen':'true'},
            'width'         : 680,
            'height'        : 495,
            'overlayColor'  : '#000'
		});
	});
	
	
}

function menu_sup() {
	
	var menu   = jQuery('#menu').after();
	var top    = menu.offset().top + menu.height() - 110;
	var menu2  = jQuery('#barra-fixa');
	emp = jQuery('#empreendimento');
	empreendimento_top = emp.offset().top
	aps         = jQuery('#apartamentos');
	aps_top     = aps.offset().top
	infra       = jQuery('#infraestrutura');
	infra_top   = infra.offset().top
	local       = jQuery('#localizacao');
	local_top   = local.offset().top
	contato     = jQuery('#contato');
	contato_top = contato.offset().top;
	
	jQuery(window).scroll(function (event) {
		var y = jQuery(this).scrollTop();
		if ( y > top ) {
			menu2.fadeIn();
		} else {
			menu2.fadeOut();
		}
		
		if ( y >= (contato_top - 50) ) {
			if ( ! contato.hasClass('bloco-ativo') )
				contato.animate({opacity:'1'}, 300).addClass('bloco-ativo');
		}
		else {
			if (contato.hasClass('bloco-ativo')) contato.css('opacity', '0.3').removeClass('bloco-ativo');
		
			if ( y >= (local_top - 50) ) {
				if ( ! local.hasClass('bloco-ativo') )
					local.animate({opacity:'1'}, 300).addClass('bloco-ativo');
			}
			else {
				if (local.hasClass('bloco-ativo')) local.css('opacity', '0.3').removeClass('bloco-ativo');
			
				if ( y >= (infra_top - 50) ) {
					if ( ! infra.hasClass('bloco-ativo') )
						infra.animate({opacity:'1'}, 300).addClass('bloco-ativo');
				}
				else {
					if (infra.hasClass('bloco-ativo')) infra.css('opacity', '0.3').removeClass('bloco-ativo');
					
					if ( y >= (aps_top - 50) ) {
						if ( ! aps.hasClass('bloco-ativo') )
							aps.animate({opacity:'1'}, 300).addClass('bloco-ativo');
					}
					else {
						if (aps.hasClass('bloco-ativo')) aps.css('opacity', '0.3').removeClass('bloco-ativo');
					
						if ( y >= empreendimento_top + 50 && ! emp.hasClass('bloco-ativo') )
							emp.addClass('bloco-ativo');
					}// Fim aps
				}// Fim ingra
			}// Fim local
		}// fim contato
		
		link = jQuery('.bloco-ativo:last').attr('id');
		jQuery('.menu-ativo').removeClass('menu-ativo');
		jQuery('#menuf-'+link).addClass('menu-ativo');
	});
	
	jQuery('a').not('#dk_container_select a').click(function(e){
		var elemento = jQuery(this).attr('href');
		if ( elemento.substr(0, 1) == '#' ) {
			jQuery('.menu-ativo').removeClass('menu-ativo');
			e.preventDefault();
			if ( elemento == '#empreendimento' )
				elemento = jQuery( elemento ).offset().top - 90;
			jQuery.scrollTo( elemento, 800);
		}
	});
	
}


function menu_sup_old() {
	var menu   = jQuery('#menu').after();
	var top    = menu.offset().top + menu.height() - 110;
	var menu2  = jQuery('#barra-fixa');
	jQuery(window).scroll(function (event) {
		var y = jQuery(this).scrollTop();
		if ( y > top ) {
			menu2.fadeIn();
		} else {
			menu2.fadeOut();
		}
	});
	
	jQuery('a').click(function(e){
		var elemento = jQuery(this).attr('href');
		if ( elemento.substr(0, 1) == '#' ) {
			e.preventDefault();
			if ( elemento == '#empreendimento' )
				elemento = jQuery( elemento ).offset().top - 90;
			jQuery.scrollTo( elemento, 800);
		}
	});
	
}

var map;

function mapa() {
	var latlng = new google.maps.LatLng(-30.01650, -51.16951);
	var myOptions = {
			zoom: 16,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}


// Add markers to the map
function setMarkers(map, locations) {
	  for (var i = 0; i < locations.length; i++) {
	    var local = locations[i];
	    var myLatLng = new google.maps.LatLng(local[1], local[2]);
	    var image = new google.maps.MarkerImage(local[3],
	  	      // This marker is 20 pixels wide by 32 pixels tall.
	  	      new google.maps.Size(local[4][0], local[4][1]),
	  	      // The origin for this image is 0,0.
	  	      new google.maps.Point(local[5][0],local[5][1]),
	  	      // The anchor for this image is the base of the flagpole at 0,32.
	  	      new google.maps.Point(local[6][0] ,local[6][1]));
	    var marker = new google.maps.Marker({
	        position: myLatLng,
	        map: map,
	        icon: image,
	        title: local[0],
	        zIndex: i
	    });
	  }
}
