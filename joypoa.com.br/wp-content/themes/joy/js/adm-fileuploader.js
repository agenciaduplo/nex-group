jQuery(document).ready(function(){
	var galeria = jQuery('#galeria'); 
	if ((adminpage =='edit-php' || adminpage == 'post-new-php' || adminpage == 'post-php') && jQuery('#file-uploader').length >0 ) {
		var post_id = jQuery('#post_ID').val();
		var uploader = new qq.FileUploader({
		    element: jQuery('#file-uploader')[0],
		    action: ajaxurl,
		    params: {action:'zt_gallery_upload', postid:post_id },
		    onComplete: function(id, fileName, responseJSON){
		    	if (responseJSON.success == true) {
		    		img  = "<li>";
			    		img += "<img src='" + responseJSON.thumbnail.url + "' />";
			    		img += "<input type='hidden' name='zt_galeria_images[]' value='" +  JSON.stringify(responseJSON) + "' />";
			    		img += "<a href='#' class='remove-image'><img src='" + theme_image_url + "/delete16.gif' alt='' /></a>";
		    		img += "</li>";
		    		galeria.append(img);
		    	}
		    },
		    degub: true
		});
		
		jQuery(document.body).delegate( 'a.remove-image', 'click', function(e){
			e.preventDefault();
			li = jQuery(this).parent();
			li.fadeOut(function(){li.remove()});
		} );
		
		jQuery(document.body).delegate( 'a.edit-image', 'click', function(e){
			//e.preventDefault();
		} );
	}
	
	galeria.sortable({});
});