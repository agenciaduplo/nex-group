jQuery(document).ready(function() {
	
	var uploadID = "";
	
	jQuery(".fileupload").live('click', function(event){
		uploadID = jQuery(this).prev('input');
		tb_show("Selecione uma imagem", jQuery(this).attr('rel'));
		return false;
	});

	window.send_to_editor = function (b) {
	    if ( uploadID ) {
	    	imgurl = jQuery("img",b).attr("src");
			uploadID.val(imgurl);
			parent = uploadID.parent();
			parent.find('.image-preview').html('<img src="'+imgurl+'" class="zt-widget-img" style="margin:5px 0;  max-width:225px; max-height:250px" />');
			parent.find('.fileupload').hide();
			parent.find('.remove_image').show();
			uploadID = "";
	    } else {
			var a;
		    if (typeof tinyMCE != "undefined" && (a = tinyMCE.activeEditor) && !a.isHidden()) {
		        if (tinymce.isIE && a.windowManager.insertimagebookmark) {
		            a.selection.moveToBookmark(a.windowManager.insertimagebookmark)
		        }
		        if (b.indexOf("[caption") === 0) {
		            if (a.plugins.wpeditimage) {
		                b = a.plugins.wpeditimage._do_shcode(b)
		            }
		        } else {
		            if (b.indexOf("[gallery") === 0) {
		                if (a.plugins.wpgallery) {
		                    b = a.plugins.wpgallery._do_gallery(b)
		                }
		            } else {
		                if (b.indexOf("[embed") === 0) {
		                    if (a.plugins.wordpress) {
		                        b = a.plugins.wordpress._setEmbed(b)
		                    }
		                }
		            }
		        }
		        a.execCommand("mceInsertContent", false, b)
		    } else {
		        if (typeof edInsertContent == "function") {
		            edInsertContent(edCanvas, b)
		        } else {
		            jQuery(edCanvas).val(jQuery(edCanvas).val() + b)
		        }
		    }
	    }
	    tb_remove()
	}
	
	jQuery('.remove_image').click(function(e){
		e.preventDefault();
		rb = jQuery(this);
		rb.hide();
		rbp = rb.parent();
		rbp.find('.image-preview').html('');
		rbp.find('input').val('');
		rbp.find('.fileupload').show().removeClass('hide');
	});
	
});