function banners() {

	opcoesslide.delay = 0; 
	opcoesslide.pager = '#banner-nav';
	opcoesslide.pagerEvent = 'click';
	opcoesslide.pauseOnPagerHover = true;
	opcoesslide.cleartype = true;
	opcoesslide.cleartypeNoBg = true;

	if (opcoesslide.number=='0'){
		opcoesslide.pagerAnchorBuilder = function(idx, slide) {
			return '<li><a href="#" class="png"></a></li>';
		};
	}
	jQuery('#banners-list').cycle(opcoesslide);
	
	if ( opcoesslide.bannerlink === "1" ) {
		jQuery('#banners-list img, #banners-list .banner-label').click(function (){
			 url = jQuery(this).attr('rel');
			 if (url.length>7)
		     	document.location.href = url;
		});
	}
	
}