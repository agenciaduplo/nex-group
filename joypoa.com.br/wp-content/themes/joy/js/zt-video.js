jQuery(function($){
	t = null;
	$('.zt-video').keyup(function(){
		clearTimeout(t);
		campo_video = $(this);
		t = setTimeout("putScreen()", 500);
	});
});

function getScreen( url, size )
{
  if(url === null){ return ""; }

  size = (size === null) ? "big" : size;
  var vid;
  var results;

  results = url.match("[\\?&]v=([^&#]*)");

  vid = ( results === null ) ? url : results[1];

  if(size == "small"){
    return "http://img.youtube.com/vi/"+vid+"/2.jpg";
  }else {
    return "http://img.youtube.com/vi/"+vid+"/0.jpg";
  }
}

function putScreen() {
	url = jQuery.trim(campo_video.val());
	div = campo_video.parent().parent().find('.screen-video');
	if (!url)
		div.html('');
	else {
		img = getScreen(url, "big");
		div.html("<img src='"+img+"' width='210' />");
	}
}