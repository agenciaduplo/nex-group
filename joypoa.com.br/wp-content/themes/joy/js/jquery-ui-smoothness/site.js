
function contato() {
	jQuery('form#contato').submit(function(e){
		e.preventDefault();
		jQuery('#msg-enviada').hide();
		jQuery('#msg-erro').hide();
		jQuery('.irequerido').removeClass('irequerido');
		if (!contato_validar()) return;
		
		jQuery('#msg-enviando').show();
		var form = jQuery(this);
		jQuery.post( form.attr('action'), form.serialize(), function(ret){
			jQuery('#msg-enviando').hide();
			jQuery('#msg-enviada').fadeIn();
		});
		
	});
}

function form_validar() {
	erro = 0;
	campos  = "";
	campos += validar('nome', 'nome', 5);
	campos += validar('empresa', 'empresa', 5);
	jQuery('#msg-erro').find('span.campos').html(campos.substr(0, campos.length-2));
	if (campos.length == 0)
		return true;
	else
		jQuery('#msg-erro').show();
}

function validar(id, label, length ) {
	campo = jQuery('#'+id);
	if (campo.val().length < length) {
		campo.addClass('irequerido');
		return "<i>" + label + '</i>, ';
	} else
		return '';
}