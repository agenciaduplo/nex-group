(function($) {
	
	var methods = {
		lightbox: function(method, o){
			var o = $.extend({
				width: 960,
				conteudo: null, // Elemento que deverá ser exibido
				ajustarConteudo: function(){ alert('zt rules');}
			}, o);
			
			var mask = $('#mask');
			if ( mask.length == 0) { $('body').append("<div id='maks'></div>"); mask = $('#mask'); }
			
			link = $(this);
			link.click(function(e){
				e.preventDefault();
				var conteudo = $(o.conteudo);
				conteudo.width( o.width );
				
				// Adiciona uma máscara para impedir clicks no restante do site
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();
				mask.css({'width':maskWidth,'height':maskHeight}).show();
				
				zt_centralizar( conteudo );
				
				o.ajustarConteudo( link, conteudo );
				
				conteudo.fadeIn(500)
			});
		},
		spambot: function(){
			var form = $(this);
			var bt = form.find(':submit');
			bt.before( "<div class='spambot'><input type='checkbox' value='1' name='spambot' id='spambot' /><label for='spambot'>Eu não sou um robô spammer</label></div>" );
		}
	}
	
	// Centraliza  um bloco na tela
	function zt_centralizar( conteudo ) {
		var winH = $(window).height();
		var winW = $(window).width();
		var scrool_top = $(window).scrollTop();
		conteudo.css('top', ( winH / 2 - conteudo.height() / 2) + scrool_top ).css('left', winW / 2 - conteudo.width() / 2);
	}
	
	// Botão de fechar do lightbox
	$('.window .close').click(function (e) {
		e.preventDefault();
		$('#mask, .window').hide();
	});
	
	// Centraliza o lightbox quando a janela é redimensionada
	$(window).resize(function() {
		zt_centralizar( $('.window') );
	});
	
	$.fn.zt = function( method ) {
		if ( methods[method] ) {
		    return methods[method].apply( this, arguments );
		}
		else {
			$.error( 'Método ' +  method + ' não existe em jQuery.zt' );
	    }
	}
	
})(jQuery);