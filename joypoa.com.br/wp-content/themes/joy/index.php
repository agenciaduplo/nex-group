<?php
// Site sem o www

wp_enqueue_script( "jquery.dropkick", get_bloginfo('template_url') . "/js/jquery.dropkick-1.0.0.js", array('jquery'), '1.0.0', TRUE );
wp_enqueue_style( "jquery.dropkick", get_bloginfo('template_url') . "/css/dropkick.css" );

add_action('zt_footer_js', 'add_page_js');
function add_page_js() { echo " $('#select').dropkick(); contato($); "; }

get_header(); ?>

<div class="centralizador">	
	<div id="conteudo" class="conteudo-central">
	
	<!-- O EMPREENDIMENTO -->
		<?php 
			$id = zt_page::get_ID_by_slug('o-empreendimento');
			$page = get_page($id);
		?>
	<div id="empreendimento" class='bloco-conteudo' >
		<div id="empreendimento-esquerda">
			<div id="img-empreendimento">
				<img src="<?php echo IMGPATH ?>realize-seu-sonho.png" alt="Realize seu sonho de morar bem." />
			</div>
			<div id="video-imagem" >
			
				<div class="relativo">
					<a href="http://www.youtube.com/watch?v=ird-sSOuWS8&feature=youtu.be" class='video'>
						<img src="<?php echo IMGPATH ?>video1.png" alt="Tour Virtual" />
						<img class="img-mascara" src="<?php echo IMGPATH ?>video1-hover.png" alt="Tour Virtual" />
						<img class="sombra" src="<?php echo IMGPATH ?>sombra3.png" alt="sombra" />
					</a>
					<p class="legenda">Tour Virtual</p>
				</div>
				
				<div class="com-margem relativo">
					<a href="<?php echo IMGPATH ?>empreendimento-img1.jpg" class='gal' rel='empreendimento' title="Fachada" rel="gal-empreendimento" >
						<img src="<?php echo IMGPATH ?>img1-normal.png" alt="Fachada" />
						<img class="img-mascara" src="<?php echo IMGPATH ?>img1-hover.png" alt="Fachada" />
						<img class="sombra" src="<?php echo IMGPATH ?>sombra3.png" alt="sombra" />
					</a>
					<p class="legenda">Fachada</p>
				</div>
				
				<div class="relativo">
					<a href="<?php echo IMGPATH ?>empreendimento-img2.jpg" class='gal' rel='empreendimento' title="Área de lazer na cobertura" rel="gal-empreendimento">
						<img src="<?php echo IMGPATH ?>img2-normal.png" alt="Área de lazer" />
						<img class="img-mascara" src="<?php echo IMGPATH ?>img2-hover.png" alt="Área de lazer" />
						<img class="sombra" src="<?php echo IMGPATH ?>sombra3.png" alt="sombra" />
					</a>
					<p class="legenda">Área de lazer na cobertura</p>
				</div>
				
			</div>
		</div>
		
		<div id="empreendimento-direita">
			<img src="<?php echo IMGPATH ?>separador.png" alt="separador" />
			<?php echo apply_filters('the_content',$page->post_content) ?>
			<div id="menu-empreendimento">
				<?php /*wp_nav_menu(array('theme_location'=>'empreendimento'))*/ ?>
				<ul>
					<li class="menu-item-first"><a href="#apartamentos" id="menue-apartamentos"><span>APARTAMENTOS</span></a></li>
					<li><a href="#infraestrutura" id="menue-infraestrutura"><span>INFRAESTRUTURA</span></a></li>
					<li><a href="#localizacao" id="menue-localizacao"><span>LOCALIZACAO</span></a></li>
				</ul>
			</div>
			<img src="<?php echo IMGPATH ?>separador.png" alt="separador" />
		</div>
	<div class="clear">&nbsp;</div>
	</div>
	

	
	<!-- OS APARTAMENTOS -->
		<?php 
			$id = zt_page::get_ID_by_slug('os-apartamentos');
			$page = get_page($id);
		?>
		<div id="apartamentos" class='bloco-conteudo'>
			<div id="apartamentos-esquerda">
				<h2 class="titulo-conteudo ff titulo-contato"><?php echo apply_filters('the_title',$page->post_title) ?></h2>
				<?php echo apply_filters('the_content',$page->post_content) ?>
				<img src="<?php echo IMGPATH; ?>3-dorms.png" alt="3 dorms. com 2 vagas, 2 dorms. com 1 vaga" />
			</div>
			
			<div id="apartamentos-direita" class="relativo">
				<div id="carrossel-apartamentos" class="relativo">
					<?php
						the_carousel_content( $id, 'apartamentos', 'scroll: 1' );
					?>
				</div>
				<img class="sombra-carrossel" src="<?php echo IMGPATH ?>sombra2.png" alt="" />
			</div>
			
		<div class="clear">&nbsp;</div>
		</div>
	
	
	
	<!-- A INFRAESTRUTURA -->
		<?php 
			$id = zt_page::get_ID_by_slug('a-infraestrutura');
			$page = get_page($id);
		?>
	<div id="infraestrutura" class='bloco-conteudo gallery'>
		
		<div class="esquerda-cima">
			<h2 class="titulo-conteudo ff titulo-contato"><?php echo apply_filters( 'the_title', $page->post_title ); ?></h2>
			<img src="<?php echo IMGPATH; ?>infraestrutura-subtitulo.png" alt="Completa área de lazer na cobertura" class='subtitulo' />
		</div>
		
		<div class="direita-cima">
			<?php echo apply_filters('the_content',$page->post_content) ?>
		</div>
		
		<div class="clear">&nbsp;</div>
		
		<?php
		$images = zt_get_images( $id, 'thumb-1', 'asc' );
		$items_per_line = 4;
		if ( is_array($images) && count($images)>0 ): 
		foreach ($images as $key => $image): $last = ( ($key+1) % $items_per_line == 0 ) ? ' sem-margem' : ''; ?>
			<div class="galeria-infra relativo<?php echo $last; ?>">
				<a href="<?php echo $image['url_full']; ?>" title="<?php echo $image['title']; ?>">
					<img class="zindex" src="<?php echo $image['url'] ?>" alt="imagem" />
					<img class="img-mascara" src="<?php echo IMGPATH ?>mascara-img.png" alt="imagem" width="203" height="132" />
					<img class="sombra" src="<?php echo IMGPATH ?>sombra3.png" alt="sombra" />
				</a>
				<p class="legenda"><?php echo $image['title']; ?></p>
			</div>
		<?php 
			if ( $key == 8 ) break;
			endforeach;
		endif; ?>
			
		<div class="clear">&nbsp;</div>
	</div>

	
	<!-- LOCALIZAÇÃO -->
		<?php 
			$id = zt_page::get_ID_by_slug('localizacao');
			$page = get_page($id);
		?>
	<div id="localizacao" class='bloco-conteudo'>
		<div class="esquerda-cima">
			<h2 class="titulo-conteudo ff titulo-contato"><?php echo apply_filters('the_title',$page->post_title) ?></h2>
		</div>
		
		<div class="direita-cima">
			<?php echo apply_filters('the_content',$page->post_content) ?>
		</div>
		
		<div id="esquerda-baixo" class="relativo">
			<img class="sombra" src="<?php echo IMGPATH ?>sombra4.png" alt="sombra" />
			<div id="map_canvas"></div>
		</div>
		
		<div id="direita-baixo">
			<a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.01650, -51.16951));" id="plantao" >
				<img src="<?php echo IMGPATH; ?>plantao.png" alt="Joy" />
			</a>
			<div class="clear">&nbsp;</div>
			<ul>
				<li class="loc-posto"><a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.016838, -51.171908));"><span>&nbsp;</span>Posto 35 (500m)</a></li>
				<li class="loc-mercado"><a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.02046, -51.18089));"><span>&nbsp;</span>Supermercado Zaffari Higienópolis (1Km)</a></li>
				<li class="loc-mercado"><a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.013516, -51.171777));"><span>&nbsp;</span>Supermercado Carrefour (1Km)</a></li>
				<li class="loc-shopping"><a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.022665, -51.162386));"><span>&nbsp;</span>Shopping Bourbon Country (2Km)</a></li>
				<li class="loc-shopping"><a href="#" onclick="map.setCenter(new google.maps.LatLng(-30.02663, -51.16315));"><span>&nbsp;</span>Shopping Iguatemi (2Km)</a></li>
			</ul>
		</div>
			
		<div class="clear">&nbsp;</div>
	</div>
	

	
	<!-- CONTATO -->
		<?php 
			$id = zt_page::get_ID_by_slug('contato');
			$page = get_page($id);
		?>
		<div id="contato" class='bloco-conteudo'>
			<div id="contato-esquerda">
				<h2 class="titulo-conteudo ff titulo-contato"><?php echo apply_filters('the_title',$page->post_title) ?></h2>
				
				
				<?php echo apply_filters('the_content',$page->post_content) ?>
				
			</div>

			<!--

		var msg 	= '';
		vet_dados 	= 'nome='+ nome
					  +'&email='+ email
					  +'&telefone='+ telefone
					  +'&empreendimento='+ empreendimento
					  +'&contato='+ contato
					  +'&descricao='+ descricao;

		base_url  	= "http://www.nexgroup.com.br/central/enviarAtendimentoUnico";


		-->
			
			<div id="contato-direita" class="relativo">
				<form action="http://www.nexgroup.com.br/central/enviarAtendimentoUnicoDhz" method="post" id="contato-form">
					<input type="hidden" name="empreendimento" value="71" />
					<input type="hidden" name="origem" value="<?=@$_SESSION['origem']?>" />
					<input type="hidden" name="url" value="<?=@$_SESSION['url']?>" />
					
					<div>
						<label for="nome">Nome*:</label><br />
						<input type="text" name="nome" class="m-right-30" id="nome" />
					</div>
					
					<div>
						<label for="email">Email*:</label><br />
						<input type="text" name="email" id="email"/>
					</div><br />
					
					<div>
						<label for="telefone">Telefone:</label><br />
						<input type="text" name="telefone" class="m-right-30" />
					</div>
					<div>
						<label for="tipo-solicitacao">Tipo de solicitação:</label><br />
						<select name="solicitacao" id="select">
							<option value="">Selecione a opção desejada</option>
							<option value="Quero visitar o empreendimento">Quero visitar o empreendimento</option>
							<option value="Quero receber mais informações">Quero receber mais informações</option>
							<option value="Quero adquirir um apartamento">Quero adquirir um apartamento</option>
							<option value="Quero solicitar um novo atendimento">Quero solicitar um novo atendimento</option>
							<option value="Outro">Outro</option>
						</select>
					</div>
					
					<div>
						<label for="mensagem">Mensagem*:</label><br />
						<textarea name="descricao" id="mensagem" cols="30" rows="10"></textarea>
					</div>
					
					<div class="msg-enviando aviso bold">
						<img src="<?php echo IMGPATH . 'ajax-loader.gif'?>" alt="Carregando" /> 
						<p>Enviando a mensagem...</p>
					</div>
					
					<div class="msg-enviada aviso bold">
						<img src="<?php echo IMGPATH ?>email-ok.png" alt="Mensagem OK" />
						<p><?php zto('mensagem-contato', 'Obrigado! Sua mensagem foi enviada com sucesso e em breve iremos respondê-la.')?></p>
					</div>
					
					<div class="msg-erro aviso bold">
						<img src="<?php echo IMGPATH ?>email-erro.png" alt="Mensagem OK" />
						<p>Atenção! Não esqueça de preencher: <span class="campos"></span></p>
					</div>

					<div id="bt-enviar">
						<input type="submit" onclick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Enviar Mensagem de Contato']);"/>
					</div>
					
				</form>
			</div>
			<div class="clear">&nbsp;</div>
			
		</div>
	<div class="clear">&nbsp;</div>
	</div>
</div>

<?php get_footer(); ?>
