<?php

/**
 * @param string $type - ID do tipo de conteúdo
 * @param array $args
 * @author Thiago Locks - Zira
 */
class zt_post_type {

	public $args = array();
	public $type;
	public $doit;

	function __construct($type, $args=array()) {
		$this->type = $type;
		$args_default = array(
			'menu_icon'=> get_bloginfo('template_url')."/images/adm_{$this->type}.png",
			'has_archive' => true,
			'rewrite' => array('slug' => $this->type),
			'public' => true,
		);
		$this->args = $args + $args_default;
		$this->doit = new zt_post_type_do($this);
	}

	
	/**
	 * Personaliza o campo "Imagem Destacada"
	 * @param string $label - Label do MetaBox
	 * @param string $context - Local do MetaBox, podendo ser: 'normal', 'advanced', ou 'side'
	 * @param string $priority - Prioridade do MetaBox, podendo ser: 'high', 'core', 'default' ou 'low'
	 * @author Thiago Locks - Zira
	 */
	function thumbnail($label='Imagem', $context='side', $priority='low') {
		$this->thumb_label = $label;
		$this->thumb_context = $context;
		$this->thumb_priority = $priority;
		add_action( 'do_meta_boxes', array(&$this->doit, '_thumbnail_do')) ;
	}
	
	
	function add_mb( $args ) {
		$args_default = array('context'=>'normal', 'priority'=>'low');
		$this->_mbs[] = $args + $args_default; 
	}
	
	
	/**
	 * Adiciona um metabox para link - ID do campo é {post-type}-link
	 * @param array $args - Array para sobreescrever os parâmetros, como "label", "context", "priority"
	 */
	function add_mb_link( $args = array() ) {
		$args_default = array('label'=>'URL do Link', 'context'=>'normal', 'priority'=>'low', 'fields'=>array(array('id'=>$this->type.'-link', 'filter'=>'url')));
		$this->_mbs[] = $args + $args_default; 
	}
	
	
	/**
	 * Adiciona um metabox para data - ID do campo é {post-type}-data
	 * @param array $args - Array para sobreescrever os parâmetros, como "label", "context", "priority"
	 */
	function add_mb_data( $args = array() ) {
		$args_default = array('label'=>'Data', 'context'=>'side', 'priority'=>'core', 'fields'=>array( array( 'id'=>$this->type.'-data', 'type'=>'date', 'filter'=>'date') ) );
		$this->_mbs[] = $args + $args_default;
		$this->add_support( array('date') );
	}
	
	/**
	 * Cria um metabox para imagens
	 * @param array $args
	 */
	function add_mb_gallery( $args = array() ) {
		$args_default = array('label'=>'Galeria', 'context'=>'normal', 'priority'=>'core', 'type'=>'gallery' );
		$this->_mbs[] = $args + $args_default;
		$this->add_support( array('gallery') );
		add_action( 'wp_ajax_zt_gallery_upload', array(&$this->doit, '_do_gallery_upload') );
		add_action( 'admin_init', array(&$this->doit, '_galeria_init') );
	}
	
	
	/**
	 * Principais argumentos:
	 * id - ID do campo
	 * label - Label do metabox
	 * content - ID do tipo de conteúdo relacionado
	 * context - Local do MetaBox, podendo ser: 'normal', 'advanced', ou 'side'
	 * priority - Prioridade do MetaBox, podendo ser: 'high', 'core', 'default' ou 'low'
	 * @param array $args de argumentos: ID, label, content, context, priority
	 */
	function add_rel( $args ) {
		$args_default = array('label'=>'Relacionamento', 'context'=>'side', 'priority'=>'core', 'fields'=>array( array( 'id' => $args['id'], 'type'=>'select', 'content'=>$args['content'], 'class'=>'grande' ) ) );
		unset($args['id']);
		$args_default['fields'][0]['options'] = array('0'=>'- Selecione -') + $this->doit->_get_content_array($args['content']);
		$this->_mbs[] = array_merge($args_default, $args); 
	}
	
	/**
	 * Cria os metaboxes
	 */
	function do_mb( ) {
		add_action('do_meta_boxes', array(&$this->doit, '_do_mb'));
	}
	

	/**
	 * Adiciona as bibliotecas necessárias para campos de vídeo, imagem, editor de texto ou data
	 * @param array $support - Array de tipos de campo a serem suportados, podendo ser video, image, editor ou date
	 */
	function add_support( $support = array() ){
		if ( in_array( 'video', $support ) ) {
			add_action( 'admin_init', array( &$this->doit, '_add_js_video' ) );
		}
		if ( in_array( 'image', $support ) ) {
			add_action( 'admin_init', array( &$this->doit, '_add_js_image' ) );
		}
		if ( in_array( 'editor', $support ) ) {
			add_action( 'admin_init', array( &$this->doit, '_add_js_editor' ) );
		}
		if ( in_array( 'date', $support ) ) {
			add_action('admin_init', array(&$this->doit, '_add_js_datapicker'));
		}
		if ( in_array( 'gallery', $support ) ) {
			add_action('admin_init', array(&$this->doit, '_scripts_gallery_init'));
			add_action( 'admin_head-post-new.php', array(&$this->doit, '_scripts_gallery') );
			add_action( 'admin_head-post.php', array(&$this->doit, '_scripts_gallery') );
		}
	}

	
	/**
	 * Adiciona uma coluna na listagem de "posts"
	 * @param array $args - Array de colunas que precisa do ID do campo a ser exposto e um Label
	 * @author Thiago Locks - Zira
	 */
	function add_col( $args ) {
		$this->_cols[] = $args;
	}
	
	function do_cols( ) {
		add_filter('manage_edit-' . $this->type . '_columns', array(&$this->doit, '_do_cols'));
		add_action('manage_' . $this->type . '_posts_custom_column', array(&$this->doit, '_do_cols_content'), 10, 2);
	}
	
	function sort() {
		add_action('admin_menu', array(&$this->doit, '_menu_organizador'));
		add_action('admin_print_scripts', array(&$this->doit, '_scripts_organizador'));
		add_action('admin_print_styles',  array(&$this->doit, '_styles_organizador'));
	}
	
}



class zt_post_type_do {

	public $args = array();
	public $type;
	public $post;

	function __construct( $post ) {
		$this->type = $post->type;
		$this->args = $post->args;
		$this->post = $post;
		if ( !in_array($post->type, array('post', 'page')) ) {
			add_action( 'init', array(&$this, '_register') );
			add_action( 'admin_head', array(&$this, '_admin_head') );
			add_action( 'admin_init', array( &$this, '_add_css_form' ) );
		}
		add_action( 'save_post', array(&$this, '_save_post') );
	}

	function _register() {
		register_post_type( $this->type, $this->args);
	}


	function _admin_head() {
		global $post_type;
		if ((isset($_GET['post_type']) && $_GET['post_type'] == $this->type) || ($post_type == $this->type)) : ?>
			<style type="text/css">#icon-edit { background:transparent url('<?php echo get_bloginfo('template_url').'/images/adm_'.$this->type.'32.png';?>') no-repeat; }</style> <?php		
		endif;
	}
	
	
	/***************************************
	* META BOX
	***************************************/
	
	function _do_mb() {
		if (is_array($this->post->_mbs)):
			foreach ($this->post->_mbs as $key=>$mb):
				$this->_mb_current = $key;
				switch ( @$mb['type'] ){
					case 'gallery':
						add_meta_box( "zt-mb-{$key}", $mb['label'], array(&$this, '_mb_print_gallery'), $this->type, $mb['context'], $mb['priority'], array('key'=>$key) );
						break;
					default:
						add_meta_box( "zt-mb-{$key}", $mb['label'], array(&$this, '_mb_print'), $this->type, $mb['context'], $mb['priority'], array('key'=>$key) );
				}
				
			endforeach;
		endif;
	}
	
	function _mb_print($post, $metabox) {
		$custom = get_post_custom($post->ID);
		$fields = $this->post->_mbs[$metabox['args']['key']]['fields'];
		$class = @$this->post->_mbs[$metabox['args']['key']]['class'];
		echo "<div class='ztmb {$class}'>";
		foreach($fields as $field):
			$valor = ( isset($custom[$field['id']][0] ) ) ? $custom[$field['id']][0] : '';
			$valor = ( empty($valor) && isset($field['default']) ) ? $field['default'] : $valor; 
			$attrs = isset($field['attrs']) && is_array($field['attrs']) ? $field['attrs'] : array();
			echo '<p id="p-'.$field['id'].'">';
			if (isset($field['label']))
				echo '<label for="'.$field['id'].'" class="' . $field['label_class'] . '">'.$field['label'].'</label>';
			
			switch (@$field['type']){
				case 'text':
					$default = array( 'class'=>'large-text' );
					if ( isset($field['editor']) && ($field['editor'] == TRUE) ) {
						add_action('admin_print_footer_scripts', array(&$this, '_add_js_editor'), 99);
						echo '<div class="editor-toolbar">
							<a onclick="switchEditors.go(\'customEditor-1\', \'html\');jQuery(\'\')" class="hide-if-no-js edButtonHTML">HTML</a>
							<a onclick="switchEditors.go(\'customEditor-1\', \'tinymce\');" class="active hide-if-no-js edButtonPreview">Visual</a></div>';
						echo '<div class="customEditor">';
							echo html::textarea( $field['id'], wp_richedit_pre($valor), array_merge( $default, $attrs ) );
						echo '</div>';
					} else
						echo html::textarea( $field['id'], $valor, array_merge( $default, $attrs ) );
					
					break;
				case 'select':
					$attr = isset($field['attr']) ? $field['attr'] : array();
					if (is_array(@$field['options']))
						echo HTML::select( $field['id'], @$field['options'], $valor, array_merge( array( 'class'=>@$field['class'] ), $attrs) + $attr, @$field['num'] );
					else
						echo "<br/><i>Nenhuma opção disponível</i><br/>&nbsp;";
					break;
				case 'date':
					$default = array( 'class'=>'all-options', 'id'=>$field['id'], 'type'=>'text' );
					echo html::input( $field['id'], databr( $valor ), array_merge( $default, $attrs ) );
					echo '<script type="text/javascript">	jQuery(document).ready(function(){ jQuery("#'.$this->type.'-data").datepicker({	dateFormat : "dd/mm/yy"	});	});	</script>';
					break;
				case 'video':
					$default = array( 'class'=>'all-options zt-video' );
					echo html::input( $field['id'], $valor, array_merge( $default, $attrs ) );
					$capa = (!empty($valor)) ? Zt_Video::get_the_screen($valor) : '';
					echo "<div class='screen-video' style='text-align:center'>{$capa}</div>";
					break;
				case 'checkbox-taxomony':
					$selected = explode( ',', $valor );
					echo '<ul class="taxonomy-checklist">';
					zt_taxonomy_checklist( $field['id'], $field['taxonomy'], $selected );
					echo '</ul>';
					break;
				case 'checkbox':
					$attr = isset($field['attr']) ? $field['attr'] : array();
					if (is_array(@$field['options']))
					echo HTML::checkboxes( $field['id'], $field['options'], $valor, array_merge( array( 'class'=>@$field['class'] ), $attrs) + $attr, @$field['num'] );
					else
					echo "<br/><i>Nenhuma opção disponível</i><br/>&nbsp;";
					break;
				case 'radio':
					$attr = isset($field['attr']) ? $field['attr'] : array();
					if (is_array(@$field['options']))
						echo HTML::radio( $field['id'], @$field['options'], $valor, array_merge( array( 'class'=>@$field['class'] ), $attrs) + $attr, @$field['num'] );
					else
						echo "<br/><i>Nenhuma opção disponível</i><br/>&nbsp;";
					break;
				case 'html':
					echo $field['content'];
					break;
				default:
					$default = array( 'class'=>'regular-text', 'type'=>'text' );
					echo html::input( $field['id'], $valor, array_merge( $default, $attrs ) );
			}
			echo @$field['after'];
			echo '</p>';
		endforeach;
		echo "</div>";
	}
	
	
	function _save_post(  ) {
		global $post;
		
		// Impede o autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post->ID;
		}

		// Verifica se está no tipo de conteúdo correto
		if ( @$post->post_type == $this->type ) {
			
			if ( is_array( $this->post->_mbs ) ):
				// Loop de metaboxs
				foreach ($this->post->_mbs as $mb_key=>$mb):
					if ( isset($mb['fields']) && is_array($mb['fields']) ) {
						// Loop dos campos do metabox
						foreach ($mb['fields'] as $field_key => $field):
							$this->_save_field($field['id'], $mb_key, $field_key, $field);
						endforeach;
					}
				endforeach;
			endif;
			
			// Salva galeria
			if ( isset($_POST['zt_galeria']) && $_POST['zt_galeria']==1 ):
				$this->_galeria_remove_imagens();
				$this->_save_field( 'zt_galeria_images', NULL, NULL, 'json');
			endif;
			
		}
		
		
	}
	
	function _save_field( $key, $mb_key, $field_key, $field, $divisor=',' ) {
		global $post;
		
		switch ( $field['type'] ) {
			case "checkbox-taxomony":
				$campos = @$_POST['tax_input'][$field['taxonomy']];
				break;
			default:
				$campos = @$_POST[$key];
				break;
		}
		
		if ( is_array( $campos ) ) {
			if ( $divisor=='json' ) {
				foreach( $campos as &$item ) {
					$item = json_decode( stripslashes( $item ));
				}
				$valor = json_encode( $campos );
			} else
				$valor = implode( $divisor, $campos );
		} else {
			$valor = $campos;
		}
		
		if (isset($this->post->_mbs[$mb_key]['fields'][$field_key]['filter'])) {
			switch($this->post->_mbs[$mb_key]['fields'][$field_key]['filter']){
				case 'url':
					$valor = zt_filters::url($valor);
					break;
				case 'date':
					$valor = zt_filters::date($valor);
					break;
			}
		}
		$valor = trim($valor);
		update_post_meta($post->ID, $key, $valor);
	}
	
	
	
	/***************************************
	* THUMBNAILS
	***************************************/
	
	function _thumbnail_do() {
		remove_meta_box( 'postimagediv', $this->type, 'side' );
		add_meta_box('postimagediv', $this->post->thumb_label, 'post_thumbnail_meta_box', $this->type, $this->post->thumb_context, $this->post->thumb_priority);
	}
	
	
	
	/***************************************
	* COLUNAS - LISTAGEM
	***************************************/
	
	function _do_cols($columns) {
		$columns1 = array_slice($columns, 0, 2);
		$columns2 = array_slice($columns, 2);
		
		foreach ($this->post->_cols as $col):
			$new_columns[$col['id']] = $col['label'];
		endforeach;
		
		$columns = $columns1 + $new_columns + $columns2;
		
		return $columns;
	}
	
	function _do_cols_content($column_name, $id) {
		$custom = get_post_custom($id);
		echo $custom[$column_name][0];
	}
	
	
	
	/***************************************
	 * ORGANIZADOR
	 ***************************************/
	
	function _menu_organizador() {
		add_submenu_page('edit.php?post_type='.$this->type, 'Ordenar', 'Ordenar', 'manage_options', $this->type.'-organizador', array( &$this, '_page_organizador' ) );
	}
	
	function _page_organizador() {
		$itens = new WP_Query( array('post_type'=>$this->type, 'posts_per_page'=>-1, 'orderby'=>'menu_order', 'order'=>'ASC') );
		echo '<div class="wrap">';
		echo '<h2>Ordenar</h2>';
		echo '<div class="fundo-escuro titulo-lista-ordenavel">Título</div>';
		if ($itens->have_posts()):
			echo '<ul id="itens-organizaveis">';
			$i = 0;
			while ($itens->have_posts()):
				$i++;
				$class = $i%2 ? "class='alternate'" : '';
				$itens->the_post();
				echo "<li id='item_".get_the_ID()."' {$class}>".get_the_title()."</li>";
			endwhile;
			echo '</ul>';
			echo '<button type="button" id="itens-salvar" class="button-secondary action">Salvar</button>';
		else:
			echo '<br/><em>'.$this->args['labels']['not_found'].'</em>';
		endif;
		echo '</div>';
		wp_reset_postdata();
	}
	
	function _scripts_organizador(){
		if ( isset($_GET['page']) && $_GET['page'] == $this->type.'-organizador' ) {
			wp_register_script('zt_organizador-dinamico', get_bloginfo('template_url').'/includes/post-type/adm-organizador-dinamico.js.php');
			wp_register_script('zt_organizador', get_bloginfo('template_url').'/includes/post-type/adm-organizador.js', array('zt_organizador-dinamico', 'jquery', 'jquery-ui-core', 'jquery-ui-sortable'));
			wp_enqueue_script('zt_organizador');
		}
	}
	
	function _styles_organizador(){
		if ( isset($_GET['page']) && $_GET['page'] == $this->type.'-organizador' ) {
			wp_register_style('zt_organizador', get_bloginfo('template_url') . '/includes/post-type/adm-organizador.css');
			wp_enqueue_style('zt_organizador');
		}
	}
	
	
	function _add_js_datapicker() {
		$themefolder = get_bloginfo( 'template_url' ) . '/js';
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-datepicker', $themefolder . '/jquery.ui.datepicker.min.js', array('jquery', 'jquery-ui-core') );
		wp_enqueue_script( 'jquery-ui-datepicker-BR', $themefolder . '/jquery.datepick-pt-BR.js', array('jquery', 'jquery-ui-core') );
		wp_enqueue_style( 'jquery.ui.theme', $themefolder . '/jquery-ui-smoothness/jquery-ui-1.8.14.custom.css' );
	}
	
	
	function _add_js_video() {
		$themefolder = get_bloginfo( 'template_url' ) . '/js';
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'zt-video', $themefolder . '/zt-video.js', array('jquery') );
	}

	function _add_js_editor()
	{
		$themefolder = get_bloginfo( 'template_url' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'zt-editor', $themefolder . '/js/adm-editor.js', array('jquery') );
	}
	
	function _add_css_form()
	{
		$themefolder = get_bloginfo( 'template_url' );
		wp_enqueue_style( 'zt-form', $themefolder . '/css/adm-form.css' );
	}
	
	function _get_content_array($content, $hierarchical=TRUE) {		
		$args = array( 'post_type'=>$content, 'posts_per_page'=>-1 );
		$args += $hierarchical ? array('orderby'=>'menu_order', 'order'=>'ASC') : array();
		
		$query = new WP_Query( $args );
		$posts = array();
		while($query->have_posts()): $query->the_post();
			$posts[get_the_ID()] = get_the_title();
		endwhile;
		wp_reset_postdata();
		return $posts;
	}
	
	
	/***************************************
	 * GALERIA
	 ***************************************/
	
	function _mb_print_gallery(){
		require TEMPLATEPATH . '/includes/post-type/gallery.php';
	}
	
	function _scripts_gallery(){
		$themefolder = get_bloginfo( 'template_url' ) ; ?>
		<script type="text/javascript">
		//<![CDATA[
			theme_image_url = '<?php echo IMGPATH ?>';
		//]]>
		</script>		
		<script type="text/javascript" src="<?php echo $themefolder . '/js/fileuploader.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $themefolder . '/js/adm-fileuploader.js'; ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo $themefolder . '/css/fileuploader.css'; ?>" media="all" />
		<?php
	}
	
	function _scripts_gallery_init(){
		//wp_register_script('zt_organizador-dinamico', get_bloginfo('template_url').'/includes/post-type/adm-organizador-dinamico.js.php');
		//wp_register_script('zt_organizador', get_bloginfo('template_url').'/includes/post-type/adm-organizador.js', array('zt_organizador-dinamico', 'jquery', 'jquery-ui-core', 'jquery-ui-sortable'));
		//wp_enqueue_script('zt_organizador');
	}
	
	function _do_gallery_upload() {
		
		$post_id = $_GET['postid'];
		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array('jpeg', 'jpg');
		// max file size in bytes
		$sizeLimit = 10 * 1024 * 1024;
		$dir = WP_CONTENT_DIR. '/uploads/zt_gallery/';
		if ( ! file_exists( $dir ) ) {
			@mkdir( $dir );
			@chmod( $dir );
		}
		$dir = WP_CONTENT_DIR. '/uploads/zt_gallery/' . $post_id .'/';
		if ( ! file_exists( $dir ) ) {
			@mkdir( $dir );
			@chmod( $dir );
		}
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload( $dir, FALSE, $post_id );
		$file = pathinfo( $result['filepath'] );
		$result['file_name'] = $file['basename'];
		$result['post_id'] = $post_id; 
		
		$sizes = get_sizes();
		foreach ( $sizes as $key=>$size ):
			$resized_file = image_resize( $result['filepath'], $size['width'], $size['height'], $size['crop'] );
			if ( !is_wp_error($resized_file) && $resized_file && $info = getimagesize($resized_file) ) {
				$resized_file = apply_filters('image_make_intermediate_size', $resized_file);
				$file_name = wp_basename( $resized_file );
				$result[ $key ] = array(
					'file' => $file_name,
					'width' => $info[0],
					'height' => $info[1],
					'url' => WP_CONTENT_URL . '/uploads/zt_gallery/' . $post_id . '/' . $file_name
				);
			}
		endforeach;
		
		$zt_galeria_images = get_post_meta($post_id, 'zt_galeria_images');
		if ( $zt_galeria_images ) $zt_galeria_images = json_decode($zt_galeria_images[0]);
		else $zt_galeria_images = array();
		
		$zt_galeria_images[] = $result; 
		
		update_post_meta( $post_id, 'zt_galeria_images', json_encode($zt_galeria_images) );
		
		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars( json_encode($result), ENT_NOQUOTES );
		
		die();
	}
	
	function _galeria_remove_imagens() {
		global $post;
		$custom = get_post_custom( $post->ID );
		if ( $_POST['zt_galeria'] == 1 ) {

			// Pega as novas imagens
			$images_new = array();
			if ( isset($_POST['zt_galeria_images']) AND is_array($_POST['zt_galeria_images']) )
			foreach ( $_POST['zt_galeria_images'] as $key=>$image ):
				$img = json_decode( stripslashes( $image ) );
				$images_new[$key] = $img->filepath;
			endforeach;
			
			// Pega as imagens antigas
			$images_db = json_decode( $custom['zt_galeria_images'][0] );
			$images_old = array();
			if ( is_array($images_db) )
			foreach ( $images_db as $img ):
				$images_old[] = $img->filepath;
			endforeach; 
			
			// Compara e remove a diferença
			$images_remove = array_diff( $images_old, $images_new );
			
			foreach ( $images_remove as $key=>$img ):
				$img = (array) $images_db[$key];
				$info = pathinfo( $img['filepath'] );
				foreach ( $img as $i ):
					if ( is_object($i) && isset($i->file) ):
						$file = $info['dirname'] . '/' . $i->file;
						if ( file_exists( $file ) ) unlink ( $file );
					endif;
				endforeach;
				if ( file_exists( $img['filepath'] ) ) unlink ( $img['filepath'] );
			endforeach;
		}
	}
	
	function _galeria_init() {
		if (current_user_can('delete_posts')) add_action( 'delete_postmeta', array(&$this, '_galeria_delete'), 10 );
	} 
	
	function _galeria_delete( $mid ) {
		global $wpdb;
		
		$pid = $wpdb->get_var( $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_id = {$mid[0]}"));
		$post = get_post( $pid );
		if ( $post->post_status == 'trash' ) {
			$galeria = get_metadata('post', $pid, 'zt_galeria_images' );
			
			$images_db = json_decode( $galeria[0] );
			foreach ( $images_db as $image ):
				$img = (array) $image;
				$info = pathinfo( $img['filepath'] );
				if ( file_exists( $img['filepath'] ) ) unlink ( $img['filepath'] );
				foreach ( $img as $i ):
					if ( is_object($i) && isset($i->file) ):
						$file = $info['dirname'] . '/' . $i->file;
						if ( file_exists( $file ) ) unlink ( $file );
					endif;
				endforeach;
			endforeach; 
			// Exclui a pasta
			if ( file_exists($info['dirname'] )) rmdir( $info['dirname'] );
		}
	}
	
	
}

class Zt_Filters {
	
	static function url($valor) {
		$valor = strtolower($valor);
		if (!empty($valor))
			return in_array(substr($valor, 0, 7), array('http://', 'https:/')) ? $valor : 'http://'.$valor;
		else
			return "";
	}
	
	static function date($valor) {
		return implode( '-', array_reverse( explode( '/', $valor ) ) );
	}
	
}



/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {    
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}


/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        //$this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size. $postSize < $this->sizeLimit (limit); uploadSize = $uploadSize'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE, $id = 0){
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }
        
        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
            return array('success'=>true, 'file'=>get_bloginfo('url') . '/wp-content/uploads/zt_gallery/' . $id . '/' . $filename . '.' . $ext, 'filepath'=>WP_CONTENT_DIR . '/uploads/zt_gallery/' . $id . '/' . $filename . '.' . $ext);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
        
    }    
}


function get_sizes() {
	global $_wp_additional_image_sizes;

	foreach ( get_intermediate_image_sizes() as $s ) {
		$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => FALSE );
		if ( isset( $_wp_additional_image_sizes[$s]['width'] ) )
			$sizes[$s]['width'] = intval( $_wp_additional_image_sizes[$s]['width'] ); // For theme-added sizes
		else
			$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
		if ( isset( $_wp_additional_image_sizes[$s]['height'] ) )
			$sizes[$s]['height'] = intval( $_wp_additional_image_sizes[$s]['height'] ); // For theme-added sizes
		else
			$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
		if ( isset( $_wp_additional_image_sizes[$s]['crop'] ) )
			$sizes[$s]['crop'] = intval( $_wp_additional_image_sizes[$s]['crop'] ); // For theme-added sizes
		else
			$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	}

	return apply_filters( 'intermediate_image_sizes_advanced', $sizes );
}
