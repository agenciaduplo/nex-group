<?php


// Classe de criação de tags html
class HTML {
	
	public static function select( $nome, $opcoes, $selecao=NULL, $atributos=NULL, $numKey=FALSE ) {
		$atributos['name'] = $nome;
		$atributos['id'] = $nome;
		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		$select  = "<select $ats>";
		if (isset($opcoes) AND is_array($opcoes)){
			foreach ($opcoes as $op_key=>$opcao):
				if (is_int($op_key) AND $numKey==FALSE) $op_key = $opcao;
				$sel = $op_key == $selecao ? 'selected="selected"' : '';
				$select .= "<option value='{$op_key}' $sel>{$opcao}</option>";
			endforeach;
		}
		$select .= "</select>";
		return $select;
	}
	
	public static function radio( $nome, $opcoes, $selecao=NULL, $atributos=NULL, $numKey=FALSE ) {
		$atributos['name'] = $nome;
		// Remove o ID dos atributos
		unset( $atributos['id'] );
		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		$inputs = '<div class="radio-group">';
		if (isset($opcoes) AND is_array($opcoes)){
			foreach ($opcoes as $op_key=>$opcao):
				if (is_int($op_key) AND $numKey==FALSE) $op_key = $opcao;
				$sel = $op_key == $selecao ? 'checked="checked"' : '';
				$inputs .= "<input type='radio' {$ats} id='{$nome}-{$op_key}' value='{$op_key}' $sel > <label for='{$nome}-{$op_key}'>{$opcao}</label>";
			endforeach;
		}
		$inputs .= '</div>';
		return $inputs;
	}
	
	public static function textarea( $nome, $valor, $atributos ) {
		$atributos['name'] = $nome;
		$atributos['id'] = $nome;
		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		$input = "<textarea {$ats}>{$valor}</textarea>";
		return $input;
	}
	
	public static function checkbox($nome, $value, $checked, $atributos, $numKey=FALSE ) {
		$atributos['name'] = $nome;
		$atributos['id'] = $nome;
		$atributos['value'] = $value;
		if ( $checked==TRUE ) $atributos['checked'] = 'checked';

		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		
		$input = "<input type='checkbox' {$ats} />";
		return $input;
	}
	
	public static function checkboxes( $nome, $opcoes, $selecao=NULL, $atributos=NULL, $numKey=FALSE ) {
		$atributos['name'] = $nome.'[]';
		// Remove o ID dos atributos
		unset( $atributos['id'] );
		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		$selecao = explode( ',', $selecao );
		$inputs = '<div class="check-group"><ul>';
		if (isset($opcoes) AND is_array($opcoes)){
			foreach ($opcoes as $op_key=>$opcao):
				if (is_int($op_key) AND $numKey==FALSE) $op_key = $opcao;
				$sel = in_array($op_key, $selecao) ? 'checked="checked"' : '';
				$inputs .= "<li><input type='checkbox' {$ats} id='{$nome}-{$op_key}' value='{$op_key}' $sel > <label for='{$nome}-{$op_key}'>{$opcao}</label></li>";
			endforeach;
		}
		$inputs .= '</ul></div>';
		return $inputs;
	}
	
	public static function input( $nome, $valor, $atributos ) {
		$atributos['name'] = $nome;
		$atributos['id'] = $nome;
		$ats = '';
		foreach ($atributos as $key=>$at):
			$ats .= $key.'="'.$at.'" ';
		endforeach;
		$input = "<input {$ats} value='{$valor}' />";
		return $input;
	}
}




// Lida com datas
class Zt_Data {
	
	// Retorna uma data por extenso
	public static function extenso( $valor ){
		$data = explode( '-', $valor );
		$dia = $data[1];
		return ucfirst(date_i18n('l, d \d\e F', mktime(0, 0, 0, $data[1], $data[2], $data[0])));
	}
	
	// Transforma uma data no formato brasileiro
	public static function br( $valor ){
		return implode('/', array_reverse( explode('-', $valor)));
	}
	
}




// Lida com as páginas
class Zt_Page {
	
	public static function get_ID_by_slug( $slug ) {
		global $wpdb;
		return $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$slug."'");
	}
	
	public static function get_link_by_slug( $slug, $complemento=null ) {
		$link = get_permalink(Zt_Page::get_ID_by_slug( $slug ));
		if ( $complemento ) {
			$url = parse_url($link);
			if ( !isset($url['query']) || empty($url['query']))
				$link .= '?';
			else
				$link .= '&';
			$link .= $complemento;
		}
		return $link;
	}
	
	public static function current( $complemento ) {
		global $post;
		$url = get_permalink( $post->ID );
		$url_aberta = parse_url($url);
		if ( ! isset( $url_aberta['query']) || empty($url_aberta['query']) )
			$url .= '?';
		else
			$url .= '&';
		$url .= $complemento;
		return $url;
	}
	
}


function get_permalink_and( $complemento ) {
	global $post;
	$url = get_permalink();
	$url_aberta = parse_url($url);
	if ( ! isset( $url_aberta['query']) || empty($url_aberta['query']) )
		$url .= '?';
	else
		$url .= '&';
	$url .= $complemento;
	return $url;
}


// Cria Breadcrumb
function zt_the_breadcrumb() {
	
	if (!is_home()) {
		if (is_category() || is_single()) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE));
			echo single_cat_title('', false);
			
			if (is_single()) {
				the_title();
			}
		} elseif (is_page()) {
			echo the_title();
		}
	} else echo 'POSTS';
	
}




// Gera os comentários
function zt_comentario( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment; ?>
	<div <?php comment_class(); ?> id="div-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class='comentario-box'>
			<div class="comment-author vcard">
				<?php echo get_avatar($comment, $size='60', get_bloginfo('template_url').'/images/mystery-man.jpg' ); ?>
				<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
			</div>
			<?php if ($comment->comment_approved == '0') : ?>
			<em><?php _e('Your comment is awaiting moderation.') ?></em>
			<br />
			<?php endif; ?>

			<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
			<?php comment_text() ?>
		</div>
		<div class="reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
	</div><?php
}




function zt_add_support( $feature, $options=array() ){
	$file = TEMPLATEPATH."/includes/library/{$feature}.php";
	if (!file_exists($file)) {
		$file = TEMPLATEPATH."/includes/library/{$feature}/{$feature}.php";
		if (!file_exists($file)) {
			$file = TEMPLATEPATH."/conteudos/{$feature}.php";
		}
	}
	global ${$feature.'Options'};
	${$feature.'Options'} = $options;
	include ($file);
}




// Imprimi na tela o "resumo"
function print_excerpt( $length, $seta=FALSE ) {
	echo get_print_excerpt($length, $seta=FALSE);
}

// Retorna o "resumo"
function get_print_excerpt ( $length, $seta=FALSE ) {
	global $post;
	$text = $post->post_excerpt;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = str_replace(']]>', ']]>', $text);
	}
	$text = strip_shortcodes($text); // optional, recommended
	$text = strip_tags($text); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags

	$text = substr($text,0,$length);
	$excerpt = reverse_strrchr($text, '.', 1);
	$img = $seta ? " <img src='".IMGPATH."setinhas.png' alt='seta' />" : '';
	if( $excerpt ) {
		return apply_filters('the_excerpt',$excerpt.$img);
	} else {
		return apply_filters('the_excerpt',$text.$img);
	}
}

// Returns the portion of haystack which goes until the last occurrence of needle
function reverse_strrchr( $haystack, $needle, $trail ) {
	return strrpos($haystack, $needle) ? substr($haystack, 0, strrpos($haystack, $needle) + $trail) : false;
}




// Busca a imagem destacada através do Título da página, ou retorna uma imagem alternativa
function zt_imagem_alternativa( $titulo, $imagem ) {
	$page = get_page_by_title( $titulo );
	if ($page->ID>0)
		echo get_the_post_thumbnail($page->ID);
	else
		echo "<img src='{$imagem}'/>";
}



// Retorna a campo customizado
function get_ztc( $id ) {
	global $custom, $post;
	if (!is_array($custom)) {
		$custom = get_post_custom($post->ID);
	}
	return isset($custom[$id][0]) ? $custom[$id][0] : NULL;	
}

// Imprime o campo customizado
function ztc( $id ) {
	echo get_ztc($id);
}





// Redireciona de um single para um archive
function zt_redirecionar(){
	header('Location: '.get_post_type_archive_link(get_post_type()));
}





// Imprime determinada opção do tema
function zto($id, $default='') {
	echo get_zto($id, $default);
}

// Retorna determinada opção do tema
function get_zto($id, $default='') {
	global $options;
	$value = @$options[$id];
	return $value ? $value : $default;
}



/**
 * Retorna uma string com a listagem de termos associados a determinado post sem os links
 * @param int $id
 * @param string $taxonomy
 * @param string $pre_text
 * @param string $separator
 */
function get_the_term_list_sem_link($id, $taxonomy, $pre_text="", $separator=", ") {
	$terms = get_the_terms($id, $taxonomy);
	$list = array();
	if (is_array($terms))
		foreach ( $terms as $term ):
			$list[] = $term->name;
		endforeach;
	$list = implode( $separator, $list );
	if (!empty($list)) return $pre_text.$list;
}



// Limita uma string adicionando reticências no final, com suporte a acentuação
function shortstr ($nome, $quantidade = 10) {
	$nome = (strlen($nome) > $quantidade)
		? utf8_encode(substr(utf8_decode($nome), 0, $quantidade)) . '...'
		: $nome;
	return $nome;
}


/**
 * Retorna um array com todas as imagens associadas aos posts selecionados
 * @param mix $id - pode ser o ID do post, ou um array de slugs e/ou ids
 * @param string $size - nome do tamanho da imagem
 * @param string $orderby - valor do campo orderby
 */
function zt_get_images( $id, $size = 'thumbnail', $orderby = 'rand' ) {
	if ( is_array($id) ) {
		$images = array();
		foreach( $id as $i ) {
			if ( ! filter_var( $id, FILTER_VALIDATE_INT) ) $i = Zt_Page::get_ID_by_slug( $i );
			$images += get_children( array('post_parent' => $i, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => $orderby) );
		}
	}
	else {
		$images = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => $orderby) );
	}
	if ( count($images) > 0) {
		$i = 0;
		$imgs = array();
		foreach ($images as $image) {
			$imgs[$i]['title'] = $image->post_title;   // title.
			$imgs[$i]['url_full'] = wp_get_attachment_url($image->ID); // url of the full size image.
			$preview_array = image_downsize( $image->ID, $size );
 			$imgs[$i]['url'] = $preview_array[0]; // thumbnail or medium image to use for preview.

			$i++;
		}
		
		if ( $orderby=='rand' ) shuffle( $imgs );
		
		return $imgs;
	}
}


/**
 * Retorna a listagem de categorias em uma cheklist
 * @param unknown_type $name
 * @param unknown_type $selected
 */
function zt_category_checklist($name = '', $selected = array()) {
	$name = esc_attr( $name );
	//	categories
	ob_start();
	wp_category_checklist(0,0, $selected, false, '', $checked_on_top = false);
	$checkboxes .= str_replace('name="post_category[]"', 'name="'.$name.'[]"', ob_get_clean());

	echo $checkboxes;
}


/**
 * Retorna a listagem de itens de determinada taxonomia em uma cheklist
 * @param string $name
 * @param string $taxonomy
 * @param array $selected
 * @param unknown_type $descendants_and_self
 * @param unknown_type $popular_cats
 * @param unknown_type $walker
 * @param unknown_type $checked_ontop
 */
function zt_taxonomy_checklist( $name = '', $taxonomy = 'category', $selected = array(), $descendants_and_self = 0, $popular_cats = false, $walker = null, $checked_ontop = true ) {
	$name = esc_attr( $name );
	//	categories
	ob_start();
	wp_terms_checklist(0, array(
			'taxonomy' => $taxonomy,
			'descendants_and_self' => $descendants_and_self,
			'selected_cats' => $selected,
			'popular_cats' => $popular_cats,
			'walker' => $walker,
			'checked_ontop' => $checked_ontop
	));
	$checkboxes .= str_replace('name="post_category[]"', 'name="'.$name.'[]"', ob_get_clean());

	echo $checkboxes;
}


/**
 * Retorna um array de todas as letras indicando a quantidade de posts por letra
 * @param string $post_type
 */
function posts_por_letra( $post_type='post' ){
    $posts = get_posts( array( 'post_type'=>$post_type, 'post_status'=>'publish', 'posts_per_page'=>-1 ) );
    $letras = array_flip( range('a','z') );
    array_walk( $letras, 'zerar' );
    foreach ( $posts as $post ){
        $letra = strtolower($post->post_title[0]);
        $letras[$letra] += 1;
    }
    return $letras;
}

function zerar( &$valor ){
	$valor = 0;
}


/**
 * Retorna o thumbnail, ou a primeira imagem do post
 */
function zt_get_image( ) {
	global $wp_query, $post;
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
	$image = $image[0];
	if ( ! $image ):
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		@$image = $matches [1] [0];
	endif;
	return $image;
}
