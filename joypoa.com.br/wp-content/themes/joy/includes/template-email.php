<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
</head>
<body bgcolor="#F1F1F1">
	<table cellpadding="0" cellspacing="0" width="100%" >
		<tr>
			<td style="background: #F1F1F1; padding:15px 30px">
				<table width="600" style="margin: 0 auto;" >
					<tr>
						<td style="background:#fff; font-family:Arial, Verdana; padding:10px 20px 40px; line-height:130%; color:#444444; -moz-box-shadow: 3px 3px 6px #888888; -webkit-box-shadow: 3px 3px 6px #888888; box-shadow: 3px 3px 6px #888888; border:1px solid #DDD; ">
							<h1 style="font-size:18px; border-bottom:1px solid #DDDDDD; padding-bottom:15px; margin-bottom:15px; font-weight: normal;"><?php echo @$assunto; ?></h1>
						
							<?php echo @$conteudo; ?>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		
		
	</div>
</body>
</html>