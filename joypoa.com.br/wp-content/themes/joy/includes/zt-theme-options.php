<?php

global $zto_groups;

if (is_array($zto_groups)) {
	add_action( 'admin_init', 'theme_options_init' );
	add_action( 'admin_menu', 'theme_options_add_page' );
}

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'ztheme', 'ztheme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Opções do Tema' ), __( 'Opções do Tema' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
* Adiciona o menu na Admin Bar
*/
function zt_admin_bar_render() {
	global $wp_admin_bar; $wp_admin_bar->add_menu( array( 'parent' => 'appearance', 'id' => 'theme_options', 'title' => __('Opções do Tema'), 'href' => admin_url( 'themes.php?page=theme_options' ) ) );
}
add_action( 'wp_before_admin_bar_render', 'zt_admin_bar_render' );


/**
 * Create the options page
 */
function theme_options_do_page() {
	global $zto_groups;
	ordenaCampos();
	
	
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

		
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . __( 'Opções do Tema' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Opções salvas' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'ztheme' ); ?>
			<?php $options = get_option( 'ztheme_options' ); ?>

			<table class="form-table zt-form-table">

				<?php foreach ($zto_groups as $grupo): ?>
					<tr valign="top">
						<td colspan=2><h3><?php echo $grupo['titulo']; ?></h3></td>
					</tr>
					<?php foreach ($grupo['campos'] as $campo): 
						$valor = ( isset($campo['default']) AND is_null(@$options[$campo['id']]) ) ? $campo['default'] : @$options[$campo['id']];
						$class = isset($campo['class']) ? $campo['class'] : '';
						?>
						<tr valign="top"><th scope="row"><?php echo "<label class='{$class}' for='ztheme_options[{$campo['id']}]' >{$campo['label']}</label>"; ?></th>
							<td>
								<?php if (!isset($campo['type'])): ?>
									<input id="ztheme_options[<?php echo $campo['id']?>]" class="regular-text" type="text" name="ztheme_options[<?php echo $campo['id']?>]" value="<?php esc_attr_e( $valor ); ?>" />
								<?php elseif ($campo['type']=='password'): ?>
									<input id="ztheme_options[<?php echo $campo['id']?>]" class="regular-text" type="password" name="ztheme_options[<?php echo $campo['id']?>]" value="<?php esc_attr_e( $valor ); ?>" />
								<?php elseif ($campo['type']=='text'): 
									$attr = isset($campo['attr']) ? $campo['attr'] : array();
									echo HTML::textarea('ztheme_options['.$campo['id'].']', $valor, $attr); ?>
								<?php elseif ($campo['type']=='select'): 
									$attr = $campo['attr'] ? $campo['attr'] : array();
									echo HTML::select('ztheme_options['.$campo['id'].']', $campo['options'], $valor, array('class'=>@$campo['class']) + $attr, @$campo['num'])?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach;?>
				<?php endforeach;?>

			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Salvar Opções' ); ?>" />
			</p>
		</form>
	</div>
	<style type="text/css">
		label.icon       { padding:0 0 0 24px; display:block;	}
		label.facebook   { background: url(<?php bloginfo('template_url'); ?>/images/zt_face.png) no-repeat left top;	}
		label.orkut      { background: url(<?php bloginfo('template_url'); ?>/images/zt_orkut.png) no-repeat left top;	}
		label.twitter    { background: url(<?php bloginfo('template_url'); ?>/images/zt_twitter.png) no-repeat left top;	}
		label.linkedin   { background: url(<?php bloginfo('template_url'); ?>/images/zt_linkedin.png) no-repeat left top;	}
		label.feedburner { background: url(<?php bloginfo('template_url'); ?>/images/zt_feedburner.png) no-repeat left top;	}
		label.newsletter { background: url(<?php bloginfo('template_url'); ?>/images/zt_newsletter.png) no-repeat left top;	}
		label.flickr     { background: url(<?php bloginfo('template_url'); ?>/images/zt_flickr.png) no-repeat left top;	}
		label.text       { background: url(<?php bloginfo('template_url'); ?>/images/zt_text.png) no-repeat left top;	}
		.zt-form-table
			textarea     { width:300px; height:100px }
		select.medium    { width:150px }
		h3 { margin-top:2.5em; }
	</style>
	<?php
}

