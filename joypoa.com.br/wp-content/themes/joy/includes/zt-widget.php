<?php

abstract class ZT_WP_Widget extends WP_Widget
{
	protected  $types = array();
	
	function before(){
		$this->zt_get_types();
		if (in_array('image', $this->types)) {
			global $pagenow;
			if ($pagenow == 'widgets.php') {	
				add_action('admin_print_scripts', array(&$this, 'admin_scripts'));
				add_action('admin_print_styles',  array(&$this, 'admin_styles'));
			}
			add_filter( 'gettext', array( $this, 'replace_text_in_thickbox' ), 1, 3 );
		}
	}
	
	function update($new_instance, $old_instance) {
		return $this->zt_update($new_instance, $old_instance);
	}
	
	
	function form($instance) {
		$this->zt_form($instance);
	}

	
	function zt_update($new_instance, $old_instance) {
		$instance = $old_instance;
		foreach ($this->campos as $campo):
			if ($campo['html']===TRUE)
				$instance[$campo['id']] = $new_instance[$campo['id']];
			else
				$instance[$campo['id']] = strip_tags($new_instance[$campo['id']]);
		endforeach;
		return $instance;
	}
	
	
	function zt_form($instance) {
		if ( count($this->campos) == 0 )
			echo "<p><i>Widget sem configurações.</i></p>";
		foreach ( $this->campos as $campo ):
			if ( isset($campo['type']) && $campo['type'] == 'checkbox' ):
				$valor = isset($instance[$campo['id']]) ? (bool) $instance[$campo['id']] :false;
			else:
				$valor = esc_attr( $instance[$campo['id']] );
			endif; 
			?>
			<p>
				<?php if ( ! isset($campo['type']) || ! in_array( $campo['type'], array('checkbox', 'radiobox') ) ): ?>
					<label for="<?php echo $this->get_field_id($campo['id']); ?>"><?php _e($campo['label'].':'); ?></label>
				<?php endif;
				
				
				switch ( @$campo['type'] ){ 
					case 'text': ?>
						<textarea class="widefat" 
							id="<?php echo $this->get_field_id($campo['id']); ?>"
							name="<?php echo $this->get_field_name($campo['id']); ?>"
							><?php echo $valor; ?></textarea>
						<?php
						break;
					case 'select':
						$options = $this->zt_get_options($campo['options']);
						$nums = $campo['options']=='category' ? TRUE : FALSE;
						echo '<br/>'.html::select($this->get_field_name($campo['id']), $options, $valor, array('class'=>'widefat'), $nums);
						break;
					case 'checkbox': ?>
						<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id($campo['id']); ?>" name="<?php echo $this->get_field_name( $campo['id'] ); ?>"<?php checked( $valor ); ?> />
						<label for="<?php echo $this->get_field_id( $campo['id'] ); ?>"><?php echo $campo['label'] ?></label><br />
						<?php
						break;
					case 'image': 
						$media_upload_iframe_src = "media-upload.php?type=image&widget_id=".$this->id; //NOTE #1: the widget id is added here to allow uploader to only return array if this is used with image widget so that all other uploads are not harmed.
						$image_upload_iframe_src = apply_filters('image_upload_iframe_src', "$media_upload_iframe_src");
						?>
						<?php if (!empty($valor)) { ?>
							<br/><img src='<?php echo $valor; ?>' class='zt-widget-img' style="margin:5px 0; max-width:225px; max-height:250px" />
						<?php } ?>
						<input id="<?php echo $this->get_field_id( $campo['id'] ); ?>" name="<?php echo $this->get_field_name( $campo['id'] ); ?>" type="hidden" value="<?php echo $valor; ?>" />
						<input type="button" style="margin-bottom:5px" class="button fileupload" value="Escolher Imagem" rel="<?php echo $image_upload_iframe_src; ?>&TB_iframe=true"><br/>
						<?php
						break;
					default: ?>
						<input class="widefat" 
							id="<?php echo $this->get_field_id($campo['id']); ?>"
							name="<?php echo $this->get_field_name($campo['id']); ?>" type="text"
							value="<?php echo $valor; ?>" />
				<?php } ?>
			</p>
			<?php	
		endforeach;
	}
	
	function zt_get_types() {
		foreach ($this->campos as $campo):
			if (isset($campo['type']))
				$this->types[] = $campo['type'];
		endforeach;
	}
	
	function zt_get_options ($options) {
		if (is_array($options))
			return $options;
		
		switch($options){
			case 'category':
				$options = array();
				$categorias = get_categories();
				foreach ($categorias as $cat):
					$options[$cat->term_id] = $cat->name;
				endforeach;
				break;
		}
		return $options;
	}
	
	function admin_scripts(){
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('widget-image-uploader', get_bloginfo('template_url') . '/js/widget-image.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('widget-image-uploader');
	}
	
	
	function admin_styles(){
		wp_enqueue_style('thickbox');
	}
	
	/**
	 * Somewhat hacky way of replacing "Insert into Post" with "Insert into Widget"
	 *
	 * @param string $translated_text text that has already been translated (normally passed straight through)
	 * @param string $source_text text as it is in the code
	 * @param string $domain domain of the text
	 * @return void
	 * @author Shane & Peter, Inc. (Peter Chester)
	 */
	function replace_text_in_thickbox($translated_text, $source_text, $domain) {
		if ($this->is_sp_widget_context()) {
			if ('Insert into Post' == $source_text) {
				return 'Inserir no Widget';
			}
		}
		return $translated_text;
	}
	
	/**
	 * Test context to see if the uploader is being used for the image widget or for other regular uploads
	 *
	 * @return void
	 * @author Shane & Peter, Inc. (Peter Chester)
	 */
	function is_sp_widget_context() {
		if ( isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $this->id_base) !== false ) {
			return true;
		} elseif ( isset($_REQUEST['_wp_http_referer']) && strpos($_REQUEST['_wp_http_referer'],$this->id_base) !== false ) {
			return true;
		} elseif ( isset($_REQUEST['widget_id']) && strpos($_REQUEST['widget_id'],$this->id_base) !== false ) {
			return true;
		}
		return false;
	}
	
}