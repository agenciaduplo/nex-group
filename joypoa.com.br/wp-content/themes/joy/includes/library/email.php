<?php
require_once TEMPLATEPATH . '/includes/vendors/swift/swift_required.php';

// Cria configura o transport
global $zt_transport;
$seguranca = get_zto('email-security');
$seguranca = ( $seguranca && $seguranca != 'none' ) ? $seguranca : NULL; 
$zt_transport = Swift_SmtpTransport::newInstance( get_zto('email-server'), get_zto('email-port'), get_zto('email-security') )
		->setUsername( get_zto('email-user') )
		->setPassword( get_zto('email-pass') );

// Cria os campos para configuração
global $zto_groups;
$zto_groups[] = array(
				'titulo' => 'Configurações para Envio de E-mail', 'peso'=>15,
				'campos' =>
					array (
						array(
							'id'    => 'email-server',
							'label' => 'Servidor SMTP',
						),
						array(
							'id'    => 'email-user',
							'label' => 'Usuário',
						),
						array(
							'id'    => 'email-pass',
							'label' => 'Senha',
							'type'  => 'password'
						),
						array(
							'id'    => 'email-port',
							'label' => 'Porta (25, 465)',
						),
						array(
							'id'    => 'email-security',
							'label' => 'Segurança',
							'type'  => 'select',
							'options'=> array( 'none'=>'Nenhuma', 'ssl'=>'SSL', 'tls'=>'TLS' ),
							'attr'  => array('style'=>'min-width:175px'),
						),
						array(
							'id'    => 'email-email',
							'label' => 'E-mail remetente',
						),
					)
);
