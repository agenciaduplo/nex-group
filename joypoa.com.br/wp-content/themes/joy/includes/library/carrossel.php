<?php

/* Documentação: http://sorgalla.com/jcarousel/

Algumas Opções:
Autoscroll:  auto: 2 (número de segundos)
Circular:  wrap: 'circular'

*/

if (!is_admin()) {
	wp_enqueue_script( 'jcaurosel', get_bloginfo('template_url') . '/js/jquery.jcarousel.min.js', array('jquery'), '0.2.8', TRUE );
}

$caroussels = 0;
$caroussel_opcoes = array();

add_image_size('carousel', '150', '150', TRUE);

function the_carousel( $query, $size = 'carousel', $opcoes=NULL ) {
	global $carousels, $caroussel_opcoes;
	$carousels++;
	$id = "carousel-{$carousels}";
	
	$opcoes = $opcoes ? $opcoes . ', initCallback: carousel_ux' : 'initCallback: carousel_ux';
	$caroussel_opcoes[$carousels] = $opcoes;
	
	$posts = get_posts( $query );
	
	$itens = 0;
	
	$carousel = "<div id='container-{$id}'>";
		$carousel .= "<ul id='{$id}'>";
		global $post;
		foreach ($posts as $post): setup_postdata($post);
			if ( has_post_thumbnail($post->ID) && get_the_post_thumbnail($post->ID, $size) ) {
				$carousel .= '<li>';
					$carousel .= get_the_post_thumbnail($post->ID, $size);
				$carousel .= '</li>';
				$itens++;
			}
		endforeach;
		$carousel .= '</ul>';
	$carousel .= '</div>';
	
	if ( $itens > 0 ) {
		echo $carousel;
		add_action( 'zt_footer_js', 'carousel_js' );
	}
}


function the_carousel_content( $post_id=NULL, $size = 'carousel', $opcoes=NULL, $link = TRUE ) {
	global $carousels, $caroussel_opcoes, $post;
	$post_id = !is_null($post_id) ? $post_id : $post->ID; 
	
	$carousels++;
	$id = "carousel-{$carousels}";
	
	$opcoes = $opcoes ? $opcoes . ', initCallback: carousel_ux' : 'initCallback: carousel_ux';
	$caroussel_opcoes[$carousels] = $opcoes;
	
	$images = zt_get_images( $post_id, $size, 'menu_asc' );
	
	$itens = 0;
	
	$carousel = "<div id='container-{$id}'>";
		$carousel .= "<ul id='{$id}' class=''>";
		foreach ($images as $img):
			$carousel .= '<li>';
				$image = "<img src='{$img['url']}' alt='{$img['title']}' /><img class='img-mascara' src='".IMGPATH."mascara-img-carrossel.png' alt='mask' />";
				if ( $link )
					$carousel .= "<a href='{$img['url_full']}' rel='gal-{$id}' title='{$img['title']}'>{$image}</a>";
				else
					$carousel .= $image;
				$carousel .= "<p>{$img['title']}</p>";
			$carousel .= '</li>';
			$itens++;
		endforeach;
		$carousel .= '</ul>';
	$carousel .= '</div>';
	
	if ( $itens > 0 ) {
		echo $carousel;
		add_action( 'zt_footer_js', 'carousel_js' );
	}
}



function carousel_js(){ 
	global $carousels, $caroussel_opcoes;
	for ($i=1; $i<=$carousels; $i++) {
		$opcoes = @$caroussel_opcoes[$i] ? $caroussel_opcoes[$i] : '';
		?>
		
			jQuery('#carousel-<?php echo $i; ?>').jcarousel({<?php echo $opcoes; ?>}); <?php
	}	
}
