<?php


// Adiciona as metatags
add_action('zt_postitle', 'zt_add_seo_tags');
function zt_add_seo_tags(){
	global $post;
	if (is_single() || is_page()) {
		$custom = get_post_custom($post->ID);
		$description = !empty($custom['seo-description'][0]) ? $custom['seo-description'][0] : '';
		$keywords = !empty($custom['seo-keywords'][0]) ? $custom['seo-keywords'][0] : '';
	}
	elseif ( is_category() ) {
		$description = str_replace(array("\n", ""), array('', ''), strip_tags(category_description( get_query_var('cat') ) ) );
	}
	elseif ( is_home() || is_front_page() ) {
		$description = get_zto('seo-description');
		$keywords = get_zto('seo-keywords');
	}
	
	if ( !empty($description) ) {
		echo "<meta name='description' content='{$description}' />\n\t";
	}
	if ( !empty($keywords) ) {
		echo "<meta name='keywords' content='{$keywords}' />\n\t";
	}
}





// Adiciona o filtro no title
add_filter( 'zt_title', 'zt_title_seo_filter' );
function zt_title_seo_filter( $title ) {
	if ( is_single() || is_page() ) {
		global $post;
		$custom = get_post_custom($post->ID);
		$title = !empty($custom['seo-title'][0]) ? $custom['seo-title'][0] : $title;
	}
	if ( is_home() || is_front_page() ) {
		$titulo = get_zto('seo-title');
		$title = $titulo ? $titulo : $title;
	}
	return $title;
}


// Adiciona opções do tema
global $zto_groups;
$site_description = get_bloginfo( 'description', 'display' );
$title = get_bloginfo( 'name' );
$title = $site_description ? $title . ' | '. $site_description : $title; 
$campos = array (
			array( 'id' => 'seo-title', 'label' => 'Título da Home', 'default' => $title ),
			array( 'id' => 'seo-description', 'label' => 'Description da Home', 'default' => '', 'type' => 'text' ),
			array( 'id' => 'seo-keywords', 'label' => 'Keywords da Home', 'type' => 'text' ),
			);
$zto_groups[] = array('titulo' => 'SEO', 'peso'=>1, 'campos'=>$campos);