<?php
wp_enqueue_script( "jQueryUi", "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js", array('jquery'), FALSE );
wp_enqueue_script( "smoothMenu", get_bloginfo('template_url')."/js/jquery.ui.smoothMenu.js", array('jquery'), FALSE );

add_action( 'zt_footer_js', 'submenu_js');
function submenu_js(){
	echo "\n\t\t\tjQuery('.menu >li').smoothMenu({zIndex:10});";
}