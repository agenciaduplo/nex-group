<?php

wp_enqueue_script( "fancybox", get_bloginfo('template_url')."/js/jquery.fancybox-1.3.4.pack.js", array('jquery'), '1.3.4', TRUE );
wp_enqueue_script( "mousewheel", get_bloginfo('template_url')."/js/jquery.mousewheel-3.0.4.pack.js", array('jquery', 'fancybox'), '3.0.4', TRUE );
wp_enqueue_script( "easing", get_bloginfo('template_url')."/js/jquery.easing-1.3.pack.js", array('jquery', 'fancybox'), '1.3', TRUE );
wp_enqueue_style( "fancybox", get_bloginfo('template_url')."/css/jquery.fancybox-1.3.4.css" );
add_action( 'zt_footer_js', 'fancybox_gallery_js');
function fancybox_gallery_js(){
	echo "\n\t\t\tfancybox_galerias();";
}