<?php

add_action( 'zt_postitle', 'zt_open_graph' );

function zt_open_graph(){
	global $wp_query, $post, $blog;
	$valores = array();
	
	$valores['site_name'] = get_bloginfo('name'); 
	
	if ( is_single() || is_page() ) {
		$valores['title'] = get_the_title();
		$valores['image'] = zt_get_image();
		$valores['url'] = get_permalink();
		
		if ( is_single() ){
			$valores['type'] = 'article';
		}
		
	} else {
		$valores['image'] = IMGPATH . 'logo-login.png';
		if ( is_home() && $blog !== 1 ) {
			$valores['type'] = 'website';
		} else {
			$valores['type'] = 'blog';
		}
	}
	
	// Imprime as metatags
	if ( count($valores) >0 )
	foreach ( $valores as $property=>$content ) {
		echo "<meta property='og:{$property}' content='{$content}'/> \n\t";
	}
}