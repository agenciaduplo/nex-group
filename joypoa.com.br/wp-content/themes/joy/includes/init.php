<?php

/***********************
* Boot
***********************/
if (is_admin()) {
	require_once TEMPLATEPATH . '/includes/admin.php';
}
require_once TEMPLATEPATH . '/includes/functions.php';
require_once TEMPLATEPATH . '/includes/zt-widget.php';
require_once TEMPLATEPATH . '/includes/zt-post-type.php';
require_once TEMPLATEPATH . '/includes/zt-options.php';

// Customiza tela de login
function custom_login_logo() { echo '<style type="text/css">h1 a {margin-bottom:20px; background: url('.get_bloginfo('template_directory').'/images/logo-login.png) 50% 50% no-repeat !important; }</style>'; }
add_action('login_head', 'custom_login_logo');
function change_wp_login_url() { echo bloginfo('url'); }
add_filter('login_headerurl', 'change_wp_login_url');
function change_wp_login_title() { echo get_option('blogname'); }
add_filter('login_headertitle', 'change_wp_login_title');


$options = get_option( "ztheme_options" );

// Define limite de palavras do excerpt
function my_excerpt_length($text){ return 100; }
add_filter('excerpt_length', 'my_excerpt_length');


// Adiciona classes ao primeiro e ao último item de menu
function nav_menu_first_last( $items ) {
	$position = strrpos($items, 'class="menu-item', -1);
	$items=substr_replace($items, 'menu-item-last ', $position+7, 0);
	$position = strpos($items, 'class="menu-item');
	$items=substr_replace($items, 'menu-item-first ', $position+7, 0);
	return $items;
}
add_filter( 'wp_nav_menu_items', 'nav_menu_first_last' );


// Adiciona a classe last-post ao último post
add_filter('post_class', 'add_last_post_class');
function add_last_post_class($classes){
  global $wp_query;
  if(($wp_query->current_post+1) == $wp_query->post_count) $classes[] = 'last-post';
  return $classes;
}

// Adiciona classes indicando o posicionamento das widgets dentro da sidebar
function widget_first_last_classes($params) {
	global $my_widget_num; // Global a counter array
	$this_id = $params[0]['id']; // Get the id for the current sidebar we're processing
	$arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets
	if(!$my_widget_num) {
		// If the counter array doesn't exist, create it
		$my_widget_num = array();
	}
	if(!isset($arr_registered_widgets[$this_id]) || !is_array($arr_registered_widgets[$this_id])) {
		// Check if the current sidebar has no widgets
		return $params; // No widgets in this sidebar... bail early.
	}
	if(isset($my_widget_num[$this_id])) {
		// See if the counter array has an entry for this sidebar
		$my_widget_num[$this_id] ++;
	} else { // If not, create it starting with 1
		$my_widget_num[$this_id] = 1;
	}
	$class = 'class="widget-' . $my_widget_num[$this_id] . ' '; // Add a widget number class for additional styling options
	if($my_widget_num[$this_id] == 1) {
		// If this is the first widget
		$class .= 'widget-first ';
	} elseif($my_widget_num[$this_id] == count($arr_registered_widgets[$this_id])) {
		// If this is the last widget
		$class .= 'widget-last ';
	}
	$params[0]['before_widget'] = str_replace('class="', $class, $params[0]['before_widget']); // Insert our new classes into "before widget"
	return $params;
}
add_filter('dynamic_sidebar_params', 'widget_first_last_classes');


// Adiciona a opção de Archive ao gerenciador de menus
class cptArchiveNavMenu {
	public function __construct() {
		add_action( 'admin_head-nav-menus.php', array( $this, 'add_filters' ) );
	}

	public function add_filters() {
		$post_type_args = array(
			'show_in_nav_menus' => true
		);

		$post_types = get_post_types( $post_type_args, 'object' );

		foreach ( $post_types as $post_type ) {
			if ( $post_type->has_archive ) {
				add_filter( 'nav_menu_items_' . $post_type->name, array( $this, 'add_archive_checkbox' ), null, 3 );
			}
		}
	}

	public function add_archive_checkbox( $posts, $args, $post_type ) {
		global $_nav_menu_placeholder, $wp_rewrite;
		$_nav_menu_placeholder = ( 0 > $_nav_menu_placeholder ) ? intval($_nav_menu_placeholder) - 1 : -1;
		
		$url = substr(get_post_type_archive_link($args['post_type']), strlen(get_bloginfo('url')));
		
		array_unshift( $posts, (object) array(
			'ID' => 0,
			'object_id' => $_nav_menu_placeholder,
			'post_content' => '',
			'post_excerpt' => '',
			'post_title' => $post_type['args']->labels->all_items,
			'post_type' => 'nav_menu_item',
			'type' => 'custom',
			'url' => site_url( $url ),
		) );

		return $posts;
	}
}
$cptArchiveNavMenu = new cptArchiveNavMenu();


// Remoção dos banners da página de busca
function SearchFilter($query) {
    if ($query->is_search) {
    	$query->set( 'post_type', array( 'post', 'page' ) );
    }
    return $query;
} 
add_filter('pre_get_posts','SearchFilter');


function zt_the_title(){
	global $page, $paged, $wp_query;
	$title = wp_title( '|', false, 'right' );

	$title .= get_bloginfo( 'name' );

	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .=  " | $site_description";

	if ( $paged >= 2 || $page >= 2 )
		$title .= ' | ' . sprintf( __( 'Page %s'), max( $paged, $page ) );
		
	return $title;
}