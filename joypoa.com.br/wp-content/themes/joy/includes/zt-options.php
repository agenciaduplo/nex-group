<?php

class ZT_Page_Options {
	
	public $infos;
	
	
	function __construct ( $infos ) {
		$this->infos = $infos;
		add_action( 'admin_menu', array( &$this, '_do_menu' ) );
		add_action( 'admin_init', array( &$this, '_admin_init' ) );
		if ( isset( $infos['admin_bar'] ) ) add_action( 'wp_before_admin_bar_render', array( &$this, '_admin_bar' ) );
	}
	
	
	/**
	 * Cria o menu
	 */
	function _do_menu() {
		$ops = array(
			'titulo' => 'Opções',
			'capability' => 'manage_options'
		);
		$ops['id'] = str_replace(' ', '-', strtolower(remove_accents($ops['titulo'])));
		$ops = $ops + $this->infos['menu'];
		
		
		if ( isset( $this->infos['menu']['post_type'] ) ){
			$ops['id'] = $this->infos['menu']['post_type'] . '-' . $ops['id']; 
			add_submenu_page( 'edit.php?post_type=' . $ops['post_type'], $ops['titulo'], $ops['titulo'], $ops['capability'], $ops['id'], array( &$this, '_page' ) );
		} else {
			$ops['id'] = 'theme-' . $ops['id'];
			add_theme_page( __( 'Opções do Tema' ), __( 'Opções do Tema' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
		}
	}
	
	
	/**
	 * Imprime a Página
	 */
	function _page() {
		$campos = $this->infos['campos'];
		$titulo = isset($infos['page']['titulo']) ? $infos['page']['titulo'] : 'Opções';
		ordenaCampos( $campos );		
		
		if ( ! isset( $_REQUEST['settings-updated'] ) )
			$_REQUEST['settings-updated'] = false;
		?>
		<div class="wrap">
			<?php screen_icon(); echo "<h2>" . __( $titulo ) . "</h2>"; ?>
	
			<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
			<div class="updated fade"><p><strong><?php _e( 'Opções salvas' ); ?></strong></p></div>
			<?php endif; ?>
	
			<form method="post" action="options.php">
				<?php settings_fields( 'ztheme' ); ?>
				<?php $options = get_option( 'ztheme_options' ); ?>
	
				<table class="form-table zt-form-table">
	
					<?php foreach ($campos as $grupo): ?>
						<tr valign="top">
							<td colspan=2><h3><?php echo $grupo['titulo']; ?></h3></td>
						</tr>
						<?php foreach ($grupo['campos'] as $campo): 
							$valor = ( isset($campo['default']) AND is_null(@$options[$campo['id']]) ) ? $campo['default'] : @$options[$campo['id']];
							$class = isset($campo['class']) ? $campo['class'] : '';
							?>
							<tr valign="top"><th scope="row"><?php echo "<label class='{$class}' for='ztheme_options[{$campo['id']}]' >{$campo['label']}</label>"; ?></th>
								<td>
									<?php if (!isset($campo['type'])): ?>
										<input id="ztheme_options[<?php echo $campo['id']?>]" class="regular-text" type="text" name="ztheme_options[<?php echo $campo['id']?>]" value="<?php esc_attr_e( $valor ); ?>" />
									<?php elseif ($campo['type']=='password'): ?>
										<input id="ztheme_options[<?php echo $campo['id']?>]" class="regular-text" type="password" name="ztheme_options[<?php echo $campo['id']?>]" value="<?php esc_attr_e( $valor ); ?>" />
									<?php elseif ($campo['type']=='text'): 
										$attr = isset($campo['attr']) ? $campo['attr'] : array();
										echo HTML::textarea('ztheme_options['.$campo['id'].']', $valor, $attr); ?>
									<?php elseif ($campo['type']=='select'): 
										$attr = $campo['attr'] ? $campo['attr'] : array();
										echo HTML::select('ztheme_options['.$campo['id'].']', $campo['options'], $valor, array('class'=>@$campo['class']) + $attr, @$campo['num'])?>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach;?>
					<?php endforeach;?>
	
				</table>
	
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Salvar Opções' ); ?>" />
				</p>
			</form>
		</div>
		<style type="text/css">
			label.icon       { padding:0 0 0 24px; display:block;	}
			label.facebook   { background: url(<?php bloginfo('template_url'); ?>/images/zt_face.png) no-repeat left top;	}
			label.orkut      { background: url(<?php bloginfo('template_url'); ?>/images/zt_orkut.png) no-repeat left top;	}
			label.twitter    { background: url(<?php bloginfo('template_url'); ?>/images/zt_twitter.png) no-repeat left top;	}
			label.linkedin   { background: url(<?php bloginfo('template_url'); ?>/images/zt_linkedin.png) no-repeat left top;	}
			label.feedburner { background: url(<?php bloginfo('template_url'); ?>/images/zt_feedburner.png) no-repeat left top;	}
			label.newsletter { background: url(<?php bloginfo('template_url'); ?>/images/zt_newsletter.png) no-repeat left top;	}
			label.flickr     { background: url(<?php bloginfo('template_url'); ?>/images/zt_flickr.png) no-repeat left top;	}
			label.text       { background: url(<?php bloginfo('template_url'); ?>/images/zt_text.png) no-repeat left top;	}
			.zt-form-table
				textarea     { width:300px; height:100px }
			select.medium    { width:150px }
			h3 { margin-top:1.5em; }
		</style>
		<?php
	}
	
	
	function _admin_init () {
		register_setting( 'ztheme', 'ztheme_options', 'theme_options_validate' );
	}
	
	
	function _admin_bar () {
		global $wp_admin_bar;
		$ops = array( 'parent' => 'appearance', 'id' => 'theme_options', 'title' => __( 'Opções' ), 'href' => admin_url( 'themes.php?page=theme_options' ) );
		$wp_admin_bar->add_menu( $ops );
	}
	
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */

function theme_options_validate( $input ) {
	global $zto_groups;
	
	foreach ($zto_groups as $grupo):
		foreach ($grupo['campos'] as $campo):
			if (isset($campo['html']) AND $campo['html']===TRUE)
				$input[$campo['id']] = $input[$campo['id']];
			else
				$input[$campo['id']] = wp_filter_nohtml_kses( $input[$campo['id']] );
		endforeach;
	endforeach;

	return $input;
}

function ordenaCampos( $campos = NULL ){
	if ( ! $campos )
		global $zto_groups;
	else
		$zto_groups = $campos;
	
	foreach ($zto_groups as $key => $grupo):
		$ordenaveis[$key] = isset($grupo['peso']) ? $grupo['peso'] : 10; 
	endforeach;
	
	asort($ordenaveis);
	
	$ordenado = array();
	foreach ($ordenaveis as $key => $grupo):
		$ordenado[$key] = $zto_groups[$key];
	endforeach;
	
	$zto_groups = $ordenado;
}