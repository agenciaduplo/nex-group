jQuery(document).ready(function() {
	jQuery('#itens-organizaveis').sortable({
		stop:function(){
			jQuery('#msg_updated').hide();	
			jQuery('#itens-salvar').fadeIn();
			
		}
	});
	jQuery('#itens-salvar').click(function(){
		valores = jQuery('#itens-organizaveis').sortable('serialize');
		url = template_url + '/includes/post-type/salvar-ordem.php';
		jQuery.post(url, {valores:valores}, function(ret){
			if (ret == 'ok') {
				jQuery('#itens-salvar').hide().after('<div class="updated" id="msg_updated"><p>Ordem salva com sucesso!</p></div>');
			}
		});
	})
});
