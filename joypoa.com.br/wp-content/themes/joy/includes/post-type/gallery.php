<?php global $post, $_wp_additional_image_sizes; $custom = get_post_custom($post->ID); ?>
<div id="file-uploader">       
    <noscript>          
        <p>Please enable JavaScript to use file uploader.</p>
    </noscript>         
</div>

<input type='hidden' name='zt_galeria' value='1' />
<ul id="galeria">
	<?php 
	if ( isset($custom['zt_galeria_images'][0]) ) {
		$images = json_decode( $custom['zt_galeria_images'][0] );
		if (is_array($images)) {
			foreach( $images as $img ):
				$imgtxt = json_encode($img);
				echo "<li><img src='{$img->thumbnail->url}' alt='' />";
					echo "<a href='#' class='remove-image'><img src='".IMGPATH."/delete16.gif' alt='' /></a>";
					echo "<a href='".get_bloginfo('template_url')."/includes/post-type/gallery-imagem-options.php?image={$img->file_name}&post_id={$img->post_id}&width=200' class='edit-image thickbox'>Editar</a>";
					echo "<input type='hidden' name='zt_galeria_images[]' value='{$imgtxt}' />";
				echo "</li>";
			endforeach;
		}
	}
	?>
</ul>
<div class="clear">&nbsp;</div>
