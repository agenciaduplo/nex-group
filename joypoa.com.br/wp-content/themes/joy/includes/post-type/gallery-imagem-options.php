<?php require_once("../../../../../wp-config.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	<style type="text/css">
		
		#imagem { width:170px; float:left; margin-top:10px }
		#formulario { float:left;  margin-top:10px }
		#formulario label, #formulario input { display: block }
	</style>
</head>
<body>
	<?php 
		$image = $_GET['image'];
		$post_id = filter_var( $_GET['post_id'], FILTER_SANITIZE_NUMBER_INT );
		$images = get_post_meta( $post_id, 'zt_galeria_images' );
		$images = json_decode( $images[0] );
		
		foreach ( $images as $img ):
			if ( $img->file_name == $image)	break;
		endforeach;
	?>
	<div id="imagem">
		<img src="<?php echo $img->thumbnail->url; ?>" alt="Imagem" />
	</div>
	<div id="formulario">
		<form action="" method="post">
			<label for="legenda">Legenda</label>
			<input type="text" name="legenda" id="legenda" />
			<button>Salvar</button>
		</form>
	</div>
</body>
</html>