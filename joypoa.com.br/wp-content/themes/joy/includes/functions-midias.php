<?php

// Classe para lidar com o Slideshare
class Zt_Slideshare{
	
	static function the_slide( $shortcode, $size =  array( 'width'=>425, 'height'=>355 ) ) {
		echo self::get_the_slide( $shortcode, $size ); 
	}
	
	static function get_the_slide( $shortcode, $size = array( 'width'=>425, 'height'=>355 ) ) {
		$id = self::get_id($shortcode);
		return '<iframe src="http://www.slideshare.net/slideshow/embed_code/'.$id.'?rel=0" width="'.$size['width'].'" height="'.$size['height'].'" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe><br/><br/>';
	}
	
	static function get_id( $shortcode ) {
		$shortcode = str_replace(array('[', ']', 'slideshare'), '', $shortcode);
		$id = ( shortcode_atts( array( 'id' => '' ), shortcode_parse_atts($shortcode) ) );
		$id = explode('&', $id['id']);
		return $id[0];
	} 
	
}


// Classe para lidar com vídeos do Youtube
class Zt_Video {
	public $url;
	public $server;
	public $video;
	
	function __construct( $url ) {
		$this->url = $url;
		$image_url = parse_url($url);
		if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
			$this->server = 'youtube';
		} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
			$this->server = 'vimeo';
		}
		$this->video = self::get_the_video( $url );
	}
	
	function video( $size = array('width'=>395, 'height'=>215) ) {
		return self::get_the_video( $url, $size );
	}
	
	static function get_the_screen ($url) {
		$capa = self::get_screen($url);
		return "<img src='{$capa}' width='210' />";
	}
	
	static function get_screen ($url) {
		$image_url = parse_url($url);
		if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
			$array = explode("&", $image_url['query']);
			return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
		} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
			$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
			return $hash[0]["thumbnail_medium"];
		}
	}
	
	static function get_the_video($url, $size = array('width'=>395, 'height'=>215)) {
		$url = parse_url($url);
		if($url['host'] == 'www.youtube.com' || $url['host'] == 'youtube.com'){
			$array = explode("&", $url['query']);
			return '<iframe width="' . $size['width'] . '" height="' . $size['height'] . '" src="http://www.youtube.com/embed/' . substr($array[0], 2) . '?wmode=opaque" frameborder="0" allowfullscreen></iframe>';
		} else if($url['host'] == 'www.vimeo.com' || $url['host'] == 'vimeo.com'){
			return '<iframe src="http://player.vimeo.com/video/' . substr($url['path'], 1) . '?title=0&amp;byline=0&amp;portrait=0&amp;color=FCAF17" width="' . $size['width'] . '" height="' . $size['height'] . '" frameborder="0"></iframe>';
		}
	}
	
	static function the_video($url, $size = array('width'=>395, 'height'=>215)) {
		echo self::get_the_video($url, $size);
	}
	
	static function get_id($url){
		$url = parse_url($url);
		$array = explode("&", $url['query']);
		return substr($array[0], 2);
	}
	
	static function get_the_title( $url ) {
		$vidID = self::get_id($url);
		$url = "http://gdata.youtube.com/feeds/api/videos/". $vidID;
	    $doc = new DOMDocument;
	    $doc->load($url);
	    return $doc->getElementsByTagName("title")->item(0)->nodeValue;
	}
	
}
