<?php 

// Customiza parte ADM
function change_admin_head_logo() { echo '<style type="text/css">	#header-logo{ background:url('.get_bloginfo('template_directory').'/images/wp-logo.png) left center no-repeat; width:16px; height:16px;	}</style>'; }
add_action('admin_head', 'change_admin_head_logo', 11);


// Chama página de opções do Tema
require_once TEMPLATEPATH . '/theme-options.php';
require_once TEMPLATEPATH . '/includes/zt-theme-options.php';


// Frase do rodapé na interface administrativa
function remove_footer_admin() { echo 'Obrigado por desenvolver o seu tema com a <a href="http://www.zira.com.br" target="_blank">Zira</a>.'; }
add_filter('admin_footer_text', 'remove_footer_admin');


// Remove itens do menu
function remove_menu_items() {
	global $menu;
	$restricted = array(__('Links'), __('Comments'), __('Posts'));
	end ($menu);
	while (prev($menu)){
		$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
	}
}
add_action('admin_menu', 'remove_menu_items');


// Adiciona separadores entre os menus
function admin_menu_separator() { add_admin_menu_separator(9); }
//add_action('admin_init','admin_menu_separator');
