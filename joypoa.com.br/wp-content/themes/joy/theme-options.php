<?php
$zto_groups = array();
$zto_groups[] = array(
				'titulo' => 'Redes Sociais e Afins', 'peso'=>5,
				'campos' =>
						array (
							array(
								'id'    => 'facebook-url',
								'label' => 'URL do Facebook',
								'class' => 'facebook icon',
							),
							array(
								'id'    => 'facebook-id',
								'label' => 'ID do Facebook',
								'class' => 'facebook icon',
							),
							/*array(
								'id'    => 'orkut-url',
								'label' => 'URL do Orkut',
								'class' => 'orkut icon',
							),
							array(
								'id'    => 'orkut-membros',
								'label' => 'Membros do Orkut',
								'class' => 'orkut icon',
							),
							*/
							array(
								'id'    => 'linkedin-url',
								'label' => 'URL do Linkedin',
								'class' => 'linkedin icon',
							),
							array(
								'id'    => 'twitter-user',
								'label' => 'Usuário do Twitter',
								'class' => 'twitter icon',
							),
							array(
								'id'    => 'feedburner-user',
								'label' => 'Usuário do Feedburner',
								'class' => 'feedburner icon',
							),
					)
			);
			
$campos = array (
			array( 'id' => 'endereco', 'label' => 'Endereço' ),
			array( 'id' => 'telefone', 'label' => 'Telefone' ),
			);
$zto_groups[] = array('titulo' => 'Informações de endereço', 'peso'=>2, 'campos'=>$campos);			
			
$campos = array (
			array( 'id' => 'contato-destino', 'label' => 'Destinatário' ),
			array( 'id' => 'contato-assunto', 'label' => 'Assunto', 'default' => 'Mensagem enviada através do site' ),
			);
$zto_groups[] = array('titulo' => 'Formulário de Contato', 'peso'=>3, 'campos'=>$campos);