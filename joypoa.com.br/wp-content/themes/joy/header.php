<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="zmGAnujZOZJNAmMM8A0E5bhyDCCD-4Bx28BfDIBep3Q" />
    <meta name="robots" content="index, follow">
	<title><?php echo apply_filters( 'zt_title', zt_the_title() ); ?></title>

	<?php do_action( 'zt_postitle' ); ?>

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=1.1" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /><?php
	$url = (get_zto('feedburner-user')) ? 'http://feeds.feedburner.com/' . get_zto('feedburner-user') : get_bloginfo('rss2_url'); ?>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo $url; ?>" />
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/ie7.css" />
		<style type="text/css">
			div#contato-direita input, div#contato-direita textarea, div#menu ul li a, div#barra-fixa, div#contato-direita input:focus, div#contato-direita textarea:focus { behavior:url("<?php bloginfo('template_url'); ?>/js/PIE.htc"); }
		</style>
	<![endif]-->

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action( 'zt_posbody' ); ?>
	<div id="header">

		<div class="centralizador relativo">
			<h1 id="logotipo"><img src="<?php echo IMGPATH ?>logo-joy.png" alt="O JOY é o lugar perfeito para ser feliz dentro e fora de casa. Ser feliz é morar em um bairro que fica perto de tudo e aproveitar a comodidade que só uma infraestrutura completa pode proporcionar. Ser feliz é garantir o bem-estar da família toda, sabendo que seus filhos vão brincar com todo conforto e segurança. São várias opções de plantas para vocês escolher a que mais combina com o seu estilo de vida e da sua família. VENHA SER FELIZ NO JOY." /></h1>
		</div>

	</div>
	<div id="menu">
		<div class="centralizador relativo">
			<ul>
				<li class="menu-item-first" id="menus-empreendimento"><a href="#empreendimento" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'O Empreendimento']);"><span>O EMPREENDIMENTO</span></a></li>
				<li id="menus-apartamentos"><a href="#apartamentos" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Os Apartamentos']);"><span>OS APARTAMENTOS</span></a></li>
				<li id="menus-infraestrutura"><a href="#infraestrutura" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'A Infraestrutura']);"><span>A INFRAESTRUTURA</span></a></li>
				<li id="menus-localizacao"><a href="#localizacao" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Localização']);"><span>LOCALIZAÇÃO</span></a></li>
				<li class="menu-item-last" id="menus-contato"><a href="#contato" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Contato']);"><span>CONTATO</span></a></li>
			</ul>

			<div id="predio">
				<img src="<?php echo IMGPATH ?>predio.png" alt="Apartamentos de dois e três dormitórios no bairro Bom Vista em Porto Alegre." />
			</div>

			<div id="borda-menu">
				<img src="<?php echo IMGPATH ?>borda-menu.png" alt="borda" />
			</div>

			<div id="share-this">
				<a href="http://www.dhz.com.br/" id="topo-dhz" target="_blank"><img src="<?php echo IMGPATH; ?>topo-dhz.png" alt="DHZ" /></a>
				<a href="http://www.nexgroup.com.br/" id="topo-nex" target="_blank"><img src="<?php echo IMGPATH; ?>topo-nex.png" alt="Nex Group" /></a>
				<?php //if(function_exists('sharethis_button')) echo sharethis_button(); ?>
				<a class="" onclick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Corretor Online']);" title="Converse com nossos Corretores On-line" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/entrar.php?id_produto=4','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">
					<div class="corretor">&nbsp;</div>
				</a>

			</div>
		</div>
	</div>