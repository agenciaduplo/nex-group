	<div class="centralizador">
		<div id="conteudo">
			<div id="footer" class="relativo">
				
				<div id="footer-esquerda">
					<p id="ser-feliz" class="ff">
						Venha ser feliz no Joy.
					</p>
					<p id="endereco">
						<?php zto('endereco')  ?> <br />
						Tel: <?php zto('telefone') ?>
					</p>
				
					<p id="apoio">
						Incorporação e Construção:
						<a href="http://www.dhz.com.br/" target="_blank"><img class="dhz-img" src="<?php echo IMGPATH ?>dhz.png" alt="DHZ Construções" /></a>
						<a href="http://www.nexgroup.com.br/" target="_blank"><img src="<?php echo IMGPATH ?>nex.png" alt="uma empresa Nex Group" /></a>
					</p>
				
					<p id="direitos">
						Todos os direitos reservados a DHZ Construções. Veja mais em 
						<a target="_blank" href="http://www.dhz.com.br/joy">dhz.com.br/joy</a>
					</p>
					
				</div>
				
				<div id="footer-direita" class="relativo">
					<div id="img-footer">
						<img src="<?php echo IMGPATH ?>img-footer.png" alt="O JOY é o lugar perfeito para ser feliz dentro e fora de casa." />
					</div>
				</div>
				
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	
	<div id="barra-fixa">
		<div class="center-barra-fixa relativo">
		
			<div class="logo-barra-fixa">
				<a href="#header"><img src="<?php echo IMGPATH ?>logo-joy-2.png" alt="Joy" /></a>
			</div>
			
			<ul>
				<li class="menu-item-first"><a href="#empreendimento" id="menuf-empreendimento" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'O Empreendimento']);">Empreendimento</a></li>
				<li><a href="#apartamentos" id="menuf-apartamentos" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Os Apartamentos']);">Apartamentos</a></li>
				<li><a href="#infraestrutura" id="menuf-infraestrutura" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'A Infraestrutura']);">Infraestrutura</a></li>
				<li><a href="#localizacao" id="menuf-localizacao" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Localização']);">Localização</a></li>
				<li class="menu-item-last"><a href="#contato" id="menuf-contato" onclick="_gaq.push(['_trackEvent', 'Menu', 'Click', 'Contato']);">Contato</a></li>
			</ul>
			
			<div class="corretor-barra-fixa">
				<a class="" onclick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Corretor Online']);" title="Converse com nossos Corretores On-line" href="javascript:window.open('http://nex.hypnobox.com.br/atendimento/entrar.php?id_produto=4','pop','width=450, height=450, top=100, left=100, scrollbars=no');void(0);">
				<img src="<?php echo IMGPATH ?>corretor-online1.png" alt="Corretor Online" /></a>
			</div>
			
		</div>
	</div>
	
	<?php wp_footer(); ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			<?php do_action('zt_footer_js'); ?>
			menu_sup();
			tela = jQuery(window).height();
			foot = jQuery('#footer').height();
			contato = jQuery('#contato');
			if ( (contato.height() + foot + 90) < tela ){
				contato.height( tela - ( contato.height() + footer + 170 ));
			}

			mapa();
			var locais = [
               ['Joy', -30.01650, -51.16951, '<?php echo IMGPATH; ?>map-icons.png', [151, 88], [0, 40], [75, 70]],
               ['Posto 35', -30.016838, -51.171908, '<?php echo IMGPATH; ?>map-icons.png', [32, 38], [7,0], [16, 37]],
               ['Supermercado Zaffari Higienópolis', -30.02046, -51.18089, '<?php echo IMGPATH; ?>map-icons.png', [32, 38], [58,0], [16, 38]],
               ['Supermercado Carrefour', -30.013516, -51.171777, '<?php echo IMGPATH; ?>map-icons.png', [32, 38], [58, 0], [16, 38]],
               ['Shopping Bourbon Country', -30.022665, -51.162386, '<?php echo IMGPATH; ?>map-icons.png', [32, 38], [111, 0], [16, 38]],
               ['Shopping Iguatemi', -30.02663, -51.16315, '<?php echo IMGPATH; ?>map-icons.png', [32, 38], [111, 0], [16, 38]]
			];
			setMarkers( map, locais );
		});
	</script>
	
	<!-- Código do Google para tag de remarketing -->
		<!--------------------------------------------------
		As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 978900397;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/978900397/?value=0&guid=ON&script=0"/>
			</div>
		</noscript>
	
</body>
</html>
