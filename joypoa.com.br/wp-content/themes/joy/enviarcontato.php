<?php
// Requisições mínimas
require('./../../../wp-config.php');

// Verifica o servidor de envio de e-mails
$server = get_zto('email-server');
if ( empty($server) ) 
	exit("Nenhum servidor de envio de e-mails foi configurado. Para resolver isso, na interface administrativa v&aacute; em <i>Apar&ecirc;ncia > Op&ccedil;&otilde;es do Tema > Configura&ccedil;&otilde;es para Envio de E-mail</i>.");

// Configurações básicas
$emails   = explode( ',', get_zto( 'contato-destino', 'thiago@zira.com.br' ) );
$mailto = array();
foreach ( $emails as $mail ):
	$mail = trim($mail);
	if ( filter_var( $mail, FILTER_SANITIZE_EMAIL ) )
		$mailto[] = $mail;
endforeach;
$subject  = get_zto('contato-assunto', "Mensagem enviada através do site");

// Pega dados postados
$nome     = strip_tags( trim( ucwords( $_POST['nome'] ) ) ); 
$email    = filter_var( $_POST['email'], FILTER_SANITIZE_EMAIL );
$solicitacao = filter_var( strip_tags($_POST['solicitacao']), FILTER_SANITIZE_STRING );
$campos = array( 'telefone', 
				 'mensagem',
				 );


// Valida o e-mail
if ( strlen($_POST['nome']) < 4 && @$_POST['spambot']!=1) {
	echo 'email_error';
} else {
	global $zt_transport;
	
	$mailer = Swift_Mailer::newInstance($zt_transport);
	
	$email_message = 
			"<b>Tipo de Solicitação</b>: {$solicitacao} <br /> " .
			"<b>Nome</b>: " . ucwords($nome) . "<br/>" .
			"<b>E-mail</b>: {$email} <br/>" ;
			
	
	
	foreach ($campos as $label=>$campo):
		$label = (is_int($label)) ? ucfirst($campo) : $label;
		$valor = nl2br( trim( strip_tags( $_POST[$campo] ) ) );
		$email_message .= "<b>{$label}</b>: {$valor} <br/>" ;
	endforeach;
	
	$email_message = trim(stripslashes($email_message));
		
	ob_start();
	
	$assunto = $subject;
	$conteudo = $email_message;
	require TEMPLATEPATH . '/includes/template-email.php';
			
	$template = ob_get_contents();
	ob_end_clean();
	
	$message = Swift_Message::newInstance( $subject )
	  ->setContentType('text/html')
	  ->setFrom( array(get_zto('email-email') => get_bloginfo('name')) )
	  ->setTo( $mailto )
	  ->setBody( $template );
	 
	if (filter_var($email, FILTER_VALIDATE_EMAIL))
		$message->setReplyTo( $email );
	
	echo $mailer->send($message);

  }
