<?php

if ($_GET['in'] != 'ajax' && $_GET['in'] != 'enviaContact' && $_GET['in'] != 'enviaSolicitacao') {
    $HTML = new htmlStructure("structure.tpl");
    $HTML->configPage();
    $HTML->body->substituiValor("swfPath", "SWF_PATH");
    $HTML->setTitle($core->url->title);
    $HTML->setDescription($core->url->description);
    $HTML->setKeywords($core->url->keywords);
    $HTML->makeLog = FALSE;
    $HTML->printPage();
} else
    Main();

function Main() {
	global $core;
	
	$core->TPLV = new TemplatePower(TEMPLATE_PATH . "contact.tpl");
	$core->TPLV->assignGlobal("uploadPath", UPLOAD_DIR);
	$core->TPLV->assignGlobal("imagePath", IMAGE_PATH);
	$core->TPLV->assignGlobal("swfPath", SWF_PATH);
	$core->TPLV->assignGlobal("localPath", LOCAL_PATH);
	$core->TPLV->assignGlobal($core->url->var);
	$core->TPLV->assignGlobal($core->lang);
	$core->TPLV->prepare();
	
	$in = $_GET['in'];
	
	switch ($in) {
		default:
		case 'contact':
			contact();
			break;
		case 'enviaContact':
			enviaContact();
			break;
		case 'enviaSolicitacao':
			enviaSolicitacao();
			break;
	}
}

function contact() {
	global $core;
		
		$core->TPLV->assignGlobal('boxPackages', $core -> element -> getPackages());
		$core->TPLV->assignGlobal('boxRedesocial', $core -> element -> getRedeSocial());
		
		if($_REQUEST['retorno']) {
			switch ($_REQUEST['retorno']) {
				case 'ok':
					$msg = 'seu contato/mensagem foi enviado com sucesso!!'; 
				break;
				case 'erro':
					$msg = 'Erro ao enviar os dados. Tente mais tarde!';
				break;
			}
			$core->TPLV->newBlock('retorno');
			$core->TPLV->assign('msg',$msg);
		}
			
  $core->TPLV->printToScreen();
}

function enviaContact(){
	global $core;

  	if (preg_match("/bcc:|cc:|Return-Path:|multipart|\[url|Content-Type:/i", implode($_REQUEST))) $spam = true;
	  if (preg_match_all("/<a|http:/i", implode($_REQUEST), $out) > 3) $spam = true;
		
  	extract($_REQUEST);
		
  	if ($nome && $spam == false) {
		
    	$body = '<h1>Contato de ' . $nome . ' pelo Site - Terra Nova</h1>
							 <b>Nome: </b> ' . $nome . '<br/>
							 <b>E-mail: </b> ' . $email . '<br/>
							 <b>Fone: </b> ' . $fone . '<br/>
							 <b>Mensagem: </b> ' . $mensagem . '<br/>
							 ';
			
      $core->db->db_query("INSERT INTO frm_contact (name, email, phone, message, datehora)
										VALUES ('$nome', '$email', '$fone', '$mensagem', NOW())");
			      
      $core->email->SetFrom('rewebnews@gmail.com', 'Contato de '.$nome.' pelo Site - Terra Nova');
      $core->email->MsgHTML($body);
			$core->email->Subject = 'Contato de '.$nome.' pelo Site - Terra Nova';      
      /* destino do contato do site */
      //$core->email->AddAddress('augusto@reweb.com.br', 'Terra Nova');
			
			$core->email->AddAddress('francisco@terranovaturismoviagens.com.br', 'Terra Nova');
			$core->email->AddCC('tatiane@terranovaturismoviagens.com.br', 'Terra Nova');
			$core->email->AddCC('marketing@reweb.com.br', 'Reweb Marketing');
      
			$core->email->Send();
						
      $retorno = 'ok';    
    } else
      	$retorno = 'erro';
   	 		header("Location: ".$core->url->var['on=contact'].'|retorno='.$retorno);
}
function enviaSolicitacao(){
	global $core;
        
  	if (preg_match("/bcc:|cc:|Return-Path:|multipart|\[url|Content-Type:/i", implode($_REQUEST))) $spam = true;
    	if (preg_match_all("/<a|http:/i", implode($_REQUEST), $out) > 3) $spam = true;
   	
  	extract($_REQUEST);
		
  	if ($nomeS && $spam == false) {
		
    	$body = '<h1>Solicitação de Contato de ' . $nome . ' pelo Site - Terra Nova</h1>
							 <b>Nome: </b> ' . $nomeS . '<br/>
							 <b>E-mail: </b> ' . $emailS . '<br/>
							 <b>Fone: </b> ' . $foneS . '<br/>
							 <b>Pacote: </b> ' . $pacoteS . '<br/>
                                                         <b>Mensagem: </b> ' . $mensagemS . '<br/>
							 ';
			
      $core->db->db_query("INSERT INTO frm_solicitacao (name, email, phone,package, message, datehora)
										VALUES ('$nomeS', '$emailS', '$foneS','$pacoteS', '$mensagemS', NOW())");
			      
      $core->email->SetFrom('rewebnews@gmail.com', 'Solicitação de Contato de '.$nome.' pelo Site - Terra Nova');
      $core->email->MsgHTML($body);
			$core->email->Subject = 'Solicitação de Contato de '.$nome.' pelo Site - Terra Nova';      
      /* destino do contato do site */
      //$core->email->AddAddress('augusto@reweb.com.br', 'Terra Nova');
			
			$core->email->AddAddress('francisco@terranovaturismoviagens.com.br', 'Terra Nova');
			$core->email->AddCC('tatiane@terranovaturismoviagens.com.br', 'Terra Nova');
			$core->email->AddCC('marketing@reweb.com.br', 'Reweb Marketing');
      
			$core->email->Send();
						
      $retorno = 'ok';    
    } else
      	$retorno = 'erro';
   	 		header("Location: ".$core->url->var['on=contact'].'|retorno='.$retorno);
}
